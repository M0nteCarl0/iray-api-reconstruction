namespace PlugInInterface
{
	public interface IPlugIn
	{
		object LoadComponent();

		bool InitDetectorInstance(object obj);
	}
}
