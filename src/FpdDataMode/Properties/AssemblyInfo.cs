using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Security;
using System.Security.Permissions;

[assembly: CompilationRelaxations(8)]
[assembly: AssemblyTitle("FpdDataModel")]
[assembly: AssemblyDescription("Data Model Module of XRay Detector Control Software")]
[assembly: AssemblyFileVersion("4.0.29.5086")]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyCompany("XRay")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright (C) 2015, 2018, XRay")]
[assembly: AssemblyProduct("XRay Detector Control Tool")]
[assembly: AssemblyTrademark("XRay")]
[assembly: ComVisible(false)]
[assembly: Guid("74b90b8b-623c-44e6-a7c6-6c63cb1b21b6")]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
[assembly: AssemblyVersion("4.0.29.5086")]
[module: UnverifiableCode]
