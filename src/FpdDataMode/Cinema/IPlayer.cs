using iDetector;

namespace Cinema
{
	public interface IPlayer
	{
		int InitParas(string filePath, int width, int height);

		void Close();

		int ReadNextImage(ref IRayImageData image);

		int ReadPrevImage(ref IRayImageData image);

		int ReadImage(ref IRayImageData image);

		void Reset();
	}
}
