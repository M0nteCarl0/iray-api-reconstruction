using iDetector;
using System;
using System.Runtime.InteropServices;

namespace Cinema
{
	public class TIFSave : ISave
	{
		private IntPtr mTiffMng;

		private IntPtr mImgPtr;

		private bool mbOpened;

		public ulong GetMaxFileSize()
		{
			return 4294967295uL;
		}

		public uint ImageHeaderSize()
		{
			return 512u;
		}

		public bool StartSave(string Filepath)
		{
			mTiffMng = TiffParser.CreateImageMng();
			mbOpened = TiffParser.TiffOpen(mTiffMng, Marshal.StringToCoTaskMemAnsi(Filepath), 1);
			if (mbOpened)
			{
				mImgPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IRayImage)));
			}
			return mbOpened;
		}

		public void StopSave()
		{
			if (mbOpened)
			{
				Marshal.FreeHGlobal(mImgPtr);
				TiffParser.TiffClose(mTiffMng);
				TiffParser.ReleaseImageMng(mTiffMng);
			}
		}

		public void DoSave(ref IRayImageData Image)
		{
			if (mbOpened && Image != null)
			{
				using (IRayImageEx rayImageEx = new IRayImageEx(ref Image))
				{
					Marshal.StructureToPtr((object)rayImageEx.iRayImage, mImgPtr, true);
					TiffParser.TiffWriteImage(mTiffMng, mImgPtr);
				}
			}
		}
	}
}
