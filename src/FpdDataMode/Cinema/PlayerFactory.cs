using System;

namespace Cinema
{
	public class PlayerFactory
	{
		public static IPlayer Create(string filePath)
		{
			string text = filePath.Substring(filePath.LastIndexOf(".") + 1, filePath.Length - filePath.LastIndexOf(".") - 1);
			string typeName = "Cinema." + text.ToUpper() + "CinePlayer";
			Type type = Type.GetType(typeName);
			if (null == type)
			{
				return null;
			}
			return (IPlayer)type.Assembly.CreateInstance(typeName);
		}
	}
}
