using iDetector;
using System;
using System.IO;

namespace Cinema
{
	public class RAWCinePlayer : RawBasePlayer, IPlayer
	{
		public int InitParas(string filePath, int width, int height)
		{
			if (!File.Exists(filePath) || width * height == 0)
			{
				return 0;
			}
			try
			{
				mFile = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
				if (mFile == null)
				{
					return 0;
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("load raw file failed! {0}", ex.ToString());
				return 0;
			}
			mFirstOffset = 0L;
			mFrameSize = width * height * 2;
			mCurIndex = -1;
			mStartIndex = 0;
			mFrameNumber = (int)(mFile.Length / mFrameSize);
			return mFrameNumber;
		}

		public void Close()
		{
			CloseFile();
		}

		public int ReadNextImage(ref IRayImageData image)
		{
			return NextImage(ref image);
		}

		public int ReadPrevImage(ref IRayImageData image)
		{
			return PrevImage(ref image);
		}

		public int ReadImage(ref IRayImageData image)
		{
			return 0;
		}
	}
}
