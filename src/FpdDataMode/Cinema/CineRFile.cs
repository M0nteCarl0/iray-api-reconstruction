using System;
using System.IO;

namespace Cinema
{
	public class CineRFile : CineFile
	{
		public static int HeaderLength = 8;

		public int FirstFrameIndex
		{
			get
			{
				return mFirstFrameIndex;
			}
		}

		public int MaxFrameCount
		{
			get
			{
				return mMaxFrameCount;
			}
		}

		public long StartOffset
		{
			get;
			private set;
		}

		public CineRFile(string filePath, int frameSize)
			: base(frameSize)
		{
			try
			{
				mFile = new FileStream(filePath, FileMode.Open, FileAccess.Read);
				byte[] array = new byte[8];
				mFile.Seek(-8L, SeekOrigin.End);
				mFile.Read(array, 0, array.Length);
				StartOffset = BitConverter.ToInt64(array, 0);
				mFile.Seek(StartOffset, SeekOrigin.Begin);
				mFirstFrameIndex = (int)(StartOffset / frameSize);
				mMaxFrameCount = (int)(mFile.Length / frameSize);
				base.Result = true;
			}
			catch (Exception ex)
			{
				mFile = null;
				base.Result = false;
				throw ex;
			}
		}
	}
}
