using iDetector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Cinema
{
	public class CineBuffer
	{
		private List<IRayVariantMapItem[]> mImgParaQueue;

		private int mIndex;

		public int ImgWidth;

		public int ImgHeight;

		private int FrameSize;

		private CineFile mCineFile;

		private MultiIrayImageBuffer ImageQueue;

		private bool bFileClosed;

		public List<IRayVariantMapItem[]> ImgParas
		{
			get
			{
				return mImgParaQueue;
			}
		}

		public int FrameCount
		{
			get
			{
				return mImgParaQueue.Count;
			}
		}

		public int MaxFrameCount
		{
			get;
			private set;
		}

		public CineBuffer(int frames, int width, int height)
		{
			MaxFrameCount = frames;
			mIndex = 0;
			ImgWidth = width;
			ImgHeight = height;
			FrameSize = width * height * 2;
			mImgParaQueue = new List<IRayVariantMapItem[]>();
		}

		public CineBuffer(int frames, int frameSize)
		{
			FrameSize = frameSize;
		}

		public void Close()
		{
			StopWrite();
			mImgParaQueue.Clear();
		}

		public bool StartWrite(string cineFilePath)
		{
			bFileClosed = false;
			mIndex = 0;
			mImgParaQueue.Clear();
			mCineFile = null;
			mCineFile = CineWFileFactory.Create(cineFilePath, MaxFrameCount, ImgWidth, ImgHeight);
			if (mCineFile.Result)
			{
				ImageQueue = new MultiIrayImageBuffer(4);
				Thread thread = new Thread(AsyncWriteThread);
				thread.IsBackground = true;
				Thread thread2 = thread;
				thread2.Start();
			}
			return mCineFile.Result;
		}

		private void AsyncWriteThread()
		{
			while (true)
			{
				ImageQueue.ApplyBuffer();
				if (bFileClosed)
				{
					break;
				}
				IRayImageData image = ImageQueue.Peek();
				if (image != null)
				{
					AsyncWrite(ref image);
					ImageQueue.Pop();
				}
			}
			ImageQueue = null;
		}

		private void AsyncWrite(ref IRayImageData image)
		{
			if (mCineFile != null && mCineFile.Result)
			{
				if (MaxFrameCount == mIndex)
				{
					mIndex = 0;
				}
				if (mImgParaQueue.Count == MaxFrameCount)
				{
					mImgParaQueue.RemoveAt(0);
				}
				if (image != null && image.ImgData != null)
				{
					try
					{
						mCineFile.WriteData(image.ImgData, mIndex);
						IRayVariantMapItem[] array = (mImgParaQueue.Count >= MaxFrameCount) ? mImgParaQueue.ElementAt(MaxFrameCount - 1) : new IRayVariantMapItem[image.Params.Length];
						Array.Copy(image.Params, array, array.Length);
						mImgParaQueue.Add(array);
						mIndex++;
					}
					catch (Exception ex)
					{
						Log.Instance().Write("CineBuffer AsyncWrite failed!Err={0}", ex.Message);
						StopWrite();
					}
				}
			}
		}

		public void WriteData(ref IRayImageData image)
		{
			try
			{
				if (mCineFile != null && mCineFile.Result && ImageQueue.Push(image))
				{
					ImageQueue.ReleaseBuffer();
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("CineBuffer WriteData failed!Err={0}", ex.Message);
			}
		}

		public void StopWrite()
		{
			bFileClosed = true;
			if (ImageQueue != null)
			{
				try
				{
					ImageQueue.ReleaseBuffer();
				}
				catch
				{
				}
			}
			if (mCineFile != null && mCineFile.Result)
			{
				mCineFile.Close();
			}
		}

		public IRayVariantMapItem[] ReadParasByIndex(int index)
		{
			if (index < 1 || index > mImgParaQueue.Count)
			{
				return null;
			}
			return mImgParaQueue[index - 1];
		}

		public void StartRead(string cineFilePath)
		{
			if (mCineFile.Result)
			{
				try
				{
					mCineFile = new CineRFile(cineFilePath, FrameSize);
				}
				catch (Exception ex)
				{
					mCineFile = null;
					Log.Instance().Write("Read cine-buffer failed: " + ex.Message);
				}
			}
		}

		public void ReadData(ref IRayImageData image, int index)
		{
			if (mCineFile != null && mCineFile.Result && image != null && image.ImgData != null)
			{
				mCineFile.ReadData(image.ImgData, index);
				if (ImgParas.Count != 0)
				{
					image.Params = new IRayVariantMapItem[ImgParas[index].Length];
					Array.Copy(ImgParas[index], image.Params, ImgParas[index].Length);
				}
			}
		}

		public void StopRead()
		{
			if (mCineFile != null)
			{
				mCineFile.Close();
			}
		}
	}
}
