using iDetector;
using System;
using System.IO;

namespace Cinema
{
	public class DCMCinePlayer : IPlayer
	{
		private int width;

		private int height;

		private string filePath;

		public int InitParas(string filePath, int width, int height)
		{
			if (!File.Exists(filePath))
			{
				return 0;
			}
			this.filePath = filePath;
			DicomInfo dicomInfo = default(DicomInfo);
			try
			{
				if (IDicom.DCM_LoadDicomFileInfo(filePath, ref dicomInfo) == 0)
				{
					this.width = dicomInfo.nWidth;
					this.height = dicomInfo.nHeight;
					return 1;
				}
				return 0;
			}
			catch (Exception ex)
			{
				Log.Instance().Write("load dicom file failed! {0}", ex.ToString());
				return 0;
			}
		}

		public void Close()
		{
		}

		public int ReadImage(ref IRayImageData image)
		{
			return 0;
		}

		public int ReadNextImage(ref IRayImageData image)
		{
			if (image.ImgData != null)
			{
				image.ImgData = null;
			}
			image = new IRayImageData(width, height);
			IDicom.DCM_GetDicomImage(filePath, image.ImgData, width, height);
			return 0;
		}

		public int ReadPrevImage(ref IRayImageData image)
		{
			return 0;
		}

		public void Reset()
		{
		}
	}
}
