using System;

namespace Cinema
{
	public class SaverFactory
	{
		public static ISave Create(string filePath)
		{
			string text = filePath.Substring(filePath.LastIndexOf(".") + 1, filePath.Length - filePath.LastIndexOf(".") - 1);
			string typeName = "Cinema." + text.ToUpper() + "Save";
			Type type = Type.GetType(typeName);
			if (null == type)
			{
				return null;
			}
			return (ISave)type.Assembly.CreateInstance(typeName);
		}
	}
}
