using iDetector;

namespace Cinema
{
	public class CineWFileFactory
	{
		public static CineFile Create(string filePath, int frames, int width, int height)
		{
			if (StoreStrategy.MemoryMapFlag)
			{
				return new CineWMapFile(filePath, frames, width, height);
			}
			return new CineWFile(filePath, frames, width, height);
		}
	}
}
