using iDetector;
using System;
using System.IO;

namespace Cinema
{
	public class RAWSave : ISave
	{
		private FileStream mSaveFile;

		public ulong GetMaxFileSize()
		{
			return 9223372036854775807uL;
		}

		public uint ImageHeaderSize()
		{
			return 0u;
		}

		public bool StartSave(string Filepath)
		{
			try
			{
				mSaveFile = new FileStream(Filepath, FileMode.Create, FileAccess.Write);
				return true;
			}
			catch (Exception ex)
			{
				Log.Instance().Write("RAWSave-StartSave failed: " + ex.Message);
				return false;
			}
		}

		public void StopSave()
		{
			if (mSaveFile != null)
			{
				mSaveFile.Flush();
				mSaveFile.Close();
			}
		}

		public void DoSave(ref IRayImageData Image)
		{
			if (mSaveFile != null)
			{
				mSaveFile.Write(Image.ImgData, 0, Image.ImgData.Length);
			}
		}
	}
}
