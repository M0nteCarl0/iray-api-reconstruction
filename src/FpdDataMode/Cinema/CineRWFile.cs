using iDetector;
using System;
using System.IO;

namespace Cinema
{
	public class CineRWFile : CineFile
	{
		public CineRWFile(string filePath, int frames, int frameSize)
			: base(frames, frameSize)
		{
			try
			{
				mFile = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);
				base.Result = true;
			}
			catch (Exception ex)
			{
				mFile = null;
				base.Result = false;
				Log.Instance().Write(string.Format("Create file:{0} failed！err:{1}", filePath, ex.Message));
			}
			if (frames == CineFile.InfiniteFrameNumber)
			{
				mFirstFrameIndex = 0;
			}
		}

		public void Open(string filePath)
		{
			if (mFile == null)
			{
				try
				{
					mFile = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
					base.Result = true;
				}
				catch (Exception ex)
				{
					mFile = null;
					base.Result = false;
					Log.Instance().Write(string.Format("OpenOrCreate file:{0} failed！ err={1}", filePath, ex.Message));
				}
			}
		}

		public override void Close()
		{
			StopWrite();
		}
	}
}
