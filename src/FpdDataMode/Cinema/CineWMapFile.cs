using iDetector;
using System;
using System.IO;
using System.IO.MemoryMappedFiles;

namespace Cinema
{
	public class CineWMapFile : CineFile
	{
		private string mFilePath;

		private MemoryMappedFile memoryFile;

		private MemoryMappedViewStream viewStream;

		private bool bHasCircled;

		public CineWMapFile(string filePath, int frames, int width, int height)
			: base(frames, width, height)
		{
			mFilePath = filePath;
			try
			{
				long num = base.FrameSize * frames;
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}
				memoryFile = MemoryMappedFile.CreateFromFile(filePath, FileMode.OpenOrCreate, "tmpFile" + filePath.GetHashCode().ToString(), num);
				viewStream = memoryFile.CreateViewStream(0L, num);
				base.Result = true;
			}
			catch (Exception)
			{
				if (memoryFile != null)
				{
					memoryFile.Dispose();
				}
				Log.Instance().Write(string.Format("Create[CineWFileEx] file:{0} failed！", filePath));
				mFile = null;
				base.Result = false;
			}
		}

		public override void WriteData(byte[] data, int frameIndex)
		{
			if (viewStream.Position != base.FrameSize * frameIndex)
			{
				viewStream.Seek(base.FrameSize * frameIndex, SeekOrigin.Begin);
				bHasCircled = true;
			}
			viewStream.Write(data, 0, data.Length);
			mLastWriteIndex = frameIndex;
		}

		public override void Close()
		{
			if (viewStream != null)
			{
				long position = viewStream.Position;
				viewStream.Close();
				memoryFile.Dispose();
				viewStream = null;
				mFile = new FileStream(mFilePath, FileMode.Open, FileAccess.Write);
				if (!bHasCircled)
				{
					mFile.SetLength(position);
				}
				StopWrite();
			}
		}
	}
}
