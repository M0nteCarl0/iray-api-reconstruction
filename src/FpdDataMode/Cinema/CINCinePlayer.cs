using iDetector;
using System.IO;

namespace Cinema
{
	public class CINCinePlayer : RawBasePlayer, IPlayer
	{
		private bool InitFileStream(string filePath, int frameSize)
		{
			bool result = false;
			try
			{
				CineRFile cineRFile = new CineRFile(filePath, frameSize);
				mFileHeaderLength = CineRFile.HeaderLength;
				mFirstOffset = cineRFile.StartOffset;
				mStartIndex = cineRFile.FirstFrameIndex;
				mFrameNumber = cineRFile.MaxFrameCount;
				result = cineRFile.Result;
				cineRFile.Close();
				cineRFile = null;
				return result;
			}
			catch
			{
				return result;
			}
		}

		public int InitParas(string filePath, int width, int height)
		{
			if (!File.Exists(filePath) || width * height == 0)
			{
				return 0;
			}
			mFrameSize = width * height * 2;
			if (!InitFileStream(filePath, mFrameSize))
			{
				return 0;
			}
			mFile = new FileStream(filePath, FileMode.Open, FileAccess.Read);
			if (mFile == null)
			{
				return 0;
			}
			mFile.Seek(mFirstOffset, SeekOrigin.Begin);
			mCurIndex = mStartIndex;
			mCurIndex--;
			return mFrameNumber;
		}

		public void Close()
		{
			CloseFile();
		}

		public int ReadNextImage(ref IRayImageData image)
		{
			return NextImage(ref image);
		}

		public int ReadPrevImage(ref IRayImageData image)
		{
			return PrevImage(ref image);
		}

		public int ReadImage(ref IRayImageData image)
		{
			return 0;
		}
	}
}
