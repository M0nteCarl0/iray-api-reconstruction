using iDetector;
using System.IO;

namespace Cinema
{
	public class CineWFile : CineFile
	{
		private string mFilePath;

		private FileStream[] FileArray;

		private int mEachFileFrames;

		private readonly int FileMaxLength = 1073741824;

		public CineWFile(string filePath, int frames, int width, int height)
			: base(frames, width, height)
		{
			mFilePath = filePath;
			try
			{
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}
				int num = (int)((base.FrameSize * frames + FileMaxLength) / FileMaxLength);
				FileArray = new FileStream[num];
				mEachFileFrames = FileMaxLength / (int)base.FrameSize;
				for (int i = 0; i < num; i++)
				{
					File.Delete(mFilePath + i.ToString());
					FileArray[i] = new FileStream(filePath + i.ToString(), FileMode.Create, FileAccess.Write, FileShare.Read);
				}
				mFile = FileArray[0];
				base.Result = true;
			}
			catch
			{
				int num2 = 0;
				while (FileArray != null && num2 < FileArray.Length)
				{
					if (FileArray[num2] != null)
					{
						FileArray[num2].Close();
					}
					num2++;
				}
				Log.Instance().Write(string.Format("Create[CineWFile] file:{0} failed！", filePath));
				mFile = null;
				base.Result = false;
			}
		}

		public override void WriteData(byte[] data, int frameIndex)
		{
			mFile = FileArray[frameIndex / mEachFileFrames];
			base.WriteData(data, frameIndex % mEachFileFrames);
		}

		public override void Close()
		{
			if (FileArray != null)
			{
				MergeImage();
				FileArray = null;
				if (File.Exists(mFilePath))
				{
					mFile = new FileStream(mFilePath, FileMode.Open, FileAccess.Write);
					StopWrite();
				}
				else
				{
					Log.Instance().Write("File {0} not exist!", mFilePath);
				}
			}
		}

		private void MergeImage()
		{
			string text = "";
			int num = 0;
			while (FileArray != null && num < FileArray.Length)
			{
				text += string.Format("\"{0}\" +", mFilePath + num.ToString());
				FileArray[num].Flush();
				FileArray[num].Dispose();
				FileArray[num].Close();
				num++;
			}
			string cmd = string.Format("copy /b /y {0} \"{1}\"", text.Remove(text.LastIndexOf("+")), mFilePath);
			Utility.RunCmd(cmd);
			for (int i = 0; i < FileArray.Length; i++)
			{
				File.Delete(mFilePath + i.ToString());
			}
		}
	}
}
