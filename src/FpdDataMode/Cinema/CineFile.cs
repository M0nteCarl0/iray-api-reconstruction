using System;
using System.IO;

namespace Cinema
{
	public abstract class CineFile
	{
		public static int InfiniteFrameNumber = int.MaxValue;

		protected FileStream mFile;

		protected int mMaxFrameCount;

		protected int mLastWriteIndex;

		protected int mFirstFrameIndex;

		public bool Result
		{
			get;
			set;
		}

		public long FrameSize
		{
			get;
			set;
		}

		public CineFile(int frames, int width, int height)
		{
			mMaxFrameCount = frames;
			FrameSize = width * height * 2;
			mLastWriteIndex = 0;
		}

		public CineFile(int frames, int frameSize)
		{
			mMaxFrameCount = frames;
			FrameSize = frameSize;
		}

		public CineFile(int frameSize)
		{
			FrameSize = frameSize;
		}

		protected void StopWrite()
		{
			if (mFile != null)
			{
				long value = 0L;
				if (mFile.Length == mMaxFrameCount * FrameSize)
				{
					value = (mLastWriteIndex + 1) % mMaxFrameCount * FrameSize;
				}
				mFile.Seek(0L, SeekOrigin.End);
				byte[] bytes = BitConverter.GetBytes(value);
				mFile.Write(bytes, 0, bytes.Length);
				mFile.Flush();
				mFile.Close();
				mFile = null;
			}
		}

		protected void StopRead()
		{
			if (mFile != null)
			{
				mFile.Close();
				mFile = null;
			}
		}

		public virtual void Close()
		{
			if (mFile != null)
			{
				mFile.Close();
				mFile = null;
			}
		}

		public virtual void WriteData(byte[] data, int frameIndex)
		{
			if (mFile != null)
			{
				mLastWriteIndex = frameIndex;
				if (mFile.Position != FrameSize * frameIndex)
				{
					mFile.Seek(FrameSize * frameIndex, SeekOrigin.Begin);
				}
				mFile.Write(data, 0, data.Length);
			}
		}

		public virtual void ReadData(byte[] data, int index)
		{
			if (mFile != null)
			{
				int num = (index + mFirstFrameIndex) % mMaxFrameCount;
				if (data != null)
				{
					mFile.Seek(FrameSize * num, SeekOrigin.Begin);
					mFile.Read(data, 0, data.Length);
				}
			}
		}
	}
}
