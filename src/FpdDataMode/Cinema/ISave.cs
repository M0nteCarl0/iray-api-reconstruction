using iDetector;

namespace Cinema
{
	public interface ISave
	{
		bool StartSave(string Filepath);

		void StopSave();

		void DoSave(ref IRayImageData image);

		ulong GetMaxFileSize();

		uint ImageHeaderSize();
	}
}
