using iDetector;
using System.IO;

namespace Cinema
{
	public class RawBasePlayer
	{
		protected FileStream mFile;

		protected int mFrameSize;

		protected int mFrameNumber;

		protected int mStartIndex;

		protected int mCurIndex;

		protected long mFirstOffset;

		protected int mFileHeaderLength;

		public int NextImage(ref IRayImageData image)
		{
			if (mFile == null)
			{
				return -1;
			}
			if (image.ImgData != null && mFrameSize == image.ImgData.Length)
			{
				mCurIndex++;
				if (mCurIndex == mFrameNumber)
				{
					mFile.Seek(0L, SeekOrigin.Begin);
					mCurIndex = 0;
				}
				mFile.Read(image.ImgData, 0, image.ImgData.Length);
				return (mCurIndex - mStartIndex + mFrameNumber) % mFrameNumber + 1;
			}
			return -1;
		}

		public int PrevImage(ref IRayImageData image)
		{
			if (mFile == null)
			{
				return -1;
			}
			if (image.ImgData != null && mFrameSize == image.ImgData.Length)
			{
				mCurIndex--;
				if (mCurIndex < 0)
				{
					mCurIndex = mFrameNumber - 1;
					mFile.Seek(-1 * mFrameSize - mFileHeaderLength, SeekOrigin.End);
				}
				else
				{
					mFile.Seek(-2 * mFrameSize, SeekOrigin.Current);
				}
				mFile.Read(image.ImgData, 0, image.ImgData.Length);
				return (mCurIndex - mStartIndex + mFrameNumber) % mFrameNumber + 1;
			}
			return -1;
		}

		public void CloseFile()
		{
			if (mFile != null)
			{
				mFile.Flush();
				mFile.Close();
			}
			mFile = null;
		}

		public void Reset()
		{
			if (mFile != null)
			{
				mCurIndex = mStartIndex - 1;
				mFile.Seek(mFirstOffset, SeekOrigin.Begin);
			}
		}
	}
}
