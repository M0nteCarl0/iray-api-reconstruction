using iDetector;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace Cinema
{
	public class TIFCinePlayer : IPlayer
	{
		private IntPtr mTiffMng;

		private IntPtr mImgPtr;

		private int mFrameNumber;

		private int mCurIndex;

		private bool mbOpened;

		public int InitParas(string filePath, int width, int height)
		{
			if (!File.Exists(filePath))
			{
				return 0;
			}
			try
			{
				mTiffMng = TiffParser.CreateImageMng();
				mbOpened = TiffParser.TiffOpen(mTiffMng, Marshal.StringToCoTaskMemAnsi(filePath), 0);
				if (!mbOpened)
				{
					return 0;
				}
				mImgPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IRayImage)));
				mFrameNumber = TiffParser.TiffGetImageCount(mTiffMng);
				mCurIndex = -1;
				return mFrameNumber;
			}
			catch (Exception ex)
			{
				Log.Instance().Write("load tif file failed! {0}", ex.ToString());
				return 0;
			}
		}

		public void Close()
		{
			if (mbOpened)
			{
				mbOpened = false;
				Marshal.FreeHGlobal(mImgPtr);
				TiffParser.TiffClose(mTiffMng);
				TiffParser.ReleaseImageMng(mTiffMng);
			}
		}

		public int ReadNextImage(ref IRayImageData image)
		{
			if (!mbOpened)
			{
				return -1;
			}
			mCurIndex++;
			if (mCurIndex == mFrameNumber)
			{
				mCurIndex = 0;
			}
			TiffParser.TiffReadImage(mTiffMng, mImgPtr, mCurIndex);
			IRayImage image2 = (IRayImage)Marshal.PtrToStructure(mImgPtr, typeof(IRayImage));
			image.ConvertIRayImage(ref image2);
			return mCurIndex + 1;
		}

		public int ReadPrevImage(ref IRayImageData image)
		{
			if (!mbOpened)
			{
				return -1;
			}
			mCurIndex--;
			if (mCurIndex < 0)
			{
				mCurIndex = mFrameNumber - 1;
			}
			TiffParser.TiffReadImage(mTiffMng, mImgPtr, mCurIndex);
			IRayImage image2 = (IRayImage)Marshal.PtrToStructure(mImgPtr, typeof(IRayImage));
			image.ConvertIRayImage(ref image2);
			return mCurIndex + 1;
		}

		private void ReadCurImage(ref IRayImageData image)
		{
			if (mbOpened)
			{
				TiffParser.TiffReadImage(mTiffMng, mImgPtr, mCurIndex);
				IRayImage image2 = (IRayImage)Marshal.PtrToStructure(mImgPtr, typeof(IRayImage));
				image.ConvertIRayImage(ref image2);
			}
		}

		public int ReadImage(ref IRayImageData image)
		{
			return 0;
		}

		public void Reset()
		{
			mCurIndex = -1;
		}
	}
}
