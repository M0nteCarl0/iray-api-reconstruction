using iDetector;
using System.Threading;

namespace Cinema
{
	internal class MultiIrayImageBuffer
	{
		private IRayImageData[] mBuffer;

		private int m_nForeIndex;

		private int m_nBackIndex;

		private int mDimension;

		private int mQueueLen;

		private Semaphore semaphore;

		public MultiIrayImageBuffer(int dimension)
		{
			mDimension = dimension;
			mBuffer = new IRayImageData[dimension];
			for (int i = 0; i < dimension; i++)
			{
				mBuffer[i] = new IRayImageData();
			}
			semaphore = new Semaphore(0, dimension);
			m_nForeIndex = 0;
			m_nBackIndex = 0;
			mQueueLen = 0;
		}

		public void ApplyBuffer()
		{
			semaphore.WaitOne();
		}

		public void ReleaseBuffer()
		{
			semaphore.Release();
		}

		public bool Push(IRayImageData data)
		{
			if (IsFull())
			{
				return false;
			}
			IRayImageData rayImageData = mBuffer[m_nForeIndex];
			m_nForeIndex = ((m_nForeIndex + 1) & ~mDimension);
			rayImageData.Clone(ref data);
			mQueueLen++;
			return true;
		}

		public IRayImageData Peek()
		{
			if (IsEmpty())
			{
				return null;
			}
			IRayImageData result = mBuffer[m_nBackIndex];
			m_nBackIndex = ((m_nBackIndex + 1) & ~mDimension);
			return result;
		}

		public void Pop()
		{
			mQueueLen--;
		}

		private bool IsEmpty()
		{
			if (mQueueLen == 0)
			{
				return true;
			}
			return false;
		}

		private bool IsFull()
		{
			if (mQueueLen == mDimension)
			{
				return true;
			}
			return false;
		}
	}
}
