using System.Threading;

namespace Cinema
{
	public class MultiBufferT<T> where T : new()
	{
		private T[][] mBuffer;

		private int m_nForeIndex;

		private int m_nBackIndex;

		private int mDimension;

		private Semaphore semaphore;

		public MultiBufferT(int dimension, int bufferLength)
		{
			mDimension = dimension;
			mBuffer = new T[dimension][];
			for (int i = 0; i < dimension; i++)
			{
				mBuffer[i] = new T[bufferLength];
			}
			semaphore = new Semaphore(0, dimension);
			m_nForeIndex = 0;
			m_nBackIndex = 0;
		}

		public void ApplyBuffer()
		{
			semaphore.WaitOne();
		}

		public void ReleaseBuffer()
		{
			semaphore.Release();
		}

		public T[] FrontBuffer()
		{
			T[] result = mBuffer[m_nForeIndex];
			m_nForeIndex = ((m_nForeIndex + 1) & ~mDimension);
			return result;
		}

		public T[] BackBuffer()
		{
			T[] result = mBuffer[m_nBackIndex];
			m_nBackIndex = ((m_nBackIndex + 1) & ~mDimension);
			return result;
		}
	}
}
