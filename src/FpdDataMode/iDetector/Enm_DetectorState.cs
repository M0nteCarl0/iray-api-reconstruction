namespace iDetector
{
	public enum Enm_DetectorState
	{
		Enm_State_Unknown,
		Enm_State_Ready,
		Enm_State_Busy,
		Enm_State_Sleeping
	}
}
