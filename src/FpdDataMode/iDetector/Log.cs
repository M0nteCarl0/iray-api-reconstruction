#define TRACE
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace iDetector
{
	public class Log
	{
		private const string DateFormat = "yyyy-MM-dd HH:mm:ss.FFF ";

		private static Log instance;

		public static string logPath;

		private Log()
		{
			logPath = Utility.GetCurrentPath();
			Trace.AutoFlush = true;
			string text = DateTime.Now.ToString("yyyyMMdd") + " iDetector.log";
			string text2 = logPath + "\\";
			Utility.CreateDirectory(text2);
			try
			{
				DeleteLog(text2, text);
				Trace.Listeners.Add(new TextWriterTraceListener(text2 + text));
			}
			catch
			{
				text = DateTime.Now.ToString("yyyyMMdd") + " iDetector1.log";
				Trace.Listeners.Add(new TextWriterTraceListener(text2 + text));
			}
		}

		private void DeleteLog(string dirPath, string curLogName)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(dirPath);
			List<FileInfo> list = directoryInfo.GetFiles("20*iDetector.log").ToList();
			if (list.Count >= 2)
			{
				int num = list.Count - 2;
				foreach (FileInfo item in list)
				{
					if (num == 0)
					{
						break;
					}
					if (item.Name.CompareTo(curLogName) != 0)
					{
						File.Delete(item.FullName);
						num--;
					}
				}
			}
		}

		public static Log Instance()
		{
			if (instance == null)
			{
				instance = new Log();
			}
			return instance;
		}

		public void Write(string message)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + message);
		}

		public void Write(int detectorID, string message)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format("Detector{0} ", detectorID) + message);
		}

		public void Write(string message, object arg)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format(message, arg));
		}

		public void Write(int detectorID, string message, object arg)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format("Detector{0} ", detectorID) + string.Format(message, arg));
		}

		public void Write(string message, object arg0, object arg1)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format(message, arg0, arg1));
		}

		public void Write(int detectorID, string message, object arg0, object arg1)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format("Detector{0} ", detectorID) + string.Format(message, arg0, arg1));
		}

		public void Write(string message, object arg0, object arg1, object arg2)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format(message, arg0, arg1, arg2));
		}

		public void Write(int detectorID, string message, object arg0, object arg1, object arg2)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format("Detector{0} ", detectorID) + string.Format(message, arg0, arg1, arg2));
		}

		public void Write(string message, params object[] args)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format(message, args));
		}

		public void Write(int detectorID, string message, params object[] args)
		{
			Trace.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.FFF ") + string.Format("Detector{0} ", detectorID) + string.Format(message, args));
		}
	}
}
