using System.Runtime.InteropServices;

namespace iDetector
{
	public struct DicomHeaderInfo
	{
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		public char[] strDetectorID;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		public char[] strSensitivity;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		public char[] strDoseInmGy;

		public int nRawImageOffset;
	}
}
