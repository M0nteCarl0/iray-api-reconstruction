using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct CmdParamInfo
	{
		public IRAY_VAR_TYPE nDataType;

		public int bIsDataBlock;

		public int bIsEnum;

		public float fMinValue;

		public float fMaxValue;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strName;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string szEnumTypeName;

		public PARAM_VALIDATOR eValidator;
	}
}
