namespace iDetector
{
	public enum CorrectOptionMask
	{
		Offset = 196611,
		Gain = 262148,
		Defect = 1048592
	}
}
