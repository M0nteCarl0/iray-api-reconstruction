namespace iDetector
{
	public enum Enm_Wifi_Channel
	{
		Enm_Wifi_Chnl_1 = 1,
		Enm_Wifi_Chnl_2 = 2,
		Enm_Wifi_Chnl_3 = 3,
		Enm_Wifi_Chnl_4 = 4,
		Enm_Wifi_Chnl_5 = 5,
		Enm_Wifi_Chnl_6 = 6,
		Enm_Wifi_Chnl_7 = 7,
		Enm_Wifi_Chnl_8 = 8,
		Enm_Wifi_Chnl_9 = 9,
		Enm_Wifi_Chnl_10 = 10,
		Enm_Wifi_Chnl_11 = 11,
		Enm_Wifi_Chnl_12 = 12,
		Enm_Wifi_Chnl_13 = 13,
		Enm_Wifi_Chnl_36 = 36,
		Enm_Wifi_Chnl_40 = 40,
		Enm_Wifi_Chnl_44 = 44,
		Enm_Wifi_Chnl_48 = 48,
		Enm_Wifi_Chnl_149 = 149,
		Enm_Wifi_Chnl_153 = 153,
		Enm_Wifi_Chnl_157 = 157,
		Enm_Wifi_Chnl_161 = 161,
		Enm_Wifi_Chnl_165 = 165
	}
}
