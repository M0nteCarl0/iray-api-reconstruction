namespace iDetector
{
	public enum Enm_Zoom
	{
		Enm_Zoom_Null,
		Enm_Zoom_1024x640,
		Enm_Zoom_1024x320,
		Enm_Zoom_1024x4,
		Enm_Zoom_1024x2,
		Enm_Zoom_1024x1,
		Enm_Zoom_512x512,
		Enm_Zoom_256x256,
		Enm_Zoom_3072x3072,
		Enm_Zoom_2048x2048,
		Enm_Zoom_1536x1536,
		Enm_Zoom_1024x1024
	}
}
