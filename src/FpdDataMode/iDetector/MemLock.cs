using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class MemLock : IDisposable
	{
		private GCHandle IndicesHandle;

		public IntPtr TargetAddr
		{
			get
			{
				return (IntPtr)IndicesHandle.Target;
			}
		}

		public MemLock(object obj)
		{
			IndicesHandle = GCHandle.Alloc(obj, GCHandleType.Pinned);
		}

		public IntPtr Addr()
		{
			return IndicesHandle.AddrOfPinnedObject();
		}

		public void Dispose()
		{
			IndicesHandle.Free();
		}
	}
}
