using System;

namespace iDetector
{
	public delegate void SdkCallbackEventHandler(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam);
}
