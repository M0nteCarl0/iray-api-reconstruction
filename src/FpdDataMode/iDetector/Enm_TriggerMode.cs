namespace iDetector
{
	public enum Enm_TriggerMode
	{
		Enm_TriggerMode_Outer,
		Enm_TriggerMode_Inner,
		Enm_TriggerMode_Soft,
		Enm_TriggerMode_Prep,
		Enm_TriggerMode_Service,
		Enm_TriggerMode_FreeSync
	}
}
