using System;

namespace iDetector
{
	public class BilinearInterpolation : AImageCompress
	{
		public override void PicZoom(ref TPicRegion Dst, TPicRegion Src)
		{
			if (Dst.width == Src.width && Dst.height == Src.height)
			{
				Dst = Src;
				return;
			}
			double num = (double)Src.width / (double)Dst.width;
			double num2 = (double)Src.height / (double)Dst.height;
			int stride = Dst.stride;
			int stride2 = Src.stride;
			byte[] pdata = Src.pdata;
			byte[] pdata2 = Dst.pdata;
			for (int i = 0; i < Dst.height; i++)
			{
				int num3 = (int)(num2 * (double)i);
				int num4 = Math.Min(num3 + 1, Src.height - 1);
				float num5 = (float)(num2 * (double)i - (double)num3);
				for (int j = 0; j < Dst.width; j++)
				{
					int num6 = (int)(num * (double)j);
					int num7 = Math.Min(num6 + 1, Src.width - j);
					float num8 = (float)(num * (double)j - (double)num6);
					pdata2[i * stride + j] = (byte)((1f - num5) * (1f - num8) * (float)(int)pdata[num3 * stride2 + num6] + (1f - num5) * num8 * (float)(int)pdata[num4 * stride2 + num6] + num5 * (1f - num8) * (float)(int)pdata[num3 * stride2 + num7] + num5 * num8 * (float)(int)pdata[num4 * stride2 + num7]);
				}
			}
		}
	}
}
