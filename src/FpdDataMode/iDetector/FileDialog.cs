using Microsoft.Win32;
using System.Windows;

namespace iDetector
{
	public class FileDialog
	{
		public static string SaveFile()
		{
			string result = null;
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.DefaultExt = ".dcm| .raw | .tif ";
			saveFileDialog.Filter = "dicom file(*.dcm)|*.dcm|raw file(*.raw)|*.raw|tiff file(*.tif)|*.tif";
			if (saveFileDialog.ShowDialog() == true)
			{
				if (!Utility.IsFileCanbeWritten(saveFileDialog.FileName))
				{
					MessageBox.Show("This file had been opened and cannot be written!");
					return null;
				}
				string text = saveFileDialog.FileName.Substring(saveFileDialog.FileName.LastIndexOf(".") + 1, saveFileDialog.FileName.Length - saveFileDialog.FileName.LastIndexOf(".") - 1);
				result = saveFileDialog.FileName;
				if (text.Length == 0 || (!text.ToLower().Equals("raw") && !text.ToLower().Equals("tif") && !text.ToLower().Equals("dcm")))
				{
					result = saveFileDialog.FileName + ".raw";
				}
			}
			return result;
		}
	}
}
