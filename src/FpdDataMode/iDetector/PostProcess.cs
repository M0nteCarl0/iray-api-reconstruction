using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class PostProcess
	{
		public const int LabErr_OK = 0;

		public const int LabErr_Config_Read_Failed = 4001;

		public const int LabErr_UnKnown_Process_Intensity = 4002;

		public const int LabErr_No_Config_File = 4003;

		public const int LabErr_AccessDenied = 4004;

		public const int LabErr_GeneralErr = 4005;

		public const int LabErr_LoadDllFailed = 4006;

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int InitializeDynamicProcessingByPreviousFrames(uint nPixelCnt, float RecursionFactor, ushort nCompositionCnt);

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int DoDynamicProcessingByPreviousFrames(IntPtr OutImg, IntPtr InImg);

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int UninitializeDynamicProcessingByPreviousFrames();

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SetUserCode([MarshalAs(UnmanagedType.LPStr)] string szUserCode);

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int InitializeOfflineCorrection(ref OfflineCorrectionInfo pOfflineCorrInfo, IntPtr ppHandle);

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ApplyOfflineCorrection(IntPtr pHandle, IntPtr pImgData, IntPtr pPostImg, int nFreeSyncExpLine = 0, int nFreeSyncExpStatus = 0);

		[DllImport("SignalProcessing.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int UninitializeOfflineCorrection(IntPtr pHandle);
	}
}
