using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct DetectorProfile
	{
		public int nProdNo;

		public int nSubProdNo;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
		public string strSN;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
		public string strIP;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strnetworkCard;

		public bool bBusy;
	}
}
