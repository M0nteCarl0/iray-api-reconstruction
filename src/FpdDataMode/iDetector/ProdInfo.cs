using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct ProdInfo
	{
		public int nProdNo;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strName;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strDescripion;
	}
}
