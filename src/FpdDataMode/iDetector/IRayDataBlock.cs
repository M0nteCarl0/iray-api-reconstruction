using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayDataBlock
	{
		public uint uBytes;

		public IntPtr pData;
	}
}
