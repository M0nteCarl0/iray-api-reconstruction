using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace iDetector
{
	public class VelueConverter : IValueConverter
	{
		private string[] mColor = new string[4]
		{
			"yellow",
			"green",
			"red",
			"gray"
		};

		private string[] mSdkStateInfo = new string[4]
		{
			"Unknown",
			"Ready",
			"Busy",
			"Sleeping"
		};

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if ((Enm_PropertyName)parameter == Enm_PropertyName.Enm_SDK_STATE)
			{
				if (targetType == typeof(string))
				{
					if ((int)value >= mSdkStateInfo.Length)
					{
						return mSdkStateInfo[0];
					}
					return mSdkStateInfo[(int)value];
				}
				if (targetType == typeof(Brush))
				{
					if ((int)value >= mColor.Length)
					{
						return mColor[0];
					}
					return mColor[(int)value];
				}
				return null;
			}
			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
