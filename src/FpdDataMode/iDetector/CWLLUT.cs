using System;
using System.Threading;

namespace iDetector
{
	public class CWLLUT
	{
		private class ConvertLUT
		{
			public ushort[] srcData;

			public byte[] dstData;

			public int startIndex;

			public int endIndex;

			public static int ThreadFinshedCnt = 0;

			public static int ThreadNumber = 4;

			public ConvertLUT(ushort[] _srcData, byte[] _dstData, int _startIndex, int _endIndex)
			{
				srcData = _srcData;
				dstData = _dstData;
				startIndex = _startIndex;
				endIndex = _endIndex;
			}
		}

		private byte[] m_pResult;

		private int m_fWindowWidth;

		private int m_fWindowCenter;

		private ushort m_nMaxIn;

		private ushort m_nMaxOut;

		private ManualResetEvent ConvertFinshed;

		private object Locker = new object();

		public CWLLUT(ushort nMaxIn, byte nMaxOut, int nWW, int nWC)
		{
			m_fWindowWidth = nWW;
			m_fWindowCenter = nWC;
			m_nMaxIn = nMaxIn;
			m_nMaxOut = nMaxOut;
			float num = m_fWindowCenter - m_fWindowWidth / 2;
			float num2 = m_fWindowCenter + m_fWindowWidth / 2;
			m_pResult = new byte[nMaxIn + 1];
			for (int i = 0; i <= nMaxIn; i++)
			{
				float num3 = (!((float)i >= num2)) ? ((!((float)i < num)) ? (((float)i - num) * 255f / (float)m_fWindowWidth) : 0f) : 255f;
				m_pResult[i] = (byte)num3;
			}
		}

		~CWLLUT()
		{
			m_pResult = null;
			GC.Collect();
		}

		public virtual byte GetLUTResult(ushort nIndex)
		{
			return m_pResult[nIndex];
		}

		public void ConvertProc(object obj)
		{
			ConvertLUT convertLUT = obj as ConvertLUT;
			if (convertLUT != null)
			{
				for (int i = convertLUT.startIndex; i < convertLUT.endIndex; i++)
				{
					convertLUT.dstData[i] = m_pResult[convertLUT.srcData[i]];
				}
				convertLUT = null;
				lock (Locker)
				{
					ConvertLUT.ThreadFinshedCnt++;
				}
				if (ConvertLUT.ThreadFinshedCnt == ConvertLUT.ThreadNumber)
				{
					ConvertFinshed.Set();
				}
			}
		}

		public void GetLUTResult(ushort[] srcData, byte[] dstData)
		{
			if (srcData != null && dstData != null && srcData.Length == dstData.Length)
			{
				ConvertLUT.ThreadFinshedCnt = 0;
				ConvertFinshed = new ManualResetEvent(false);
				int num = srcData.Length / ConvertLUT.ThreadNumber;
				for (int i = 0; i < ConvertLUT.ThreadNumber; i++)
				{
					ThreadPool.QueueUserWorkItem(ConvertProc, new ConvertLUT(srcData, dstData, i * num, (i + 1) * num));
				}
				if (!ConvertFinshed.WaitOne(500))
				{
					Log.Instance().Write("GetLUTResult timeout");
				}
			}
		}

		public void GetCurrentWL(ref int nWW, ref int nWC)
		{
			nWW = m_fWindowWidth;
			nWC = m_fWindowCenter;
		}

		public bool LUTIsValid()
		{
			return true;
		}

		public bool RebuildLUT(ushort nMaxIn, byte nMaxOut, int nWW, int nWC)
		{
			int num = 0;
			int num2 = 0;
			bool flag = false;
			if (nMaxIn != m_nMaxIn)
			{
				m_pResult = new byte[nMaxIn + 1];
				flag = true;
			}
			if (m_fWindowCenter == nWC && m_fWindowWidth == nWW && !flag)
			{
				return false;
			}
			m_nMaxIn = nMaxIn;
			m_nMaxOut = nMaxOut;
			m_fWindowWidth = nWW;
			m_fWindowCenter = nWC;
			int num3 = 16711680 / m_fWindowWidth;
			num = Math.Max(0, m_fWindowCenter - m_fWindowWidth / 2);
			num2 = Math.Min(m_fWindowCenter + m_fWindowWidth / 2, nMaxIn);
			int num4 = 32768;
			Array.Clear(m_pResult, 0, num);
			for (int i = num2; i <= nMaxIn; i++)
			{
				m_pResult[i] = byte.MaxValue;
			}
			for (int j = num; j < num2; j++)
			{
				m_pResult[j] = Convert.ToByte((j - num) * num3 + num4 >> 16);
			}
			return true;
		}
	}
}
