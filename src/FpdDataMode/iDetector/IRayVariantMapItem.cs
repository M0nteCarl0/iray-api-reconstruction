using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayVariantMapItem
	{
		public int nMapKey;

		public IRayVariant varMapVal;
	}
}
