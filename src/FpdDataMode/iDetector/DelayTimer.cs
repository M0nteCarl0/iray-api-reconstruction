using System;

namespace iDetector
{
	public class DelayTimer : MMTimer
	{
		public DelayTimer(int interval, Action timeProc)
			: base(interval, timeProc)
		{
		}

		protected override void OnTimer(object sender, EventArgs e)
		{
			Stop();
			OnProcess();
		}
	}
}
