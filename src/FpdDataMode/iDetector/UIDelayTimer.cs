using System;

namespace iDetector
{
	public class UIDelayTimer : UITimer
	{
		public UIDelayTimer(int interval, Action timeProc)
			: base(interval, timeProc)
		{
		}

		protected override void OnTimer(object sender, EventArgs e)
		{
			Stop();
			ProcessHandler();
		}
	}
}
