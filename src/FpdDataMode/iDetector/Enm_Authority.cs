namespace iDetector
{
	public enum Enm_Authority
	{
		Enm_Authority_Basic = 0,
		Enm_Authority_RawImage = 1,
		Enm_Authority_UserDetConfig = 2,
		Enm_Authority_Test = 0x2000,
		Enm_Authority_FactoryConfig = 0x4000,
		Enm_Authority_WriteSN = 0x8000,
		Enm_Authority_EMS = 0x10000,
		Enm_Authority_RemoveGrid = 0x20000,
		Enm_Authority_VirtualGrid = 0x40000,
		Enm_Authority_DynaNoiseReduct = 0x80000,
		Enm_Authority_OEM = 0x7FFF,
		Enm_Authority_Full = int.MaxValue
	}
}
