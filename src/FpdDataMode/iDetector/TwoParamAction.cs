namespace iDetector
{
	public delegate void TwoParamAction(object para1, object para2);
	public delegate void TwoParamAction<in T1, in T2>(T1 obj, T2 para);
}
