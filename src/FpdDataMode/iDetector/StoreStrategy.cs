using System;

namespace iDetector
{
	public class StoreStrategy
	{
		public static bool MemoryMapFlag = false;

		private static uint LeftSpace = 524288000u;

		public static int CalcMaxFrames(uint eachFrameSize, uint frames)
		{
			uint result = frames;
			if (Environment.Is64BitProcess)
			{
				ulong num = (ulong)((long)frames * (long)eachFrameSize + LeftSpace);
				ulong availPhysMemory = Utility.GetAvailPhysMemory();
				if (num < availPhysMemory)
				{
					MemoryMapFlag = true;
				}
				else if (eachFrameSize / 1024u / 1024u * 30 < 120)
				{
					MemoryMapFlag = false;
				}
				else
				{
					MemoryMapFlag = false;
					result = (uint)((availPhysMemory - LeftSpace) / eachFrameSize);
				}
			}
			else
			{
				MemoryMapFlag = false;
			}
			return (int)result;
		}
	}
}
