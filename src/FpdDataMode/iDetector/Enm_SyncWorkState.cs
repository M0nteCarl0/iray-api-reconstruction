namespace iDetector
{
	public enum Enm_SyncWorkState
	{
		Enm_SyncWorkState_Unknown,
		Enm_SyncWorkState_Ready,
		Enm_SyncWorkState_Busy
	}
}
