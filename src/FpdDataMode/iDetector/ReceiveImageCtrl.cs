using Cinema;
using System;

namespace iDetector
{
	public class ReceiveImageCtrl
	{
		private const int DispatchTimeout = 20;

		private const int AutoAdustWLWWTimeout = 400;

		private UITimer ReceiveImageTimer;

		private MMTimer AutoAdjustWLWWTimer;

		private bool EnableBuferCine;

		private bool AutoSetWinWidLevelFlag;

		private CineBuffer CineBuffer;

		private IRayImageData lastImage;

		private object Locker = new object();

		private bool EnableUpdatePaintImage;

		public IRayImageData LastImage
		{
			get
			{
				return lastImage;
			}
		}

		public event Action<IRayImageData> PanitImage;

		public event Action AdjustWinLevelAndWidth;

		public ReceiveImageCtrl()
		{
			ReceiveImageTimer = new UITimer(20, DispatchImage);
			AutoAdjustWLWWTimer = new MMTimer(400, OnAutoAdjustWLWWTimer);
			EnableUpdatePaintImage = false;
		}

		private void OnAutoAdjustWLWWTimer()
		{
			AutoSetWinWidLevelFlag = true;
		}

		public void StartReceive()
		{
			ReceiveImageTimer.Start();
			AutoAdjustWLWWTimer.Start();
		}

		public void StopReceive()
		{
			ReceiveImageTimer.Stop();
			AutoAdjustWLWWTimer.Stop();
		}

		public void Close()
		{
			StopReceive();
			StopBufferCine();
		}

		public void StartBufferCine(string filePath, int maxFrames, int imgWidth, int imgHeight)
		{
			StopBufferCine();
			CineBuffer = new CineBuffer(maxFrames, imgWidth, imgHeight);
			CineBuffer.StartWrite(filePath);
			EnableBuferCine = true;
		}

		public void StopBufferCine()
		{
			EnableBuferCine = false;
			if (CineBuffer != null)
			{
				CineBuffer.StopWrite();
			}
		}

		private void DispatchImage()
		{
			EnableUpdatePaintImage = true;
		}

		public void ReceiveImage(IRayImageData image)
		{
			lock (Locker)
			{
				StoreCineData(image);
				if (EnableUpdatePaintImage)
				{
					DisplayImageDelgegate(image);
					EnableUpdatePaintImage = false;
				}
				lastImage = image;
			}
		}

		public void DisplayImageDelgegate(IRayImageData image)
		{
			if (image != null && image.ImgData != null)
			{
				this.PanitImage(image);
				if (AutoSetWinWidLevelFlag)
				{
					AutoSetWinWidLevelFlag = false;
					this.AdjustWinLevelAndWidth();
				}
			}
		}

		private void StoreCineData(IRayImageData image)
		{
			if (EnableBuferCine)
			{
				CineBuffer.WriteData(ref image);
			}
		}
	}
}
