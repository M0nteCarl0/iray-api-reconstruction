namespace iDetector
{
	public interface iThumbnailMedium
	{
		bool Create(string fileName, int totalSize);

		void Close();

		int Write(long position, byte[] array, int offset, int count);

		int Read(long position, byte[] array, int offset, int count);
	}
}
