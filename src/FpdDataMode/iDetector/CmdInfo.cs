using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct CmdInfo
	{
		public int nCmdID;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strName;
	}
}
