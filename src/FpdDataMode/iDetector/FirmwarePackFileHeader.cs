using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct FirmwarePackFileHeader
	{
		public byte ucVerByte0;

		public byte ucVerByte1;

		public byte ucVerByte2;

		public byte ucVerByte3;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 512)]
		public string strDescription;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public int[] arrProducts;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 68)]
		public byte[] ex;
	}
}
