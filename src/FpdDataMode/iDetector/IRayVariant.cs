using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayVariant
	{
		public IRAY_VAR_TYPE vt;

		public IRayVariantVal val;
	}
}
