using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayVariantMap
	{
		public int nItemCount;

		public IntPtr pItems;
	}
}
