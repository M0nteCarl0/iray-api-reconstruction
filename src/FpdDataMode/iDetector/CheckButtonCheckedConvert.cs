using System;
using System.Globalization;
using System.Windows.Data;

namespace iDetector
{
	public class CheckButtonCheckedConvert : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values == null)
			{
				return false;
			}
			bool flag = false;
			for (int i = 0; i < values.Length; i++)
			{
				bool flag2 = (bool)values[i];
				flag |= flag2;
			}
			return flag;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
