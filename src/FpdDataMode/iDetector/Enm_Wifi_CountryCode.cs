namespace iDetector
{
	public enum Enm_Wifi_CountryCode
	{
		Enm_Wifi_Country_CN,
		Enm_Wifi_Country_DE,
		Enm_Wifi_Country_FR,
		Enm_Wifi_Country_GB,
		Enm_Wifi_Country_HK,
		Enm_Wifi_Country_IT,
		Enm_Wifi_Country_KR,
		Enm_Wifi_Country_NL,
		Enm_Wifi_Country_RU,
		Enm_Wifi_Country_US
	}
}
