namespace iDetector
{
	public delegate void OneParamAction(object obj);
	public delegate void OneParamAction<in T>(T obj);
}
