namespace iDetector
{
	public enum Enm_LogLevel
	{
		Enm_LogLevel_Debug,
		Enm_LogLevel_Info,
		Enm_LogLevel_Warn,
		Enm_LogLevel_Error,
		Enm_LogLevel_Always
	}
}
