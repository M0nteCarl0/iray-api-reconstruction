using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace iDetector
{
	public class FrmFileParser
	{
		private const int FIRMWAREPACK_DEVICECOUNT_MAX = 16;

		private const int FIRMWAREPACK_HEADER_LENGTH = 616;

		private const int FIRMWAREPACK_SUBFILEINFO_LENGTH = 266;

		private const string FILE_TYPE_FIRMWARE_PACK = "firmware packages";

		private FirmwarePackFileHeader _header;

		private FirmwarePackSubFileInfo[] _arrSubFileInfo;

		public FirmwarePackFileHeader Header
		{
			get
			{
				return _header;
			}
		}

		public FirmwarePackSubFileInfo[] SubFileInfo
		{
			get
			{
				return _arrSubFileInfo;
			}
		}

		public FrmFileParser()
		{
			_header.ucVerByte0 = (_header.ucVerByte1 = (_header.ucVerByte2 = (_header.ucVerByte3 = 0)));
			_header.strDescription = "";
			_header.arrProducts = new int[8];
			_arrSubFileInfo = new FirmwarePackSubFileInfo[16];
			for (int i = 0; i < 16; i++)
			{
				_arrSubFileInfo[i] = default(FirmwarePackSubFileInfo);
				_arrSubFileInfo[i].ucDeviceType = byte.MaxValue;
				_arrSubFileInfo[i].ucDataType = 0;
			}
		}

		public bool Load(string strFilePath)
		{
			string text = strFilePath.Substring(strFilePath.LastIndexOf('.'), strFilePath.Length - strFilePath.LastIndexOf('.'));
			text = text.ToUpper();
			if (text != ".IFRM")
			{
				return false;
			}
			FileStream fileStream = null;
			try
			{
				fileStream = File.Open(strFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
			}
			catch
			{
				fileStream = null;
			}
			if (fileStream == null || fileStream.Length < 1280)
			{
				return false;
			}
			fileStream.Seek(34L, SeekOrigin.Begin);
			byte[] array = new byte[1024];
			fileStream.Read(array, 0, 64);
			string @string = Encoding.ASCII.GetString(array);
			char[] trimChars = new char[1];
			string a = @string.TrimEnd(trimChars);
			if (a != "firmware packages")
			{
				fileStream.Close();
				return false;
			}
			fileStream.Seek(396L, SeekOrigin.Begin);
			fileStream.Read(array, 0, 2);
			ushort num = array[0];
			if (num != 16)
			{
				fileStream.Close();
				return false;
			}
			Clear();
			fileStream.Seek(406L, SeekOrigin.Begin);
			fileStream.Read(array, 0, 616);
			IntPtr intPtr = Marshal.AllocHGlobal(616);
			Marshal.Copy(array, 0, intPtr, 616);
			_header = (FirmwarePackFileHeader)Marshal.PtrToStructure(intPtr, typeof(FirmwarePackFileHeader));
			Marshal.FreeHGlobal(intPtr);
			bool result = true;
			fileStream.Seek(1024L, SeekOrigin.Begin);
			int num2 = 0;
			IntPtr intPtr2 = Marshal.AllocHGlobal(266);
			for (int i = 0; i < 16; i++)
			{
				try
				{
					fileStream.Read(array, 0, 4);
					num2 = BytesToInt(array, 0);
					fileStream.Seek(12L, SeekOrigin.Current);
					if (num2 != 0)
					{
						fileStream.Read(array, 0, 266);
						Marshal.Copy(array, 0, intPtr2, 266);
						_arrSubFileInfo[i] = (FirmwarePackSubFileInfo)Marshal.PtrToStructure(intPtr2, typeof(FirmwarePackSubFileInfo));
						fileStream.Seek(num2 - 266, SeekOrigin.Current);
					}
				}
				catch
				{
					result = false;
					break;
				}
			}
			Marshal.FreeHGlobal(intPtr2);
			fileStream.Close();
			return result;
		}

		private int BytesToInt(byte[] ary, int offset)
		{
			return (int)((ary[offset] & 0xFF) | ((ary[offset + 1] << 8) & 0xFF00) | ((ary[offset + 2] << 16) & 0xFF0000) | ((ary[offset + 3] << 24) & 4278190080u));
		}

		private void Clear()
		{
			_header.ucVerByte0 = (_header.ucVerByte1 = (_header.ucVerByte2 = (_header.ucVerByte3 = 0)));
			_header.strDescription = "";
			for (int i = 0; i < 16; i++)
			{
				_arrSubFileInfo[i].ucDeviceType = byte.MaxValue;
				_arrSubFileInfo[i].ucDataType = 0;
			}
		}
	}
}
