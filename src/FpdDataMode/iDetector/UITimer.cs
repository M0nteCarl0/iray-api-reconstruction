using System;
using System.Windows.Threading;

namespace iDetector
{
	public class UITimer : IDisposable
	{
		private DispatcherTimer timer;

		private bool isTimerStoped;

		public bool IsTimerStoped
		{
			get
			{
				return isTimerStoped;
			}
		}

		private event Action OnProcess;

		public UITimer(DispatcherPriority priority, int interval, Action process)
		{
			timer = new DispatcherTimer(priority);
			timer.Interval = TimeSpan.FromMilliseconds(interval);
			timer.Tick += OnTimer;
			this.OnProcess = process;
			isTimerStoped = true;
		}

		public UITimer(int interval, Action process)
		{
			timer = new DispatcherTimer();
			timer.Interval = TimeSpan.FromMilliseconds(interval);
			timer.Tick += OnTimer;
			this.OnProcess = process;
			isTimerStoped = true;
		}

		public void Dispose()
		{
			this.OnProcess = null;
			timer.Tick -= OnTimer;
			GC.SuppressFinalize(this);
		}

		public void Close()
		{
			Stop();
			Dispose();
		}

		public void SetIntervalSeconds(int interval)
		{
			timer.Interval = TimeSpan.FromSeconds(interval);
		}

		public void SetIntervalMilliseconds(int interval)
		{
			timer.Interval = TimeSpan.FromMilliseconds(interval);
		}

		public void Start()
		{
			timer.Start();
			isTimerStoped = false;
		}

		public void Stop()
		{
			isTimerStoped = true;
			timer.Stop();
		}

		protected virtual void OnTimer(object sender, EventArgs e)
		{
			ProcessHandler();
		}

		protected void ProcessHandler()
		{
			if (this.OnProcess != null)
			{
				this.OnProcess();
			}
		}
	}
}
