using System;
using System.Runtime.InteropServices;
using System.Text;

namespace iDetector
{
	public class IniParser
	{
		[DllImport("kernel32")]
		public static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

		[DllImport("kernel32")]
		public static extern uint GetPrivateProfileInt(string lpAppName, string lpKeyName, int nDefault, string lpFileName);

		[DllImport("kernel32")]
		public static extern int GetPrivateProfileSection(string lpAppName, StringBuilder lpReturnedString, int nSize, string lpFileName);

		[DllImport("kernel32")]
		public static extern int WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);

		public static double GetPrivateProfileDouble(string lpAppName, string lpKeyName, double nDefault, string lpFileName)
		{
			StringBuilder stringBuilder = new StringBuilder(64);
			GetPrivateProfileString(lpAppName, lpKeyName, nDefault.ToString(), stringBuilder, stringBuilder.Capacity, lpFileName);
			double value = Convert.ToDouble(stringBuilder.ToString());
			return Math.Round(value, 1);
		}

		public static string GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, string lpFileName)
		{
			StringBuilder stringBuilder = new StringBuilder(128);
			GetPrivateProfileString(lpAppName, lpKeyName, lpDefault, stringBuilder, stringBuilder.Capacity, lpFileName);
			return stringBuilder.ToString();
		}

		public static bool IsSectionExisted(string lpAppName, string lpFileName)
		{
			StringBuilder stringBuilder = new StringBuilder(64);
			int privateProfileSection = GetPrivateProfileSection(lpAppName, stringBuilder, stringBuilder.Capacity, lpFileName);
			return 0 != privateProfileSection;
		}
	}
}
