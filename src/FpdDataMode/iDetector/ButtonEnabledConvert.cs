using System;
using System.Globalization;
using System.Windows.Data;

namespace iDetector
{
	public class ButtonEnabledConvert : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values == null)
			{
				return false;
			}
			bool flag = false;
			for (int i = 0; i < values.Length - 1; i++)
			{
				flag |= (bool)values[i];
			}
			flag &= (bool)values[values.Length - 1];
			return flag;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
