namespace iDetector
{
	public class FileSave
	{
		public static bool Save(string filePath, IRayImageData image)
		{
			if (image == null || image.ImgData == null)
			{
				return false;
			}
			string text = filePath.Substring(filePath.LastIndexOf(".") + 1, filePath.Length - filePath.LastIndexOf(".") - 1);
			if (text.ToLower().Equals("raw"))
			{
				FileOperation.SaveRawFile(filePath, image.ImgData, image.ImgData.Length);
			}
			else if (text.ToLower().Equals("tif"))
			{
				IRayImageEx rayImageEx = new IRayImageEx(ref image);
				FileOperation.SaveTiffFile(filePath, rayImageEx.iRayImage);
			}
			else if (text.ToLower().Equals("dcm"))
			{
				FileOperation.SaveDcmFile(filePath, image);
			}
			return true;
		}
	}
}
