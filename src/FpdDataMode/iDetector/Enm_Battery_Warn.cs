namespace iDetector
{
	public enum Enm_Battery_Warn
	{
		Enm_Battery_Normal,
		Enm_Battery_LowPower,
		Enm_Battery_PowerOff
	}
}
