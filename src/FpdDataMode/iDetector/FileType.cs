namespace iDetector
{
	public enum FileType
	{
		RAW_FILE = 1,
		TIFF_FILE,
		DCM_FILE
	}
}
