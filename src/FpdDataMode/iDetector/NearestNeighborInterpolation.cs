namespace iDetector
{
	public class NearestNeighborInterpolation : AImageCompress
	{
		public override void PicZoom(ref TPicRegion Dst, TPicRegion Src)
		{
			if (Dst.width == Src.width && Dst.height == Src.height)
			{
				Dst = Src;
				return;
			}
			double num = (double)Src.width / (double)Dst.width;
			double num2 = (double)Src.height / (double)Dst.height;
			byte[] pdata = Src.pdata;
			byte[] pdata2 = Dst.pdata;
			for (int i = 0; i < Dst.height; i++)
			{
				int num3 = (int)(num2 * (double)i + 0.5);
				for (int j = 0; j < Dst.width; j++)
				{
					int num4 = (int)(num * (double)j + 0.5);
					pdata2[i * Dst.width + j] = pdata[num3 * Src.width + num4];
				}
			}
		}
	}
}
