using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayVariantVal
	{
		public int nVal;

		public float fVal;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 512)]
		public string strVal;
	}
}
