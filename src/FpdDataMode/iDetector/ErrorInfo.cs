using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct ErrorInfo
	{
		public int nErrorCode;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strDescription;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strSolution;
	}
}
