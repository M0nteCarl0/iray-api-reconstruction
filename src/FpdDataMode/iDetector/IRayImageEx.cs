using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public class IRayImageEx : IDisposable
	{
		public IRayImage iRayImage;

		public IRayImageEx(ref IRayImageData image)
		{
			if (image != null && image.ImgData.Length != iRayImage.nWidth * iRayImage.nHeight * iRayImage.nBytesPerPixel)
			{
				iRayImage = new IRayImage
				{
					nWidth = image.nWidth,
					nHeight = image.nHeight,
					nBytesPerPixel = image.nBytesPerPixel
				};
				iRayImage.pData = Marshal.AllocHGlobal(image.nWidth * image.nHeight * image.nBytesPerPixel);
				Marshal.Copy(image.ImgData, 0, iRayImage.pData, image.ImgData.Length);
				if (image.Params != null)
				{
					iRayImage.propList.nItemCount = image.Params.Length;
					iRayImage.propList.pItems = Marshal.AllocHGlobal(iRayImage.propList.nItemCount * Marshal.SizeOf(typeof(IRayVariantMapItem)));
					SdkParamConvertor<IRayVariantMapItem>.StructArrayToIntPtr(image.Params, ref iRayImage.propList.pItems);
				}
			}
		}

		public void Dispose()
		{
			ReleaseMemory();
		}

		private void ReleaseMemory()
		{
			if (IntPtr.Zero != iRayImage.pData)
			{
				Marshal.FreeHGlobal(iRayImage.pData);
			}
			if (IntPtr.Zero != iRayImage.propList.pItems)
			{
				Marshal.FreeHGlobal(iRayImage.propList.pItems);
			}
		}
	}
}
