using System;
using System.IO;

namespace iDetector
{
	public class HardDiskStoreThumbnail : iThumbnailMedium
	{
		private FileStream fileStream;

		private string mapFileName;

		public bool Create(string fileName, int totalSize)
		{
			mapFileName = CurrentDetectorSingleton.Instance().GetTempDataDir() + fileName;
			try
			{
				fileStream = new FileStream(mapFileName, FileMode.Create, FileAccess.ReadWrite);
				Log.Instance().Write("HDThumbnail file path:{0}", mapFileName);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Create HDThumnail failed!Path:{0},Err={1}", mapFileName, ex.ToString());
				throw ex;
			}
			return true;
		}

		public void Close()
		{
			if (fileStream != null)
			{
				fileStream.Close();
				fileStream = null;
				try
				{
					if (File.Exists(mapFileName))
					{
						File.Delete(mapFileName);
					}
				}
				catch
				{
				}
			}
		}

		public int Write(long position, byte[] array, int offset, int count)
		{
			if (fileStream == null || array == null || count == 0)
			{
				return 0;
			}
			fileStream.Seek(position, SeekOrigin.Begin);
			fileStream.Write(array, offset, count);
			return count;
		}

		public int Read(long position, byte[] array, int offset, int count)
		{
			if (fileStream == null || array == null || count == 0)
			{
				return 0;
			}
			fileStream.Seek(position, SeekOrigin.Begin);
			return fileStream.Read(array, offset, count);
		}
	}
}
