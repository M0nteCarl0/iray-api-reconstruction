namespace iDetector
{
	public enum Enm_ExpMode
	{
		Enm_ExpMode_Null = 0,
		Enm_ExpMode_Manual = 1,
		Enm_ExpMode_AEC = 2,
		Enm_ExpMode_Flush = 3,
		Enm_ExpMode_Pulse = 0x80,
		Enm_ExpMode_Continous = 129,
		Enm_ExpMode_Linewise = 130
	}
}
