using System;

namespace iDetector
{
	public static class EnumConvert<T>
	{
		public static T ParseFromString(string s)
		{
			return (T)Enum.Parse(typeof(T), s);
		}
	}
}
