namespace iDetector
{
	public enum Enm_CorrectOption
	{
		Enm_CorrectOp_HW_PreOffset = 1,
		Enm_CorrectOp_HW_PostOffset = 2,
		Enm_CorrectOp_HW_Gain = 4,
		Enm_CorrectOp_HW_Defect = 0x10,
		Enm_CorrectOp_HW_Ghost = 0x20,
		Enm_CorrectOp_HW_Lag = 0x40,
		Enm_CorrectOp_HW_MicroPhony = 0x80,
		Enm_CorrectOp_SW_PreOffset = 0x10000,
		Enm_CorrectOp_SW_PostOffset = 0x20000,
		Enm_CorrectOp_SW_Gain = 0x40000,
		Enm_CorrectOp_SW_Defect = 0x100000,
		Enm_CorrectOp_SW_Ghost = 0x200000,
		Enm_CorrectOp_SW_Lag = 0x400000,
		Enm_CorrectOp_SW_MicroPhony = 0x800000
	}
}
