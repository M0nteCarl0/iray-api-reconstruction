using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace iDetector
{
	public class SdkInterface
	{
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct ProdInfoEx
		{
			public int nProdNo;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
			public string strName;

			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string strDescripion;

			public int nFullWidth;

			public int nFullHeight;

			public int nDummyTop;

			public int nDummyBottom;

			public int nDummyLeft;

			public int nDummyRight;

			public int nAfeChSize;

			public int nGateSize;

			public int nGateEdge;

			public int nPhysicalPixelSize;

			public int nBitDepth;

			public int nDataBytesPerPacket;

			public int nTotalPackets;

			public int nRetransferMode;

			public int nDriveMode;
		}

		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate void SdkCallbackHandler(int nDetectorID, int nEventID, int nEventLevel, IntPtr pszMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam);

		[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
		public delegate void ScanCallbackHandler(IntPtr pProfile);

		public const int Evt_NotDefine = 0;

		public const int Cmd_NotDefine = 0;

		public const int Cfg_ProtocolEdition = 1;

		public const int Cfg_ProductNo = 2;

		public const int Cfg_SN = 3;

		public const int Cfg_UseServiceProcess = 8;

		public const int Cfg_DetectorImp = 4;

		public const int Cfg_ConnImp = 5;

		public const int Cfg_CaliImp = 6;

		public const int Cfg_LogLevel = 7;

		public const int Cfg_HostIP = 101;

		public const int Cfg_HostPort = 102;

		public const int Cfg_RemoteIP = 103;

		public const int Cfg_RemotePort = 104;

		public const int Cfg_ComPort = 105;

		public const int Cfg_PleoraConnStr = 106;

		public const int Cfg_PleoraPacketSize = 107;

		public const int Cfg_WinpcapConnStr = 108;

		public const int Cfg_PleoraMaxFps = 109;

		public const int Cfg_RepeatCmdEnable = 150;

		public const int Cfg_FTP_Download_HostIP = 201;

		public const int Cfg_FTP_Download_HostPort = 202;

		public const int Cfg_FTP_Download_User = 203;

		public const int Cfg_FTP_Download_PWD = 204;

		public const int Cfg_FTP_Download_LocalPath = 205;

		public const int Cfg_FTP_Upload_HostIP = 206;

		public const int Cfg_FTP_Upload_HostPort = 207;

		public const int Cfg_FTP_Upload_User = 208;

		public const int Cfg_FTP_Upload_PWD = 209;

		public const int Cfg_FTP_Upload_LocalPath = 210;

		public const int Cfg_OffsetAlarmMinute = 301;

		public const int Cfg_GainAlarmTime = 302;

		public const int Cfg_DefectAlarmTime = 303;

		public const int Cfg_CaliValidity_PreWarnMinute = 304;

		public const int Cfg_CaliValidity_Enable = 305;

		public const int Cfg_DefaultSubset = 306;

		public const int Cfg_DefaultCorrectOption = 307;

		public const int Cfg_DefectStatistical_DummyTop = 308;

		public const int Cfg_DefectStatistical_DummyBottom = 309;

		public const int Cfg_DefectStatistical_DummyLeft = 310;

		public const int Cfg_DefectStatistical_DummyRight = 311;

		public const int Cfg_ClearAcqParam_DelayTime = 501;

		public const int Cfg_FpsCheck_Enable = 502;

		public const int Cfg_FpsCheck_Tolerance = 503;

		public const int Cfg_FWUpdTimeOut = 504;

		public const int Cfg_OfflineInspectTimeout = 505;

		public const int Cfg_AllowReconnectByOnlineNotice = 506;

		public const int Cfg_ResetTimeout = 507;

		public const int Cfg_PreviewImage_Enable = 508;

		public const int Cfg_PushImageAtExpTimeout_Enable = 509;

		public const int Cfg_RetransferCount = 510;

		public const int Cfg_ConnRecoverTimeout = 511;

		public const int Cfg_TemperatureHighThreshold = 512;

		public const int Cfg_AllowMismatchSN = 513;

		public const int Cfg_ImagePacketGapTimeout = 514;

		public const int Cfg_FwAllowedDefectPoints = 515;

		public const int Cfg_PostOffsetStart_DelayTime = 516;

		public const int Cfg_TotalAcqTimeout = 517;

		public const int Cfg_PreExpImageAcqTimeout = 518;

		public const int Cfg_CleanupProcessTime = 519;

		public const int Cfg_SeqAcq_AutoStopToSyncExp = 520;

		public const int Cfg_Acq2SubFlow = 521;

		public const int Attr_Prod_Name = 1001;

		public const int Attr_Prod_Description = 1002;

		public const int Attr_Prod_FullWidth = 1003;

		public const int Attr_Prod_FullHeight = 1004;

		public const int Attr_Prod_PhysicalPixelSize = 1005;

		public const int Attr_Prod_BitDepth = 1006;

		public const int Attr_Prod_DataBytesPerPacket = 1007;

		public const int Attr_Prod_TotalPacketNumber = 1008;

		public const int Attr_Prod_DummyTop = 1009;

		public const int Attr_Prod_DummyBottom = 1010;

		public const int Attr_Prod_DummyLeft = 1011;

		public const int Attr_Prod_DummyRight = 1012;

		public const int Attr_Prod_AfeChSize = 1014;

		public const int Attr_Prod_GateSize = 1016;

		public const int Attr_Prod_GateEdge = 1017;

		public const int Attr_Prod_DriveMode = 1013;

		public const int Attr_Prod_ReXferMode = 1015;

		public const int Attr_UROM_ProductNo = 2001;

		public const int Attr_UROM_MainVersion = 2002;

		public const int Attr_UROM_ReadVersion = 2003;

		public const int Attr_UROM_McuVersion = 2004;

		public const int Attr_UROM_ArmVersion = 2005;

		public const int Attr_UROM_KernelVersion = 2006;

		public const int Attr_UROM_ProtocolVersion = 2007;

		public const int Attr_UROM_MasterBuildTime = 2008;

		public const int Attr_UROM_SlaveBuildTime = 2009;

		public const int Attr_UROM_McuBuildTime = 2010;

		public const int Attr_UROM_RowPreDelayTime = 2011;

		public const int Attr_UROM_RowPostDelayTime = 2012;

		public const int Attr_UROM_IntegrateTime = 2013;

		public const int Attr_UROM_ZoomMode = 2014;

		public const int Attr_UROM_ExpEnable_SignalLevel = 2015;

		public const int Attr_UROM_SelfClearEnable = 2016;

		public const int Attr_UROM_SelfClearSpanTime = 2017;

		public const int Attr_UROM_SequenceIntervalTime = 2018;

		public const int Attr_UROM_TriggerMode = 2019;

		public const int Attr_UROM_DynamicFlag = 2020;

		public const int Attr_UROM_TubeReadyTime = 2021;

		public const int Attr_UROM_SequenceIntervalTime_HighPrecision = 2022;

		public const int Attr_UROM_SetDelayTime = 2023;

		public const int Attr_UROM_ExpWindowTime = 2025;

		public const int Attr_UROM_SyncExpTime = 2027;

		public const int Attr_UROM_SyncExpTime_HighPrecision = 2028;

		public const int Attr_UROM_VT = 2029;

		public const int Attr_UROM_PGA = 2030;

		public const int Attr_UROM_PrepCapMode = 2032;

		public const int Attr_UROM_SelfCapEnable = 2033;

		public const int Attr_UROM_FluroSync = 2034;

		public const int Attr_UROM_SrcPort = 2035;

		public const int Attr_UROM_SrcIP = 2036;

		public const int Attr_UROM_SrcMAC = 2037;

		public const int Attr_UROM_DestPort = 2038;

		public const int Attr_UROM_DestIP = 2039;

		public const int Attr_UROM_DestMAC = 2040;

		public const int Attr_UROM_SyncboxIP = 2041;

		public const int Attr_UROM_PreviewImgMode = 2044;

		public const int Attr_UROM_HWOffsetType = 2045;

		public const int Attr_UROM_AcquireDelayTime = 2046;

		public const int Attr_UROM_BinningMode = 2047;

		public const int Attr_UROM_ExpMode = 2050;

		public const int Attr_UROM_AecMainTime = 2051;

		public const int Attr_UROM_DynaOffsetGapTime = 2052;

		public const int Attr_UROM_DynaOffsetEnable = 2053;

		public const int Attr_UROM_ImagePktGapTime = 2054;

		public const int Attr_UROM_OutModeCapTrig = 2069;

		public const int Attr_UROM_HvgPrepOn = 2055;

		public const int Attr_UROM_HvgXRayEnable = 2056;

		public const int Attr_UROM_HvgXRayOn = 2057;

		public const int Attr_UROM_HvgXRaySyncOut = 2058;

		public const int Attr_UROM_HvgXRaySyncIn = 2059;

		public const int Attr_UROM_CbxBuildTime = 2060;

		public const int Attr_UROM_SubProductNo = 2061;

		public const int Attr_UROM_SerialNo = 2062;

		public const int Attr_UROM_ImageChType = 2063;

		public const int Attr_UROM_ImageChProtocol = 2064;

		public const int Attr_UROM_HWGainEnable = 2065;

		public const int Attr_UROM_ExpTimeValidPercent = 2066;

		public const int Attr_UROM_FreesyncCenterThreshold = 2067;

		public const int Attr_UROM_FreesyncEdgeThreshold = 2068;

		public const int Attr_UROM_FreesyncSubFlow = 2070;

		public const int Attr_UROM_PowSeriesCorrectEnable = 2071;

		public const int Attr_UROM_PulseClearTimes = 2072;

		public const int Attr_UROM_ROIColStartPos = 2073;

		public const int Attr_UROM_ROIColEndPos = 2074;

		public const int Attr_UROM_ROIRowStartPos = 2075;

		public const int Attr_UROM_ROIRowEndPos = 2076;

		public const int Attr_UROM_FullWell = 2077;

		public const int Attr_UROM_InnerSubFlow = 2078;

		public const int Attr_UROM_SoftwareSubFlow = 2079;

		public const int Attr_UROM_IntegrateTime_W = 2540;

		public const int Attr_UROM_ZoomMode_W = 2501;

		public const int Attr_UROM_ExpEnable_SignalLevel_W = 2502;

		public const int Attr_UROM_SelfClearEnable_W = 2503;

		public const int Attr_UROM_SelfClearSpanTime_W = 2504;

		public const int Attr_UROM_SequenceIntervalTime_W = 2505;

		public const int Attr_UROM_TriggerMode_W = 2506;

		public const int Attr_UROM_DynamicFlag_W = 2507;

		public const int Attr_UROM_TubeReadyTime_W = 2508;

		public const int Attr_UROM_SetDelayTime_W = 2510;

		public const int Attr_UROM_SequenceIntervalTime_HighPrecision_W = 2511;

		public const int Attr_UROM_ExpWindowTime_W = 2512;

		public const int Attr_UROM_PGA_W = 2513;

		public const int Attr_UROM_PrepCapMode_W = 2514;

		public const int Attr_UROM_SelfCapEnable_W = 2515;

		public const int Attr_UROM_FluroSync_W = 2516;

		public const int Attr_UROM_SrcIP_W = 2518;

		public const int Attr_UROM_SrcMAC_W = 2519;

		public const int Attr_UROM_DestPort_W = 2520;

		public const int Attr_UROM_DestIP_W = 2521;

		public const int Attr_UROM_DestMAC_W = 2522;

		public const int Attr_UROM_PreviewImgMode_W = 2523;

		public const int Attr_UROM_HWOffsetType_W = 2544;

		public const int Attr_UROM_SyncboxIP_W = 2543;

		public const int Attr_UROM_AcquireDelayTime_W = 2524;

		public const int Attr_UROM_BinningMode_W = 2525;

		public const int Attr_UROM_ExpMode_W = 2528;

		public const int Attr_UROM_AecMainTime_W = 2529;

		public const int Attr_UROM_DynaOffsetGapTime_W = 2530;

		public const int Attr_UROM_DynaOffsetEnable_W = 2531;

		public const int Attr_UROM_ImagePktGapTime_W = 2542;

		public const int Attr_UROM_OutModeCapTrig_W = 2541;

		public const int Attr_UROM_HvgPrepOn_W = 2532;

		public const int Attr_UROM_HvgXRayEnable_W = 2533;

		public const int Attr_UROM_HvgXRayOn_W = 2534;

		public const int Attr_UROM_HvgXRaySyncOut_W = 2535;

		public const int Attr_UROM_HvgXRaySyncIn_W = 2536;

		public const int Attr_UROM_ExpTimeValidPercent_W = 2537;

		public const int Attr_UROM_FreesyncCenterThreshold_W = 2538;

		public const int Attr_UROM_FreesyncEdgeThreshold_W = 2539;

		public const int Attr_UROM_PowSeriesCorrectEnable_W = 2545;

		public const int Attr_UROM_ROIColStartPos_W = 2546;

		public const int Attr_UROM_ROIColEndPos_W = 2547;

		public const int Attr_UROM_ROIRowStartPos_W = 2548;

		public const int Attr_UROM_ROIRowEndPos_W = 2549;

		public const int Attr_UROM_FullWell_W = 2550;

		public const int Attr_UROM_PulseClearTimes_W = 2551;

		public const int Attr_UROM_InnerSubFlow_W = 2552;

		public const int Attr_UROM_SoftwareSubFlow_W = 2553;

		public const int Attr_FROM_ProductNo = 3001;

		public const int Attr_FROM_MainVersion = 3002;

		public const int Attr_FROM_ReadVersion = 3003;

		public const int Attr_FROM_McuVersion = 3004;

		public const int Attr_FROM_ArmVersion = 3005;

		public const int Attr_FROM_KernelVersion = 3006;

		public const int Attr_FROM_ProtocolVersion = 3007;

		public const int Attr_FROM_MasterBuildTime = 3008;

		public const int Attr_FROM_SlaveBuildTime = 3009;

		public const int Attr_FROM_McuBuildTime = 3010;

		public const int Attr_FROM_RowPreDelayTime = 3011;

		public const int Attr_FROM_RowPostDelayTime = 3012;

		public const int Attr_FROM_IntegrateTime = 3013;

		public const int Attr_FROM_ZoomMode = 3014;

		public const int Attr_FROM_ExpEnable_SignalLevel = 3015;

		public const int Attr_FROM_SelfClearEnable = 3016;

		public const int Attr_FROM_SelfClearSpanTime = 3017;

		public const int Attr_FROM_SequenceIntervalTime = 3018;

		public const int Attr_FROM_TriggerMode = 3019;

		public const int Attr_FROM_DynamicFlag = 3020;

		public const int Attr_FROM_TubeReadyTime = 3021;

		public const int Attr_FROM_SequenceIntervalTime_HighPrecision = 3022;

		public const int Attr_FROM_SetDelayTime = 3023;

		public const int Attr_FROM_ExpWindowTime = 3025;

		public const int Attr_FROM_SyncExpTime = 3027;

		public const int Attr_FROM_SyncExpTime_HighPrecision = 3028;

		public const int Attr_FROM_VT = 3029;

		public const int Attr_FROM_PGA = 3030;

		public const int Attr_FROM_PrepCapMode = 3032;

		public const int Attr_FROM_SelfCapEnable = 3033;

		public const int Attr_FROM_FluroSync = 3034;

		public const int Attr_FROM_SrcPort = 3035;

		public const int Attr_FROM_SrcIP = 3036;

		public const int Attr_FROM_SrcMAC = 3037;

		public const int Attr_FROM_DestPort = 3038;

		public const int Attr_FROM_DestIP = 3039;

		public const int Attr_FROM_DestMAC = 3040;

		public const int Attr_FROM_SyncboxIP = 3041;

		public const int Attr_FROM_PreviewImgMode = 3044;

		public const int Attr_FROM_HWOffsetType = 3045;

		public const int Attr_FROM_AcquireDelayTime = 3046;

		public const int Attr_FROM_BinningMode = 3047;

		public const int Attr_FROM_ExpMode = 3050;

		public const int Attr_FROM_AecMainTime = 3051;

		public const int Attr_FROM_DynaOffsetGapTime = 3052;

		public const int Attr_FROM_DynaOffsetEnable = 3053;

		public const int Attr_FROM_ImagePktGapTime = 3054;

		public const int Attr_FROM_OutModeCapTrig = 3069;

		public const int Attr_FROM_HvgPrepOn = 3055;

		public const int Attr_FROM_HvgXRayEnable = 3056;

		public const int Attr_FROM_HvgXRayOn = 3057;

		public const int Attr_FROM_HvgXRaySyncOut = 3058;

		public const int Attr_FROM_HvgXRaySyncIn = 3059;

		public const int Attr_FROM_CbxBuildTime = 3060;

		public const int Attr_FROM_SubProductNo = 3061;

		public const int Attr_FROM_SerialNo = 3062;

		public const int Attr_FROM_ImageChType = 3063;

		public const int Attr_FROM_ImageChProtocol = 3064;

		public const int Attr_FROM_HWGainEnable = 3065;

		public const int Attr_FROM_ExpTimeValidPercent = 3066;

		public const int Attr_FROM_FreesyncCenterThreshold = 3067;

		public const int Attr_FROM_FreesyncEdgeThreshold = 3068;

		public const int Attr_FROM_FreesyncSubFlow = 3070;

		public const int Attr_FROM_AutoSleepIdleTime = 3071;

		public const int Attr_FROM_FSyncParalClearTimes = 3072;

		public const int Attr_FROM_FSyncFastScanCpvCycle = 3073;

		public const int Attr_FROM_FSyncTriggerCheckTimeout = 3074;

		public const int Attr_FROM_FSyncSegmentThreshold = 3075;

		public const int Attr_FROM_FSyncLineThreshold = 3076;

		public const int Attr_FROM_FSyncFalseTriggerUnresponseStageTime = 3077;

		public const int Attr_FROM_FSyncParalClearLine = 3078;

		public const int Attr_FROM_PowSeriesCorrectEnable = 3079;

		public const int Attr_FROM_PulseClearTimes = 3080;

		public const int Attr_FROM_ROIColStartPos = 3081;

		public const int Attr_FROM_ROIColEndPos = 3082;

		public const int Attr_FROM_ROIRowStartPos = 3083;

		public const int Attr_FROM_ROIRowEndPos = 3084;

		public const int Attr_FROM_FullWell = 3085;

		public const int Attr_FROM_InnerSubFlow = 3086;

		public const int Attr_FROM_SoftwareSubFlow = 3087;

		public const int Attr_FROM_Debug1 = 3200;

		public const int Attr_FROM_Debug2 = 3201;

		public const int Attr_FROM_Debug3 = 3202;

		public const int Attr_FROM_Debug4 = 3203;

		public const int Attr_FROM_Debug5 = 3204;

		public const int Attr_FROM_Debug6 = 3205;

		public const int Attr_FROM_Debug7 = 3206;

		public const int Attr_FROM_Debug8 = 3207;

		public const int Attr_FROM_Debug9 = 3208;

		public const int Attr_FROM_Debug10 = 3209;

		public const int Attr_FROM_Debug11 = 3210;

		public const int Attr_FROM_Debug12 = 3211;

		public const int Attr_FROM_Debug13 = 3212;

		public const int Attr_FROM_Debug14 = 3213;

		public const int Attr_FROM_Debug15 = 3214;

		public const int Attr_FROM_Debug16 = 3215;

		public const int Attr_FROM_Debug17 = 3216;

		public const int Attr_FROM_Debug18 = 3217;

		public const int Attr_FROM_Debug19 = 3218;

		public const int Attr_FROM_Debug20 = 3219;

		public const int Attr_FROM_Debug21 = 3220;

		public const int Attr_FROM_Debug22 = 3221;

		public const int Attr_FROM_Debug23 = 3222;

		public const int Attr_FROM_Debug24 = 3223;

		public const int Attr_FROM_Debug25 = 3224;

		public const int Attr_FROM_Debug26 = 3225;

		public const int Attr_FROM_Debug27 = 3226;

		public const int Attr_FROM_Debug28 = 3227;

		public const int Attr_FROM_Debug29 = 3228;

		public const int Attr_FROM_Debug30 = 3229;

		public const int Attr_FROM_Debug31 = 3230;

		public const int Attr_FROM_Debug32 = 3231;

		public const int Attr_FROM_Test1 = 3232;

		public const int Attr_FROM_Test2 = 3233;

		public const int Attr_FROM_Test3 = 3234;

		public const int Attr_FROM_Test4 = 3235;

		public const int Attr_FROM_Test5 = 3236;

		public const int Attr_FROM_Test6 = 3237;

		public const int Attr_FROM_Test7 = 3238;

		public const int Attr_FROM_Test8 = 3239;

		public const int Attr_FROM_Test9 = 3240;

		public const int Attr_FROM_Test10 = 3241;

		public const int Attr_FROM_Test11 = 3242;

		public const int Attr_FROM_Test12 = 3243;

		public const int Attr_FROM_Test13 = 3244;

		public const int Attr_FROM_Test14 = 3245;

		public const int Attr_FROM_Test15 = 3246;

		public const int Attr_FROM_Test16 = 3247;

		public const int Attr_FROM_RowPreDelayTime_W = 3511;

		public const int Attr_FROM_RowPostDelayTime_W = 3512;

		public const int Attr_FROM_IntegrateTime_W = 3513;

		public const int Attr_FROM_ZoomMode_W = 3514;

		public const int Attr_FROM_ExpEnable_SignalLevel_W = 3515;

		public const int Attr_FROM_SelfClearEnable_W = 3516;

		public const int Attr_FROM_SelfClearSpanTime_W = 3517;

		public const int Attr_FROM_SequenceIntervalTime_W = 3518;

		public const int Attr_FROM_TriggerMode_W = 3519;

		public const int Attr_FROM_DynamicFlag_W = 3520;

		public const int Attr_FROM_TubeReadyTime_W = 3521;

		public const int Attr_FROM_SequenceIntervalTime_HighPrecision_W = 3522;

		public const int Attr_FROM_SetDelayTime_W = 3523;

		public const int Attr_FROM_ExpWindowTime_W = 3525;

		public const int Attr_FROM_VT_W = 3529;

		public const int Attr_FROM_PGA_W = 3530;

		public const int Attr_FROM_PrepCapMode_W = 3532;

		public const int Attr_FROM_SelfCapEnable_W = 3533;

		public const int Attr_FROM_FluroSync_W = 3534;

		public const int Attr_FROM_SrcIP_W = 3536;

		public const int Attr_FROM_SrcMAC_W = 3537;

		public const int Attr_FROM_DestPort_W = 3538;

		public const int Attr_FROM_DestIP_W = 3539;

		public const int Attr_FROM_DestMAC_W = 3540;

		public const int Attr_FROM_SyncboxIP_W = 3541;

		public const int Attr_FROM_PreviewImgMode_W = 3544;

		public const int Attr_FROM_HWOffsetType_W = 3545;

		public const int Attr_FROM_AcquireDelayTime_W = 3546;

		public const int Attr_FROM_BinningMode_W = 3547;

		public const int Attr_FROM_ExpMode_W = 3550;

		public const int Attr_FROM_AecMainTime_W = 3551;

		public const int Attr_FROM_DynaOffsetGapTime_W = 3552;

		public const int Attr_FROM_DynaOffsetEnable_W = 3553;

		public const int Attr_FROM_ImagePktGapTime_W = 3554;

		public const int Attr_FROM_OutModeCapTrig_W = 3569;

		public const int Attr_FROM_HvgPrepOn_W = 3555;

		public const int Attr_FROM_HvgXRayEnable_W = 3556;

		public const int Attr_FROM_HvgXRayOn_W = 3557;

		public const int Attr_FROM_HvgXRaySyncOut_W = 3558;

		public const int Attr_FROM_HvgXRaySyncIn_W = 3559;

		public const int Attr_FROM_CbxBuildTime_W = 3560;

		public const int Attr_FROM_SubProductNo_W = 3561;

		public const int Attr_FROM_SerialNo_W = 3562;

		public const int Attr_FROM_ImageChType_W = 3563;

		public const int Attr_FROM_ImageChProtocol_W = 3564;

		public const int Attr_FROM_HWGainEnable_W = 3565;

		public const int Attr_FROM_ExpTimeValidPercent_W = 3566;

		public const int Attr_FROM_FreesyncCenterThreshold_W = 3567;

		public const int Attr_FROM_FreesyncEdgeThreshold_W = 3568;

		public const int Attr_FROM_FreesyncSubFlow_W = 3570;

		public const int Attr_FROM_AutoSleepIdleTime_W = 3571;

		public const int Attr_FROM_FSyncParalClearTimes_W = 3572;

		public const int Attr_FROM_FSyncFastScanCpvCycle_W = 3573;

		public const int Attr_FROM_FSyncTriggerCheckTimeout_W = 3574;

		public const int Attr_FROM_FSyncSegmentThreshold_W = 3575;

		public const int Attr_FROM_FSyncLineThreshold_W = 3576;

		public const int Attr_FROM_FSyncFalseTriggerUnresponseStageTime_W = 3577;

		public const int Attr_FROM_FSyncParalClearLine_W = 3578;

		public const int Attr_FROM_PowSeriesCorrectEnable_W = 3579;

		public const int Attr_FROM_PulseClearTimes_W = 3580;

		public const int Attr_FROM_ROIColStartPos_W = 3581;

		public const int Attr_FROM_ROIColEndPos_W = 3582;

		public const int Attr_FROM_ROIRowStartPos_W = 3583;

		public const int Attr_FROM_ROIRowEndPos_W = 3584;

		public const int Attr_FROM_FullWell_W = 3585;

		public const int Attr_FROM_InnerSubFlow_W = 3586;

		public const int Attr_FROM_SoftwareSubFlow_W = 3587;

		public const int Attr_FROM_Debug1_W = 3700;

		public const int Attr_FROM_Debug2_W = 3701;

		public const int Attr_FROM_Debug3_W = 3702;

		public const int Attr_FROM_Debug4_W = 3703;

		public const int Attr_FROM_Debug5_W = 3704;

		public const int Attr_FROM_Debug6_W = 3705;

		public const int Attr_FROM_Debug7_W = 3706;

		public const int Attr_FROM_Debug8_W = 3707;

		public const int Attr_FROM_Debug9_W = 3708;

		public const int Attr_FROM_Debug10_W = 3709;

		public const int Attr_FROM_Debug11_W = 3710;

		public const int Attr_FROM_Debug12_W = 3711;

		public const int Attr_FROM_Debug13_W = 3712;

		public const int Attr_FROM_Debug14_W = 3713;

		public const int Attr_FROM_Debug15_W = 3714;

		public const int Attr_FROM_Debug16_W = 3715;

		public const int Attr_FROM_Debug17_W = 3716;

		public const int Attr_FROM_Debug18_W = 3717;

		public const int Attr_FROM_Debug19_W = 3718;

		public const int Attr_FROM_Debug20_W = 3719;

		public const int Attr_FROM_Debug21_W = 3720;

		public const int Attr_FROM_Debug22_W = 3721;

		public const int Attr_FROM_Debug23_W = 3722;

		public const int Attr_FROM_Debug24_W = 3723;

		public const int Attr_FROM_Debug25_W = 3724;

		public const int Attr_FROM_Debug26_W = 3725;

		public const int Attr_FROM_Debug27_W = 3726;

		public const int Attr_FROM_Debug28_W = 3727;

		public const int Attr_FROM_Debug29_W = 3728;

		public const int Attr_FROM_Debug30_W = 3729;

		public const int Attr_FROM_Debug31_W = 3730;

		public const int Attr_FROM_Debug32_W = 3731;

		public const int Attr_FROM_Test1_W = 3732;

		public const int Attr_FROM_Test2_W = 3733;

		public const int Attr_FROM_Test3_W = 3734;

		public const int Attr_FROM_Test4_W = 3735;

		public const int Attr_FROM_Test5_W = 3736;

		public const int Attr_FROM_Test6_W = 3737;

		public const int Attr_FROM_Test7_W = 3738;

		public const int Attr_FROM_Test8_W = 3739;

		public const int Attr_FROM_Test9_W = 3740;

		public const int Attr_FROM_Test10_W = 3741;

		public const int Attr_FROM_Test11_W = 3742;

		public const int Attr_FROM_Test12_W = 3743;

		public const int Attr_FROM_Test13_W = 3744;

		public const int Attr_FROM_Test14_W = 3745;

		public const int Attr_FROM_Test15_W = 3746;

		public const int Attr_FROM_Test16_W = 3747;

		public const int Attr_Wifi_AP_SSID = 4001;

		public const int Attr_Wifi_AP_Key = 4002;

		public const int Attr_Wifi_AP_CountryCode = 4003;

		public const int Attr_Wifi_AP_FrequencySel = 4004;

		public const int Attr_Wifi_AP_BandWidthSel = 4005;

		public const int Attr_Wifi_AP_ChannelSel = 4006;

		public const int Attr_Wifi_AP_SecuritySel = 4007;

		public const int Attr_Wifi_AP_ApModeEn = 4008;

		public const int Attr_Wifi_AP_DhcpServerEn = 4009;

		public const int Attr_Wifi_Client_ListNum = 4010;

		public const int Attr_Wifi_Client_CurrentSel = 4011;

		public const int Attr_Wifi_Client_SSID0 = 4012;

		public const int Attr_Wifi_Client_Key0 = 4013;

		public const int Attr_Wifi_Client_SSID1 = 4014;

		public const int Attr_Wifi_Client_Key1 = 4015;

		public const int Attr_Wifi_Client_SSID2 = 4016;

		public const int Attr_Wifi_Client_Key2 = 4017;

		public const int Attr_Wifi_Client_SSID3 = 4018;

		public const int Attr_Wifi_Client_Key3 = 4019;

		public const int Attr_Wifi_Client_SSID4 = 4020;

		public const int Attr_Wifi_Client_Key4 = 4021;

		public const int Attr_Wifi_Client_SSID5 = 4022;

		public const int Attr_Wifi_Client_Key5 = 4023;

		public const int Attr_Wifi_Client_SSID6 = 4024;

		public const int Attr_Wifi_Client_Key6 = 4025;

		public const int Attr_Wifi_Client_SSID7 = 4026;

		public const int Attr_Wifi_Client_Key7 = 4027;

		public const int Attr_Wifi_Client_SSID8 = 4028;

		public const int Attr_Wifi_Client_Key8 = 4029;

		public const int Attr_Wifi_Client_SSID9 = 4030;

		public const int Attr_Wifi_Client_Key9 = 4031;

		public const int Attr_Wifi_AP_SSID_W = 4501;

		public const int Attr_Wifi_AP_Key_W = 4502;

		public const int Attr_Wifi_AP_CountryCode_W = 4503;

		public const int Attr_Wifi_AP_FrequencySel_W = 4504;

		public const int Attr_Wifi_AP_BandWidthSel_W = 4505;

		public const int Attr_Wifi_AP_ChannelSel_W = 4506;

		public const int Attr_Wifi_AP_SecuritySel_W = 4507;

		public const int Attr_Wifi_AP_ApModeEn_W = 4508;

		public const int Attr_Wifi_AP_DhcpServerEn_W = 4509;

		public const int Attr_Wifi_Client_ListNum_W = 4510;

		public const int Attr_Wifi_Client_CurrentSel_W = 4511;

		public const int Attr_Wifi_Client_SSID0_W = 4512;

		public const int Attr_Wifi_Client_Key0_W = 4513;

		public const int Attr_Wifi_Client_SSID1_W = 4514;

		public const int Attr_Wifi_Client_Key1_W = 4515;

		public const int Attr_Wifi_Client_SSID2_W = 4516;

		public const int Attr_Wifi_Client_Key2_W = 4517;

		public const int Attr_Wifi_Client_SSID3_W = 4518;

		public const int Attr_Wifi_Client_Key3_W = 4519;

		public const int Attr_Wifi_Client_SSID4_W = 4520;

		public const int Attr_Wifi_Client_Key4_W = 4521;

		public const int Attr_Wifi_Client_SSID5_W = 4522;

		public const int Attr_Wifi_Client_Key5_W = 4523;

		public const int Attr_Wifi_Client_SSID6_W = 4524;

		public const int Attr_Wifi_Client_Key6_W = 4525;

		public const int Attr_Wifi_Client_SSID7_W = 4526;

		public const int Attr_Wifi_Client_Key7_W = 4527;

		public const int Attr_Wifi_Client_SSID8_W = 4528;

		public const int Attr_Wifi_Client_Key8_W = 4529;

		public const int Attr_Wifi_Client_SSID9_W = 4530;

		public const int Attr_Wifi_Client_Key9_W = 4531;

		public const int Attr_WorkDir = 5001;

		public const int Attr_State = 5002;

		public const int Attr_ConnState = 5003;

		public const int Attr_CurrentTask = 5004;

		public const int Attr_CurrentTransaction = 5005;

		public const int Attr_FsmState = 5006;

		public const int Attr_Width = 5007;

		public const int Attr_Height = 5008;

		public const int Attr_PrevImg_Width = 5009;

		public const int Attr_PrevImg_Height = 5010;

		public const int Attr_Authority = 5011;

		public const int Attr_ConnState_CmdCh = 5012;

		public const int Attr_ConnState_ImgCh = 5013;

		public const int Attr_ElapsedExpWindowTime = 5014;

		public const int Attr_FWUpdateProgress = 5015;

		public const int Attr_ImageTransProgress = 5016;

		public const int Attr_RdResult_T1 = 5017;

		public const int Attr_RdResult_T2 = 5018;

		public const int Attr_RdResult_Humidity = 5019;

		public const int Attr_RdResult_Shock_Threshold = 5020;

		public const int Attr_CurrentSubset = 5021;

		public const int Attr_CurrentCorrectOption = 5022;

		public const int Attr_OffsetValidityState = 5023;

		public const int Attr_GainValidityState = 5024;

		public const int Attr_DefectValidityState = 5025;

		public const int Attr_LagValidityState = 5026;

		public const int Attr_GhostValidityState = 5027;

		public const int Attr_OffsetTotalFrames = 5028;

		public const int Attr_OffsetValidFrames = 5029;

		public const int Attr_GainTotalFrames = 5030;

		public const int Attr_GainValidFrames = 5031;

		public const int Attr_DefectTotalFrames = 5032;

		public const int Attr_DefectValidFrames = 5033;

		public const int Attr_LagTotalFrames = 5034;

		public const int Attr_LagValidFrames = 5035;

		public const int Attr_Battery_Exist = 5036;

		public const int Attr_Battery_Remaining = 5037;

		public const int Attr_Battery_ChargingStatus = 5038;

		public const int Attr_Battery_PowerWarnStatus = 5039;

		public const int Attr_NetworkInterface = 5040;

		public const int Attr_WifiStatu_LinkedAP = 5041;

		public const int Attr_WifiStatu_WorkingBand = 5042;

		public const int Attr_WifiStatu_WorkingSignalIntensity = 5043;

		public const int Attr_WifiStatu_WorkingLinkQuality = 5044;

		public const int Attr_WifiStatu_WorkingTxPower = 5045;

		public const int Attr_HwTmpl_Offset_Enable = 5046;

		public const int Attr_HwTmpl_Offset_ValidIndex = 5047;

		public const int Attr_HwTmpl_Offset_FileCount = 5048;

		public const int Attr_HwTmpl_Gain_Enable = 5049;

		public const int Attr_HwTmpl_Gain_ValidIndex = 5050;

		public const int Attr_HwTmpl_Gain_FileCount = 5051;

		public const int Attr_HwTmpl_MostGain_Enable = 5052;

		public const int Attr_HwTmpl_MostGain_ValidIndex = 5053;

		public const int Attr_HwTmpl_MostGain_FileCount = 5054;

		public const int Attr_HwTmpl_Defect_Enable = 5055;

		public const int Attr_HwTmpl_Defect_ValidIndex = 5056;

		public const int Attr_HwTmpl_Defect_FileCount = 5057;

		public const int Attr_HwTmpl_Lag_Enable = 5058;

		public const int Attr_HwTmpl_Lag_ValidIndex = 5059;

		public const int Attr_HwTmpl_Lag_FileCount = 5060;

		public const int Attr_CorrectionPrepared = 5061;

		public const int Attr_RdResult_OutExpState = 5062;

		public const int Attr_RdResult_AutoSleepState = 5063;

		public const int Attr_Battery_ExternalPower = 5064;

		public const int Attr_GCU_OnlineState = 5065;

		public const int Cmd_SetLogLevel = 1;

		public const int Cmd_Connect = 2;

		public const int Cmd_Disconnect = 3;

		public const int Cmd_Sleep = 4;

		public const int Cmd_Wakeup = 5;

		public const int Cmd_SetCorrectOption = 6;

		public const int Cmd_SetCaliSubset = 7;

		public const int Cmd_Reset = 8;

		public const int Cmd_Clear = 1001;

		public const int Cmd_ClearAcq = 1002;

		public const int Cmd_Acq2 = 1003;

		public const int Cmd_AecPreExp = 1016;

		public const int Cmd_StartAcq = 1004;

		public const int Cmd_StopAcq = 1005;

		public const int Cmd_ForceSingleAcq = 1006;

		public const int Cmd_ForceContinuousAcq = 1007;

		public const int Cmd_ForceDarkSingleAcq = 1008;

		public const int Cmd_ForceDarkClearAcq = 1009;

		public const int Cmd_ForceDarkContinuousAcq = 1010;

		public const int Cmd_ProhibOutExp = 1011;

		public const int Cmd_EnableOutExp = 1012;

		public const int Cmd_SyncStart = 1013;

		public const int Cmd_SyncCancel = 1014;

		public const int Cmd_SyncAcq = 1015;

		public const int Cmd_ReadUserROM = 2001;

		public const int Cmd_WriteUserROM = 2002;

		public const int Cmd_ReadUserRAM = 2030;

		public const int Cmd_WriteUserRAM = 2031;

		public const int Cmd_ReadFactoryROM = 2003;

		public const int Cmd_WriteFactoryROM = 2004;

		public const int Cmd_ReadVtMap = 2005;

		public const int Cmd_WriteVtMap = 2006;

		public const int Cmd_Recover = 2007;

		public const int Cmd_UpdateFirmware = 2008;

		public const int Cmd_SetImgChannel = 2009;

		public const int Cmd_ReadTemperature = 2010;

		public const int Cmd_ReadHumidity = 2011;

		public const int Cmd_UploadDetectorLog = 2012;

		public const int Cmd_UploadShockLog = 2013;

		public const int Cmd_ClearShockLog = 2014;

		public const int Cmd_WriteShockThreshold = 2015;

		public const int Cmd_ReadShockThreshold = 2016;

		public const int Cmd_ReadBatteryStatus = 2017;

		public const int Cmd_SetTimeByDiff = 2018;

		public const int Cmd_QueryTimeDiff = 2019;

		public const int Cmd_QueryLivingTime = 2020;

		public const int Cmd_ReadWifiStatus = 2021;

		public const int Cmd_QueryWifiScanList = 2022;

		public const int Cmd_WriteWifiSettings = 2023;

		public const int Cmd_ReadWifiSettings = 2024;

		public const int Cmd_DownloadWorkList = 2025;

		public const int Cmd_QueryHistoricalImageList = 2026;

		public const int Cmd_SelectWorkItem = 2027;

		public const int Cmd_UploadHistoricalImages = 2028;

		public const int Cmd_StopUploadingHistoricalImages = 2029;

		public const int Cmd_ChangeParamsOfCurrentAppMode = 2032;

		public const int Cmd_OffsetGeneration = 3001;

		public const int Cmd_GainInit = 3002;

		public const int Cmd_GainSelectCurrent = 3004;

		public const int Cmd_GainSelectAll = 3005;

		public const int Cmd_GainGeneration = 3006;

		public const int Cmd_DefectInit = 3007;

		public const int Cmd_LoadTemporaryDefectFile = 3008;

		public const int Cmd_DefectSelectCurrent = 3009;

		public const int Cmd_DefectSelectAll = 3033;

		public const int Cmd_DefectGeneration = 3010;

		public const int Cmd_LagInit = 3012;

		public const int Cmd_LagSelectCurrent = 3013;

		public const int Cmd_LagGeneration = 3014;

		public const int Cmd_UpdateFreqCompositeCoeff = 3032;

		public const int Cmd_FinishGenerationProcess = 3015;

		public const int Cmd_DeleteGenerationTempFile = 3016;

		public const int Cmd_DownloadCaliFile = 3017;

		public const int Cmd_UploadCaliFile = 3018;

		public const int Cmd_SelectCaliFile = 3019;

		public const int Cmd_HwGeneratePreOffsetTemplate = 3020;

		public const int Cmd_QueryHwCaliTemplateList = 3021;

		public const int Cmd_ApplyDefectCorrection = 3022;

		public const int Cmd_RequestSyncboxControl = 3023;

		public const int Cmd_ReleaseSyncboxControl = 3024;

		public const int Cmd_ReadOutExpEnableState = 3025;

		public const int Cmd_EnableAutoSleep = 3026;

		public const int Cmd_DisableAutoSleep = 3027;

		public const int Cmd_ReadAutoSleepEnableState = 3028;

		public const int Cmd_PowerOff = 3029;

		public const int Cmd_StartAutoCleanup = 3030;

		public const int Cmd_StopAutoCleanup = 3031;

		public const int Cmd_PanelRecover = 3034;

		public const int Cmd_Debug_ReadCRC = 10001;

		public const int Err_OK = 0;

		public const int Err_TaskPending = 1;

		public const int Err_Unknown = 2;

		public const int Err_DuplicatedCreation = 3;

		public const int Err_DetectorIdNotFound = 4;

		public const int Err_StateErr = 5;

		public const int Err_NotInitialized = 6;

		public const int Err_NotImplemented = 7;

		public const int Err_AccessDenied = 8;

		public const int Err_LoadDllFailed = 9;

		public const int Err_DllCreateObjFailed = 10;

		public const int Err_OpenFileFailed = 11;

		public const int Err_FileNotExist = 12;

		public const int Err_ConfigFileNotExist = 13;

		public const int Err_TemplateFileNotExist = 14;

		public const int Err_TemplateFileNotMatch = 15;

		public const int Err_InvalidFileFormat = 16;

		public const int Err_CreateLoggerFailed = 17;

		public const int Err_InvalidParamCount = 18;

		public const int Err_InvalidParamType = 19;

		public const int Err_InvalidParamValue = 20;

		public const int Err_PreCondition = 21;

		public const int Err_TaskTimeOut = 22;

		public const int Err_ProdInfoMismatch = 23;

		public const int Err_DetectorRespTimeout = 24;

		public const int Err_InvalidPacketNo = 25;

		public const int Err_InvalidPacketFormat = 26;

		public const int Err_PacketDataCheckFailed = 27;

		public const int Err_PacketLost_BufOverflow = 28;

		public const int Err_FrameLost_BufOverflow = 29;

		public const int Err_ImgChBreak = 30;

		public const int Err_BadImgQuality = 31;

		public const int Err_GeneralSocketErr = 32;

		public const int Err_DetectorSN_Mismatch = 33;

		public const int Err_CommDeviceNotFound = 34;

		public const int Err_CommDeviceOccupied = 35;

		public const int Err_CommParamNotMatch = 36;

		public const int Err_NotEnoughDiskSpace = 37;

		public const int Err_NotEnoughMemorySpace = 38;

		public const int Err_ApplyFirmwareFailed = 39;

		public const int Err_CallbackNotFinished = 40;

		public const int Err_FirmwareUpdated = 41;

		public const int Err_TooMuchDefectPoints = 42;

		public const int Err_TooLongFilePath = 43;

		public const int Err_FPD_General_Detector_Error = 1001;

		public const int Err_FPD_General_ControlBox_Error = 1002;

		public const int Err_FPD_General_FirmwareUpgrade_Error = 1003;

		public const int Err_FPD_General_GSensor_Error = 1004;

		public const int Err_FPD_NotImplemented = 1005;

		public const int Err_FPD_SeqNoOutOfSync = 1006;

		public const int Err_FPD_Busy = 1007;

		public const int Err_FPD_Busy_Initializing = 1018;

		public const int Err_FPD_Busy_Last_Command_Suspending = 1019;

		public const int Err_FPD_Busy_Mode_Not_Supported = 1020;

		public const int Err_FPD_Busy_MCU_Busy = 1021;

		public const int Err_FPD_Busy_FPGA_Busy = 1022;

		public const int Err_FPD_Busy_FPGA_Timeout = 1023;

		public const int Err_FPD_Busy_Doing_Dynamic_Ghost = 1024;

		public const int Err_FPD_Busy_Doing_Dynamic_Preoffset = 1025;

		public const int Err_FPD_Busy_FTP_Image_Uploading = 1026;

		public const int Err_FPD_Busy_Capture_State_Recover = 1027;

		public const int Err_FPD_Busy_System_Error = 1028;

		public const int Err_FPD_Occupied = 1008;

		public const int Err_FPD_SleepWakeupFailed = 1009;

		public const int Err_FPD_SleepCaptureError = 1010;

		public const int Err_FPD_CmdExecuteTimeout = 1011;

		public const int Err_FPD_FirmwareFallback = 1012;

		public const int Err_FPD_NotSupportInCurrMode = 1013;

		public const int Err_FPD_NoEnoughStorageSpace = 1014;

		public const int Err_FPD_FileNotExist = 1015;

		public const int Err_FPD_FtpServerAccessError = 1016;

		public const int Err_FPD_HWCaliFileError = 1017;

		public const int Err_FTP_OpenLoginFailed = 2001;

		public const int Err_FTP_MkdirCdFailed = 2002;

		public const int Err_FTP_LocalFileOpenFailed = 2003;

		public const int Err_FTP_UploadFailed = 2004;

		public const int Err_FTP_DownloadFailed = 2005;

		public const int Err_FTP_FileVerifyFailed = 2006;

		public const int Err_FTP_TypeError = 2007;

		public const int Err_Cali_GeneralError = 3001;

		public const int Err_Cali_UnexpectImage_DoseHighHigh = 3002;

		public const int Err_Cali_UnexpectImage_ExpLineNotSatisfy = 3003;

		public const int Err_Cali_UnexpectImage_MistakeTrigger = 3004;

		public const int Err_Cali_DataNotReadyForGen = 3005;

		public const int Err_Cali_NotEnoughIntervalTime_OffsetTmpl = 3006;

		public const int Evt_GeneralInfo = 1;

		public const int Evt_GeneralWarn = 2;

		public const int Evt_GeneralError = 3;

		public const int Evt_TaskResult_Succeed = 4;

		public const int Evt_TaskResult_Failed = 5;

		public const int Evt_TaskResult_Canceled = 6;

		public const int Evt_AutoTask_Started = 7;

		public const int Evt_ScanOnce_Request = 8;

		public const int Evt_ImageFileUpload_Result = 9;

		public const int Evt_TemplateFileUpload_Result = 10;

		public const int Evt_TemplateFileDownload_Result = 11;

		public const int Evt_HwCaliTemplateList = 12;

		public const int Evt_HwWiFiScanList = 13;

		public const int Evt_HistoricalImageList = 14;

		public const int Evt_TimeDiff = 15;

		public const int Evt_LivingTime = 16;

		public const int Evt_TransactionAborted = 17;

		public const int Evt_Image = 1001;

		public const int Evt_Prev_Image = 1002;

		public const int Evt_Retransfer_Image = 1012;

		public const int Evt_WaitImage_Timeout = 1003;

		public const int Evt_Exp_Prohibit = 1004;

		public const int Evt_Exp_Enable = 1005;

		public const int Evt_Exp_Timeout = 1006;

		public const int Evt_OffsetAmassingTime = 1008;

		public const int Evt_MistakenTrigger = 1009;

		public const int Evt_Image_Trans_Slow = 1010;

		public const int Evt_Image_Abandon = 1011;

		public const int Evt_ConnectProcess = 2001;

		public const int Evt_CommFailure = 2002;

		public const int Evt_TemperatureHigh = 2003;

		public const int Evt_FpsOutOfRange = 2004;

		public const int Evt_LowBattery = 2005;

		public const int Evt_TemplateOverDue = 2006;

		public const int IRAY_MAX_STR_LEN = 512;

		public const int IRAY_MAX_NAME_LEN = 128;

		public const int IRAY_MAX_DESC_LEN = 256;

		public static readonly Dictionary<int, string> EventTable = new Dictionary<int, string>
		{
			{
				0,
				"Evt_NotDefine"
			},
			{
				1,
				"Evt_GeneralInfo"
			},
			{
				2,
				"Evt_GeneralWarn"
			},
			{
				3,
				"Evt_GeneralError"
			},
			{
				4,
				"Evt_TaskResult_Succeed"
			},
			{
				5,
				"Evt_TaskResult_Failed"
			},
			{
				6,
				"Evt_TaskResult_Canceled"
			},
			{
				7,
				"Evt_AutoTask_Started"
			},
			{
				8,
				"Evt_ScanOnce_Request"
			},
			{
				9,
				"Evt_ImageFileUpload_Result"
			},
			{
				10,
				"Evt_TemplateFileUpload_Result"
			},
			{
				11,
				"Evt_TemplateFileDownload_Result"
			},
			{
				12,
				"Evt_HwCaliTemplateList"
			},
			{
				13,
				"Evt_HwWiFiScanList"
			},
			{
				14,
				"Evt_HistoricalImageList"
			},
			{
				15,
				"Evt_TimeDiff"
			},
			{
				16,
				"Evt_LivingTime"
			},
			{
				17,
				"Evt_TransactionAborted"
			},
			{
				1001,
				"Evt_Image"
			},
			{
				1002,
				"Evt_Prev_Image"
			},
			{
				1012,
				"Evt_Retransfer_Image"
			},
			{
				1003,
				"Evt_WaitImage_Timeout"
			},
			{
				1004,
				"Evt_Exp_Prohibit"
			},
			{
				1005,
				"Evt_Exp_Enable"
			},
			{
				1006,
				"Evt_Exp_Timeout"
			},
			{
				1008,
				"Evt_OffsetAmassingTime"
			},
			{
				1009,
				"Evt_MistakenTrigger"
			},
			{
				1010,
				"Evt_Image_Trans_Slow"
			},
			{
				1011,
				"Evt_Image_Abandon"
			},
			{
				2001,
				"Evt_ConnectProcess"
			},
			{
				2002,
				"Evt_CommFailure"
			},
			{
				2003,
				"Evt_TemperatureHigh"
			},
			{
				2004,
				"Evt_FpsOutOfRange"
			},
			{
				2005,
				"Evt_LowBattery"
			},
			{
				2006,
				"Evt_TemplateOverDue"
			}
		};

		public static readonly Dictionary<int, string> CommandIDTable = new Dictionary<int, string>
		{
			{
				0,
				"NotDefine"
			},
			{
				1,
				"SetLogLevel"
			},
			{
				2,
				"Connect"
			},
			{
				3,
				"Disconnect"
			},
			{
				4,
				"Sleep"
			},
			{
				5,
				"Wakeup"
			},
			{
				6,
				"SetCorrectOption"
			},
			{
				7,
				"SetCaliSubset"
			},
			{
				8,
				"Reset"
			},
			{
				1001,
				"Clear"
			},
			{
				1002,
				"ClearAcq"
			},
			{
				1003,
				"Acq2"
			},
			{
				1016,
				"AecPreExp"
			},
			{
				1004,
				"StartAcq"
			},
			{
				1005,
				"StopAcq"
			},
			{
				1006,
				"ForceSingleAcq"
			},
			{
				1007,
				"ForceContinuousAcq"
			},
			{
				1008,
				"ForceDarkSingleAcq"
			},
			{
				1009,
				"ForceDarkClearAcq"
			},
			{
				1010,
				"ForceDarkContinuousAcq"
			},
			{
				1011,
				"ProhibOutExp"
			},
			{
				1012,
				"EnableOutExp"
			},
			{
				1013,
				"SyncStart"
			},
			{
				1014,
				"SyncCancel"
			},
			{
				1015,
				"SyncAcq"
			},
			{
				2001,
				"ReadUserROM"
			},
			{
				2002,
				"WriteUserROM"
			},
			{
				2030,
				"ReadUserRAM"
			},
			{
				2031,
				"WriteUserRAM"
			},
			{
				2003,
				"ReadFactoryROM"
			},
			{
				2004,
				"WriteFactoryROM"
			},
			{
				2005,
				"ReadVtMap"
			},
			{
				2006,
				"WriteVtMap"
			},
			{
				2007,
				"Recover"
			},
			{
				2008,
				"UpdateFirmware"
			},
			{
				2009,
				"SetImgChannel"
			},
			{
				2010,
				"ReadTemperature"
			},
			{
				2011,
				"ReadHumidity"
			},
			{
				2012,
				"UploadDetectorLog"
			},
			{
				2013,
				"UploadShockLog"
			},
			{
				2014,
				"ClearShockLog"
			},
			{
				2015,
				"WriteShockThreshold"
			},
			{
				2016,
				"ReadShockThreshold"
			},
			{
				2017,
				"ReadBatteryStatus"
			},
			{
				2018,
				"SetTimeByDiff"
			},
			{
				2019,
				"QueryTimeDiff"
			},
			{
				2020,
				"QueryLivingTime"
			},
			{
				2021,
				"ReadWifiStatus"
			},
			{
				2022,
				"QueryWifiScanList"
			},
			{
				2023,
				"WriteWifiSettings"
			},
			{
				2024,
				"ReadWifiSettings"
			},
			{
				2025,
				"DownloadWorkList"
			},
			{
				2026,
				"QueryHistoricalImageList"
			},
			{
				2027,
				"SelectWorkItem"
			},
			{
				2028,
				"UploadHistoricalImages"
			},
			{
				2029,
				"StopUploadingHistoricalImages"
			},
			{
				3001,
				"OffsetGeneration"
			},
			{
				3002,
				"GainInit"
			},
			{
				3004,
				"GainSelectCurrent"
			},
			{
				3005,
				"GainSelectAll"
			},
			{
				3006,
				"GainGeneration"
			},
			{
				3007,
				"DefectInit"
			},
			{
				3008,
				"LoadTemporaryDefectFile"
			},
			{
				3009,
				"DefectSelectCurrent"
			},
			{
				3010,
				"DefectGeneration"
			},
			{
				3012,
				"LagInit"
			},
			{
				3013,
				"LagSelectCurrent"
			},
			{
				3014,
				"LagGeneration"
			},
			{
				3032,
				"UpdateFreqCompositeCoeff"
			},
			{
				3015,
				"FinishGenerationProcess"
			},
			{
				3016,
				"DeleteGenerationTempFile"
			},
			{
				3017,
				"DownloadCaliFile"
			},
			{
				3018,
				"UploadCaliFile"
			},
			{
				3019,
				"SelectCaliFile"
			},
			{
				3020,
				"HwGeneratePreOffsetTemplate"
			},
			{
				3021,
				"QueryHwCaliTemplateList"
			},
			{
				3022,
				"ApplyDefectCorrection"
			},
			{
				3023,
				"RequestSyncboxControl"
			},
			{
				3024,
				"ReleaseSyncboxControl"
			},
			{
				3025,
				"ReadOutExpEnableState"
			},
			{
				3026,
				"EnableAutoSleep"
			},
			{
				3027,
				"DisableAutoSleep"
			},
			{
				3028,
				"ReadAutoSleepEnableState"
			},
			{
				3029,
				"PowerOff"
			},
			{
				3034,
				"RecoverPanel"
			},
			{
				3030,
				"StartAutoCleanup"
			},
			{
				3031,
				"StopAutoCleanup"
			},
			{
				10001,
				"Debug_ReadCRC"
			}
		};

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetProdInfoEx(int nProdNo, ref ProdInfoEx pInfo);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetAuthority(ref int nAuthority);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetAttrsCount(int nDetectorID, ref int pCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetAttrIDList(int nDetectorID, int[] pIDList, int nCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetAttrInfo(int nDetectorID, int nAttrID, ref AttrInfo pInfo);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetCommandCount(int nDetectorID, ref int pCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetCommandInfoList(int nDetectorID, IntPtr pCmdInfoList, int nCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetCmdParamCount(int nDetectorID, int nCmdID, ref int pCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetCmdParamInfo(int nDetectorID, int nCmdID, IntPtr pCmdParamInfoList, int nCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetProdCount(ref int pCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetProdList(IntPtr pProdList, int nCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetErrInfo(int nErrorCode, ref ErrorInfo pInfo);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetEnumItemsCount([MarshalAs(UnmanagedType.LPStr)] string strEnumTypeName, ref int pCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetEnumItemList([MarshalAs(UnmanagedType.LPStr)] string strEnumTypeName, IntPtr pEnumItemList, int nCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetSDKVersion(StringBuilder szVersion);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SetUserCode([MarshalAs(UnmanagedType.LPStr)] string strUserCode);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int RegisterScanNotify(ScanCallbackHandler fpCallback);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int ScanOnce([MarshalAs(UnmanagedType.LPStr)] string strLocalIP);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int Create([MarshalAs(UnmanagedType.LPStr)] string strWorkDir, SdkCallbackHandler fpCallback, ref int pDetectorID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int Destroy(int nDetectorID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int GetAttr(int nDetectorID, int nAttrID, ref IRayVariant pVar);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SetAttr(int nDetectorID, int nAttrID, ref IRayVariant pVar);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int Invoke(int nDetectorID, int nCommandID, IRayCmdParam[] pars, int nParCount);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int Abort(int nDetectorID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int OpenDefectTemplateFile([MarshalAs(UnmanagedType.LPStr)] string strFilePath, ref IntPtr ppHandler, ref ushort usWidth, ref ushort usHeight, ref IntPtr ppPoints, ref IntPtr ppRows, ref IntPtr ppCols, ref IntPtr ppDualReadCols2);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int CalcDefectWithImageData(IntPtr pData, ushort usWidth, ushort usHeight, ushort usDummyTop, ushort usDummyBottom, ushort usDummyLeft, ushort usDummyRight, IntPtr pPoints, IntPtr pRows, IntPtr pCols);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int TestCollimatorWithImageData(IntPtr pData, ushort usWidth, ushort usHeight, ushort usDummyTop, ushort usDummyBottom, ushort usDummyLeft, ushort usDummyRight, ref bool bResult);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SaveDefectTemplateFile(IntPtr pHandler);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int CloseDefectTemplateFile(IntPtr pHandler);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int DoExpLineCorrect(IntPtr pHandler, ushort usWidth, ushort usHeight, ushort usExpLine);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxCreate(int nComPort, ref int pSyncboxID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxDestroy(int nSyncboxID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxBind(int nSyncboxID, int nDetectorID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxGetBind(int nSyncboxID, ref int pDetectorID);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxGetTubeReadyTime(int nSyncboxID, ref int pTimeInMS);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxSetTubeReadyTime(int nSyncboxID, int nTimeInMS);

		[DllImport("FpdSys.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int SyncBoxGetState(int nSyncboxID, ref int pState);

		public static int SetAttr(int nDetectorID, int nAttrID, int nValue)
		{
			IRayVariant pVar = default(IRayVariant);
			pVar.vt = IRAY_VAR_TYPE.IVT_INT;
			pVar.val.nVal = nValue;
			return SetAttr(nDetectorID, nAttrID, ref pVar);
		}

		public static int SetAttr(int nDetectorID, int nAttrID, float fValue)
		{
			IRayVariant pVar = default(IRayVariant);
			pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
			pVar.val.fVal = fValue;
			return SetAttr(nDetectorID, nAttrID, ref pVar);
		}

		public static int SetAttr(int nDetectorID, int nAttrID, string strValue)
		{
			IRayVariant pVar = default(IRayVariant);
			pVar.vt = IRAY_VAR_TYPE.IVT_STR;
			pVar.val.strVal = strValue;
			return SetAttr(nDetectorID, nAttrID, ref pVar);
		}
	}
}
