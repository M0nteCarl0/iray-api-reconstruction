using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayImage
	{
		public int nWidth;

		public int nHeight;

		public int nBytesPerPixel;

		public IntPtr pData;

		public IRayVariantMap propList;
	}
}
