using System.Runtime.InteropServices;

namespace iDetector
{
	public struct DicomInfo
	{
		public int nWidth;

		public int nHeight;

		public int nWindowWidth;

		public int nWindowCenter;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strPatientName;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strPatientID;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strPatientBirthDate;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strPatientSex;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strStudyID;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strStudyDate;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strImagerPixelSpacing;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strModality;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strPixelSpacing;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strStudyInstanceUID;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strSeriesInstanceUID;

		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 512)]
		public char[] strSOPInstanceUID;
	}
}
