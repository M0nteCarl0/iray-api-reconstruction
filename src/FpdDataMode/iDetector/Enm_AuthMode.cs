namespace iDetector
{
	public enum Enm_AuthMode
	{
		Enm_AuthMode_Null,
		Enm_AuthMode_PCID,
		Enm_AuthMode_UserCode,
		Enm_AuthMode_DetSN
	}
}
