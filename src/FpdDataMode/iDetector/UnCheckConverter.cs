using System;
using System.Globalization;
using System.Windows.Data;

namespace iDetector
{
	public class UnCheckConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null)
			{
				return false;
			}
			if ((bool?)value == true)
			{
				return false;
			}
			return true;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return null;
		}
	}
}
