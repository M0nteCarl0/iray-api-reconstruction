using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct EnumItem
	{
		public int nVal;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strName;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strDescription;
	}
}
