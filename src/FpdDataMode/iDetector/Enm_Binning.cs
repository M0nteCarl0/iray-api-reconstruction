namespace iDetector
{
	public enum Enm_Binning
	{
		Enm_Binning_Null,
		Enm_Binning_2x2,
		Enm_Binning_3x3,
		Enm_Binning_4x4
	}
}
