namespace iDetector
{
	public enum Enm_EventLevel
	{
		Enm_EventLevel_Info,
		Enm_EventLevel_Warn,
		Enm_EventLevel_Error,
		Enm_EventLevel_Notify
	}
}
