namespace iDetector
{
	public enum Enm_FreesyncSubFlow
	{
		Enm_FreesyncSubFlow_Reserved,
		Enm_FreesyncSubFlow_NFNR,
		Enm_FreesyncSubFlow_FFNR,
		Enm_FreesyncSubFlow_3,
		Enm_FreesyncSubFlow_4
	}
}
