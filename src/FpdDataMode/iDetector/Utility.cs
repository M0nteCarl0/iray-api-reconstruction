using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class Utility
	{
		[DllImport("kernel32")]
		public static extern uint GlobalMemoryStatusEx(ref MEMORYSTATUSEX meminfo);

		[DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
		private unsafe static extern int memcpy(void* pDst, void* pSrc, int count);

		public static ulong GetAvailPhysMemory()
		{
			MEMORYSTATUSEX meminfo = default(MEMORYSTATUSEX);
			meminfo.dwLength = (uint)Marshal.SizeOf((object)meminfo);
			GlobalMemoryStatusEx(ref meminfo);
			Log.Instance().Write("GetAvailPhysMemory:{0}", meminfo.ullAvailPhys);
			return meminfo.ullAvailPhys;
		}

		public static long GetDiskFreeSpace(string disk)
		{
			DriveInfo.GetDrives();
			DriveInfo[] drives = DriveInfo.GetDrives();
			foreach (DriveInfo driveInfo in drives)
			{
				if (driveInfo.Name == disk)
				{
					return driveInfo.AvailableFreeSpace / 1024 / 1024;
				}
			}
			return 0L;
		}

		public static string GetDiskName(string path)
		{
			return Directory.GetDirectoryRoot(path);
		}

		public static string GetCurrentPath()
		{
			return Directory.GetCurrentDirectory();
		}

		public static bool IsFileCanbeWritten(string filePath)
		{
			bool result = false;
			try
			{
				bool flag = IsFileExisted(filePath);
				FileStream fileStream = File.OpenWrite(filePath);
				fileStream.Close();
				if (!flag)
				{
					DeleteFile(filePath);
				}
				result = true;
				return result;
			}
			catch
			{
				return result;
			}
		}

		public static bool IsFileExisted(string filePath)
		{
			return File.Exists(filePath);
		}

		public static void DeleteFile(string path)
		{
			try
			{
				if (IsFileExisted(path))
				{
					File.Delete(path);
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("DeleteFile failed! Err:" + ex.Message);
			}
		}

		public static bool IsDirectoryExisted(string dirPath)
		{
			if (dirPath == null)
			{
				return false;
			}
			return Directory.Exists(dirPath);
		}

		public static bool CreateDirectory(string dirPath)
		{
			if (dirPath == null)
			{
				return false;
			}
			try
			{
				Directory.CreateDirectory(dirPath);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static void DeleteDirectory(string path)
		{
			if (IsDirectoryExisted(path))
			{
				Directory.Delete(path, true);
			}
		}

		public static void CopyFolders(string srcPath, string dstPath)
		{
			if (srcPath != null && dstPath != null)
			{
				try
				{
					if (dstPath[dstPath.Length - 1] != Path.DirectorySeparatorChar)
					{
						dstPath += Path.DirectorySeparatorChar;
					}
					if (!IsDirectoryExisted(dstPath))
					{
						CreateDirectory(dstPath);
					}
					string[] fileSystemEntries = Directory.GetFileSystemEntries(srcPath);
					string[] array = fileSystemEntries;
					foreach (string text in array)
					{
						if (IsDirectoryExisted(text))
						{
							CopyFolders(text, dstPath + Path.GetFileName(text));
						}
						else
						{
							File.Copy(text, dstPath + Path.GetFileName(text), true);
						}
					}
				}
				catch (Exception ex)
				{
					ex.ToString();
				}
			}
		}

		public static long GetFileSize(string filePath)
		{
			try
			{
				FileStream fileStream = File.OpenRead(filePath);
				long length = fileStream.Length;
				fileStream.Close();
				return length;
			}
			catch
			{
				return 0L;
			}
		}

		public static int GetAverageValue(byte[] data)
		{
			if (data == null)
			{
				return 0;
			}
			ulong num = 0uL;
			int num2 = data.Length / 2;
			for (int i = 0; i < num2; i++)
			{
				num += BitConverter.ToUInt16(data, i * 2);
			}
			return (int)(num / (ulong)num2);
		}

		public unsafe static bool Memcopy(byte[] srcData, ushort[] dstData)
		{
			if (srcData == null || dstData == null || srcData.Length != dstData.Length * 2)
			{
				return false;
			}
			try
			{
				try
				{
					fixed (IntPtr* pDst = (IntPtr*)(&dstData[0]))
					{
						try
						{
							fixed (IntPtr* pSrc = (IntPtr*)(&srcData[0]))
							{
								memcpy(pDst, pSrc, srcData.Length);
							}
						}
						finally
						{
						}
					}
				}
				finally
				{
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Alloc memory error:{0}", ex.ToString());
				return false;
			}
			return true;
		}

		public unsafe static bool Memcopy(short[] srcData, byte[] dstData)
		{
			if (srcData == null || dstData == null || dstData.Length != srcData.Length * 2)
			{
				return false;
			}
			try
			{
				try
				{
					fixed (IntPtr* pDst = (IntPtr*)(&dstData[0]))
					{
						try
						{
							fixed (IntPtr* pSrc = (IntPtr*)(&srcData[0]))
							{
								memcpy(pDst, pSrc, dstData.Length);
							}
						}
						finally
						{
						}
					}
				}
				finally
				{
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Alloc memory error:{0}", ex.ToString());
				return false;
			}
			return true;
		}

		public unsafe static bool Memcopy(ushort[] srcData, byte[] dstData)
		{
			if (srcData == null || dstData == null || dstData.Length != srcData.Length * 2)
			{
				return false;
			}
			try
			{
				try
				{
					fixed (IntPtr* pDst = (IntPtr*)(&dstData[0]))
					{
						try
						{
							fixed (IntPtr* pSrc = (IntPtr*)(&srcData[0]))
							{
								memcpy(pDst, pSrc, dstData.Length);
							}
						}
						finally
						{
						}
					}
				}
				finally
				{
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Utility-Memcopy failed: " + ex.Message);
				return false;
			}
			return true;
		}

		public unsafe static bool Memcopy(ushort[] srcData, ushort[] dstData)
		{
			if (srcData == null || dstData == null || dstData.Length != srcData.Length)
			{
				return false;
			}
			try
			{
				try
				{
					fixed (IntPtr* pDst = (IntPtr*)(&dstData[0]))
					{
						try
						{
							fixed (IntPtr* pSrc = (IntPtr*)(&srcData[0]))
							{
								memcpy(pDst, pSrc, dstData.Length * 2);
							}
						}
						finally
						{
						}
					}
				}
				finally
				{
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Utility-Memcopy failed: " + ex.Message);
				return false;
			}
			return true;
		}

		public unsafe static bool Memcopy(short[] srcData, ushort[] dstData)
		{
			if (srcData == null || dstData == null || dstData.Length != srcData.Length)
			{
				return false;
			}
			try
			{
				try
				{
					fixed (IntPtr* pDst = (IntPtr*)(&dstData[0]))
					{
						try
						{
							fixed (IntPtr* pSrc = (IntPtr*)(&srcData[0]))
							{
								memcpy(pDst, pSrc, dstData.Length * 2);
							}
						}
						finally
						{
						}
					}
				}
				finally
				{
				}
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Utility-Memcopy failed: " + ex.Message);
				return false;
			}
			return true;
		}

		public static void CollectMemory<T>(ref T[] obj)
		{
			obj = null;
			GC.Collect();
		}

		public static bool NeedReallocMemory(Array obj, int newLength)
		{
			if (obj == null || obj.Length != newLength)
			{
				return true;
			}
			return false;
		}

		public static void ReallocMemory<T>(ref T[] obj, int newLength)
		{
			if (obj == null || obj.Length != newLength)
			{
				obj = new T[newLength];
			}
		}

		public static bool IsSameDirPath(string dir1, string dir2)
		{
			string fullPath = Path.GetFullPath(dir1);
			string fullPath2 = Path.GetFullPath(dir2);
			fullPath = (('\\' == fullPath[fullPath.Length - 1]) ? fullPath.Substring(0, fullPath.Length - 1) : fullPath);
			fullPath2 = (('\\' == fullPath2[fullPath2.Length - 1]) ? fullPath2.Substring(0, fullPath2.Length - 1) : fullPath2);
			if (fullPath.Equals(fullPath2, StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}
			return false;
		}

		public static string AddFileNameExtension(string fileName, string extension)
		{
			string result = fileName;
			string text = fileName.Substring(fileName.LastIndexOf(".") + 1, fileName.Length - fileName.LastIndexOf(".") - 1);
			if (text.Length == 0 || !text.ToLower().Equals(extension))
			{
				result = fileName + "." + extension;
			}
			return result;
		}

		public static void RunCmd(string cmd)
		{
			cmd = cmd.Trim().TrimEnd('&') + "&exit";
			using (Process process = new Process())
			{
				process.StartInfo.FileName = "cmd.exe";
				process.StartInfo.UseShellExecute = false;
				process.StartInfo.RedirectStandardInput = true;
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.RedirectStandardError = true;
				process.StartInfo.CreateNoWindow = true;
				process.Start();
				process.StandardInput.WriteLine(cmd);
				process.StandardInput.AutoFlush = true;
				process.StandardOutput.ReadToEnd();
				process.WaitForExit();
				process.Close();
			}
		}
	}
}
