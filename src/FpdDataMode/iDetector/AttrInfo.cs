using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct AttrInfo
	{
		public int nAttrID;

		public IRAY_VAR_TYPE nDataType;

		public int bIsConfigItem;

		public int bIsWritable;

		public int bIsEnum;

		public int nPrecision;

		public float fMinValue;

		public float fMaxValue;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strPath;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strName;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strDisplayName;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strUnit;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strDescription;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
		public string strEnumTypeName;

		public PARAM_VALIDATOR eValidator;
	}
}
