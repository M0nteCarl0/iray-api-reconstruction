using System.Runtime.InteropServices;

namespace iDetector
{
	public class IDicom
	{
		[DllImport("IDicom.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int DCM_SaveDicomFile(string strFullPathFile, string strDcmInfoPathFile, ushort[] pImageData, int iWidth, int iHeight);

		[DllImport("IDicom.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int DCM_LoadDicomFileInfo(string strFullPathDicomFile, ref DicomInfo dicomInfo);

		[DllImport("IDicom.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int DCM_GetDicomImage(string strFullPathDicomFile, byte[] pImageData, int nWidth, int nHeight);
	}
}
