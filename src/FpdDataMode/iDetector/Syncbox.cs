namespace iDetector
{
	public class Syncbox
	{
		private int nSyncboxID;

		~Syncbox()
		{
		}

		public int SyncboxConncet()
		{
			return SdkInterface.SyncBoxCreate(0, ref nSyncboxID);
		}

		public int SyncboxDisconncet()
		{
			return SdkInterface.SyncBoxDestroy(nSyncboxID);
		}

		public int SyncBoxWrite(int nTime)
		{
			return SdkInterface.SyncBoxSetTubeReadyTime(nSyncboxID, nTime);
		}

		public int SyncBoxBind(int nBind)
		{
			return SdkInterface.SyncBoxBind(nSyncboxID, nBind);
		}

		public int SyncBoxState(ref int nState)
		{
			return SdkInterface.SyncBoxGetState(nSyncboxID, ref nState);
		}

		public int SyncBoxRead(ref int nTime)
		{
			return SdkInterface.SyncBoxGetTubeReadyTime(nSyncboxID, ref nTime);
		}

		public int SyncBoxBindId(ref int nState)
		{
			return SdkInterface.SyncBoxGetBind(nSyncboxID, ref nState);
		}
	}
}
