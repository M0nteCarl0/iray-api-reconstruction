using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace iDetector
{
	public class FpdMgr
	{
		private List<Detector> _detectors;

		private List<DetectorProfile> _profiles;

		private SdkInterface.ScanCallbackHandler _scanHandler;

		private SdkInterface.SdkCallbackHandler _sdkHandler;

		private DateTime _scanStartTime;

		public List<Detector> Detectors
		{
			get
			{
				return _detectors;
			}
		}

		public List<DetectorProfile> ScanProfiles
		{
			get
			{
				return _profiles;
			}
		}

		public event EventHandler ScanResultNotify;

		public FpdMgr()
		{
			_detectors = new List<Detector>();
			_profiles = new List<DetectorProfile>();
			_sdkHandler = OnSdkCallback;
			_scanHandler = OnScanResultCallback;
			SdkInterface.RegisterScanNotify(null);
		}

		public string GetVersion()
		{
			StringBuilder stringBuilder = new StringBuilder(64);
			SdkInterface.GetSDKVersion(stringBuilder);
			return stringBuilder.ToString();
		}

		public int StartScan()
		{
			_scanStartTime = DateTime.Now;
			_profiles.Clear();
			return SdkInterface.ScanOnce(null);
		}

		public int Create(string strWorkDir, ref int nDetectorID)
		{
			int num = 0;
			try
			{
				num = SdkInterface.Create(strWorkDir, _sdkHandler, ref nDetectorID);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			if (num == 0)
			{
				Detector item = new Detector(nDetectorID);
				_detectors.Add(item);
				return 0;
			}
			return num;
		}

		public int Destroy(int nDetectorID)
		{
			Detector detector = FindDetector(nDetectorID);
			if (detector == null)
			{
				return 2;
			}
			detector.Close();
			_detectors.Remove(detector);
			try
			{
				return SdkInterface.Destroy(nDetectorID);
			}
			catch
			{
				return 0;
			}
		}

		public Detector FindDetector(int nDetectorID)
		{
			foreach (Detector detector in _detectors)
			{
				if (detector.ID == nDetectorID)
				{
					return detector;
				}
			}
			return null;
		}

		public void DoCheckAttrValChanging()
		{
			foreach (Detector detector in _detectors)
			{
				detector.AttrChangingMonitor.Update(true);
			}
		}

		private void OnScanResultCallback(IntPtr pProfile)
		{
			DetectorProfile detectorProfile = default(DetectorProfile);
			detectorProfile = (DetectorProfile)Marshal.PtrToStructure(pProfile, typeof(DetectorProfile));
			if (DateTime.Now - _scanStartTime > TimeSpan.FromMilliseconds(2000.0))
			{
				_profiles.Insert(0, detectorProfile);
			}
			if (this.ScanResultNotify != null)
			{
				this.ScanResultNotify(this, null);
			}
		}

		private void OnSdkCallback(int nDetectorID, int nEventID, int nEventLevel, IntPtr pszMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			Detector detector = FindDetector(nDetectorID);
			if (detector != null)
			{
				try
				{
					detector.OnSdkCallback(nDetectorID, nEventID, nEventLevel, pszMsg, nParam1, nParam2, nPtrParamLen, pParam);
				}
				catch (Exception ex)
				{
					Log.Instance().Write("SDKCallback failed!:{0}", ex.Message);
				}
			}
		}
	}
}
