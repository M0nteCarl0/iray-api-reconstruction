using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct IRayCmdParam
	{
		public IRAY_PARAM_TYPE pt;

		public IRayVariant var;

		public IRayDataBlock blc;
	}
}
