using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class IRayImageData
	{
		public int nWidth;

		public int nHeight;

		public int nBytesPerPixel;

		public byte[] ImgData;

		public IRayVariantMapItem[] Params;

		public IRayImageData()
		{
		}

		public IRayImageData(byte[] data, int _nWidth, int _nHeight, int _nBps = 2)
		{
			if (data != null && data.Length == _nWidth * _nHeight * _nBps)
			{
				ImgData = new byte[data.Length];
				nWidth = _nWidth;
				nHeight = _nHeight;
				nBytesPerPixel = _nBps;
				Array.Copy(data, ImgData, ImgData.Length);
			}
		}

		public IRayImageData(int _nWidth, int _nHeight, int _nBps = 2)
		{
			nWidth = _nWidth;
			nHeight = _nHeight;
			nBytesPerPixel = _nBps;
			ImgData = new byte[_nWidth * _nHeight * _nBps];
		}

		public IRayImageData(ref IRayImage image)
		{
			Write(ref image);
		}

		public IRayImageData(ref IRayImageData other)
		{
			Clone(ref other);
		}

		public void Clone(ref IRayImageData other)
		{
			if (this != other)
			{
				nWidth = other.nWidth;
				nHeight = other.nHeight;
				nBytesPerPixel = other.nBytesPerPixel;
				if (ImgData == null || ImgData.Length != other.ImgData.Length)
				{
					ImgData = new byte[other.ImgData.Length];
				}
				Array.Copy(other.ImgData, 0, ImgData, 0, ImgData.Length);
				if (Params == null || Params.Length != other.Params.Length)
				{
					Params = new IRayVariantMapItem[other.Params.Length];
				}
				Array.Copy(other.Params, 0, Params, 0, Params.Length);
			}
		}

		public void Write(ref IRayImage image)
		{
			nWidth = image.nWidth;
			nHeight = image.nHeight;
			nBytesPerPixel = image.nBytesPerPixel;
			int num = nWidth * nHeight * nBytesPerPixel;
			if (num != 0 && IntPtr.Zero != image.pData)
			{
				if (ImgData == null || ImgData.Length != num)
				{
					ImgData = new byte[num];
				}
				try
				{
					Marshal.Copy(image.pData, ImgData, 0, num);
				}
				catch (Exception ex)
				{
					Log.Instance().Write("IRayImageData write error:{0}", ex.Message);
				}
			}
			if (image.propList.nItemCount > 0)
			{
				if (Params == null || Params.Length != image.propList.nItemCount)
				{
					Params = new IRayVariantMapItem[image.propList.nItemCount];
				}
				SdkParamConvertor<IRayVariantMapItem>.IntPtrToStructArray(image.propList.pItems, ref Params);
			}
		}

		public void ConvertIRayImage(ref IRayImage image)
		{
			nWidth = image.nWidth;
			nHeight = image.nHeight;
			nBytesPerPixel = image.nBytesPerPixel;
			int num = nWidth * nHeight * nBytesPerPixel;
			if (num != 0)
			{
				try
				{
					if (ImgData == null || ImgData.Length != num)
					{
						ImgData = new byte[num];
					}
					Marshal.Copy(image.pData, ImgData, 0, num);
				}
				catch (Exception ex)
				{
					Log.Instance().Write("ConvertIRayImage failed: " + ex.Message);
				}
			}
			if (image.propList.nItemCount > 0)
			{
				if (Params == null || Params.Length != image.propList.nItemCount)
				{
					Params = new IRayVariantMapItem[image.propList.nItemCount];
				}
				SdkParamConvertor<IRayVariantMapItem>.IntPtrToStructArray(image.propList.pItems, ref Params);
			}
		}

		public static int GetImageAttr(Enm_ImageTag tagId, ref IRayVariantMapItem[] Params)
		{
			if (Params == null)
			{
				return 0;
			}
			IRayVariantMapItem[] array = Params;
			for (int i = 0; i < array.Length; i++)
			{
				IRayVariantMapItem rayVariantMapItem = array[i];
				if (rayVariantMapItem.nMapKey == (int)tagId)
				{
					return rayVariantMapItem.varMapVal.val.nVal;
				}
			}
			return 0;
		}

		public int GetFrameNo()
		{
			if (Params == null)
			{
				return 0;
			}
			IRayVariantMapItem[] @params = Params;
			for (int i = 0; i < @params.Length; i++)
			{
				IRayVariantMapItem rayVariantMapItem = @params[i];
				if (rayVariantMapItem.nMapKey == 32769)
				{
					return rayVariantMapItem.varMapVal.val.nVal;
				}
			}
			return 0;
		}
	}
}
