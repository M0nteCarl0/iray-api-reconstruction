namespace iDetector
{
	public enum Enm_CaliDataState
	{
		Enm_CaliDataState_NoData,
		Enm_CaliDataState_Valid,
		Enm_CaliDataState_ValidWarn,
		Enm_CaliDataState_OutOfDate,
		Enm_CaliDataState_ParamMisMatch
	}
}
