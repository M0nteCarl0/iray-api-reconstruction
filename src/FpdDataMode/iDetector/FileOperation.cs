using Cinema;
using System;
using System.IO;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class FileOperation
	{
		private IntPtr DefectFileHandle;

		private IntPtr DataHandle;

		private IntPtr RowHandle;

		private IntPtr ColHandle;

		private IntPtr DualReadColHandle;

		private ushort mDefectWidth;

		private ushort mDefectHeight;

		~FileOperation()
		{
			CloseDefectFile();
		}

		public void InitFile()
		{
		}

		public void DisposeFile()
		{
		}

		public static void SaveDcmFile(string path, IRayImageData image)
		{
			if (image != null && image.ImgData != null && path != null)
			{
				ushort[] array = new ushort[image.nWidth * image.nHeight];
				Utility.Memcopy(image.ImgData, array);
				IDicom.DCM_SaveDicomFile(path, null, array, image.nWidth, image.nHeight);
				array = null;
				GC.Collect();
			}
		}

		public static void SaveRawFile(string path, byte[] data, int size)
		{
			if (data != null && data.Length == size && path != null)
			{
				FileStream fileStream = File.Open(path, FileMode.Create, FileAccess.Write, FileShare.Read);
				fileStream.Write(data, 0, size);
				fileStream.Close();
			}
		}

		public static void SaveTiffFile(string path, IRayImage image)
		{
			if (path != null)
			{
				IntPtr pMng = TiffParser.CreateImageMng();
				IntPtr intPtr = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IRayImage)));
				Marshal.StructureToPtr((object)image, intPtr, true);
				if (TiffParser.TiffOpen(pMng, Marshal.StringToCoTaskMemAnsi(path), 1))
				{
					TiffParser.TiffWriteImage(pMng, intPtr);
					TiffParser.TiffClose(pMng);
				}
				Marshal.FreeHGlobal(intPtr);
				TiffParser.ReleaseImageMng(pMng);
			}
		}

		public byte[] LoadDefectFile(string path, ref ushort nWidth, ref ushort nHeight)
		{
			if (path == null)
			{
				return null;
			}
			DataHandle = default(IntPtr);
			DefectFileHandle = default(IntPtr);
			if (SdkInterface.OpenDefectTemplateFile(path, ref DefectFileHandle, ref nWidth, ref nHeight, ref DataHandle, ref RowHandle, ref ColHandle, ref DualReadColHandle) != 0)
			{
				return null;
			}
			mDefectWidth = nWidth;
			mDefectHeight = nHeight;
			byte[] array = new byte[nWidth * nHeight];
			Marshal.Copy(DataHandle, array, 0, nWidth * nHeight);
			return array;
		}

		public void ReLoadDefectFile(string path, byte[] outData)
		{
			if (!(IntPtr.Zero == DefectFileHandle) && outData != null)
			{
				Marshal.Copy(DataHandle, outData, 0, outData.Length);
			}
		}

		public bool SaveDefectFile(byte[] defectData)
		{
			if (IntPtr.Zero == DefectFileHandle || defectData == null || defectData.Length != mDefectWidth * mDefectHeight)
			{
				return false;
			}
			Marshal.Copy(defectData, 0, DataHandle, mDefectWidth * mDefectHeight);
			SdkInterface.SaveDefectTemplateFile(DefectFileHandle);
			return true;
		}

		public void CloseDefectFile()
		{
			if (IntPtr.Zero != DefectFileHandle)
			{
				SdkInterface.CloseDefectTemplateFile(DefectFileHandle);
				DefectFileHandle = IntPtr.Zero;
			}
		}

		public void Close()
		{
			CloseDefectFile();
		}

		public static bool SaveToFileEnd(string path, byte[] data)
		{
			if (data == null || path == null)
			{
				return false;
			}
			try
			{
				FileStream fileStream = File.Open(path, FileMode.Append, FileAccess.Write, FileShare.Read);
				fileStream.Write(data, 0, data.Length);
				fileStream.Close();
				return true;
			}
			catch
			{
				return false;
			}
		}

		public static int GetFramesNumber(string filePath, int width, int height)
		{
			if (filePath == null)
			{
				return 0;
			}
			IPlayer player = PlayerFactory.Create(filePath);
			if (player == null)
			{
				return 0;
			}
			int result = player.InitParas(filePath, width, height);
			player.Close();
			return result;
		}
	}
}
