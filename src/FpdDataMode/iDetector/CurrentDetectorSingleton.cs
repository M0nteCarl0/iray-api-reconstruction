namespace iDetector
{
	public class CurrentDetectorSingleton
	{
		private static CurrentDetectorSingleton CurDetector;

		public Detector ActiveDetector
		{
			get;
			set;
		}

		private CurrentDetectorSingleton()
		{
		}

		public static CurrentDetectorSingleton Instance()
		{
			if (CurDetector == null)
			{
				CurDetector = new CurrentDetectorSingleton();
			}
			return CurDetector;
		}

		public string GetWorkDir()
		{
			if (ActiveDetector != null)
			{
				return ActiveDetector.Prop_Attr_WorkDir;
			}
			return null;
		}

		public string GetTempDataDir()
		{
			if (ActiveDetector != null)
			{
				string text = ActiveDetector.Prop_Attr_WorkDir + "temp\\";
				if (!Utility.IsDirectoryExisted(text))
				{
					Utility.CreateDirectory(text);
				}
				return text;
			}
			return null;
		}
	}
}
