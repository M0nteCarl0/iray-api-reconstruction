using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct OfflineCorrectionInfo
	{
		public int nFpdProdNo;

		public int nFpdTriggerMode;

		public int nFpdPrepCapMode;

		public int nFpdSeqInterval;

		public int nFpdExpMode;

		public int nImgWidth;

		public int nImgHeight;

		public int nBinningWidth;

		public int nBinningHeight;

		[MarshalAs(UnmanagedType.LPWStr)]
		public string pszFpdWorkDir;

		[MarshalAs(UnmanagedType.LPWStr)]
		public string pszFpdCaliSubset;

		public bool bDoPreOffset;

		public bool bDoGain;

		public bool bDoDefect;

		public bool bPreOffsetDone;

		public string FullWorkDirPath;

		public void Reset()
		{
			bDoPreOffset = false;
			bDoGain = false;
			bDoDefect = false;
		}
	}
}
