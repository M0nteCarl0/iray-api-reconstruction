namespace iDetector
{
	public enum Enm_ConnectionState
	{
		Enm_ConnState_Unknown,
		Enm_ConnState_HardwareBreak,
		Enm_ConnState_NotConnected,
		Enm_ConnState_LowRate,
		Enm_ConnState_OK
	}
}
