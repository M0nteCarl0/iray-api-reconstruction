namespace iDetector
{
	public enum Enm_FileTypes
	{
		Enm_File_Offset = 1,
		Enm_File_Gain = 2,
		Enm_File_Defect = 4,
		Enm_File_Lag = 5,
		Enm_File_ArmLog = 160,
		Enm_File_Firmware = 161
	}
}
