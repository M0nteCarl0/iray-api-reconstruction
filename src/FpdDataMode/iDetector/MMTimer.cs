using System;
using System.Timers;

namespace iDetector
{
	public class MMTimer : IDisposable
	{
		private Timer mTimer;

		private bool isTimerStoped;

		public bool IsTimerStoped
		{
			get
			{
				return isTimerStoped;
			}
		}

		public double TimerInterval
		{
			get
			{
				return mTimer.Interval;
			}
			set
			{
				mTimer.Interval = value;
			}
		}

		protected event Action mTimerProc;

		public MMTimer(int interval, Action timeProc)
		{
			mTimer = new Timer(interval);
			mTimer.Elapsed += OnTimer;
			mTimer.Interval = interval;
			this.mTimerProc = timeProc;
			isTimerStoped = true;
		}

		public void Dispose()
		{
			this.mTimerProc = null;
			mTimer.Elapsed -= OnTimer;
			GC.SuppressFinalize(this);
		}

		public void Close()
		{
			Stop();
			Dispose();
		}

		public void Start()
		{
			if (mTimer != null)
			{
				mTimer.Start();
				isTimerStoped = false;
			}
		}

		public void Stop()
		{
			isTimerStoped = true;
			if (mTimer != null)
			{
				mTimer.Stop();
			}
		}

		protected virtual void OnTimer(object sender, EventArgs e)
		{
			OnProcess();
		}

		protected void OnProcess()
		{
			if (this.mTimerProc != null)
			{
				this.mTimerProc();
			}
		}
	}
}
