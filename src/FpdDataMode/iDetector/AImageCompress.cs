using System;

namespace iDetector
{
	public abstract class AImageCompress
	{
		public struct TPicRegion
		{
			public byte[] pdata;

			public int stride;

			public int width;

			public int height;

			public void SetDataParams(byte[] srcData, int _stride, int _width, int _height)
			{
				pdata = srcData;
				stride = _stride;
				width = _width;
				height = _height;
			}
		}

		public virtual void PicZoom(ref TPicRegion Dst, TPicRegion Src)
		{
			throw new NotImplementedException();
		}

		internal byte Pixels(TPicRegion pic, long x, long y)
		{
			return pic.pdata[pic.width * y + x];
		}
	}
}
