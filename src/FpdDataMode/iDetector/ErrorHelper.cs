namespace iDetector
{
	public class ErrorHelper
	{
		private static ErrorInfo Err = default(ErrorInfo);

		public static string GetErrorDesp(int ErrorCode)
		{
			if (SdkInterface.GetErrInfo(ErrorCode, ref Err) == 0)
			{
				return Err.strDescription;
			}
			return "Unknow error";
		}

		public static string GetErrorSolution(int ErrorCode)
		{
			SdkInterface.GetErrInfo(ErrorCode, ref Err);
			return Err.strSolution;
		}
	}
}
