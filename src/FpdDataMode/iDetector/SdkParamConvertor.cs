using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class SdkParamConvertor<T>
	{
		public static bool IntPtrToStructArray(IntPtr ptr, ref T[] arr)
		{
			if (ptr.Equals(null) || arr == null)
			{
				return false;
			}
			int num = Marshal.SizeOf(typeof(T));
			for (int i = 0; i < arr.Length; i++)
			{
				IntPtr ptr2;
				if (IntPtr.Size == 4)
				{
					ptr2 = (IntPtr)((uint)(int)ptr + i * num);
				}
				else
				{
					if (IntPtr.Size != 8)
					{
						return false;
					}
					ptr2 = (IntPtr)((long)ptr + i * num);
				}
				arr[i] = (T)Marshal.PtrToStructure(ptr2, typeof(T));
			}
			return true;
		}

		public static bool StructArrayToIntPtr(T[] arr, ref IntPtr ptr)
		{
			if (ptr.Equals(null) || arr == null)
			{
				return false;
			}
			int num = Marshal.SizeOf(typeof(T));
			for (int i = 0; i < arr.Length; i++)
			{
				IntPtr ptr2;
				if (IntPtr.Size == 4)
				{
					ptr2 = (IntPtr)((uint)(int)ptr + i * num);
				}
				else
				{
					if (IntPtr.Size != 8)
					{
						return false;
					}
					ptr2 = (IntPtr)((long)ptr + i * num);
				}
				Marshal.StructureToPtr((object)arr[i], ptr2, false);
			}
			return true;
		}

		public static byte[] StructToBytes(T[] structObj)
		{
			int num = structObj.Length;
			if (num == 0)
			{
				return null;
			}
			int num2 = Marshal.SizeOf((object)structObj[0]);
			byte[] array = new byte[num2 * num];
			IntPtr intPtr = Marshal.AllocHGlobal(num2);
			for (int i = 0; i < num; i++)
			{
				Marshal.StructureToPtr((object)structObj[i], intPtr, false);
				Marshal.Copy(intPtr, array, num2 * i, num2);
			}
			Marshal.FreeHGlobal(intPtr);
			return array;
		}

		public static T[] BytesToStruct(byte[] data)
		{
			int num = Marshal.SizeOf(typeof(T));
			int num2 = data.Length / num;
			if (num2 == 0)
			{
				return null;
			}
			T[] array = new T[num2];
			IntPtr intPtr = Marshal.AllocHGlobal(num);
			for (int i = 0; i < num2; i++)
			{
				Marshal.Copy(data, i * num, intPtr, num);
				array[i] = (T)Marshal.PtrToStructure(intPtr, typeof(T));
			}
			Marshal.FreeHGlobal(intPtr);
			return array;
		}
	}
}
