using System;
using System.IO.MemoryMappedFiles;

namespace iDetector
{
	public class MemoryStoreThumbnail : iThumbnailMedium
	{
		private MemoryMappedFile memoryFile;

		private MemoryMappedViewAccessor accessor;

		public bool Create(string fileName, int totalSize)
		{
			try
			{
				memoryFile = MemoryMappedFile.CreateOrOpen(fileName, totalSize);
				accessor = memoryFile.CreateViewAccessor();
				Log.Instance().Write("MEMThumbnail mapFile path:{0}", fileName);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Create MEMThumnail failed!Path:{0},Err={1}", fileName, ex.ToString());
				throw ex;
			}
			return true;
		}

		public void Close()
		{
			if (accessor != null)
			{
				accessor.Dispose();
			}
			if (memoryFile != null)
			{
				memoryFile.Dispose();
			}
		}

		public int Write(long position, byte[] array, int offset, int count)
		{
			if (accessor == null || array == null || count == 0)
			{
				return 0;
			}
			accessor.WriteArray(position, array, offset, count);
			return count;
		}

		public int Read(long position, byte[] array, int offset, int count)
		{
			if (accessor == null || array == null || count == 0)
			{
				return 0;
			}
			return accessor.ReadArray(position, array, offset, count);
		}
	}
}
