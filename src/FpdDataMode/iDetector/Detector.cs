using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class Detector : INotifyPropertyChanged
	{
		private int _nID;

		private AttrValMonitor _attrChangingMonitor;

		public Enm_ConnectionState ConnectState;

		private string mTask;

		private List<SdkCallbackEventHandler> SubscribeEventList;

		public int ID
		{
			get
			{
				return _nID;
			}
		}

		public AttrValMonitor AttrChangingMonitor
		{
			get
			{
				return _attrChangingMonitor;
			}
		}

		public string TASK
		{
			get
			{
				return mTask;
			}
			set
			{
				mTask = value;
				NotifyPropertyChanged("TASK");
			}
		}

		public int Prop_Cfg_ProtocolEdition
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1, ref pVar);
			}
		}

		public int Prop_Cfg_ProductNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2, ref pVar);
			}
		}

		public string Prop_Cfg_SN
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3, ref pVar);
			}
		}

		public int Prop_Cfg_UseServiceProcess
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(8, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(8, ref pVar);
			}
		}

		public string Prop_Cfg_DetectorImp
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4, ref pVar);
			}
		}

		public string Prop_Cfg_ConnImp
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(5, ref pVar);
			}
		}

		public string Prop_Cfg_CaliImp
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(6, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(6, ref pVar);
			}
		}

		public int Prop_Cfg_LogLevel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(7, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(7, ref pVar);
			}
		}

		public string Prop_Cfg_HostIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(101, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(101, ref pVar);
			}
		}

		public int Prop_Cfg_HostPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(102, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(102, ref pVar);
			}
		}

		public string Prop_Cfg_RemoteIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(103, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(103, ref pVar);
			}
		}

		public int Prop_Cfg_RemotePort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(104, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(104, ref pVar);
			}
		}

		public int Prop_Cfg_ComPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(105, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(105, ref pVar);
			}
		}

		public string Prop_Cfg_PleoraConnStr
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(106, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(106, ref pVar);
			}
		}

		public int Prop_Cfg_PleoraPacketSize
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(107, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(107, ref pVar);
			}
		}

		public string Prop_Cfg_WinpcapConnStr
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(108, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(108, ref pVar);
			}
		}

		public int Prop_Cfg_PleoraMaxFps
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(109, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(109, ref pVar);
			}
		}

		public int Prop_Cfg_RepeatCmdEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(150, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(150, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Download_HostIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(201, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(201, ref pVar);
			}
		}

		public int Prop_Cfg_FTP_Download_HostPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(202, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(202, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Download_User
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(203, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(203, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Download_PWD
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(204, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(204, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Download_LocalPath
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(205, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(205, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Upload_HostIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(206, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(206, ref pVar);
			}
		}

		public int Prop_Cfg_FTP_Upload_HostPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(207, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(207, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Upload_User
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(208, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(208, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Upload_PWD
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(209, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(209, ref pVar);
			}
		}

		public string Prop_Cfg_FTP_Upload_LocalPath
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(210, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(210, ref pVar);
			}
		}

		public int Prop_Cfg_OffsetAlarmMinute
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(301, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(301, ref pVar);
			}
		}

		public int Prop_Cfg_GainAlarmTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(302, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(302, ref pVar);
			}
		}

		public int Prop_Cfg_DefectAlarmTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(303, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(303, ref pVar);
			}
		}

		public int Prop_Cfg_CaliValidity_PreWarnMinute
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(304, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(304, ref pVar);
			}
		}

		public int Prop_Cfg_CaliValidity_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(305, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(305, ref pVar);
			}
		}

		public string Prop_Cfg_DefaultSubset
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(306, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(306, ref pVar);
			}
		}

		public int Prop_Cfg_DefaultCorrectOption
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(307, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(307, ref pVar);
			}
		}

		public int Prop_Cfg_DefectStatistical_DummyTop
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(308, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(308, ref pVar);
			}
		}

		public int Prop_Cfg_DefectStatistical_DummyBottom
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(309, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(309, ref pVar);
			}
		}

		public int Prop_Cfg_DefectStatistical_DummyLeft
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(310, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(310, ref pVar);
			}
		}

		public int Prop_Cfg_DefectStatistical_DummyRight
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(311, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(311, ref pVar);
			}
		}

		public int Prop_Cfg_ClearAcqParam_DelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(501, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(501, ref pVar);
			}
		}

		public int Prop_Cfg_FpsCheck_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(502, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(502, ref pVar);
			}
		}

		public int Prop_Cfg_FpsCheck_Tolerance
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(503, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(503, ref pVar);
			}
		}

		public int Prop_Cfg_FWUpdTimeOut
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(504, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(504, ref pVar);
			}
		}

		public int Prop_Cfg_OfflineInspectTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(505, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(505, ref pVar);
			}
		}

		public int Prop_Cfg_AllowReconnectByOnlineNotice
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(506, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(506, ref pVar);
			}
		}

		public int Prop_Cfg_ResetTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(507, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(507, ref pVar);
			}
		}

		public int Prop_Cfg_PreviewImage_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(508, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(508, ref pVar);
			}
		}

		public int Prop_Cfg_PushImageAtExpTimeout_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(509, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(509, ref pVar);
			}
		}

		public int Prop_Cfg_RetransferCount
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(510, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(510, ref pVar);
			}
		}

		public int Prop_Cfg_ConnRecoverTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(511, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(511, ref pVar);
			}
		}

		public int Prop_Cfg_TemperatureHighThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(512, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(512, ref pVar);
			}
		}

		public int Prop_Cfg_AllowMismatchSN
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(513, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(513, ref pVar);
			}
		}

		public int Prop_Cfg_ImagePacketGapTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(514, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(514, ref pVar);
			}
		}

		public int Prop_Cfg_FwAllowedDefectPoints
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(515, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(515, ref pVar);
			}
		}

		public int Prop_Cfg_PostOffsetStart_DelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(516, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(516, ref pVar);
			}
		}

		public int Prop_Cfg_TotalAcqTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(517, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(517, ref pVar);
			}
		}

		public int Prop_Cfg_PreExpImageAcqTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(518, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(518, ref pVar);
			}
		}

		public int Prop_Cfg_CleanupProcessTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(519, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(519, ref pVar);
			}
		}

		public int Prop_Cfg_SeqAcq_AutoStopToSyncExp
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(520, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(520, ref pVar);
			}
		}

		public int Prop_Cfg_Acq2SubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(521, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(521, ref pVar);
			}
		}

		public string Prop_Attr_Prod_Name
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1001, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(1001, ref pVar);
			}
		}

		public string Prop_Attr_Prod_Description
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1002, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(1002, ref pVar);
			}
		}

		public int Prop_Attr_Prod_FullWidth
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1003, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1003, ref pVar);
			}
		}

		public int Prop_Attr_Prod_FullHeight
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1004, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1004, ref pVar);
			}
		}

		public int Prop_Attr_Prod_PhysicalPixelSize
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1005, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1005, ref pVar);
			}
		}

		public int Prop_Attr_Prod_BitDepth
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1006, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1006, ref pVar);
			}
		}

		public int Prop_Attr_Prod_DataBytesPerPacket
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1007, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1007, ref pVar);
			}
		}

		public int Prop_Attr_Prod_TotalPacketNumber
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1008, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1008, ref pVar);
			}
		}

		public int Prop_Attr_Prod_DummyTop
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1009, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1009, ref pVar);
			}
		}

		public int Prop_Attr_Prod_DummyBottom
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1010, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1010, ref pVar);
			}
		}

		public int Prop_Attr_Prod_DummyLeft
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1011, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1011, ref pVar);
			}
		}

		public int Prop_Attr_Prod_DummyRight
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1012, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1012, ref pVar);
			}
		}

		public int Prop_Attr_Prod_AfeChSize
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1014, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1014, ref pVar);
			}
		}

		public int Prop_Attr_Prod_GateSize
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1016, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1016, ref pVar);
			}
		}

		public int Prop_Attr_Prod_GateEdge
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1017, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1017, ref pVar);
			}
		}

		public int Prop_Attr_Prod_DriveMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1013, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1013, ref pVar);
			}
		}

		public int Prop_Attr_Prod_ReXferMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(1015, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(1015, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ProductNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2001, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2001, ref pVar);
			}
		}

		public string Prop_Attr_UROM_MainVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2002, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2002, ref pVar);
			}
		}

		public string Prop_Attr_UROM_ReadVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2003, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2003, ref pVar);
			}
		}

		public string Prop_Attr_UROM_McuVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2004, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2004, ref pVar);
			}
		}

		public string Prop_Attr_UROM_ArmVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2005, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2005, ref pVar);
			}
		}

		public string Prop_Attr_UROM_KernelVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2006, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2006, ref pVar);
			}
		}

		public string Prop_Attr_UROM_ProtocolVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2007, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2007, ref pVar);
			}
		}

		public string Prop_Attr_UROM_MasterBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2008, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2008, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SlaveBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2009, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2009, ref pVar);
			}
		}

		public string Prop_Attr_UROM_McuBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2010, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2010, ref pVar);
			}
		}

		public int Prop_Attr_UROM_RowPreDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2011, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2011, ref pVar);
			}
		}

		public int Prop_Attr_UROM_RowPostDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2012, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2012, ref pVar);
			}
		}

		public int Prop_Attr_UROM_IntegrateTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2013, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2013, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ZoomMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2014, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2014, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpEnable_SignalLevel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2015, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2015, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SelfClearEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2016, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2016, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SelfClearSpanTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2017, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2017, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SequenceIntervalTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2018, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2018, ref pVar);
			}
		}

		public int Prop_Attr_UROM_TriggerMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2019, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2019, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DynamicFlag
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2020, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2020, ref pVar);
			}
		}

		public int Prop_Attr_UROM_TubeReadyTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2021, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2021, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SequenceIntervalTime_HighPrecision
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2022, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2022, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SetDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2023, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2023, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpWindowTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2025, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2025, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SyncExpTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2027, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2027, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SyncExpTime_HighPrecision
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2028, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2028, ref pVar);
			}
		}

		public float Prop_Attr_UROM_VT
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2029, ref pVar);
				return pVar.val.fVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
				pVar.val.fVal = value;
				SetAttr(2029, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PGA
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2030, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2030, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PrepCapMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2032, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2032, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SelfCapEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2033, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2033, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FluroSync
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2034, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2034, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SrcPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2035, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2035, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SrcIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2036, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2036, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SrcMAC
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2037, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2037, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DestPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2038, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2038, ref pVar);
			}
		}

		public string Prop_Attr_UROM_DestIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2039, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2039, ref pVar);
			}
		}

		public string Prop_Attr_UROM_DestMAC
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2040, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2040, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SyncboxIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2041, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2041, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PreviewImgMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2044, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2044, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HWOffsetType
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2045, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2045, ref pVar);
			}
		}

		public int Prop_Attr_UROM_AcquireDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2046, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2046, ref pVar);
			}
		}

		public int Prop_Attr_UROM_BinningMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2047, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2047, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2050, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2050, ref pVar);
			}
		}

		public int Prop_Attr_UROM_AecMainTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2051, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2051, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DynaOffsetGapTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2052, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2052, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DynaOffsetEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2053, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2053, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ImagePktGapTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2054, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2054, ref pVar);
			}
		}

		public int Prop_Attr_UROM_OutModeCapTrig
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2069, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2069, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgPrepOn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2055, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2055, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRayEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2056, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2056, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRayOn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2057, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2057, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRaySyncOut
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2058, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2058, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRaySyncIn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2059, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2059, ref pVar);
			}
		}

		public string Prop_Attr_UROM_CbxBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2060, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2060, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SubProductNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2061, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2061, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SerialNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2062, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2062, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ImageChType
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2063, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2063, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ImageChProtocol
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2064, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2064, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HWGainEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2065, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2065, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpTimeValidPercent
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2066, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2066, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FreesyncCenterThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2067, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2067, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FreesyncEdgeThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2068, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2068, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FreesyncSubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2070, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2070, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PowSeriesCorrectEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2071, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2071, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PulseClearTimes
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2072, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2072, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIColStartPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2073, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2073, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIColEndPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2074, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2074, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIRowStartPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2075, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2075, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIRowEndPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2076, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2076, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FullWell
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2077, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2077, ref pVar);
			}
		}

		public int Prop_Attr_UROM_InnerSubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2078, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2078, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SoftwareSubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2079, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2079, ref pVar);
			}
		}

		public int Prop_Attr_UROM_IntegrateTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2540, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2540, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ZoomMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2501, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2501, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpEnable_SignalLevel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2502, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2502, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SelfClearEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2503, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2503, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SelfClearSpanTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2504, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2504, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SequenceIntervalTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2505, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2505, ref pVar);
			}
		}

		public int Prop_Attr_UROM_TriggerMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2506, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2506, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DynamicFlag_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2507, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2507, ref pVar);
			}
		}

		public int Prop_Attr_UROM_TubeReadyTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2508, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2508, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SetDelayTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2510, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2510, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SequenceIntervalTime_HighPrecision_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2511, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2511, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpWindowTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2512, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2512, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PGA_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2513, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2513, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PrepCapMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2514, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2514, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SelfCapEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2515, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2515, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FluroSync_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2516, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2516, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SrcIP_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2518, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2518, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SrcMAC_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2519, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2519, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DestPort_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2520, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2520, ref pVar);
			}
		}

		public string Prop_Attr_UROM_DestIP_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2521, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2521, ref pVar);
			}
		}

		public string Prop_Attr_UROM_DestMAC_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2522, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2522, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PreviewImgMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2523, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2523, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HWOffsetType_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2544, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2544, ref pVar);
			}
		}

		public string Prop_Attr_UROM_SyncboxIP_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2543, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(2543, ref pVar);
			}
		}

		public int Prop_Attr_UROM_AcquireDelayTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2524, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2524, ref pVar);
			}
		}

		public int Prop_Attr_UROM_BinningMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2525, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2525, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2528, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2528, ref pVar);
			}
		}

		public int Prop_Attr_UROM_AecMainTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2529, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2529, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DynaOffsetGapTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2530, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2530, ref pVar);
			}
		}

		public int Prop_Attr_UROM_DynaOffsetEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2531, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2531, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ImagePktGapTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2542, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2542, ref pVar);
			}
		}

		public int Prop_Attr_UROM_OutModeCapTrig_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2541, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2541, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgPrepOn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2532, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2532, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRayEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2533, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2533, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRayOn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2534, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2534, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRaySyncOut_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2535, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2535, ref pVar);
			}
		}

		public int Prop_Attr_UROM_HvgXRaySyncIn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2536, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2536, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ExpTimeValidPercent_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2537, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2537, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FreesyncCenterThreshold_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2538, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2538, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FreesyncEdgeThreshold_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2539, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2539, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PowSeriesCorrectEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2545, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2545, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIColStartPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2546, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2546, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIColEndPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2547, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2547, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIRowStartPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2548, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2548, ref pVar);
			}
		}

		public int Prop_Attr_UROM_ROIRowEndPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2549, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2549, ref pVar);
			}
		}

		public int Prop_Attr_UROM_FullWell_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2550, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2550, ref pVar);
			}
		}

		public int Prop_Attr_UROM_PulseClearTimes_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2551, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2551, ref pVar);
			}
		}

		public int Prop_Attr_UROM_InnerSubFlow_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2552, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2552, ref pVar);
			}
		}

		public int Prop_Attr_UROM_SoftwareSubFlow_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(2553, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(2553, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ProductNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3001, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3001, ref pVar);
			}
		}

		public string Prop_Attr_FROM_MainVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3002, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3002, ref pVar);
			}
		}

		public string Prop_Attr_FROM_ReadVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3003, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3003, ref pVar);
			}
		}

		public string Prop_Attr_FROM_McuVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3004, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3004, ref pVar);
			}
		}

		public string Prop_Attr_FROM_ArmVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3005, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3005, ref pVar);
			}
		}

		public string Prop_Attr_FROM_KernelVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3006, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3006, ref pVar);
			}
		}

		public string Prop_Attr_FROM_ProtocolVersion
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3007, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3007, ref pVar);
			}
		}

		public string Prop_Attr_FROM_MasterBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3008, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3008, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SlaveBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3009, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3009, ref pVar);
			}
		}

		public string Prop_Attr_FROM_McuBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3010, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3010, ref pVar);
			}
		}

		public int Prop_Attr_FROM_RowPreDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3011, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3011, ref pVar);
			}
		}

		public int Prop_Attr_FROM_RowPostDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3012, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3012, ref pVar);
			}
		}

		public int Prop_Attr_FROM_IntegrateTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3013, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3013, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ZoomMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3014, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3014, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpEnable_SignalLevel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3015, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3015, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SelfClearEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3016, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3016, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SelfClearSpanTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3017, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3017, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SequenceIntervalTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3018, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3018, ref pVar);
			}
		}

		public int Prop_Attr_FROM_TriggerMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3019, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3019, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DynamicFlag
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3020, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3020, ref pVar);
			}
		}

		public int Prop_Attr_FROM_TubeReadyTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3021, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3021, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SequenceIntervalTime_HighPrecision
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3022, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3022, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SetDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3023, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3023, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpWindowTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3025, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3025, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SyncExpTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3027, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3027, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SyncExpTime_HighPrecision
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3028, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3028, ref pVar);
			}
		}

		public float Prop_Attr_FROM_VT
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3029, ref pVar);
				return pVar.val.fVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
				pVar.val.fVal = value;
				SetAttr(3029, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PGA
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3030, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3030, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PrepCapMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3032, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3032, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SelfCapEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3033, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3033, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FluroSync
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3034, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3034, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SrcPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3035, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3035, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SrcIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3036, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3036, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SrcMAC
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3037, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3037, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DestPort
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3038, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3038, ref pVar);
			}
		}

		public string Prop_Attr_FROM_DestIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3039, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3039, ref pVar);
			}
		}

		public string Prop_Attr_FROM_DestMAC
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3040, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3040, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SyncboxIP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3041, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3041, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PreviewImgMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3044, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3044, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HWOffsetType
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3045, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3045, ref pVar);
			}
		}

		public int Prop_Attr_FROM_AcquireDelayTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3046, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3046, ref pVar);
			}
		}

		public int Prop_Attr_FROM_BinningMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3047, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3047, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpMode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3050, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3050, ref pVar);
			}
		}

		public int Prop_Attr_FROM_AecMainTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3051, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3051, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DynaOffsetGapTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3052, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3052, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DynaOffsetEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3053, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3053, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ImagePktGapTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3054, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3054, ref pVar);
			}
		}

		public int Prop_Attr_FROM_OutModeCapTrig
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3069, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3069, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgPrepOn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3055, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3055, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRayEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3056, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3056, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRayOn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3057, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3057, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRaySyncOut
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3058, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3058, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRaySyncIn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3059, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3059, ref pVar);
			}
		}

		public string Prop_Attr_FROM_CbxBuildTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3060, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3060, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SubProductNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3061, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3061, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SerialNo
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3062, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3062, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ImageChType
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3063, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3063, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ImageChProtocol
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3064, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3064, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HWGainEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3065, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3065, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpTimeValidPercent
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3066, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3066, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FreesyncCenterThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3067, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3067, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FreesyncEdgeThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3068, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3068, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FreesyncSubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3070, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3070, ref pVar);
			}
		}

		public int Prop_Attr_FROM_AutoSleepIdleTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3071, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3071, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncParalClearTimes
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3072, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3072, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncFastScanCpvCycle
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3073, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3073, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncTriggerCheckTimeout
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3074, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3074, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncSegmentThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3075, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3075, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncLineThreshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3076, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3076, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncFalseTriggerUnresponseStageTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3077, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3077, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncParalClearLine
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3078, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3078, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PowSeriesCorrectEnable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3079, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3079, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PulseClearTimes
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3080, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3080, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIColStartPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3081, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3081, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIColEndPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3082, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3082, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIRowStartPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3083, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3083, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIRowEndPos
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3084, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3084, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FullWell
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3085, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3085, ref pVar);
			}
		}

		public int Prop_Attr_FROM_InnerSubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3086, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3086, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SoftwareSubFlow
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3087, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3087, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug1
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3200, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3200, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug2
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3201, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3201, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug3
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3202, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3202, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug4
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3203, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3203, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug5
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3204, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3204, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug6
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3205, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3205, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug7
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3206, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3206, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug8
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3207, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3207, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug9
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3208, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3208, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug10
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3209, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3209, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug11
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3210, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3210, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug12
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3211, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3211, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug13
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3212, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3212, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug14
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3213, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3213, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug15
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3214, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3214, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug16
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3215, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3215, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug17
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3216, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3216, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug18
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3217, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3217, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug19
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3218, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3218, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug20
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3219, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3219, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug21
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3220, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3220, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug22
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3221, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3221, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug23
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3222, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3222, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug24
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3223, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3223, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug25
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3224, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3224, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug26
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3225, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3225, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug27
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3226, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3226, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug28
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3227, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3227, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug29
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3228, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3228, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug30
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3229, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3229, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug31
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3230, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3230, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug32
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3231, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3231, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test1
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3232, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3232, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test2
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3233, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3233, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test3
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3234, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3234, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test4
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3235, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3235, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test5
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3236, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3236, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test6
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3237, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3237, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test7
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3238, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3238, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test8
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3239, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3239, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test9
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3240, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3240, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test10
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3241, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3241, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test11
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3242, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3242, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test12
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3243, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3243, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test13
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3244, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3244, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test14
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3245, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3245, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test15
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3246, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3246, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test16
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3247, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3247, ref pVar);
			}
		}

		public int Prop_Attr_FROM_RowPreDelayTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3511, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3511, ref pVar);
			}
		}

		public int Prop_Attr_FROM_RowPostDelayTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3512, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3512, ref pVar);
			}
		}

		public int Prop_Attr_FROM_IntegrateTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3513, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3513, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ZoomMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3514, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3514, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpEnable_SignalLevel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3515, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3515, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SelfClearEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3516, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3516, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SelfClearSpanTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3517, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3517, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SequenceIntervalTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3518, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3518, ref pVar);
			}
		}

		public int Prop_Attr_FROM_TriggerMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3519, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3519, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DynamicFlag_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3520, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3520, ref pVar);
			}
		}

		public int Prop_Attr_FROM_TubeReadyTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3521, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3521, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SequenceIntervalTime_HighPrecision_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3522, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3522, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SetDelayTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3523, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3523, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpWindowTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3525, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3525, ref pVar);
			}
		}

		public float Prop_Attr_FROM_VT_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3529, ref pVar);
				return pVar.val.fVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
				pVar.val.fVal = value;
				SetAttr(3529, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PGA_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3530, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3530, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PrepCapMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3532, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3532, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SelfCapEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3533, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3533, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FluroSync_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3534, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3534, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SrcIP_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3536, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3536, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SrcMAC_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3537, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3537, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DestPort_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3538, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3538, ref pVar);
			}
		}

		public string Prop_Attr_FROM_DestIP_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3539, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3539, ref pVar);
			}
		}

		public string Prop_Attr_FROM_DestMAC_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3540, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3540, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SyncboxIP_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3541, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3541, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PreviewImgMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3544, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3544, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HWOffsetType_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3545, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3545, ref pVar);
			}
		}

		public int Prop_Attr_FROM_AcquireDelayTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3546, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3546, ref pVar);
			}
		}

		public int Prop_Attr_FROM_BinningMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3547, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3547, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpMode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3550, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3550, ref pVar);
			}
		}

		public int Prop_Attr_FROM_AecMainTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3551, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3551, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DynaOffsetGapTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3552, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3552, ref pVar);
			}
		}

		public int Prop_Attr_FROM_DynaOffsetEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3553, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3553, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ImagePktGapTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3554, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3554, ref pVar);
			}
		}

		public int Prop_Attr_FROM_OutModeCapTrig_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3569, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3569, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgPrepOn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3555, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3555, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRayEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3556, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3556, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRayOn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3557, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3557, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRaySyncOut_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3558, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3558, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HvgXRaySyncIn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3559, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3559, ref pVar);
			}
		}

		public int Prop_Attr_FROM_CbxBuildTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3560, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3560, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SubProductNo_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3561, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3561, ref pVar);
			}
		}

		public string Prop_Attr_FROM_SerialNo_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3562, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(3562, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ImageChType_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3563, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3563, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ImageChProtocol_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3564, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3564, ref pVar);
			}
		}

		public int Prop_Attr_FROM_HWGainEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3565, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3565, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ExpTimeValidPercent_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3566, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3566, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FreesyncCenterThreshold_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3567, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3567, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FreesyncEdgeThreshold_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3568, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3568, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FreesyncSubFlow_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3570, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3570, ref pVar);
			}
		}

		public int Prop_Attr_FROM_AutoSleepIdleTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3571, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3571, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncParalClearTimes_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3572, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3572, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncFastScanCpvCycle_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3573, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3573, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncTriggerCheckTimeout_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3574, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3574, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncSegmentThreshold_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3575, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3575, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncLineThreshold_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3576, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3576, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncFalseTriggerUnresponseStageTime_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3577, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3577, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FSyncParalClearLine_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3578, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3578, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PowSeriesCorrectEnable_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3579, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3579, ref pVar);
			}
		}

		public int Prop_Attr_FROM_PulseClearTimes_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3580, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3580, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIColStartPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3581, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3581, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIColEndPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3582, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3582, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIRowStartPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3583, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3583, ref pVar);
			}
		}

		public int Prop_Attr_FROM_ROIRowEndPos_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3584, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3584, ref pVar);
			}
		}

		public int Prop_Attr_FROM_FullWell_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3585, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3585, ref pVar);
			}
		}

		public int Prop_Attr_FROM_InnerSubFlow_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3586, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3586, ref pVar);
			}
		}

		public int Prop_Attr_FROM_SoftwareSubFlow_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3587, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3587, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug1_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3700, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3700, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug2_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3701, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3701, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug3_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3702, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3702, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug4_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3703, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3703, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug5_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3704, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3704, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug6_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3705, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3705, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug7_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3706, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3706, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug8_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3707, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3707, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug9_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3708, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3708, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug10_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3709, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3709, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug11_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3710, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3710, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug12_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3711, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3711, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug13_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3712, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3712, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug14_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3713, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3713, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug15_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3714, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3714, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug16_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3715, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3715, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug17_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3716, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3716, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug18_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3717, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3717, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug19_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3718, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3718, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug20_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3719, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3719, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug21_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3720, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3720, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug22_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3721, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3721, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug23_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3722, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3722, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug24_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3723, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3723, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug25_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3724, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3724, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug26_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3725, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3725, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug27_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3726, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3726, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug28_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3727, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3727, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug29_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3728, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3728, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug30_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3729, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3729, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug31_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3730, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3730, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Debug32_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3731, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3731, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test1_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3732, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3732, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test2_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3733, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3733, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test3_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3734, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3734, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test4_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3735, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3735, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test5_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3736, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3736, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test6_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3737, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3737, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test7_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3738, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3738, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test8_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3739, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3739, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test9_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3740, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3740, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test10_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3741, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3741, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test11_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3742, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3742, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test12_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3743, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3743, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test13_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3744, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3744, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test14_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3745, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3745, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test15_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3746, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3746, ref pVar);
			}
		}

		public int Prop_Attr_FROM_Test16_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(3747, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(3747, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_AP_SSID
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4001, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4001, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_AP_Key
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4002, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4002, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_CountryCode
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4003, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4003, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_FrequencySel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4004, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4004, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_BandWidthSel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4005, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4005, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_ChannelSel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4006, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4006, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_SecuritySel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4007, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4007, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_ApModeEn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4008, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4008, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_DhcpServerEn
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4009, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4009, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_Client_ListNum
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4010, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4010, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_Client_CurrentSel
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4011, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4011, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID0
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4012, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4012, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key0
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4013, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4013, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID1
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4014, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4014, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key1
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4015, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4015, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID2
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4016, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4016, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key2
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4017, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4017, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID3
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4018, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4018, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key3
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4019, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4019, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID4
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4020, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4020, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key4
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4021, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4021, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID5
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4022, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4022, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key5
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4023, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4023, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID6
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4024, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4024, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key6
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4025, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4025, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID7
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4026, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4026, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key7
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4027, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4027, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID8
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4028, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4028, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key8
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4029, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4029, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID9
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4030, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4030, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key9
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4031, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4031, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_AP_SSID_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4501, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4501, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_AP_Key_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4502, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4502, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_CountryCode_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4503, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4503, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_FrequencySel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4504, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4504, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_BandWidthSel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4505, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4505, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_ChannelSel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4506, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4506, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_SecuritySel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4507, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4507, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_ApModeEn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4508, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4508, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_AP_DhcpServerEn_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4509, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4509, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_Client_ListNum_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4510, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4510, ref pVar);
			}
		}

		public int Prop_Attr_Wifi_Client_CurrentSel_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4511, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(4511, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID0_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4512, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4512, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key0_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4513, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4513, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID1_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4514, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4514, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key1_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4515, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4515, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID2_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4516, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4516, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key2_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4517, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4517, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID3_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4518, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4518, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key3_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4519, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4519, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID4_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4520, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4520, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key4_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4521, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4521, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID5_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4522, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4522, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key5_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4523, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4523, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID6_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4524, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4524, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key6_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4525, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4525, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID7_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4526, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4526, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key7_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4527, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4527, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID8_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4528, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4528, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key8_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4529, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4529, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_SSID9_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4530, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4530, ref pVar);
			}
		}

		public string Prop_Attr_Wifi_Client_Key9_W
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(4531, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(4531, ref pVar);
			}
		}

		public string Prop_Attr_WorkDir
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5001, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(5001, ref pVar);
			}
		}

		public int Prop_Attr_State
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5002, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5002, ref pVar);
			}
		}

		public int Prop_Attr_ConnState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5003, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5003, ref pVar);
			}
		}

		public int Prop_Attr_CurrentTask
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5004, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5004, ref pVar);
			}
		}

		public int Prop_Attr_CurrentTransaction
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5005, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5005, ref pVar);
			}
		}

		public int Prop_Attr_FsmState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5006, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5006, ref pVar);
			}
		}

		public int Prop_Attr_Width
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5007, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5007, ref pVar);
			}
		}

		public int Prop_Attr_Height
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5008, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5008, ref pVar);
			}
		}

		public int Prop_Attr_PrevImg_Width
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5009, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5009, ref pVar);
			}
		}

		public int Prop_Attr_PrevImg_Height
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5010, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5010, ref pVar);
			}
		}

		public int Prop_Attr_Authority
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5011, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5011, ref pVar);
			}
		}

		public int Prop_Attr_ConnState_CmdCh
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5012, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5012, ref pVar);
			}
		}

		public int Prop_Attr_ConnState_ImgCh
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5013, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5013, ref pVar);
			}
		}

		public int Prop_Attr_ElapsedExpWindowTime
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5014, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5014, ref pVar);
			}
		}

		public int Prop_Attr_FWUpdateProgress
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5015, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5015, ref pVar);
			}
		}

		public int Prop_Attr_ImageTransProgress
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5016, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5016, ref pVar);
			}
		}

		public float Prop_Attr_RdResult_T1
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5017, ref pVar);
				return pVar.val.fVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
				pVar.val.fVal = value;
				SetAttr(5017, ref pVar);
			}
		}

		public float Prop_Attr_RdResult_T2
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5018, ref pVar);
				return pVar.val.fVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
				pVar.val.fVal = value;
				SetAttr(5018, ref pVar);
			}
		}

		public float Prop_Attr_RdResult_Humidity
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5019, ref pVar);
				return pVar.val.fVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_FLT;
				pVar.val.fVal = value;
				SetAttr(5019, ref pVar);
			}
		}

		public int Prop_Attr_RdResult_Shock_Threshold
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5020, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5020, ref pVar);
			}
		}

		public string Prop_Attr_CurrentSubset
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5021, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(5021, ref pVar);
			}
		}

		public int Prop_Attr_CurrentCorrectOption
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5022, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5022, ref pVar);
			}
		}

		public int Prop_Attr_OffsetValidityState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5023, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5023, ref pVar);
			}
		}

		public int Prop_Attr_GainValidityState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5024, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5024, ref pVar);
			}
		}

		public int Prop_Attr_DefectValidityState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5025, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5025, ref pVar);
			}
		}

		public int Prop_Attr_LagValidityState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5026, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5026, ref pVar);
			}
		}

		public int Prop_Attr_GhostValidityState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5027, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5027, ref pVar);
			}
		}

		public int Prop_Attr_OffsetTotalFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5028, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5028, ref pVar);
			}
		}

		public int Prop_Attr_OffsetValidFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5029, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5029, ref pVar);
			}
		}

		public int Prop_Attr_GainTotalFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5030, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5030, ref pVar);
			}
		}

		public int Prop_Attr_GainValidFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5031, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5031, ref pVar);
			}
		}

		public int Prop_Attr_DefectTotalFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5032, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5032, ref pVar);
			}
		}

		public int Prop_Attr_DefectValidFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5033, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5033, ref pVar);
			}
		}

		public int Prop_Attr_LagTotalFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5034, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5034, ref pVar);
			}
		}

		public int Prop_Attr_LagValidFrames
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5035, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5035, ref pVar);
			}
		}

		public int Prop_Attr_Battery_Exist
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5036, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5036, ref pVar);
			}
		}

		public int Prop_Attr_Battery_Remaining
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5037, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5037, ref pVar);
			}
		}

		public int Prop_Attr_Battery_ChargingStatus
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5038, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5038, ref pVar);
			}
		}

		public int Prop_Attr_Battery_PowerWarnStatus
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5039, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5039, ref pVar);
			}
		}

		public int Prop_Attr_NetworkInterface
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5040, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5040, ref pVar);
			}
		}

		public string Prop_Attr_WifiStatu_LinkedAP
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5041, ref pVar);
				return pVar.val.strVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.strVal = value;
				SetAttr(5041, ref pVar);
			}
		}

		public int Prop_Attr_WifiStatu_WorkingBand
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5042, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5042, ref pVar);
			}
		}

		public int Prop_Attr_WifiStatu_WorkingSignalIntensity
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5043, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5043, ref pVar);
			}
		}

		public int Prop_Attr_WifiStatu_WorkingLinkQuality
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5044, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5044, ref pVar);
			}
		}

		public int Prop_Attr_WifiStatu_WorkingTxPower
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5045, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5045, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Offset_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5046, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5046, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Offset_ValidIndex
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5047, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5047, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Offset_FileCount
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5048, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5048, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Gain_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5049, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5049, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Gain_ValidIndex
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5050, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5050, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Gain_FileCount
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5051, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5051, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_MostGain_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5052, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5052, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_MostGain_ValidIndex
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5053, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5053, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_MostGain_FileCount
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5054, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5054, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Defect_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5055, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5055, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Defect_ValidIndex
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5056, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5056, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Defect_FileCount
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5057, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5057, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Lag_Enable
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5058, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5058, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Lag_ValidIndex
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5059, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5059, ref pVar);
			}
		}

		public int Prop_Attr_HwTmpl_Lag_FileCount
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5060, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5060, ref pVar);
			}
		}

		public int Prop_Attr_CorrectionPrepared
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5061, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5061, ref pVar);
			}
		}

		public int Prop_Attr_RdResult_OutExpState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5062, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5062, ref pVar);
			}
		}

		public int Prop_Attr_RdResult_AutoSleepState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5063, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5063, ref pVar);
			}
		}

		public int Prop_Attr_Battery_ExternalPower
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5064, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5064, ref pVar);
			}
		}

		public int Prop_Attr_GCU_OnlineState
		{
			get
			{
				IRayVariant pVar = default(IRayVariant);
				GetAttr(5065, ref pVar);
				return pVar.val.nVal;
			}
			set
			{
				IRayVariant pVar = default(IRayVariant);
				pVar.vt = IRAY_VAR_TYPE.IVT_INT;
				pVar.val.nVal = value;
				SetAttr(5065, ref pVar);
			}
		}

		public event SdkCallbackEventHandler SdkCallbackEvent;

		public event PropertyChangedEventHandler PropertyChanged;

		public Detector(int nID)
		{
			_nID = nID;
			ConnectState = Enm_ConnectionState.Enm_ConnState_Unknown;
			_attrChangingMonitor = new AttrValMonitor(_nID);
			_attrChangingMonitor.AttrValChangedEvent += OnAttrValChanged;
			SubscribeEventList = new List<SdkCallbackEventHandler>();
		}

		public void Close()
		{
			_attrChangingMonitor.AttrValChangedEvent -= OnAttrValChanged;
			SubscribeEventList.Clear();
			this.SdkCallbackEvent = null;
			this.PropertyChanged = null;
		}

		public bool SubscribeCBEvent(SdkCallbackEventHandler callbackHandler)
		{
			if (SubscribeEventList.Contains(callbackHandler))
			{
				return false;
			}
			SubscribeEventList.Add(callbackHandler);
			SdkCallbackEvent += callbackHandler;
			return true;
		}

		public void UnSubscribeCBEvent(SdkCallbackEventHandler callbackHandler)
		{
			if (SubscribeEventList.Contains(callbackHandler))
			{
				SubscribeEventList.Remove(callbackHandler);
				SdkCallbackEvent -= callbackHandler;
			}
		}

		public int Invoke(int nCommandID, IRayCmdParam[] pars, int nParCount)
		{
			string arg = (!SdkInterface.CommandIDTable.ContainsKey(nCommandID)) ? SdkInterface.CommandIDTable[0] : SdkInterface.CommandIDTable[nCommandID];
			Log.Instance().Write("Detector[{0}] send command[{1}]:{2}", _nID, nCommandID, arg);
			return SdkInterface.Invoke(_nID, nCommandID, pars, nParCount);
		}

		public int Invoke(int nCommandID)
		{
			return Invoke(nCommandID, null, 0);
		}

		public int Invoke(int nCommandID, int nPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[1];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara;
			return Invoke(nCommandID, array, array.Length);
		}

		public int Invoke(int nCommandID, int nPara1, int nPara2)
		{
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			return Invoke(nCommandID, array, array.Length);
		}

		public int Invoke(int nCommandID, string strValue)
		{
			IRayCmdParam[] array = new IRayCmdParam[1];
			array[0].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[0].var.val.strVal = strValue;
			return Invoke(nCommandID, array, array.Length);
		}

		public int Invoke(int nCommandID, int nPara, string strValue)
		{
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[1].var.val.strVal = strValue;
			return Invoke(nCommandID, array, array.Length);
		}

		public int Invoke(int nCommandID, int nPara1, int nPara2, string strValue)
		{
			IRayCmdParam[] array = new IRayCmdParam[3];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			array[2].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[2].var = default(IRayVariant);
			array[2].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[2].var.val.strVal = strValue;
			return Invoke(nCommandID, array, array.Length);
		}

		public int Invoke(int nCmdId, int noop, string strPara, int nPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[0].var.val.strVal = strPara;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCommandID, int nPara1, int nPara2, string strValue1, string strValue2)
		{
			IRayCmdParam[] array = new IRayCmdParam[4];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			array[2].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[2].var = default(IRayVariant);
			array[2].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[2].var.val.strVal = strValue1;
			array[3].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[3].var = default(IRayVariant);
			array[3].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[3].var.val.strVal = strValue2;
			return Invoke(nCommandID, array, array.Length);
		}

		public int Abort()
		{
			return SdkInterface.Abort(_nID);
		}

		public void OnSdkCallback(int nDetectorID, int nEventID, int nEventLevel, IntPtr pszMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (this.SdkCallbackEvent != null)
			{
				string strMsg = Marshal.PtrToStringAnsi(pszMsg);
				this.SdkCallbackEvent(this, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, pParam);
			}
		}

		public void ForceUpdatePropertyChanged(string strPropertyName, int attrID)
		{
			NotifyPropertyChanged(strPropertyName);
			AttrMonitorItem item = _attrChangingMonitor.FindItem(attrID);
			_attrChangingMonitor.UpdateUIItemValue(attrID, ref item);
		}

		protected void NotifyPropertyChanged(string strPropertyName)
		{
			PropertyChangedEventArgs e = new PropertyChangedEventArgs(strPropertyName);
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, e);
			}
		}

		protected void OnAttrValChanged(int nAttrID, string strAttrName)
		{
			NotifyPropertyChanged("Prop_" + strAttrName);
		}

		public int GetAttr(int nAttrID, ref IRayVariant pVar)
		{
			return SdkInterface.GetAttr(_nID, nAttrID, ref pVar);
		}

		public string GetAttrStrInfo(int nAttrID)
		{
			IRayVariant pVar = default(IRayVariant);
			SdkInterface.GetAttr(_nID, nAttrID, ref pVar);
			if (pVar.vt == IRAY_VAR_TYPE.IVT_INT)
			{
				return Convert.ToString(pVar.val.nVal);
			}
			if (IRAY_VAR_TYPE.IVT_FLT == pVar.vt)
			{
				return Convert.ToString(pVar.val.fVal);
			}
			if (IRAY_VAR_TYPE.IVT_STR == pVar.vt)
			{
				return pVar.val.strVal;
			}
			return null;
		}

		protected int SetAttr(int nAttrID, ref IRayVariant pVar)
		{
			return SdkInterface.SetAttr(_nID, nAttrID, ref pVar);
		}
	}
}
