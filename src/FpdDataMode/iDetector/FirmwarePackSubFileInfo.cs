using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct FirmwarePackSubFileInfo
	{
		public byte ucDeviceType;

		public byte ucDataType;

		public byte ucVerByte0;

		public byte ucVerByte1;

		public byte ucVerByte2;

		public byte ucVerByte3;

		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
		public string strDescription;

		public int nSubFileLength;
	}
}
