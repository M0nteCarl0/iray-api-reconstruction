using System.Collections.Generic;
using System.Xml;

namespace iDetector
{
	public class XmlParser
	{
		private XmlDocument xmlDoc;

		private bool LoadSuccess;

		public XmlParser()
		{
			xmlDoc = new XmlDocument();
		}

		public void LoadXml(string data)
		{
			if (data != null)
			{
				try
				{
					xmlDoc.LoadXml(data);
					LoadSuccess = true;
				}
				catch
				{
					LoadSuccess = false;
				}
			}
		}

		public void Load(string fileName)
		{
			if (fileName != null)
			{
				try
				{
					xmlDoc.Load(fileName);
					LoadSuccess = true;
				}
				catch
				{
					LoadSuccess = false;
				}
			}
		}

		public List<string> GetNodes(string path)
		{
			if (!LoadSuccess)
			{
				return null;
			}
			List<string> list = new List<string>();
			XmlNodeList xmlNodeList = xmlDoc.SelectNodes(path);
			foreach (XmlNode item in xmlNodeList)
			{
				list.Add(item.InnerText);
			}
			return list;
		}

		public string GetSingleNoteValue(string path)
		{
			if (!LoadSuccess)
			{
				return null;
			}
			try
			{
				return xmlDoc.SelectSingleNode(path).InnerText;
			}
			catch
			{
				return null;
			}
		}
	}
}
