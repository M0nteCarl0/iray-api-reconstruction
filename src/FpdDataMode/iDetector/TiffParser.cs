using System;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class TiffParser
	{
		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern IntPtr CreateImageMng();

		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern void ReleaseImageMng(IntPtr pMng);

		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern bool TiffOpen(IntPtr pMng, IntPtr path, int flag);

		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern void TiffClose(IntPtr pMng);

		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern void TiffWriteImage(IntPtr pMng, IntPtr pImg);

		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern void TiffReadImage(IntPtr pMng, IntPtr pImg, int index = 0);

		[DllImport("TiffParse.dll", CallingConvention = CallingConvention.Cdecl)]
		public static extern int TiffGetImageCount(IntPtr pMng);
	}
}
