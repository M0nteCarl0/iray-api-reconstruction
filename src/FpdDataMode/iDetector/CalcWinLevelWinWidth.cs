using System;
using System.Windows;

namespace iDetector
{
	public class CalcWinLevelWinWidth
	{
		private static int InvalidPercent = 10;

		private static uint[] grayCount;

		private static ushort[] Gray16Data;

		private static byte[] Gray8Data;

		public static bool GetWinLevelAndWidthByROIArea(ushort[] image, ref int winLevel, ref int winWidth, int width, int height, Point ROIStart, Point ROIEnd)
		{
			if (image == null)
			{
				return false;
			}
			Point point = new Point(Math.Min(ROIStart.X, ROIEnd.X), Math.Min(ROIStart.Y, ROIEnd.Y));
			Point point2 = new Point(Math.Max(ROIStart.X, ROIEnd.X), Math.Max(ROIStart.Y, ROIEnd.Y));
			int num = 8192;
			Utility.ReallocMemory(ref grayCount, num);
			Array.Clear(grayCount, 0, num);
			int num2 = (int)((point2.X - point.X) * (point2.Y - point.Y));
			for (int i = (int)point.Y; i < (int)point2.Y; i++)
			{
				for (int j = (int)point.X; j < (int)point2.X; j++)
				{
					grayCount[image[i * width + j] >> 3]++;
				}
			}
			uint num3 = (uint)((long)num2 * (long)InvalidPercent / 1000);
			if (num3 == 0)
			{
				num3 = (uint)num2 / 2u;
			}
			uint num4 = 0u;
			int num5 = 0;
			uint num6 = 0u;
			int num7 = 0;
			for (int k = 0; k < num; k++)
			{
				num4 += grayCount[k];
				if (num4 > num3)
				{
					num5 = k;
					break;
				}
			}
			num5 <<= 3;
			for (int l = 0; l < num; l++)
			{
				num6 += grayCount[num - l - 1];
				if (num6 > num3)
				{
					num7 = num - l - 1;
					break;
				}
			}
			num7 <<= 3;
			if (num7 <= num5)
			{
				winWidth = 100;
			}
			else
			{
				winWidth = num7 - num5;
			}
			winLevel = (num7 + num5) / 2;
			return true;
		}

		public static bool GetWinLevelAndWidth(ushort[] image, ref int winLevel, ref int winWidth, int width, int height)
		{
			return GetWinLevelAndWidthByROIArea(image, ref winLevel, ref winWidth, width, height, new Point(width / 4, height / 4), new Point(width / 2 + width / 4, height / 2 + height / 4));
		}

		public static byte[] GetGray8ImageByAutoWinLevelAndWidth(byte[] image, int width, int height)
		{
			if (image == null || image.Length == 0)
			{
				return null;
			}
			Utility.ReallocMemory(ref Gray16Data, image.Length / 2);
			Utility.Memcopy(image, Gray16Data);
			return GetGray8ImageByAutoWinLevelAndWidth(ref Gray16Data, width, height);
		}

		public static byte[] GetGray8ImageByAutoWinLevelAndWidth(ref ushort[] image, int width, int height)
		{
			if (image == null)
			{
				return null;
			}
			int winLevel = 0;
			int winWidth = 0;
			ushort nMaxIn = ushort.MaxValue;
			Utility.ReallocMemory(ref Gray8Data, image.Length);
			GetWinLevelAndWidth(image, ref winLevel, ref winWidth, width, height);
			CWLLUT cWLLUT = new CWLLUT(nMaxIn, byte.MaxValue, winWidth, winLevel);
			for (int i = 0; i < image.Length; i++)
			{
				Gray8Data[i] = cWLLUT.GetLUTResult(image[i]);
			}
			return Gray8Data;
		}
	}
}
