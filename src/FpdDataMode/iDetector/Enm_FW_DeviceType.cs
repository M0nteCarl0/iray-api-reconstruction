namespace iDetector
{
	public enum Enm_FW_DeviceType
	{
		Enm_FW_DeviceType_MainFPGA = 1,
		Enm_FW_DeviceType_ReadFPGA1 = 2,
		Enm_FW_DeviceType_ReadFPGA2 = 3,
		Enm_FW_DeviceType_ControlBox = 4,
		Enm_FW_DeviceType_MCU1 = 0x10,
		Enm_FW_DeviceType_AllInOne = 240,
		Enm_FW_DeviceType_None = 0xFF
	}
}
