using System;
using System.Collections.Generic;
using System.Threading;

namespace iDetector
{
	public class AttrValMonitor
	{
		public delegate void AttrValChangedEventHandler(int nAttrID, string strAttrName);

		private const float FLOAT_EPSILON = 1E-05f;

		private MMTimer QueryAttrsTimer;

		private int _nDetectorID;

		private Dictionary<int, AttrMonitorItem> _attrMap;

		private ReaderWriterLock _lock;

		public event AttrValChangedEventHandler AttrValChangedEvent;

		public AttrValMonitor(int nDetectorID)
		{
			_nDetectorID = nDetectorID;
			_attrMap = new Dictionary<int, AttrMonitorItem>();
			_lock = new ReaderWriterLock();
			InitiazeMap();
			if (_attrMap.Count == 0)
			{
				QueryAttrsTimer = new MMTimer(1000, delegate
				{
					Log.Instance().Write("stop query Attrs timer.");
					QueryAttrsTimer.Close();
					InitiazeMap();
					if (_attrMap.Count == 0)
					{
						Log.Instance().Write("Attr list is empty???");
					}
				});
				Log.Instance().Write("start query Attrs timer.");
				QueryAttrsTimer.Start();
			}
		}

		private void InitiazeMap()
		{
			_lock.AcquireWriterLock(2000);
			_attrMap.Clear();
			int pCount = 0;
			int attrsCount = SdkInterface.GetAttrsCount(_nDetectorID, ref pCount);
			if (pCount == 0)
			{
				Log.Instance().Write("GetAttrsCount failed! retCode:{0} count=0", attrsCount);
			}
			int[] array = new int[pCount];
			SdkInterface.GetAttrIDList(_nDetectorID, array, pCount);
			int[] array2 = array;
			foreach (int num in array2)
			{
				AttrMonitorItem attrMonitorItem = new AttrMonitorItem();
				attrMonitorItem.info = default(AttrInfo);
				attrMonitorItem.var = default(IRayVariant);
				attrMonitorItem.nRefCount = 0;
				if (SdkInterface.GetAttrInfo(_nDetectorID, num, ref attrMonitorItem.info) == 0 && SdkInterface.GetAttr(_nDetectorID, num, ref attrMonitorItem.var) == 0)
				{
					_attrMap.Add(num, attrMonitorItem);
				}
			}
			Log.Instance().Write("Detector[{0}] monitor {1} attributes", _nDetectorID, _attrMap.Count);
			_lock.ReleaseWriterLock();
		}

		public void AddRef(int nAttrID)
		{
			try
			{
				_lock.AcquireWriterLock(1000);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("AddRef failed: {0}", ex.Message);
				return;
			}
			AttrMonitorItem attrMonitorItem = FindItem(nAttrID);
			if (attrMonitorItem != null)
			{
				attrMonitorItem.nRefCount++;
			}
			_lock.ReleaseWriterLock();
		}

		public void AddRef(int[] nAttrIDs)
		{
			if (nAttrIDs == null)
			{
				return;
			}
			try
			{
				_lock.AcquireWriterLock(1000);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("AddRefs failed: {0}", ex.Message);
				return;
			}
			foreach (int nAttrID in nAttrIDs)
			{
				AttrMonitorItem attrMonitorItem = FindItem(nAttrID);
				if (attrMonitorItem != null)
				{
					attrMonitorItem.nRefCount++;
				}
			}
			_lock.ReleaseWriterLock();
		}

		public void ReleaseRef(int nAttrID)
		{
			try
			{
				_lock.AcquireWriterLock(1000);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("ReleaseRef failed: {0}", ex.Message);
				return;
			}
			AttrMonitorItem attrMonitorItem = FindItem(nAttrID);
			if (attrMonitorItem != null)
			{
				attrMonitorItem.nRefCount--;
				if (attrMonitorItem.nRefCount < 0)
				{
					attrMonitorItem.nRefCount = 0;
				}
			}
			_lock.ReleaseWriterLock();
		}

		public void ReleaseRef(int[] nAttrIDs)
		{
			if (nAttrIDs == null)
			{
				return;
			}
			try
			{
				_lock.AcquireWriterLock(1000);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("ReleaseRefs failed: {0} ", ex.Message);
				return;
			}
			foreach (int nAttrID in nAttrIDs)
			{
				AttrMonitorItem attrMonitorItem = FindItem(nAttrID);
				if (attrMonitorItem != null)
				{
					attrMonitorItem.nRefCount--;
					if (attrMonitorItem.nRefCount < 0)
					{
						attrMonitorItem.nRefCount = 0;
					}
				}
			}
			_lock.ReleaseWriterLock();
		}

		public AttrMonitorItem FindItem(int nAttrID)
		{
			try
			{
				_lock.AcquireReaderLock(1000);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("FindItem failed: {0}", ex.Message);
				return null;
			}
			if (!_attrMap.ContainsKey(nAttrID))
			{
				_lock.ReleaseReaderLock();
				if (nAttrID != 0)
				{
					Log.Instance().Write("FindItem failed! attrid: {0}", nAttrID);
				}
				return null;
			}
			_lock.ReleaseReaderLock();
			return _attrMap[nAttrID];
		}

		public void Update(bool bCheckRefCount)
		{
			try
			{
				_lock.AcquireReaderLock(1000);
			}
			catch (Exception ex)
			{
				Log.Instance().Write("Update attr failed: {0}", ex.Message);
				return;
			}
			int num = 0;
			foreach (KeyValuePair<int, AttrMonitorItem> item2 in _attrMap)
			{
				num = item2.Key;
				AttrMonitorItem item = item2.Value;
				if (bCheckRefCount && item.nRefCount <= 0)
				{
					if (5002 != num)
					{
					}
				}
				else
				{
					UpdateUIItemValue(num, ref item);
				}
			}
			_lock.ReleaseReaderLock();
		}

		public void UpdateUIItemValue(int nAttrID, ref AttrMonitorItem item)
		{
			if (item == null)
			{
				return;
			}
			IRayVariant pVar = default(IRayVariant);
			int attr = SdkInterface.GetAttr(_nDetectorID, nAttrID, ref pVar);
			if (attr == 0)
			{
				if (item.var.vt == IRAY_VAR_TYPE.IVT_INT)
				{
					if (pVar.val.nVal != item.var.val.nVal)
					{
						item.var = pVar;
						this.AttrValChangedEvent(nAttrID, item.info.strName);
					}
				}
				else if (item.var.vt == IRAY_VAR_TYPE.IVT_FLT)
				{
					if (Math.Abs(pVar.val.fVal - item.var.val.fVal) > 1E-05f)
					{
						item.var = pVar;
						this.AttrValChangedEvent(nAttrID, item.info.strName);
					}
				}
				else if (item.var.vt == IRAY_VAR_TYPE.IVT_STR)
				{
					if (pVar.val.strVal != item.var.val.strVal)
					{
						item.var = pVar;
						this.AttrValChangedEvent(nAttrID, item.info.strName);
					}
				}
				else
				{
					Log.Instance().Write("GetAttr[{0}] failed! Attr type is wrong[{1}]", nAttrID, item.var.vt);
				}
			}
			else
			{
				Log.Instance().Write("GetAttr[{0}] failed! Error:{1}", nAttrID, ErrorHelper.GetErrorDesp(attr));
			}
		}
	}
}
