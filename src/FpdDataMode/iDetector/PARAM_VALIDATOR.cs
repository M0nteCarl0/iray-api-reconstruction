namespace iDetector
{
	public enum PARAM_VALIDATOR
	{
		Validator_Null,
		Validator_MinMax,
		Validator_Enum,
		Validator_FilePath,
		Validator_IP,
		Validator_MAC,
		Validator_FpdSN
	}
}
