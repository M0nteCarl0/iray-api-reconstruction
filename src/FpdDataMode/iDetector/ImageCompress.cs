namespace iDetector
{
	public class ImageCompress : AImageCompress
	{
		public override void PicZoom(ref TPicRegion Dst, TPicRegion Src)
		{
			if (Dst.width == Src.width && Dst.height == Src.height)
			{
				Dst = Src;
				return;
			}
			for (long num = 0L; num < Dst.width; num++)
			{
				for (long num2 = 0L; num2 < Dst.height; num2++)
				{
					long x = num * Src.width / Dst.width;
					long y = num2 * Src.height / Dst.height;
					Dst.pdata[Dst.width * num2 + num] = Pixels(Src, x, y);
				}
			}
		}

		public void Test()
		{
			TPicRegion tPicRegion = default(TPicRegion);
			tPicRegion.stride = 4;
			tPicRegion.width = 4;
			tPicRegion.height = 4;
			TPicRegion src = tPicRegion;
			src.pdata = new byte[16]
			{
				1,
				2,
				3,
				4,
				5,
				6,
				7,
				8,
				9,
				10,
				11,
				12,
				13,
				14,
				15,
				16
			};
			TPicRegion tPicRegion2 = default(TPicRegion);
			tPicRegion2.stride = 2;
			tPicRegion2.width = 2;
			tPicRegion2.height = 2;
			TPicRegion Dst = tPicRegion2;
			Dst.pdata = new byte[4];
			PicZoom(ref Dst, src);
		}
	}
}
