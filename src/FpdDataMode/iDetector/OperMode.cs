using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Size = 1)]
	public struct OperMode
	{
		public const int READ = 0;

		public const int WRITE = 1;
	}
}
