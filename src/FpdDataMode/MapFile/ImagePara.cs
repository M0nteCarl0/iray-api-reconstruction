using iDetector;
using System;

namespace MapFile
{
	internal class ImagePara
	{
		public int Offset;

		public int Width;

		public int Height;

		public string FilePath;

		public Enum_ItemType FileType;

		public IRayVariantMapItem[] Para;

		public ImagePara(int _offset)
		{
			Offset = _offset;
		}

		public ImagePara(int _offset, int _width, int _height, IRayVariantMapItem[] _para)
		{
			Offset = _offset;
			if (_para != null)
			{
				Para = new IRayVariantMapItem[_para.Length];
				Array.Copy(_para, Para, _para.Length);
			}
			Width = _width;
			Height = _height;
		}

		public ImagePara(int _width, int _height, string path)
		{
			FilePath = path;
			Width = _width;
			Height = _height;
			FileType = Enum_ItemType.Cine;
		}
	}
}
