using iDetector;
using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;

namespace MapFile
{
	public class MapFileMgr
	{
		private MemoryMappedFile memoryFile;

		private MemoryMappedViewAccessor accessor;

		private int m_WriteOffset;

		private int mFrameSize;

		private int m_FileCount;

		private List<ImagePara> m_ImgQueueEx;

		private string MapName = "ImageMap";

		private Enum_FileStoreMedium ImageStoreMedium;

		private iThumbnailMedium StoreMedium;

		public MapFileMgr(int fileIndex, Enum_FileStoreMedium medium = Enum_FileStoreMedium.Memory)
		{
			m_ImgQueueEx = new List<ImagePara>();
			MapName += fileIndex.ToString();
			ImageStoreMedium = medium;
		}

		public void CreateFileMap(int nFileCount, int nFileSize)
		{
			if (nFileCount != 0)
			{
				m_FileCount = nFileCount;
				mFrameSize = nFileSize;
				if (StoreMedium != null)
				{
					StoreMedium.Close();
				}
				if (Enum_FileStoreMedium.Memory == ImageStoreMedium)
				{
					StoreMedium = new MemoryStoreThumbnail();
				}
				else
				{
					StoreMedium = new HardDiskStoreThumbnail();
				}
				try
				{
					StoreMedium.Create(MapName, nFileCount * nFileSize);
				}
				catch
				{
				}
			}
		}

		private void CreateMemoryMapFile(string name, int nMemoryMapSize)
		{
			try
			{
				DisposeMemoryMapFile();
				memoryFile = MemoryMappedFile.CreateOrOpen(name, nMemoryMapSize);
				accessor = memoryFile.CreateViewAccessor();
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
		}

		public void DisposeMemoryMapFile()
		{
			if (accessor != null)
			{
				accessor.Dispose();
			}
			if (memoryFile != null)
			{
				memoryFile.Dispose();
			}
			if (StoreMedium != null)
			{
				StoreMedium.Close();
			}
		}

		public void RemoveCineItem()
		{
			ImagePara imagePara = null;
			foreach (ImagePara item in m_ImgQueueEx)
			{
				if (Enum_ItemType.Cine == item.FileType)
				{
					imagePara = item;
					break;
				}
			}
			if (imagePara != null)
			{
				m_ImgQueueEx.Remove(imagePara);
			}
		}

		public void OccupiedImageList(int totalFrames)
		{
			m_ImgQueueEx.Clear();
			int num = 0;
			for (int i = 0; i < totalFrames; i++)
			{
				num = mFrameSize * i;
				m_ImgQueueEx.Add(new ImagePara(num));
			}
		}

		public void SaveImageToFile(IRayImageData image)
		{
			if (m_FileCount != 0 && StoreMedium != null)
			{
				if (m_ImgQueueEx.Count == m_FileCount)
				{
					ImagePara imagePara = m_ImgQueueEx[0];
					m_WriteOffset = imagePara.Offset;
					m_ImgQueueEx.RemoveAt(0);
				}
				try
				{
					StoreMedium.Write(m_WriteOffset, image.ImgData, 0, image.ImgData.Length);
					m_ImgQueueEx.Add(new ImagePara(m_WriteOffset, image.nWidth, image.nHeight, image.Params));
					m_WriteOffset = m_ImgQueueEx.Count * mFrameSize;
				}
				catch (Exception ex)
				{
					ex.ToString();
				}
			}
		}

		public void SaveImageToFile(int width, int height, string filePath)
		{
			if (m_FileCount != 0)
			{
				if (m_ImgQueueEx.Count == m_FileCount)
				{
					ImagePara imagePara = m_ImgQueueEx[0];
					m_WriteOffset = imagePara.Offset;
					m_ImgQueueEx.RemoveAt(0);
				}
				try
				{
					m_ImgQueueEx.Add(new ImagePara(width, height, filePath));
				}
				catch (Exception ex)
				{
					ex.ToString();
				}
			}
		}

		public void InsertImageToFile(int index, byte[] data, int width, int height, IRayVariantMapItem[] param)
		{
			if (index < m_ImgQueueEx.Count && StoreMedium != null)
			{
				int offset = m_ImgQueueEx[index].Offset;
				try
				{
					if (param != null && m_ImgQueueEx[index].Para != null)
					{
						Array.Copy(param, m_ImgQueueEx[index].Para, param.Length);
					}
					m_ImgQueueEx[index].Width = width;
					m_ImgQueueEx[index].Height = height;
					if (m_ImgQueueEx[index].FileType == Enum_ItemType.Image)
					{
						StoreMedium.Write(offset, data, 0, data.Length);
					}
				}
				catch (Exception ex)
				{
					ex.ToString();
				}
			}
		}

		public IRayImageData ReadImageFromFile(int index)
		{
			if (m_ImgQueueEx.Count == 0 || index < 0 || StoreMedium == null)
			{
				return null;
			}
			if (m_ImgQueueEx.Count <= index)
			{
				return null;
			}
			IRayImageData rayImageData = null;
			if (m_ImgQueueEx[index].FileType == Enum_ItemType.Image)
			{
				rayImageData = new IRayImageData(m_ImgQueueEx[index].Width, m_ImgQueueEx[index].Height);
				int offset = m_ImgQueueEx[index].Offset;
				StoreMedium.Read(offset, rayImageData.ImgData, 0, rayImageData.ImgData.Length);
				rayImageData.Params = m_ImgQueueEx[index].Para;
			}
			else
			{
				rayImageData = new IRayImageData();
				rayImageData.nWidth = m_ImgQueueEx[index].Width;
				rayImageData.nHeight = m_ImgQueueEx[index].Height;
			}
			return rayImageData;
		}

		public string ReadImageFromFile(int index, IRayImageData image, int width, int height)
		{
			if (m_ImgQueueEx.Count <= index || StoreMedium == null)
			{
				return null;
			}
			int num = width * height * 2;
			if (m_ImgQueueEx[index].FileType == Enum_ItemType.Image)
			{
				int offset = m_ImgQueueEx[index].Offset;
				if (num == image.ImgData.Length)
				{
					StoreMedium.Read(offset, image.ImgData, 0, num);
					image.Params = m_ImgQueueEx[index].Para;
				}
				return null;
			}
			return m_ImgQueueEx[index].FilePath;
		}

		public bool ReadImageFromFile(int index, byte[] data, int size)
		{
			if (index >= m_ImgQueueEx.Count || data == null || data.Length < size || StoreMedium == null)
			{
				return false;
			}
			int offset = m_ImgQueueEx[index].Offset;
			StoreMedium.Read(offset, data, 0, size);
			return true;
		}

		public void ResetFileMap()
		{
			m_ImgQueueEx.Clear();
			m_WriteOffset = 0;
		}
	}
}
