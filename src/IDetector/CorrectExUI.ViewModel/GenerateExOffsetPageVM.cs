using iDetector;
using System;
using System.Threading;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExOffsetPageVM : NotifyObject
	{
		private const int TaskTimeOutSeconds = 30;

		private int prepareTime;

		private bool prepareTimeVisibity;

		private string generatingInfo;

		private bool startProgressBarRoll;

		private bool createButtonEnable;

		private bool cancelButtonEnable;

		private bool progressBarVisibility;

		private string offsetMessageColor;

		private readonly string PrepareInfo = StringResource.FindString("PrepareToCrateOffset");

		private readonly string InitInfo = StringResource.FindString("CreatingOffset");

		private readonly string FinishedInfo = StringResource.FindString("GenerateOffsetSucceed");

		private readonly string FailedInfo = StringResource.FindString("GenerateOffsetFailed");

		private readonly string CancelInfo = StringResource.FindString("GenerateTemplateCancel");

		private DetecInfo Instance;

		private UITimer timer;

		private int TotalTime = 5;

		private bool bCorrectTemplating;

		private DlgTaskTimeOut CloseCreateWnd;

		private bool IsCancelAbort;

		private string WarningColor = "#EE7600";

		private string NormalColor = "";

		private int IsCancelOK;

		public int PrepareTime
		{
			get
			{
				return prepareTime;
			}
			set
			{
				prepareTime = value;
				NotifyPropertyChanged("PrepareTime");
			}
		}

		public bool PrepareTimeVisibity
		{
			get
			{
				return prepareTimeVisibity;
			}
			set
			{
				prepareTimeVisibity = value;
				NotifyPropertyChanged("PrepareTimeVisibity");
			}
		}

		public string GeneratingInfo
		{
			get
			{
				return generatingInfo;
			}
			set
			{
				generatingInfo = value;
				NotifyPropertyChanged("GeneratingInfo");
			}
		}

		public bool StartProgressBarRoll
		{
			get
			{
				return startProgressBarRoll;
			}
			set
			{
				startProgressBarRoll = value;
				NotifyPropertyChanged("StartProgressBarRoll");
			}
		}

		public bool CreateButtonEnable
		{
			get
			{
				return createButtonEnable;
			}
			set
			{
				createButtonEnable = value;
				NotifyPropertyChanged("CreateButtonEnable");
			}
		}

		public bool CancelButtonEnable
		{
			get
			{
				return cancelButtonEnable;
			}
			set
			{
				cancelButtonEnable = value;
				NotifyPropertyChanged("CancelButtonEnable");
			}
		}

		public bool ProgressBarVisibility
		{
			get
			{
				return progressBarVisibility;
			}
			set
			{
				progressBarVisibility = value;
				NotifyPropertyChanged("ProgressBarVisibility");
			}
		}

		public string OffsetMessageColor
		{
			get
			{
				return offsetMessageColor;
			}
			set
			{
				offsetMessageColor = value;
				NotifyPropertyChanged("OffsetMessageColor");
			}
		}

		public GenerateExOffsetPageVM(DetecInfo instance)
		{
			Instance = instance;
			timer = new UITimer(1, TimerTick);
			ButtonEnable(false);
			bCorrectTemplating = false;
			ProgressBarVisibility = false;
			IsCancelAbort = false;
			IsCancelOK = 0;
		}

		private void TimerTick()
		{
			lock (this)
			{
				if (PrepareTime-- == 0)
				{
					timer.Stop();
					CompletePrepare();
					IsCancelAbort = true;
					IsCancelOK = 1;
				}
			}
		}

		public void StartGenerateOffset(bool bEnableSkip)
		{
			Prepare(bEnableSkip);
			TotalTime = 5;
			PrepareTime = TotalTime;
			timer.Start();
			ButtonEnable(true);
			bCorrectTemplating = true;
			ProgressBarVisibility = true;
		}

		public void CancelGenerateOffset()
		{
			timer.Stop();
			StartProgressBarRoll = false;
			if (IsCancelAbort)
			{
				int num = Instance.Detector.Abort();
				if (1 == num)
				{
					OffsetMessageColor = WarningColor;
					GeneratingInfo = "Cancel current task...";
					IsCancelOK = 2;
				}
				else if (num != 0)
				{
					IsCancelOK = 0;
					OffsetMessageColor = WarningColor;
					GeneratingInfo = "Abort failed ! Err=" + ErrorHelper.GetErrorDesp(num);
				}
			}
			else
			{
				CancelGui();
			}
		}

		private void CancelGui()
		{
			OffsetMessageColor = WarningColor;
			GeneratingInfo = CancelInfo;
			PrepareTime = 0;
			ButtonEnable(false);
			bCorrectTemplating = false;
			ProgressBarVisibility = false;
			PrepareTimeVisibity = false;
			IsCancelAbort = false;
			IsCancelOK = 0;
		}

		private void Prepare(bool bEnableSkip)
		{
			PrepareTimeVisibity = true;
			StartProgressBarRoll = true;
			OffsetMessageColor = NormalColor;
			GeneratingInfo = PrepareInfo;
		}

		private void CompletePrepare()
		{
			PrepareTimeVisibity = false;
			OffsetMessageColor = NormalColor;
			GeneratingInfo = InitInfo;
			StartCrate();
		}

		private void StartCrate()
		{
			if (Instance.InvokeProxy != null)
			{
				int num = Instance.InvokeProxy.Invoke(3001);
				if (num != 0 && 1 != num)
				{
					GenerateFailed(num);
				}
				else
				{
					StartProgressBarRoll = true;
				}
			}
		}

		private void ButtonEnable(bool bEnable)
		{
			CreateButtonEnable = !bEnable;
			CancelButtonEnable = bEnable;
		}

		private void InitOffsetGui()
		{
			ButtonEnable(false);
			StartProgressBarRoll = false;
			PrepareTimeVisibity = false;
			OffsetMessageColor = NormalColor;
			GeneratingInfo = "";
		}

		private void GenerateSucceed()
		{
			OffsetMessageColor = NormalColor;
			GeneratingInfo = FinishedInfo;
			StartProgressBarRoll = false;
			ButtonEnable(false);
			bCorrectTemplating = false;
			ProgressBarVisibility = false;
			IsCancelAbort = false;
			IsCancelOK = 0;
		}

		private void GenerateFailed(int errorCode)
		{
			OffsetMessageColor = WarningColor;
			GeneratingInfo = FailedInfo;
			StartProgressBarRoll = false;
			ButtonEnable(false);
			ProgressBarVisibility = false;
			IsCancelAbort = false;
			IsCancelOK = 0;
			bCorrectTemplating = false;
		}

		public void AddCallbackHandler()
		{
			Instance.Detector.SdkCallbackEvent += OnSdkCallback;
		}

		public void RemoveCallbackHandler()
		{
			InitOffsetGui();
			Instance.Detector.SdkCallbackEvent -= OnSdkCallback;
		}

		public void Release()
		{
			timer.Stop();
			GC.Collect();
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			switch (nEventID)
			{
			case 6:
				if (bCorrectTemplating)
				{
					CancelGui();
				}
				break;
			case 4:
				if (nParam1 == 3001)
				{
					if (IsCancelOK == 1)
					{
						GenerateSucceed();
					}
					else if (bCorrectTemplating)
					{
						int num = 2023;
						Thread.Sleep(4000 + num);
						CancelGui();
					}
				}
				else if (bCorrectTemplating)
				{
					CancelGui();
				}
				break;
			case 5:
				if (nParam1 == 3001)
				{
					GenerateFailed(nParam2);
				}
				else if (bCorrectTemplating)
				{
					CancelGui();
				}
				break;
			}
		}

		public void DoCancel()
		{
			OffsetMessageColor = WarningColor;
			GeneratingInfo = "The correction has been initialized and can not be switched!";
		}

		public bool GetCancelStatus()
		{
			return bCorrectTemplating;
		}

		public void SetCancelStatus(bool bValue)
		{
			bCorrectTemplating = bValue;
			CancelGui();
		}
	}
}
