using iDetector;
using System;
using System.Windows;

namespace CorrectExUI.ViewModel
{
	internal class LoadLocalDefectTemporaryFileEx
	{
		protected bool[,] LocalTemplateFileStatus;

		private UpdateStatusProgress UpdateLoadLocalStatus;

		private int curLocalFileIndex;

		private int OneGroupDefectImageNumber;

		private int GroupNumber;

		private int TotalDefectFiles;

		private Window MessageBoxOwner;

		private int TaskTimeOutSeconds;

		public int CurLocalFileIndex
		{
			get
			{
				return curLocalFileIndex;
			}
		}

		public int CurGroupIndex
		{
			get
			{
				return curLocalFileIndex / OneGroupDefectImageNumber;
			}
		}

		public event Action<LoadLocalFileStatusEx> LoadNextGroupFiles;

		public LoadLocalDefectTemporaryFileEx(Window owner, int groupNumber, int oneGroupDefectImageNumber, int timeoutSeconds)
		{
			MessageBoxOwner = owner;
			GroupNumber = groupNumber;
			TaskTimeOutSeconds = timeoutSeconds;
			OneGroupDefectImageNumber = oneGroupDefectImageNumber;
			TotalDefectFiles = GroupNumber * OneGroupDefectImageNumber;
			LocalTemplateFileStatus = new bool[groupNumber, oneGroupDefectImageNumber];
		}

		public void InitLocalFile()
		{
			curLocalFileIndex = 0;
			Array.Clear(LocalTemplateFileStatus, 0, LocalTemplateFileStatus.Length);
		}

		public void StartLoadLocalFile()
		{
			UpdateLoadLocalStatus = new UpdateStatusProgress(MessageBoxOwner, "Load local file", 100, TaskTimeOutSeconds * 1000);
			UpdateLoadLocalStatus.RunTask();
			UpdateLoadLocalStatus.SetStatus("Start load local template file");
		}

		public void StopLoadLocalFile()
		{
			UpdateLoadLocalStatus.StopTask();
		}

		public bool IsAllFileLoaded()
		{
			if (curLocalFileIndex >= TotalDefectFiles)
			{
				return true;
			}
			return false;
		}

		public void LoadNextLocalFile(bool prevStatus)
		{
			int num = curLocalFileIndex / OneGroupDefectImageNumber;
			int num2 = curLocalFileIndex % OneGroupDefectImageNumber;
			LocalTemplateFileStatus[num, num2] = prevStatus;
			curLocalFileIndex++;
			if (curLocalFileIndex % OneGroupDefectImageNumber == 0 && this.LoadNextGroupFiles != null)
			{
				this.LoadNextGroupFiles(CheckLocalFileCompleteness(num));
			}
			string str = "Load local file:" + curLocalFileIndex.ToString() + "/" + TotalDefectFiles.ToString();
			str = ((!prevStatus) ? (str + " - Failed") : (str + " - Succeed"));
			UpdateLoadLocalStatus.SetStatus(str);
		}

		public LoadLocalFileStatusEx CheckLocalFileCompleteness(int groupIndex)
		{
			int num = 0;
			for (int i = 0; i < OneGroupDefectImageNumber; i++)
			{
				if (!LocalTemplateFileStatus[groupIndex, i])
				{
					num++;
				}
			}
			if (num == OneGroupDefectImageNumber)
			{
				return LoadLocalFileStatusEx.None;
			}
			if (num == 0)
			{
				return LoadLocalFileStatusEx.Ok;
			}
			return LoadLocalFileStatusEx.Incomplete;
		}
	}
}
