using CorrectUI;
using iDetector;
using System.Windows.Forms;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExDefectPageVM : GenerateExStaticTemplate
	{
		private const int DoseCount = 4;

		private DoseInfo[] DefectDoseList;

		private string[] KVList;

		private int[] ExpectedGrayList;

		private string LocalFilePath;

		private SyncLoadLocalDefectTemporaryFileEx LoadLocalFile;

		private bool LoadLocalFileFlag;

		private bool AllLoadFinishedFlag;

		private DoseAlgorithm DoseAlgorithm;

		private int nLoadStage;

		public GenerateExDefectPageVM(DetecInfo instance)
			: base(instance)
		{
			InitDoseInfo();
			base.LoadImageFromFile = new DelegateCommand(OnLoadImageFromFile);
			ExpectedGrayList = GetExpectedGrayValue(instance.Detector);
			int prop_Attr_DefectTotalFrames = instance.Detector.Prop_Attr_DefectTotalFrames;
			prop_Attr_DefectTotalFrames = ((prop_Attr_DefectTotalFrames == 0) ? 19 : prop_Attr_DefectTotalFrames);
			KVList = new string[prop_Attr_DefectTotalFrames];
			LoadKVList(KVList);
			DefectDoseList = new DoseInfo[4];
			for (int i = 0; i < 4; i++)
			{
				uint result = 0u;
				if (uint.TryParse(KVList[i], out result))
				{
					DefectDoseList[i] = new DoseInfo(string.Format("{0}kV", KVList[i]));
				}
				else
				{
					DefectDoseList[i] = new DoseInfo(KVList[i]);
				}
			}
			LoadLocalFileFlag = false;
			AllLoadFinishedFlag = true;
			InitCheckDownload(2);
		}

		private void InitDoseInfo()
		{
			string str = "mAs";
			string lpAppName = "Algorithm";
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncInAlgorithm";
				}
				else if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncOutAlgorithm";
				}
				break;
			}
			DoseAlgorithm = new DoseAlgorithm();
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			uint privateProfileInt = IniParser.GetPrivateProfileInt("Study", "DoseCount", 3, lpFileName);
			for (int i = 0; i < privateProfileInt; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Study", lpKeyName, 0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_1";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_2";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = "Coeff" + (i + 1).ToString();
				double privateProfileDouble = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				lpKeyName = "Intercept" + (i + 1).ToString();
				double privateProfileDouble2 = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				DoseAlgorithm.AddCoeffItem(privateProfileInt2, privateProfileDouble, privateProfileDouble2);
			}
			DoseAlgorithm.UpdateTimeCoeff(IniParser.GetPrivateProfileDouble(lpAppName, "TimeCoeff", 1.0, lpFileName));
		}

		private int[] GetExpectedGrayValue(Detector detector)
		{
			int[] array = null;
			switch (detector.Prop_Attr_UROM_ProductNo)
			{
			case 45:
				array = new int[4]
				{
					4000,
					4000,
					16000,
					40000
				};
				break;
			case 41:
			case 42:
			case 51:
			case 52:
			case 60:
			case 61:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				array = new int[4]
				{
					4000,
					4000,
					16000,
					20000
				};
				break;
			default:
				array = new int[4]
				{
					1000,
					1000,
					4000,
					11000
				};
				break;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			if (IniParser.GetPrivateProfileInt("Defect_DQE", "DQE_Enable", 0, lpFileName) != 0)
			{
				for (int i = 0; i < 4; i++)
				{
					array[i] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Expected", i + 1), array[i], lpFileName);
				}
			}
			return array;
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i < 4)
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Defect", lpKeyName, "", lpFileName);
					ExpectedGrayList[i] = (int)IniParser.GetPrivateProfileInt("Defect", string.Format("Gray{0}", i + 1), ExpectedGrayList[i], lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Defect", lpKeyName, kvlist[3], lpFileName);
				}
			}
		}

		private void OnLoadImageFromFile(object obj)
		{
			FolderBrowserDialogEx folderBrowserDialogEx = new FolderBrowserDialogEx();
			folderBrowserDialogEx.SelectedPath = LocalFilePath;
			folderBrowserDialogEx.Description = "Select a location to load the image files.";
			folderBrowserDialogEx.ShowNewFolderButton = true;
			folderBrowserDialogEx.ShowFullPathInEditBox = true;
			if (DialogResult.OK != folderBrowserDialogEx.ShowDialog())
			{
				return;
			}
			if (folderBrowserDialogEx.SelectedPath == string.Empty)
			{
				MessageBox.Show("Path cannot be empty!");
				return;
			}
			if (DetInstance.Detector == null)
			{
				MessageBox.Show("Detector instance is null!");
				return;
			}
			base.AcquiedProgress = "";
			for (int i = 0; i < AcquiredStatus.Count; i++)
			{
				AcquiredStatus[i] = false;
			}
			LocalFilePath = folderBrowserDialogEx.SelectedPath;
			LoadLocalFile = new SyncLoadLocalDefectTemporaryFileEx(DetInstance, folderBrowserDialogEx.SelectedPath, DetInstance.Detector.Prop_Attr_DefectTotalFrames, 1, 500);
			LoadLocalFile.GroupLoadFinished += OnGroupLoadFinished;
			ImageSelectedIndex = 0;
			AllLoadFinishedFlag = false;
			StartLoadLocalFile();
			nLoadStage = 0;
		}

		private void StartLoadLocalFile()
		{
			LoadLocalFileFlag = true;
			LoadLocalFile.StartLoadLocalFile();
		}

		private void OnGroupLoadFinished(bool bAllFinished, int groupIndex, LoadLocalFileStatusEx status)
		{
			if (LoadLocalFileStatusEx.Ok != status)
			{
				ImageSelectedIndex = groupIndex + 1;
				if (groupIndex + 1 == DetInstance.Detector.Prop_Attr_DefectTotalFrames)
				{
					LoadLocalFileFlag = false;
				}
			}
			else
			{
				nLoadStage++;
			}
			if (nLoadStage == GroupNumber)
			{
				base.EnableGenerateBtn = true;
			}
			else
			{
				base.EnableGenerateBtn = false;
			}
			base.StageValue = nLoadStage.ToString() + "/" + GroupNumber.ToString();
			if (bAllFinished)
			{
				AllLoadFinishedFlag = true;
				LoadLocalFileFlag = false;
				base.EnableAcquireBtn = false;
				base.EnableAcceptBtn = false;
				base.EnableCheckDownload = true;
				base.EnableLoadFileBtn = false;
				base.EnableGenerateBtn = false;
				if (nLoadStage == GroupNumber)
				{
					ImageSelectedIndex = GroupNumber;
					OnClickGenerateBtn();
				}
				else
				{
					string text = "Load the images error(" + nLoadStage.ToString() + "/" + GroupNumber.ToString() + "), and cancel the correction interface!";
					MessageBox.Show(text);
					OnClickCancelBtn();
				}
			}
		}

		protected override void InitFinished()
		{
			InitImagelist(KVList);
			PrepareForAcquire();
			LoadLocalFileFlag = false;
			AllLoadFinishedFlag = true;
			base.InitFinished();
		}

		protected override void OnClickStartBtn()
		{
			InitForCreateTemplate(3007, "DEFECT");
			if (InitedStatus)
			{
				SetDoseInfo(DefectDoseList[0].GetDoseInfo());
				ExpectedGrayList = GetExpectedGrayValue(DetInstance.Detector);
			}
			else
			{
				base.InitFinished();
				base.OnClickCancelBtn();
				ClearGuiInitFailed();
			}
		}

		protected override void OnClickCancelBtn()
		{
			base.OnClickCancelBtn();
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			int num = (frameIndex < 4) ? frameIndex : 3;
			SetDoseInfo(DefectDoseList[num].GetDoseInfo());
		}

		protected override int SelectImage(int frameIndex)
		{
			if (LoadLocalFileFlag)
			{
				return 0;
			}
			return DetInstance.InvokeProxy.Invoke(3009, frameIndex);
		}

		protected override void SelectImageSucceed()
		{
			base.SelectImageSucceed();
			if (AllLoadFinishedFlag)
			{
				LoadLocalFileFlag = false;
			}
		}

		protected override void SelectImageFailed()
		{
			base.SelectImageFailed();
			if (AllLoadFinishedFlag)
			{
				LoadLocalFileFlag = false;
			}
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			frameIndex = ((frameIndex < 4) ? frameIndex : 3);
			return ExpectedGrayList[frameIndex];
		}

		public void Clear()
		{
			if (LoadLocalFile != null)
			{
				LoadLocalFile.GroupLoadFinished -= OnGroupLoadFinished;
			}
		}

		protected override GrayValueStatus IsRealGrayBeyondExpected(out int realValue, out int expectedValue)
		{
			GrayValueStatus result = base.IsRealGrayBeyondExpected(out realValue, out expectedValue);
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			if (IniParser.GetPrivateProfileInt("Defect_DQE", "DQE_Enable", 0, lpFileName) != 0)
			{
				int num = (ImageSelectedIndex < 4) ? (ImageSelectedIndex + 1) : 4;
				int[] array = new int[8]
				{
					-2147483648,
					0,
					0,
					0,
					0,
					65535,
					65535,
					2147483647
				};
				array[3] = (int)(0.9f * (float)expectedValue);
				array[4] = (int)(1.1f * (float)expectedValue);
				int[] array2 = array;
				array2[6] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Deny_Upper", num), array2[6], lpFileName);
				array2[5] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Warn_Upper", num), array2[5], lpFileName);
				array2[4] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Good_Upper", num), array2[4], lpFileName);
				array2[3] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Good_Below", num), array2[3], lpFileName);
				array2[2] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Warn_Below", num), array2[2], lpFileName);
				array2[1] = (int)IniParser.GetPrivateProfileInt("Defect_DQE", string.Format("KV{0}_Deny_Below", num), array2[1], lpFileName);
				int i;
				for (i = 1; i < array2.Length && (realValue < array2[i - 1] || realValue > array2[i]); i++)
				{
				}
				switch (i)
				{
				case 1:
				case 2:
					result = GrayValueStatus.DenyBelow;
					break;
				case 3:
					result = GrayValueStatus.WarnBelow;
					break;
				case 4:
					result = GrayValueStatus.Expected;
					break;
				case 5:
					result = GrayValueStatus.WarnAbove;
					break;
				case 6:
				case 7:
					result = GrayValueStatus.DenyAbove;
					break;
				}
			}
			return result;
		}
	}
}
