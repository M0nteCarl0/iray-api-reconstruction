namespace CorrectExUI.ViewModel
{
	public struct DoseInfo
	{
		private string mDoseInfo;

		public DoseInfo(string doseinfo)
		{
			mDoseInfo = doseinfo;
		}

		public string GetDoseInfo()
		{
			return mDoseInfo;
		}
	}
}
