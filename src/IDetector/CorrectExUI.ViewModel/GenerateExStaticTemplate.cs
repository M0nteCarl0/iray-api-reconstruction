using iDetector;
using System;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExStaticTemplate : GenerateExTemplatePageVM
	{
		private bool Prepared;

		private bool bPostEnable;

		public GenerateExStaticTemplate(DetecInfo instance)
			: base(instance)
		{
			Prepared = false;
			base.ProgressValueVisibility = false;
			base.ExProgressVisibility = false;
		}

		protected override void StartExposeWindowTimerAfterStartAcquire()
		{
			if (DetInstance.Detector != null)
			{
				switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
				{
				}
			}
		}

		protected override void StartExposeWindowTimerAfterExpEnable(int nCallbackDelayTime)
		{
			if (DetInstance.Detector == null)
			{
				return;
			}
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Mercu1616TE)
			{
				StartExposeWindowTimerNoCancel((int)((double)DetInstance.Detector.Prop_Cfg_ClearAcqParam_DelayTime / 1000.0));
				return;
			}
			if (DetInstance.Detector.Prop_Attr_UROM_TriggerMode == 3 || DetInstance.Detector.Prop_Attr_UROM_TriggerMode == 5)
			{
				int nDelayTime = (int)((double)DetInstance.Detector.Prop_Cfg_ClearAcqParam_DelayTime / 1000.0);
				StartExposeWindowTimerNoCancel(nDelayTime);
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 4:
				break;
			case 0:
			case 3:
				if (52 == DetInstance.Detector.Prop_Attr_UROM_ProductNo || 51 == DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
					StartExposeWindowTimer();
				}
				break;
			case 2:
				Prepared = true;
				if (!bPostEnable)
				{
					int nDelayTime2 = (int)((double)DetInstance.Detector.Prop_Attr_UROM_ExpWindowTime / 1000.0);
					StartExposeWindowTimerNoCancel(nDelayTime2);
				}
				break;
			case 1:
			case 5:
				StartExposeWindowTimer();
				break;
			}
		}

		protected override double GetExposeWindowLength()
		{
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Mercu1616TE)
			{
				return (double)DetInstance.Detector.Prop_Cfg_ClearAcqParam_DelayTime / 1000.0;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 0:
			case 3:
				return 10.0;
			case 1:
			case 2:
				return (double)DetInstance.Detector.Prop_Attr_UROM_ExpWindowTime / 1000.0;
			case 5:
				return (double)DetInstance.Detector.Prop_Cfg_ClearAcqParam_DelayTime / 1000.0;
			default:
				return 0.0;
			}
		}

		protected override void EnterExposeView()
		{
			if (AcquiredStatus != null && AcquiredStatus.Count > ImageSelectedIndex)
			{
				AcquiredStatus[ImageSelectedIndex] = false;
			}
			Prepared = false;
			IsInExposeView = true;
			base.AcquireBtnContent = ExposeStr;
			base.ImageViewVisibility = false;
			SetDoseInfo(ImageSelectedIndex);
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Mercu1616TE)
			{
				base.EnableAcquireBtn = true;
				return;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 1:
			case 4:
				break;
			case 0:
			case 3:
				if (45 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 41 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 61 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 52 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 51 != DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
					base.EnableAcquireBtn = false;
				}
				break;
			case 2:
				base.EnableAcquireBtn = true;
				break;
			case 5:
				if (this is GenerateExGainPageVM)
				{
					if (4 == DetInstance.Detector.Prop_Attr_UROM_FreesyncSubFlow)
					{
						base.EnableAcquireBtn = true;
					}
					else
					{
						base.EnableAcquireBtn = false;
					}
				}
				else if (this is GenerateExDefectPageVM && 3 == DetInstance.Detector.Prop_Attr_UROM_FreesyncSubFlow)
				{
					base.EnableAcquireBtn = false;
				}
				break;
			}
		}

		protected override void EnterPreViewImageView()
		{
			CloseMessageBox();
			IsInExposeView = false;
			base.AcquireBtnContent = ExposeStr;
			base.ImageViewVisibility = true;
		}

		protected override void OnClickPerpAcquireBtn()
		{
			if (IsInExposeView)
			{
				int num = 0;
				bool flag = false;
				Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
				if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Mercu1616TE)
				{
					num = DetInstance.InvokeProxy.Invoke(1002);
					flag = true;
				}
				if (!flag)
				{
					switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
					{
					case 0:
					case 3:
						switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
						{
						case 45:
							num = DetInstance.InvokeProxy.Invoke(3023);
							break;
						case 41:
						case 61:
							num = DetInstance.InvokeProxy.Invoke(3030);
							break;
						case 51:
						case 52:
							num = DetInstance.InvokeProxy.Invoke(1003);
							break;
						}
						break;
					case 1:
						num = ((52 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 51 != DetInstance.Detector.Prop_Attr_UROM_ProductNo) ? DetInstance.InvokeProxy.Invoke(1001) : ((DetInstance.Detector.Prop_Attr_UROM_InnerSubFlow != 0) ? DetInstance.InvokeProxy.Invoke(1002) : DetInstance.InvokeProxy.Invoke(1004)));
						break;
					case 2:
						bPostEnable = Prepared;
						if (Prepared)
						{
							_startAcqFlag = true;
							num = DetInstance.InvokeProxy.Invoke(1004);
						}
						else
						{
							_startAcqFlag = false;
							num = DetInstance.InvokeProxy.Invoke(1001);
						}
						break;
					case 5:
						num = DetInstance.InvokeProxy.Invoke(1002);
						break;
					default:
						num = DetInstance.InvokeProxy.Invoke(1004);
						break;
					}
				}
				if (1 == num || num == 0)
				{
					StartExposeWindowTimerAfterStartAcquire();
					base.EnableAcquireBtn = false;
					base.EnableLoadFileBtn = false;
					base.EnableAcceptBtn = false;
				}
				int num2 = 1;
				if (bPostEnable)
				{
					StopExposeWindowTimer();
				}
			}
			else
			{
				EnterExposeView();
			}
		}

		protected override void ResumePrepStage()
		{
			Prepared = false;
			base.AcquireBtnContent = ExposeStr;
		}

		protected override void CollectImage()
		{
			OnShowImage();
			AcquiredImage();
		}

		protected override void AcquiredImage()
		{
			StopTimer();
			EnterPreViewImageView();
			base.AcquiredImage();
			if (DQEDenied)
			{
				SelectImageFailed();
			}
			else
			{
				DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
				{
					int num = SelectImage(ImageSelectedIndex);
					if (num == 0)
					{
						SelectImageSucceed();
					}
					else if (1 != num)
					{
						SelectImageFailed();
						switch (num)
						{
						case 3003:
						{
							string text2 = base.MessageInfo = StringResource.FindString("ExpLineErrorInfo");
							base.MessageColor = WarningColor;
							base.CurrentValueColor = ErrorCurrentValueColor;
							break;
						}
						case 3004:
							EnterExposeView();
							break;
						}
					}
				}, new object[0]);
			}
		}

		protected override void InitFinished()
		{
			base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
			base.ExpectedValue = GetExpectedGray(0).ToString();
			base.InitFinished();
		}

		protected override void PrepareForAcquire()
		{
			base.PrepareForAcquire();
			base.CurrentValue = "";
			base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
		}
	}
}
