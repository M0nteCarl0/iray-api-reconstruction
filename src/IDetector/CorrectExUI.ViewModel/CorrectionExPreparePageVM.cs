using iDetector;
using System;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CorrectExUI.ViewModel
{
	internal class CorrectionExPreparePageVM : NotifyObject
	{
		private Detector Instance;

		private ImageSource exposeTableImg;

		public ObservableCollection<object> ShootConditions
		{
			get;
			set;
		}

		public ImageSource ExposeTableImg
		{
			get
			{
				return exposeTableImg;
			}
			set
			{
				exposeTableImg = value;
				NotifyPropertyChanged("ExposeTableImg");
			}
		}

		public CorrectionExPreparePageVM(Detector instance)
		{
			Instance = instance;
		}

		public void InitializeGui()
		{
			if (!LoadDemoExposeGraphic())
			{
				ExposeTableImg = new BitmapImage(new Uri("/Images/ExposeTable.jpg", UriKind.Relative));
			}
			LoadShootConditions();
		}

		public bool LoadDemoExposeGraphic()
		{
			if (Instance != null && Utility.IsFileExisted(Instance.Prop_Attr_WorkDir + "\\Others\\ExposeTable.jpg"))
			{
				ExposeTableImg = new BitmapImage(new Uri(Instance.Prop_Attr_WorkDir + "\\Others\\ExposeTable.jpg", UriKind.Absolute));
				return true;
			}
			return false;
		}

		private void LoadShootConditions()
		{
			string lpFileName = Instance.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			ShootConditions = new ObservableCollection<object>();
			int num = 1;
			if (IniParser.IsSectionExisted("CreateTemplateCondition", lpFileName))
			{
				while (true)
				{
					string privateProfileString = IniParser.GetPrivateProfileString("CreateTemplateCondition", string.Format("condition{0}", num), "", lpFileName);
					if (string.IsNullOrEmpty(privateProfileString))
					{
						break;
					}
					ShootConditions.Add(new ConditionText(string.Format("{0}.{1}", num, privateProfileString)));
					num++;
				}
			}
			else
			{
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition1")));
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition2")));
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition3")));
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition4")));
			}
		}
	}
}
