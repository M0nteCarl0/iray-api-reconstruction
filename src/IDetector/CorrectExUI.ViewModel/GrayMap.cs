namespace CorrectExUI.ViewModel
{
	internal class GrayMap
	{
		public int KVValue;

		public int GrayValue;

		public int Frames;

		public GrayMap(int grayValue, int kv, int frames)
		{
			GrayValue = grayValue;
			Frames = frames;
			KVValue = kv;
		}
	}
}
