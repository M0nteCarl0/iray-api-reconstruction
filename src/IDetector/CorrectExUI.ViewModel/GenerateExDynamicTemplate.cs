using CorrectUI.ViewModel;
using iDetector;
using System;
using System.Windows.Forms;

namespace CorrectExUI.ViewModel
{
	public class GenerateExDynamicTemplate : GenerateExTemplatePageVM
	{
		protected enum Enm_CreateStage
		{
			PrepExpose = 1,
			Acquiring,
			Selecting,
			SelectFinished,
			Finished
		}

		private const int MonitorTime = 40;

		public const int GroupNumbers = 3;

		protected readonly string SelectImageForGain = StringResource.FindString("SelectImageForGain");

		protected Enm_CreateStage CurStage;

		private IProgress ValidImageMonitor;

		private int OneGroupDefectImageNumber;

		private bool IsAcquireLightImage;

		private string LocalFilePath;

		private SyncLoadLocalDefectTemporaryFileEx LoadLocalFile;

		protected int StartValidDftFrameNumber;

		private int nDynamicType;

		private int nLoadStage;

		protected bool EnableBufferCine;

		private int ValidCount;

		public event Action ClosePlaybar;

		public event Action<int> StartStoreImage;

		public event Action<bool> StopStoreImage;

		public GenerateExDynamicTemplate(DetecInfo instance)
			: base(instance)
		{
			EnterPrepExposeStage(true);
			base.LoadImageFromFile = new DelegateCommand(OnLoadImageFromFile);
			nDynamicType = 0;
		}

		protected override void InitFinished()
		{
			OneGroupDefectImageNumber = DetInstance.Detector.Prop_Attr_DefectTotalFrames / 3;
			if (nDynamicType == 1 || nDynamicType == 2)
			{
				base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
				base.ExpectedValue = GetExpectedGray(0).ToString();
			}
			else
			{
				base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + 3.ToString();
			}
			PrepareForAcquire();
			base.InitFinished();
		}

		private void OnLoadImageFromFile(object obj)
		{
			FolderBrowserDialogEx folderBrowserDialogEx = new FolderBrowserDialogEx();
			folderBrowserDialogEx.SelectedPath = LocalFilePath;
			folderBrowserDialogEx.Description = "Select a location to load the image files.";
			folderBrowserDialogEx.ShowNewFolderButton = true;
			folderBrowserDialogEx.ShowFullPathInEditBox = true;
			if (DialogResult.OK != folderBrowserDialogEx.ShowDialog())
			{
				return;
			}
			if (folderBrowserDialogEx.SelectedPath == string.Empty)
			{
				MessageBox.Show("Path cannot be empty!");
				return;
			}
			if (DetInstance.Detector == null)
			{
				MessageBox.Show("Detector instance is null!");
				return;
			}
			base.AcquiedProgress = "";
			for (int i = 0; i < AcquiredStatus.Count; i++)
			{
				AcquiredStatus[i] = false;
			}
			LocalFilePath = folderBrowserDialogEx.SelectedPath;
			int groupNumbers = 0;
			int eachGroupDefectImageNumber = 0;
			GetGroupInfo(out groupNumbers, out eachGroupDefectImageNumber);
			LoadLocalFile = new SyncLoadLocalDefectTemporaryFileEx(DetInstance, folderBrowserDialogEx.SelectedPath, groupNumbers, eachGroupDefectImageNumber, 100);
			LoadLocalFile.GroupLoadFinished += OnGroupLoadFinished;
			ImageSelectedIndex = 0;
			StartLoadLocalFile();
			EnableBufferCine = true;
			ValidCount = 0;
			nLoadStage = 0;
		}

		protected virtual void GetGroupInfo(out int groupNumbers, out int eachGroupDefectImageNumber)
		{
			groupNumbers = 3;
			eachGroupDefectImageNumber = OneGroupDefectImageNumber / 2;
		}

		private void OnAllLoadFinised(LoadLocalFileStatus status)
		{
			if (LoadLocalFileStatus.Ok == status)
			{
				StopStore(false);
				GroupAcquireFinished();
			}
			else
			{
				AcquiredStatus[ImageSelectedIndex] = false;
				StopStore(false);
			}
		}

		private void OnGroupLoadFinished(bool bAllFinished, int groupIndex, LoadLocalFileStatusEx status)
		{
			switch (status)
			{
			case LoadLocalFileStatusEx.Ok:
				StopStore();
				GroupAcquireFinished();
				nLoadStage++;
				break;
			case LoadLocalFileStatusEx.None:
				StopStore(false);
				break;
			case LoadLocalFileStatusEx.Incomplete:
				base.AcquiedProgress = "Local file is incomplete";
				AcquiredStatus[ImageSelectedIndex] = false;
				StopStore(false);
				break;
			}
			if (bAllFinished)
			{
				EnterSelectFinsihedStage(true);
				ImageSelectedIndex = 0;
				base.EnableAcquireBtn = false;
				base.EnableAcceptBtn = false;
				base.EnableCheckDownload = true;
			}
			else
			{
				ImageSelectedIndex = groupIndex + 1;
				StartStore();
			}
			if (nLoadStage == GroupNumber)
			{
				ImageSelectedIndex = GroupNumber;
				base.EnableGenerateBtn = true;
			}
			else
			{
				base.EnableGenerateBtn = false;
			}
			base.StageValue = nLoadStage.ToString() + "/" + GroupNumber.ToString();
		}

		private void StartLoadLocalFile()
		{
			StartStore();
			LoadLocalFile.StartLoadLocalFile();
		}

		protected override void ExposeTimerEndCallback()
		{
			if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("WaitImage"), true, TaskCanceledHandler);
			}
			else
			{
				base.ExposeTimerEndCallback();
			}
		}

		private void TaskCanceledHandler()
		{
			EnterPrepExposeStage(true);
			int num = DetInstance.InvokeProxy.Invoke(1005);
			int num2 = 1;
		}

		protected override void StartExposeWindowTimerAfterStartAcquire()
		{
			ExposeTimerEndCallback();
		}

		protected override double GetExposeWindowLength()
		{
			return 0.1;
		}

		protected override void EnterExposeView()
		{
			if (AcquiredStatus.Count > ImageSelectedIndex)
			{
				AcquiredStatus[ImageSelectedIndex] = false;
			}
			if (this.ClosePlaybar != null)
			{
				this.ClosePlaybar();
			}
			IsInExposeView = true;
			base.AcquireBtnContent = ExposeStr;
			EnterPrepExposeStage(true);
			UpdateProgress(0);
			base.ImageViewVisibility = false;
			IsAcquireLightImage = true;
			SetDoseInfo(ImageSelectedIndex);
			if (nDynamicType == 1 || nDynamicType == 2)
			{
				base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
				base.ExpectedValue = GetExpectedGray(0).ToString();
			}
			else
			{
				base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + 3.ToString();
			}
		}

		protected override void EnterPreViewImageView()
		{
			CloseMessageBox();
			IsInExposeView = false;
			base.ImageViewVisibility = true;
		}

		protected void EnterNextStage(Enm_CreateStage nextStage)
		{
			switch (nextStage)
			{
			case Enm_CreateStage.PrepExpose:
				EnterPrepExposeStage();
				break;
			case Enm_CreateStage.Acquiring:
				if (EnterAcquingStage())
				{
					base.EnableLoadFileBtn = false;
					base.AcquireBtnContent = SelectImageForGain;
				}
				break;
			case Enm_CreateStage.Selecting:
				EnterSelectingStage();
				break;
			case Enm_CreateStage.SelectFinished:
				if (EnterSelectFinsihedStage())
				{
					base.AcquireBtnContent = ExposeStr;
				}
				break;
			}
		}

		private bool EnterPrepExposeStage(bool forceEnter = false)
		{
			bool result = false;
			if (Enm_CreateStage.SelectFinished == CurStage || forceEnter)
			{
				CurStage = Enm_CreateStage.PrepExpose;
				result = true;
			}
			return result;
		}

		private bool EnterAcquingStage()
		{
			bool result = false;
			if (Enm_CreateStage.PrepExpose == CurStage)
			{
				CurStage = Enm_CreateStage.Acquiring;
				result = true;
			}
			return result;
		}

		private bool EnterSelectingStage()
		{
			bool result = false;
			if (Enm_CreateStage.Acquiring == CurStage)
			{
				CurStage = Enm_CreateStage.Selecting;
				result = true;
			}
			return result;
		}

		protected bool EnterSelectFinsihedStage(bool forceEnter = false)
		{
			bool result = false;
			if (Enm_CreateStage.Selecting == CurStage || forceEnter)
			{
				if (IsInExposeView)
				{
					CurStage = Enm_CreateStage.PrepExpose;
				}
				else
				{
					CurStage = Enm_CreateStage.SelectFinished;
				}
				result = true;
			}
			return result;
		}

		protected virtual int StartSelectImages()
		{
			return DetInstance.InvokeProxy.Invoke(3005, ImageSelectedIndex, OneGroupDefectImageNumber);
		}

		private void ResetAcquireState()
		{
			if (IsInExposeView)
			{
				EnterPrepExposeStage(true);
			}
			else
			{
				EnterSelectFinsihedStage(true);
			}
			StopCheckFramesMonitor();
		}

		private void SelelctImageFailed()
		{
			base.EnableLoadFileBtn = true;
			base.AcquireBtnContent = ReExposeStr;
			EnterSelectFinsihedStage(true);
			ResetAcquireState();
		}

		protected override void WaitImageTimeout()
		{
			ResetAcquireState();
		}

		protected override void ProcessSuceedEvent(int cmdID)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFinished();
				break;
			case 1005:
				CloseMessageBox();
				break;
			case 3015:
				PrepareForAcquire();
				FinishGenerationProcess();
				break;
			case 3017:
				DownloadCaliFile();
				break;
			}
		}

		protected override void ProcessFailedEvent(int cmdID, int errorCode)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFailed(errorCode);
				break;
			case 3005:
				SelelctImageFailed();
				break;
			case 1004:
			case 1010:
				SelelctImageFailed();
				break;
			case 1005:
				CloseMessageBox();
				break;
			case 3017:
				if (_downloadWnd != null)
				{
					_downloadWnd.OnClose();
				}
				_loadFileStatus = 0;
				base.MessageColor = WarningColor;
				base.MessageInfo = "Download failed! Err = " + ErrorHelper.GetErrorDesp(errorCode);
				MessageBox.Show(base.MessageInfo);
				break;
			}
			StopCheckFramesMonitor();
		}

		protected override void CancelCurrentTask()
		{
			StopCheckFramesMonitor();
			if (this.ClosePlaybar != null)
			{
				this.ClosePlaybar();
			}
		}

		protected void StopCheckFramesMonitor()
		{
			if (ValidImageMonitor != null)
			{
				ValidImageMonitor.StopTask();
				ValidImageMonitor = null;
			}
		}

		private void AcquireLightImages()
		{
			StartValidDftFrameNumber = DetInstance.Detector.Prop_Attr_DefectValidFrames;
			StartStore();
			ValidCount = 0;
			EnableBufferCine = true;
			ValidImageMonitor = new MonitorTask(40, UpdateValidLightImageStatus);
			ValidImageMonitor.RunTask();
			base.EnableAcquireBtn = false;
		}

		protected void AcquireDarkImages()
		{
			DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
			{
				if (!IsCanceled)
				{
					MessageBox.Show("Stop exposure and start to acquire dark images.");
				}
			}, new object[0]);
			if (DetInstance.Detector != null && !IsCanceled)
			{
				DetInstance.InvokeProxy.Invoke(1010, 0);
				ValidImageMonitor = new MonitorTask(40, UpdateValidDarkImageStatus);
				ValidImageMonitor.RunTask();
				IsAcquireLightImage = false;
			}
		}

		private void StartStore()
		{
			if (this.ClosePlaybar != null)
			{
				this.ClosePlaybar();
			}
			if (this.StartStoreImage != null)
			{
				this.StartStoreImage(ImageSelectedIndex);
			}
		}

		private void StopStore(bool updateCover = true)
		{
			if (this.StopStoreImage != null)
			{
				this.StopStoreImage(updateCover);
			}
		}

		public virtual void StartInit()
		{
			throw new NotImplementedException();
		}

		protected virtual void UpdateValidLightImageStatus()
		{
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			UpdateProgress(num);
			if (num >= OneGroupDefectImageNumber / 2 && OneGroupDefectImageNumber / 2 == num && ValidImageMonitor.IsTaskRunning)
			{
				ValidImageMonitor.StopTask();
				AcquireDarkImages();
				StopStore();
				EnableBufferCine = false;
			}
		}

		protected virtual void UpdateValidDarkImageStatus()
		{
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			UpdateProgress(num);
			if (num >= OneGroupDefectImageNumber && OneGroupDefectImageNumber == num)
			{
				ValidImageMonitor.StopTask();
				DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
				{
					GroupAcquireFinished();
				}, new object[0]);
			}
		}

		protected override void AcquiredImage()
		{
			if (CurStage != Enm_CreateStage.PrepExpose && CurStage != Enm_CreateStage.SelectFinished)
			{
				if (IsInExposeView)
				{
					EnterPreViewImageView();
				}
				if (EnableBufferCine)
				{
					ValidCount++;
				}
				if (IsAcquireLightImage)
				{
					base.AcquiredImage();
				}
			}
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			base.AcquiedProgress = acquiredFrames.ToString() + "/" + OneGroupDefectImageNumber.ToString();
		}

		protected override void OnClickPerpAcquireBtn()
		{
			int num = 0;
			switch (CurStage)
			{
			case Enm_CreateStage.Selecting:
				break;
			case Enm_CreateStage.PrepExpose:
				num = DetInstance.InvokeProxy.Invoke(1004);
				if (1 == num || num == 0)
				{
					EnterNextStage(Enm_CreateStage.Acquiring);
					StartExposeWindowTimerAfterStartAcquire();
					if (nDynamicType == 1 || nDynamicType == 2)
					{
						base.EnableGenerateBtn = false;
					}
				}
				break;
			case Enm_CreateStage.Acquiring:
				if (StartSelectImages() == 0)
				{
					AcquireLightImages();
					EnterNextStage(Enm_CreateStage.Selecting);
					base.EnableCancelBtn = false;
				}
				else
				{
					SelelctImageFailed();
				}
				break;
			case Enm_CreateStage.SelectFinished:
				EnterExposeView();
				break;
			}
		}

		public override void SetDynamicType(int nType)
		{
			nDynamicType = nType;
			if (nType == 1)
			{
				base.AcceptBtnVisibility = false;
			}
			else
			{
				base.AcceptBtnVisibility = true;
			}
		}
	}
}
