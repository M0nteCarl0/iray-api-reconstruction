using CorrectUI;
using iDetector;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExDynamicGainDefectPageVM : GenerateExDynamicTemplate
	{
		private DoseInfo[] DefectDoseList;

		private int[] ExpectedGrayList;

		private string[] KVList;

		private DoseAlgorithm DoseAlgorithm;

		public GenerateExDynamicGainDefectPageVM(DetecInfo instance)
			: base(instance)
		{
			InitDoseInfo();
			ExpectedGrayList = new int[3]
			{
				2000,
				12000,
				4000
			};
			KVList = new string[3];
			LoadKVList(KVList);
			DefectDoseList = new DoseInfo[3];
			for (int i = 0; i < 3; i++)
			{
				uint result = 0u;
				if (uint.TryParse(KVList[i], out result))
				{
					DefectDoseList[i] = new DoseInfo(string.Format("{0}kV", KVList[i]));
				}
				else
				{
					DefectDoseList[i] = new DoseInfo(KVList[i]);
				}
			}
			InitImagelist(KVList);
			InitCheckDownload(2);
			base.ProgressValueVisibility = true;
			base.ExProgressVisibility = true;
		}

		private void InitDoseInfo()
		{
			string str = "mAs";
			string lpAppName = "Algorithm";
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncInAlgorithm";
				}
				else if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncOutAlgorithm";
				}
				break;
			}
			DoseAlgorithm = new DoseAlgorithm();
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			uint privateProfileInt = IniParser.GetPrivateProfileInt("Study", "DoseCount", 3, lpFileName);
			for (int i = 0; i < privateProfileInt; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Study", lpKeyName, 0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_1";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_2";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = "Coeff" + (i + 1).ToString();
				double privateProfileDouble = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				lpKeyName = "Intercept" + (i + 1).ToString();
				double privateProfileDouble2 = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				DoseAlgorithm.AddCoeffItem(privateProfileInt2, privateProfileDouble, privateProfileDouble2);
			}
			DoseAlgorithm.UpdateTimeCoeff(IniParser.GetPrivateProfileDouble(lpAppName, "TimeCoeff", 1.0, lpFileName));
		}

		private double CalcDosemA(DoseAlgorithm doseAlgorithm, uint _kv, int gray)
		{
			if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
			{
				return doseAlgorithm.GetDosemAs(_kv, gray);
			}
			if (DetInstance.Detector.Prop_Attr_UROM_SyncExpTime != 0)
			{
				return doseAlgorithm.GetDosemAs(_kv, gray) * doseAlgorithm.TimeCoeff / (double)DetInstance.Detector.Prop_Attr_UROM_SyncExpTime;
			}
			return 0.0;
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist != null && DetInstance.Detector != null)
			{
				string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
				for (int i = 0; i < kvlist.Length; i++)
				{
					string lpKeyName = "KV" + (i + 1).ToString();
					kvlist[i] = IniParser.GetPrivateProfileString("GainDefect", lpKeyName, "", lpFileName);
					ExpectedGrayList[i] = (int)IniParser.GetPrivateProfileInt("GainDefect", string.Format("Gray{0}", i + 1), ExpectedGrayList[i], lpFileName);
				}
			}
		}

		protected override void OnClickStartBtn()
		{
			if (CanCreateTemplate())
			{
				InitForCreateTemplate(3002, "GAINDEFECT");
				if (InitedStatus)
				{
					SetDoseInfo(DefectDoseList[0].GetDoseInfo());
					base.ExpectedValue = GetExpectedGray(0).ToString();
				}
				else
				{
					InitFinished();
					base.OnClickCancelBtn();
					ClearGuiInitFailed();
				}
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			if (frameIndex < DefectDoseList.Length)
			{
				SetDoseInfo(DefectDoseList[frameIndex].GetDoseInfo());
			}
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			frameIndex = ((frameIndex < 3) ? frameIndex : 2);
			return ExpectedGrayList[frameIndex];
		}

		private bool CanCreateTemplate()
		{
			bool result = true;
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (DetInstance.Detector.Prop_Attr_GainTotalFrames == 0)
				{
					base.MessageColor = WarningColor;
					base.MessageInfo = "Frame number cannot be 0, Please reset the value in configure file!";
					result = false;
				}
				break;
			}
			return result;
		}
	}
}
