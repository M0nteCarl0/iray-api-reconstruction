using iDetector;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;

namespace CorrectExUI.ViewModel
{
	public class GenerateExTemplatePageVM : NotifyObject
	{
		public delegate void AsyncInvokeMethod(int Cmd);

		public enum GrayValueStatus
		{
			Below,
			Expected,
			Above,
			WarnBelow,
			WarnAbove,
			DenyBelow,
			DenyAbove
		}

		public struct GenerateTempInfo
		{
			public string strGenInitInfo;

			public string strGeneratingInfo;

			public string strGenSucceedInfo;

			public string strGenFailedInfo;

			public int nCurGenCmd;
		}

		private bool imageViewVisibility;

		private string acquiedProgress;

		private bool enableStartBtn;

		private bool enableCancelBtn;

		private bool enableGenerateBtn;

		private bool enableAcquireBtn;

		private bool enableAcceptBtn;

		private string messageInfo;

		private string stageValue;

		private string suggestedKvValue;

		private string expectedValue;

		private string currentValue;

		private string acquireBtnContent;

		private bool progressValueVisibility;

		private string offsetType;

		private bool offsetTypeVisibility;

		private bool checkDownloadVisibility;

		private bool downloadIsChecked;

		private bool enableCheckDownload;

		private bool acceptBtnVisibility;

		private string messageColor;

		private bool enableLoadFileBtn;

		private string currentValueColor;

		private bool exProgressVisibility;

		protected readonly string ExposeStr = StringResource.FindString("PrepareExpose");

		protected readonly string ReExposeStr = StringResource.FindString("ReExpose");

		protected readonly string CompleteStr = StringResource.FindString("CompletedAcquire");

		protected bool IsInExposeView;

		protected bool IsCanceled;

		private DlgTaskTimeOut MessageBoxTimer;

		protected int TimeoutSeconds = 30;

		protected DetecInfo DetInstance;

		protected int GroupNumber;

		protected List<bool> AcquiredStatus;

		protected bool _startAcqFlag;

		protected IRayImageData newImage;

		protected bool InitedStatus;

		protected int ImageSelectedIndex;

		protected bool _bCorrectTemplating;

		private GenerateTempInfo _genTempInfo;

		protected int _nIndexCount;

		protected int _nIndex;

		protected string _strDesc;

		protected int _loadFileStatus;

		protected List<DownloadInfo> _downloadList;

		protected DownloadCorrProgressWnd _downloadWnd;

		protected string TemplatePath = "Correct";

		protected string WarningColor = "#EE7600";

		protected string NormalColor = "";

		private Detector _detectorSender;

		protected int _nExposeWindowTime;

		protected string InitCurrentValueColor = "#FFFFFF";

		protected string NormalCurrentValueColor = "#00FF00";

		protected string WarningCurrentValueColor = "#FFFF00";

		protected string WarningCurrentValueColorEx = "#FFD000";

		protected string ErrorCurrentValueColor = "#FF0000";

		protected string ErrorCurrentValueColorEx = "#800000";

		protected bool DQEDenied;

		private string strActiveSubset = "";

		public DelegateCommand Start
		{
			get;
			set;
		}

		public DelegateCommand Cancel
		{
			get;
			set;
		}

		public DelegateCommand Generate
		{
			get;
			set;
		}

		public DelegateCommand Acquire
		{
			get;
			set;
		}

		public DelegateCommand Accept
		{
			get;
			set;
		}

		public DelegateCommand CheckedDownload
		{
			get;
			set;
		}

		public DelegateCommand LoadImageFromFile
		{
			get;
			set;
		}

		public bool ImageViewVisibility
		{
			get
			{
				return imageViewVisibility;
			}
			set
			{
				imageViewVisibility = value;
				NotifyPropertyChanged("ImageViewVisibility");
			}
		}

		public string AcquiedProgress
		{
			get
			{
				return acquiedProgress;
			}
			set
			{
				acquiedProgress = value;
				NotifyPropertyChanged("AcquiedProgress");
			}
		}

		public bool EnableStartBtn
		{
			get
			{
				return enableStartBtn;
			}
			set
			{
				enableStartBtn = value;
				NotifyPropertyChanged("EnableStartBtn");
			}
		}

		public bool EnableCancelBtn
		{
			get
			{
				return enableCancelBtn;
			}
			set
			{
				enableCancelBtn = value;
				NotifyPropertyChanged("EnableCancelBtn");
			}
		}

		public bool EnableGenerateBtn
		{
			get
			{
				return enableGenerateBtn;
			}
			set
			{
				enableGenerateBtn = value;
				NotifyPropertyChanged("EnableGenerateBtn");
			}
		}

		public bool EnableAcquireBtn
		{
			get
			{
				return enableAcquireBtn;
			}
			set
			{
				enableAcquireBtn = value;
				NotifyPropertyChanged("EnableAcquireBtn");
			}
		}

		public bool EnableAcceptBtn
		{
			get
			{
				return enableAcceptBtn;
			}
			set
			{
				enableAcceptBtn = value;
				NotifyPropertyChanged("EnableAcceptBtn");
			}
		}

		public string MessageInfo
		{
			get
			{
				return messageInfo;
			}
			set
			{
				messageInfo = value;
				NotifyPropertyChanged("MessageInfo");
			}
		}

		public string StageValue
		{
			get
			{
				return stageValue;
			}
			set
			{
				stageValue = value;
				NotifyPropertyChanged("StageValue");
			}
		}

		public string SuggestedKvValue
		{
			get
			{
				return suggestedKvValue;
			}
			set
			{
				suggestedKvValue = value;
				NotifyPropertyChanged("SuggestedKvValue");
			}
		}

		public string ExpectedValue
		{
			get
			{
				return expectedValue;
			}
			set
			{
				expectedValue = value;
				NotifyPropertyChanged("ExpectedValue");
			}
		}

		public string CurrentValue
		{
			get
			{
				return currentValue;
			}
			set
			{
				currentValue = value;
				NotifyPropertyChanged("CurrentValue");
			}
		}

		public string AcquireBtnContent
		{
			get
			{
				return acquireBtnContent;
			}
			set
			{
				acquireBtnContent = value;
				NotifyPropertyChanged("AcquireBtnContent");
			}
		}

		public bool ProgressValueVisibility
		{
			get
			{
				return progressValueVisibility;
			}
			set
			{
				progressValueVisibility = value;
				NotifyPropertyChanged("ProgressValueVisibility");
			}
		}

		public string OffsetType
		{
			get
			{
				return offsetType;
			}
			set
			{
				offsetType = value;
				NotifyPropertyChanged("OffsetType");
			}
		}

		public bool OffsetTypeVisibility
		{
			get
			{
				return offsetTypeVisibility;
			}
			set
			{
				offsetTypeVisibility = value;
				NotifyPropertyChanged("OffsetTypeVisibility");
			}
		}

		public bool CheckDownloadVisibility
		{
			get
			{
				return checkDownloadVisibility;
			}
			set
			{
				checkDownloadVisibility = value;
				NotifyPropertyChanged("CheckDownloadVisibility");
			}
		}

		public bool DownloadIsChecked
		{
			get
			{
				return downloadIsChecked;
			}
			set
			{
				downloadIsChecked = value;
				NotifyPropertyChanged("DownloadIsChecked");
			}
		}

		public bool EnableCheckDownload
		{
			get
			{
				return enableCheckDownload;
			}
			set
			{
				enableCheckDownload = value;
				NotifyPropertyChanged("EnableCheckDownload");
			}
		}

		public bool AcceptBtnVisibility
		{
			get
			{
				return acceptBtnVisibility;
			}
			set
			{
				acceptBtnVisibility = value;
				NotifyPropertyChanged("AcceptBtnVisibility");
			}
		}

		public string MessageColor
		{
			get
			{
				return messageColor;
			}
			set
			{
				messageColor = value;
				NotifyPropertyChanged("MessageColor");
			}
		}

		public bool EnableLoadFileBtn
		{
			get
			{
				return enableLoadFileBtn;
			}
			set
			{
				enableLoadFileBtn = value;
				NotifyPropertyChanged("EnableLoadFileBtn");
			}
		}

		public string CurrentValueColor
		{
			get
			{
				return currentValueColor;
			}
			set
			{
				currentValueColor = value;
				NotifyPropertyChanged("CurrentValueColor");
			}
		}

		public bool ExProgressVisibility
		{
			get
			{
				return exProgressVisibility;
			}
			set
			{
				exProgressVisibility = value;
				NotifyPropertyChanged("ExProgressVisibility");
			}
		}

		protected int AcquirdFrames
		{
			get
			{
				return GetAcquirdFrames();
			}
		}

		public ExposeWndUserControl exposeWindowControl
		{
			get;
			set;
		}

		public event Action<IRayImageData> ShowImage;

		protected GenerateExTemplatePageVM(DetecInfo instance)
		{
			DetInstance = instance;
			Start = new DelegateCommand(OnStart);
			Cancel = new DelegateCommand(OnCancel);
			Generate = new DelegateCommand(OnGenerate);
			Acquire = new DelegateCommand(OnAcquire);
			Accept = new DelegateCommand(OnAccept);
			CheckedDownload = new DelegateCommand(OnCheckedDownload);
			EnableStartBtn = true;
			AcquireBtnContent = ExposeStr;
			newImage = new IRayImageData();
			InitedStatus = false;
			_startAcqFlag = true;
			_bCorrectTemplating = false;
			_genTempInfo = default(GenerateTempInfo);
			OffsetTypeVisibility = false;
			EnableCheckDownload = false;
			_loadFileStatus = 0;
			_downloadList = new List<DownloadInfo>();
			AcceptBtnVisibility = true;
			EnableLoadFileBtn = false;
			CurrentValueColor = InitCurrentValueColor;
		}

		private int GetAcquirdFrames()
		{
			if (AcquiredStatus == null)
			{
				return 0;
			}
			int num = 0;
			foreach (bool item in AcquiredStatus)
			{
				if (item)
				{
					num++;
				}
			}
			return num;
		}

		protected void InitForCreateTemplate(int cmdID, string message)
		{
			DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
			{
				SetGenTemplateInfo(message);
				int num = DetInstance.Detector.Invoke(cmdID);
				if (num == 0)
				{
					InitedStatus = true;
					InitFinished();
				}
				else if (1 != num)
				{
					InitFailed(num);
				}
				else
				{
					ShowMessageBox("Initialize", message, WaitInitAckTimeOut);
					MessageColor = NormalColor;
					MessageInfo = _genTempInfo.strGenInitInfo;
				}
			}, new object[0]);
		}

		protected virtual void InitFinished()
		{
			InitedStatus = true;
			EnableLoadFileBtn = true;
			CloseMessageBox();
			_bCorrectTemplating = true;
		}

		protected void InitFailed(int errorCode)
		{
			InitedStatus = false;
			EnableLoadFileBtn = false;
			EnterExposeView();
			CloseMessageBox();
			string messageBoxText = "Initialization failed! Err = " + ErrorHelper.GetErrorDesp(errorCode);
			MessageBox.Show(messageBoxText);
			messageBoxText = "Please go Back and then restart the task!";
			MessageBox.Show(messageBoxText);
		}

		private void ClearImagelist()
		{
			AcquiredStatus.Clear();
			for (int i = 0; i < GroupNumber; i++)
			{
				AcquiredStatus.Add(false);
			}
			ImageSelectedIndex = 0;
			UpdateProgress(0);
		}

		protected void InitImagelist(string[] kvArray)
		{
			GroupNumber = kvArray.Length;
			AcquiredStatus = new List<bool>();
			AcquiredStatus.Capacity = GroupNumber;
		}

		protected virtual void ResumePrepStage()
		{
		}

		protected virtual void ExposeTimerEndCallback()
		{
			if (!_startAcqFlag)
			{
				ResumePrepStage();
			}
			else
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("WaitImage"), WaitImageTimerTimeOut);
			}
		}

		protected void ShowMessageBox(string title, string content, Action timeOutHandler)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TimeoutSeconds, DetInstance.Shell.ParentUI, timeOutHandler);
			MessageBoxTimer.RunTask();
		}

		protected void ShowMessageBox(string title, string content, bool canCancel, Action cancelWait)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, canCancel, DetInstance.Shell.ParentUI, cancelWait);
			MessageBoxTimer.RunTask();
		}

		private void WaitImageTimerTimeOut()
		{
			MessageBox.Show(StringResource.FindString("WaitImageTimeOut"));
		}

		private void WaitInitAckTimeOut()
		{
			MessageBox.Show(StringResource.FindString("InitTimeOut"));
			InitFailed(22);
			InitedStatus = true;
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		protected void StopTimer()
		{
		}

		protected virtual void PrepareForAcquire()
		{
			IsCanceled = false;
			AcquireBtnContent = ExposeStr;
			ImageSelectedIndex = 0;
			UpdateProgress(0);
			InitGuiStatus();
			EnterExposeView();
			ClearImagelist();
		}

		protected virtual void CancelCurrentTask()
		{
		}

		protected virtual void UpdateProgress(int acquiredFrames)
		{
		}

		protected virtual void EnterExposeView()
		{
		}

		protected virtual void EnterPreViewImageView()
		{
		}

		private void OnStartAcq(object obj)
		{
			OnClickAcquireBtn();
		}

		protected virtual void OnStart(object obj)
		{
			OnClickStartBtn();
		}

		protected virtual void OnCancel(object obj)
		{
			OnClickCancelBtn();
		}

		protected virtual void OnAcquire(object obj)
		{
			OnClickPerpAcquireBtn();
		}

		protected virtual void OnAccept(object obj)
		{
			OnClickAcceptBtn();
		}

		protected virtual void OnGenerate(object obj)
		{
			OnClickGenerateBtn();
		}

		protected virtual void OnCheckedDownload(object obj)
		{
			OnClickCheckedDownloadBtn();
		}

		protected virtual void OnClickCancelBtn()
		{
			if (AbortCurrentTask())
			{
				CancelGuiStatus();
				_bCorrectTemplating = false;
			}
		}

		protected virtual void OnClickGenerateBtn()
		{
			if (ImageSelectedIndex < GroupNumber)
			{
				if (MessageBoxResult.OK != MessageBox.Show("Warning：A better Gain template will be generated after acquiring all frames. Will you generate the Gain template now?", "Create Template", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation))
				{
					return;
				}
				MessageColor = WarningColor;
				MessageInfo = "Warning: Do not reach the best number, the quality of the template is not good!";
			}
			else
			{
				MessageColor = NormalColor;
				MessageInfo = _genTempInfo.strGeneratingInfo;
			}
			new Thread((ThreadStart)delegate
			{
				GenerateExTemplatePageVM generateExTemplatePageVM = this;
				int nRetCode = DetInstance.Detector.Invoke(_genTempInfo.nCurGenCmd);
				DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
				{
					if (nRetCode == 0)
					{
						generateExTemplatePageVM.GenerateTempalteSucceed();
					}
					else if (1 != nRetCode)
					{
						generateExTemplatePageVM.GenerateTempalteFailed(nRetCode);
					}
				}, new object[0]);
			}).Start();
		}

		protected virtual void OnClickAcceptBtn()
		{
			if (ImageSelectedIndex < GroupNumber - 1)
			{
				ImageSelectedIndex++;
				EnterExposeView();
				StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
			}
			else
			{
				StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
				EnableGenerateBtn = true;
				EnableCheckDownload = true;
				IsInExposeView = true;
				EnterExposeView();
				EnableAcquireBtn = false;
				MessageColor = NormalColor;
				MessageInfo = "Complete images collection, Press [Generate] to generate template";
				ImageSelectedIndex++;
			}
			EnableAcceptBtn = false;
			int realValue = 0;
			int num = 0;
			IsRealGrayBeyondExpected(out realValue, out num);
			ExpectedValue = num.ToString();
		}

		protected virtual void OnClickCheckedDownloadBtn()
		{
			if (DownloadIsChecked)
			{
				DownloadIsChecked = true;
			}
			else
			{
				DownloadIsChecked = false;
			}
		}

		protected virtual void GenerateTempalteSucceed()
		{
			MessageColor = NormalColor;
			MessageInfo = _genTempInfo.strGenSucceedInfo;
			int num = DetInstance.Detector.Invoke(3015);
			if (1 != num)
			{
				PrepareForAcquire();
			}
		}

		protected virtual void GenerateTempalteFailed(int errorCode)
		{
			MessageColor = WarningColor;
			MessageInfo = _genTempInfo.strGenFailedInfo;
			string messageBoxText = "Create template failed! Err = " + ErrorHelper.GetErrorDesp(errorCode);
			MessageBox.Show(messageBoxText);
		}

		protected virtual void OnClickStartBtn()
		{
			throw new NotImplementedException();
		}

		protected virtual void OnClickPerpAcquireBtn()
		{
			throw new NotImplementedException();
		}

		protected virtual void OnClickAcquireBtn()
		{
		}

		protected virtual void StartExposeWindowTimerAfterStartAcquire()
		{
		}

		protected virtual void StartExposeWindowTimerAfterExpEnable(int nCallbackDelayTime)
		{
		}

		protected void StartExposeWindowTimer()
		{
			StartExpWindow(_detectorSender, _nExposeWindowTime / 1000);
		}

		protected void StopExposeWindowTimer()
		{
			CloseExpWindow();
		}

		protected void StartExposeWindowTimerNoCancel(int nDelayTime)
		{
			if (nDelayTime >= 1)
			{
				exposeWindowControl.Show(nDelayTime, false);
			}
			else
			{
				exposeWindowControl.Show(1, false);
			}
		}

		private void StartExpWindow(Detector sender, int delayTimeSecond)
		{
			if (delayTimeSecond < 1)
			{
				return;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				exposeWindowControl.Show(delayTimeSecond, false);
				return;
			}
			if (DetInstance.Detector.Prop_Attr_UROM_TriggerMode == 1)
			{
				exposeWindowControl.Show(delayTimeSecond, true);
			}
		}

		private void CloseExpWindow()
		{
			if (exposeWindowControl != null)
			{
				exposeWindowControl.Close();
			}
		}

		private void OnCancelExpWindowCmd()
		{
			int num = DetInstance.Detector.Invoke(1005);
			if (num != 0 && 1 != num)
			{
				MessageBox.Show("Cancel ExpWindow failed! Err = " + ErrorHelper.GetErrorDesp(num));
			}
			else
			{
				EnableAcquireBtn = true;
			}
		}

		protected void StopExposeWindowTimerAfterRecvImage(Detector sender)
		{
			if (sender.Prop_Attr_UROM_ExpMode != 2)
			{
				int prop_Attr_UROM_ExpMode = sender.Prop_Attr_UROM_ExpMode;
				int num = 1;
			}
		}

		protected virtual double GetExposeWindowLength()
		{
			return 0.1;
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (1001 == nEventID || 1012 == nEventID)
			{
				IRayImage image = (IRayImage)Marshal.PtrToStructure(pParam, typeof(IRayImage));
				newImage.Write(ref image);
				if (newImage.ImgData == null)
				{
					return;
				}
			}
			DetInstance.Shell.ParentUI.Dispatcher.BeginInvoke((Action)delegate
			{
				switch (nEventID)
				{
				case 1002:
					StopExposeWindowTimerAfterRecvImage(sender);
					break;
				case 1001:
				case 1012:
					StopExposeWindowTimerAfterRecvImage(sender);
					if (_bCorrectTemplating)
					{
						CollectImage();
					}
					break;
				case 4:
					if (IsCanceled)
					{
						CancelGuiStatus();
						_bCorrectTemplating = false;
					}
					else
					{
						ProcessSuceedEvent(nParam1);
					}
					break;
				case 5:
					if (!IsCanceled)
					{
						ProcessFailedEvent(nParam1, nParam2);
					}
					break;
				case 6:
					if (IsCanceled)
					{
						CancelGuiStatus();
						_bCorrectTemplating = false;
					}
					break;
				case 1003:
				case 1006:
					CloseMessageBox();
					WaitImageTimeout();
					if (nEventID == 1003)
					{
						AcquireBtnContent = ReExposeStr;
					}
					else if (nEventID == 1006)
					{
						ResumePrepStage();
					}
					break;
				case 1005:
					_detectorSender = sender;
					_nExposeWindowTime = nParam1;
					StartExposeWindowTimerAfterExpEnable(nParam1);
					break;
				case 1008:
					StopExposeWindowTimer();
					break;
				case 17:
				{
					string messageBoxText = strMsg + " Error:" + ErrorHelper.GetErrorDesp(nParam2);
					MessageBox.Show(messageBoxText);
					EnableLoadFileBtn = false;
					break;
				}
				case 7:
					if (1004 == nParam1)
					{
						StartExposeWindowTimer();
					}
					break;
				case 1011:
					StopTimer();
					break;
				}
			});
		}

		protected virtual void WaitImageTimeout()
		{
		}

		protected virtual void ProcessSuceedEvent(int cmdID)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFinished();
				break;
			case 3004:
			case 3009:
				SelectImageSucceed();
				break;
			case 1001:
				if (DetInstance.Detector.Prop_Attr_UROM_TriggerMode == 2)
				{
					EnableAcquireBtn = true;
					AcquireBtnContent = StringResource.FindString("Acquire");
				}
				break;
			case 1004:
			case 1006:
				AcquireBtnContent = ExposeStr;
				break;
			case 3015:
				PrepareForAcquire();
				CheckDownloadFile();
				CancelGuiStatus();
				_bCorrectTemplating = false;
				break;
			case 3017:
				DownloadCaliFile();
				break;
			case 3023:
			{
				AsyncInvokeMethod asyncInvokeMethod = AsyncInvokeNextPREP;
				asyncInvokeMethod(1012);
				break;
			}
			}
		}

		protected virtual void ProcessFailedEvent(int cmdID, int errorCode)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFailed(errorCode);
				break;
			case 3004:
			case 3009:
				EnableAcquireBtn = true;
				break;
			case 1001:
				EnableAcquireBtn = true;
				break;
			case 1004:
				EnableAcquireBtn = true;
				AcquireBtnContent = ExposeStr;
				if (!IsInExposeView)
				{
					StopTimer();
				}
				break;
			case 1012:
				if (IsInExposeView)
				{
					EnableAcquireBtn = true;
				}
				break;
			case 3017:
				if (_downloadWnd != null)
				{
					_downloadWnd.OnClose();
				}
				_loadFileStatus = 0;
				MessageColor = WarningColor;
				MessageInfo = "Download failed! Err = " + ErrorHelper.GetErrorDesp(errorCode);
				break;
			case 3023:
			{
				AsyncInvokeMethod asyncInvokeMethod = AsyncInvokeNextPREP;
				asyncInvokeMethod(1003);
				break;
			}
			}
		}

		private void AsyncInvokeNextPREP(int Cmd)
		{
			Thread.Sleep(1000);
			int num = DetInstance.InvokeProxy.Invoke(Cmd);
			if (1 == num || num == 0)
			{
				StartExposeWindowTimerAfterStartAcquire();
				EnableAcquireBtn = false;
				EnableLoadFileBtn = false;
				EnableAcceptBtn = false;
			}
		}

		protected virtual void SelectImageSucceed()
		{
			GroupAcquireFinished();
			UpdateProgress(AcquirdFrames);
		}

		protected virtual void CollectImage()
		{
			if (this.ShowImage != null)
			{
				this.ShowImage(newImage);
			}
			AcquiredImage();
		}

		protected virtual void AcquiredImage()
		{
			int realValue = 0;
			int num = 0;
			DQEDenied = false;
			string text;
			switch (IsRealGrayBeyondExpected(out realValue, out num))
			{
			case GrayValueStatus.DenyAbove:
				MessageColor = WarningColor;
				text = StringResource.FindString("GrayValueAboveAlarm");
				CurrentValueColor = ErrorCurrentValueColorEx;
				DQEDenied = true;
				break;
			case GrayValueStatus.DenyBelow:
				MessageColor = WarningColor;
				text = StringResource.FindString("GrayValueBelowAlarm");
				CurrentValueColor = ErrorCurrentValueColor;
				DQEDenied = true;
				break;
			case GrayValueStatus.WarnAbove:
				MessageColor = WarningColor;
				text = StringResource.FindString("GrayValueAboveWarn");
				CurrentValueColor = WarningCurrentValueColorEx;
				break;
			case GrayValueStatus.WarnBelow:
				MessageColor = WarningColor;
				text = StringResource.FindString("GrayValueBelowWarn");
				CurrentValueColor = WarningCurrentValueColor;
				break;
			case GrayValueStatus.Above:
				MessageColor = WarningColor;
				text = StringResource.FindString("GrayValueAboveAlarm");
				CurrentValueColor = WarningCurrentValueColor;
				break;
			case GrayValueStatus.Below:
				MessageColor = WarningColor;
				text = StringResource.FindString("GrayValueBelowAlarm");
				CurrentValueColor = WarningCurrentValueColor;
				break;
			default:
				MessageColor = NormalColor;
				text = StringResource.FindString("GrayValueNormal");
				CurrentValueColor = NormalCurrentValueColor;
				break;
			}
			MessageInfo = text;
			ExpectedValue = num.ToString();
			CurrentValue = realValue.ToString();
		}

		protected void GroupAcquireFinished()
		{
			AcquiredStatus[ImageSelectedIndex] = true;
			if (IsAcquireCompleted())
			{
				AcquiedProgress = CompleteStr;
			}
			EnableAcceptBtn = true;
			EnableAcquireBtn = true;
			EnableCancelBtn = true;
			EnterExposeView();
		}

		protected int GetCenterAverageValue(IRayImageData image)
		{
			int result = 0;
			IRayVariantMapItem[] @params = image.Params;
			for (int i = 0; i < @params.Length; i++)
			{
				IRayVariantMapItem rayVariantMapItem = @params[i];
				if (rayVariantMapItem.nMapKey == 32788)
				{
					result = rayVariantMapItem.varMapVal.val.nVal;
					break;
				}
			}
			return result;
		}

		private bool AbortCurrentTask()
		{
			if (!InitedStatus)
			{
				return true;
			}
			bool result = false;
			IsCanceled = true;
			if (DetInstance.Detector.Abort() == 0)
			{
				result = true;
			}
			return result;
		}

		protected bool IsAcquireCompleted()
		{
			if (AcquirdFrames == GroupNumber)
			{
				return true;
			}
			return false;
		}

		protected virtual GrayValueStatus IsRealGrayBeyondExpected(out int realValue, out int expectedValue)
		{
			realValue = GetCenterAverageValue(newImage);
			expectedValue = GetExpectedGray(ImageSelectedIndex);
			int num = realValue - expectedValue;
			int num2 = Math.Abs(num);
			if (num2 * 10 < GetExpectedGray(ImageSelectedIndex))
			{
				return GrayValueStatus.Expected;
			}
			if (num > 0)
			{
				return GrayValueStatus.Above;
			}
			return GrayValueStatus.Below;
		}

		protected virtual int GetExpectedGray(int frameIndex)
		{
			return 0;
		}

		protected void SetDoseInfo(string doseInfo)
		{
			SuggestedKvValue = doseInfo;
		}

		protected virtual void SetDoseInfo(int frameIndex)
		{
			throw new NotImplementedException();
		}

		protected virtual int SelectImage(int frameIndex)
		{
			throw new NotImplementedException();
		}

		public void AddCallbackHandler()
		{
			DetInstance.Detector.SdkCallbackEvent += OnSdkCallback;
			if (this.exposeWindowControl != null)
			{
				ExposeWndUserControl exposeWindowControl = this.exposeWindowControl;
				exposeWindowControl.CancelHandler = (Action)Delegate.Combine(exposeWindowControl.CancelHandler, new Action(OnCancelExpWindowCmd));
			}
		}

		public void RemoveCallbackHandler()
		{
			IsCanceled = false;
			DetInstance.Detector.SdkCallbackEvent -= OnSdkCallback;
			CancelCurrentTask();
			if (this.exposeWindowControl != null)
			{
				ExposeWndUserControl exposeWindowControl = this.exposeWindowControl;
				exposeWindowControl.CancelHandler = (Action)Delegate.Remove(exposeWindowControl.CancelHandler, new Action(OnCancelExpWindowCmd));
			}
		}

		private void InitGuiStatus()
		{
			EnableStartBtn = false;
			EnableCancelBtn = true;
			EnableGenerateBtn = false;
			EnableCheckDownload = false;
			EnableAcquireBtn = true;
			EnableAcceptBtn = false;
			EnableLoadFileBtn = true;
		}

		private void CancelGuiStatus()
		{
			EnableStartBtn = true;
			EnableCancelBtn = false;
			EnableGenerateBtn = false;
			EnableAcquireBtn = false;
			EnableAcceptBtn = false;
			EnableCheckDownload = false;
			MessageColor = NormalColor;
			MessageInfo = "";
			StageValue = "";
			SuggestedKvValue = "";
			ExpectedValue = "";
			CurrentValue = "";
			IsCanceled = false;
			AcquireBtnContent = ExposeStr;
			EnableLoadFileBtn = false;
			CurrentValueColor = InitCurrentValueColor;
			AcquiedProgress = "";
			CloseMessageBox();
			StopExposeWindowTimer();
			ResumePrepStage();
		}

		public bool GetCancelStatus()
		{
			if (!_bCorrectTemplating)
			{
				AcquiedProgress = "";
			}
			return _bCorrectTemplating;
		}

		public void SetCancelStatus(bool bValue)
		{
			_bCorrectTemplating = bValue;
			CancelGuiStatus();
			DetInstance.Detector.Abort();
		}

		public void DoCancel()
		{
			MessageColor = WarningColor;
			MessageInfo = "The correction has been initialized and can not be switched!";
		}

		public void SetCurSubset(string strSubset)
		{
			strActiveSubset = strSubset;
		}

		protected void OnShowImage()
		{
			if (this.ShowImage != null)
			{
				this.ShowImage(newImage);
			}
		}

		protected virtual void SelectImageFailed()
		{
			EnableAcquire();
		}

		protected void EnableAcquire()
		{
			EnableAcquireBtn = true;
			CurrentValueColor = ErrorCurrentValueColor;
		}

		private void SetGenTemplateInfo(string type)
		{
			if (type == "GAINDEFECT")
			{
				_genTempInfo.nCurGenCmd = 3006;
				_genTempInfo.strGenInitInfo = "Initialize to create gain and defect";
				_genTempInfo.strGeneratingInfo = StringResource.FindString("GeneratingGainDefect");
				_genTempInfo.strGenSucceedInfo = StringResource.FindString("GenerateGainDefectSuceed");
				_genTempInfo.strGenFailedInfo = StringResource.FindString("GenerateGainDefectFailed");
			}
			else if (type == "GAIN")
			{
				_genTempInfo.nCurGenCmd = 3006;
				_genTempInfo.strGenInitInfo = "Initialize to create gain";
				_genTempInfo.strGeneratingInfo = StringResource.FindString("GeneratingGain");
				_genTempInfo.strGenSucceedInfo = StringResource.FindString("GenerateGainSuceed");
				_genTempInfo.strGenFailedInfo = StringResource.FindString("GenerateGainFailed");
			}
			else if (type == "DEFECT")
			{
				_genTempInfo.nCurGenCmd = 3010;
				_genTempInfo.strGenInitInfo = "Initialize to create defect";
				_genTempInfo.strGeneratingInfo = StringResource.FindString("GeneratingDefect");
				_genTempInfo.strGenSucceedInfo = StringResource.FindString("GenerateDefectSuceed");
				_genTempInfo.strGenFailedInfo = StringResource.FindString("GenerateDefectFailed");
			}
		}

		protected virtual bool IsVisibleOffsetWindow()
		{
			if (1 == DetInstance.Detector.Prop_Attr_UROM_PrepCapMode && 45 == DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
				return false;
			}
			int num = 196611;
			bool flag = ((DetInstance.Detector.Prop_Attr_CurrentCorrectOption & num) != 0) ? true : false;
			bool flag2 = ((DetInstance.Detector.Prop_Attr_CurrentCorrectOption & ~num) == 0) ? true : false;
			if (flag && flag2)
			{
				return false;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
			case 58:
				return false;
			default:
				if ((DetInstance.Detector.Prop_Attr_CurrentCorrectOption & 0x30003) == 0)
				{
					return true;
				}
				return false;
			}
		}

		protected virtual void SetOffsetType(bool bSet)
		{
			OffsetTypeVisibility = bSet;
			if (bSet)
			{
				string str = "None";
				int prop_Attr_CurrentCorrectOption = DetInstance.Detector.Prop_Attr_CurrentCorrectOption;
				if ((prop_Attr_CurrentCorrectOption & 0x10000) == 65536)
				{
					str = "PreOffset";
				}
				else if ((prop_Attr_CurrentCorrectOption & 0x20000) == 131072)
				{
					str = "PostOffset";
				}
				else if ((prop_Attr_CurrentCorrectOption & 1) == 1)
				{
					str = "HWPreOffset";
				}
				else if ((prop_Attr_CurrentCorrectOption & 2) == 2)
				{
					str = "HWPostOffset";
				}
				OffsetType = "Current use offset type: " + str;
			}
		}

		protected virtual void InitCheckDownload(int nType)
		{
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 11:
			case 38:
			case 58:
				CheckDownloadVisibility = false;
				DownloadIsChecked = false;
				_nIndexCount = 0;
				break;
			case 32:
			case 37:
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 59:
			case 60:
			case 61:
			case 62:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				CheckDownloadVisibility = true;
				if (nType == 1)
				{
					DownloadIsChecked = false;
				}
				else
				{
					DownloadIsChecked = true;
				}
				_nIndexCount = 10;
				break;
			case 6:
			case 48:
			case 49:
			case 56:
				CheckDownloadVisibility = true;
				DownloadIsChecked = true;
				_nIndexCount = 12;
				break;
			default:
				CheckDownloadVisibility = false;
				DownloadIsChecked = false;
				_nIndexCount = 0;
				break;
			}
		}

		private void CheckDownloadFile()
		{
			if (!DownloadIsChecked)
			{
				return;
			}
			_downloadList.Clear();
			_loadFileStatus = 0;
			string text;
			string text2;
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
			case 58:
			{
				text = strActiveSubset;
				string[] array = strActiveSubset.Split('-');
				text2 = array[0];
				break;
			}
			default:
				text = DetInstance.Detector.Prop_Cfg_DefaultSubset;
				if (string.IsNullOrEmpty(text))
				{
					text = "Default";
				}
				text2 = text;
				break;
			}
			string gainName = GetGainName(DetInstance.Detector);
			string text3 = DetInstance.Detector.Prop_Attr_WorkDir + TemplatePath + "\\" + text2 + "\\" + gainName;
			if (File.Exists(text3))
			{
				FileInfo fileInfo = new FileInfo(text3);
				DownloadInfo item = default(DownloadInfo);
				item.index = 1;
				item.modeName = text;
				item.fileType = Enm_FileTypes.Enm_File_Gain;
				item.fullPath = text3;
				item.desc = fileInfo.LastWriteTime.ToString();
				item.indexCount = _nIndexCount;
				_downloadList.Add(item);
				_loadFileStatus++;
			}
			string defectName = GetDefectName(DetInstance.Detector);
			string text4 = DetInstance.Detector.Prop_Attr_WorkDir + TemplatePath + "\\" + text2 + "\\" + defectName;
			if (File.Exists(text4))
			{
				FileInfo fileInfo2 = new FileInfo(text4);
				DownloadInfo item2 = default(DownloadInfo);
				item2.index = 1;
				item2.modeName = text;
				item2.fileType = Enm_FileTypes.Enm_File_Defect;
				item2.fullPath = text4;
				item2.desc = fileInfo2.LastWriteTime.ToString();
				item2.indexCount = _nIndexCount;
				_downloadList.Add(item2);
				_loadFileStatus++;
			}
			string lagName = GetLagName(DetInstance.Detector);
			string text5 = DetInstance.Detector.Prop_Attr_WorkDir + TemplatePath + "\\" + text2 + "\\" + lagName;
			if (File.Exists(text5))
			{
				FileInfo fileInfo3 = new FileInfo(text5);
				DownloadInfo item3 = default(DownloadInfo);
				item3.index = 1;
				item3.modeName = text;
				item3.fileType = Enm_FileTypes.Enm_File_Lag;
				item3.fullPath = text5;
				item3.desc = fileInfo3.LastWriteTime.ToString();
				item3.indexCount = _nIndexCount;
				_downloadList.Add(item3);
				_loadFileStatus++;
			}
			if (_loadFileStatus == 0)
			{
				MessageColor = WarningColor;
				MessageInfo = "Template file does not exist!";
				return;
			}
			DownloadToFpdWnd downloadToFpdWnd = new DownloadToFpdWnd(_downloadList);
			if (downloadToFpdWnd.ShowDialog() != true)
			{
				return;
			}
			_loadFileStatus--;
			_nIndex = downloadToFpdWnd.nIndex;
			_strDesc = downloadToFpdWnd.strDesc;
			int fileType = (int)_downloadList[_loadFileStatus].fileType;
			string fullPath = _downloadList[_loadFileStatus].fullPath;
			int num = 0;
			num = ((fileType == 5) ? 1 : _nIndex);
			int num2 = DetInstance.Detector.Invoke(3017, fileType, num, fullPath, _strDesc);
			if (1 == num2 || num2 == 0)
			{
				switch (fileType)
				{
				case 2:
					MessageColor = NormalColor;
					MessageInfo = "Downloading gain ...";
					break;
				case 4:
					MessageColor = NormalColor;
					MessageInfo = "Downloading defect ...";
					break;
				case 5:
					MessageColor = NormalColor;
					MessageInfo = "Downloading lag ...";
					break;
				}
				_downloadWnd = new DownloadCorrProgressWnd(DetInstance.Detector, "Download File Progress");
				_downloadWnd.ShowDialog();
				DetInstance.Detector.AttrChangingMonitor.AddRef(5016);
			}
		}

		private string GetGainName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("gain_{0}x{1}.gn", width, height);
		}

		private string GetDefectName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("defect_{0}x{1}.dft", width, height);
		}

		private string GetLagName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("lag_{0}x{1}_matrix.lag", width, height);
		}

		private bool GetWidthAndHeight(Detector instance, out int width, out int height)
		{
			width = 0;
			height = 0;
			switch (instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 56:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					width = 1024;
					height = 1024;
					break;
				case 1:
					width = 512;
					height = 512;
					break;
				default:
					return false;
				}
				break;
			case 49:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					width = 2048;
					height = 2048;
					break;
				case 1:
					width = 1024;
					height = 1024;
					break;
				case 3:
					width = 512;
					height = 512;
					break;
				default:
					return false;
				}
				break;
			case 48:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					switch (instance.Prop_Attr_UROM_ZoomMode)
					{
					case 0:
					case 8:
						width = 3072;
						height = 3072;
						break;
					case 9:
						width = 2048;
						height = 2048;
						break;
					default:
						return false;
					}
					break;
				case 1:
					switch (instance.Prop_Attr_UROM_ZoomMode)
					{
					case 0:
					case 10:
						width = 1536;
						height = 1536;
						break;
					case 11:
						width = 1024;
						height = 1024;
						break;
					default:
						return false;
					}
					break;
				case 2:
				{
					Enm_Zoom prop_Attr_UROM_ZoomMode = (Enm_Zoom)instance.Prop_Attr_UROM_ZoomMode;
					if (prop_Attr_UROM_ZoomMode == Enm_Zoom.Enm_Zoom_Null || prop_Attr_UROM_ZoomMode == Enm_Zoom.Enm_Zoom_1024x1024)
					{
						width = 1024;
						height = 1024;
						break;
					}
					return false;
				}
				default:
					return false;
				}
				break;
			default:
				width = instance.Prop_Attr_Width;
				height = instance.Prop_Attr_Height;
				break;
			}
			return true;
		}

		public virtual void SetDynamicType(int nType)
		{
			throw new NotImplementedException();
		}

		protected virtual void FinishGenerationProcess()
		{
			CheckDownloadFile();
			CancelGuiStatus();
			_bCorrectTemplating = false;
		}

		protected virtual void DownloadCaliFile()
		{
			if (_loadFileStatus > 0)
			{
				_loadFileStatus--;
				int fileType = (int)_downloadList[_loadFileStatus].fileType;
				string fullPath = _downloadList[_loadFileStatus].fullPath;
				int num = 0;
				num = ((fileType == 5) ? 1 : _nIndex);
				int num2 = DetInstance.Detector.Invoke(3017, fileType, num, fullPath, _strDesc);
				if (1 != num2 && num2 != 0)
				{
					if (_downloadWnd != null)
					{
						_downloadWnd.OnClose();
					}
					MessageColor = WarningColor;
					MessageInfo = "Download failed!";
					return;
				}
				switch (fileType)
				{
				case 2:
					MessageColor = NormalColor;
					MessageInfo = "Downloading gain ...";
					break;
				case 4:
					MessageColor = NormalColor;
					MessageInfo = "Downloading defect ...";
					break;
				case 5:
					MessageColor = NormalColor;
					MessageInfo = "Downloading lag ...";
					break;
				}
			}
			else
			{
				if (_downloadWnd != null)
				{
					_downloadWnd.OnClose();
				}
				MessageColor = NormalColor;
				MessageInfo = "Download succeed!";
			}
		}

		public void ClearGuiInitFailed()
		{
			CancelGuiStatus();
			_bCorrectTemplating = false;
		}
	}
}
