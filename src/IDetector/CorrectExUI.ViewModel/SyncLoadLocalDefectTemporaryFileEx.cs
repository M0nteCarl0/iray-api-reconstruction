using iDetector;
using System;
using System.Threading;

namespace CorrectExUI.ViewModel
{
	public class SyncLoadLocalDefectTemporaryFileEx
	{
		protected bool[,] LocalTemplateFileStatus;

		private UpdateStatusProgress UpdateLoadLocalStatus;

		private int OneGroupDefectImageNumber;

		private int GroupNumber;

		private int TotalDefectFiles;

		private DetecInfo DetInstance;

		private int TaskTimeOutSeconds;

		private string LocalFilePath;

		public event Action<bool, int, LoadLocalFileStatusEx> GroupLoadFinished;

		public SyncLoadLocalDefectTemporaryFileEx(DetecInfo detector, string filePath, int groupNumber, int oneGroupDefectImageNumber, int timeoutSeconds)
		{
			DetInstance = detector;
			LocalFilePath = filePath;
			GroupNumber = groupNumber;
			TaskTimeOutSeconds = timeoutSeconds;
			OneGroupDefectImageNumber = oneGroupDefectImageNumber;
			TotalDefectFiles = GroupNumber * OneGroupDefectImageNumber;
			LocalTemplateFileStatus = new bool[groupNumber, oneGroupDefectImageNumber];
			Array.Clear(LocalTemplateFileStatus, 0, LocalTemplateFileStatus.Length);
		}

		public void StartLoadLocalFile()
		{
			UpdateLoadLocalStatus = new UpdateStatusProgress(DetInstance.Shell.ParentUI, "Load local file", 100, TaskTimeOutSeconds * 1000);
			UpdateLoadLocalStatus.RunTask();
			UpdateLoadLocalStatus.SetStatus("Start load local template file");
			new Thread((ThreadStart)delegate
			{
				SyncLoadLocalDefectTemporaryFileEx syncLoadLocalDefectTemporaryFileEx = this;
				int groupIndex = default(int);
				int totalIndex = default(int);
				bool retResult = default(bool);
				for (groupIndex = 0; groupIndex < GroupNumber; groupIndex++)
				{
					for (int i = 0; i < OneGroupDefectImageNumber; i++)
					{
						totalIndex = groupIndex * OneGroupDefectImageNumber + i;
						retResult = LoadTemporaryDefectFile(totalIndex);
						DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
						{
							syncLoadLocalDefectTemporaryFileEx.UpdateLoadProgress(retResult, totalIndex + 1);
						}, new object[0]);
						LocalTemplateFileStatus[groupIndex, i] = retResult;
					}
					Thread.Sleep(50);
					DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
					{
						if (groupIndex == syncLoadLocalDefectTemporaryFileEx.GroupNumber - 1)
						{
							syncLoadLocalDefectTemporaryFileEx.StopLoadLocalFile();
							syncLoadLocalDefectTemporaryFileEx.GroupLoadFinished(true, groupIndex, syncLoadLocalDefectTemporaryFileEx.CheckLocalFileCompleteness(groupIndex));
						}
						else
						{
							syncLoadLocalDefectTemporaryFileEx.GroupLoadFinished(false, groupIndex, syncLoadLocalDefectTemporaryFileEx.CheckLocalFileCompleteness(groupIndex));
						}
					}, new object[0]);
				}
			}).Start();
		}

		public void UpdateLoadProgress(bool prevStatus, int curIndex)
		{
			string str = "Load local file:" + curIndex.ToString() + "/" + TotalDefectFiles.ToString();
			str = ((!prevStatus) ? (str + " - Failed") : (str + " - Succeed"));
			UpdateLoadLocalStatus.SetStatus(str);
		}

		public void StopLoadLocalFile()
		{
			UpdateLoadLocalStatus.StopTask();
		}

		public LoadLocalFileStatusEx CheckLocalFileCompleteness(int groupIndex)
		{
			int num = 0;
			for (int i = 0; i < OneGroupDefectImageNumber; i++)
			{
				if (!LocalTemplateFileStatus[groupIndex, i])
				{
					num++;
				}
			}
			if (num == OneGroupDefectImageNumber)
			{
				return LoadLocalFileStatusEx.None;
			}
			if (num == 0)
			{
				return LoadLocalFileStatusEx.Ok;
			}
			return LoadLocalFileStatusEx.Incomplete;
		}

		private bool LoadTemporaryDefectFile(int fileIndex)
		{
			if (fileIndex >= DetInstance.Detector.Prop_Attr_DefectTotalFrames)
			{
				return false;
			}
			lock (this)
			{
				int num = DetInstance.Detector.Invoke(3008, 0, LocalFilePath, fileIndex);
				return 0 == num;
			}
		}
	}
}
