using CorrectUI;
using iDetector;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExDynamicDefectPage4MNVM : GenerateExDynamicTemplate
	{
		private const int DoseCount = 4;

		private DoseAlgorithm DoseAlgorithm;

		private string[] KVList;

		private DoseInfo[] DefectDoseList;

		private List<GrayMap> GrayList;

		private int mReceivedValidFrames;

		public GenerateExDynamicDefectPage4MNVM(DetecInfo instance)
			: base(instance)
		{
			InitDoseInfo();
			int num = instance.Detector.Prop_Attr_DefectTotalFrames / 2;
			num = ((num == 0) ? 8 : num);
			KVList = new string[num];
			LoadKVList(KVList);
			DefectDoseList = new DoseInfo[num];
			int num2 = 0;
			for (int i = 0; i < GrayList.Count; i++)
			{
				for (int j = 0; j < GrayList[i].Frames; j++)
				{
					DefectDoseList[num2] = new DoseInfo(string.Format("{0}kV", GrayList[i].KVValue));
					num2++;
				}
			}
			InitImagelist(KVList);
			InitCheckDownload(1);
			base.ProgressValueVisibility = true;
			base.ExProgressVisibility = true;
		}

		private void InitDoseInfo()
		{
			string str = "mAs";
			string lpAppName = "Algorithm";
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncInAlgorithm";
				}
				else if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncOutAlgorithm";
				}
				break;
			}
			DoseAlgorithm = new DoseAlgorithm();
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			uint privateProfileInt = IniParser.GetPrivateProfileInt("Study", "DoseCount", 3, lpFileName);
			for (int i = 0; i < privateProfileInt; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Study", lpKeyName, 0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_1";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_2";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = "Coeff" + (i + 1).ToString();
				double privateProfileDouble = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				lpKeyName = "Intercept" + (i + 1).ToString();
				double privateProfileDouble2 = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				DoseAlgorithm.AddCoeffItem(privateProfileInt2, privateProfileDouble, privateProfileDouble2);
			}
			DoseAlgorithm.UpdateTimeCoeff(IniParser.GetPrivateProfileDouble(lpAppName, "TimeCoeff", 1.0, lpFileName));
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			int privateProfileInt = (int)IniParser.GetPrivateProfileInt("defect", "Cfg_FramesOfHighestDose", 5, DetInstance.Detector.Prop_Attr_WorkDir + "calib.ini");
			GrayList = new List<GrayMap>
			{
				new GrayMap(1000, 70, 1),
				new GrayMap(5000, 140, 1),
				new GrayMap(10000, 110, 1),
				new GrayMap(2000, 90, privateProfileInt)
			};
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			int num = GrayList.Count();
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i < num)
				{
					uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Defect", lpKeyName, GrayList[i].KVValue, lpFileName);
					kvlist[i] = privateProfileInt2.ToString();
					GrayList[i].KVValue = (int)privateProfileInt2;
					GrayList[i].GrayValue = (int)IniParser.GetPrivateProfileInt("Defect", string.Format("Gray{0}", i + 1), GrayList[i].KVValue, lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileInt("Defect", lpKeyName, GrayList[num - 1].KVValue, lpFileName).ToString();
				}
			}
		}

		protected override void GetGroupInfo(out int groupNumbers, out int eachGroupDefectImageNumber)
		{
			groupNumbers = KVList.Length;
			eachGroupDefectImageNumber = 1;
		}

		protected override void OnClickStartBtn()
		{
			InitForCreateTemplate(3007, "DEFECT");
			SetDoseInfo(DefectDoseList[0].GetDoseInfo());
		}

		protected override int StartSelectImages()
		{
			int groupIndex = GetGroupIndex();
			int result = DetInstance.InvokeProxy.Invoke(3033, groupIndex, GrayList[groupIndex].Frames);
			mReceivedValidFrames = DetInstance.Detector.Prop_Attr_DefectValidFrames;
			return result;
		}

		protected override void EnterExposeView()
		{
			int groupIndex = GetGroupIndex();
			int num = 0;
			for (int i = 0; i < groupIndex; i++)
			{
				num += GrayList[i].Frames;
			}
			if (GrayList[groupIndex].Frames > 1)
			{
				for (int j = 0; j < GrayList[groupIndex].Frames; j++)
				{
					AcquiredStatus[num + j] = false;
				}
			}
			base.EnterExposeView();
		}

		protected override void AcquiredImage()
		{
			base.AcquiredImage();
			if (EnableBufferCine && DetInstance.Detector.Prop_Attr_DefectValidFrames - mReceivedValidFrames > 0)
			{
				mReceivedValidFrames = DetInstance.Detector.Prop_Attr_DefectValidFrames;
				UpdateValidImage();
				Convert.ToInt32(KVList[ImageSelectedIndex]);
				int groupIndex = GetGroupIndex();
				if (GrayList[groupIndex].Frames > 1 && ImageSelectedIndex < KVList.Length - 1)
				{
					ImageSelectedIndex++;
				}
			}
		}

		private int GetGroupIndex()
		{
			int result = ImageSelectedIndex;
			if (ImageSelectedIndex >= GrayList.Count)
			{
				result = GrayList.Count - 1;
			}
			return result;
		}

		private void UpdateValidImage()
		{
			AcquiredStatus[ImageSelectedIndex] = true;
		}

		protected override void UpdateValidLightImageStatus()
		{
			Convert.ToInt32(KVList[ImageSelectedIndex]);
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			int groupIndex = GetGroupIndex();
			if (num <= GrayList[groupIndex].Frames)
			{
				UpdateProgressInner(num);
				if (num >= GrayList[groupIndex].Frames && GrayList[groupIndex].Frames == num)
				{
					StopCheckFramesMonitor();
					AcquireDarkImages();
					EnableBufferCine = false;
				}
			}
		}

		protected override void UpdateValidDarkImageStatus()
		{
			Convert.ToInt32(KVList[ImageSelectedIndex]);
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			UpdateProgressInner(num);
			int groupIndex = GetGroupIndex();
			if (num >= GrayList[groupIndex].Frames * 2 && GrayList[groupIndex].Frames * 2 == num)
			{
				StopCheckFramesMonitor();
				DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
				{
					GroupAcquireFinished();
				}, new object[0]);
				EnterNextStage(Enm_CreateStage.SelectFinished);
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			if (frameIndex < DefectDoseList.Length)
			{
				SetDoseInfo(DefectDoseList[frameIndex].GetDoseInfo());
			}
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			frameIndex = ((frameIndex < 4) ? frameIndex : 3);
			Convert.ToInt32(KVList[frameIndex]);
			GetGroupIndex();
			return GrayList[GetGroupIndex()].GrayValue;
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			if (IsInExposeView)
			{
				Convert.ToInt32(KVList[ImageSelectedIndex]);
				base.AcquiedProgress = acquiredFrames.ToString() + "/" + GrayList[GetGroupIndex()].Frames * 2;
			}
		}

		private void UpdateProgressInner(int acquiredFrames)
		{
			Convert.ToInt32(KVList[ImageSelectedIndex]);
			base.AcquiedProgress = acquiredFrames.ToString() + "/" + GrayList[GetGroupIndex()].Frames * 2;
		}
	}
}
