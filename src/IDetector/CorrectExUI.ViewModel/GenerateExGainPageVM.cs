using CorrectUI;
using iDetector;
using iDetector.PopupWnds;
using System;
using System.Threading;
using System.Windows;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExGainPageVM : GenerateExStaticTemplate
	{
		public new delegate void AsyncInvokeMethod();

		private const int TaskTimeOutSeconds = 60;

		private DoseInfo GainDose;

		private DoseAlgorithm DoseAlgorithm;

		private int ExpectedGray = 5500;

		private uint DoseKV = 70u;

		private string[] KVList;

		private DlgTaskTimeOut CloseCreateWnd;

		private UITimer ConnectStateTimer;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public GenerateExGainPageVM(DetecInfo detInstance)
			: base(detInstance)
		{
			InitList();
			SetOffsetType(true);
			InitCheckDownload(1);
		}

		private void InitDoseInfo()
		{
			string str = "mAs";
			string lpAppName = "Algorithm";
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncInAlgorithm";
				}
				else if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncOutAlgorithm";
				}
				break;
			}
			DoseAlgorithm = new DoseAlgorithm();
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			uint privateProfileInt = IniParser.GetPrivateProfileInt("Study", "DoseCount", 3, lpFileName);
			for (int i = 0; i < privateProfileInt; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Study", lpKeyName, 0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_1";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_2";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = "Coeff" + (i + 1).ToString();
				double privateProfileDouble = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				lpKeyName = "Intercept" + (i + 1).ToString();
				double privateProfileDouble2 = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				DoseAlgorithm.AddCoeffItem(privateProfileInt2, privateProfileDouble, privateProfileDouble2);
			}
			DoseAlgorithm.UpdateTimeCoeff(IniParser.GetPrivateProfileDouble(lpAppName, "TimeCoeff", 1.0, lpFileName));
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i == 0)
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, DoseKV.ToString(), lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, kvlist[0], lpFileName);
				}
			}
			ExpectedGray = (int)IniParser.GetPrivateProfileInt("Gain", "Gray", ExpectedGray, lpFileName);
		}

		private int GetExpectedGrayValue(Detector detector)
		{
			int num = 0;
			switch (detector.Prop_Attr_UROM_ProductNo)
			{
			case 45:
				num = 40000;
				break;
			case 41:
			case 42:
			case 51:
			case 52:
			case 60:
			case 61:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				num = 20000;
				break;
			case 32:
			case 37:
				num = ((detector.Prop_Attr_UROM_TriggerMode != 5) ? 5500 : 10000);
				break;
			case 39:
			case 59:
			case 62:
				num = ((detector.Prop_Attr_UROM_TriggerMode != 5) ? 5500 : ((detector.Prop_Attr_UROM_FreesyncSubFlow != 3) ? 10000 : 7000));
				break;
			default:
				num = 5500;
				break;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			if (IniParser.GetPrivateProfileInt("Gain_DQE", "DQE_Enable", 0, lpFileName) != 0)
			{
				num = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Expected", num, lpFileName);
			}
			return num;
		}

		protected override void InitFinished()
		{
			InitImagelist(KVList);
			PrepareForAcquire();
			base.InitFinished();
		}

		protected override void OnClickStartBtn()
		{
			if (!CanCreateTemplate())
			{
				return;
			}
			if (IsVisibleOffsetWindow())
			{
				int num = 0;
				if (GetOffsetInfo_VenuX())
				{
					MessageBox.Show("Offset type have not been set.\nClick 'OK' button to set hardware post offset.", "Info", MessageBoxButton.OK, MessageBoxImage.Asterisk);
					num = 2;
				}
				else
				{
					SelectOffsetTypeWnd selectOffsetTypeWnd = new SelectOffsetTypeWnd(DetInstance.Detector);
					if (selectOffsetTypeWnd.ShowDialog() == false)
					{
						return;
					}
					num = selectOffsetTypeWnd._setCorrectOption;
				}
				this.SdkEventHandler = ProcessEvent;
				if (ConnectStateTimer == null)
				{
					DetInstance.Detector.SdkCallbackEvent += OnSdkCallback;
					ConnectStateTimer = new UITimer(800, MonitorChannelStateChange);
					ConnectStateTimer.Start();
				}
				int num2 = DetInstance.Detector.Invoke(6, num);
				if (1 == num2)
				{
					CloseCreateWnd = new DlgTaskTimeOut("Initialization", "Initializing for gain...", 60, null, InitFailed);
					CloseCreateWnd.RunTask();
				}
			}
			else
			{
				Log.Instance().Write("Start GainInit");
				InitForCreateTemplate(3002, "GAIN");
				if (InitedStatus)
				{
					InitList();
					InitFinished();
					SetDoseInfo(GainDose.GetDoseInfo());
					ExpectedGray = GetExpectedGrayValue(DetInstance.Detector);
					base.ExpectedValue = ExpectedGray.ToString();
				}
				else
				{
					base.InitFinished();
					base.OnClickCancelBtn();
					ClearGuiInitFailed();
				}
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override int SelectImage(int frameIndex)
		{
			return DetInstance.InvokeProxy.Invoke(3004, 0, frameIndex);
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			return ExpectedGray;
		}

		protected override void OnClickAcceptBtn()
		{
			base.OnClickAcceptBtn();
			base.EnableGenerateBtn = true;
			base.EnableCheckDownload = true;
		}

		private bool CanCreateTemplate()
		{
			bool result = true;
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Mammo1012F)
			{
				int prop_Attr_UROM_ExpMode = DetInstance.Detector.Prop_Attr_UROM_ExpMode;
				if (prop_Attr_UROM_ExpMode == 1 || prop_Attr_UROM_ExpMode == 2)
				{
					base.MessageColor = WarningColor;
					base.MessageInfo = "Correction template cann't be created in the Manual/AEC exposure mode!";
					result = false;
				}
			}
			return result;
		}

		private void InitFailed()
		{
			CloseCreateWnd.StopTask();
			base.MessageColor = WarningColor;
			base.MessageInfo = "Set correction failed! Timeout!";
		}

		private new void CloseMessageBox()
		{
			if (CloseCreateWnd != null)
			{
				CloseCreateWnd.StopTask();
				CloseCreateWnd = null;
				DetInstance.Detector.SdkCallbackEvent -= OnSdkCallback;
				SetOffsetType(true);
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			DetInstance.Shell.ParentUI.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
		}

		private void AsyncInvokeGainInit()
		{
			for (int i = 0; i < 10; i++)
			{
				if (1 == DetInstance.Detector.Prop_Attr_State)
				{
					break;
				}
				Thread.Sleep(500);
			}
			Log.Instance().Write("Start GainInit");
			InitForCreateTemplate(3002, "GAIN");
			if (InitedStatus)
			{
				InitList();
				InitFinished();
				SetDoseInfo(GainDose.GetDoseInfo());
				ExpectedGray = GetExpectedGrayValue(DetInstance.Detector);
				base.ExpectedValue = ExpectedGray.ToString();
			}
			else
			{
				base.InitFinished();
				base.OnClickCancelBtn();
				ClearGuiInitFailed();
			}
		}

		private void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			switch (nEventID)
			{
			case 6:
				CloseMessageBox();
				break;
			case 4:
				if (1005 != nParam1)
				{
					CloseMessageBox();
					AsyncInvokeMethod asyncInvokeMethod = AsyncInvokeGainInit;
					asyncInvokeMethod();
				}
				break;
			case 5:
				CloseMessageBox();
				if (6 == nParam1)
				{
					ConnectStateTimer = null;
					base.MessageColor = WarningColor;
					base.MessageInfo = "Set correction failed! Err=" + ErrorHelper.GetErrorDesp(nParam2);
				}
				break;
			case 17:
				if (30 == nParam2)
				{
					ConnectStateTimer.Stop();
					ConnectStateTimer = null;
					DetInstance.Detector.SdkCallbackEvent -= OnSdkCallback;
				}
				break;
			}
		}

		private void MonitorChannelStateChange()
		{
			if (DetInstance.Detector != null)
			{
				switch (DetInstance.Detector.Prop_Attr_ConnState)
				{
				}
			}
		}

		public void UpdateOffsetType()
		{
			SetOffsetType(true);
		}

		private void InitList()
		{
			InitDoseInfo();
			KVList = new string[Math.Max(DetInstance.Detector.Prop_Attr_GainTotalFrames, 1)];
			LoadKVList(KVList);
			ExpectedGray = GetExpectedGrayValue(DetInstance.Detector);
			GainDose = new DoseInfo(string.Format("{0}KV", KVList[0]));
		}

		public void Release()
		{
			if (ConnectStateTimer != null)
			{
				ConnectStateTimer.Stop();
				ConnectStateTimer = null;
			}
			DetInstance.Detector.SdkCallbackEvent -= OnSdkCallback;
			this.SdkEventHandler = null;
		}

		protected override GrayValueStatus IsRealGrayBeyondExpected(out int realValue, out int expectedValue)
		{
			GrayValueStatus result = base.IsRealGrayBeyondExpected(out realValue, out expectedValue);
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			if (IniParser.GetPrivateProfileInt("Gain_DQE", "DQE_Enable", 0, lpFileName) != 0)
			{
				int[] array = new int[8]
				{
					-2147483648,
					0,
					0,
					0,
					0,
					65535,
					65535,
					2147483647
				};
				array[3] = (int)(0.9f * (float)expectedValue);
				array[4] = (int)(1.1f * (float)expectedValue);
				int[] array2 = array;
				array2[6] = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Deny_Upper", array2[6], lpFileName);
				array2[5] = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Warn_Upper", array2[5], lpFileName);
				array2[4] = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Good_Upper", array2[4], lpFileName);
				array2[3] = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Good_Below", array2[3], lpFileName);
				array2[2] = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Warn_Below", array2[2], lpFileName);
				array2[1] = (int)IniParser.GetPrivateProfileInt("Gain_DQE", "KV1_Deny_Below", array2[1], lpFileName);
				int i;
				for (i = 1; i < array2.Length && (realValue < array2[i - 1] || realValue > array2[i]); i++)
				{
				}
				switch (i)
				{
				case 1:
				case 2:
					result = GrayValueStatus.DenyBelow;
					break;
				case 3:
					result = GrayValueStatus.WarnBelow;
					break;
				case 4:
					result = GrayValueStatus.Expected;
					break;
				case 5:
					result = GrayValueStatus.WarnAbove;
					break;
				case 6:
				case 7:
					result = GrayValueStatus.DenyAbove;
					break;
				}
			}
			return result;
		}

		protected override void OnClickCancelBtn()
		{
			base.OnClickCancelBtn();
		}

		private bool GetOffsetInfo_VenuX()
		{
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 60:
			case 72:
			case 80:
			case 88:
			case 91:
				return true;
			default:
				return false;
			}
		}
	}
}
