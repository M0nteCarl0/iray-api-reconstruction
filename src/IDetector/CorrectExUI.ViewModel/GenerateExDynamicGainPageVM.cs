using CorrectUI;
using iDetector;
using iDetector.PopupWnds;
using System;

namespace CorrectExUI.ViewModel
{
	internal class GenerateExDynamicGainPageVM : GenerateExDynamicTemplate
	{
		private DoseInfo GainDose;

		private string[] KVList;

		private DoseAlgorithm DoseAlgorithm;

		private int ExpectedGray = 1000;

		private uint DoseKV = 70u;

		public GenerateExDynamicGainPageVM(DetecInfo instance)
			: base(instance)
		{
			InitDoseInfo();
			KVList = new string[Math.Max(DetInstance.Detector.Prop_Attr_GainTotalFrames, 1)];
			LoadKVList(KVList);
			GainDose = new DoseInfo(string.Format("{0}KV", KVList[0]));
			InitImagelist(KVList);
			InitCheckDownload(1);
			base.ProgressValueVisibility = false;
			base.ExProgressVisibility = false;
			ImageSelectedIndex = 0;
		}

		private void InitDoseInfo()
		{
			string str = "mAs";
			string lpAppName = "Algorithm";
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncInAlgorithm";
				}
				else if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					lpAppName = "SyncOutAlgorithm";
				}
				break;
			}
			DoseAlgorithm = new DoseAlgorithm();
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			uint privateProfileInt = IniParser.GetPrivateProfileInt("Study", "DoseCount", 3, lpFileName);
			for (int i = 0; i < privateProfileInt; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Study", lpKeyName, 0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_1";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = str + (i + 1).ToString() + "_2";
				IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = "Coeff" + (i + 1).ToString();
				double privateProfileDouble = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				lpKeyName = "Intercept" + (i + 1).ToString();
				double privateProfileDouble2 = IniParser.GetPrivateProfileDouble(lpAppName, lpKeyName, 0.0, lpFileName);
				DoseAlgorithm.AddCoeffItem(privateProfileInt2, privateProfileDouble, privateProfileDouble2);
			}
			DoseAlgorithm.UpdateTimeCoeff(IniParser.GetPrivateProfileDouble(lpAppName, "TimeCoeff", 1.0, lpFileName));
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i == 0)
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, DoseKV.ToString(), lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, kvlist[0], lpFileName);
				}
			}
			ExpectedGray = (int)IniParser.GetPrivateProfileInt("Gain", "Gray", ExpectedGray, lpFileName);
		}

		protected override void OnClickStartBtn()
		{
			if (!CanCreateTemplate())
			{
				return;
			}
			if (IsVisibleOffsetWindow())
			{
				SelectOffsetTypeWnd selectOffsetTypeWnd = new SelectOffsetTypeWnd(DetInstance.Detector);
				if (selectOffsetTypeWnd.ShowDialog() == false)
				{
					return;
				}
			}
			InitForCreateTemplate(3002, "GAIN");
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override int StartSelectImages()
		{
			return DetInstance.InvokeProxy.Invoke(3005, ImageSelectedIndex, KVList.Length);
		}

		protected override void EnterExposeView()
		{
			for (int i = 0; i < AcquiredStatus.Count; i++)
			{
				AcquiredStatus[i] = false;
			}
			base.EnterExposeView();
		}

		protected override void AcquiredImage()
		{
			base.AcquiredImage();
			if (!EnableBufferCine)
			{
				return;
			}
			SelectImageSucceed();
			if (ImageSelectedIndex < KVList.Length - 1)
			{
				ImageSelectedIndex++;
				base.EnableAcquireBtn = false;
				base.StageValue = ImageSelectedIndex.ToString() + "/" + GroupNumber.ToString();
				return;
			}
			EnableBufferCine = false;
			EnterNextStage(Enm_CreateStage.SelectFinished);
			if (IsAcquireCompleted())
			{
				base.AcquiedProgress = CompleteStr;
			}
			base.EnableAcquireBtn = true;
			base.EnableGenerateBtn = true;
			base.StageValue = (ImageSelectedIndex + 1).ToString() + "/" + GroupNumber.ToString();
			ImageSelectedIndex = 0;
		}

		protected override void UpdateValidLightImageStatus()
		{
			StopCheckFramesMonitor();
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			return ExpectedGray;
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			base.AcquiedProgress = acquiredFrames.ToString() + "/" + KVList.Length;
		}

		private bool CanCreateTemplate()
		{
			bool result = true;
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (DetInstance.Detector.Prop_Attr_GainTotalFrames == 0)
				{
					base.MessageColor = WarningColor;
					base.MessageInfo = "Frame number cannot be 0, Please reset the value in configure file!";
					result = false;
				}
				break;
			}
			return result;
		}
	}
}
