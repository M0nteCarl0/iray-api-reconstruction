using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class SeqSaveSetWnd : Window, INotifyPropertyChanged, IComponentConnector
	{
		private readonly string WarningColor = "#FFF0F000";

		private readonly string NormalColor = "";

		private string superpostionBGColor;

		private string reduceNoiseBGColor;

		private string framesBGColor;

		private string coeffBGColor;

		public bool Result;

		public Enm_SeqSettingtype OperationType;

		private WeakReference SeqSettingsWeakRef;

		private uint mEachFrameSize;

		public int MaxFrameNumber;

		internal TabItem SettingTab;

		internal DigitalTextBox _SaveFrames;

		internal Label xMaxNumberHint;

		internal CheckBox EnableBufferCine;

		internal TabItem SaveTab;

		internal TextBox _FilePath;

		internal DigitalTextBox xStartIndex;

		internal DigitalTextBox xEndIndex;

		internal TabItem ProcessTab;

		internal CheckBox EnableSuperpostion;

		internal DigitalTextBox xMaxCompositeFrameNumber;

		internal CheckBox EnableReduceNoise;

		internal TextBox xNoiseCoeff;

		internal Label xNotes;

		private bool _contentLoaded;

		public string SuperpostionBGColor
		{
			get
			{
				return superpostionBGColor;
			}
			set
			{
				superpostionBGColor = value;
				NotifyPropertyChanged("SuperpostionBGColor");
			}
		}

		public string ReduceNoiseBGColor
		{
			get
			{
				return reduceNoiseBGColor;
			}
			set
			{
				reduceNoiseBGColor = value;
				NotifyPropertyChanged("ReduceNoiseBGColor");
			}
		}

		public string FramesBGColor
		{
			get
			{
				return framesBGColor;
			}
			set
			{
				framesBGColor = value;
				NotifyPropertyChanged("FramesBGColor");
			}
		}

		public string CoeffBGColor
		{
			get
			{
				return coeffBGColor;
			}
			set
			{
				coeffBGColor = value;
				NotifyPropertyChanged("CoeffBGColor");
			}
		}

		public string FilePath
		{
			get
			{
				return _FilePath.Text;
			}
			set
			{
				_FilePath.Text = value;
			}
		}

		public int Frames
		{
			get
			{
				return _SaveFrames.Value;
			}
			set
			{
				_SaveFrames.Text = value.ToString();
			}
		}

		public bool IsEnableCine
		{
			get
			{
				return EnableBufferCine.IsChecked == true;
			}
			set
			{
				EnableBufferCine.IsChecked = value;
			}
		}

		private SeqSaveSetSave SeqSettings
		{
			get
			{
				return SeqSettingsWeakRef.Target as SeqSaveSetSave;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyPropertyChanged(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		public SeqSaveSetWnd(ref SeqSaveSetSave seqSet, int eachFrameSize)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			mEachFrameSize = (uint)eachFrameSize;
			base.Owner = Application.Current.MainWindow;
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				if (seqSet != null)
				{
					FilePath = seqSet.FilePath;
					Frames = seqSet.Frames;
					IsEnableCine = seqSet.IsEnableCine;
					EnableSuperpostion.IsChecked = seqSet.EnableSuperpostion;
					EnableReduceNoise.IsChecked = seqSet.EnableReduceNoise;
					xMaxCompositeFrameNumber.Text = seqSet.MaxAddFrames.ToString();
					xNoiseCoeff.Text = seqSet.NoiseCoeff.ToString();
					SeqSettingsWeakRef = new WeakReference(seqSet);
				}
				SetMaxFrameNumber(mEachFrameSize);
				xMaxNumberHint.Content = MaxFrameNumber.ToString();
				base.DataContext = this;
			}
		}

		public void SetMaxFrameNumber(uint eachFrameSize)
		{
			_SetMaxFrameNumber(eachFrameSize);
			if (Frames > MaxFrameNumber)
			{
				Frames = MaxFrameNumber;
			}
		}

		private void _SetMaxFrameNumber(uint eachFrameSize)
		{
			long num = 1073741824L;
			MaxFrameNumber = (int)(20 * num / (long)eachFrameSize) / 1000 * 1000;
			MaxFrameNumber = Math.Min(10000, MaxFrameNumber);
		}

		private void Button_Click_OK(object sender, RoutedEventArgs e)
		{
			if (SaveTab.IsSelected)
			{
				if (_FilePath.Text.Length == 0)
				{
					MessageBox.Show("File path cannot be empty!");
					return;
				}
				string text = _FilePath.Text;
				string text2 = text.Substring(text.LastIndexOf(".") + 1, text.Length - text.LastIndexOf(".") - 1);
				if (text2.Length == 0 || (!text2.ToLower().Equals("raw") && !text2.ToLower().Equals("tif")))
				{
					_FilePath.Text = text + ".tif";
				}
				OperationType = Enm_SeqSettingtype.Save;
				SeqSettings.FilePath = _FilePath.Text;
				SeqSettings.StartIndex = ((xStartIndex.Value > 0) ? (xStartIndex.Value - 1) : xStartIndex.Value);
				SeqSettings.EndIndex = ((xEndIndex.Value > 0) ? (xEndIndex.Value - 1) : xEndIndex.Value);
			}
			else if (SettingTab.IsSelected)
			{
				if (Frames == 0)
				{
					MessageBox.Show("Frame number cannot be empty or 0!");
					return;
				}
				if (Frames > MaxFrameNumber)
				{
					MessageBox.Show(string.Format("Please input a number 1~{0}!", MaxFrameNumber));
					return;
				}
				int num = StoreStrategy.CalcMaxFrames(mEachFrameSize, (uint)Frames);
				if (num < Frames && IsEnableCine)
				{
					MessageBox.Show(string.Format("Please reduce framerate, or it will lose frame when write to disk"));
				}
				OperationType = Enm_SeqSettingtype.Setting;
				SeqSettings.Frames = Frames;
				SeqSettings.IsEnableCine = IsEnableCine;
			}
			else if (ProcessTab.IsSelected)
			{
				OperationType = Enm_SeqSettingtype.ProcessImg;
				SeqSettings.EnableSuperpostion = ((EnableSuperpostion.IsChecked == true) ? true : false);
				SeqSettings.EnableReduceNoise = ((EnableReduceNoise.IsChecked == true) ? true : false);
				SeqSettings.MaxAddFrames = xMaxCompositeFrameNumber.Value;
				if (!float.TryParse(xNoiseCoeff.Text, out SeqSettings.NoiseCoeff))
				{
					MessageBox.Show("Please input a legal float!");
					return;
				}
				if (SeqSettings.NoiseCoeff > 1f || SeqSettings.NoiseCoeff < 0f)
				{
					SeqSettings.NoiseCoeff = 0f;
					MessageBox.Show(Application.Current.MainWindow, "The coeff should be a float in [0, 1]!");
					return;
				}
			}
			Result = true;
			Hide();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			Result = false;
			base.OnClosing(e);
		}

		private void OnOpenPath(object sender, RoutedEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.DefaultExt = ".tif | .raw";
			saveFileDialog.Filter = "tif files(*.tif)|*.tif|raw files(*.raw)|*.raw";
			if (saveFileDialog.ShowDialog() == true)
			{
				_FilePath.Text = saveFileDialog.FileName;
			}
		}

		private void OnSelectTab(object sender, SelectionChangedEventArgs e)
		{
			if (ProcessTab.IsSelected)
			{
				xNotes.Visibility = Visibility.Collapsed;
			}
			else
			{
				xNotes.Visibility = Visibility.Visible;
			}
		}

		private void OnCheckChange(object sender, RoutedEventArgs e)
		{
			if (SeqSettingsWeakRef == null)
			{
				return;
			}
			CheckBox checkBox = sender as CheckBox;
			if (checkBox == EnableReduceNoise)
			{
				bool flag = (checkBox.IsChecked == true) ? true : false;
				if (SeqSettings.EnableReduceNoise ^ flag)
				{
					ReduceNoiseBGColor = WarningColor;
				}
				else
				{
					ReduceNoiseBGColor = NormalColor;
				}
			}
			else if (checkBox == EnableSuperpostion)
			{
				bool flag2 = (checkBox.IsChecked == true) ? true : false;
				if (SeqSettings.EnableSuperpostion ^ flag2)
				{
					SuperpostionBGColor = WarningColor;
				}
				else
				{
					SuperpostionBGColor = NormalColor;
				}
			}
		}

		private void OnChanged(object sender, TextChangedEventArgs e)
		{
			if (SeqSettingsWeakRef == null)
			{
				return;
			}
			TextBox textBox = sender as TextBox;
			float result2;
			if (textBox == xMaxCompositeFrameNumber)
			{
				int result;
				if (int.TryParse(textBox.Text, out result))
				{
					if (SeqSettings.MaxAddFrames.Equals(result))
					{
						FramesBGColor = NormalColor;
					}
					else
					{
						FramesBGColor = WarningColor;
					}
				}
			}
			else if (textBox == xNoiseCoeff && float.TryParse(textBox.Text, out result2))
			{
				if (SeqSettings.NoiseCoeff.Equals(result2))
				{
					CoeffBGColor = NormalColor;
				}
				else
				{
					CoeffBGColor = WarningColor;
				}
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/seqsavesetwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((TabControl)target).SelectionChanged += OnSelectTab;
				break;
			case 2:
				SettingTab = (TabItem)target;
				break;
			case 3:
				_SaveFrames = (DigitalTextBox)target;
				break;
			case 4:
				xMaxNumberHint = (Label)target;
				break;
			case 5:
				EnableBufferCine = (CheckBox)target;
				break;
			case 6:
				SaveTab = (TabItem)target;
				break;
			case 7:
				_FilePath = (TextBox)target;
				break;
			case 8:
				((Button)target).Click += OnOpenPath;
				break;
			case 9:
				xStartIndex = (DigitalTextBox)target;
				break;
			case 10:
				xEndIndex = (DigitalTextBox)target;
				break;
			case 11:
				ProcessTab = (TabItem)target;
				break;
			case 12:
				EnableSuperpostion = (CheckBox)target;
				EnableSuperpostion.Click += OnCheckChange;
				break;
			case 13:
				xMaxCompositeFrameNumber = (DigitalTextBox)target;
				break;
			case 14:
				EnableReduceNoise = (CheckBox)target;
				EnableReduceNoise.Click += OnCheckChange;
				break;
			case 15:
				xNoiseCoeff = (TextBox)target;
				xNoiseCoeff.TextChanged += OnChanged;
				break;
			case 16:
				xNotes = (Label)target;
				break;
			case 17:
				((Button)target).Click += Button_Click_OK;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
