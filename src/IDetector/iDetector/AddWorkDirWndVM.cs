using System.Collections.ObjectModel;

namespace iDetector
{
	public class AddWorkDirWndVM : NotifyObject
	{
		private EnumDisplayItem _CurProduct;

		private string _WorkDir;

		public ObservableCollection<EnumDisplayItem> ProductList
		{
			get;
			set;
		}

		public EnumDisplayItem CurProduct
		{
			get
			{
				return _CurProduct;
			}
			set
			{
				_CurProduct = value;
				NotifyPropertyChanged("CurProduct");
			}
		}

		public string WorkDir
		{
			get
			{
				return _WorkDir;
			}
			set
			{
				_WorkDir = value;
				NotifyPropertyChanged("WorkDir");
			}
		}

		public DelegateCommand AddWorkDir
		{
			get;
			set;
		}

		public AddWorkDirWndVM(int productNo, string workDir)
		{
			ProductList = SDKEnumCollector.LoadParam("Enm_ProdType");
			WorkDir = workDir;
			foreach (EnumDisplayItem product in ProductList)
			{
				if (product.Val == productNo)
				{
					CurProduct = product;
					break;
				}
			}
			if (CurProduct == null)
			{
				CurProduct = ProductList[0];
			}
		}
	}
}
