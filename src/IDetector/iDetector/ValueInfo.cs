namespace iDetector
{
	public struct ValueInfo
	{
		public double dwEvenAVG;

		public double dwEvenSV;

		public double dwOddAVG;

		public double dwOddSV;
	}
}
