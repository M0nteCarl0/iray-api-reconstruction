using System.Windows;

namespace iDetector
{
	public class Shell
	{
		private Window Window;

		public Window ParentUI
		{
			get
			{
				return Window;
			}
		}

		public Shell()
		{
		}

		public Shell(Window window)
		{
			Window = window;
		}

		public void ShowMessage(string message, string title = null)
		{
			if (title == null)
			{
				if (Window == null)
				{
					MessageBox.Show(message);
				}
				else
				{
					MessageBox.Show(Window, message);
				}
			}
			else if (Window == null)
			{
				MessageBox.Show(Application.Current.MainWindow, message, title);
			}
			else
			{
				MessageBox.Show(Window, message, title);
			}
		}

		public MessageBoxResult ShowYesNoMessage(string message, string caption)
		{
			if (Window == null)
			{
				return MessageBox.Show(message, caption, MessageBoxButton.YesNo, MessageBoxImage.Question);
			}
			return MessageBox.Show(Window, message, caption, MessageBoxButton.YesNo, MessageBoxImage.Question);
		}

		public static void ShowMessageAutoOwner(string message)
		{
			MessageBox.Show(message);
		}

		public static void ShowMessageOwnerByMainwindow(string message, string title = null)
		{
			if (title == null)
			{
				MessageBox.Show(Application.Current.MainWindow, message);
			}
			else
			{
				MessageBox.Show(Application.Current.MainWindow, message, title);
			}
		}

		public static MessageBoxResult ShowYesNoMessageOwnerByMainwindow(string message, string caption)
		{
			return MessageBox.Show(Application.Current.MainWindow, message, caption, MessageBoxButton.YesNo, MessageBoxImage.Question);
		}
	}
}
