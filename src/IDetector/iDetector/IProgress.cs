namespace iDetector
{
	public interface IProgress
	{
		bool IsTaskRunning
		{
			get;
			set;
		}

		int RunTask();

		void StopTask();
	}
}
