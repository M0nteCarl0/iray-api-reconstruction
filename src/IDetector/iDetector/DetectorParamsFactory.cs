using System.Collections.Generic;

namespace iDetector
{
	public class DetectorParamsFactory
	{
		public static List<AttrItem> LoadAttr(int productNo)
		{
			switch (productNo)
			{
			case 6:
			case 56:
				return LoadMercuXAttr();
			case 68:
			case 73:
				return LoadMercu0505Attr();
			case 74:
				return LoadJupi0606Attr();
			case 49:
			case 50:
				return LoadMercu1616TEAttr();
			case 48:
				return LoadMercu1717VAttr();
			case 46:
			case 87:
				return LoadPluto0406Attr();
			case 32:
			case 37:
			case 39:
			case 41:
			case 42:
			case 51:
			case 52:
			case 59:
			case 60:
			case 61:
			case 62:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				return LoadMarsXAttr(productNo);
			case 11:
				return LoadMommoAttr();
			case 38:
				return LoadVenuMFAttr();
			case 58:
				return LoadVenuMNAttr();
			case 45:
				return Load1717XUAttr();
			default:
				return LoadOtherAttr();
			}
		}

		private static List<AttrItem> LoadMercuXAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("Zoom Mode", 2014, 0));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("PowSeriesCorrect Enable", 2071, 2545));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 0));
			list.Add(new AttrItem("Exp Window Time", 2027, 0));
			list.Add(new AttrItem("ExpTime Valid Percent", 2066, 2537));
			list.Add(new AttrItem("PGA", 2030, 0));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadMercu0505Attr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("ROIColStartPos", 2073, 0));
			list.Add(new AttrItem("ROIRowStartPos", 2075, 0));
			list.Add(new AttrItem("ROIColEndPos", 2074, 0));
			list.Add(new AttrItem("ROIRowEndPos", 2076, 0));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("Sequence Interval Time", 2022, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 0));
			list.Add(new AttrItem("Exp Window Time", 2028, 0));
			list.Add(new AttrItem("Exp Mode", 2050, 0));
			list.Add(new AttrItem("PGA", 2030, 0));
			list.Add(new AttrItem("Pulse Clear Times", 2072, 2551));
			list.Add(new AttrItem("ExpTime Valid Percent", 2066, 2537));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadJupi0606Attr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 2518));
			list.Add(new AttrItem("Src MAC", 2037, 2519));
			list.Add(new AttrItem("Dest Port", 2038, 2520));
			list.Add(new AttrItem("Dest IP", 2039, 2521));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("ROIColStartPos", 2073, 0));
			list.Add(new AttrItem("ROIRowStartPos", 2075, 0));
			list.Add(new AttrItem("ROIColEndPos", 2074, 0));
			list.Add(new AttrItem("ROIRowEndPos", 2076, 0));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("Sequence Interval Time", 2022, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 0));
			list.Add(new AttrItem("Exp Window Time", 2028, 0));
			list.Add(new AttrItem("Exp Mode", 2050, 0));
			list.Add(new AttrItem("PGA", 2030, 0));
			list.Add(new AttrItem("Pulse Clear Times", 2072, 2551));
			list.Add(new AttrItem("ExpTime Valid Percent", 2066, 2537));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadMercu1616TEAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("Zoom Mode", 2014, 0));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 0));
			list.Add(new AttrItem("Exp Window Time", 2027, 0));
			list.Add(new AttrItem("Exp Mode", 2050, 0));
			list.Add(new AttrItem("PGA", 2030, 0));
			list.Add(new AttrItem("Pulse Clear Times", 2072, 2551));
			list.Add(new AttrItem("ExpTime Valid Percent", 2066, 2537));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadMercu1717VAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("PGA", 2030, 0));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("Zoom Mode", 2014, 0));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 0));
			list.Add(new AttrItem("Dynamic Exp Window Time", 2027, 0));
			list.Add(new AttrItem("Exp Mode", 2050, 0));
			list.Add(new AttrItem("Dynamic Flag", 2020, 0));
			list.Add(new AttrItem("Staic Exp Window Time", 2025, 2512));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("Trigger Mode", 2019, 2506));
			list.Add(new AttrItem("AEC Pre Main TimeGap", 2051, 2529));
			list.Add(new AttrItem("Pulse Clear Times", 2072, 2551));
			list.Add(new AttrItem("ExpTime Valid Percent", 2066, 2537));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadPluto0406Attr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("ROIColStartPos", 2073, 0));
			list.Add(new AttrItem("ROIRowStartPos", 2075, 0));
			list.Add(new AttrItem("ROIColEndPos", 2074, 0));
			list.Add(new AttrItem("ROIRowEndPos", 2076, 0));
			list.Add(new AttrItem("FullWell", 2077, 0));
			list.Add(new AttrItem("Sequence Interval Time", 2022, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 0));
			list.Add(new AttrItem("Exp Window Time", 2028, 0));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("ExpTime Valid Percent", 2066, 2537));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadMarsXAttr(int productNo)
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Mcu Version", 2004, 0));
			list.Add(new AttrItem("Arm Version", 2005, 0));
			list.Add(new AttrItem("Kernel Version", 2006, 0));
			switch (productNo)
			{
			case 51:
			case 52:
				list.Add(new AttrItem("Inner SubFlow", 2078, 2552));
				break;
			}
			list.Add(new AttrItem("Prep CapMode", 2032, 2514));
			list.Add(new AttrItem("Self CapEnable", 2033, 2515));
			list.Add(new AttrItem("Self Cap Span Time", 2052, 2530));
			list.Add(new AttrItem("Trigger Mode", 2019, 2506));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 2505));
			list.Add(new AttrItem("Set Delay Time", 2023, 2510));
			list.Add(new AttrItem("Exp Window Time", 2025, 2512));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("IntegrateTime", 2013, 2540));
			list.Add(new AttrItem("Image Pkt Gap Time", 2054, 2542));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 2518));
			list.Add(new AttrItem("Src MAC", 2037, 2519));
			list.Add(new AttrItem("Self Clear Enable", 2016, 2503));
			list.Add(new AttrItem("Self Clear Span Time", 2017, 2504));
			list.Add(new AttrItem("Hvg Prep On", 2055, 2532));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Tube Ready Time", 2021, 2508));
			list.Add(new AttrItem("Out Mode Cap Trigger", 2069, 2541));
			return list;
		}

		private static List<AttrItem> LoadMommoAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 2518));
			list.Add(new AttrItem("Src MAC", 2037, 2519));
			list.Add(new AttrItem("Dest Port", 2038, 2520));
			list.Add(new AttrItem("Dest IP", 2039, 2521));
			list.Add(new AttrItem("Dest MAC", 2040, 2522));
			list.Add(new AttrItem("Trigger Mode", 2019, 2506));
			list.Add(new AttrItem("Exp Mode", 2050, 2528));
			list.Add(new AttrItem("Preview Image Mode", 2044, 2523));
			list.Add(new AttrItem("Dyna Offset Mode", 2053, 2531));
			list.Add(new AttrItem("HW Offset Mode", 2045, 0));
			list.Add(new AttrItem("Level Signal", 2015, 2502));
			list.Add(new AttrItem("Self Clear Enable", 2016, 2503));
			list.Add(new AttrItem("Self Clear Span Time", 2017, 2504));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 2505));
			list.Add(new AttrItem("Set Delay Time", 2023, 2510));
			list.Add(new AttrItem("Exp Window Time", 2025, 2512));
			list.Add(new AttrItem("PGA", 2030, 2513));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("AEC Pre Main TimeGap", 2051, 2529));
			list.Add(new AttrItem("Dyna Offset Gap Time", 2052, 2530));
			list.Add(new AttrItem("Image Pkt Gap Time", 2054, 2542));
			list.Add(new AttrItem("Hvg Prep On", 2055, 2532));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			return list;
		}

		private static List<AttrItem> LoadVenuMFAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Prep CapMode", 2032, 2514));
			list.Add(new AttrItem("Self CapEnable", 2033, 2515));
			list.Add(new AttrItem("Self Cap Span Time", 2052, 2530));
			list.Add(new AttrItem("Trigger Mode", 2019, 2506));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 2505));
			list.Add(new AttrItem("Set Delay Time", 2023, 2510));
			list.Add(new AttrItem("Exp Window Time", 2025, 2512));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Integrate Time", 2013, 2540));
			list.Add(new AttrItem("Self Clear Enable", 2016, 2503));
			list.Add(new AttrItem("Self Clear Span Time", 2017, 2504));
			list.Add(new AttrItem("Hvg Prep On", 2055, 2532));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Tube Ready Time", 2021, 2508));
			list.Add(new AttrItem("Image Pkt Gap Time", 2054, 2542));
			list.Add(new AttrItem("Out Mode Cap Trigger", 2069, 2541));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 2518));
			list.Add(new AttrItem("Src MAC", 2037, 2519));
			list.Add(new AttrItem("Dest Port", 2038, 0));
			list.Add(new AttrItem("Dest IP", 2039, 0));
			return list;
		}

		private static List<AttrItem> LoadVenuMNAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 2505));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 2518));
			list.Add(new AttrItem("Src MAC", 2037, 2519));
			list.Add(new AttrItem("Dest Port", 2038, 0));
			list.Add(new AttrItem("Dest IP", 2039, 0));
			return list;
		}

		private static List<AttrItem> Load1717XUAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Mcu Version", 2004, 0));
			list.Add(new AttrItem("Arm Version", 2005, 0));
			list.Add(new AttrItem("Kernel Version", 2006, 0));
			list.Add(new AttrItem("Master Build Time", 2008, 0));
			list.Add(new AttrItem("Slave Build Time", 2009, 0));
			list.Add(new AttrItem("MCU Build Time", 2010, 0));
			list.Add(new AttrItem("Prep CapMode", 2032, 2514));
			list.Add(new AttrItem("Self CapEnable", 2033, 2515));
			list.Add(new AttrItem("Self Cap Span Time", 2052, 2530));
			list.Add(new AttrItem("Trigger Mode", 2019, 2506));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 2505));
			list.Add(new AttrItem("Set Delay Time", 2023, 2510));
			list.Add(new AttrItem("Exp Window Time", 2025, 2512));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Integrate Time", 2013, 2540));
			list.Add(new AttrItem("Self Clear Enable", 2016, 2503));
			list.Add(new AttrItem("Self Clear Span Time", 2017, 2504));
			list.Add(new AttrItem("Hvg Prep On", 2055, 2532));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Tube Ready Time", 2021, 2508));
			list.Add(new AttrItem("Image Pkt Gap Time", 2054, 2542));
			list.Add(new AttrItem("Out Mode Cap Trigger", 2069, 2541));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 2518));
			list.Add(new AttrItem("Src MAC", 2037, 2519));
			list.Add(new AttrItem("Dest Port", 2038, 0));
			list.Add(new AttrItem("Dest IP", 2039, 0));
			list.Add(new AttrItem("Sync-Box IP", 2041, 2543));
			return list;
		}

		private static List<AttrItem> LoadOtherAttr()
		{
			List<AttrItem> list = new List<AttrItem>();
			list.Add(new AttrItem("Product No", 2001, 0));
			list.Add(new AttrItem("Sub Product No", 2061, 0));
			list.Add(new AttrItem("Serial No", 2062, 0));
			list.Add(new AttrItem("Main Version", 2002, 0));
			list.Add(new AttrItem("Read Version", 2003, 0));
			list.Add(new AttrItem("Master Build Time", 2008, 0));
			list.Add(new AttrItem("Slave Build Time", 2009, 0));
			list.Add(new AttrItem("MCU Build Time", 2010, 0));
			list.Add(new AttrItem("CBX Build Time", 2060, 0));
			list.Add(new AttrItem("Arm Version", 2005, 0));
			list.Add(new AttrItem("Kernel Version", 2006, 0));
			list.Add(new AttrItem("Src Port", 2035, 0));
			list.Add(new AttrItem("Src IP", 2036, 0));
			list.Add(new AttrItem("Src MAC", 2037, 0));
			list.Add(new AttrItem("Dest Port", 2038, 0));
			list.Add(new AttrItem("Dest IP", 2039, 0));
			list.Add(new AttrItem("Dest MAC", 2040, 0));
			list.Add(new AttrItem("Trigger Mode", 2019, 2506));
			list.Add(new AttrItem("Fluro Sync", 2034, 2516));
			list.Add(new AttrItem("Binning Mode", 2047, 0));
			list.Add(new AttrItem("Zoom Mode", 2014, 0));
			list.Add(new AttrItem("Integrate Time", 2013, 2540));
			list.Add(new AttrItem("Level Signal", 2015, 2502));
			list.Add(new AttrItem("Self Clear", 2016, 2503));
			list.Add(new AttrItem("Self Clear Span Time", 2017, 2504));
			list.Add(new AttrItem("Sequence Interval Time", 2018, 2505));
			list.Add(new AttrItem("Tube Ready Time", 2021, 2508));
			list.Add(new AttrItem("Set Delay Time", 2023, 2510));
			list.Add(new AttrItem("Exp Window Time", 2025, 2512));
			list.Add(new AttrItem("PGA", 2030, 2513));
			list.Add(new AttrItem("Pre Mode", 2044, 2523));
			list.Add(new AttrItem("Acquire Delay Time", 2046, 2524));
			list.Add(new AttrItem("Exp Mode", 2050, 2528));
			list.Add(new AttrItem("AEC Main Time", 2051, 2529));
			list.Add(new AttrItem("Dyna Offset Gap Time", 2052, 2530));
			list.Add(new AttrItem("Dyna Offset Mode", 2053, 2531));
			list.Add(new AttrItem("Hvg Prep On", 2055, 2532));
			list.Add(new AttrItem("Hvg XRay Enable", 2056, 2533));
			list.Add(new AttrItem("Hvg XRay On", 2057, 2534));
			list.Add(new AttrItem("Hvg XRay SyncOut", 2058, 2535));
			list.Add(new AttrItem("Hvg XRay SyncIn", 2059, 2536));
			list.Add(new AttrItem("Image Channel", 2063, 0));
			list.Add(new AttrItem("Image Channel Protocol", 2064, 0));
			return list;
		}
	}
}
