using System.ComponentModel;

namespace iDetector
{
	public class SubsetSettingsList : INotifyPropertyChanged
	{
		public string SubsetName
		{
			get;
			set;
		}

		public string Activity
		{
			get;
			set;
		}

		public string OffsetValidity
		{
			get;
			set;
		}

		public string GainValidity
		{
			get;
			set;
		}

		public string DefectValidity
		{
			get;
			set;
		}

		public string LagValidity
		{
			get;
			set;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyView(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
	}
}
