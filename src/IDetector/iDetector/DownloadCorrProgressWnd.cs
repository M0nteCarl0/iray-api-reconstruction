namespace iDetector
{
	public class DownloadCorrProgressWnd : DownloadFileProgressBar
	{
		private Detector Dectector;

		public DownloadCorrProgressWnd(Detector detector, string title)
			: base(title, GetTimeOut(detector))
		{
			Dectector = detector;
		}

		private static int GetTimeOut(Detector det)
		{
			int num = 0;
			switch (det.Prop_Attr_UROM_ProductNo)
			{
			case 48:
			case 49:
			case 50:
				num = 500;
				break;
			default:
				num = 200;
				break;
			}
			return (int)IniParser.GetPrivateProfileInt("System", "DownloadFileTimeout", num, det.Prop_Attr_WorkDir + "iDetectorConfig.ini");
		}

		protected override void UpdateProgress()
		{
			ProgressBar.Value = Dectector.Prop_Attr_ImageTransProgress;
			PercentValue.Content = Dectector.Prop_Attr_ImageTransProgress.ToString();
		}
	}
}
