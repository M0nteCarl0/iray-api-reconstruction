using System.Collections.ObjectModel;

namespace iDetector
{
	public class WifiClient
	{
		public static void LoadParam(Collection<WifiClientParam> clientList, Detector det)
		{
			if (det != null && clientList != null)
			{
				int num = 4012;
				int num2 = 4013;
				for (int i = 0; i < det.Prop_Attr_Wifi_Client_ListNum; i++)
				{
					det.GetAttrStrInfo(num);
					clientList.Add(new WifiClientParam
					{
						SSID = det.GetAttrStrInfo(num),
						Key = det.GetAttrStrInfo(num2)
					});
					num += 2;
					num2 += 2;
				}
			}
		}
	}
}
