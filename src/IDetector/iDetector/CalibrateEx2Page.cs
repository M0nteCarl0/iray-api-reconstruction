using CorrectExUI;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class CalibrateEx2Page : UserControl, IDetSubPage, IComponentConnector
	{
		private Detector instance;

		private DetecInfo _detecInfo;

		public CorrectionExPreparePage _preparePage;

		private bool _contentLoaded;

		public virtual Detector Instance
		{
			get
			{
				return instance;
			}
			set
			{
				instance = value;
				InitInstance();
			}
		}

		public bool bHandled
		{
			get;
			set;
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		public void OnClose()
		{
		}

		public bool Exit()
		{
			return true;
		}

		public bool Enter()
		{
			return true;
		}

		public CalibrateEx2Page()
		{
			InitializeComponent();
		}

		private void InitInstance()
		{
			_detecInfo = new DetecInfo(Instance, new Shell(Application.Current.MainWindow));
			_preparePage = new CorrectionExPreparePage(instance);
		}

		public void Initialize()
		{
			base.Content = _preparePage;
			_preparePage.Initialize();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/calibrateex2page.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			_contentLoaded = true;
		}
	}
}
