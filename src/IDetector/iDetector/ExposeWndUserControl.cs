using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class ExposeWndUserControl : UserControl, IComponentConnector
	{
		private UITimer mTimer;

		private int mTotalTime;

		private int mLeftTime;

		public Action CancelHandler;

		internal ProgressBar LeftTimeBar;

		internal Label LeftTime;

		internal Button xCancelBtn;

		private bool _contentLoaded;

		public ExposeWndUserControl()
		{
			InitializeComponent();
			mTimer = new UITimer(1000, OnTimer);
			base.Visibility = Visibility.Hidden;
		}

		public void Show(int totalSeconds, bool canCancel)
		{
			if (totalSeconds == 0)
			{
				mTotalTime = 1;
				mLeftTime = 1;
				LeftTimeBar.Value = 100.0;
				LeftTime.Content = "...";
			}
			else
			{
				mTotalTime = totalSeconds;
				mLeftTime = totalSeconds;
				LeftTimeBar.Value = mLeftTime * 100 / mTotalTime;
				LeftTime.Content = mLeftTime.ToString();
				mTimer.Start();
			}
			base.Visibility = Visibility.Visible;
			if (canCancel)
			{
				xCancelBtn.Visibility = Visibility.Visible;
			}
			else
			{
				xCancelBtn.Visibility = Visibility.Collapsed;
			}
		}

		public void Close()
		{
			mTimer.Stop();
			base.Visibility = Visibility.Hidden;
		}

		private void OnTimer()
		{
			mLeftTime--;
			LeftTimeBar.Value = mLeftTime * 100 / mTotalTime;
			LeftTime.Content = mLeftTime.ToString();
			if (mLeftTime <= 0)
			{
				Close();
			}
		}

		private void OnCancel(object sender, RoutedEventArgs e)
		{
			if (CancelHandler != null)
			{
				CancelHandler();
			}
			Close();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/exposewndusercontrol.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				LeftTimeBar = (ProgressBar)target;
				break;
			case 2:
				LeftTime = (Label)target;
				break;
			case 3:
				xCancelBtn = (Button)target;
				xCancelBtn.Click += OnCancel;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
