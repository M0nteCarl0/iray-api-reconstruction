namespace iDetector
{
	public class OfflineCorrectionParams
	{
		public int nFpdProdNo;

		public int nFpdTriggerMode;

		public int nFpdPrepCapMode;

		public int nFpdSeqInterval;

		public int nFpdExpMode;

		public int nImgWidth;

		public int nImgHeight;

		public string pszFpdWorkDir;

		public string pszFpdCaliSubset;

		public bool bDoPreOffset;

		public bool bDoGain;

		public bool bDoDefect;

		public bool bPreOffsetDone;

		public string FullWorkDirPath;

		public float OffsetTemplateFreq;

		public void Reset()
		{
			bDoPreOffset = false;
			bDoGain = false;
			bDoDefect = false;
		}
	}
}
