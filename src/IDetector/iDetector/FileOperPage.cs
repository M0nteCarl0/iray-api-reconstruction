using IMG.Process;
using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Markup;

namespace iDetector
{
	public class FileOperPage : UserControl, IComponentConnector
	{
		private const Enm_ImageTag GainTagId = (Enm_ImageTag)294924;

		private const Enm_ImageTag DefectTagId = (Enm_ImageTag)294925;

		private DefectMapSetWnd defectMapWnd;

		private PostProcessWnd postwnd;

		private OfflineCorrectionParams CorrectionInfo = new OfflineCorrectionParams();

		private string mFilePath = "iDetector";

		private FileOperation fileEx;

		private string mSrcFilePath;

		private int mWidth = 1024;

		private int mHeight = 1024;

		private GateValueInfo gateInfo;

		private Dictionary<Enm_ImageTag, int> TiffAttrMap;

		private bool ClearTiffValue = true;

		internal StackPanel xSaveAs;

		internal Button xSaveAsEx;

		internal ToggleButton xPostSetBtn;

		internal Button xGateStatisticsBtn;

		internal Button xGicAfeBtn;

		internal ScrollViewer _TiffAttrSV;

		internal StackPanel _tifAttrPanel;

		internal ListView TiffAttr_unused;

		internal CtrImagePresent ImageViewCtrl;

		private bool _contentLoaded;

		public event UpdateTitleDel UpdateTitleHandle;

		public FileOperPage()
		{
			InitializeComponent();
			ImageViewCtrl.ShowImageListView(false);
			ImageViewCtrl.CollapsedFpsAndFramesGrid();
			base.IsVisibleChanged += VisibleChanged;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (!(bool)e.NewValue)
			{
				HidePage();
			}
			else
			{
				EnterPage();
			}
		}

		private void GetRawWidthAndHeight(long fileSize, ref int width, ref int height)
		{
			width = (int)IniParser.GetPrivateProfileInt("ImageSize", "width", 1024, "./rawimage.ini");
			height = (int)IniParser.GetPrivateProfileInt("ImageSize", "height", 1024, "./rawimage.ini");
		}

		private void RecordRawImageSize(int width, int height)
		{
			IniParser.WritePrivateProfileString("ImageSize", "width", width.ToString(), "./rawimage.ini");
			IniParser.WritePrivateProfileString("ImageSize", "height", height.ToString(), "./rawimage.ini");
		}

		private void LoadFile(object sender, RoutedEventArgs e)
		{
			if (postwnd != null)
			{
				MessageBox.Show("Please close the PostProcessWnd window firstly!");
				return;
			}
			int nAuthority = 0;
			SdkInterface.GetAuthority(ref nAuthority);
			OpenFileDialog openFileDialog = new OpenFileDialog();
			if ((0x2000 & nAuthority) != 0)
			{
				openFileDialog.Filter = "Files(*.dcm;*.raw;*.tif;*.dft;*.cin;)|*.dcm;*.raw;*.tif;*.dft;*.cin;)";
			}
			else
			{
				openFileDialog.Filter = "Files(*.dcm; *.raw; *.tif; *.dft;)|*.dcm; *.raw; *.tif; *.dft;)";
			}
			if (openFileDialog.ShowDialog() == false)
			{
				return;
			}
			string fileName = openFileDialog.FileName;
			byte[] array = null;
			string text = openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf(".") + 1, openFileDialog.FileName.Length - openFileDialog.FileName.LastIndexOf(".") - 1);
			if (text.ToLower().Equals("dft"))
			{
				if (fileEx == null)
				{
					fileEx = new FileOperation();
				}
				else
				{
					fileEx.CloseDefectFile();
				}
				ushort nWidth = 0;
				ushort nHeight = 0;
				array = fileEx.LoadDefectFile(fileName, ref nWidth, ref nHeight);
				if (array == null)
				{
					MessageBox.Show("Open defect file failed!");
					return;
				}
				mSrcFilePath = fileName;
				CollapseTiffPanel(false);
				mWidth = nWidth;
				mHeight = nHeight;
				if (defectMapWnd == null)
				{
					defectMapWnd = new DefectMapSetWnd(fileName);
					defectMapWnd.SaveDefectAction += SaveDefectData;
					defectMapWnd.SetSource(ImageViewCtrl.ImgView);
					defectMapWnd.PaintView += PaintImageView;
					defectMapWnd.Closing += OnClosingDefectMapSetting;
				}
				else
				{
					QuerySaveDefectMapData();
				}
				defectMapWnd.InitDefectData(ref array, mWidth, mHeight, fileName);
				defectMapWnd.Show();
				ImageViewCtrl.ClosePlayer();
				mFilePath = "iDetector: [" + openFileDialog.FileName + "]";
				UpdateTitle();
				ImageViewCtrl.CollapsedOperationImageButton(true);
				xSaveAs.Visibility = Visibility.Collapsed;
				return;
			}
			ImageViewCtrl.StartTimer();
			Visibility visibility = Visibility.Visible;
			if (text.ToLower().Equals("raw") || text.ToLower().Equals("cin"))
			{
				int width = 1024;
				int height = 1024;
				long fileSize = Utility.GetFileSize(openFileDialog.FileName);
				if (0 == fileSize)
				{
					MessageBox.Show("Cannot open the file or file is incomplete!");
					return;
				}
				GetRawWidthAndHeight(fileSize, ref width, ref height);
				RawFileSizeSetWnd rawFileSizeSetWnd = new RawFileSizeSetWnd(width, height);
				rawFileSizeSetWnd.ShowDialog();
				if (!rawFileSizeSetWnd.ResultOK)
				{
					return;
				}
				mWidth = rawFileSizeSetWnd.ImgWidth;
				mHeight = rawFileSizeSetWnd.ImgHeight;
				RecordRawImageSize(mWidth, mHeight);
			}
			else if (text.ToLower().Equals("dcm"))
			{
				visibility = Visibility.Visible;
			}
			else
			{
				if (!text.ToLower().Equals("tif"))
				{
					MessageBox.Show("Not support this file!");
					return;
				}
				InitTiffAttrPanel();
			}
			if (ImageViewCtrl.StartPlayCine(openFileDialog.FileName, mWidth, mHeight))
			{
				mFilePath = "iDetector: [" + openFileDialog.FileName + "]";
				UpdateTitle();
				if (text.ToLower().Equals("tif"))
				{
					CollapseTiffPanel(true);
				}
				else
				{
					CollapseTiffPanel(false);
				}
				if (defectMapWnd != null)
				{
					defectMapWnd.Close();
					defectMapWnd = null;
				}
				mSrcFilePath = fileName;
				ImageViewCtrl.CollapsedOperationImageButton(false);
				xSaveAs.Visibility = visibility;
				SetSaveRange(mSrcFilePath, mWidth, mHeight);
				if ((0x2000 & nAuthority) != 0)
				{
					xGateStatisticsBtn.Visibility = Visibility.Visible;
					xGicAfeBtn.Visibility = Visibility.Visible;
				}
				else
				{
					xGateStatisticsBtn.Visibility = Visibility.Collapsed;
					xGicAfeBtn.Visibility = Visibility.Collapsed;
				}
				CorrectionInfo.Reset();
			}
			else
			{
				ResumeTiffPanel();
				MessageBox.Show("Cannot open the file or file is incomplete!");
			}
		}

		private void SaveFile(object sender, RoutedEventArgs e)
		{
			if (mSrcFilePath == null)
			{
				return;
			}
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			string text = mSrcFilePath.Substring(mSrcFilePath.LastIndexOf(".") + 1, mSrcFilePath.Length - mSrcFilePath.LastIndexOf(".") - 1);
			FileType fileType = FileType.RAW_FILE;
			if (text.ToLower().Equals("tif"))
			{
				saveFileDialog.DefaultExt = ".raw";
				saveFileDialog.Filter = "raw file(*.raw)|*.raw";
			}
			else if (text.ToLower().Equals("raw"))
			{
				fileType = FileType.TIFF_FILE;
				saveFileDialog.DefaultExt = ".tif";
				saveFileDialog.Filter = "tiff file(*.tif)|*.tif";
			}
			else if (text.ToLower().Equals("dcm"))
			{
				saveFileDialog.DefaultExt = ".raw";
				saveFileDialog.Filter = "raw files(*.raw)|*.raw|tif files(*.tif)|*.tif";
			}
			else
			{
				if (!text.ToLower().Equals("cin"))
				{
					return;
				}
				saveFileDialog.DefaultExt = ".raw";
				saveFileDialog.Filter = "raw file(*.raw)|*.raw";
			}
			if (saveFileDialog.ShowDialog() == true)
			{
				string text2 = saveFileDialog.FileName.Substring(saveFileDialog.FileName.LastIndexOf(".") + 1, saveFileDialog.FileName.Length - saveFileDialog.FileName.LastIndexOf(".") - 1);
				if (text2.ToLower().Equals("tif"))
				{
					fileType = FileType.TIFF_FILE;
				}
				if (FileType.TIFF_FILE == fileType)
				{
					ImageViewCtrl.SaveToTiffFile(mSrcFilePath, saveFileDialog.FileName);
				}
				else
				{
					ImageViewCtrl.SaveToRawFile(mSrcFilePath, saveFileDialog.FileName);
				}
			}
		}

		private void GateStatistics(object sender, RoutedEventArgs e)
		{
			int imgWidth = ImageViewCtrl.ImgWidth;
			int imgHeight = ImageViewCtrl.ImgHeight;
			if ((imgWidth == 3072 && imgHeight == 3072) || (imgWidth == 2304 && imgHeight == 2800))
			{
				int num = imgWidth * imgHeight;
				gateInfo.nWidth = imgWidth;
				gateInfo.nHeight = imgHeight;
				gateInfo.ImgData = new ushort[num];
				ushort[] oriGray16ImgBuffer = ImageViewCtrl.ImgView.OriGray16ImgBuffer;
				Buffer.BlockCopy(oriGray16ImgBuffer, 0, gateInfo.ImgData, 0, num * 2);
				GateStatisticsPage gateStatisticsPage = new GateStatisticsPage(gateInfo);
				gateStatisticsPage.ShowDialog();
			}
			else
			{
				MessageBox.Show("Image size error!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
		}

		private void OnGicAfe(object sender, RoutedEventArgs e)
		{
			int imgWidth = ImageViewCtrl.ImgWidth;
			int imgHeight = ImageViewCtrl.ImgHeight;
			if (imgWidth > 0 && imgHeight > 0)
			{
				int num = imgWidth * imgHeight;
				gateInfo.nWidth = imgWidth;
				gateInfo.nHeight = imgHeight;
				gateInfo.ImgData = new ushort[num];
				ushort[] oriGray16ImgBuffer = ImageViewCtrl.ImgView.OriGray16ImgBuffer;
				Buffer.BlockCopy(oriGray16ImgBuffer, 0, gateInfo.ImgData, 0, num * 2);
				GateAfePage gateAfePage = new GateAfePage(gateInfo);
				gateAfePage.ShowDialog();
			}
			else
			{
				MessageBox.Show("Image size error!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
		}

		private void OnPostProcessSet(object sender, RoutedEventArgs e)
		{
			ToggleButton button = sender as ToggleButton;
			if (button != null)
			{
				if (postwnd == null)
				{
					postwnd = new PostProcessWnd();
					SetSaveRange(mSrcFilePath, ImageViewCtrl.ImgWidth, ImageViewCtrl.ImgHeight);
					postwnd.Closed += delegate
					{
						postwnd.SaveDel -= SavePostedImage;
						postwnd.SetPostProcessParaDel -= SetPostParams;
						postwnd.SetCorrectOption -= SetOfflineCorrectionParams;
						button.IsChecked = false;
						postwnd = null;
					};
					postwnd.SetCorrectionParams(ref CorrectionInfo);
					postwnd.SetImageInfo(ImageViewCtrl.ImgWidth, ImageViewCtrl.ImgHeight);
					postwnd.SaveDel += SavePostedImage;
					postwnd.SetPostProcessParaDel += SetPostParams;
					postwnd.SetCorrectOption += SetOfflineCorrectionParams;
				}
				if (button.IsChecked == true)
				{
					postwnd.Show();
				}
				else
				{
					postwnd.Hide();
				}
			}
		}

		private void SetSaveRange(string path, int width, int height)
		{
			int framesNumber = FileOperation.GetFramesNumber(path, width, height);
			if (framesNumber < 2)
			{
				xPostSetBtn.Visibility = Visibility.Collapsed;
				if (postwnd != null)
				{
					postwnd.Close();
					SetPostParams(new ProcessedCoeff
					{
						enableReduceNoise = false,
						enableSuperostion = false
					});
				}
				PostProcessWnd.ResetPostCoeff();
			}
			else
			{
				xPostSetBtn.Visibility = Visibility.Visible;
			}
			if (postwnd != null)
			{
				postwnd.SetFramesNumber(framesNumber);
			}
		}

		private void SavePostedImage(string path, int startIndex, int endIndex)
		{
			string text = path.Substring(path.LastIndexOf(".") + 1, path.Length - path.LastIndexOf(".") - 1);
			if (text.ToLower().Equals("tif"))
			{
				ImageViewCtrl.SaveToTiffFile(PostProcessWnd.storeFilePath, path, new StoreFileParams
				{
					filePath = path,
					startIndex = startIndex,
					endIndex = endIndex
				});
			}
			else if (text.ToLower().Equals("raw"))
			{
				ImageViewCtrl.SaveToRawFile(PostProcessWnd.storeFilePath, path, new StoreFileParams
				{
					filePath = path,
					startIndex = startIndex,
					endIndex = endIndex
				});
			}
		}

		private void SetPostParams(ProcessedCoeff coeff)
		{
			if (!ImageViewCtrl.SetImageProcessorCoeff(coeff))
			{
				MessageBox.Show(Application.Current.MainWindow, "Set post-params Failed!");
			}
		}

		private void PaintImageView()
		{
			if (defectMapWnd != null)
			{
				ImageViewCtrl.DisplayDefectFile(defectMapWnd.DefectData, (ushort)defectMapWnd.nWidth, (ushort)defectMapWnd.nHeight);
			}
		}

		private void SaveDefectData(byte[] data)
		{
			if (fileEx != null)
			{
				fileEx.SaveDefectFile(data);
				ReloadDefectData();
			}
		}

		private void ReloadDefectData()
		{
			if (defectMapWnd != null)
			{
				fileEx.ReLoadDefectFile(mSrcFilePath, defectMapWnd.DefectData);
				PaintImageView();
			}
		}

		private void CloseDefectFile()
		{
			if (fileEx != null)
			{
				fileEx.CloseDefectFile();
			}
		}

		protected void OnClosingDefectMapSetting(object sender, CancelEventArgs e)
		{
			QuerySaveDefectMapData();
			defectMapWnd = null;
		}

		private void QuerySaveDefectMapData()
		{
			if (defectMapWnd.hasDirtyData && MessageBoxResult.Yes == MessageBox.Show(Application.Current.MainWindow, "Save to defect file?", "save", MessageBoxButton.YesNo))
			{
				defectMapWnd.SaveDefectMap();
			}
		}

		public void UpdateTitle()
		{
			if (this.UpdateTitleHandle != null)
			{
				this.UpdateTitleHandle(mFilePath);
			}
		}

		private int SetOfflineCorrectionParams(OfflineCorrectionInfo correctoption)
		{
			return ImageViewCtrl.SetOfflineCorrectionParams(correctoption);
		}

		public void ClosePage()
		{
			ImageViewCtrl.OnClose();
			if (defectMapWnd != null)
			{
				defectMapWnd.Close();
				defectMapWnd = null;
			}
			CloseDefectFile();
			Utility.DeleteFile(PostProcessWnd.storeFilePath);
		}

		public void HidePage()
		{
			if (defectMapWnd != null)
			{
				defectMapWnd.Hide();
			}
			ImageViewCtrl.HidePlayBar();
			if (postwnd != null)
			{
				postwnd.Hide();
			}
		}

		public void EnterPage()
		{
			if (defectMapWnd != null)
			{
				defectMapWnd.Show();
			}
			if (postwnd != null)
			{
				postwnd.Show();
			}
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			Image image = e.OriginalSource as Image;
			if (image != null && defectMapWnd != null)
			{
				defectMapWnd.UpdatePosition();
			}
			base.OnPreviewMouseLeftButtonDown(e);
		}

		public void OnLocationChanged()
		{
			ImageViewCtrl.OnLocationChanged();
		}

		private void CollapseTiffPanel(bool isVisible)
		{
			_TiffAttrSV.Visibility = ((!isVisible) ? Visibility.Collapsed : Visibility.Visible);
		}

		private void ResumeTiffPanel()
		{
			if (_TiffAttrSV.Visibility != 0)
			{
				_TiffAttrSV.Visibility = Visibility.Collapsed;
			}
		}

		public void InitTiffAttrPanel()
		{
			ClearTiffValue = true;
			if (_tifAttrPanel.Children.Count <= 0)
			{
				_tifAttrPanel.Children.Clear();
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_Maker, "Maker"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_Model, "Model"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_FrameNo, "FrameNo"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_BinningMode, "Binning"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_CorrectFlag, "Offset"));
				_tifAttrPanel.Children.Add(new TifList((Enm_ImageTag)294924, "Gain"));
				_tifAttrPanel.Children.Add(new TifList((Enm_ImageTag)294925, "Defect"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_RealFrequency, "RealFrameRate"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_XRayWindow, "XRayWindow"));
				_tifAttrPanel.Children.Add(new TifList(Enm_ImageTag.Enm_ImageTag_Temperature, "Temperature"));
				ImageViewCtrl.UpdateTiffAttr += UpdateTiffAttrHandle;
				TiffAttrMap = new Dictionary<Enm_ImageTag, int>();
				for (int i = 0; i < _tifAttrPanel.Children.Count; i++)
				{
					TifList tifList = _tifAttrPanel.Children[i] as TifList;
					TiffAttrMap.Add(tifList.Id, i);
				}
			}
		}

		private void UpdateTiffAttrHandle(ref IRayImageData image)
		{
			if (image == null || image.Params == null)
			{
				return;
			}
			if (ClearTiffValue)
			{
				ClearTiffValue = false;
				foreach (TifList child in _tifAttrPanel.Children)
				{
					child.Value = "";
				}
			}
			IRayVariantMapItem[] @params = image.Params;
			for (int i = 0; i < @params.Length; i++)
			{
				IRayVariantMapItem rayVariantMapItem = @params[i];
				Enm_ImageTag nMapKey = (Enm_ImageTag)rayVariantMapItem.nMapKey;
				if (!TiffAttrMap.ContainsKey(nMapKey))
				{
					continue;
				}
				TifList tifList2 = _tifAttrPanel.Children[TiffAttrMap[nMapKey]] as TifList;
				switch (nMapKey)
				{
				case Enm_ImageTag.Enm_ImageTag_Temperature:
					tifList2.Value = rayVariantMapItem.varMapVal.val.fVal.ToString("0.0");
					break;
				case Enm_ImageTag.Enm_ImageTag_FrameNo:
				case Enm_ImageTag.Enm_ImageTag_RealFrequency:
				case Enm_ImageTag.Enm_ImageTag_XRayWindow:
				case Enm_ImageTag.Enm_ImageTag_FrameRate:
					tifList2.Value = rayVariantMapItem.varMapVal.val.nVal.ToString();
					break;
				case Enm_ImageTag.Enm_ImageTag_Maker:
				case Enm_ImageTag.Enm_ImageTag_Model:
					tifList2.Value = rayVariantMapItem.varMapVal.val.strVal;
					break;
				case Enm_ImageTag.Enm_ImageTag_BinningMode:
					tifList2.Value = string.Format("{0}X{0}", rayVariantMapItem.varMapVal.val.nVal + 1);
					break;
				case Enm_ImageTag.Enm_ImageTag_CorrectFlag:
					if ((rayVariantMapItem.varMapVal.val.nVal & 1) != 0)
					{
						tifList2.Value = "HWPre";
					}
					else if ((rayVariantMapItem.varMapVal.val.nVal & 2) != 0)
					{
						tifList2.Value = "HWPost";
					}
					else if ((rayVariantMapItem.varMapVal.val.nVal & 0x10000) != 0)
					{
						tifList2.Value = "SWPre";
					}
					else if ((rayVariantMapItem.varMapVal.val.nVal & 0x20000) != 0)
					{
						tifList2.Value = "SWPost";
					}
					else
					{
						tifList2.Value = "Off";
					}
					tifList2 = (_tifAttrPanel.Children[TiffAttrMap[(Enm_ImageTag)294924]] as TifList);
					if ((rayVariantMapItem.varMapVal.val.nVal & 0x40000) != 0)
					{
						tifList2.Value = "SW";
					}
					else if ((rayVariantMapItem.varMapVal.val.nVal & 4) != 0)
					{
						tifList2.Value = "HW";
					}
					else
					{
						tifList2.Value = "Off";
					}
					tifList2 = (_tifAttrPanel.Children[TiffAttrMap[(Enm_ImageTag)294925]] as TifList);
					if ((rayVariantMapItem.varMapVal.val.nVal & 0x100000) != 0)
					{
						tifList2.Value = "SW";
					}
					else if ((rayVariantMapItem.varMapVal.val.nVal & 0x10) != 0)
					{
						tifList2.Value = "HW";
					}
					else
					{
						tifList2.Value = "Off";
					}
					break;
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/fileoperpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((Button)target).Click += LoadFile;
				break;
			case 2:
				xSaveAs = (StackPanel)target;
				break;
			case 3:
				xSaveAsEx = (Button)target;
				xSaveAsEx.Click += SaveFile;
				break;
			case 4:
				xPostSetBtn = (ToggleButton)target;
				xPostSetBtn.Click += OnPostProcessSet;
				break;
			case 5:
				xGateStatisticsBtn = (Button)target;
				xGateStatisticsBtn.Click += GateStatistics;
				break;
			case 6:
				xGicAfeBtn = (Button)target;
				xGicAfeBtn.Click += OnGicAfe;
				break;
			case 7:
				_TiffAttrSV = (ScrollViewer)target;
				break;
			case 8:
				_tifAttrPanel = (StackPanel)target;
				break;
			case 9:
				TiffAttr_unused = (ListView)target;
				break;
			case 10:
				ImageViewCtrl = (CtrImagePresent)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
