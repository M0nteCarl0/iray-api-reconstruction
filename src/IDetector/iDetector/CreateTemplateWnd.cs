using CorrectUI;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Navigation;

namespace iDetector
{
	public class CreateTemplateWnd : Window, IDisposable, IComponentConnector
	{
		private const int TaskTimeOutSeconds = 30;

		private Detector Instance;

		private Page CurPage;

		private GenerateEventMsg EventMsg;

		private DlgTaskTimeOut CloseCreateWnd;

		private int backupCorrection;

		private UITimer ConnectStateTimer;

		private PresetOffsetOption presetOffsetOption;

		internal Frame xFrame;

		internal ComboBox xComboMsg;

		private bool _contentLoaded;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public CreateTemplateWnd(Detector instance, PresetOffsetOption offsetOption)
		{
			InitializeComponent();
			presetOffsetOption = offsetOption;
			Instance = instance;
			xFrame.NavigationService.LoadCompleted += OnLoadCompleted;
			this.SdkEventHandler = ProcessEvent;
			Instance.SdkCallbackEvent += OnSdkCallback;
			EventMsg = new GenerateEventMsg(xComboMsg);
			backupCorrection = Instance.Prop_Attr_CurrentCorrectOption;
			ConnectStateTimer = new UITimer(800, MonitorChannelStateChange);
			ConnectStateTimer.Start();
			base.ShowInTaskbar = false;
			base.PreviewKeyDown += OnPreviewKeyDown;
		}

		private void OnPreviewKeyDown(object sender, KeyEventArgs e)
		{
			Key key = e.Key;
			if (key == Key.Back || key == Key.F5)
			{
				e.Handled = true;
			}
		}

		public void Dispose()
		{
			this.SdkEventHandler = null;
		}

		protected override void OnClosed(EventArgs e)
		{
			ConnectStateTimer.Stop();
			Instance.SdkCallbackEvent -= OnSdkCallback;
			base.OnClosed(e);
		}

		private void MonitorChannelStateChange()
		{
			if (Instance != null)
			{
				switch (Instance.Prop_Attr_ConnState)
				{
				case 0:
				case 1:
				case 2:
					Close();
					break;
				}
			}
		}

		private void SetCorrectionForDoseStudy()
		{
			if (Instance == null || (1 == Instance.Prop_Attr_UROM_PrepCapMode && 45 == Instance.Prop_Attr_UROM_ProductNo))
			{
				return;
			}
			int num = 196611;
			bool flag = ((Instance.Prop_Attr_CurrentCorrectOption & num) != 0) ? true : false;
			bool flag2 = ((Instance.Prop_Attr_CurrentCorrectOption & ~num) == 0) ? true : false;
			if (!flag || !flag2)
			{
				int num2 = 0;
				num2 = (flag ? (Instance.Prop_Attr_CurrentCorrectOption & num) : ((int)((presetOffsetOption == PresetOffsetOption.None) ? PresetOffsetOption.SWPreOffset : presetOffsetOption)));
				int num3 = Instance.Invoke(6, num2);
				if (1 == num3)
				{
					CloseCreateWnd = new DlgTaskTimeOut("Initialization", "Initializing for dose study...", 30, this, InitFailed);
					CloseCreateWnd.RunTask();
				}
			}
		}

		private void ResumeCorrection()
		{
			if (backupCorrection != Instance.Prop_Attr_CurrentCorrectOption)
			{
				if (presetOffsetOption != 0)
				{
					backupCorrection |= (int)presetOffsetOption;
				}
				int num = Instance.Invoke(6, backupCorrection);
				if (1 == num)
				{
					CloseCreateWnd = new DlgTaskTimeOut("SetCorrection", "Resume correction", 30, this);
					CloseCreateWnd.RunTask();
				}
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
		}

		private void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			EventMsg.AddSdkInfo(sender.ID, (Enm_EventLevel)nEventLevel, nEventID, nParam1, nParam2, strMsg);
			switch (nEventID)
			{
			case 6:
				CloseMessageBox();
				break;
			case 4:
				if (1005 != nParam1)
				{
					CloseMessageBox();
				}
				break;
			case 5:
				CloseMessageBox();
				if (6 == nParam1)
				{
					MessageBoxShow("Set correction failed! Err=" + ErrorHelper.GetErrorDesp(nParam2));
				}
				break;
			case 17:
				if (30 == nParam2)
				{
					ConnectStateTimer.Stop();
					Close();
				}
				break;
			}
		}

		private void OnLoadCompleted(object sender, NavigationEventArgs e)
		{
			CurPage = (xFrame.NavigationService.Content as Page);
			if (CurPage is CorrectionPreparePage)
			{
				xComboMsg.Visibility = Visibility.Hidden;
			}
			else
			{
				xComboMsg.Visibility = Visibility.Visible;
			}
			if (CurPage is CorrectionPreparePage)
			{
				CorrectionPreparePage correctionPreparePage = CurPage as CorrectionPreparePage;
				correctionPreparePage.InitDetectorInfo(new DetectorInfo(Instance, new Shell(this)));
			}
			else if (CurPage is GenerateOffsetPage)
			{
				bool flag = true;
				switch (Instance.Prop_Attr_UROM_ProductNo)
				{
				case 6:
				case 46:
				case 48:
				case 49:
				case 50:
				case 56:
				case 68:
				case 73:
				case 74:
				case 87:
					flag = ((Instance.Prop_Attr_OffsetValidityState == 1 || Instance.Prop_Attr_OffsetValidityState == 2) ? true : false);
					break;
				case 58:
					flag = false;
					break;
				default:
					flag = ((presetOffsetOption != PresetOffsetOption.SWPreOffset) ? true : false);
					break;
				}
				(CurPage as GenerateOffsetPage).EnableSkip(flag);
			}
			else if (CurPage is CorrectionStudyPage)
			{
				base.Dispatcher.BeginInvoke((Action)delegate
				{
					SetCorrectionForDoseStudy();
				});
			}
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (4 == Instance.Prop_Attr_ConnState || 3 == Instance.Prop_Attr_ConnState)
			{
				if (CurPage is GenerateGainPage || CurPage is GenerateDefectPage || CurPage is GenerateDynamicAcqPage || (CurPage is GenerateOffsetPage && !(CurPage as GenerateOffsetPage).Finished) || CurPage is GenerateDynamicGainDefectPage)
				{
					int num = Instance.Abort();
					if (1 == num)
					{
						CloseCreateWnd = new DlgTaskTimeOut("Cancel task", "Cancel current task...", 30, this);
						CloseCreateWnd.RunTask();
					}
					else if (num != 0)
					{
						MessageBoxShow("Abort failed ! Err=" + ErrorHelper.GetErrorDesp(num));
					}
				}
				ResumeCorrection();
			}
			base.OnClosing(e);
		}

		private void InitFailed()
		{
			CloseCreateWnd.StopTask();
			MessageBoxShow("Set correction failed! Timeout!");
		}

		private void CloseMessageBox()
		{
			if (CloseCreateWnd != null)
			{
				CloseCreateWnd.StopTask();
				CloseCreateWnd = null;
			}
		}

		private void MessageBoxShow(string content)
		{
			MessageBox.Show(this, content);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/createtemplatewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xFrame = (Frame)target;
				break;
			case 2:
				xComboMsg = (ComboBox)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
