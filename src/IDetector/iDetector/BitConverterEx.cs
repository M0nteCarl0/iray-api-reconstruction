using System;

namespace iDetector
{
	public static class BitConverterEx
	{
		public static float ToFloat(byte[] _input, int startIndex)
		{
			byte[] array = new byte[8];
			Array.Copy(_input, startIndex, array, 4, 4);
			double num = BitConverter.ToDouble(array, 0);
			return (float)num;
		}
	}
}
