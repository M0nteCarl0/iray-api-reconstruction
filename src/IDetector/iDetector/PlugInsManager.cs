using PlugInInterface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace iDetector
{
	internal class PlugInsManager
	{
		private List<IPlugIn> plugs;

		private static PlugInsManager plugInstance;

		private static List<PlugInsManager> plugList = new List<PlugInsManager>();

		public List<IPlugIn> Plugs
		{
			get
			{
				return plugs;
			}
		}

		public static PlugInsManager Instance()
		{
			if (plugInstance == null)
			{
				plugInstance = new PlugInsManager();
			}
			return plugInstance;
		}

		public static PlugInsManager Creator()
		{
			PlugInsManager plugInsManager = new PlugInsManager();
			plugList.Add(plugInsManager);
			return plugInsManager;
		}

		public static void Destory()
		{
			foreach (PlugInsManager plug in plugList)
			{
				plug.Clear();
			}
			plugList.Clear();
		}

		public void Clear()
		{
			if (plugs != null)
			{
				plugs.Clear();
				plugList.Remove(this);
			}
		}

		private PlugInsManager()
		{
			DirectoryInfo directoryInfo = new DirectoryInfo("./plugin/");
			if (Utility.IsDirectoryExisted(directoryInfo.FullName))
			{
				plugs = new List<IPlugIn>();
				List<FileInfo> list = directoryInfo.GetFiles("*.dll").ToList();
				foreach (FileInfo item in list)
				{
					try
					{
						Assembly assembly = Assembly.LoadFrom(item.FullName);
						Type[] exportedTypes = assembly.GetExportedTypes();
						Type[] array = exportedTypes;
						foreach (Type type in array)
						{
							try
							{
								if (assembly.CreateInstance(type.FullName) is IPlugIn)
								{
									plugs.Add(assembly.CreateInstance(type.FullName) as IPlugIn);
								}
							}
							catch
							{
							}
						}
					}
					catch
					{
					}
				}
			}
		}

		public object GetPlugElementByClassName(string name)
		{
			if (plugs == null)
			{
				return null;
			}
			foreach (IPlugIn plug in plugs)
			{
				string fullName = plug.GetType().FullName;
				string text = fullName.Substring(fullName.LastIndexOf(".") + 1, fullName.Length - fullName.LastIndexOf(".") - 1);
				if (text.Equals(name))
				{
					return plug.LoadComponent();
				}
			}
			return null;
		}

		public IPlugIn GetPlugObjByClassName(string name)
		{
			if (plugs == null)
			{
				return null;
			}
			foreach (IPlugIn plug in plugs)
			{
				string fullName = plug.GetType().FullName;
				string text = fullName.Substring(fullName.LastIndexOf(".") + 1, fullName.Length - fullName.LastIndexOf(".") - 1);
				if (text.Equals(name))
				{
					return plug;
				}
			}
			return null;
		}
	}
}
