using CorrectExUI;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class CalibrateExPage : UserControl, IDetSubPage, IComponentConnector
	{
		private Detector instance;

		private DetecInfo _detecInfo;

		public CorrectionExPreparePage _preparePage;

		public CorrectionExModeFilePage _modeFilePage;

		public CorrectionExModeFileStaticPage _modeFileStaticPage;

		public GenerateExOffsetPage _genOffsetPage;

		public GenerateExDynamicGainDefectPage _genGainDefectPage;

		public GenerateExGainPage _genGainPage;

		public GenerateExDefectPage _genDefectPage;

		public GenerateExDynamicAcqPage _genDynamicGainAcqPage;

		public GenerateExDynamicAcqPage _genDynamicDefectAcqPage;

		private TabItem _currentTabitem;

		internal TabControl _tabCalibration;

		internal TabItem _tabItemWelcome;

		internal TabItem _tabItemModeFiles;

		internal TabItem _tabItemModeFilesStatic;

		internal TabItem _tabItemOffset;

		internal TabItem _tabItemGain;

		internal TabItem _tabItemDefect;

		internal TabItem _tabItemDynaGain;

		internal TabItem _tabItemDynaDefect;

		internal TabItem _tabItemGainDefect;

		private bool _contentLoaded;

		public virtual Detector Instance
		{
			get
			{
				return instance;
			}
			set
			{
				instance = value;
				InitInstance();
			}
		}

		public bool bHandled
		{
			get;
			set;
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		public void OnClose()
		{
		}

		public bool Exit()
		{
			return true;
		}

		public bool Enter()
		{
			return true;
		}

		public CalibrateExPage()
		{
			InitializeComponent();
		}

		private void InitInstance()
		{
			_detecInfo = new DetecInfo(Instance, new Shell(Application.Current.MainWindow));
			_preparePage = new CorrectionExPreparePage(instance);
			_genOffsetPage = new GenerateExOffsetPage(_detecInfo);
			_modeFileStaticPage = new CorrectionExModeFileStaticPage(instance);
			_modeFilePage = new CorrectionExModeFilePage(instance);
			_genGainPage = new GenerateExGainPage(_detecInfo);
			_genDefectPage = new GenerateExDefectPage(_detecInfo);
			_genGainDefectPage = new GenerateExDynamicGainDefectPage(_detecInfo);
			_genDynamicGainAcqPage = new GenerateExDynamicAcqPage(_detecInfo, Enm_FileTypes.Enm_File_Gain);
			_genDynamicDefectAcqPage = new GenerateExDynamicAcqPage(_detecInfo, Enm_FileTypes.Enm_File_Defect);
		}

		public void Initialize()
		{
			_tabItemWelcome.Content = _preparePage;
			_tabItemOffset.Content = _genOffsetPage;
			_tabItemModeFilesStatic.Header = "Mode&Files";
			_tabItemGainDefect.Header = "Create Gain&Defect";
			_preparePage.Initialize();
			switch (instance.Prop_Attr_UROM_ProductNo)
			{
			case 11:
			case 32:
			case 37:
			case 38:
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 59:
			case 60:
			case 62:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				_tabItemModeFilesStatic.Content = _modeFileStaticPage;
				_tabItemGain.Content = _genGainPage;
				_tabItemDefect.Content = _genDefectPage;
				_tabCalibration.Items.Remove(_tabItemModeFiles);
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_tabCalibration.Items.Remove(_tabItemDynaGain);
				_tabCalibration.Items.Remove(_tabItemDynaDefect);
				_modeFileStaticPage.Initialize();
				_genGainPage.Initialize();
				_genDefectPage.Initialize();
				_modeFilePage.RemoveCallbackHandler();
				break;
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 68:
			case 73:
			case 74:
			case 87:
				_tabItemModeFiles.Content = _modeFilePage;
				_tabItemGainDefect.Content = _genGainDefectPage;
				_tabCalibration.Items.Remove(_tabItemModeFilesStatic);
				_tabCalibration.Items.Remove(_tabItemGain);
				_tabCalibration.Items.Remove(_tabItemDefect);
				_tabCalibration.Items.Remove(_tabItemDynaGain);
				_tabCalibration.Items.Remove(_tabItemDynaDefect);
				_modeFilePage.Initialize(instance.Prop_Attr_UROM_ProductNo);
				_genGainDefectPage.Initialize();
				_modeFileStaticPage.RemoveCallbackHandler();
				break;
			case 58:
				_tabItemModeFilesStatic.Content = _modeFileStaticPage;
				_tabItemDynaGain.Content = _genDynamicGainAcqPage;
				_tabItemDynaDefect.Content = _genDynamicDefectAcqPage;
				_tabCalibration.Items.Remove(_tabItemModeFiles);
				_tabCalibration.Items.Remove(_tabItemGain);
				_tabCalibration.Items.Remove(_tabItemDefect);
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_modeFileStaticPage.Initialize();
				_genDynamicGainAcqPage.Initialize();
				_genDynamicDefectAcqPage.Initialize();
				_modeFilePage.RemoveCallbackHandler();
				break;
			default:
				_tabItemModeFilesStatic.Content = _modeFileStaticPage;
				_tabItemGain.Content = _genGainPage;
				_tabItemDefect.Content = _genDefectPage;
				_tabCalibration.Items.Remove(_tabItemModeFiles);
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_tabCalibration.Items.Remove(_tabItemDynaGain);
				_tabCalibration.Items.Remove(_tabItemDynaDefect);
				_modeFileStaticPage.Initialize();
				_genGainPage.Initialize();
				_genDefectPage.Initialize();
				_modeFilePage.RemoveCallbackHandler();
				break;
			}
		}

		private void _tabCalibration_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!(e.Source is TabControl))
			{
				return;
			}
			TabItem currentTabitem = _tabCalibration.SelectedItem as TabItem;
			if (_currentTabitem != null)
			{
				switch (_currentTabitem.Name)
				{
				case "_tabItemOffset":
					if (_genOffsetPage.GetCancelStatus())
					{
						_genOffsetPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemOffset;
						_currentTabitem = _tabItemOffset;
						return;
					}
					break;
				case "_tabItemGain":
					if (_genGainPage.GetCancelStatus())
					{
						_genGainPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemGain;
						_currentTabitem = _tabItemGain;
						return;
					}
					break;
				case "_tabItemDefect":
					if (_genDefectPage.GetCancelStatus())
					{
						_genDefectPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemDefect;
						_currentTabitem = _tabItemDefect;
						return;
					}
					break;
				case "_tabItemGainDefect":
					if (_genGainDefectPage.GetCancelStatus())
					{
						_genGainDefectPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemGainDefect;
						_currentTabitem = _tabItemGainDefect;
						return;
					}
					break;
				case "_tabItemDynaGain":
					if (_genDynamicGainAcqPage.GetCancelStatus())
					{
						_genDynamicGainAcqPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemDynaGain;
						_currentTabitem = _tabItemDynaGain;
						return;
					}
					break;
				case "_tabItemDynaDefect":
					if (_genDynamicDefectAcqPage.GetCancelStatus())
					{
						_genDynamicDefectAcqPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemDynaDefect;
						_currentTabitem = _tabItemDynaDefect;
						return;
					}
					break;
				}
			}
			_currentTabitem = currentTabitem;
		}

		public bool GetGenExCorrectStatus()
		{
			bool flag = false;
			if (_currentTabitem != null)
			{
				switch (_currentTabitem.Name)
				{
				case "_tabItemOffset":
					flag = _genOffsetPage.GetCancelStatus();
					if (flag)
					{
						_genOffsetPage.DoCancel();
					}
					break;
				case "_tabItemGain":
					flag = _genGainPage.GetCancelStatus();
					if (flag)
					{
						_genGainPage.DoCancel();
					}
					break;
				case "_tabItemDefect":
					flag = _genDefectPage.GetCancelStatus();
					if (flag)
					{
						_genDefectPage.DoCancel();
					}
					break;
				case "_tabItemGainDefect":
					flag = _genGainDefectPage.GetCancelStatus();
					if (flag)
					{
						_genGainDefectPage.DoCancel();
					}
					break;
				case "_tabItemDynaGain":
					flag = _genDynamicGainAcqPage.GetCancelStatus();
					if (flag)
					{
						_genDynamicGainAcqPage.DoCancel();
					}
					break;
				case "_tabItemDynaDefect":
					flag = _genDynamicDefectAcqPage.GetCancelStatus();
					if (flag)
					{
						_genDynamicDefectAcqPage.DoCancel();
					}
					break;
				}
			}
			return flag;
		}

		public void RemoveCallbackHdl()
		{
			_modeFilePage.RemoveCallbackHandler();
			_modeFileStaticPage.RemoveCallbackHandler();
		}

		public void SetGenExCorrectStatus(bool bValue)
		{
			if (_currentTabitem != null)
			{
				switch (_currentTabitem.Name)
				{
				case "_tabItemOffset":
					_genOffsetPage.SetCancelStatus(bValue);
					break;
				case "_tabItemGain":
					_genGainPage.SetCancelStatus(bValue);
					break;
				case "_tabItemDefect":
					_genDefectPage.SetCancelStatus(bValue);
					break;
				case "_tabItemGainDefect":
					_genGainDefectPage.SetCancelStatus(bValue);
					break;
				case "_tabItemDynaGain":
					_genDynamicGainAcqPage.SetCancelStatus(bValue);
					break;
				case "_tabItemDynaDefect":
					_genDynamicDefectAcqPage.SetCancelStatus(bValue);
					break;
				}
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/calibrateexpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_tabCalibration = (TabControl)target;
				_tabCalibration.SelectionChanged += _tabCalibration_SelectionChanged;
				break;
			case 2:
				_tabItemWelcome = (TabItem)target;
				break;
			case 3:
				_tabItemModeFiles = (TabItem)target;
				break;
			case 4:
				_tabItemModeFilesStatic = (TabItem)target;
				break;
			case 5:
				_tabItemOffset = (TabItem)target;
				break;
			case 6:
				_tabItemGain = (TabItem)target;
				break;
			case 7:
				_tabItemDefect = (TabItem)target;
				break;
			case 8:
				_tabItemDynaGain = (TabItem)target;
				break;
			case 9:
				_tabItemDynaDefect = (TabItem)target;
				break;
			case 10:
				_tabItemGainDefect = (TabItem)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
