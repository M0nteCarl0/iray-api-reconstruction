using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace iDetector
{
	internal class VTConfigVM : NotifyObject
	{
		private VTMapItem curVTItem;

		private int selBinningValue;

		private int selPGAValue;

		private float setVTValue;

		private string resultLabel;

		public DelegateCommand AddVTItem
		{
			get;
			set;
		}

		public DelegateCommand DelVTItem
		{
			get;
			set;
		}

		public DelegateCommand ReadVTMap
		{
			get;
			set;
		}

		public DelegateCommand WriteVTMap
		{
			get;
			set;
		}

		public ObservableCollection<VTMapItem> VTMapList
		{
			get;
			set;
		}

		public VTMapItem CurVTItem
		{
			get
			{
				return curVTItem;
			}
			set
			{
				curVTItem = value;
				NotifyPropertyChanged("CurVTItem");
			}
		}

		public ObservableCollection<EnumDisplayItem> PGAList
		{
			get;
			set;
		}

		public int SelBinningValue
		{
			get
			{
				return selBinningValue;
			}
			set
			{
				selBinningValue = value;
				NotifyPropertyChanged("SelBinningValue");
			}
		}

		public ObservableCollection<EnumDisplayItem> BinningList
		{
			get;
			set;
		}

		public int SelPGAValue
		{
			get
			{
				return selPGAValue;
			}
			set
			{
				selPGAValue = value;
				NotifyPropertyChanged("SelPGAValue");
			}
		}

		public float SetVTValue
		{
			get
			{
				return setVTValue;
			}
			set
			{
				setVTValue = value;
				NotifyPropertyChanged("SetVTValue");
			}
		}

		public string ResultLabel
		{
			get
			{
				return resultLabel;
			}
			set
			{
				resultLabel = value;
				NotifyPropertyChanged("ResultLabel");
			}
		}

		private Detector DetInstance
		{
			get;
			set;
		}

		public VTConfigVM(Detector detector)
		{
			DetInstance = detector;
			VTMapList = new ObservableCollection<VTMapItem>();
			AddVTItem = new DelegateCommand(OnAddVTItem);
			DelVTItem = new DelegateCommand(OnDelVTItem);
			ReadVTMap = new DelegateCommand(OnReadVTMap);
			WriteVTMap = new DelegateCommand(OnWriteVTMap);
			PGAList = SDKEnumCollector.LoadParam("Enm_PGA");
			BinningList = SDKEnumCollector.LoadParam("Enm_Binning");
			ReadVTMapList();
		}

		private void OnWriteVTMap(object obj)
		{
			stVTMapItem[] array = new stVTMapItem[VTMapList.Count];
			for (int i = 0; i < VTMapList.Count; i++)
			{
				array[i] = VTMapList[i].vtItem;
			}
			IRayCmdParam[] array2 = new IRayCmdParam[1];
			array2[0].pt = IRAY_PARAM_TYPE.IPT_BLOCK;
			array2[0].blc.uBytes = (uint)(Marshal.SizeOf(typeof(stVTMapItem)) * array.Length);
			array2[0].blc.pData = Marshal.AllocHGlobal((int)array2[0].blc.uBytes);
			SdkParamConvertor<stVTMapItem>.StructArrayToIntPtr(array, ref array2[0].blc.pData);
			int num = DetInstance.Invoke(2006, array2, array2.Length);
			if (num != 0)
			{
				ResultLabel = "Write failed";
				Shell.ShowMessageAutoOwner("Write VTMap failed! Err = " + ErrorHelper.GetErrorDesp(num));
			}
			else
			{
				ResultLabel = "Write succeed";
			}
			Marshal.FreeHGlobal(array2[0].blc.pData);
		}

		private void OnReadVTMap(object obj)
		{
			ReadVTMapList();
		}

		private void ReadVTMapList()
		{
			int num = 32;
			IRayCmdParam[] array = new IRayCmdParam[1];
			array[0].pt = IRAY_PARAM_TYPE.IPT_BLOCK;
			array[0].blc.uBytes = (uint)(Marshal.SizeOf(typeof(stVTMapItem)) * num);
			array[0].blc.pData = Marshal.AllocHGlobal((int)array[0].blc.uBytes);
			int num2 = DetInstance.Invoke(2005, array, array.Length);
			if (num2 != 0)
			{
				ResultLabel = "Read failed";
				Shell.ShowMessageAutoOwner("Read VTMap failed! Err = " + ErrorHelper.GetErrorDesp(num2));
			}
			else
			{
				ResultLabel = "Read succeed";
			}
			stVTMapItem[] arr = new stVTMapItem[num];
			SdkParamConvertor<stVTMapItem>.IntPtrToStructArray(array[0].blc.pData, ref arr);
			VTMapList.Clear();
			stVTMapItem[] array2 = arr;
			for (int i = 0; i < array2.Length; i++)
			{
				stVTMapItem stVTMapItem = array2[i];
				if ((double)stVTMapItem.VTValue > 0.001)
				{
					VTMapList.Add(new VTMapItem
					{
						PGA = stVTMapItem.PGA,
						Binning = stVTMapItem.Binning,
						VTValue = stVTMapItem.VTValue
					});
				}
			}
			Marshal.FreeHGlobal(array[0].blc.pData);
		}

		private void OnDelVTItem(object obj)
		{
			if (CurVTItem != null)
			{
				VTMapList.RemoveAt(VTMapList.IndexOf(CurVTItem));
			}
		}

		private void OnAddVTItem(object obj)
		{
			if ((double)SetVTValue < 0.001)
			{
				Shell.ShowMessageAutoOwner("VT value cannot be zero!");
				return;
			}
			foreach (VTMapItem vTMap in VTMapList)
			{
				if (vTMap.PGA == SelPGAValue && vTMap.Binning == SelBinningValue)
				{
					vTMap.VTValue = SetVTValue;
					return;
				}
			}
			VTMapList.Add(new VTMapItem
			{
				PGA = SelPGAValue,
				Binning = SelBinningValue,
				VTValue = SetVTValue
			});
		}
	}
}
