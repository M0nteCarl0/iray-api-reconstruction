using System;
using System.Windows;

namespace iDetector
{
	public class DlgTaskTimeOut : IProgress
	{
		private MessageWnd messageBox;

		private MMTimer timer;

		public bool IsTaskRunning
		{
			get;
			set;
		}

		private event Action timeoutCallback;

		private event Action cancelCallback;

		public DlgTaskTimeOut(string title, string content, int timeOutSeconds, Window owner = null, Action _timeoutCallback = null)
		{
			messageBox = new MessageWnd(title, owner);
			messageBox.SetMessage(content);
			timer = new MMTimer(timeOutSeconds * 1000, TimerCallback);
			this.timeoutCallback = _timeoutCallback;
		}

		public DlgTaskTimeOut(string title, string content, bool canCancel, Window owner, Action _cancelCallback)
		{
			messageBox = new MessageWnd(title, canCancel, owner);
			messageBox.SetMessage(content);
			timer = new MMTimer(int.MaxValue, TimerCallback);
			this.cancelCallback = _cancelCallback;
		}

		private void TimerCallback()
		{
			messageBox.Dispatcher.Invoke((Action)delegate
			{
				StopTask();
				if (this.timeoutCallback != null)
				{
					this.timeoutCallback();
				}
			}, new object[0]);
		}

		public int RunTask()
		{
			IsTaskRunning = true;
			timer.Start();
			messageBox.ShowDialog();
			if (messageBox != null && messageBox.Result == MessageWnd.ResultCode.Cancel && this.cancelCallback != null)
			{
				this.cancelCallback();
			}
			return 0;
		}

		public void SetStatus(string curStatus)
		{
			if (messageBox != null)
			{
				messageBox.SetMessage(curStatus);
			}
		}

		public void StopTask()
		{
			IsTaskRunning = false;
			timer.Close();
			if (messageBox != null)
			{
				messageBox.Close();
				messageBox = null;
			}
			this.timeoutCallback = null;
			this.cancelCallback = null;
		}
	}
}
