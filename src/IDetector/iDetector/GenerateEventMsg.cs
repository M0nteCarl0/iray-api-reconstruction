using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace iDetector
{
	internal class GenerateEventMsg
	{
		private ComboBox comBox;

		private static int MessageNumber = 20;

		private StringBuilder eventInfo;

		public readonly Dictionary<Enm_EventLevel, Brush> backgroundMap;

		public bool EnableLogCallbackEvent
		{
			get;
			set;
		}

		public GenerateEventMsg(ComboBox _comBox)
		{
			comBox = _comBox;
			eventInfo = new StringBuilder();
			backgroundMap = new Dictionary<Enm_EventLevel, Brush>();
			backgroundMap.Add(Enm_EventLevel.Enm_EventLevel_Info, Brushes.White);
			backgroundMap.Add(Enm_EventLevel.Enm_EventLevel_Notify, Brushes.White);
			backgroundMap.Add(Enm_EventLevel.Enm_EventLevel_Error, Brushes.LightYellow);
			backgroundMap.Add(Enm_EventLevel.Enm_EventLevel_Warn, Brushes.Yellow);
		}

		public void AddSdkInfo(int detID, Enm_EventLevel eventLevel, int eventID, int cmdID, int errorCode, string defaultStr)
		{
			SetBackground(eventLevel);
			AddSdkInfo(eventID, cmdID, errorCode, defaultStr);
			if (!EnableLogCallbackEvent || 1001 == eventID)
			{
				return;
			}
			string text = SdkInterface.EventTable[0];
			if (SdkInterface.EventTable.ContainsKey(eventID))
			{
				text = SdkInterface.EventTable[eventID];
			}
			switch (eventID)
			{
			case 4:
			case 5:
			case 6:
			{
				string text2 = SdkInterface.CommandIDTable[0];
				if (SdkInterface.CommandIDTable.ContainsKey(cmdID))
				{
					text2 = SdkInterface.CommandIDTable[cmdID];
				}
				Log.Instance().Write("Detector[{0}] receive event[{1}]:{2}, command[{3}]:{4}, para2:{5}", detID, eventID, text, cmdID, text2, errorCode);
				break;
			}
			default:
				Log.Instance().Write("Detector[{0}] receive event[{1}]:{2}--{3}", detID, eventID, text, defaultStr);
				break;
			}
		}

		private void AddSdkInfo(int eventID, int cmdID, int errorCode, string defaultStr)
		{
			if (4 == eventID || 5 == eventID || 6 == eventID)
			{
				eventInfo.Clear();
				eventInfo.Append(defaultStr);
				eventInfo.Append(": ");
				if (SdkInterface.CommandIDTable.ContainsKey(cmdID))
				{
					eventInfo.Append(SdkInterface.CommandIDTable[cmdID]);
				}
				else
				{
					eventInfo.Append(SdkInterface.CommandIDTable[0]);
				}
				if (5 == eventID)
				{
					eventInfo.Append(" - ");
					eventInfo.Append(ErrorHelper.GetErrorDesp(errorCode));
				}
				AddSdkInfo(DateTime.Now.ToString("HH:mm:ss") + " " + eventInfo.ToString());
			}
			else
			{
				AddSdkInfo(DateTime.Now.ToString("HH:mm:ss") + " " + defaultStr);
			}
		}

		private void SetBackground(Enm_EventLevel eventLevel)
		{
			if (comBox != null && backgroundMap.ContainsKey(eventLevel))
			{
				comBox.Background = backgroundMap[eventLevel];
			}
		}

		public void AddSdkInfo(string info)
		{
			if (comBox != null)
			{
				if (comBox.Items.Count > MessageNumber)
				{
					comBox.Items.RemoveAt(0);
				}
				comBox.Items.Add(info);
				comBox.SelectedIndex = comBox.Items.Count - 1;
			}
		}
	}
}
