namespace iDetector
{
	public class ModeList
	{
		public string Disp
		{
			get;
			set;
		}

		public ApplicationMode Mode
		{
			get;
			set;
		}
	}
}
