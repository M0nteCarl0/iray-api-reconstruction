using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class MessageWnd : Window, IComponentConnector
	{
		public enum ResultCode
		{
			Failed,
			Cancel,
			Succeed
		}

		internal Label _Title;

		internal TextBlock _Msg;

		internal Button xCancelBtn;

		private bool _contentLoaded;

		public ResultCode Result
		{
			get;
			set;
		}

		public MessageWnd(string title, Window owner = null)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			if (owner == null)
			{
				base.Owner = Application.Current.MainWindow;
			}
			else
			{
				base.Owner = owner;
			}
			_Title.Content = title;
			Result = ResultCode.Succeed;
		}

		public MessageWnd(string title, bool canCancel, Window owner = null)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			if (owner == null)
			{
				base.Owner = Application.Current.MainWindow;
			}
			else
			{
				base.Owner = owner;
			}
			_Title.Content = title;
			if (canCancel)
			{
				xCancelBtn.Visibility = Visibility.Visible;
			}
		}

		public void SetMessage(string msg)
		{
			_Msg.Text = msg;
		}

		private void CancelBtn_Click(object sender, RoutedEventArgs e)
		{
			if (MessageBoxResult.Yes == MessageBox.Show("Continue cancel?", "Question", MessageBoxButton.YesNo))
			{
				Close();
				Result = ResultCode.Cancel;
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/messagewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_Title = (Label)target;
				break;
			case 2:
				_Msg = (TextBlock)target;
				break;
			case 3:
				xCancelBtn = (Button)target;
				xCancelBtn.Click += CancelBtn_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
