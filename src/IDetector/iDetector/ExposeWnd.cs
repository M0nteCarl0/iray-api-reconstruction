using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Threading;

namespace iDetector
{
	public class ExposeWnd : Window, IComponentConnector
	{
		private DispatcherTimer mTimer;

		private int mTotalTime;

		private int mLeftTime;

		internal ProgressBar LeftTimeBar;

		internal Label LeftTime;

		private bool _contentLoaded;

		public ExposeWnd(int totalTime)
		{
			if (totalTime != 0)
			{
				mTotalTime = totalTime;
				mLeftTime = totalTime;
				InitializeComponent();
				base.ShowInTaskbar = false;
				base.Owner = Application.Current.MainWindow;
				mTimer = new DispatcherTimer();
				mTimer.Interval = TimeSpan.FromSeconds(1.0);
				mTimer.Tick += OnTimer;
				mTimer.Start();
				LeftTimeBar.Value = mLeftTime * 100 / mTotalTime;
				LeftTime.Content = mLeftTime.ToString();
			}
		}

		private void OnTimer(object sender, EventArgs e)
		{
			mLeftTime--;
			LeftTimeBar.Value = mLeftTime * 100 / mTotalTime;
			LeftTime.Content = mLeftTime.ToString();
			if (mLeftTime <= 0)
			{
				mTimer.Stop();
				Close();
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/exposewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				LeftTimeBar = (ProgressBar)target;
				break;
			case 2:
				LeftTime = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
