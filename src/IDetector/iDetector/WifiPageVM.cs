using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;

namespace iDetector
{
	public class WifiPageVM : NotifyObject
	{
		private string _WifiSSID;

		private string _WifiKey;

		private string _WifiStatus;

		private bool _WifiDHCPServer;

		private bool _APModeEnable;

		private bool _ClientModeEnable;

		private bool _DHCPServerEnable;

		private EnumDisplayItem _CurWifiSecurity;

		private EnumDisplayItem _CurWifiFreq;

		private EnumDisplayItem _CurWifiCountry;

		private EnumDisplayItem _CurWifiBand;

		private EnumDisplayItem _CurWifiChannel;

		private WifiClientParam LastSelectClient;

		private WifiClientParam _CurWifiClient;

		private WirelessNetwork _CurWifiNetWork;

		private SDKInvokeProxy sdkInvokeProxy;

		private readonly int ClientCount = 10;

		private FrameworkElement UIUser;

		private readonly string WarningColor = "#FFF0F000";

		private readonly string NormalColor = "";

		private WifiClientParam UsingClient;

		public DelegateCommand ReadWifiConfig
		{
			get;
			set;
		}

		public DelegateCommand WriteWifiConfig
		{
			get;
			set;
		}

		public DelegateCommand ReadWifiStatus
		{
			get;
			set;
		}

		public DelegateCommand ScanWifiNetWork
		{
			get;
			set;
		}

		public DelegateCommand AddClient
		{
			get;
			set;
		}

		public DelegateCommand DelClient
		{
			get;
			set;
		}

		public DelegateCommand UpClient
		{
			get;
			set;
		}

		public DelegateCommand DownClient
		{
			get;
			set;
		}

		public DelegateCommand SelectClient
		{
			get;
			set;
		}

		public DelegateCommand DBClickClient
		{
			get;
			set;
		}

		public string WifiSSID
		{
			get
			{
				return _WifiSSID;
			}
			set
			{
				_WifiSSID = value;
				NotifyPropertyChanged("WifiSSID");
			}
		}

		public string WifiKey
		{
			get
			{
				return _WifiKey;
			}
			set
			{
				_WifiKey = value;
				NotifyPropertyChanged("WifiKey");
			}
		}

		public string WifiStatus
		{
			get
			{
				return _WifiStatus;
			}
			set
			{
				_WifiStatus = value;
				NotifyPropertyChanged("WifiStatus");
			}
		}

		public bool WifiDHCPServer
		{
			get
			{
				return _WifiDHCPServer;
			}
			set
			{
				_WifiDHCPServer = value;
				NotifyPropertyChanged("WifiDHCPServer");
			}
		}

		public bool APModeEnable
		{
			get
			{
				return _APModeEnable;
			}
			set
			{
				_APModeEnable = value;
				NotifyPropertyChanged("APModeEnable");
			}
		}

		public bool ClientModeEnable
		{
			get
			{
				return _ClientModeEnable;
			}
			set
			{
				_ClientModeEnable = value;
				NotifyPropertyChanged("ClientModeEnable");
			}
		}

		public bool DHCPServerEnable
		{
			get
			{
				return _DHCPServerEnable;
			}
			set
			{
				_DHCPServerEnable = value;
				NotifyPropertyChanged("DHCPServerEnable");
			}
		}

		public ObservableCollection<EnumDisplayItem> WifiSecurityList
		{
			get;
			set;
		}

		public EnumDisplayItem CurWifiSecurity
		{
			get
			{
				return _CurWifiSecurity;
			}
			set
			{
				_CurWifiSecurity = value;
				NotifyPropertyChanged("CurWifiSecurity");
			}
		}

		public ObservableCollection<EnumDisplayItem> WifiFreqList
		{
			get;
			set;
		}

		public EnumDisplayItem CurWifiFreq
		{
			get
			{
				return _CurWifiFreq;
			}
			set
			{
				_CurWifiFreq = value;
				NotifyPropertyChanged("CurWifiFreq");
				FilterWifiChanelList();
			}
		}

		public ObservableCollection<EnumDisplayItem> WifiCountryList
		{
			get;
			set;
		}

		public EnumDisplayItem CurWifiCountry
		{
			get
			{
				return _CurWifiCountry;
			}
			set
			{
				_CurWifiCountry = value;
				NotifyPropertyChanged("CurWifiCountry");
				FilterWifiChanelList();
			}
		}

		public ObservableCollection<EnumDisplayItem> WifiBandList
		{
			get;
			set;
		}

		public EnumDisplayItem CurWifiBand
		{
			get
			{
				return _CurWifiBand;
			}
			set
			{
				_CurWifiBand = value;
				NotifyPropertyChanged("CurWifiBand");
				FilterWifiChanelList();
			}
		}

		public ObservableCollection<EnumDisplayItem> WifiChannelList
		{
			get;
			set;
		}

		public ObservableCollection<EnumDisplayItem> TempWifiChannelList
		{
			get;
			set;
		}

		public EnumDisplayItem CurWifiChannel
		{
			get
			{
				return _CurWifiChannel;
			}
			set
			{
				_CurWifiChannel = value;
				NotifyPropertyChanged("CurWifiChannel");
			}
		}

		public ObservableCollection<WifiClientParam> WifiClientList
		{
			get;
			set;
		}

		public WifiClientParam CurWifiClient
		{
			get
			{
				return _CurWifiClient;
			}
			set
			{
				_CurWifiClient = value;
				NotifyPropertyChanged("CurWifiClient");
			}
		}

		public ObservableCollection<WirelessNetwork> WifiNetWorkList
		{
			get;
			set;
		}

		public WirelessNetwork CurWifiNetWork
		{
			get
			{
				return _CurWifiNetWork;
			}
			set
			{
				_CurWifiNetWork = value;
				NotifyPropertyChanged("CurWifiNetWork");
			}
		}

		private Detector Instance
		{
			get;
			set;
		}

		public WifiPageVM(FrameworkElement uiUser, Detector detector)
		{
			if (detector != null)
			{
				UIUser = uiUser;
				Instance = detector;
				sdkInvokeProxy = new SDKInvokeProxy(Instance, new Shell(Application.Current.MainWindow));
				ReadWifiConfig = new DelegateCommand(OnReadWifiConfig);
				WriteWifiConfig = new DelegateCommand(OnWriteWifiConfig);
				ReadWifiStatus = new DelegateCommand(OnReadWifiStatus);
				ScanWifiNetWork = new DelegateCommand(OnScanWifiNetWork);
				AddClient = new DelegateCommand(OnAddClient);
				DelClient = new DelegateCommand(OnDelClient);
				UpClient = new DelegateCommand(OnUpClient);
				DownClient = new DelegateCommand(OnDownClient);
				SelectClient = new DelegateCommand(OnSelectClient);
				WifiNetWorkList = new ObservableCollection<WirelessNetwork>();
				InitParam();
			}
		}

		public void RegisterCallback()
		{
			Instance.SdkCallbackEvent += OnSdkCallback;
		}

		public void UnRegisterCallback()
		{
			Instance.SdkCallbackEvent -= OnSdkCallback;
		}

		private void OnReadWifiConfig(object obj)
		{
			sdkInvokeProxy.Invoke(2024);
		}

		private void OnWriteWifiConfig(object obj)
		{
			int num = WriteWifiParams();
			if (num == 0)
			{
				sdkInvokeProxy.Invoke(2023);
			}
			else
			{
				Shell.ShowMessageOwnerByMainwindow("Failed! Err = " + ErrorHelper.GetErrorDesp(num));
			}
		}

		private void OnReadWifiStatus(object obj)
		{
			sdkInvokeProxy.Invoke(2021);
		}

		private void OnScanWifiNetWork(object obj)
		{
			sdkInvokeProxy.Invoke(2022);
		}

		private void OnAddClient(object obj)
		{
			if (ClientCount == WifiClientList.Count)
			{
				Shell.ShowMessageOwnerByMainwindow("Client item is full!");
				return;
			}
			AddWifiClientWnd addWifiClientWnd = new AddWifiClientWnd();
			addWifiClientWnd.ShowDialog();
			if (addWifiClientWnd.Result)
			{
				WifiClientList.Add(new WifiClientParam
				{
					SSID = addWifiClientWnd.SSID,
					Key = addWifiClientWnd.Key
				});
			}
		}

		private void OnDelClient(object obj)
		{
			if (CurWifiClient == null)
			{
				return;
			}
			if (WifiClientList.Count < 2)
			{
				Shell.ShowMessageOwnerByMainwindow("Client cannot be empty!");
				return;
			}
			WifiClientList.RemoveAt(WifiClientList.IndexOf(CurWifiClient));
			if (WifiClientList.Count != 0)
			{
				int prop_Attr_Wifi_Client_CurrentSel = Instance.Prop_Attr_Wifi_Client_CurrentSel;
				WifiClientParam newclient;
				if (prop_Attr_Wifi_Client_CurrentSel < WifiClientList.Count)
				{
					newclient = WifiClientList[Instance.Prop_Attr_Wifi_Client_CurrentSel];
				}
				else
				{
					newclient = WifiClientList[WifiClientList.Count - 1];
					SdkInterface.SetAttr(Instance.ID, 4511, WifiClientList.Count - 1);
				}
				UpdateSelectedClient(newclient);
			}
		}

		private void UpdateSelectedClient(WifiClientParam newclient)
		{
			if (LastSelectClient != null)
			{
				LastSelectClient.SelectedFlag = false;
			}
			LastSelectClient = newclient;
			LastSelectClient.SelectedFlag = true;
			if (LastSelectClient.Equals(UsingClient))
			{
				LastSelectClient.BKColor = NormalColor;
			}
			else
			{
				LastSelectClient.BKColor = WarningColor;
			}
		}

		public void OnDBClickClient(object sender, MouseButtonEventArgs e)
		{
			if (CurWifiClient != null)
			{
				AddWifiClientWnd addWifiClientWnd = new AddWifiClientWnd(CurWifiClient.SSID, CurWifiClient.Key);
				addWifiClientWnd.ShowDialog();
				if (addWifiClientWnd.Result)
				{
					CurWifiClient.SSID = addWifiClientWnd.SSID;
					CurWifiClient.Key = addWifiClientWnd.Key;
				}
			}
		}

		private void OnUpClient(object obj)
		{
			if (CurWifiClient != null)
			{
				int num = WifiClientList.IndexOf(CurWifiClient);
				if (num > 0)
				{
					WifiClientList.Move(num, num - 1);
				}
			}
		}

		private void OnDownClient(object obj)
		{
			if (CurWifiClient != null)
			{
				int num = WifiClientList.IndexOf(CurWifiClient);
				if (num < WifiClientList.Count - 1)
				{
					WifiClientList.Move(num, num + 1);
				}
			}
		}

		private void OnSelectClient(object obj)
		{
			if (CurWifiClient != null && Instance != null)
			{
				SdkInterface.SetAttr(Instance.ID, 4511, WifiClientList.IndexOf(CurWifiClient));
				UpdateSelectedClient(CurWifiClient);
			}
		}

		private void InitParam()
		{
			InitAP();
			SetCurrentAP();
			InitClient();
			InitWifiStatus();
		}

		private void ResetClientAP()
		{
			SetCurrentAP();
			InitClient();
		}

		private void SetCurrentAP()
		{
			if (Instance != null)
			{
				APModeEnable = ((Instance.Prop_Attr_Wifi_AP_ApModeEn > 0) ? true : false);
				ClientModeEnable = !APModeEnable;
				WifiDHCPServer = ((Instance.Prop_Attr_Wifi_AP_DhcpServerEn > 0) ? true : false);
				CurWifiSecurity = SDKEnumCollector.GetItem(WifiSecurityList, Instance.Prop_Attr_Wifi_AP_SecuritySel);
				CurWifiFreq = SDKEnumCollector.GetItem(WifiFreqList, Instance.Prop_Attr_Wifi_AP_FrequencySel);
				CurWifiCountry = SDKEnumCollector.GetItem(WifiCountryList, Instance.Prop_Attr_Wifi_AP_CountryCode);
				CurWifiBand = SDKEnumCollector.GetItem(WifiBandList, Instance.Prop_Attr_Wifi_AP_BandWidthSel);
				WifiSSID = Instance.Prop_Attr_Wifi_AP_SSID;
				WifiKey = Instance.Prop_Attr_Wifi_AP_Key;
			}
		}

		private void InitAP()
		{
			if (Instance != null)
			{
				WifiSecurityList = SDKEnumCollector.LoadParam("Enm_Wifi_Security");
				WifiFreqList = SDKEnumCollector.LoadParam("Enm_Wifi_Frequency");
				WifiCountryList = SDKEnumCollector.LoadParam("Enm_Wifi_CountryCode");
				WifiBandList = SDKEnumCollector.LoadParam("Enm_Wifi_BandWidth");
				WifiChannelList = new ObservableCollection<EnumDisplayItem>();
				TempWifiChannelList = SDKEnumCollector.LoadParam("Enm_Wifi_Channel");
			}
		}

		private void FilterWifiChanelList()
		{
			if (CurWifiCountry == null || CurWifiFreq == null || CurWifiBand == null || WifiChannelList == null)
			{
				return;
			}
			int[] validChannelList = WifiChannelTable.GetValidChannelList((Enm_Wifi_CountryCode)CurWifiCountry.Val, (Enm_Wifi_Frequency)CurWifiFreq.Val, (Enm_Wifi_BandWidth)CurWifiBand.Val);
			if (validChannelList != null)
			{
				WifiChannelList.Clear();
				foreach (EnumDisplayItem tempWifiChannel in TempWifiChannelList)
				{
					if (validChannelList.Contains(tempWifiChannel.Val))
					{
						WifiChannelList.Add(tempWifiChannel);
					}
				}
				if (WifiChannelList.Count > 0)
				{
					CurWifiChannel = SDKEnumCollector.GetItem(WifiChannelList, Instance.Prop_Attr_Wifi_AP_ChannelSel);
				}
			}
		}

		private void InitClient()
		{
			if (Instance == null)
			{
				return;
			}
			if (WifiClientList == null)
			{
				WifiClientList = new ObservableCollection<WifiClientParam>();
			}
			else
			{
				WifiClientList.Clear();
			}
			WifiClient.LoadParam(WifiClientList, Instance);
			if (Instance.Prop_Attr_Wifi_Client_CurrentSel < WifiClientList.Count)
			{
				CurWifiClient = WifiClientList[Instance.Prop_Attr_Wifi_Client_CurrentSel];
				CurWifiClient.SelectedFlag = true;
				LastSelectClient = CurWifiClient;
			}
			if (CurWifiClient != null)
			{
				if (UsingClient == null)
				{
					UsingClient = new WifiClientParam();
				}
				UsingClient.SSID = CurWifiClient.SSID;
				UsingClient.Key = CurWifiClient.Key;
			}
		}

		private void InitWifiStatus()
		{
			if (Instance != null)
			{
				ObservableCollection<EnumDisplayItem> itemList = SDKEnumCollector.LoadParam("Enm_NetworkInterface");
				EnumDisplayItem item = SDKEnumCollector.GetItem(itemList, Instance.Prop_Attr_NetworkInterface);
				string text = item.Label;
				int num = item.Label.LastIndexOf("_") + 1;
				if (num < item.Label.Length)
				{
					text = item.Label.Remove(0, num);
				}
				WifiStatus = "Interface:\t" + text + "\r\nLinkedAP:\t" + Instance.Prop_Attr_WifiStatu_LinkedAP + "\r\nBand:\t\t" + Instance.Prop_Attr_WifiStatu_WorkingBand.ToString() + "\r\nSignalIntensity:\t" + Instance.Prop_Attr_WifiStatu_WorkingSignalIntensity.ToString() + "\r\nLinkQuality:\t" + Instance.Prop_Attr_WifiStatu_WorkingLinkQuality.ToString() + "\r\nTxPower:\t" + Instance.Prop_Attr_WifiStatu_WorkingTxPower.ToString();
			}
		}

		private int WriteWifiParams()
		{
			if (Instance == null)
			{
				return 6;
			}
			int num = 0;
			List<APWriteParams> list = new List<APWriteParams>();
			try
			{
				list.Add(new APWriteParams
				{
					AttrWriteID = 4508,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = (APModeEnable ? 1 : 0)
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4501,
					type = IRAY_VAR_TYPE.IVT_STR,
					strValue = WifiSSID
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4502,
					type = IRAY_VAR_TYPE.IVT_STR,
					strValue = WifiKey
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4507,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = CurWifiSecurity.Val
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4504,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = CurWifiFreq.Val
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4503,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = CurWifiCountry.Val
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4505,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = CurWifiBand.Val
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4506,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = CurWifiChannel.Val
				});
				list.Add(new APWriteParams
				{
					AttrWriteID = 4509,
					type = IRAY_VAR_TYPE.IVT_INT,
					nValue = (WifiDHCPServer ? 1 : 0)
				});
			}
			catch (Exception)
			{
				return 20;
			}
			foreach (APWriteParams item in list)
			{
				if (item.type == IRAY_VAR_TYPE.IVT_INT)
				{
					num = SdkInterface.SetAttr(Instance.ID, item.AttrWriteID, item.nValue);
				}
				else if (IRAY_VAR_TYPE.IVT_STR == item.type)
				{
					num = SdkInterface.SetAttr(Instance.ID, item.AttrWriteID, item.strValue);
				}
				if (num != 0)
				{
					return num;
				}
			}
			int num2 = 4512;
			int num3 = 4513;
			SdkInterface.SetAttr(Instance.ID, 4510, WifiClientList.Count);
			foreach (WifiClientParam wifiClient in WifiClientList)
			{
				num = SdkInterface.SetAttr(Instance.ID, num2, wifiClient.SSID);
				if (num != 0)
				{
					return num;
				}
				num2 += 2;
				num = SdkInterface.SetAttr(Instance.ID, num3, wifiClient.Key);
				num3 += 2;
				if (num != 0)
				{
					return num;
				}
			}
			return num;
		}

		private void ParseWirelessNetworkInfo(string xmlData)
		{
			if (xmlData != null)
			{
				XmlParser xmlParser = new XmlParser();
				xmlParser.LoadXml(xmlData);
				List<string> nodes = xmlParser.GetNodes("WifiScanList/item/ssid");
				List<string> nodes2 = xmlParser.GetNodes("WifiScanList/item/signalLevel");
				WifiNetWorkList.Clear();
				for (int i = 0; i < nodes.Count && i < nodes2.Count; i++)
				{
					WifiNetWorkList.Add(new WirelessNetwork
					{
						SSID = nodes[i],
						SignalLevel = nodes2[i]
					});
				}
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			UIUser.Dispatcher.Invoke((Action)delegate
			{
				switch (nEventID)
				{
				case 4:
					switch (nParam1)
					{
					case 2022:
					case 2023:
						break;
					case 2024:
						ResetClientAP();
						break;
					case 2021:
						InitWifiStatus();
						break;
					}
					break;
				case 13:
				{
					string xmlData = Marshal.PtrToStringAuto(pParam);
					ParseWirelessNetworkInfo(xmlData);
					break;
				}
				}
			}, new object[0]);
		}
	}
}
