namespace iDetector
{
	public class EnumDisplayItem
	{
		private int _nVal;

		private string _strLabel;

		public int Val
		{
			get
			{
				return _nVal;
			}
			set
			{
				_nVal = value;
			}
		}

		public string Label
		{
			get
			{
				return _strLabel;
			}
			set
			{
				_strLabel = value;
			}
		}

		public EnumDisplayItem()
		{
			_nVal = 0;
			_strLabel = "";
		}
	}
}
