using System.ComponentModel;

namespace iDetector
{
	internal class CineFrame : INotifyPropertyChanged
	{
		private int TotalSaveCount;

		private string _saveImageProgress;

		private int _frameCount;

		public string SaveImageProgress
		{
			get
			{
				return _saveImageProgress;
			}
			set
			{
				_saveImageProgress = value;
				if (this.PropertyChanged != null)
				{
					this.PropertyChanged(this, new PropertyChangedEventArgs("SaveImageProgress"));
				}
			}
		}

		public int FrameCount
		{
			get
			{
				return _frameCount;
			}
			set
			{
				_frameCount = value;
				SaveImageProgress = string.Format("{0}/{1}", value, TotalSaveCount);
			}
		}

		public bool EnableUpdate
		{
			get;
			set;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void SetSaveNumber(int totalCount)
		{
			TotalSaveCount = totalCount;
		}
	}
}
