namespace iDetector
{
	public struct DetecInfo
	{
		public SDKInvokeProxy InvokeProxy;

		public Detector Detector;

		public Shell Shell;

		public DetecInfo(Detector instance, Shell shell)
		{
			Detector = instance;
			Shell = shell;
			InvokeProxy = new SDKInvokeProxy(Detector, shell);
		}
	}
}
