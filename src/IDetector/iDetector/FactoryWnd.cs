using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Markup;

namespace iDetector
{
	public class FactoryWnd : Window, IComponentConnector
	{
		private FpdMgr DetMgr;

		private Detector Instance;

		private int DetectorID;

		private DlgTaskTimeOut MessageBoxTimer;

		private int TaskTimeoutSeconds = 10;

		private UITimer timer;

		private string workPath;

		internal FactoryPage xFactoryPage;

		private bool _contentLoaded;

		public FactoryWnd(string DirPath)
		{
			InitializeComponent();
			workPath = DirPath;
			base.Loaded += OnLoad;
			base.ShowInTaskbar = false;
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				DetMgr = new FpdMgr();
				int num = DetMgr.Create(workPath, ref DetectorID);
				if (num == 0)
				{
					Instance = DetMgr.FindDetector(DetectorID);
					xFactoryPage.Instance = Instance;
				}
				else
				{
					MessageBox.Show(this, "Create instance error:" + ErrorHelper.GetErrorDesp(num));
				}
				CloseMessageBox();
				timer = new UITimer(1000, UpdateAttr);
				timer.Start();
				DetMgr.DoCheckAttrValChanging();
			});
			CreateMessageBox("Initilize", "Initilize factory params...");
		}

		private void CreateMessageBox(string title, string content)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TaskTimeoutSeconds, this);
			MessageBoxTimer.RunTask();
		}

		private void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		private void UpdateAttr()
		{
			DetMgr.DoCheckAttrValChanging();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (timer != null)
			{
				timer.Stop();
			}
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				DetMgr.Destroy(DetectorID);
				CloseMessageBox();
			});
			CreateMessageBox("Close", "Destroy factory window...");
			base.OnClosing(e);
		}

		protected override void OnClosed(EventArgs e)
		{
			xFactoryPage.OnClose();
			base.OnClosed(e);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/factorywnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xFactoryPage = (FactoryPage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
