using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Windows;

namespace iDetector
{
	internal class LocalImagesVM : NotifyObject
	{
		public DelegateCommand QueryImages
		{
			get;
			set;
		}

		public DelegateCommand UploadImages
		{
			get;
			set;
		}

		public DelegateCommand StopUpload
		{
			get;
			set;
		}

		public ObservableCollection<ImageItem> LocalImageList
		{
			get;
			set;
		}

		private FrameworkElement UIUser
		{
			get;
			set;
		}

		private Detector Instance
		{
			get;
			set;
		}

		private SDKInvokeProxy sdkInvokeProxy
		{
			get;
			set;
		}

		public LocalImagesVM(FrameworkElement uiUser, Detector detector)
		{
			Instance = detector;
			UIUser = uiUser;
			QueryImages = new DelegateCommand(OnQueryImages);
			UploadImages = new DelegateCommand(OnUploadImages);
			StopUpload = new DelegateCommand(OnStopUpload);
			sdkInvokeProxy = new SDKInvokeProxy(Instance, new Shell(Application.Current.MainWindow));
			LocalImageList = new ObservableCollection<ImageItem>();
		}

		public void RegisterCallback()
		{
			Instance.SdkCallbackEvent += OnSdkCallback;
		}

		public void UnRegisterCallback()
		{
			Instance.SdkCallbackEvent -= OnSdkCallback;
		}

		private void OnQueryImages(object obj)
		{
			sdkInvokeProxy.Invoke(2026);
		}

		private void OnUploadImages(object obj)
		{
			UploadImageRangeWnd uploadImageRangeWnd = new UploadImageRangeWnd();
			if (uploadImageRangeWnd.OnShowDialog())
			{
				sdkInvokeProxy.Invoke(2028, uploadImageRangeWnd.StartIndex, uploadImageRangeWnd.EndIndex);
			}
		}

		private void OnStopUpload(object obj)
		{
			sdkInvokeProxy.Invoke(2029);
		}

		private void UpdateImageList(string xmlData)
		{
			if (xmlData == null)
			{
				Log.Instance().Write("UpdateImageList data is null!");
				return;
			}
			XmlParser xmlParser = new XmlParser();
			xmlParser.LoadXml(xmlData);
			List<string> nodes = xmlParser.GetNodes("HistoricalImageList/item/index");
			List<string> nodes2 = xmlParser.GetNodes("HistoricalImageList/item/fileName");
			List<string> nodes3 = xmlParser.GetNodes("HistoricalImageList/item/fileTime");
			List<string> nodes4 = xmlParser.GetNodes("HistoricalImageList/item/delayTime");
			List<string> nodes5 = xmlParser.GetNodes("HistoricalImageList/item/imageAttr");
			int val = Math.Min(Math.Min(nodes.Count, nodes2.Count), Math.Min(nodes3.Count, nodes4.Count));
			val = Math.Min(val, nodes5.Count);
			LocalImageList.Clear();
			for (int i = 0; i < val; i++)
			{
				LocalImageList.Add(new ImageItem
				{
					Index = nodes[i],
					FileName = nodes2[i],
					CreateTime = nodes3[i],
					DelayTime = nodes4[i],
					ImageAttr = string.Format("0x{0}", Convert.ToInt32(nodes5[i]).ToString("x8"))
				});
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			UIUser.Dispatcher.Invoke((Action)delegate
			{
				switch (nEventID)
				{
				case 14:
				{
					string xmlData = Marshal.PtrToStringAuto(pParam);
					UpdateImageList(xmlData);
					break;
				}
				case 9:
					foreach (ImageItem localImage in LocalImageList)
					{
						if (Convert.ToInt32(localImage.Index) == nParam1)
						{
							localImage.UploadStatus = ((nParam2 == 0) ? "success" : "failed");
						}
					}
					break;
				}
			}, new object[0]);
		}
	}
}
