using iDetector.PopupWnds;
using IMG.Process;
using PlugInInterface;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace iDetector
{
	public class AcquirePage : UserControl, IDetSubPage, IComponentConnector
	{
		private const string verifyHWPlugInName = "VerifyHWCorrection";

		private DlgTaskTimeOut MessageBoxTimer;

		private int TaskTimeoutSeconds = 15;

		private CineFrame SavedFrames;

		private StatusControl StatusControler;

		private int ThumbsNumber = 5;

		private int nFactoryAuthority;

		private OQCSeqSaveParam oqcSeqSaveParam;

		private Detector instance;

		private SDKInvokeProxy SdkInvokeProxy;

		public ToolTipMsgDel ShowToolTip;

		private PlugInsManager mPlugin;

		private IPlugIn verifyHWPlugIn;

		private object VerifyHWComp;

		private IRayImageData CurImage;

		private bool CountLabelVisible;

		private SeqSaveSetWnd mSeqSaveSet;

		private SeqSaveSetSave mSeqSaveSetBackup;

		private string[] templateStateInfo = new string[4]
		{
			"absent",
			"validity",
			"validity",
			"unmatched"
		};

		internal DockPanel xUIPanel;

		internal CheckBox offsetCB;

		internal RadioButton SWPreOffsetRB;

		internal RadioButton HWPreOffsetRB;

		internal RadioButton SWPostOffsetRB;

		internal RadioButton HWPostOffsetRB;

		internal CheckBox gainCB;

		internal RadioButton SWGainRB;

		internal RadioButton HWGainRB;

		internal CheckBox defectCB;

		internal RadioButton SWDefectRB;

		internal RadioButton HWDefectRB;

		internal StackPanel xOpButton;

		internal Button ClearBtn;

		internal Button SingleAcqBtn;

		internal Button PrepAcqBtn;

		internal Button Acq2Btn;

		internal Button AecPreExpBtn;

		internal Button StartAcqBtn;

		internal Button StopAcqBtn;

		internal Button SleepBtn;

		internal Button WackupBtn;

		internal Button SaveBtn;

		internal Button FreeSeqAcq;

		internal Button SeqAcq;

		internal Button SeqSaveSet;

		internal Button OQCSeqSave;

		internal Button ActiveSensorBtn;

		internal Button EnableOutExpBtn;

		internal Button ProhibitOutExpBtn;

		internal Button StartSelfCapBtn;

		internal Button StopSelfCapBtn;

		internal Button PowerOffBtn;

		internal Label SaveFileCountLabel;

		internal CtrImagePresent ImageViewCtrl;

		private bool _contentLoaded;

		public bool bHandled
		{
			get;
			set;
		}

		public bool IsAcquiring
		{
			get;
			set;
		}

		private Enm_FPDSleepStatus FpdStatus
		{
			get;
			set;
		}

		public virtual Detector Instance
		{
			get
			{
				return instance;
			}
			set
			{
				instance = value;
			}
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		public event Action StartExposeWindow;

		public event Action StopExposeWindow;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public AcquirePage()
		{
			InitializeComponent();
		}

		protected override void OnInitialized(EventArgs e)
		{
			base.OnInitialized(e);
			IsAcquiring = false;
			FpdStatus = Enm_FPDSleepStatus.Wakeup;
			SdkInterface.GetAuthority(ref nFactoryAuthority);
			SetMultiBinding();
			CurImage = new IRayImageData();
			oqcSeqSaveParam.EnableSave = false;
			CountLabelVisible = false;
			base.IsVisibleChanged += VisibleChanged;
		}

		private void AddDebugPlugin()
		{
			if ((0x2000 & nFactoryAuthority) != 0)
			{
				mPlugin = PlugInsManager.Creator();
				VerifyHWComp = mPlugin.GetPlugElementByClassName("VerifyHWCorrection");
				if (VerifyHWComp != null && VerifyHWComp is FrameworkElement)
				{
					verifyHWPlugIn = mPlugin.GetPlugObjByClassName("VerifyHWCorrection");
				}
			}
			if (verifyHWPlugIn != null && verifyHWPlugIn.InitDetectorInstance(instance) && !xOpButton.Children.Contains(VerifyHWComp as FrameworkElement))
			{
				xOpButton.Children.Add(VerifyHWComp as FrameworkElement);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (DesignerProperties.GetIsInDesignMode(this))
			{
				return;
			}
			if (!(bool)e.NewValue)
			{
				ImageViewCtrl.HidePlayBar();
				return;
			}
			ResetCorrectionRadioStatus();
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
				PrepAcqBtn.Visibility = ((1 == Instance.Prop_Attr_UROM_FluroSync) ? Visibility.Collapsed : Visibility.Visible);
				break;
			case 46:
			case 87:
				PrepAcqBtn.Visibility = Visibility.Collapsed;
				break;
			case 48:
				if (1 == Instance.Prop_Attr_UROM_DynamicFlag)
				{
					PrepAcqBtn.Visibility = ((1 == Instance.Prop_Attr_UROM_FluroSync) ? Visibility.Collapsed : Visibility.Visible);
				}
				else
				{
					PrepAcqBtn.Visibility = ((1 != Instance.Prop_Attr_UROM_ExpMode) ? Visibility.Collapsed : Visibility.Visible);
				}
				break;
			case 11:
				MammoButtonControl();
				break;
			case 45:
				if (3 == Instance.Prop_Attr_UROM_TriggerMode && 1 == Instance.Prop_Attr_UROM_PrepCapMode)
				{
					ClearBtn.Visibility = Visibility.Collapsed;
					SingleAcqBtn.Visibility = Visibility.Collapsed;
					PrepAcqBtn.Visibility = Visibility.Collapsed;
					StartAcqBtn.Visibility = Visibility.Collapsed;
					StartSelfCapBtn.Visibility = Visibility.Visible;
					StopSelfCapBtn.Visibility = Visibility.Visible;
					PowerOffBtn.Visibility = Visibility.Visible;
				}
				else
				{
					ClearBtn.Visibility = Visibility.Visible;
					SingleAcqBtn.Visibility = Visibility.Visible;
					PrepAcqBtn.Visibility = Visibility.Visible;
					StartAcqBtn.Visibility = Visibility.Visible;
					StartSelfCapBtn.Visibility = Visibility.Collapsed;
					StopSelfCapBtn.Visibility = Visibility.Collapsed;
					PowerOffBtn.Visibility = Visibility.Collapsed;
				}
				break;
			case 72:
			case 91:
				ClearBtn.Visibility = Visibility.Visible;
				SingleAcqBtn.Visibility = Visibility.Visible;
				PrepAcqBtn.Visibility = Visibility.Visible;
				StartAcqBtn.Visibility = Visibility.Visible;
				StartSelfCapBtn.Visibility = Visibility.Collapsed;
				StopSelfCapBtn.Visibility = Visibility.Collapsed;
				PowerOffBtn.Visibility = Visibility.Visible;
				Acq2Btn.Visibility = Visibility.Collapsed;
				AecPreExpBtn.Visibility = Visibility.Collapsed;
				EnableOutExpBtn.Visibility = Visibility.Visible;
				ProhibitOutExpBtn.Visibility = Visibility.Visible;
				ActiveSensorBtn.Visibility = Visibility.Collapsed;
				break;
			case 80:
				ClearBtn.Visibility = Visibility.Visible;
				SingleAcqBtn.Visibility = Visibility.Visible;
				PrepAcqBtn.Visibility = Visibility.Visible;
				StartAcqBtn.Visibility = Visibility.Visible;
				StartSelfCapBtn.Visibility = Visibility.Collapsed;
				StopSelfCapBtn.Visibility = Visibility.Collapsed;
				PowerOffBtn.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Collapsed;
				AecPreExpBtn.Visibility = Visibility.Collapsed;
				EnableOutExpBtn.Visibility = Visibility.Collapsed;
				ProhibitOutExpBtn.Visibility = Visibility.Collapsed;
				ActiveSensorBtn.Visibility = Visibility.Collapsed;
				break;
			case 42:
			case 93:
				ClearBtn.Visibility = Visibility.Visible;
				SingleAcqBtn.Visibility = Visibility.Visible;
				PrepAcqBtn.Visibility = Visibility.Visible;
				StartAcqBtn.Visibility = Visibility.Visible;
				StartSelfCapBtn.Visibility = Visibility.Collapsed;
				StopSelfCapBtn.Visibility = Visibility.Collapsed;
				PowerOffBtn.Visibility = Visibility.Visible;
				Acq2Btn.Visibility = Visibility.Collapsed;
				AecPreExpBtn.Visibility = Visibility.Collapsed;
				EnableOutExpBtn.Visibility = Visibility.Collapsed;
				ProhibitOutExpBtn.Visibility = Visibility.Collapsed;
				ActiveSensorBtn.Visibility = Visibility.Collapsed;
				break;
			default:
				if (2 == Instance.Prop_Attr_UROM_TriggerMode)
				{
					ClearBtn.Visibility = Visibility.Visible;
				}
				else
				{
					ClearBtn.Visibility = Visibility.Collapsed;
				}
				if (3 == Instance.Prop_Attr_UROM_TriggerMode && 1 == Instance.Prop_Attr_UROM_PrepCapMode)
				{
					PrepAcqBtn.Visibility = Visibility.Collapsed;
					StartAcqBtn.Visibility = Visibility.Collapsed;
					Acq2Btn.Visibility = Visibility.Visible;
					switch (Instance.Prop_Attr_UROM_ProductNo)
					{
					case 41:
					case 61:
						StartSelfCapBtn.Visibility = Visibility.Visible;
						StopSelfCapBtn.Visibility = Visibility.Visible;
						PowerOffBtn.Visibility = Visibility.Visible;
						SleepBtn.Visibility = Visibility.Visible;
						WackupBtn.Visibility = Visibility.Visible;
						EnableOutExpBtn.Visibility = Visibility.Visible;
						ProhibitOutExpBtn.Visibility = Visibility.Visible;
						break;
					case 60:
					case 88:
						StartSelfCapBtn.Visibility = Visibility.Visible;
						StopSelfCapBtn.Visibility = Visibility.Visible;
						break;
					case 32:
					case 37:
					case 59:
					case 62:
						AecPreExpBtn.Visibility = Visibility.Visible;
						break;
					}
				}
				else
				{
					if (41 == Instance.Prop_Attr_UROM_ProductNo)
					{
						SleepBtn.Visibility = Visibility.Collapsed;
						WackupBtn.Visibility = Visibility.Collapsed;
					}
					PrepAcqBtn.Visibility = Visibility.Visible;
					StartAcqBtn.Visibility = Visibility.Visible;
					Acq2Btn.Visibility = Visibility.Collapsed;
					AecPreExpBtn.Visibility = Visibility.Collapsed;
					StartSelfCapBtn.Visibility = Visibility.Collapsed;
					StopSelfCapBtn.Visibility = Visibility.Collapsed;
					PowerOffBtn.Visibility = Visibility.Collapsed;
					EnableOutExpBtn.Visibility = Visibility.Collapsed;
					ProhibitOutExpBtn.Visibility = Visibility.Collapsed;
					ActiveSensorBtn.Visibility = Visibility.Collapsed;
				}
				break;
			}
			if (!StatusControler.IsSequenceAcq)
			{
				ImageViewCtrl.EnableUpdateImageList(true);
			}
			ImageViewCtrl.InitFPS(Instance.Prop_Attr_UROM_SequenceIntervalTime);
		}

		private void InitDynamicDetPanel()
		{
			AddDebugPlugin();
			ClearBtn.Visibility = Visibility.Collapsed;
			HWPostOffsetRB.Visibility = Visibility.Collapsed;
			SleepBtn.Visibility = Visibility.Collapsed;
			WackupBtn.Visibility = Visibility.Collapsed;
			SingleAcqBtn.Visibility = Visibility.Collapsed;
			SeqAcq.Visibility = Visibility.Collapsed;
			PrepAcqBtn.Visibility = ((1 == Instance.Prop_Attr_UROM_FluroSync) ? Visibility.Collapsed : Visibility.Visible);
			StatusControler = new DynamicStatusControl();
			if ((0x2000 & Instance.Prop_Attr_Authority) != 0)
			{
				FreeSeqAcq.Visibility = Visibility.Visible;
			}
		}

		private void InitPanel()
		{
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 49:
			case 50:
			case 56:
				InitDynamicDetPanel();
				break;
			case 48:
				InitDynamicDetPanel();
				HWPostOffsetRB.Visibility = Visibility.Visible;
				HWPostOffsetRB.IsEnabled = false;
				break;
			case 46:
			case 68:
			case 73:
			case 74:
			case 87:
				InitDynamicDetPanel();
				HWPreOffsetRB.Visibility = Visibility.Collapsed;
				HWGainRB.Visibility = Visibility.Collapsed;
				HWDefectRB.Visibility = Visibility.Collapsed;
				break;
			case 32:
			case 37:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				AecPreExpBtn.Visibility = Visibility.Visible;
				SleepBtn.Visibility = Visibility.Visible;
				WackupBtn.Visibility = Visibility.Visible;
				StatusControler = new StaticStatusControl();
				break;
			case 59:
			case 62:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				AecPreExpBtn.Visibility = Visibility.Visible;
				SleepBtn.Visibility = Visibility.Visible;
				WackupBtn.Visibility = Visibility.Visible;
				StatusControler = new StaticStatusControl();
				break;
			case 39:
			case 41:
			case 51:
			case 52:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				StatusControler = new StaticStatusControl();
				break;
			case 61:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				StatusControler = new StaticStatusControl();
				SleepBtn.Visibility = Visibility.Visible;
				WackupBtn.Visibility = Visibility.Visible;
				break;
			case 60:
			case 88:
				SleepBtn.Visibility = Visibility.Collapsed;
				WackupBtn.Visibility = Visibility.Collapsed;
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				StatusControler = new StaticStatusControl();
				break;
			case 45:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				StatusControler = new StaticStatusControl();
				SleepBtn.Visibility = Visibility.Visible;
				WackupBtn.Visibility = Visibility.Visible;
				EnableOutExpBtn.Visibility = Visibility.Visible;
				ProhibitOutExpBtn.Visibility = Visibility.Visible;
				ActiveSensorBtn.Visibility = Visibility.Visible;
				break;
			case 38:
				HWPreOffsetRB.Visibility = Visibility.Collapsed;
				HWPostOffsetRB.Visibility = Visibility.Collapsed;
				HWDefectRB.Visibility = Visibility.Collapsed;
				HWGainRB.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Visible;
				SleepBtn.Visibility = Visibility.Collapsed;
				WackupBtn.Visibility = Visibility.Collapsed;
				SeqSaveSet.Visibility = Visibility.Collapsed;
				StatusControler = new StaticStatusControl();
				if ((0x2000 & nFactoryAuthority) != 0)
				{
					EnableOutExpBtn.Visibility = Visibility.Visible;
					ProhibitOutExpBtn.Visibility = Visibility.Visible;
					SetExpButtonStateBinding();
				}
				break;
			case 11:
				SWPreOffsetRB.Visibility = Visibility.Collapsed;
				HWPreOffsetRB.Visibility = Visibility.Collapsed;
				HWDefectRB.Visibility = Visibility.Collapsed;
				HWGainRB.Visibility = Visibility.Collapsed;
				SleepBtn.Visibility = Visibility.Collapsed;
				WackupBtn.Visibility = Visibility.Collapsed;
				SeqSaveSet.Visibility = Visibility.Collapsed;
				MammoButtonControl();
				StatusControler = new StaticStatusControl();
				break;
			case 58:
				ClearBtn.Visibility = Visibility.Collapsed;
				HWPreOffsetRB.Visibility = Visibility.Collapsed;
				HWPostOffsetRB.Visibility = Visibility.Collapsed;
				HWDefectRB.Visibility = Visibility.Collapsed;
				HWGainRB.Visibility = Visibility.Collapsed;
				SleepBtn.Visibility = Visibility.Collapsed;
				WackupBtn.Visibility = Visibility.Collapsed;
				SeqSaveSet.Visibility = Visibility.Visible;
				SeqAcq.Visibility = Visibility.Visible;
				StopAcqBtn.Visibility = Visibility.Visible;
				StatusControler = new DynamicStatusControl();
				if ((0x2000 & nFactoryAuthority) != 0)
				{
					EnableOutExpBtn.Visibility = Visibility.Visible;
					ProhibitOutExpBtn.Visibility = Visibility.Visible;
					SetExpButtonStateBinding();
				}
				break;
			case 72:
			case 80:
			case 91:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Collapsed;
				StatusControler = new StaticStatusControl();
				SleepBtn.Visibility = Visibility.Collapsed;
				WackupBtn.Visibility = Visibility.Collapsed;
				EnableOutExpBtn.Visibility = Visibility.Visible;
				ProhibitOutExpBtn.Visibility = Visibility.Visible;
				ActiveSensorBtn.Visibility = Visibility.Collapsed;
				HWPreOffsetRB.Visibility = Visibility.Collapsed;
				SWPreOffsetRB.Visibility = Visibility.Collapsed;
				SWPostOffsetRB.Visibility = Visibility.Collapsed;
				SWDefectRB.Visibility = Visibility.Collapsed;
				SWGainRB.Visibility = Visibility.Collapsed;
				break;
			case 42:
			case 93:
				SeqSaveSet.Visibility = Visibility.Collapsed;
				Acq2Btn.Visibility = Visibility.Collapsed;
				StatusControler = new StaticStatusControl();
				SleepBtn.Visibility = Visibility.Visible;
				WackupBtn.Visibility = Visibility.Visible;
				EnableOutExpBtn.Visibility = Visibility.Collapsed;
				ProhibitOutExpBtn.Visibility = Visibility.Collapsed;
				break;
			default:
				HWPreOffsetRB.Visibility = Visibility.Collapsed;
				HWPostOffsetRB.Visibility = Visibility.Collapsed;
				HWDefectRB.Visibility = Visibility.Collapsed;
				HWGainRB.Visibility = Visibility.Collapsed;
				SleepBtn.Visibility = Visibility.Collapsed;
				WackupBtn.Visibility = Visibility.Collapsed;
				SeqSaveSet.Visibility = Visibility.Collapsed;
				StatusControler = new StaticStatusControl();
				break;
			}
			if ((0x2000 & nFactoryAuthority) == 0)
			{
				SeqAcq.Visibility = Visibility.Collapsed;
				OQCSeqSave.Visibility = Visibility.Collapsed;
				SingleAcqBtn.Visibility = Visibility.Collapsed;
				switch (Instance.Prop_Attr_UROM_ProductNo)
				{
				case 6:
				case 46:
				case 48:
				case 49:
				case 50:
				case 56:
				case 58:
				case 68:
				case 73:
				case 74:
				case 87:
					return;
				}
				StopAcqBtn.Visibility = Visibility.Collapsed;
			}
		}

		public void InitImageParam()
		{
			Instance.SubscribeCBEvent(OnSdkCallback);
			SdkInvokeProxy = new SDKInvokeProxy(Instance, new Shell(Application.Current.MainWindow));
			this.SdkEventHandler = ProcessEvent;
			ThumbsNumber = (int)IniParser.GetPrivateProfileInt("Thumb", "ThumbImageNumber", 5, instance.Prop_Attr_WorkDir + "iDetectorConfig.ini");
			ImageViewCtrl.SetImageBuffer(ThumbsNumber, Instance.Prop_Attr_Prod_FullWidth, Instance.Prop_Attr_Prod_FullHeight);
			ImageViewCtrl.SetImageSize(Instance.Prop_Attr_Width, Instance.Prop_Attr_Height);
			ImageViewCtrl.InitFPS(Instance.Prop_Attr_UROM_SequenceIntervalTime);
			int privateProfileInt = (int)IniParser.GetPrivateProfileInt("DisplaySetting", "EnableFullSizeDisplayInSeqAcq", 0, instance.Prop_Attr_WorkDir + "iDetectorConfig.ini");
			ImageViewCtrl.EnableFullSizeDisplayInSeqAcq((privateProfileInt != 0) ? true : false);
			InitPanel();
			EnableAllUIElement(true);
			ResetCorrectionRadioStatus();
			ImageViewCtrl.EnableUpdateImageList(true);
			privateProfileInt = (int)IniParser.GetPrivateProfileInt("DisplaySetting", "EnableAutoAdjustWWWL", 0, instance.Prop_Attr_WorkDir + "iDetectorConfig.ini");
			ImageViewCtrl.StartTimer((privateProfileInt != 0) ? true : false);
		}

		private void SetMultiBinding()
		{
			Binding binding = new Binding("IsChecked");
			binding.ElementName = "SWPreOffsetRB";
			Binding item = binding;
			Binding binding2 = new Binding("IsChecked");
			binding2.ElementName = "SWPostOffsetRB";
			Binding item2 = binding2;
			Binding binding3 = new Binding("IsChecked");
			binding3.ElementName = "HWPreOffsetRB";
			Binding item3 = binding3;
			Binding binding4 = new Binding("IsChecked");
			binding4.ElementName = "HWPostOffsetRB";
			Binding item4 = binding4;
			MultiBinding multiBinding = new MultiBinding();
			multiBinding.Mode = BindingMode.OneWay;
			MultiBinding multiBinding2 = multiBinding;
			multiBinding2.Bindings.Add(item);
			multiBinding2.Bindings.Add(item2);
			multiBinding2.Bindings.Add(item3);
			multiBinding2.Bindings.Add(item4);
			multiBinding2.Converter = new CheckButtonCheckedConvert();
			offsetCB.SetBinding(ToggleButton.IsCheckedProperty, multiBinding2);
			Binding binding5 = new Binding("IsChecked");
			binding5.ElementName = "SWGainRB";
			Binding item5 = binding5;
			Binding binding6 = new Binding("IsChecked");
			binding6.ElementName = "HWGainRB";
			Binding item6 = binding6;
			MultiBinding multiBinding3 = new MultiBinding();
			multiBinding3.Mode = BindingMode.OneWay;
			MultiBinding multiBinding4 = multiBinding3;
			multiBinding4.Bindings.Add(item5);
			multiBinding4.Bindings.Add(item6);
			multiBinding4.Converter = new CheckButtonCheckedConvert();
			gainCB.SetBinding(ToggleButton.IsCheckedProperty, multiBinding4);
			Binding binding7 = new Binding("IsChecked");
			binding7.ElementName = "SWDefectRB";
			Binding item7 = binding7;
			Binding binding8 = new Binding("IsChecked");
			binding8.ElementName = "HWDefectRB";
			Binding item8 = binding8;
			MultiBinding multiBinding5 = new MultiBinding();
			multiBinding5.Mode = BindingMode.OneWay;
			MultiBinding multiBinding6 = multiBinding5;
			multiBinding6.Bindings.Add(item7);
			multiBinding6.Bindings.Add(item8);
			multiBinding6.Converter = new CheckButtonCheckedConvert();
			defectCB.SetBinding(ToggleButton.IsCheckedProperty, multiBinding6);
			Binding binding9 = new Binding("IsChecked");
			binding9.ElementName = "offsetCB";
			Binding binding10 = new Binding("IsEnabled");
			binding10.ElementName = "offsetCB";
			Binding item9 = binding10;
			MultiBinding multiBinding7 = new MultiBinding();
			multiBinding7.Mode = BindingMode.OneWay;
			MultiBinding multiBinding8 = multiBinding7;
			multiBinding8.Bindings.Add(item9);
			multiBinding8.Converter = new RadioButtonEnableConvert();
			SWPreOffsetRB.SetBinding(UIElement.IsEnabledProperty, multiBinding8);
			SWPostOffsetRB.SetBinding(UIElement.IsEnabledProperty, multiBinding8);
			HWPreOffsetRB.SetBinding(UIElement.IsEnabledProperty, multiBinding8);
			HWPostOffsetRB.SetBinding(UIElement.IsEnabledProperty, multiBinding8);
			Binding binding11 = new Binding("IsChecked");
			binding11.ElementName = "gainCB";
			Binding binding12 = new Binding("IsEnabled");
			binding12.ElementName = "gainCB";
			Binding item10 = binding12;
			MultiBinding multiBinding9 = new MultiBinding();
			multiBinding9.Mode = BindingMode.OneWay;
			MultiBinding multiBinding10 = multiBinding9;
			multiBinding10.Bindings.Add(item10);
			multiBinding10.Converter = new RadioButtonEnableConvert();
			SWGainRB.SetBinding(UIElement.IsEnabledProperty, multiBinding10);
			HWGainRB.SetBinding(UIElement.IsEnabledProperty, multiBinding10);
			Binding binding13 = new Binding("IsChecked");
			binding13.ElementName = "defectCB";
			Binding binding14 = new Binding("IsEnabled");
			binding14.ElementName = "defectCB";
			Binding item11 = binding14;
			MultiBinding multiBinding11 = new MultiBinding();
			multiBinding11.Mode = BindingMode.OneWay;
			MultiBinding multiBinding12 = multiBinding11;
			multiBinding12.Bindings.Add(item11);
			multiBinding12.Converter = new RadioButtonEnableConvert();
			SWDefectRB.SetBinding(UIElement.IsEnabledProperty, multiBinding12);
			HWDefectRB.SetBinding(UIElement.IsEnabledProperty, multiBinding12);
			MultiBinding multiBinding13 = new MultiBinding();
			multiBinding13.Mode = BindingMode.OneWay;
			MultiBinding multiBinding14 = multiBinding13;
			if ((1 & nFactoryAuthority) == 0)
			{
				multiBinding14.Bindings.Add(item);
				multiBinding14.Bindings.Add(item2);
				multiBinding14.Bindings.Add(item3);
				multiBinding14.Bindings.Add(item4);
				multiBinding14.Bindings.Add(item9);
				multiBinding14.Converter = new ButtonEnabledConvert();
			}
			else
			{
				multiBinding14.Bindings.Add(item9);
				multiBinding14.Converter = new RadioButtonEnableConvert();
			}
			SingleAcqBtn.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			PrepAcqBtn.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			StartAcqBtn.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			FreeSeqAcq.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			SeqAcq.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			Acq2Btn.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			AecPreExpBtn.SetBinding(UIElement.IsEnabledProperty, multiBinding14);
			SavedFrames = new CineFrame();
			SaveFileCountLabel.SetBinding(ContentControl.ContentProperty, new Binding("SaveImageProgress")
			{
				Source = SavedFrames
			});
		}

		private void SetExpButtonStateBinding()
		{
			ExpButtonStateConverter converter = new ExpButtonStateConverter();
			EnableOutExpBtn.SetBinding(UIElement.IsEnabledProperty, new Binding("Prop_Attr_State")
			{
				Mode = BindingMode.OneWay,
				Converter = converter,
				Source = Instance
			});
			ProhibitOutExpBtn.SetBinding(UIElement.IsEnabledProperty, new Binding("Prop_Attr_State")
			{
				Mode = BindingMode.OneWay,
				Converter = converter,
				Source = Instance
			});
		}

		private void Button_Clear(object sender, RoutedEventArgs e)
		{
			if (Instance == null)
			{
				return;
			}
			int num = SdkInvokeProxy.Invoke(1001);
			if (1 == num)
			{
				EnableAllUIElement(false);
				if (this.StartExposeWindow != null && Instance.Prop_Attr_UROM_TriggerMode == 2)
				{
					this.StartExposeWindow();
				}
			}
		}

		private void Button_PrepAcq(object sender, RoutedEventArgs e)
		{
			if (Instance == null)
			{
				return;
			}
			ImageViewCtrl.ExitCinePlayMode();
			int num = SdkInvokeProxy.Invoke(1002);
			if (1 == num || num == 0)
			{
				if (!oqcSeqSaveParam.EnableSave)
				{
					CountLabelVisible = false;
				}
				EnableAllUIElement(false);
				ImageViewCtrl.EnableUpdateImageList(true);
				Instance.TASK = "Acquire Acquire";
			}
			if (this.StopExposeWindow != null)
			{
				this.StopExposeWindow();
			}
		}

		private void Button_Acq2(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				ImageViewCtrl.ExitCinePlayMode();
				int num = SdkInvokeProxy.Invoke(1003);
				if (1 == num || num == 0)
				{
					EnableAllUIElement(false);
					ImageViewCtrl.EnableUpdateImageList(true);
					Instance.TASK = "Prep Acquire";
				}
			}
		}

		private void Button_AecPreExp(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				ImageViewCtrl.ExitCinePlayMode();
				int num = SdkInvokeProxy.Invoke(1016);
				if (1 == num || num == 0)
				{
					EnableAllUIElement(false);
					ImageViewCtrl.EnableUpdateImageList(true);
					Instance.TASK = "Prep Acquire";
				}
			}
		}

		private void Button_SingleAcq(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				ImageViewCtrl.ExitCinePlayMode();
				int num = SdkInvokeProxy.Invoke(1006);
				if (1 == num || num == 0)
				{
					EnableAllUIElement(false);
					ImageViewCtrl.EnableUpdateImageList(true);
					Instance.TASK = "Single Acquire";
				}
				if (this.StopExposeWindow != null)
				{
					this.StopExposeWindow();
				}
			}
		}

		private void Button_FreeSeqAcq(object sender, RoutedEventArgs e)
		{
			if (Instance != null && CanSequenceAcquire())
			{
				Prompt4SeqAcq();
				StatusControler.UpdateStatusAfterSeqAcq();
				SetImageViewCtrlParamsInEnterSeqAcq();
				int num = SdkInvokeProxy.Invoke(1010, 0);
				if (1 == num)
				{
					SavedFrames.EnableUpdate = true;
					CountLabelVisible = true;
					EnableAllUIElement(false);
					Instance.TASK = "Free Sequence Acquiring";
				}
				else if (num != 0)
				{
					SetImageViewCtrlParamsInExitSeqAcq();
					StatusControler.UpdateStatusAfterStopAcq();
				}
			}
		}

		private void Button_SeqAcq(object sender, RoutedEventArgs e)
		{
			if (Instance != null && CanSequenceAcquire())
			{
				Prompt4SeqAcq();
				StatusControler.UpdateStatusAfterSeqAcq();
				SetImageViewCtrlParamsInEnterSeqAcq();
				int num = SdkInvokeProxy.Invoke(1007, 0);
				if (1 == num)
				{
					EnableAllUIElement(false);
					Instance.TASK = "Sequence Acquiring";
				}
				else if (num != 0)
				{
					SetImageViewCtrlParamsInExitSeqAcq();
					StatusControler.UpdateStatusAfterStopAcq();
				}
			}
		}

		private void Button_Acquire(object sender, RoutedEventArgs e)
		{
			if (Instance == null)
			{
				return;
			}
			StatusControler.UpdateStatusAfterStartAcq();
			if (StatusControler.IsSequenceAcq)
			{
				if (!CanSequenceAcquire())
				{
					StatusControler.UpdateStatusAfterStopAcq();
					return;
				}
				Prompt4SeqAcq();
				SetImageViewCtrlParamsInEnterSeqAcq();
			}
			int num = SdkInvokeProxy.Invoke(1004);
			if (1 == num)
			{
				if (StatusControler.IsSequenceAcq)
				{
					IsAcquiring = true;
					Instance.TASK = "Acquiring";
					SavedFrames.EnableUpdate = true;
					CountLabelVisible = true;
				}
				else
				{
					SetImageViewCtrlParamsInExitSeqAcq();
				}
				EnableAllUIElement(false);
			}
			else if (num != 0)
			{
				SetImageViewCtrlParamsInExitSeqAcq();
				StatusControler.UpdateStatusAfterStopAcq();
			}
			if (this.StopExposeWindow != null)
			{
				this.StopExposeWindow();
			}
		}

		private void SetImageViewCtrlParamsInExitSeqAcq()
		{
			ImageViewCtrl.ExitCinePlayMode();
			ImageViewCtrl.EnableUpdateImageList(true);
		}

		private void SetImageViewCtrlParamsInEnterSeqAcq()
		{
			ImageViewCtrl.StartStoreCineData();
			ImageViewCtrl.EnableUpdateImageList(false);
		}

		private void Button_OQCSeqSave(object sender, RoutedEventArgs e)
		{
			if (mSeqSaveSetBackup != null && mSeqSaveSetBackup.IsEnableCine)
			{
				MessageBoxShow("Not support OQCSave in sequence save!");
				return;
			}
			OQCSeqSaveWnd oQCSeqSaveWnd = new OQCSeqSaveWnd(Instance.Prop_Attr_Width * Instance.Prop_Attr_Height * 2 / 1024 / 1024);
			if (oQCSeqSaveWnd.ShowDlg(oqcSeqSaveParam))
			{
				oqcSeqSaveParam.EnableSave = false;
				if (oQCSeqSaveWnd.EnableSave)
				{
					oqcSeqSaveParam.SavedFrames = 0;
					oqcSeqSaveParam.EnableSave = oQCSeqSaveWnd.EnableSave;
					SavedFrames.SetSaveNumber(oQCSeqSaveWnd.Frames);
					SavedFrames.FrameCount = 0;
				}
				oqcSeqSaveParam.TotalFrames = oQCSeqSaveWnd.Frames;
				oqcSeqSaveParam.Path = oQCSeqSaveWnd.Path;
				SavedFrames.EnableUpdate = oqcSeqSaveParam.EnableSave;
				SaveFileCountLabel.Visibility = ((!oQCSeqSaveWnd.EnableSave) ? Visibility.Hidden : Visibility.Visible);
				CountLabelVisible = oQCSeqSaveWnd.EnableSave;
				oQCSeqSaveWnd = null;
				GC.Collect();
			}
		}

		private void Button_EnableOutExp(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(1012);
			}
		}

		private void Button_ProhibitOutExp(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(1011);
			}
		}

		private void Button_ActiveSensor(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(3023);
			}
		}

		private bool CanSequenceAcquire()
		{
			if (HWPostOffsetRB.IsChecked == true || SWPostOffsetRB.IsChecked == true)
			{
				MessageBoxShow("Do not select Post offset in sequence mode");
				return false;
			}
			return true;
		}

		private void Button_Stop(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				StatusControler.UpdateStatusAfterStopAcq();
				if (SdkInvokeProxy.Invoke(1005) == 0)
				{
					SavedFrames.EnableUpdate = false;
					EnableAllUIElement(true);
				}
				Instance.TASK = "No Task";
			}
		}

		private void Button_Sleep(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(4);
			}
		}

		private void Button_Wakeup(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(5);
			}
		}

		private void Prompt4SeqAcq()
		{
		}

		private void Button_Save(object sender, RoutedEventArgs e)
		{
			string text = FileDialog.SaveFile();
			if (text != null)
			{
				long diskFreeSpace = Utility.GetDiskFreeSpace(Utility.GetDiskName(text));
				int num = Instance.Prop_Attr_Width * Instance.Prop_Attr_Height * 2 / 1024 / 1024 + 2;
				if (diskFreeSpace <= num)
				{
					MessageBoxShow("No enough space!");
				}
				else
				{
					ImageViewCtrl.SaveCurrentImage(text);
				}
			}
		}

		private void Button_SeqAcqSet(object sender, RoutedEventArgs e)
		{
			if (mSeqSaveSetBackup == null)
			{
				mSeqSaveSetBackup = new SeqSaveSetSave();
			}
			mSeqSaveSet = null;
			mSeqSaveSet = new SeqSaveSetWnd(ref mSeqSaveSetBackup, Instance.Prop_Attr_Width * Instance.Prop_Attr_Height * 2);
			mSeqSaveSet.ShowDialog();
			if (!mSeqSaveSet.Result)
			{
				return;
			}
			if (Enm_SeqSettingtype.Save == mSeqSaveSet.OperationType)
			{
				if (Utility.IsFileCanbeWritten(mSeqSaveSet.FilePath))
				{
					ImageViewCtrl.StartSeqSave(new StoreFileParams
					{
						filePath = mSeqSaveSet.FilePath,
						startIndex = mSeqSaveSetBackup.StartIndex,
						endIndex = mSeqSaveSetBackup.EndIndex
					});
				}
				else
				{
					MessageBoxShow("This file had been opened and cannot be written!");
				}
			}
			else if (Enm_SeqSettingtype.Setting == mSeqSaveSet.OperationType)
			{
				SavedFrames.SetSaveNumber(mSeqSaveSet.Frames);
				long diskFreeSpace = Utility.GetDiskFreeSpace(Utility.GetDiskName(Instance.Prop_Attr_WorkDir));
				int num = Instance.Prop_Attr_Width * Instance.Prop_Attr_Height * 2 / 1024 / 1024;
				if (mSeqSaveSet.IsEnableCine && diskFreeSpace <= num * mSeqSaveSet.Frames)
				{
					MessageBoxShow("Work Directory has no enough space!");
					mSeqSaveSetBackup.IsEnableCine = false;
					mSeqSaveSet.IsEnableCine = false;
				}
				else
				{
					ImageViewCtrl.EnableStoreCine(mSeqSaveSet.IsEnableCine, mSeqSaveSet.Frames, Instance.Prop_Attr_WorkDir + "temp\\");
				}
				if (oqcSeqSaveParam.EnableSave && mSeqSaveSet.IsEnableCine)
				{
					oqcSeqSaveParam.EnableSave = false;
				}
				if (!oqcSeqSaveParam.EnableSave && !mSeqSaveSet.IsEnableCine)
				{
					SaveFileCountLabel.Visibility = Visibility.Hidden;
				}
			}
			else if (Enm_SeqSettingtype.ProcessImg == mSeqSaveSet.OperationType)
			{
				ImageViewCtrl.SetImageProcessorCoeff(new ProcessedCoeff
				{
					enableReduceNoise = mSeqSaveSetBackup.EnableReduceNoise,
					enableSuperostion = mSeqSaveSetBackup.EnableSuperpostion,
					maxAddFrames = mSeqSaveSetBackup.MaxAddFrames,
					noiseCoeff = mSeqSaveSetBackup.NoiseCoeff
				});
			}
			mSeqSaveSet.Close();
		}

		private void SaveImageToFile(IRayImageData image)
		{
			if (!oqcSeqSaveParam.EnableSave)
			{
				return;
			}
			if (oqcSeqSaveParam.SavedFrames < oqcSeqSaveParam.TotalFrames)
			{
				if (FileOperation.SaveToFileEnd(oqcSeqSaveParam.Path, image.ImgData))
				{
					oqcSeqSaveParam.SavedFrames++;
					SavedFrames.EnableUpdate = true;
					SavedFrames.FrameCount = oqcSeqSaveParam.SavedFrames;
				}
				else
				{
					base.Dispatcher.Invoke((Action)delegate
					{
						if (ShowToolTip != null)
						{
							ShowToolTip(string.Format("Save file to '{0}' failed!", oqcSeqSaveParam.Path));
						}
					}, new object[0]);
					SavedFrames.EnableUpdate = false;
				}
			}
			else if (oqcSeqSaveParam.SavedFrames >= oqcSeqSaveParam.TotalFrames)
			{
				base.Dispatcher.BeginInvoke((Action)delegate
				{
					SaveFileCountLabel.Visibility = Visibility.Hidden;
					SavedFrames.EnableUpdate = false;
				});
			}
		}

		private void Button_StartSelfCap(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(3030);
			}
		}

		private void Button_StopSelfCap(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(3031);
			}
		}

		private void Button_PowerOff(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				SdkInvokeProxy.Invoke(3029);
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			switch (nEventID)
			{
			case 1001:
			case 1002:
			case 1012:
			{
				if (base.Visibility != 0)
				{
					break;
				}
				IRayImage image = (IRayImage)Marshal.PtrToStructure(pParam, typeof(IRayImage));
				CurImage.Write(ref image);
				if (CurImage.ImgData == null)
				{
					break;
				}
				if (nEventID != 1002)
				{
					ImageViewCtrl.StoreImage2Buffer(CurImage);
					if (!oqcSeqSaveParam.EnableSave && SavedFrames.EnableUpdate)
					{
						SavedFrames.FrameCount++;
					}
					if (StatusControler != null && !StatusControler.IsSequenceAcq)
					{
						IsAcquiring = false;
					}
					SaveImageToFile(CurImage);
				}
				else
				{
					base.Dispatcher.BeginInvoke((Action)delegate
					{
						ImageViewCtrl.DisplayImage(CurImage.ImgData, CurImage.nWidth, CurImage.nHeight);
					});
				}
				break;
			}
			default:
				base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
				break;
			}
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			bHandled = true;
			switch (nEventID)
			{
			case 3:
				HandleGeneralEvent(nParam1, nParam2);
				break;
			case 1:
				bHandled = false;
				break;
			case 4:
				switch (nParam1)
				{
				case 7:
				case 8:
				case 2001:
					if (2001 != nParam1)
					{
						ResetCorrectionRadioStatus();
					}
					ImageViewCtrl.SetImageSize(Instance.Prop_Attr_Width, Instance.Prop_Attr_Height);
					if (mSeqSaveSetBackup != null && mSeqSaveSetBackup.IsEnableCine)
					{
						mSeqSaveSet = null;
						mSeqSaveSet = new SeqSaveSetWnd(ref mSeqSaveSetBackup, Instance.Prop_Attr_Width * Instance.Prop_Attr_Height * 2);
						ImageViewCtrl.EnableStoreCine(mSeqSaveSet.IsEnableCine, mSeqSaveSet.Frames, Instance.Prop_Attr_WorkDir + "temp\\");
						mSeqSaveSetBackup.Frames = mSeqSaveSet.Frames;
						SavedFrames.SetSaveNumber(mSeqSaveSet.Frames);
					}
					break;
				case 6:
					CloseMessageBox();
					ResetCorrectionRadioStatus();
					break;
				case 1005:
					if (base.Visibility == Visibility.Visible)
					{
						ImageViewCtrl.EnterCinePlayMode();
						ImageViewCtrl.EnableUpdateImageList(true);
					}
					EnableAllUIElement(true);
					IsAcquiring = false;
					break;
				case 1004:
					if (!StatusControler.IsSequenceAcq)
					{
						EnableAllUIElement(true);
					}
					break;
				case 1002:
				case 1003:
				case 1006:
				case 1016:
					EnableAllUIElement(true);
					break;
				case 1001:
					EnableAllUIElement(true);
					break;
				case 2:
					EnableAllUIElement(true);
					break;
				}
				break;
			case 5:
				switch (nParam1)
				{
				case 1004:
				case 1007:
					if (base.Visibility == Visibility.Visible)
					{
						EnableAllUIElement(true);
						StatusControler.UpdateStatusAfterStopAcq();
						ImageViewCtrl.EnableUpdateImageList(true);
					}
					break;
				case 6:
					ResetCorrectionRadioStatus();
					CloseMessageBox();
					if (base.Visibility != 0)
					{
					}
					break;
				case 1002:
				case 1003:
				case 1006:
				case 1016:
				{
					Visibility visibility = base.Visibility;
					EnableAllUIElement(true);
					break;
				}
				case 1001:
				{
					Visibility visibility2 = base.Visibility;
					EnableAllUIElement(true);
					break;
				}
				}
				break;
			case 6:
				EnableAllUIElement(true);
				if (1005 == nParam1 && base.Visibility == Visibility.Visible)
				{
					ImageViewCtrl.EnterCinePlayMode();
				}
				break;
			case 1003:
				IsAcquiring = false;
				if (base.Visibility == Visibility.Visible)
				{
					ImageViewCtrl.EnterCinePlayMode();
					ImageViewCtrl.EnableUpdateImageList(true);
					EnableAllUIElement(true);
				}
				break;
			case 1006:
				EnableAllUIElement(true);
				break;
			case 7:
				if (base.Visibility == Visibility.Visible && (1004 == nParam1 || 1002 == nParam1))
				{
					EnableAllUIElement(false);
				}
				break;
			}
		}

		private void HandleGeneralEvent(int cmdId, int param)
		{
			string text = "";
			switch (cmdId)
			{
			case 6:
			{
				Enm_CaliDataState enm_CaliDataState = Enm_CaliDataState.Enm_CaliDataState_NoData;
				switch (param)
				{
				case 65536:
					enm_CaliDataState = (Enm_CaliDataState)Instance.Prop_Attr_OffsetValidityState;
					break;
				case 262144:
					enm_CaliDataState = (Enm_CaliDataState)Instance.Prop_Attr_GainValidityState;
					break;
				case 1048576:
					enm_CaliDataState = (Enm_CaliDataState)Instance.Prop_Attr_DefectValidityState;
					break;
				}
				switch (enm_CaliDataState)
				{
				case Enm_CaliDataState.Enm_CaliDataState_NoData:
					text += "correction file is absent";
					MessageBoxShow(text);
					break;
				case Enm_CaliDataState.Enm_CaliDataState_OutOfDate:
					text += "correction file is invalid";
					MessageBoxShow(text);
					break;
				case Enm_CaliDataState.Enm_CaliDataState_ValidWarn:
					text = text + "correction file will be invalid in " + param.ToString() + "minutes";
					break;
				case Enm_CaliDataState.Enm_CaliDataState_Valid:
					text += "correction file:load succeed";
					break;
				}
				MessageNotify(text);
				break;
			}
			case 2:
				if (4 == param)
				{
					EnableAllUIElement(true);
				}
				break;
			}
		}

		public void UpdateTemplateFileValidityState()
		{
			int prop_Attr_OffsetValidityState = Instance.Prop_Attr_OffsetValidityState;
			int prop_Attr_GainValidityState = Instance.Prop_Attr_GainValidityState;
			int prop_Attr_DefectValidityState = Instance.Prop_Attr_DefectValidityState;
			if (prop_Attr_OffsetValidityState == 0 && 65536 == (0x10000 & Instance.Prop_Attr_CurrentCorrectOption))
			{
				SetOffsetCorrection(true);
			}
			if (prop_Attr_GainValidityState == 0 && 262144 == (0x40000 & Instance.Prop_Attr_CurrentCorrectOption))
			{
				SetGainCorrection(true);
			}
			if (prop_Attr_DefectValidityState == 0 && 1048576 == (0x100000 & Instance.Prop_Attr_CurrentCorrectOption))
			{
				SetDefectCorrection(true);
			}
		}

		private void ResetCorrectionRadioStatus()
		{
			int prop_Attr_CurrentCorrectOption = Instance.Prop_Attr_CurrentCorrectOption;
			if ((prop_Attr_CurrentCorrectOption & 0x10000) == 65536)
			{
				SWPreOffsetRB.IsChecked = true;
			}
			else if ((prop_Attr_CurrentCorrectOption & 0x20000) == 131072)
			{
				SWPostOffsetRB.IsChecked = true;
			}
			else if ((prop_Attr_CurrentCorrectOption & 1) == 1)
			{
				HWPreOffsetRB.IsChecked = true;
			}
			else if ((prop_Attr_CurrentCorrectOption & 2) == 2)
			{
				HWPostOffsetRB.IsChecked = true;
			}
			else
			{
				SWPreOffsetRB.IsChecked = false;
				SWPostOffsetRB.IsChecked = false;
				HWPreOffsetRB.IsChecked = false;
				HWPostOffsetRB.IsChecked = false;
			}
			if ((prop_Attr_CurrentCorrectOption & 0x40000) == 262144)
			{
				SWGainRB.IsChecked = true;
			}
			else if ((prop_Attr_CurrentCorrectOption & 4) == 4)
			{
				HWGainRB.IsChecked = true;
			}
			else
			{
				SWGainRB.IsChecked = false;
				HWGainRB.IsChecked = false;
			}
			if ((prop_Attr_CurrentCorrectOption & 0x100000) == 1048576)
			{
				SWDefectRB.IsChecked = true;
				return;
			}
			if ((prop_Attr_CurrentCorrectOption & 0x10) == 16)
			{
				HWDefectRB.IsChecked = true;
				return;
			}
			SWDefectRB.IsChecked = false;
			HWDefectRB.IsChecked = false;
		}

		private int SetOffsetCorrection(bool bReset)
		{
			int result = 2;
			if (Instance == null)
			{
				return result;
			}
			int prop_Attr_CurrentCorrectOption = Instance.Prop_Attr_CurrentCorrectOption;
			int num = 196611;
			prop_Attr_CurrentCorrectOption &= ~num;
			if (!bReset)
			{
				if (SWPreOffsetRB.IsChecked == true)
				{
					prop_Attr_CurrentCorrectOption |= 0x10000;
				}
				else if (SWPostOffsetRB.IsChecked == true)
				{
					prop_Attr_CurrentCorrectOption |= 0x20000;
				}
				else if (HWPreOffsetRB.IsChecked == true)
				{
					prop_Attr_CurrentCorrectOption |= 1;
				}
				else
				{
					if (HWPostOffsetRB.IsChecked != true)
					{
						return 0;
					}
					prop_Attr_CurrentCorrectOption |= 2;
				}
			}
			else
			{
				ResetOffsetSubset();
			}
			result = SdkInvokeProxy.Invoke(6, prop_Attr_CurrentCorrectOption);
			if (5 == result)
			{
				MessageNotify("Cannot set correction mode in current state!");
			}
			return result;
		}

		private void ResetOffsetSubset()
		{
			SWPreOffsetRB.IsChecked = false;
			HWPreOffsetRB.IsChecked = false;
			SWPostOffsetRB.IsChecked = false;
			HWPostOffsetRB.IsChecked = false;
		}

		private int SetGainCorrection(bool bReset)
		{
			int result = 2;
			if (Instance == null)
			{
				return result;
			}
			int prop_Attr_CurrentCorrectOption = Instance.Prop_Attr_CurrentCorrectOption;
			prop_Attr_CurrentCorrectOption = (prop_Attr_CurrentCorrectOption & -262145 & -5);
			if (!bReset)
			{
				if (SWGainRB.IsChecked == true)
				{
					prop_Attr_CurrentCorrectOption |= 0x40000;
				}
				else
				{
					if (HWGainRB.IsChecked != true)
					{
						return 0;
					}
					prop_Attr_CurrentCorrectOption |= 4;
				}
			}
			else
			{
				ResetGainSubset();
			}
			result = SdkInvokeProxy.Invoke(6, prop_Attr_CurrentCorrectOption);
			if (5 == result)
			{
				MessageBox.Show("Cannot set correction mode in current state!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
			return result;
		}

		private void ResetGainSubset()
		{
			SWGainRB.IsChecked = false;
			HWGainRB.IsChecked = false;
		}

		private int SetDefectCorrection(bool bReset)
		{
			int result = 2;
			if (Instance == null)
			{
				return result;
			}
			int prop_Attr_CurrentCorrectOption = Instance.Prop_Attr_CurrentCorrectOption;
			prop_Attr_CurrentCorrectOption = (prop_Attr_CurrentCorrectOption & -1048577 & -17);
			if (!bReset)
			{
				if (SWDefectRB.IsChecked == true)
				{
					prop_Attr_CurrentCorrectOption |= 0x100000;
				}
				else
				{
					if (HWDefectRB.IsChecked != true)
					{
						return 0;
					}
					prop_Attr_CurrentCorrectOption |= 0x10;
				}
			}
			else
			{
				ResetDefectSubset();
			}
			result = SdkInvokeProxy.Invoke(6, prop_Attr_CurrentCorrectOption);
			if (5 == result)
			{
				MessageBox.Show("Cannot set correction mode in current state!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
			return result;
		}

		private void ResetDefectSubset()
		{
			SWDefectRB.IsChecked = false;
			HWDefectRB.IsChecked = false;
		}

		private void OnSelectOffsetMode(object sender, RoutedEventArgs e)
		{
			int num = SetOffsetCorrection(false);
			if (1 == num)
			{
				ShowMessageBox("Set Correction", "Setting correction option");
			}
			else if (num != 0)
			{
				ResetCorrectionRadioStatus();
			}
		}

		private void OnSelectGainMode(object sender, RoutedEventArgs e)
		{
			int num = SetGainCorrection(false);
			if (1 == num)
			{
				ShowMessageBox("Set Correction", "Setting correction option");
			}
			else if (num != 0)
			{
				ResetCorrectionRadioStatus();
			}
		}

		private void OnSelectDefectMode(object sender, RoutedEventArgs e)
		{
			int num = SetDefectCorrection(false);
			if (1 == num)
			{
				ShowMessageBox("Set Correction", "Setting correction option");
			}
			else if (num != 0)
			{
				ResetCorrectionRadioStatus();
			}
		}

		private void OnCheckCorrection(object sender, RoutedEventArgs e)
		{
			if (Instance == null)
			{
				return;
			}
			CheckBox checkBox = sender as CheckBox;
			int num = 0;
			if (checkBox.Equals(offsetCB))
			{
				if (11 == Instance.Prop_Attr_UROM_ProductNo && Instance.Prop_Attr_UROM_ExpMode != 0)
				{
					offsetCB.IsChecked = true;
					MessageBoxShow("Hard Post-Offset correction must be used in Manual/AEC EXP-Mode");
					return;
				}
				num = SetOffsetCorrection(checkBox.IsChecked == false);
			}
			else if (checkBox.Equals(gainCB))
			{
				num = SetGainCorrection(checkBox.IsChecked == false);
			}
			else if (checkBox.Equals(defectCB))
			{
				num = SetDefectCorrection(checkBox.IsChecked == false);
			}
			if (1 == num)
			{
				ShowMessageBox("Set Correction", "Setting correction option");
			}
			else if (num == 0)
			{
				if (checkBox.IsChecked == false)
				{
					ResetCorrectionRadioStatus();
				}
			}
			else if (num != 0)
			{
				ResetCorrectionRadioStatus();
			}
		}

		private void MessageBoxShow(string content)
		{
			MessageBox.Show(Application.Current.MainWindow, content);
		}

		protected void ShowMessageBox(string title, string content)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TaskTimeoutSeconds, null, CurTaskTimeOut);
			MessageBoxTimer.RunTask();
		}

		private void CurTaskTimeOut()
		{
			MessageBoxShow("Current task Timeout!");
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		public void NotifyReconnected()
		{
			ResetCorrectionRadioStatus();
			EnableAllUIElement(true);
		}

		public void OnClose()
		{
			if (VerifyHWComp != null && xOpButton.Children.Contains(VerifyHWComp as FrameworkElement))
			{
				xOpButton.Children.Remove(VerifyHWComp as FrameworkElement);
			}
			if (mPlugin != null)
			{
				mPlugin.Clear();
			}
			CurImage = null;
			instance.UnSubscribeCBEvent(OnSdkCallback);
			ImageViewCtrl.OnClose();
			xUIPanel.Children.Remove(ImageViewCtrl);
		}

		public bool Exit()
		{
			if (!IsAcquiring && this.StopExposeWindow != null)
			{
				this.StopExposeWindow();
			}
			return !IsAcquiring;
		}

		public bool Enter()
		{
			return true;
		}

		private void EnablePanel(bool bEnable)
		{
			ClearBtn.IsEnabled = bEnable;
			offsetCB.IsEnabled = bEnable;
			gainCB.IsEnabled = bEnable;
			defectCB.IsEnabled = bEnable;
			if (StatusControler != null && !StatusControler.IsSequenceAcq)
			{
				StopAcqBtn.IsEnabled = bEnable;
			}
			SaveBtn.IsEnabled = bEnable;
			SeqSaveSet.IsEnabled = bEnable;
			WackupBtn.IsEnabled = bEnable;
			SleepBtn.IsEnabled = bEnable;
			if (mSeqSaveSetBackup != null && mSeqSaveSetBackup.IsEnableCine)
			{
				SaveFileCountLabel.Visibility = ((!CountLabelVisible) ? Visibility.Hidden : Visibility.Visible);
				if (!bEnable && CountLabelVisible && !oqcSeqSaveParam.EnableSave)
				{
					SavedFrames.FrameCount = 0;
				}
			}
		}

		private void EnableAllUIElement(bool bEnable)
		{
			IsAcquiring = !bEnable;
			EnablePanel(bEnable);
		}

		private void MessageNotify(string info)
		{
			if (this.MessageNotifyProxy != null)
			{
				this.MessageNotifyProxy(info);
			}
		}

		private void MammoButtonControl()
		{
			int prop_Attr_UROM_ExpMode = Instance.Prop_Attr_UROM_ExpMode;
			int prop_Attr_UROM_TriggerMode = Instance.Prop_Attr_UROM_TriggerMode;
			if ((0x2000 & nFactoryAuthority) != 0)
			{
				switch (prop_Attr_UROM_ExpMode)
				{
				case 0:
					ClearBtn.Visibility = Visibility.Visible;
					SingleAcqBtn.Visibility = Visibility.Visible;
					PrepAcqBtn.Visibility = Visibility.Visible;
					SingleAcqBtn.Visibility = Visibility.Visible;
					StopAcqBtn.Visibility = Visibility.Visible;
					SeqAcq.Visibility = Visibility.Visible;
					OQCSeqSave.Visibility = Visibility.Visible;
					return;
				case 1:
					if (prop_Attr_UROM_TriggerMode == 2)
					{
						ClearBtn.Visibility = Visibility.Visible;
						SingleAcqBtn.Visibility = Visibility.Visible;
						PrepAcqBtn.Visibility = Visibility.Collapsed;
						StartAcqBtn.Visibility = Visibility.Collapsed;
						SingleAcqBtn.Visibility = Visibility.Collapsed;
						StopAcqBtn.Visibility = Visibility.Collapsed;
						SeqAcq.Visibility = Visibility.Collapsed;
						OQCSeqSave.Visibility = Visibility.Collapsed;
						return;
					}
					break;
				}
				ClearBtn.Visibility = Visibility.Collapsed;
				SingleAcqBtn.Visibility = Visibility.Collapsed;
				PrepAcqBtn.Visibility = Visibility.Collapsed;
				StartAcqBtn.Visibility = Visibility.Collapsed;
				SingleAcqBtn.Visibility = Visibility.Collapsed;
				StopAcqBtn.Visibility = Visibility.Collapsed;
				SeqAcq.Visibility = Visibility.Collapsed;
				OQCSeqSave.Visibility = Visibility.Collapsed;
				return;
			}
			switch (prop_Attr_UROM_ExpMode)
			{
			case 0:
				ClearBtn.Visibility = Visibility.Visible;
				SingleAcqBtn.Visibility = Visibility.Visible;
				PrepAcqBtn.Visibility = Visibility.Visible;
				return;
			case 1:
				if (prop_Attr_UROM_TriggerMode == 2)
				{
					ClearBtn.Visibility = Visibility.Visible;
					SingleAcqBtn.Visibility = Visibility.Visible;
					PrepAcqBtn.Visibility = Visibility.Collapsed;
					StartAcqBtn.Visibility = Visibility.Collapsed;
					return;
				}
				break;
			}
			ClearBtn.Visibility = Visibility.Collapsed;
			SingleAcqBtn.Visibility = Visibility.Collapsed;
			PrepAcqBtn.Visibility = Visibility.Collapsed;
			StartAcqBtn.Visibility = Visibility.Collapsed;
		}

		private void OnStartUpTestCase(object sender, MouseButtonEventArgs e)
		{
			try
			{
				AutoTestCaseWnd autoTestCaseWnd = new AutoTestCaseWnd(instance);
				autoTestCaseWnd.WindowStartupLocation = WindowStartupLocation.CenterOwner;
				autoTestCaseWnd.ShowDialog();
			}
			catch (Exception)
			{
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/acquirepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xUIPanel = (DockPanel)target;
				break;
			case 2:
				offsetCB = (CheckBox)target;
				offsetCB.Click += OnCheckCorrection;
				break;
			case 3:
				SWPreOffsetRB = (RadioButton)target;
				SWPreOffsetRB.Click += OnSelectOffsetMode;
				break;
			case 4:
				HWPreOffsetRB = (RadioButton)target;
				HWPreOffsetRB.Click += OnSelectOffsetMode;
				break;
			case 5:
				SWPostOffsetRB = (RadioButton)target;
				SWPostOffsetRB.Click += OnSelectOffsetMode;
				break;
			case 6:
				HWPostOffsetRB = (RadioButton)target;
				HWPostOffsetRB.Click += OnSelectOffsetMode;
				break;
			case 7:
				gainCB = (CheckBox)target;
				gainCB.Click += OnCheckCorrection;
				break;
			case 8:
				SWGainRB = (RadioButton)target;
				SWGainRB.Click += OnSelectGainMode;
				break;
			case 9:
				HWGainRB = (RadioButton)target;
				HWGainRB.Click += OnSelectGainMode;
				break;
			case 10:
				defectCB = (CheckBox)target;
				defectCB.Click += OnCheckCorrection;
				break;
			case 11:
				SWDefectRB = (RadioButton)target;
				SWDefectRB.Click += OnSelectDefectMode;
				break;
			case 12:
				HWDefectRB = (RadioButton)target;
				HWDefectRB.Click += OnSelectDefectMode;
				break;
			case 13:
				xOpButton = (StackPanel)target;
				break;
			case 14:
				ClearBtn = (Button)target;
				ClearBtn.Click += Button_Clear;
				break;
			case 15:
				SingleAcqBtn = (Button)target;
				SingleAcqBtn.Click += Button_SingleAcq;
				break;
			case 16:
				PrepAcqBtn = (Button)target;
				PrepAcqBtn.Click += Button_PrepAcq;
				break;
			case 17:
				Acq2Btn = (Button)target;
				Acq2Btn.Click += Button_Acq2;
				break;
			case 18:
				AecPreExpBtn = (Button)target;
				AecPreExpBtn.Click += Button_AecPreExp;
				break;
			case 19:
				StartAcqBtn = (Button)target;
				StartAcqBtn.Click += Button_Acquire;
				break;
			case 20:
				StopAcqBtn = (Button)target;
				StopAcqBtn.Click += Button_Stop;
				break;
			case 21:
				SleepBtn = (Button)target;
				SleepBtn.Click += Button_Sleep;
				break;
			case 22:
				WackupBtn = (Button)target;
				WackupBtn.Click += Button_Wakeup;
				break;
			case 23:
				SaveBtn = (Button)target;
				SaveBtn.Click += Button_Save;
				break;
			case 24:
				FreeSeqAcq = (Button)target;
				FreeSeqAcq.Click += Button_FreeSeqAcq;
				break;
			case 25:
				SeqAcq = (Button)target;
				SeqAcq.Click += Button_SeqAcq;
				break;
			case 26:
				SeqSaveSet = (Button)target;
				SeqSaveSet.Click += Button_SeqAcqSet;
				break;
			case 27:
				OQCSeqSave = (Button)target;
				OQCSeqSave.Click += Button_OQCSeqSave;
				break;
			case 28:
				ActiveSensorBtn = (Button)target;
				ActiveSensorBtn.Click += Button_ActiveSensor;
				break;
			case 29:
				EnableOutExpBtn = (Button)target;
				EnableOutExpBtn.Click += Button_EnableOutExp;
				break;
			case 30:
				ProhibitOutExpBtn = (Button)target;
				ProhibitOutExpBtn.Click += Button_ProhibitOutExp;
				break;
			case 31:
				StartSelfCapBtn = (Button)target;
				StartSelfCapBtn.Click += Button_StartSelfCap;
				break;
			case 32:
				StopSelfCapBtn = (Button)target;
				StopSelfCapBtn.Click += Button_StopSelfCap;
				break;
			case 33:
				PowerOffBtn = (Button)target;
				PowerOffBtn.Click += Button_PowerOff;
				break;
			case 34:
				SaveFileCountLabel = (Label)target;
				break;
			case 35:
				ImageViewCtrl = (CtrImagePresent)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
