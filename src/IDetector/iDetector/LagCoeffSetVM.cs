using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace iDetector
{
	internal class LagCoeffSetVM : NotifyObject
	{
		private LagGhostMapItem selectedMapItem;

		private string resultLabel;

		public LagGhostMapItem writeItem;

		private readonly string MapFileName = "DcMap";

		public DelegateCommand AddItem
		{
			get;
			set;
		}

		public DelegateCommand DelItem
		{
			get;
			set;
		}

		public DelegateCommand ReadMap
		{
			get;
			set;
		}

		public DelegateCommand WriteMap
		{
			get;
			set;
		}

		public ObservableCollection<LagGhostMapItem> MapList
		{
			get;
			set;
		}

		public LagGhostMapItem SelectedMapItem
		{
			get
			{
				return selectedMapItem;
			}
			set
			{
				selectedMapItem = value;
				NotifyPropertyChanged("SelectedMapItem");
			}
		}

		public ObservableCollection<EnumDisplayItem> PGAList
		{
			get;
			set;
		}

		public ObservableCollection<EnumDisplayItem> BinningList
		{
			get;
			set;
		}

		public string ResultLabel
		{
			get
			{
				return resultLabel;
			}
			set
			{
				resultLabel = value;
				NotifyPropertyChanged("ResultLabel");
			}
		}

		public int SelPGAValue
		{
			get
			{
				return writeItem.PGA;
			}
			set
			{
				writeItem.PGA = value;
				NotifyPropertyChanged("SelPGAValue");
			}
		}

		public int SelBinningValue
		{
			get
			{
				return writeItem.Binning;
			}
			set
			{
				writeItem.Binning = value;
				NotifyPropertyChanged("SelBinningValue");
			}
		}

		public int TimeGap_W
		{
			get
			{
				return writeItem.TimeGap;
			}
			set
			{
				writeItem.TimeGap = value;
				NotifyPropertyChanged("TimeGap_W");
			}
		}

		public bool EnableLag_W
		{
			get
			{
				return writeItem.EnableLag;
			}
			set
			{
				writeItem.EnableLag = value;
				NotifyPropertyChanged("EnableLag_W");
			}
		}

		public float LagA1_W
		{
			get
			{
				return writeItem.LagA1;
			}
			set
			{
				writeItem.LagA1 = value;
				NotifyPropertyChanged("LagA1_W");
			}
		}

		public float LagB1_W
		{
			get
			{
				return writeItem.LagB1;
			}
			set
			{
				writeItem.LagB1 = value;
				NotifyPropertyChanged("LagB1_W");
			}
		}

		public float LagA2_W
		{
			get
			{
				return writeItem.LagA2;
			}
			set
			{
				writeItem.LagA2 = value;
				NotifyPropertyChanged("LagA2_W");
			}
		}

		public float LagB2_W
		{
			get
			{
				return writeItem.LagB2;
			}
			set
			{
				writeItem.LagB2 = value;
				NotifyPropertyChanged("LagB2_W");
			}
		}

		public bool EnableGhost_W
		{
			get
			{
				return writeItem.EnableGhost;
			}
			set
			{
				writeItem.EnableGhost = value;
				NotifyPropertyChanged("EnableGhost_W");
			}
		}

		public float GhostM1_W
		{
			get
			{
				return writeItem.GhostM1;
			}
			set
			{
				writeItem.GhostM1 = value;
				NotifyPropertyChanged("GhostM1_W");
			}
		}

		public float GhostN1_W
		{
			get
			{
				return writeItem.GhostN1;
			}
			set
			{
				writeItem.GhostN1 = value;
				NotifyPropertyChanged("GhostN1_W");
			}
		}

		public float GhostM2_W
		{
			get
			{
				return writeItem.GhostM2;
			}
			set
			{
				writeItem.GhostM2 = value;
				NotifyPropertyChanged("GhostM2_W");
			}
		}

		public float GhostN2_W
		{
			get
			{
				return writeItem.GhostN2;
			}
			set
			{
				writeItem.GhostN2 = value;
				NotifyPropertyChanged("GhostN2_W");
			}
		}

		public float GhostP1_W
		{
			get
			{
				return writeItem.GhostP1;
			}
			set
			{
				writeItem.GhostP1 = value;
				NotifyPropertyChanged("GhostP1_W");
			}
		}

		public float GhostQ1_W
		{
			get
			{
				return writeItem.GhostQ1;
			}
			set
			{
				writeItem.GhostQ1 = value;
				NotifyPropertyChanged("GhostQ1_W");
			}
		}

		public float GhostP2_W
		{
			get
			{
				return writeItem.GhostP2;
			}
			set
			{
				writeItem.GhostP2 = value;
				NotifyPropertyChanged("GhostP2_W");
			}
		}

		public float GhostQ2_W
		{
			get
			{
				return writeItem.GhostQ2;
			}
			set
			{
				writeItem.GhostQ2 = value;
				NotifyPropertyChanged("GhostQ2_W");
			}
		}

		private Detector DetInstance
		{
			get;
			set;
		}

		public LagCoeffSetVM(Detector detector)
		{
			DetInstance = detector;
			writeItem = new LagGhostMapItem();
			MapList = new ObservableCollection<LagGhostMapItem>();
			AddItem = new DelegateCommand(OnAddVTItem);
			DelItem = new DelegateCommand(OnDelVTItem);
			ReadMap = new DelegateCommand(OnReadVTMap);
			WriteMap = new DelegateCommand(OnWriteVTMap);
			PGAList = SDKEnumCollector.LoadParam("Enm_PGA");
			BinningList = SDKEnumCollector.LoadParam("Enm_Binning");
			ReadVTMapList();
		}

		public void onLoadCurSelectedItem()
		{
			SelPGAValue = SelectedMapItem.PGA;
			SelBinningValue = SelectedMapItem.Binning;
			TimeGap_W = SelectedMapItem.TimeGap;
			LagA1_W = SelectedMapItem.LagA1;
			LagB1_W = SelectedMapItem.LagB1;
			LagA2_W = SelectedMapItem.LagA2;
			LagB2_W = SelectedMapItem.LagB2;
			EnableLag_W = SelectedMapItem.EnableLag;
			EnableGhost_W = SelectedMapItem.EnableGhost;
			GhostM1_W = SelectedMapItem.GhostM1;
			GhostN1_W = SelectedMapItem.GhostN1;
			GhostM2_W = SelectedMapItem.GhostM2;
			GhostN2_W = SelectedMapItem.GhostN2;
			GhostP1_W = SelectedMapItem.GhostP1;
			GhostQ1_W = SelectedMapItem.GhostQ1;
			GhostP2_W = SelectedMapItem.GhostP2;
			GhostQ2_W = SelectedMapItem.GhostQ2;
		}

		private void OnWriteVTMap(object obj)
		{
			if (MapList.Count != 0)
			{
				try
				{
					FileStream fileStream = new FileStream(DetInstance.Prop_Attr_WorkDir + MapFileName, FileMode.Create, FileAccess.Write);
					fileStream.Seek(34L, SeekOrigin.Begin);
					byte[] bytes = new ASCIIEncoding().GetBytes("dynamic coeff");
					fileStream.Write(bytes, 0, bytes.Length);
					fileStream.Seek(396L, SeekOrigin.Begin);
					short value = 1;
					byte[] bytes2 = BitConverter.GetBytes(value);
					fileStream.Write(bytes2, 0, bytes2.Length);
					fileStream.Seek(1024L, SeekOrigin.Begin);
					int value2 = MapList.Count * LagGhostMapItem.GetArrayLength();
					bytes2 = BitConverter.GetBytes(value2);
					fileStream.Write(bytes2, 0, bytes2.Length);
					fileStream.Seek(1040L, SeekOrigin.Begin);
					foreach (LagGhostMapItem map in MapList)
					{
						byte[] array = map.ToArray();
						fileStream.Write(array, 0, array.Length);
					}
					fileStream.Close();
					ResultLabel = "Write succeed";
				}
				catch (Exception ex)
				{
					ResultLabel = "Write failed";
					Shell.ShowMessageAutoOwner("Write VTMap failed! Err = " + ex.Message);
				}
			}
		}

		private void OnReadVTMap(object obj)
		{
			ReadVTMapList();
		}

		private void ReadVTMapList()
		{
			MapList.Clear();
			if (!Utility.IsFileExisted(DetInstance.Prop_Attr_WorkDir + MapFileName))
			{
				return;
			}
			FileStream fileStream = new FileStream(DetInstance.Prop_Attr_WorkDir + MapFileName, FileMode.Open, FileAccess.Read);
			fileStream.Seek(1040L, SeekOrigin.Begin);
			byte[] array = new byte[LagGhostMapItem.GetArrayLength()];
			while (true)
			{
				LagGhostMapItem lagGhostMapItem = new LagGhostMapItem();
				int num = fileStream.Read(array, 0, array.Length);
				if (num < array.Length)
				{
					break;
				}
				lagGhostMapItem.Load(array);
				MapList.Add(lagGhostMapItem);
			}
			fileStream.Close();
			ResultLabel = "Read succeed";
		}

		private void OnDelVTItem(object obj)
		{
			MapList.Remove(SelectedMapItem);
		}

		private void OnAddVTItem(object obj)
		{
			LagGhostMapItem lagGhostMapItem = writeItem.Clone();
			for (int i = 0; i < MapList.Count; i++)
			{
				if (MapList.ElementAt(i) == writeItem)
				{
					MapList[i] = lagGhostMapItem;
					return;
				}
			}
			MapList.Add(lagGhostMapItem);
		}
	}
}
