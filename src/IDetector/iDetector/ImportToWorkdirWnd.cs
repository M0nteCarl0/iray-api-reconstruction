using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class ImportToWorkdirWnd : Window, INotifyPropertyChanged, IComponentConnector
	{
		private int _nWidth;

		private int _nHeight;

		private string WarningColor = "#EE7600";

		private string importNoticeColor;

		internal TextBox _txtWorkdir;

		internal TextBox _txtImportPath;

		internal Label _txtNotice;

		internal Label _txtNoticeRed;

		internal Button btnImport;

		internal Button btnCancel;

		internal Button btnOpen;

		private bool _contentLoaded;

		public string ImportNoticeColor
		{
			get
			{
				return importNoticeColor;
			}
			set
			{
				importNoticeColor = value;
				NotifyPropertyChanged("ImportNoticeColor");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyPropertyChanged(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		public ImportToWorkdirWnd(string path, int nW, int nH)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			_nWidth = nW;
			_nHeight = nH;
			_txtWorkdir.Text = path;
			_txtImportPath.Text = "";
			_txtNotice.Content = "";
			_txtNoticeRed.Content = "";
			_txtNotice.Visibility = Visibility.Collapsed;
			_txtNoticeRed.Visibility = Visibility.Collapsed;
		}

		private void btnImport_Click(object sender, RoutedEventArgs e)
		{
			string text = _txtWorkdir.Text;
			long diskFreeSpace = Utility.GetDiskFreeSpace(Utility.GetDiskName(text));
			int num = _nWidth * _nHeight * 2 / 1024 / 1024 + 2;
			if (diskFreeSpace <= num)
			{
				_txtNotice.Visibility = Visibility.Collapsed;
				_txtNoticeRed.Visibility = Visibility.Visible;
				_txtNoticeRed.Content = "Disk available space error!";
			}
			else
			{
				try
				{
					string text2 = _txtImportPath.Text;
					string fileName = Path.GetFileName(text2);
					string destFileName = text + fileName;
					File.Copy(text2, destFileName, true);
				}
				catch (Exception ex)
				{
					_txtNotice.Visibility = Visibility.Collapsed;
					_txtNoticeRed.Visibility = Visibility.Visible;
					_txtNoticeRed.Content = "Copy failed!:" + ex.ToString();
					return;
				}
				importNoticeColor = "";
				_txtNotice.Visibility = Visibility.Visible;
				_txtNoticeRed.Visibility = Visibility.Collapsed;
				_txtNotice.Content = "Import success!";
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void btnOpen_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Files(*.gn; *.dft; *.lag;)|*.gn; *.dft; *.lag;)";
			if (openFileDialog.ShowDialog() != false)
			{
				string fileName = openFileDialog.FileName;
				_txtImportPath.Text = fileName;
				string fileName2 = Path.GetFileName(fileName);
				string path = _txtWorkdir.Text + fileName2;
				if (File.Exists(path))
				{
					_txtNotice.Visibility = Visibility.Collapsed;
					_txtNoticeRed.Visibility = Visibility.Visible;
					_txtNoticeRed.Content = "Notice: Over wirte exist file!";
				}
				else
				{
					_txtNotice.Content = "";
					_txtNoticeRed.Content = "";
					_txtNotice.Visibility = Visibility.Collapsed;
					_txtNoticeRed.Visibility = Visibility.Collapsed;
				}
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/importtoworkdirwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_txtWorkdir = (TextBox)target;
				break;
			case 2:
				_txtImportPath = (TextBox)target;
				break;
			case 3:
				_txtNotice = (Label)target;
				break;
			case 4:
				_txtNoticeRed = (Label)target;
				break;
			case 5:
				btnImport = (Button)target;
				btnImport.Click += btnImport_Click;
				break;
			case 6:
				btnCancel = (Button)target;
				btnCancel.Click += btnCancel_Click;
				break;
			case 7:
				btnOpen = (Button)target;
				btnOpen.Click += btnOpen_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
