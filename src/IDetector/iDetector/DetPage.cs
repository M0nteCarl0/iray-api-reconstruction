using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace iDetector
{
	public class DetPage : UserControl, IComponentConnector
	{
		private VelueConverter conveter;

		private IDetSubPage mCurSubPage;

		public ToolTipMsgDel ShowToolTip;

		private GenerateEventMsg CallbackEventMsg;

		private bool bInited;

		private Detector _instance;

		private List<UIElement> PageList;

		private IProgress WaitStopProgress;

		internal AcquirePage _acqPage;

		internal CalibratePage _caliPage;

		internal CalibrateEx2Page _caliEx2Page;

		internal DetCfgPage _detCfgPage;

		internal SdkCfgPage _sdkCfgPage;

		internal ExposeWndUserControl xExposeWnd;

		internal TextBox _txtSN;

		internal TextBox _txtTemperature;

		internal TextBox _txtState;

		internal TextBox _txtTask;

		internal ComboBox _comboMsg;

		internal DockPanel xBattery;

		internal Label xBatteryPower;

		private bool _contentLoaded;

		public IDetSubPage CurSubPage
		{
			get
			{
				return mCurSubPage;
			}
		}

		public Detector Instance
		{
			get
			{
				return _instance;
			}
			set
			{
				_instance = value;
				if (_instance != null)
				{
					base.DataContext = _instance;
					InitSubpageInstance();
					_instance.SubscribeCBEvent(OnSdkCallback);
				}
			}
		}

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public DetPage()
		{
			InitializeComponent();
			conveter = new VelueConverter();
			base.DataContext = _instance;
			Enm_PropertyName enm_PropertyName = Enm_PropertyName.Enm_SDK_STATE;
			_txtSN.SetBinding(TextBox.TextProperty, new Binding("Prop_Attr_UROM_SerialNo")
			{
				Mode = BindingMode.OneWay
			});
			_txtState.SetBinding(TextBox.TextProperty, new Binding("Prop_Attr_State")
			{
				Mode = BindingMode.OneWay,
				Converter = conveter,
				ConverterParameter = enm_PropertyName
			});
			_txtState.SetBinding(Control.BackgroundProperty, new Binding("Prop_Attr_State")
			{
				Mode = BindingMode.OneWay,
				Converter = conveter,
				ConverterParameter = enm_PropertyName
			});
			_txtTask.SetBinding(TextBox.TextProperty, new Binding("TASK")
			{
				Mode = BindingMode.OneWay
			});
			_txtTemperature.SetBinding(TextBox.TextProperty, new Binding("Prop_Attr_RdResult_T1")
			{
				Mode = BindingMode.OneWay
			});
			_acqPage.Visibility = Visibility.Visible;
			_caliPage.Visibility = Visibility.Collapsed;
			_caliEx2Page.Visibility = Visibility.Collapsed;
			_detCfgPage.Visibility = Visibility.Collapsed;
			_sdkCfgPage.Visibility = Visibility.Collapsed;
			mCurSubPage = _acqPage;
			ExposeWndUserControl exposeWndUserControl = xExposeWnd;
			exposeWndUserControl.CancelHandler = (Action)Delegate.Combine(exposeWndUserControl.CancelHandler, new Action(OnCancelExpWindow));
			_acqPage.StartExposeWindow += ShowExpWindow;
			_acqPage.StopExposeWindow += CloseExpWindow;
			AcquirePage acqPage = _acqPage;
			acqPage.ShowToolTip = (ToolTipMsgDel)Delegate.Combine(acqPage.ShowToolTip, new ToolTipMsgDel(ShowTipMessage));
			PageList = new List<UIElement>();
			PageList.Add(_acqPage);
			PageList.Add(_caliEx2Page);
			PageList.Add(_detCfgPage);
			PageList.Add(_sdkCfgPage);
			this.SdkEventHandler = ProcessEvent;
			foreach (UIElement page in PageList)
			{
				IDetSubPage detSubPage = page as IDetSubPage;
				detSubPage.MessageNotifyProxy += AddSdkInfo;
			}
			CallbackEventMsg = new GenerateEventMsg(_comboMsg);
			CallbackEventMsg.EnableLogCallbackEvent = true;
		}

		~DetPage()
		{
		}

		public void Release()
		{
			BindingOperations.ClearAllBindings(_txtSN);
			foreach (UIElement page in PageList)
			{
				IDetSubPage detSubPage = page as IDetSubPage;
				detSubPage.OnClose();
				detSubPage.MessageNotifyProxy -= AddSdkInfo;
			}
			if (Instance != null)
			{
				Instance.UnSubscribeCBEvent(OnSdkCallback);
			}
			PageList.Clear();
			GC.Collect();
		}

		public bool ShowPage(UIElement page)
		{
			if (mCurSubPage == null || page == null || !mCurSubPage.Exit())
			{
				return false;
			}
			IDetSubPage detSubPage = page as IDetSubPage;
			if (detSubPage != null)
			{
				detSubPage.Enter();
				mCurSubPage = detSubPage;
			}
			_acqPage.Visibility = ((!page.Equals(_acqPage)) ? Visibility.Collapsed : Visibility.Visible);
			_caliPage.Visibility = ((!page.Equals(_caliPage)) ? Visibility.Collapsed : Visibility.Visible);
			_caliEx2Page.Visibility = ((!page.Equals(_caliEx2Page)) ? Visibility.Collapsed : Visibility.Visible);
			_detCfgPage.Visibility = ((!page.Equals(_detCfgPage)) ? Visibility.Collapsed : Visibility.Visible);
			_sdkCfgPage.Visibility = ((!page.Equals(_sdkCfgPage)) ? Visibility.Collapsed : Visibility.Visible);
			return true;
		}

		private void InitStatusbar()
		{
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 32:
			case 37:
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 59:
			case 61:
			case 62:
			case 93:
				Instance.AttrChangingMonitor.AddRef(5037);
				xBatteryPower.SetBinding(ContentControl.ContentProperty, new Binding("Prop_Attr_Battery_Remaining")
				{
					Mode = BindingMode.OneWay
				});
				break;
			default:
				xBattery.Visibility = Visibility.Collapsed;
				break;
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			if (nEventID == 4 && (nParam1 == 2 || nParam1 == 8))
			{
				sender.AttrChangingMonitor.Update(false);
			}
			CallbackEventMsg.AddSdkInfo(sender.ID, (Enm_EventLevel)nEventLevel, nEventID, nParam1, nParam2, strMsg);
			switch (nEventID)
			{
			case 2003:
				ShowTipMessage(sender.Prop_Attr_UROM_SerialNo + ": " + strMsg);
				break;
			case 2004:
				ShowTipMessage(sender.Prop_Attr_UROM_SerialNo + ": Real framerate isn't corresponds to setting value!");
				break;
			case 4:
				sender.ForceUpdatePropertyChanged("Prop_Attr_State", 5002);
				switch (nParam1)
				{
				case 2:
					Instance.TASK = "No Task";
					break;
				case 1005:
					if (WaitStopProgress != null)
					{
						WaitStopProgress.StopTask();
					}
					break;
				case 4:
					CloseExpWindow();
					break;
				}
				break;
			case 5:
				sender.ForceUpdatePropertyChanged("Prop_Attr_State", 5002);
				if (nParam1 == 2)
				{
				}
				break;
			case 6:
				CloseExpWindow();
				break;
			case 1005:
				if (_acqPage.Visibility == Visibility.Visible)
				{
					StartExpWindow(sender, nParam1 / 1000);
				}
				break;
			case 1004:
				if (_acqPage.Visibility == Visibility.Visible)
				{
				}
				break;
			case 1008:
				CloseExpWindow();
				break;
			case 1001:
			case 1002:
				CloseExpWindowAfterRecvImage(sender);
				break;
			}
		}

		private void StartExpWindow(Detector sender, int delayTimeSecond)
		{
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 68:
			case 73:
			case 74:
			case 87:
				if (delayTimeSecond >= 1)
				{
					xExposeWnd.Show(delayTimeSecond, false);
				}
				return;
			}
			if (Instance.Prop_Attr_UROM_TriggerMode == 1)
			{
				if (0 > delayTimeSecond && 1 == Instance.Prop_Attr_UROM_InnerSubFlow)
				{
					xExposeWnd.Show(0, true);
				}
				else if (delayTimeSecond >= 1)
				{
					xExposeWnd.Show(delayTimeSecond, true);
				}
			}
		}

		private void OnCancelExpWindow()
		{
			int num = Instance.Invoke(1005);
			if (num != 0 && 1 != num)
			{
				MessageBox.Show("Cancel ExpWindow failed! Err = " + ErrorHelper.GetErrorDesp(num));
			}
		}

		private void ShowExpWindow()
		{
			if (Instance.Prop_Attr_UROM_ProductNo == 61 && Instance.Prop_Attr_UROM_TriggerMode == 2)
			{
				xExposeWnd.Show(180, false);
				return;
			}
			int num = Instance.Prop_Attr_UROM_ExpWindowTime / 1000;
			if (num >= 1 && Instance.Prop_Attr_UROM_TriggerMode == 2)
			{
				xExposeWnd.Show(num, false);
			}
		}

		private void CloseExpWindowAfterRecvImage(Detector sender)
		{
			if (sender.Prop_Attr_UROM_ExpMode != 2 && sender.Prop_Attr_UROM_ExpMode != 1)
			{
				xExposeWnd.Close();
			}
		}

		private void CloseExpWindow()
		{
			xExposeWnd.Close();
		}

		public void InitSubPageParam()
		{
			if (!bInited)
			{
				bInited = true;
				Instance.SubscribeCBEvent(OnSdkCallback);
				InitStatusbar();
				_caliEx2Page.Initialize();
				_acqPage.InitImageParam();
				_detCfgPage.InitPanel();
				Instance.AttrChangingMonitor.AddRef(5002);
				Instance.AttrChangingMonitor.AddRef(5017);
				Instance.AttrChangingMonitor.AddRef(5006);
			}
		}

		private void InitSubpageInstance()
		{
			_acqPage.Instance = _instance;
			_caliEx2Page.Instance = _instance;
			_detCfgPage.Instance = _instance;
			_sdkCfgPage.Instance = _instance;
		}

		public void AddSdkInfo(string info)
		{
			CallbackEventMsg.AddSdkInfo(info);
		}

		public void ClosePage()
		{
			ExposeWndUserControl exposeWndUserControl = xExposeWnd;
			exposeWndUserControl.CancelHandler = (Action)Delegate.Remove(exposeWndUserControl.CancelHandler, new Action(OnCancelExpWindow));
			_acqPage.StartExposeWindow -= ShowExpWindow;
			_acqPage.StopExposeWindow -= CloseExpWindow;
			AcquirePage acqPage = _acqPage;
			acqPage.ShowToolTip = (ToolTipMsgDel)Delegate.Remove(acqPage.ShowToolTip, new ToolTipMsgDel(ShowTipMessage));
			foreach (UIElement page in PageList)
			{
				IDetSubPage detSubPage = page as IDetSubPage;
				if (detSubPage != null)
				{
					detSubPage.OnClose();
				}
			}
			if (Instance != null)
			{
				Instance.UnSubscribeCBEvent(OnSdkCallback);
				Instance.AttrChangingMonitor.ReleaseRef(5002);
				Instance.AttrChangingMonitor.ReleaseRef(5017);
				Instance.AttrChangingMonitor.ReleaseRef(5006);
			}
			bInited = false;
			this.SdkEventHandler = null;
			BindingOperations.ClearAllBindings(_txtSN);
			BindingOperations.ClearAllBindings(_txtState);
			BindingOperations.ClearAllBindings(_txtTask);
			BindingOperations.ClearAllBindings(_txtTemperature);
		}

		public bool IsInAcquireMode()
		{
			return false;
		}

		private void ShowTipMessage(string info)
		{
			if (ShowToolTip != null)
			{
				ShowToolTip(info);
			}
		}

		public void OnLocationChanged()
		{
			if (_acqPage != null)
			{
				_acqPage.ImageViewCtrl.OnLocationChanged();
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_acqPage = (AcquirePage)target;
				break;
			case 2:
				_caliPage = (CalibratePage)target;
				break;
			case 3:
				_caliEx2Page = (CalibrateEx2Page)target;
				break;
			case 4:
				_detCfgPage = (DetCfgPage)target;
				break;
			case 5:
				_sdkCfgPage = (SdkCfgPage)target;
				break;
			case 6:
				xExposeWnd = (ExposeWndUserControl)target;
				break;
			case 7:
				_txtSN = (TextBox)target;
				break;
			case 8:
				_txtTemperature = (TextBox)target;
				break;
			case 9:
				_txtState = (TextBox)target;
				break;
			case 10:
				_txtTask = (TextBox)target;
				break;
			case 11:
				_comboMsg = (ComboBox)target;
				break;
			case 12:
				xBattery = (DockPanel)target;
				break;
			case 13:
				xBatteryPower = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
