using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class UpdateCorrFileWnd : Window, IComponentConnector
	{
		public enum Enm_WndType
		{
			Download = 1,
			UpLoad,
			Select,
			Query
		}

		private class ListItem
		{
			public string Name
			{
				get;
				set;
			}

			public int Value
			{
				get;
				set;
			}
		}

		private class FileFilter
		{
			public string defaultExt;

			public string filter;

			public FileFilter(string _ext, string _filter)
			{
				defaultExt = _ext;
				filter = _filter;
			}
		}

		private int filetype;

		public bool DiagResult;

		private Enm_WndType mType;

		private int FactoryAuthority;

		private bool bEnableAutoModifyFileName;

		private readonly Dictionary<int, string> fileKeyName = new Dictionary<int, string>
		{
			{
				0,
				""
			},
			{
				2,
				"_Gain"
			},
			{
				4,
				"_Defect"
			},
			{
				5,
				"_Lag"
			}
		};

		internal Grid PanelGrid;

		internal RowDefinition xPathRow;

		internal RowDefinition xTypeRow;

		internal RowDefinition xIndexRow;

		internal RowDefinition xDespRow;

		internal RowDefinition xOkRow;

		internal TextBox xFilePath;

		internal ComboBox xFileType;

		internal DigitalTextBox xFileIndex;

		internal Label xDespLabel;

		internal TextBox xFileDesp;

		private bool _contentLoaded;

		public int FileIndex
		{
			get
			{
				return xFileIndex.Value;
			}
		}

		public int FileType
		{
			get
			{
				return filetype;
			}
			set
			{
				filetype = value;
				SetFileType((Enm_FileTypes)value);
			}
		}

		public string FilePath
		{
			get
			{
				return xFilePath.Text;
			}
		}

		public string DespInfo
		{
			get
			{
				return xFileDesp.Text;
			}
		}

		public string SubCaliPath
		{
			get;
			set;
		}

		public string FileSizeInfo
		{
			get;
			set;
		}

		public string ConfigFileName
		{
			get;
			set;
		}

		public UpdateCorrFileWnd(Enm_WndType type = Enm_WndType.Download, int uploadIndex = 0)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			SdkInterface.GetAuthority(ref FactoryAuthority);
			mType = type;
			xFileType.SelectedIndex = 0;
			InitFileTypeCombox();
			switch (type)
			{
			case Enm_WndType.Download:
				base.Title = "Download file";
				break;
			case Enm_WndType.UpLoad:
				base.Title = "Upload file";
				xFileType.IsEnabled = true;
				base.Height -= xDespRow.Height.Value;
				xDespRow.MaxHeight = 0.0;
				xFileIndex.IsEnabled = false;
				xFileIndex.Text = uploadIndex.ToString();
				break;
			case Enm_WndType.Select:
				xFileType.IsEnabled = true;
				base.Title = "Select file";
				base.Height = base.Height - xDespRow.Height.Value - xPathRow.Height.Value;
				xPathRow.MaxHeight = 0.0;
				xDespRow.MaxHeight = 0.0;
				break;
			case Enm_WndType.Query:
				xFileType.IsEnabled = true;
				base.Title = "Select file";
				base.Height = base.Height - xDespRow.Height.Value - xPathRow.Height.Value - xIndexRow.Height.Value;
				xIndexRow.MaxHeight = 0.0;
				xPathRow.MaxHeight = 0.0;
				xDespRow.MaxHeight = 0.0;
				break;
			}
		}

		private void InitFileTypeCombox()
		{
			ArrayList arrayList = new ArrayList();
			arrayList.Capacity = 2;
			xFileType.ItemsSource = arrayList;
			xFileType.DisplayMemberPath = "Name";
			xFileType.SelectedValue = "Value";
			arrayList.Add(new ListItem
			{
				Name = "Enm_File_Gain",
				Value = 2
			});
			arrayList.Add(new ListItem
			{
				Name = "Enm_File_Defect",
				Value = 4
			});
			arrayList.Add(new ListItem
			{
				Name = "Enm_File_Lag",
				Value = 5
			});
		}

		private void OnOpenPath(object sender, RoutedEventArgs e)
		{
			Microsoft.Win32.FileDialog fileDialog;
			if (Enm_WndType.Download == mType)
			{
				fileDialog = new OpenFileDialog();
			}
			else
			{
				fileDialog = new SaveFileDialog();
				fileDialog.FileName = IniParser.GetPrivateProfileString("UploadFileName", FileSizeInfo + fileKeyName[FileType], "", ConfigFileName);
			}
			fileDialog.InitialDirectory = SubCaliPath;
			if (Enm_WndType.Download == mType)
			{
				Dictionary<int, int> dictionary = new Dictionary<int, int>();
				dictionary.Add(0, 0);
				dictionary.Add(2, 1);
				dictionary.Add(4, 2);
				dictionary.Add(5, 3);
				Dictionary<int, int> dictionary2 = dictionary;
				fileDialog.DefaultExt = ".gn | .dft | .lag";
				fileDialog.Filter = "gain files(*.gn)|*.gn|defect files(*.dft)|*.dft|lag files(*.lag)|*.lag";
				fileDialog.FilterIndex = dictionary2[FileType];
			}
			else
			{
				Dictionary<int, FileFilter> dictionary3 = new Dictionary<int, FileFilter>();
				dictionary3.Add(0, null);
				dictionary3.Add(2, new FileFilter(".gn", "gain files(*.gn)|*.gn"));
				dictionary3.Add(4, new FileFilter(".dft", "defect files(*.dft)|*.dft"));
				dictionary3.Add(5, new FileFilter(".lag", "lag files(*.lag)|*.lag"));
				Dictionary<int, FileFilter> dictionary4 = dictionary3;
				fileDialog.DefaultExt = dictionary4[FileType].defaultExt;
				fileDialog.Filter = dictionary4[FileType].filter;
				fileDialog.FilterIndex = 0;
			}
			if (fileDialog.ShowDialog() == true)
			{
				xFilePath.Text = fileDialog.FileName;
			}
		}

		private void OnPathChanged(object sender, RoutedEventArgs e)
		{
			if (xFilePath.Text.Length > 0)
			{
				SetFileType(xFilePath.Text);
			}
		}

		private void SetFileType(Enm_FileTypes fileType)
		{
			switch (fileType)
			{
			case (Enm_FileTypes)3:
				break;
			case Enm_FileTypes.Enm_File_Gain:
				xFileType.Text = "Enm_File_Gain";
				break;
			case Enm_FileTypes.Enm_File_Defect:
				xFileType.Text = "Enm_File_Defect";
				break;
			case Enm_FileTypes.Enm_File_Lag:
				xFileType.Text = "Enm_File_Lag";
				break;
			case Enm_FileTypes.Enm_File_Offset:
				xFileType.Text = "Enm_File_Offset";
				break;
			}
		}

		private void SetFileType(string fileName)
		{
			bEnableAutoModifyFileName = false;
			string text = fileName.Substring(fileName.LastIndexOf(".") + 1, fileName.Length - fileName.LastIndexOf(".") - 1);
			if (text.ToLower().Equals("gn"))
			{
				xFileType.Text = "Enm_File_Gain";
			}
			else if (text.ToLower().Equals("dft"))
			{
				xFileType.Text = "Enm_File_Defect";
			}
			else if (text.ToLower().Equals("lag"))
			{
				xFileType.Text = "Enm_File_Lag";
			}
			else if (text.ToLower().Equals("off"))
			{
				xFileType.Text = "Enm_File_Offset";
			}
			bEnableAutoModifyFileName = true;
		}

		private void OnOK(object sender, RoutedEventArgs e)
		{
			if (Enm_WndType.Select != mType && Enm_WndType.Query != mType && xFilePath.Text.Length == 0)
			{
				MessageBox.Show("Please input file path!");
				return;
			}
			if (xFileIndex.Length == 0 && Enm_WndType.Query != mType)
			{
				MessageBox.Show("File Index cannot be empty!");
				return;
			}
			DiagResult = true;
			Close();
		}

		private void OnSelectFileType(object sender, SelectionChangedEventArgs e)
		{
			ListItem listItem = xFileType.SelectedItem as ListItem;
			FileType = listItem.Value;
			if (bEnableAutoModifyFileName)
			{
				string text = xFilePath.Text;
				string text2 = text.Substring(0, text.LastIndexOf("\\") + 1);
				string lpDefault = text.Substring(text.LastIndexOf("\\") + 1, text.Length - text2.Length);
				xFilePath.Text = text2 + IniParser.GetPrivateProfileString("UploadFileName", FileSizeInfo + fileKeyName[FileType], lpDefault, ConfigFileName);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/updatecorrfilewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				PanelGrid = (Grid)target;
				break;
			case 2:
				xPathRow = (RowDefinition)target;
				break;
			case 3:
				xTypeRow = (RowDefinition)target;
				break;
			case 4:
				xIndexRow = (RowDefinition)target;
				break;
			case 5:
				xDespRow = (RowDefinition)target;
				break;
			case 6:
				xOkRow = (RowDefinition)target;
				break;
			case 7:
				xFilePath = (TextBox)target;
				xFilePath.TextChanged += OnPathChanged;
				break;
			case 8:
				((Button)target).Click += OnOpenPath;
				break;
			case 9:
				xFileType = (ComboBox)target;
				xFileType.SelectionChanged += OnSelectFileType;
				break;
			case 10:
				xFileIndex = (DigitalTextBox)target;
				break;
			case 11:
				xDespLabel = (Label)target;
				break;
			case 12:
				xFileDesp = (TextBox)target;
				break;
			case 13:
				((Button)target).Click += OnOK;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
