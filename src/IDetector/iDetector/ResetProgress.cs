namespace iDetector
{
	internal class ResetProgress : IProgress
	{
		private MessageWnd messageBox;

		private UITimer timer;

		private int timeOut;

		public bool IsTaskRunning
		{
			get;
			set;
		}

		public ResetProgress(int timeoutSecond)
		{
			messageBox = new MessageWnd("Reset");
			timer = new UITimer(1000, UpdateStatus);
			timeOut = timeoutSecond;
		}

		public int RunTask()
		{
			IsTaskRunning = true;
			timer.Start();
			SetStatus("Reseting detector: remain about:" + timeOut.ToString() + " s");
			messageBox.ShowDialog();
			return 0;
		}

		public void StopTask()
		{
			TerminateTask();
		}

		public void SetStatus(string status)
		{
			messageBox.SetMessage(status);
		}

		private void TerminateTask()
		{
			messageBox.Close();
			IsTaskRunning = false;
			timer.Stop();
		}

		private void UpdateStatus()
		{
			if (timeOut-- <= 0)
			{
				TerminateTask();
			}
			else
			{
				SetStatus("Reseting detector: remain about:" + timeOut.ToString() + " s");
			}
		}
	}
}
