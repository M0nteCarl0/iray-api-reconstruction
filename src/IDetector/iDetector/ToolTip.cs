using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace iDetector
{
	public class ToolTip : UserControl, IComponentConnector
	{
		private UITimer mTimer;

		internal TextBlock xDispInfo;

		private bool _contentLoaded;

		public new string Content
		{
			set
			{
				xDispInfo.Text = value;
			}
		}

		public ToolTip()
		{
			InitializeComponent();
			mTimer = new UITimer(10000, OnTimer);
			base.Visibility = Visibility.Hidden;
		}

		public void ShowTip(string info)
		{
			Content = info;
			base.Visibility = Visibility.Visible;
			mTimer.Start();
		}

		private void OnTimer()
		{
			Close();
		}

		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			Close();
		}

		public void Close()
		{
			mTimer.Stop();
			base.Visibility = Visibility.Hidden;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/tooltip.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xDispInfo = (TextBlock)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
