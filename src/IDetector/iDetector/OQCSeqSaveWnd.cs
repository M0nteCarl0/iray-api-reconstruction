using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class OQCSeqSaveWnd : Window, IComponentConnector
	{
		private bool result;

		private int EachFrameSizeMB;

		internal DigitalTextBox _SaveFrames;

		internal CheckBox EnableBufferCine;

		internal TextBox _FilePath;

		private bool _contentLoaded;

		public int Frames
		{
			get
			{
				return _SaveFrames.Value;
			}
			private set
			{
				_SaveFrames.Text = value.ToString();
			}
		}

		public string Path
		{
			get
			{
				return _FilePath.Text;
			}
			private set
			{
				_FilePath.Text = value;
			}
		}

		public bool EnableSave
		{
			get
			{
				if (EnableBufferCine.IsChecked == true)
				{
					return result;
				}
				return false;
			}
		}

		public bool DlgResult
		{
			get
			{
				return result;
			}
		}

		public OQCSeqSaveWnd(int frameSizeMB)
		{
			InitializeComponent();
			EachFrameSizeMB = frameSizeMB;
			base.Owner = Application.Current.MainWindow;
			base.ShowInTaskbar = false;
		}

		public bool ShowDlg(OQCSeqSaveParam para)
		{
			Path = para.Path;
			Frames = ((para.TotalFrames == 0) ? 100 : para.TotalFrames);
			ShowDialog();
			return DlgResult;
		}

		private void OnOk(object sender, RoutedEventArgs e)
		{
			result = false;
			if (_FilePath.Text.Length == 0)
			{
				MessageBox.Show("File path cannot be empty!");
				return;
			}
			if (Frames == 0)
			{
				MessageBox.Show("Frame number cannot be empty or 0!");
				return;
			}
			if (Frames > 1000)
			{
				MessageBox.Show("Please input a number 1~1000!");
				return;
			}
			long diskFreeSpace = Utility.GetDiskFreeSpace(Utility.GetDiskName(_FilePath.Text));
			if (EnableBufferCine.IsChecked == true && diskFreeSpace <= EachFrameSizeMB * Frames)
			{
				MessageBox.Show("No enough space!");
				return;
			}
			if (EnableBufferCine.IsChecked == false)
			{
				result = true;
				Close();
			}
			else if (Utility.IsFileCanbeWritten(_FilePath.Text))
			{
				if (Utility.IsFileExisted(_FilePath.Text))
				{
					if (MessageBoxResult.Yes != MessageBox.Show(string.Format("{0} already exists.\r\nDo you want to replace it?", _FilePath.Text), "Save As", MessageBoxButton.YesNo, MessageBoxImage.Question))
					{
						return;
					}
					Utility.DeleteFile(_FilePath.Text);
				}
				result = true;
				Close();
			}
			else
			{
				MessageBox.Show("The file is not writable!");
			}
			if (result)
			{
				_FilePath.Text = Utility.AddFileNameExtension(_FilePath.Text, "raw");
			}
		}

		private void OnOpenPath(object sender, RoutedEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.DefaultExt = ".raw";
			saveFileDialog.Filter = "Data file(*.raw)|*.raw";
			saveFileDialog.OverwritePrompt = false;
			if (saveFileDialog.ShowDialog() == true)
			{
				_FilePath.Text = saveFileDialog.FileName;
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/oqcseqsavewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_SaveFrames = (DigitalTextBox)target;
				break;
			case 2:
				EnableBufferCine = (CheckBox)target;
				break;
			case 3:
				((Button)target).Click += OnOk;
				break;
			case 4:
				_FilePath = (TextBox)target;
				break;
			case 5:
				((Button)target).Click += OnOpenPath;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
