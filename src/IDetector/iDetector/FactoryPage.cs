using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class FactoryPage : UserControl, IDetSubPage, IComponentConnector
	{
		private readonly string INIAPPName = "FactoryParameters";

		private readonly string INIAPPName_New = "FactoryAttributes";

		private Dictionary<int, string> DictFactoryAttr = new Dictionary<int, string>();

		private Detector _det;

		private bool _bIsLoaded;

		private List<int> _monitorAttrs;

		internal StackPanel _attrsPanel;

		internal CheckBox xSelectFileCheckbox;

		private bool _contentLoaded;

		public bool bHandled
		{
			get;
			set;
		}

		private bool bCanExit
		{
			get;
			set;
		}

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
				_det.SdkCallbackEvent += OnSdkCallback;
				InitAttribute();
			}
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		public FactoryPage()
		{
			InitializeComponent();
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				_bIsLoaded = false;
				base.Loaded += OnLoaded;
				base.IsVisibleChanged += OnIsVisibleChanged;
				_monitorAttrs = new List<int>();
				BuildDictionary();
			}
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			InitAttribute();
		}

		private void InitAttribute()
		{
			if (_bIsLoaded)
			{
				return;
			}
			if (Instance == null)
			{
				EnableView(false);
				return;
			}
			AddGroupDescription("Product Information:");
			AddItem("Product No", 3001, 0);
			AddItem("Sub Product No", 3061, 3561);
			AddItem("Serial No", 3062, 3562);
			AddItem("Main Version", 3002, 0);
			AddItem("Read Version", 3003, 0);
			AddItem("Master Build Time", 3008, 0);
			AddItem("Slave Build Time", 3009, 0);
			AddItem("MCU Build Time", 3010, 0);
			AddItem("CBX Build Time", 3060, 0);
			AddGroupDescription("Connection setting:");
			AddItem("Src IP", 3036, 3536);
			AddItem("Src MAC", 3037, 3537);
			AddItem("Dest Port", 3038, 3538);
			AddItem("Dest IP", 3039, 3539);
			AddItem("Dest MAC", 3040, 3540);
			if (45 == Instance.Prop_Cfg_ProductNo)
			{
				AddItem("Sync-Box IP", 3041, 3541);
			}
			AddGroupDescription("Mode:");
			AddItem("Dynamic Mode", 3020, 0);
			AddItem("Trigger Mode", 3019, 3519);
			AddItem("Fluro Sync", 3034, 3534);
			AddItem("Binning Mode", 3047, 3547);
			AddItem("Zoom Mode", 3014, 3514);
			AddGroupDescription("Parameter setting:");
			AddItem("Prep CapMode", 3032, 3532);
			AddItem("Self CapEnable", 3033, 3533);
			AddItem("Row Pre Delay Time", 3011, 3511);
			AddItem("Row Post Delay Time", 3012, 3512);
			AddItem("Integrate Time", 3013, 3513);
			AddItem("Level Signal", 3015, 3515);
			AddItem("Self Clear", 3016, 3516);
			AddItem("Self Clear Span Time", 3017, 3517);
			AddItem("Sequence Interval Time", 3018, 3518);
			AddItem("Tube Ready Time", 3021, 3521);
			AddItem("Set Delay Time", 3023, 3523);
			AddItem("Exp Window Time", 3025, 3525);
			AddItem("Sync Exp Time", 3027, 0);
			AddItem("Exp Time Valid Percent", 3066, 3566);
			AddItem("VT", 3029, 3529);
			if (46 == Instance.Prop_Cfg_ProductNo)
			{
				AddItem("FullWell", 3085, 3585);
			}
			else
			{
				AddItem("PGA", 3030, 3530);
			}
			AddItem("Pre Mode", 3044, 3544);
			AddItem("Offset Type", 3045, 3545);
			AddItem("PowSeriesCorrect Enable", 3079, 3579);
			AddItem("Acquire Delay Time", 3046, 3546);
			AddItem("Clear Times", 3080, 3580);
			AddItem("Exp Mode", 3050, 3550);
			AddItem("AEC Pre Main TimeGap", 3051, 3551);
			AddItem("Dyna Offset Gap Time", 3052, 3552);
			AddItem("Dyna Offset Mode", 3053, 3553);
			AddItem("Image Pkt Gap Time", 3054, 3554);
			AddItem("Out Mode Cap Trigger", 3069, 3569);
			AddItem("Hvg Prep On", 3055, 3555);
			AddItem("Hvg XRay Enable", 3056, 3556);
			AddItem("Hvg XRay On", 3057, 3557);
			AddItem("Hvg XRay SyncOut", 3058, 3558);
			AddItem("Hvg XRay SyncIn", 3059, 3559);
			AddItem("Image Channel", 3063, 3563);
			AddItem("Image Channel Protocol", 3064, 3564);
			switch (Instance.Prop_Cfg_ProductNo)
			{
			case 45:
				AddItem("Auto Sleep Idle Time", 3071, 3571);
				break;
			case 32:
			case 37:
			case 42:
			case 59:
			case 60:
			case 62:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				AddFsyncAttributes();
				break;
			case 41:
			case 61:
				AddItem("Auto Sleep Idle Time", 3071, 3571);
				AddFsyncAttributes();
				break;
			case 51:
			case 52:
				AddItem("Inner SubFlow", 3086, 3586);
				AddItem("Auto Sleep Idle Time", 3071, 3571);
				AddFsyncAttributes();
				break;
			case 46:
			case 68:
			case 73:
			case 74:
			case 87:
				AddItem("ROIColStartPos", 3081, 3581);
				AddItem("ROIRowStartPos", 3083, 3583);
				AddItem("ROIColEndPos", 3082, 3582);
				AddItem("ROIRowEndPos", 3084, 3584);
				AddItem("HighPrecisionSyncExpTime", 3028, 0);
				AddItem("HighPrecisionSequenceIntervalTime", 3022, 3522);
				break;
			}
			for (int i = 0; i < 16; i++)
			{
				string strLabel = "Test Param" + (i + 1).ToString();
				AddItem(strLabel, 3232 + i, 3732 + i);
			}
			for (int j = 0; j < 32; j++)
			{
				string strLabel2 = "Debug Param" + (j + 1).ToString();
				AddItem(strLabel2, 3200 + j, 3700 + j);
			}
			_det.AttrChangingMonitor.AddRef(Enumerable.ToArray(_monitorAttrs));
			_bIsLoaded = true;
			bCanExit = true;
		}

		private void AddFsyncAttributes()
		{
			AddItem("Fsync Paral Clear Times", 3072, 3572);
			AddItem("Fsync Fast Scan CPV Cycle", 3073, 3573);
			AddItem("Fsync Trigger Check Timeout", 3074, 3574);
			AddItem("Fsync Segment Threshold", 3075, 3575);
			AddItem("Fsync Line Threshold", 3076, 3576);
			AddItem("Fsync False Trigger Unresponse Stage Time", 3077, 3577);
			AddItem("Fsync Paral Clear Line", 3078, 3578);
		}

		private void Button_Click_LoadParas(object sender, RoutedEventArgs e)
		{
			string text = Utility.GetCurrentPath() + "\\FactoryConfig.ini";
			if (Instance != null)
			{
				text = Instance.Prop_Attr_WorkDir + "FactoryConfig.ini";
			}
			if (xSelectFileCheckbox.IsChecked == true)
			{
				OpenFileDialog openFileDialog = new OpenFileDialog();
				openFileDialog.DefaultExt = ".ini";
				openFileDialog.Filter = "ini file(*.ini)|*.ini";
				if (openFileDialog.ShowDialog() == false)
				{
					return;
				}
				text = openFileDialog.FileName;
			}
			if (!Utility.IsFileExisted(text))
			{
				MessageBox.Show(string.Format("File not existed:{0}", text));
			}
			else
			{
				foreach (UIElement child in _attrsPanel.Children)
				{
					CtrAttrEditor ctrAttrEditor;
					if ((ctrAttrEditor = (child as CtrAttrEditor)) != null && ctrAttrEditor.WriteAttrID != 3562)
					{
						StringBuilder stringBuilder = new StringBuilder(128);
						int privateProfileString = IniParser.GetPrivateProfileString(INIAPPName_New, DictFactoryAttr[ctrAttrEditor.ReadAttrID], null, stringBuilder, 128, text);
						if (privateProfileString == 0)
						{
							privateProfileString = IniParser.GetPrivateProfileString(INIAPPName, ctrAttrEditor._dispName.Text.ToString(), null, stringBuilder, 128, text);
						}
						if (privateProfileString == 0)
						{
							AttrInfo pInfo = default(AttrInfo);
							if (SdkInterface.GetAttrInfo(_det.ID, ctrAttrEditor.WriteAttrID, ref pInfo) != 0)
							{
								continue;
							}
							stringBuilder.Clear();
							switch (pInfo.nDataType)
							{
							case IRAY_VAR_TYPE.IVT_INT:
								if (pInfo.bIsEnum == 0)
								{
									stringBuilder.Append(pInfo.fMinValue.ToString());
								}
								break;
							case IRAY_VAR_TYPE.IVT_FLT:
								stringBuilder.Append(pInfo.fMinValue.ToString());
								break;
							case IRAY_VAR_TYPE.IVT_STR:
								stringBuilder.Append("");
								break;
							}
						}
						if (ctrAttrEditor._comboEdit.Visibility == Visibility.Visible)
						{
							foreach (EnumDisplayItem item in (IEnumerable)ctrAttrEditor._comboEdit.Items)
							{
								if (item.Label == stringBuilder.ToString())
								{
									ctrAttrEditor._comboEdit.SelectedItem = item;
									break;
								}
							}
						}
						else
						{
							ctrAttrEditor._txtEdit.Text = stringBuilder.ToString();
						}
					}
				}
			}
		}

		private void Button_Click_SaveParas(object sender, RoutedEventArgs e)
		{
			SaveFileDialog saveFileDialog = new SaveFileDialog();
			saveFileDialog.DefaultExt = ".ini";
			saveFileDialog.Filter = "ini file(*.ini)|*.ini";
			saveFileDialog.FileName = "FactoryConfig.ini";
			if (saveFileDialog.ShowDialog() == true)
			{
				foreach (UIElement child in _attrsPanel.Children)
				{
					CtrAttrEditor ctrAttrEditor;
					if ((ctrAttrEditor = (child as CtrAttrEditor)) != null)
					{
						IniParser.WritePrivateProfileString(INIAPPName_New, DictFactoryAttr[ctrAttrEditor.ReadAttrID], ctrAttrEditor._txtRead.Text.ToString(), saveFileDialog.FileName);
					}
				}
			}
		}

		private CtrAttrEditor AddItem(string strLabel, int nReadAttrID, int nWriteAttrID, bool bIsBooleanVal = false)
		{
			CtrAttrEditor ctrAttrEditor = new CtrAttrEditor(strLabel, nReadAttrID, nWriteAttrID, _det);
			ctrAttrEditor.Height = 32.0;
			AttrEditorStyle ctrlStyle = default(AttrEditorStyle);
			ctrlStyle.nLabelColWidth = ((strLabel != null && strLabel.Length != 0) ? 200 : 0);
			if (CtrAttrEditor.MeasureTextWidth(strLabel, ctrAttrEditor.FontSize, ctrAttrEditor.FontFamily.ToString()) > (double)ctrlStyle.nLabelColWidth)
			{
				ctrAttrEditor.Height = 40.0;
			}
			ctrlStyle.nReadColWidth = ((nReadAttrID != 0) ? 180 : 0);
			ctrlStyle.nWriteColWidth = 0;
			ctrlStyle.nEditorColWidth = ((nWriteAttrID != 0) ? 190 : 0);
			ctrlStyle.bCheckBoxForIntType = bIsBooleanVal;
			ctrlStyle.nBtnColWidth = 0;
			ctrAttrEditor.SetCtrlStyle(ctrlStyle);
			_attrsPanel.Children.Add(ctrAttrEditor);
			if (nReadAttrID > 0)
			{
				_monitorAttrs.Add(nReadAttrID);
			}
			if (nWriteAttrID > 0)
			{
				_monitorAttrs.Add(nWriteAttrID);
			}
			return ctrAttrEditor;
		}

		private void AddGroupDescription(string strText)
		{
			TextBox textBox = new TextBox();
			textBox.Text = strText;
			textBox.IsEnabled = false;
			_attrsPanel.Children.Add(textBox);
		}

		private bool AcceptAll()
		{
			foreach (UIElement child in _attrsPanel.Children)
			{
				if (child is CtrAttrEditor)
				{
					CtrAttrEditor ctrAttrEditor = child as CtrAttrEditor;
					if (!ctrAttrEditor.Accept())
					{
						return false;
					}
				}
			}
			bool flag = false;
			Enm_TriggerMode prop_Attr_FROM_TriggerMode_W = (Enm_TriggerMode)_det.Prop_Attr_FROM_TriggerMode_W;
			switch (_det.Prop_Attr_FROM_ProductNo)
			{
			case 51:
			case 52:
			{
				if (prop_Attr_FROM_TriggerMode_W != Enm_TriggerMode.Enm_TriggerMode_Inner && (prop_Attr_FROM_TriggerMode_W != Enm_TriggerMode.Enm_TriggerMode_Prep || 1 != _det.Prop_Attr_FROM_PrepCapMode_W))
				{
					break;
				}
				bool flag2 = Enm_TriggerMode.Enm_TriggerMode_Inner == prop_Attr_FROM_TriggerMode_W && 1 == _det.Prop_Attr_FROM_InnerSubFlow_W;
				switch (flag2 ? _det.Prop_Attr_FROM_SetDelayTime_W : _det.Prop_Attr_FROM_ExpWindowTime_W)
				{
				default:
					if (flag2)
					{
						MessageBox.Show("SetDelayTime must among {700,1200,2200,3200,4200} in this mode.");
					}
					else
					{
						MessageBox.Show("ExpWindowTime must among {700,1200,2200,3200,4200} in this mode.");
					}
					flag = true;
					break;
				case 700:
				case 1200:
				case 2200:
				case 3200:
				case 4200:
					break;
				}
				break;
			}
			}
			if (!flag)
			{
				int length = _det.Prop_Attr_FROM_SerialNo_W.Length;
				switch (_det.Prop_Attr_FROM_ProductNo)
				{
				case 41:
				case 42:
				case 51:
				case 52:
				case 60:
				case 61:
				case 72:
				case 80:
				case 88:
				case 91:
				case 93:
					if (13 != length && 19 != length)
					{
						MessageBox.Show("Invalid SN!");
						flag = true;
					}
					break;
				}
			}
			if (flag)
			{
				return false;
			}
			return true;
		}

		private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			bool bIsLoaded = _bIsLoaded;
		}

		private void Button_Click_ReadROM(object sender, RoutedEventArgs e)
		{
			int num = SdkInterface.Invoke(_det.ID, 2003, null, 0);
			if (num != 0 && num != 1)
			{
				MessageBox.Show("Read ROM failed! Err = " + ErrorHelper.GetErrorDesp(num));
			}
			else
			{
				EnableView(false);
			}
		}

		private void Button_Click_WriteROM(object sender, RoutedEventArgs e)
		{
			if (AcceptAll())
			{
				int num = SdkInterface.Invoke(_det.ID, 2004, null, 0);
				if (num != 0 && num != 1)
				{
					MessageBox.Show("Write ROM failed! Err = " + ErrorHelper.GetErrorDesp(num));
				}
				else
				{
					EnableView(false);
				}
			}
		}

		private void Button_Click_ConfigureVT(object sender, RoutedEventArgs e)
		{
			new VTConfigWnd(Instance).ShowDialog();
		}

		private void Button_Click_ConfigureLagGhost(object sender, RoutedEventArgs e)
		{
			new LagCoeffSetWnd(Instance).ShowDialog();
		}

		private void EnableView(bool bEnable)
		{
			base.IsEnabled = bEnable;
			bCanExit = bEnable;
			if (bEnable && Instance != null)
			{
				Instance.TASK = "No Task";
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (base.Visibility == Visibility.Visible && IntPtr.Zero == pParam)
			{
				base.Dispatcher.BeginInvoke(new SdkCallbackEventHandler_WithImgParam(ProcessEvent), sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
			}
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			switch (nEventID)
			{
			case 4:
				switch (nParam1)
				{
				case 2003:
					MessageNotify("Read factory succeed.");
					break;
				case 2004:
					MessageNotify("Wrtie factory succeed.");
					break;
				}
				EnableView(true);
				break;
			case 5:
				switch (nParam1)
				{
				case 2003:
					MessageBox.Show("Read factory failed! Err=" + ErrorHelper.GetErrorDesp(nParam2));
					break;
				case 2004:
					switch (nParam2)
					{
					case 34:
						MessageBox.Show("Cannot find device!");
						break;
					case 35:
						MessageBox.Show("Device is beeing occupied!");
						break;
					case 36:
						MessageBox.Show("Param error, please check IP address!");
						break;
					default:
						MessageBox.Show("Wrtie factory failed! Err=" + ErrorHelper.GetErrorDesp(nParam2));
						break;
					}
					break;
				}
				EnableView(true);
				break;
			}
		}

		public void OnClose()
		{
			if (_det != null)
			{
				_det.AttrChangingMonitor.ReleaseRef(Enumerable.ToArray(_monitorAttrs));
				_det.SdkCallbackEvent -= OnSdkCallback;
			}
		}

		public bool Exit()
		{
			return bCanExit;
		}

		public bool Enter()
		{
			return true;
		}

		private void MessageNotify(string info)
		{
			if (this.MessageNotifyProxy != null)
			{
				this.MessageNotifyProxy(info);
			}
		}

		private void BuildDictionary()
		{
			DictFactoryAttr.Clear();
			SdkInterface sdkInterface = new SdkInterface();
			FieldInfo[] fields = sdkInterface.GetType().GetFields();
			foreach (FieldInfo fieldInfo in fields)
			{
				if (fieldInfo.Name.Length > 10 && "Attr_FROM_" == fieldInfo.Name.Substring(0, 10))
				{
					DictFactoryAttr.Add(Convert.ToInt32(fieldInfo.GetValue(sdkInterface)), fieldInfo.Name);
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/factorypage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_attrsPanel = (StackPanel)target;
				break;
			case 2:
				((Button)target).Click += Button_Click_ReadROM;
				break;
			case 3:
				((Button)target).Click += Button_Click_WriteROM;
				break;
			case 4:
				xSelectFileCheckbox = (CheckBox)target;
				break;
			case 5:
				((Button)target).Click += Button_Click_LoadParas;
				break;
			case 6:
				((Button)target).Click += Button_Click_SaveParas;
				break;
			case 7:
				((Button)target).Click += Button_Click_ConfigureVT;
				break;
			case 8:
				((Button)target).Click += Button_Click_ConfigureLagGhost;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
