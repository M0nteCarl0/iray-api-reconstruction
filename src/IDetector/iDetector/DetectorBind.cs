namespace iDetector
{
	public struct DetectorBind
	{
		public string strWorkDir;

		public string strSN;

		public string strBindName;

		public string strLocalIP;

		public string strProductType;
	}
}
