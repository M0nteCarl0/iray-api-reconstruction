using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace iDetector
{
	public class TipMessageWnd : Window
	{
		private Label ContentUI;

		public string Text
		{
			get
			{
				return ContentUI.Content.ToString();
			}
			set
			{
				ContentUI.Content = value;
			}
		}

		public TipMessageWnd()
		{
			ContentUI = new Label
			{
				Background = Brushes.LightGray,
				Foreground = Brushes.Green,
				HorizontalContentAlignment = HorizontalAlignment.Center
			};
			base.Background = Brushes.Gray;
			base.Foreground = Brushes.LawnGreen;
			base.ShowInTaskbar = false;
			base.Width = 470.0;
			base.Height = 30.0;
			AddChild(ContentUI);
			base.Owner = Application.Current.MainWindow;
			base.WindowStyle = WindowStyle.None;
			base.ResizeMode = ResizeMode.NoResize;
		}

		public void SetMessage(string info)
		{
			Text = info;
		}
	}
}
