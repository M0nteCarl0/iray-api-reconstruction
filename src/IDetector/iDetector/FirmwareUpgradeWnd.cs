using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class FirmwareUpgradeWnd : Window, IComponentConnector
	{
		public bool DiagResult;

		private bool _bUpgraded;

		private Detector _det;

		internal ComboBox _comboDeviceType;

		internal TextBox _txtFilePath;

		internal Button _btnStart;

		internal Button _btnOK;

		internal ProgressBar _progressBar;

		private bool _contentLoaded;

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
			}
		}

		public FirmwareUpgradeWnd(Detector det)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				_det = det;
				int pCount = 0;
				SdkInterface.GetEnumItemsCount("Enm_FW_DeviceType", ref pCount);
				EnumItem[] arr = new EnumItem[pCount];
				IntPtr intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
				SdkInterface.GetEnumItemList("Enm_FW_DeviceType", intPtr, pCount);
				SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref arr);
				Marshal.FreeHGlobal(intPtr);
				EnumItem[] array = arr;
				for (int i = 0; i < array.Length; i++)
				{
					EnumItem enumItem = array[i];
					EnumDisplayItem newItem = new EnumDisplayItem
					{
						Val = enumItem.nVal,
						Label = enumItem.strName
					};
					_comboDeviceType.Items.Add(newItem);
				}
				_comboDeviceType.SelectedIndex = 0;
				_bUpgraded = false;
			}
		}

		private void Button_Click_Browse(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Firmware setup files(*.mcs)|*.mcs|Firmware setup files(*.ma)|*.ma|Firmware setup files(*.sa)|*.sa|Firmware setup files(*.img)|*.img";
			if (openFileDialog.ShowDialog() == true)
			{
				_txtFilePath.Text = openFileDialog.FileName;
			}
		}

		private void Button_Click_Upgrade(object sender, RoutedEventArgs e)
		{
			_progressBar.Value = 0.0;
			Enm_FW_DeviceType val = (Enm_FW_DeviceType)(_comboDeviceType.SelectedItem as EnumDisplayItem).Val;
			string text = _txtFilePath.Text;
			string text2 = text.Substring(text.LastIndexOf(".") + 1, text.Length - text.LastIndexOf(".") - 1);
			if ((text2.ToLower().Equals("ma") && val != Enm_FW_DeviceType.Enm_FW_DeviceType_MainFPGA) || (text2.ToLower().Equals("sa") && val != Enm_FW_DeviceType.Enm_FW_DeviceType_ReadFPGA1))
			{
				MessageBox.Show("Upgrade file does not match with the Device type!");
				return;
			}
			int num = Instance.Invoke(2008, (int)val, _txtFilePath.Text);
			if (num != 0 && num != 1)
			{
				MessageBox.Show("Upgrade firmware failed! Err = " + ErrorHelper.GetErrorDesp(num));
				return;
			}
			_btnStart.IsEnabled = false;
			_btnOK.IsEnabled = false;
			DiagResult = true;
			Close();
		}

		private void Button_Click_OK(object sender, RoutedEventArgs e)
		{
			if (_bUpgraded)
			{
				_det.Invoke(3, null, 0);
				MessageBoxResult messageBoxResult = MessageBox.Show("Detector will be disconnected after the Firmware upgrading, \r\nDo you want to  re-connect it at once?", "iDetector", MessageBoxButton.YesNo, MessageBoxImage.Question);
				if (messageBoxResult == MessageBoxResult.Yes)
				{
					_det.Invoke(2, null, 0);
				}
			}
			Close();
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (nParam1 == 2008)
			{
				switch (nEventID)
				{
				case 4:
					_bUpgraded = true;
					MessageBox.Show("Succeed!");
					break;
				case 5:
					MessageBox.Show("Download failed! Err = " + ErrorHelper.GetErrorDesp(nParam2));
					break;
				default:
				{
					int num = 6;
					break;
				}
				}
				base.Dispatcher.BeginInvoke((Action)delegate
				{
					_btnStart.IsEnabled = true;
					_btnOK.IsEnabled = true;
				});
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/firmwareupgradewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_comboDeviceType = (ComboBox)target;
				break;
			case 2:
				_txtFilePath = (TextBox)target;
				break;
			case 3:
				((Button)target).Click += Button_Click_Browse;
				break;
			case 4:
				_btnStart = (Button)target;
				_btnStart.Click += Button_Click_Upgrade;
				break;
			case 5:
				_btnOK = (Button)target;
				_btnOK.Click += Button_Click_OK;
				break;
			case 6:
				_progressBar = (ProgressBar)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
