using System.Collections.Generic;
using System.IO;

namespace iDetector
{
	internal class BindRecords
	{
		private List<DetectorBind> _bindList;

		public List<DetectorBind> BindList
		{
			get
			{
				return _bindList;
			}
		}

		public bool Load()
		{
			if (_bindList == null)
			{
				_bindList = new List<DetectorBind>();
			}
			else
			{
				_bindList.Clear();
			}
			StreamReader streamReader = null;
			try
			{
				streamReader = new StreamReader("Bind.txt");
			}
			catch
			{
				return false;
			}
			while (true)
			{
				string text;
				try
				{
					text = streamReader.ReadLine().Trim();
				}
				catch
				{
					break;
				}
				if (text.Length != 0)
				{
					char[] separator = new char[1]
					{
						','
					};
					string[] array = text.Split(separator);
					if (array.Length == 5)
					{
						DetectorBind item = default(DetectorBind);
						item.strSN = array[0].Trim();
						item.strBindName = array[1].Trim();
						item.strLocalIP = array[2].Trim();
						item.strProductType = array[3].Trim();
						item.strWorkDir = array[4].Trim();
						_bindList.Add(item);
					}
				}
			}
			streamReader.Close();
			return true;
		}

		public bool Save()
		{
			StreamWriter streamWriter = new StreamWriter("Bind.txt");
			if (streamWriter == null)
			{
				return false;
			}
			foreach (DetectorBind bind in _bindList)
			{
				streamWriter.WriteLine("{0},{1},{2},{3},{4}", bind.strSN, bind.strBindName, bind.strLocalIP, bind.strProductType, bind.strWorkDir);
			}
			streamWriter.Close();
			return true;
		}
	}
}
