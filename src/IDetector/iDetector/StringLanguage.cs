using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Size = 1)]
	public struct StringLanguage
	{
		public const string English = "en-US";

		public const string Chinese = "zh-CN";
	}
}
