using System;
using System.Collections.Generic;

namespace iDetector
{
	public class LagGhostMapItem : NotifyObject
	{
		private int _PGA;

		private int _Binning;

		private int _TimeGap;

		private int _EnableLag;

		private float _LagA1;

		private float _LagB1;

		private float _LagA2;

		private float _LagB2;

		private int _EnableGhost;

		private float _GhostM1;

		private float _GhostN1;

		private float _GhostM2;

		private float _GhostN2;

		private float _GhostP1;

		private float _GhostQ1;

		private float _GhostP2;

		private float _GhostQ2;

		public int PGA
		{
			get
			{
				return _PGA;
			}
			set
			{
				_PGA = value;
				NotifyPropertyChanged("PGA");
			}
		}

		public int Binning
		{
			get
			{
				return _Binning;
			}
			set
			{
				_Binning = value;
				NotifyPropertyChanged("Binning");
			}
		}

		public int TimeGap
		{
			get
			{
				return _TimeGap;
			}
			set
			{
				_TimeGap = value;
				NotifyPropertyChanged("TimeGap");
			}
		}

		public bool EnableLag
		{
			get
			{
				return _EnableLag == 1;
			}
			set
			{
				_EnableLag = (value ? 1 : 0);
				NotifyPropertyChanged("EnableLag");
			}
		}

		public float LagA1
		{
			get
			{
				return _LagA1;
			}
			set
			{
				_LagA1 = value;
				NotifyPropertyChanged("LagA1");
			}
		}

		public float LagB1
		{
			get
			{
				return _LagB1;
			}
			set
			{
				_LagB1 = value;
				NotifyPropertyChanged("LagB1");
			}
		}

		public float LagA2
		{
			get
			{
				return _LagA2;
			}
			set
			{
				_LagA2 = value;
				NotifyPropertyChanged("LagA2");
			}
		}

		public float LagB2
		{
			get
			{
				return _LagB2;
			}
			set
			{
				_LagB2 = value;
				NotifyPropertyChanged("LagB2");
			}
		}

		public bool EnableGhost
		{
			get
			{
				return _EnableGhost == 1;
			}
			set
			{
				_EnableGhost = (value ? 1 : 0);
				NotifyPropertyChanged("EnableGhost");
			}
		}

		public float GhostM1
		{
			get
			{
				return _GhostM1;
			}
			set
			{
				_GhostM1 = value;
				NotifyPropertyChanged("GhostM1");
			}
		}

		public float GhostN1
		{
			get
			{
				return _GhostN1;
			}
			set
			{
				_GhostN1 = value;
				NotifyPropertyChanged("GhostN1");
			}
		}

		public float GhostM2
		{
			get
			{
				return _GhostM2;
			}
			set
			{
				_GhostM2 = value;
				NotifyPropertyChanged("GhostM2");
			}
		}

		public float GhostN2
		{
			get
			{
				return _GhostN2;
			}
			set
			{
				_GhostN2 = value;
				NotifyPropertyChanged("GhostN2");
			}
		}

		public float GhostP1
		{
			get
			{
				return _GhostP1;
			}
			set
			{
				_GhostP1 = value;
				NotifyPropertyChanged("GhostP1");
			}
		}

		public float GhostQ1
		{
			get
			{
				return _GhostQ1;
			}
			set
			{
				_GhostQ1 = value;
				NotifyPropertyChanged("GhostQ1");
			}
		}

		public float GhostP2
		{
			get
			{
				return _GhostP2;
			}
			set
			{
				_GhostP2 = value;
				NotifyPropertyChanged("GhostP2");
			}
		}

		public float GhostQ2
		{
			get
			{
				return _GhostQ2;
			}
			set
			{
				_GhostQ2 = value;
				NotifyPropertyChanged("GhostQ2");
			}
		}

		public LagGhostMapItem()
		{
		}

		private LagGhostMapItem(LagGhostMapItem other)
		{
			_PGA = other._PGA;
			_Binning = other._Binning;
			_TimeGap = other._TimeGap;
			_EnableLag = other._EnableLag;
			_LagA1 = other._LagA1;
			_LagB1 = other._LagB1;
			_LagA2 = other._LagA2;
			_LagB2 = other._LagB2;
			_EnableGhost = other._EnableGhost;
			_GhostM1 = other._GhostM1;
			_GhostN1 = other._GhostN1;
			_GhostM2 = other._GhostM2;
			_GhostN2 = other._GhostN2;
			_GhostP1 = other._GhostP1;
			_GhostQ1 = other._GhostQ1;
			_GhostP2 = other._GhostP2;
			_GhostQ2 = other._GhostQ2;
		}

		public byte[] ToArray()
		{
			List<byte[]> list = new List<byte[]>();
			list.Add(BitConverter.GetBytes(_PGA));
			list.Add(BitConverter.GetBytes(_Binning));
			list.Add(BitConverter.GetBytes(_TimeGap));
			list.Add(BitConverter.GetBytes(_EnableLag));
			list.Add(BitConverter.GetBytes(_LagA1));
			list.Add(BitConverter.GetBytes(_LagB1));
			list.Add(BitConverter.GetBytes(_LagA2));
			list.Add(BitConverter.GetBytes(_LagB2));
			list.Add(BitConverter.GetBytes(_EnableGhost));
			list.Add(BitConverter.GetBytes(_GhostM1));
			list.Add(BitConverter.GetBytes(_GhostN1));
			list.Add(BitConverter.GetBytes(_GhostM2));
			list.Add(BitConverter.GetBytes(_GhostN2));
			list.Add(BitConverter.GetBytes(_GhostP1));
			list.Add(BitConverter.GetBytes(_GhostQ1));
			list.Add(BitConverter.GetBytes(_GhostP2));
			list.Add(BitConverter.GetBytes(_GhostQ2));
			byte[] array = new byte[list.Count * 4];
			int num = 0;
			foreach (byte[] item in list)
			{
				Array.Copy(item, 0, array, num, item.Length);
				num += item.Length;
			}
			return array;
		}

		public static int GetArrayLength()
		{
			return 68;
		}

		public void Load(byte[] buffer)
		{
			int num = 0;
			_PGA = BitConverter.ToInt32(buffer, num);
			num += 4;
			_Binning = BitConverter.ToInt32(buffer, num);
			num += 4;
			_TimeGap = BitConverter.ToInt32(buffer, num);
			num += 4;
			_EnableLag = BitConverter.ToInt32(buffer, num);
			num += 4;
			_LagA1 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_LagB1 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_LagA2 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_LagB2 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_EnableGhost = BitConverter.ToInt32(buffer, num);
			num += 4;
			_GhostM1 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostN1 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostM2 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostN2 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostP1 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostQ1 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostP2 = BitConverter.ToSingle(buffer, num);
			num += 4;
			_GhostQ2 = BitConverter.ToSingle(buffer, num);
		}

		public LagGhostMapItem Clone()
		{
			return new LagGhostMapItem(this);
		}

		public static bool operator ==(LagGhostMapItem lhs, LagGhostMapItem rhs)
		{
			if (lhs.PGA == rhs.PGA && lhs.Binning == rhs.Binning && lhs.TimeGap == rhs.TimeGap)
			{
				return true;
			}
			return false;
		}

		public static bool operator !=(LagGhostMapItem lhs, LagGhostMapItem rhs)
		{
			if (lhs.PGA == rhs.PGA && lhs.Binning == rhs.Binning && lhs.TimeGap == rhs.TimeGap)
			{
				return false;
			}
			return true;
		}
	}
}
