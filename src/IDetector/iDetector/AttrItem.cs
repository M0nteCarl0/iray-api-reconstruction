namespace iDetector
{
	public struct AttrItem
	{
		public string Label;

		public int ReadAttrID;

		public int WriteAttrID;

		public bool IsBooleanVal;

		public AttrItem(string label, int readId, int writeId, bool isBoolean = false)
		{
			Label = label;
			ReadAttrID = readId;
			WriteAttrID = writeId;
			IsBooleanVal = isBoolean;
		}
	}
}
