using CorrectExUI;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class UploadToWorkdirWnd : Window, IComponentConnector
	{
		internal TextBox _txtIndexInfo;

		internal TextBox _txtWorkdir;

		internal Label _txtNotice;

		internal Button btnOK;

		internal Button btnCancel;

		private bool _contentLoaded;

		public UploadToWorkdirWnd(UploadFileInfo info)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			_txtIndexInfo.Text = info.uploadInfo;
			_txtWorkdir.Text = info.modeName;
			_txtNotice.Content = info.noticeInfo;
		}

		private void btnOK_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = true;
			Close();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
			Close();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/uploadtoworkdirwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_txtIndexInfo = (TextBox)target;
				break;
			case 2:
				_txtWorkdir = (TextBox)target;
				break;
			case 3:
				_txtNotice = (Label)target;
				break;
			case 4:
				btnOK = (Button)target;
				btnOK.Click += btnOK_Click;
				break;
			case 5:
				btnCancel = (Button)target;
				btnCancel.Click += btnCancel_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
