using IMG.Process;
using System;
using System.CodeDom.Compiler;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;

namespace iDetector
{
	public class PostProcessWnd : Window, INotifyPropertyChanged, IComponentConnector
	{
		private string superpostionBGColor;

		private string reduceNoiseBGColor;

		private string framesBGColor;

		private string coeffBGColor;

		public static string storeFilePath = SaveProcessedImage.tempDataPath;

		private static ProcessedCoeff PostCoeff = new ProcessedCoeff
		{
			maxAddFrames = 16,
			noiseCoeff = 0.5f
		};

		private string dstFilePath;

		private readonly string WarningColor = "#FFF0F000";

		private readonly string NormalColor = "";

		private bool enableOffsetCorrection;

		private bool enableGainCorrection;

		private bool enableDefectCorrection;

		private float offsetTemplateFreq;

		private string workdirPath;

		private EnumDisplayItem _CurProduct;

		private EnumDisplayItem _CurExpMode;

		private string offsetCBBGColor;

		private string gainCBBGColor;

		private string defectCBBGColor;

		private int imgWidth;

		private int imgHeight;

		private static OfflineCorrectionParams CorrectionInfo = new OfflineCorrectionParams();

		private static WeakReference CorrectionRef;

		internal TabItem SaveTab;

		internal System.Windows.Controls.TextBox FilePath;

		internal DigitalTextBox xStartIndex;

		internal DigitalTextBox xEndIndex;

		internal TabItem ProcessTab;

		internal System.Windows.Controls.CheckBox EnableSuperpostion;

		internal DigitalTextBox FrameNumber;

		internal System.Windows.Controls.CheckBox EnableReduceNoise;

		internal System.Windows.Controls.TextBox NoiseCoeff;

		internal TabItem CorrectionTab;

		internal System.Windows.Controls.TextBox xWorkdirPath;

		internal System.Windows.Controls.CheckBox xOffsetCorrection;

		internal System.Windows.Controls.CheckBox xGainCorrection;

		internal System.Windows.Controls.CheckBox xDefectCorrection;

		private bool _contentLoaded;

		public string SuperpostionBGColor
		{
			get
			{
				return superpostionBGColor;
			}
			set
			{
				superpostionBGColor = value;
				NotifyPropertyChanged("SuperpostionBGColor");
			}
		}

		public string ReduceNoiseBGColor
		{
			get
			{
				return reduceNoiseBGColor;
			}
			set
			{
				reduceNoiseBGColor = value;
				NotifyPropertyChanged("ReduceNoiseBGColor");
			}
		}

		public string FramesBGColor
		{
			get
			{
				return framesBGColor;
			}
			set
			{
				framesBGColor = value;
				NotifyPropertyChanged("FramesBGColor");
			}
		}

		public string CoeffBGColor
		{
			get
			{
				return coeffBGColor;
			}
			set
			{
				coeffBGColor = value;
				NotifyPropertyChanged("CoeffBGColor");
			}
		}

		public bool EnableOffsetCorrection
		{
			get
			{
				return enableOffsetCorrection;
			}
			set
			{
				enableOffsetCorrection = value;
				if (value != CorrectionInfo.bDoPreOffset)
				{
					OffsetCBBGColor = WarningColor;
				}
				else
				{
					OffsetCBBGColor = NormalColor;
				}
				NotifyPropertyChanged("EnableOffsetCorrection");
			}
		}

		public bool EnableGainCorrection
		{
			get
			{
				return enableGainCorrection;
			}
			set
			{
				enableGainCorrection = value;
				if (value != CorrectionInfo.bDoGain)
				{
					GainCBBGColor = WarningColor;
				}
				else
				{
					GainCBBGColor = NormalColor;
				}
				NotifyPropertyChanged("EnableGainCorrection");
			}
		}

		public bool EnableDefectCorrection
		{
			get
			{
				return enableDefectCorrection;
			}
			set
			{
				enableDefectCorrection = value;
				if (value != CorrectionInfo.bDoDefect)
				{
					DefectCBBGColor = WarningColor;
				}
				else
				{
					DefectCBBGColor = NormalColor;
				}
				NotifyPropertyChanged("EnableDefectCorrection");
			}
		}

		public float OffsetTemplateFreq
		{
			get
			{
				return offsetTemplateFreq;
			}
			set
			{
				offsetTemplateFreq = (float)Math.Round(value, 1);
				NotifyPropertyChanged("OffsetTemplateFreq");
			}
		}

		public string WorkdirPath
		{
			get
			{
				return workdirPath;
			}
			set
			{
				workdirPath = value;
				NotifyPropertyChanged("WorkdirPath");
			}
		}

		public ObservableCollection<EnumDisplayItem> ProductList
		{
			get;
			set;
		}

		public EnumDisplayItem CurProduct
		{
			get
			{
				return _CurProduct;
			}
			set
			{
				_CurProduct = value;
				if (value != null)
				{
					CorrectionInfo.nFpdProdNo = _CurProduct.Val;
				}
				NotifyPropertyChanged("CurProduct");
			}
		}

		public ObservableCollection<EnumDisplayItem> ExpModeList
		{
			get;
			set;
		}

		public EnumDisplayItem CurExpMode
		{
			get
			{
				return _CurExpMode;
			}
			set
			{
				_CurExpMode = value;
				if (value != null)
				{
					CorrectionInfo.nFpdExpMode = _CurExpMode.Val;
				}
				NotifyPropertyChanged("CurExpMode");
			}
		}

		public string OffsetCBBGColor
		{
			get
			{
				return offsetCBBGColor;
			}
			set
			{
				offsetCBBGColor = value;
				NotifyPropertyChanged("OffsetCBBGColor");
			}
		}

		public string GainCBBGColor
		{
			get
			{
				return gainCBBGColor;
			}
			set
			{
				gainCBBGColor = value;
				NotifyPropertyChanged("GainCBBGColor");
			}
		}

		public string DefectCBBGColor
		{
			get
			{
				return defectCBBGColor;
			}
			set
			{
				defectCBBGColor = value;
				NotifyPropertyChanged("DefectCBBGColor");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public event Action<string, int, int> SaveDel;

		public event Action<ProcessedCoeff> SetPostProcessParaDel;

		public event Func<OfflineCorrectionInfo, int> SetCorrectOption;

		public void NotifyPropertyChanged(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		public PostProcessWnd()
		{
			InitializeComponent();
			base.Owner = System.Windows.Application.Current.MainWindow;
			base.ShowInTaskbar = false;
			PostCoeff.tempDataPath = storeFilePath;
			EnableReduceNoise.IsChecked = PostCoeff.enableReduceNoise;
			EnableSuperpostion.IsChecked = PostCoeff.enableSuperostion;
			FrameNumber.Text = PostCoeff.maxAddFrames.ToString();
			NoiseCoeff.Text = PostCoeff.noiseCoeff.ToString();
			InitCorrectionTab();
			base.DataContext = this;
		}

		public static void ResetPostCoeff()
		{
			PostCoeff.maxAddFrames = 16;
			PostCoeff.noiseCoeff = 0.5f;
			PostCoeff.enableSuperostion = false;
			PostCoeff.enableReduceNoise = false;
		}

		public void SetFramesNumber(int totalNumber)
		{
			if (totalNumber < 1)
			{
				totalNumber = int.MaxValue;
			}
			xEndIndex.Text = totalNumber.ToString();
		}

		private void OnOpenPath(object sender, RoutedEventArgs e)
		{
			FilePath.Text = FileDialog.SaveFile();
		}

		private void OnSave(object sender, RoutedEventArgs e)
		{
			if (this.SaveDel != null)
			{
				dstFilePath = FilePath.Text;
				if (dstFilePath == null || dstFilePath.Length == 0)
				{
					System.Windows.MessageBox.Show(System.Windows.Application.Current.MainWindow, "Please input file path!");
				}
				else if (!HadDoneDenoise() && !HadDoneOfflineCorrection())
				{
					System.Windows.MessageBox.Show(System.Windows.Application.Current.MainWindow, "The original file existed! No post process.");
				}
				else
				{
					this.SaveDel(dstFilePath, xStartIndex.Value - 1, xEndIndex.Value);
				}
			}
		}

		private bool HadDoneDenoise()
		{
			if (!PostCoeff.enableSuperostion)
			{
				return PostCoeff.enableReduceNoise;
			}
			return true;
		}

		private void OnSet(object sender, RoutedEventArgs e)
		{
			if (this.SetPostProcessParaDel != null)
			{
				PostCoeff.enableSuperostion = ((EnableSuperpostion.IsChecked == true) ? true : false);
				PostCoeff.enableReduceNoise = ((EnableReduceNoise.IsChecked == true) ? true : false);
				PostCoeff.maxAddFrames = FrameNumber.Value;
				if (!float.TryParse(NoiseCoeff.Text, out PostCoeff.noiseCoeff))
				{
					System.Windows.MessageBox.Show(System.Windows.Application.Current.MainWindow, "Please input a legal float!");
					return;
				}
				if (PostCoeff.noiseCoeff > 1f || PostCoeff.noiseCoeff < 0f)
				{
					PostCoeff.noiseCoeff = 0f;
					System.Windows.MessageBox.Show(System.Windows.Application.Current.MainWindow, "The coeff should be a float in [0, 1]!");
					return;
				}
				this.SetPostProcessParaDel(PostCoeff);
				SuperpostionBGColor = NormalColor;
				ReduceNoiseBGColor = NormalColor;
				FramesBGColor = NormalColor;
				CoeffBGColor = NormalColor;
			}
		}

		private void OnCheckChange(object sender, RoutedEventArgs e)
		{
			System.Windows.Controls.CheckBox checkBox = sender as System.Windows.Controls.CheckBox;
			if (checkBox == EnableReduceNoise)
			{
				bool flag = (checkBox.IsChecked == true) ? true : false;
				if (PostCoeff.enableReduceNoise ^ flag)
				{
					ReduceNoiseBGColor = WarningColor;
				}
				else
				{
					ReduceNoiseBGColor = NormalColor;
				}
			}
			else if (checkBox == EnableSuperpostion)
			{
				bool flag2 = (checkBox.IsChecked == true) ? true : false;
				if (PostCoeff.enableSuperostion ^ flag2)
				{
					SuperpostionBGColor = WarningColor;
				}
				else
				{
					SuperpostionBGColor = NormalColor;
				}
			}
		}

		private void OnChanged(object sender, TextChangedEventArgs e)
		{
			System.Windows.Controls.TextBox textBox = sender as System.Windows.Controls.TextBox;
			float result2;
			if (textBox == FrameNumber)
			{
				int result;
				if (int.TryParse(textBox.Text, out result))
				{
					if (PostCoeff.maxAddFrames.Equals(result))
					{
						FramesBGColor = NormalColor;
					}
					else
					{
						FramesBGColor = WarningColor;
					}
				}
			}
			else if (textBox == NoiseCoeff && float.TryParse(textBox.Text, out result2))
			{
				if (PostCoeff.noiseCoeff.Equals(result2))
				{
					CoeffBGColor = NormalColor;
				}
				else
				{
					CoeffBGColor = WarningColor;
				}
			}
		}

		public void SetImageInfo(int widht, int height)
		{
			imgWidth = widht;
			imgHeight = height;
		}

		private bool HadDoneOfflineCorrection()
		{
			if (!CorrectionInfo.bDoDefect && !CorrectionInfo.bDoGain)
			{
				return CorrectionInfo.bDoPreOffset;
			}
			return true;
		}

		public void SetCorrectionParams(ref OfflineCorrectionParams option)
		{
			if (CorrectionRef == null)
			{
				CorrectionRef = new WeakReference(option);
				CorrectionInfo = (OfflineCorrectionParams)CorrectionRef.Target;
			}
		}

		public void ResetCorrectionInfo()
		{
			this.SetCorrectOption(new OfflineCorrectionInfo
			{
				nFpdProdNo = CorrectionInfo.nFpdProdNo,
				nFpdSeqInterval = CorrectionInfo.nFpdSeqInterval,
				nImgWidth = CorrectionInfo.nImgWidth,
				nImgHeight = CorrectionInfo.nImgHeight,
				pszFpdWorkDir = CorrectionInfo.pszFpdWorkDir,
				pszFpdCaliSubset = CorrectionInfo.pszFpdCaliSubset,
				bDoPreOffset = false,
				bDoGain = false,
				bDoDefect = false,
				bPreOffsetDone = false
			});
			CorrectionInfo.bDoDefect = false;
			CorrectionInfo.bDoGain = false;
			CorrectionInfo.bDoPreOffset = false;
			CorrectionInfo.pszFpdWorkDir = "";
			CorrectionInfo.nFpdSeqInterval = 0;
		}

		private void InitCorrectionTab()
		{
			InitProductList();
			InitExpModeList();
			WorkdirPath = CorrectionInfo.FullWorkDirPath;
			EnableOffsetCorrection = CorrectionInfo.bDoPreOffset;
			EnableGainCorrection = CorrectionInfo.bDoGain;
			EnableDefectCorrection = CorrectionInfo.bDoDefect;
			OffsetTemplateFreq = CorrectionInfo.OffsetTemplateFreq;
			foreach (EnumDisplayItem product in ProductList)
			{
				if (product.Val == CorrectionInfo.nFpdProdNo)
				{
					CurProduct = product;
					break;
				}
			}
			foreach (EnumDisplayItem expMode in ExpModeList)
			{
				if (expMode.Val == CorrectionInfo.nFpdExpMode)
				{
					CurExpMode = expMode;
					break;
				}
			}
		}

		private void InitProductList()
		{
			ProductList = SDKEnumCollector.LoadParam("Enm_ProdType");
		}

		private void InitExpModeList()
		{
			ExpModeList = SDKEnumCollector.LoadParam("Enm_ExpMode");
			foreach (EnumDisplayItem expMode in ExpModeList)
			{
				expMode.Label = string.Format("{0}({1})", expMode.Label.Remove(0, "ExpMode_".Length), expMode.Val);
			}
		}

		private void OnOpenWorkDir(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialogEx folderBrowserDialogEx = new FolderBrowserDialogEx();
			folderBrowserDialogEx.ShowNewFolderButton = true;
			folderBrowserDialogEx.ShowFullPathInEditBox = true;
			if (System.Windows.Forms.DialogResult.OK == folderBrowserDialogEx.ShowDialog())
			{
				WorkdirPath = folderBrowserDialogEx.SelectedPath;
			}
		}

		private void OnSetCorrection(object sender, RoutedEventArgs e)
		{
			try
			{
				if (string.IsNullOrEmpty(WorkdirPath))
				{
					throw new ArgumentException("Workdir path cannot be empty!");
				}
				WorkdirPath = WorkdirPath.TrimEnd('\\');
				if (this.SetCorrectOption != null)
				{
					string text = WorkdirPath.Substring(WorkdirPath.LastIndexOf("\\") + 1, WorkdirPath.Length - WorkdirPath.LastIndexOf("\\") - 1);
					if (-1 == WorkdirPath.LastIndexOf("Correct"))
					{
						throw new ArgumentException("Workdir path is incorrect!");
					}
					string text2 = WorkdirPath.Substring(0, WorkdirPath.LastIndexOf("Correct"));
					text = WorkdirPath.Remove(0, string.Format("{0}Correct\\", text2).Length);
					int num = this.SetCorrectOption(new OfflineCorrectionInfo
					{
						nFpdProdNo = CurProduct.Val,
						nFpdSeqInterval = (int)((OffsetTemplateFreq == 0f) ? 0f : (1000f / OffsetTemplateFreq)),
						nFpdExpMode = CurExpMode.Val,
						nImgWidth = imgWidth,
						nImgHeight = imgHeight,
						pszFpdWorkDir = text2,
						pszFpdCaliSubset = text,
						bDoPreOffset = EnableOffsetCorrection,
						bDoGain = EnableGainCorrection,
						bDoDefect = EnableDefectCorrection,
						bPreOffsetDone = false
					});
					if (num != 0)
					{
						System.Windows.MessageBox.Show(System.Windows.Application.Current.MainWindow, string.Format("Set correction failed!Err={0}", ErrorHelper.GetErrorDesp(num)));
					}
					else
					{
						CorrectionInfo.OffsetTemplateFreq = OffsetTemplateFreq;
						CorrectionInfo.bDoDefect = EnableDefectCorrection;
						CorrectionInfo.bDoGain = EnableGainCorrection;
						CorrectionInfo.bDoPreOffset = EnableOffsetCorrection;
						CorrectionInfo.FullWorkDirPath = WorkdirPath;
						CorrectionInfo.nFpdSeqInterval = (int)((OffsetTemplateFreq == 0f) ? 0f : (1000f / OffsetTemplateFreq));
						CorrectionInfo.nFpdExpMode = CurExpMode.Val;
						DefectCBBGColor = NormalColor;
						GainCBBGColor = NormalColor;
						OffsetCBBGColor = NormalColor;
					}
				}
			}
			catch (Exception ex)
			{
				System.Windows.MessageBox.Show(System.Windows.Application.Current.MainWindow, ex.Message);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/postprocesswnd.xaml", UriKind.Relative);
				System.Windows.Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				SaveTab = (TabItem)target;
				break;
			case 2:
				FilePath = (System.Windows.Controls.TextBox)target;
				break;
			case 3:
				((System.Windows.Controls.Button)target).Click += OnOpenPath;
				break;
			case 4:
				xStartIndex = (DigitalTextBox)target;
				break;
			case 5:
				xEndIndex = (DigitalTextBox)target;
				break;
			case 6:
				((System.Windows.Controls.Button)target).Click += OnSave;
				break;
			case 7:
				ProcessTab = (TabItem)target;
				break;
			case 8:
				EnableSuperpostion = (System.Windows.Controls.CheckBox)target;
				EnableSuperpostion.Click += OnCheckChange;
				break;
			case 9:
				FrameNumber = (DigitalTextBox)target;
				break;
			case 10:
				EnableReduceNoise = (System.Windows.Controls.CheckBox)target;
				EnableReduceNoise.Click += OnCheckChange;
				break;
			case 11:
				NoiseCoeff = (System.Windows.Controls.TextBox)target;
				NoiseCoeff.TextChanged += OnChanged;
				break;
			case 12:
				((System.Windows.Controls.Button)target).Click += OnSet;
				break;
			case 13:
				CorrectionTab = (TabItem)target;
				break;
			case 14:
				xWorkdirPath = (System.Windows.Controls.TextBox)target;
				break;
			case 15:
				((System.Windows.Controls.Button)target).Click += OnOpenWorkDir;
				break;
			case 16:
				xOffsetCorrection = (System.Windows.Controls.CheckBox)target;
				break;
			case 17:
				xGainCorrection = (System.Windows.Controls.CheckBox)target;
				break;
			case 18:
				xDefectCorrection = (System.Windows.Controls.CheckBox)target;
				break;
			case 19:
				((System.Windows.Controls.Button)target).Click += OnSetCorrection;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
