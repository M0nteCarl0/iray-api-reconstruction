namespace iDetector
{
	public class TiffAttrList
	{
		public Enm_ImageTag Id
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public string Value
		{
			get;
			set;
		}
	}
}
