namespace iDetector
{
	public interface IDetSubPage
	{
		Detector Instance
		{
			get;
			set;
		}

		bool bHandled
		{
			get;
			set;
		}

		event SubPageMessageHandler MessageNotifyProxy;

		void OnClose();

		bool Exit();

		bool Enter();
	}
}
