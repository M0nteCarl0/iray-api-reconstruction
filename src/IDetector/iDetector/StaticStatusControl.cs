namespace iDetector
{
	public class StaticStatusControl : StatusControl
	{
		public override void UpdateStatusAfterStartAcq()
		{
			base.IsSequenceAcq = false;
			base.EnableUpdateImagelist = false;
		}
	}
}
