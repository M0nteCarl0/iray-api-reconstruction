using System.Collections.ObjectModel;

namespace iDetector
{
	public class ModeListArr : ObservableCollection<ModeList>
	{
		public void AddItem(ApplicationMode mode, Enm_ProdType ProductNo)
		{
			string text;
			if (ProductNo == Enm_ProdType.Enm_Prd_Pluto0406X || ProductNo == Enm_ProdType.Enm_Prd_Pluto0900X)
			{
				text = string.Format("{0} (FullWell:{1}, Binning:{2}, ROI:{3} Freq:{4}fps)", mode.Subset, mode.PGA, mode.Binning, mode.ROIRange, mode.Freq);
			}
			else
			{
				text = (mode.ExistedROIRange ? string.Format("{0} (PGA:{1}, Binning:{2}, ROI:{3} Freq:{4}fps)", mode.Subset, mode.PGA, mode.Binning, mode.ROIRange, mode.Freq) : (mode.NoZoom ? string.Format("{0} (PGA:{1}, Binning:{2}, Freq:{3}fps)", mode.Subset, mode.PGA, mode.Binning, mode.Freq) : string.Format("{0} (PGA:{1}, Binning:{2}, Zoom:{3}, Freq:{4}fps)", mode.Subset, mode.PGA, mode.Binning, mode.Zoom, mode.Freq)));
				if (mode.ExistedExpMode)
				{
					text = string.Format("{0}, ExpMode:{1}", text, mode.ExpMode);
				}
			}
			Add(new ModeList
			{
				Disp = text,
				Mode = mode
			});
		}
	}
}
