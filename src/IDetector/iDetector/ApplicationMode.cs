using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;

namespace iDetector
{
	public class ApplicationMode : INotifyPropertyChanged
	{
		public bool ExistedROIRange;

		private string _gainValidity;

		private string _defectValidity;

		private string _offsetValidity;

		private Brush bgColor;

		public string Subset;

		public string _activity;

		public int Index
		{
			get;
			set;
		}

		public int PGA
		{
			get;
			set;
		}

		public int FullWell
		{
			get;
			set;
		}

		public int Binning
		{
			get;
			set;
		}

		public int Zoom
		{
			get;
			set;
		}

		public string ROIRange
		{
			get;
			set;
		}

		public double Freq
		{
			get;
			set;
		}

		public bool NoZoom
		{
			get;
			set;
		}

		public bool ExistedExpMode
		{
			get;
			set;
		}

		public bool PGAAsFull
		{
			get;
			set;
		}

		public string ExpMode
		{
			get;
			set;
		}

		public string GainValidity
		{
			get
			{
				return _gainValidity;
			}
			set
			{
				_gainValidity = value;
				NotifyView("GainValidity");
			}
		}

		public string DefectValidity
		{
			get
			{
				return _defectValidity;
			}
			set
			{
				_defectValidity = value;
				NotifyView("DefectValidity");
			}
		}

		public string OffsetValidity
		{
			get
			{
				return _offsetValidity;
			}
			set
			{
				_offsetValidity = value;
				NotifyView("OffsetValidity");
			}
		}

		public Brush BGColor
		{
			get
			{
				return bgColor;
			}
			set
			{
				bgColor = value;
				NotifyView("BGColor");
			}
		}

		public string SubsetName
		{
			get
			{
				return Subset;
			}
			set
			{
				Subset = value;
				NotifyView("SubsetName");
			}
		}

		public string Activity
		{
			get
			{
				return _activity;
			}
			set
			{
				_activity = value;
				NotifyView("Activity");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public ApplicationMode()
		{
			PGA = (FullWell = (Binning = (Zoom = 0)));
			ROIRange = string.Empty;
		}

		public void NotifyView(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		public static List<ApplicationMode> ParseIni(string workDir)
		{
			StringBuilder stringBuilder = new StringBuilder(128);
			string lpFileName = workDir + "\\DynamicApplicationMode.ini";
			List<ApplicationMode> list = new List<ApplicationMode>();
			for (int i = 1; i < 50; i++)
			{
				string lpAppName = "ApplicationMode" + i.ToString();
				IniParser.GetPrivateProfileString(lpAppName, "Binning", null, stringBuilder, stringBuilder.Capacity, lpFileName);
				if (stringBuilder.Length == 0)
				{
					break;
				}
				ApplicationMode applicationMode = new ApplicationMode();
				int result = 0;
				int.TryParse(stringBuilder.ToString(), out result);
				applicationMode.Binning = result;
				IniParser.GetPrivateProfileString(lpAppName, "PGA", null, stringBuilder, stringBuilder.Capacity, lpFileName);
				applicationMode.PGAAsFull = false;
				if (string.IsNullOrEmpty(stringBuilder.ToString()))
				{
					IniParser.GetPrivateProfileString(lpAppName, "FullWell", null, stringBuilder, stringBuilder.Capacity, lpFileName);
					applicationMode.PGAAsFull = true;
				}
				int.TryParse(stringBuilder.ToString(), out result);
				applicationMode.PGA = result;
				applicationMode.FullWell = result;
				applicationMode.ExistedROIRange = false;
				applicationMode.Zoom = (int)IniParser.GetPrivateProfileInt(lpAppName, "Zoom", -1, lpFileName);
				if (applicationMode.Zoom == -1)
				{
					applicationMode.NoZoom = true;
					applicationMode.Zoom = 0;
					IniParser.GetPrivateProfileString(lpAppName, "ROIRange", null, stringBuilder, stringBuilder.Capacity, lpFileName);
					if (!string.IsNullOrEmpty(stringBuilder.ToString()))
					{
						applicationMode.ROIRange = stringBuilder.ToString().Replace(" ", "");
						applicationMode.ExistedROIRange = true;
					}
				}
				else
				{
					applicationMode.NoZoom = false;
				}
				result = (int)IniParser.GetPrivateProfileInt(lpAppName, "ExposureMode", -1, lpFileName);
				if (-1 == result)
				{
					applicationMode.ExistedExpMode = false;
					applicationMode.ExpMode = string.Empty;
				}
				else
				{
					applicationMode.ExistedExpMode = true;
					applicationMode.ExpMode = ((Enm_ExpMode)result).ToString().Replace("Enm_ExpMode_", "");
				}
				IniParser.GetPrivateProfileString(lpAppName, "Frequency", null, stringBuilder, stringBuilder.Capacity, lpFileName);
				applicationMode.Freq = Convert.ToDouble(stringBuilder.ToString());
				IniParser.GetPrivateProfileString(lpAppName, "Subset", null, stringBuilder, stringBuilder.Capacity, lpFileName);
				applicationMode.Subset = stringBuilder.ToString();
				bool flag = false;
				foreach (ApplicationMode item in list)
				{
					if (item.PGA == applicationMode.PGA && item.Binning == applicationMode.Binning && item.Freq == applicationMode.Freq && item.Subset == applicationMode.Subset && item.Zoom == applicationMode.Zoom && item.ExpMode == applicationMode.ExpMode && item.ROIRange == applicationMode.ROIRange)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					applicationMode.Index = list.Count + 1;
					list.Add(applicationMode);
				}
			}
			return list;
		}

		public static void UpdateAppMode2File(string workDir, ApplicationMode mode)
		{
			string lpFileName = workDir + "\\DynamicApplicationMode.ini";
			string lpAppName = "ApplicationMode" + mode.Index.ToString();
			if (mode.PGAAsFull)
			{
				IniParser.WritePrivateProfileString(lpAppName, "FullWell", mode.PGA.ToString(), lpFileName);
			}
			else
			{
				IniParser.WritePrivateProfileString(lpAppName, "PGA", mode.PGA.ToString(), lpFileName);
			}
			IniParser.WritePrivateProfileString(lpAppName, "Binning", mode.Binning.ToString(), lpFileName);
			IniParser.WritePrivateProfileString(lpAppName, "Frequency", mode.Freq.ToString(), lpFileName);
		}

		public static string GenerateTemplateSubDir(ApplicationMode mode)
		{
			return new StringBuilder(string.Format("\\Mode{0}", mode.Index)).ToString();
		}

		public static string GenerateTemplateSubDir(int Binning, int PGA)
		{
			return new StringBuilder("\\Binning").Append(Binning.ToString()).Append("\\PGA").Append(PGA.ToString())
				.ToString();
		}
	}
}
