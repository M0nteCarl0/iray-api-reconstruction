using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Markup;

namespace iDetector
{
	public class AddWorkDirWnd : Window, IComponentConnector
	{
		private DetectorBind newBind;

		private AddWorkDirWndVM addDirVM;

		private readonly string templateWorkDir = ".\\work_dir_templates\\";

		private string defaultWorkDirPath;

		private bool _contentLoaded;

		public event Action<DetectorBind> AddBindItem;

		public AddWorkDirWnd(int productNo, string defaultWorkDir, string serialNo)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			defaultWorkDirPath = defaultWorkDir;
			string arg = (string.Empty == serialNo) ? "newDetector" : serialNo;
			addDirVM = new AddWorkDirWndVM(productNo, string.Format("{0}\\{1}", defaultWorkDir, arg));
			base.DataContext = addDirVM;
			newBind = default(DetectorBind);
			newBind.strSN = serialNo;
		}

		private void Button_SelectFolder(object sender, RoutedEventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.SelectedPath = defaultWorkDirPath;
			if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK && !(folderBrowserDialog.SelectedPath == string.Empty))
			{
				addDirVM.WorkDir = folderBrowserDialog.SelectedPath;
			}
		}

		private void Button_AddWorkDir(object sender, RoutedEventArgs e)
		{
			if (addDirVM.WorkDir == null || addDirVM.WorkDir.Length == 0)
			{
				System.Windows.MessageBox.Show("Work directory path cannot be null!");
				return;
			}
			if (!Utility.IsDirectoryExisted(templateWorkDir))
			{
				System.Windows.MessageBox.Show("The directory work_dir_templates not exists in current WorkDir!");
				return;
			}
			if (!Utility.IsFileExisted(templateWorkDir + "ConfigTable.txt"))
			{
				System.Windows.MessageBox.Show("The ConfigTable not exists in work_dir_templates!");
				return;
			}
			string text = null;
			StreamReader streamReader = new StreamReader(templateWorkDir + "ConfigTable.txt");
			while (!streamReader.EndOfStream)
			{
				char[] trimChars = new char[3]
				{
					'(',
					')',
					' '
				};
				string text2 = streamReader.ReadLine().Trim(trimChars);
				string[] array = text2.Split(',');
				int result = 0;
				int.TryParse(array[0], out result);
				if (result == addDirVM.CurProduct.Val)
				{
					text = templateWorkDir + array[1].Trim();
					break;
				}
			}
			streamReader.Close();
			if (string.IsNullOrEmpty(text))
			{
				System.Windows.MessageBox.Show("No matched config file in work_dir_templates!");
				return;
			}
			newBind.strWorkDir = addDirVM.WorkDir;
			newBind.strProductType = addDirVM.CurProduct.Label;
			newBind.strBindName = addDirVM.WorkDir.Substring(addDirVM.WorkDir.LastIndexOfAny(new char[2]
			{
				'/',
				'\\'
			}) + 1);
			if (!Utility.IsDirectoryExisted(newBind.strWorkDir))
			{
				if (!Utility.CreateDirectory(newBind.strWorkDir))
				{
					System.Windows.MessageBox.Show("Invalid path!");
					return;
				}
			}
			else if (Utility.IsFileExisted(Path.GetFullPath(newBind.strWorkDir) + "\\config.ini") && MessageBoxResult.No == System.Windows.MessageBox.Show("Work dir already exists! OverWrite it?", "Create new WorkDir", MessageBoxButton.YesNo))
			{
				return;
			}
			Utility.CopyFolders(text, newBind.strWorkDir);
			if (this.AddBindItem != null)
			{
				this.AddBindItem(newBind);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/addworkdirwnd.xaml", UriKind.Relative);
				System.Windows.Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((System.Windows.Controls.Button)target).Click += Button_SelectFolder;
				break;
			case 2:
				((System.Windows.Controls.Button)target).Click += Button_AddWorkDir;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
