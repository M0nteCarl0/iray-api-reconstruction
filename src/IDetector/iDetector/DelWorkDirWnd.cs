using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class DelWorkDirWnd : Window, IComponentConnector
	{
		private bool delBind;

		internal CheckBox xDelDirCB;

		private bool _contentLoaded;

		public bool DelDirectorty
		{
			get
			{
				return xDelDirCB.IsChecked == true;
			}
		}

		public bool DelBind
		{
			get
			{
				return delBind;
			}
		}

		public DelWorkDirWnd()
		{
			delBind = false;
			InitializeComponent();
			base.ShowInTaskbar = false;
		}

		private void Button_Yes(object sender, RoutedEventArgs e)
		{
			delBind = true;
			Close();
		}

		private void Button_No(object sender, RoutedEventArgs e)
		{
			delBind = false;
			Close();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/delworkdirwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xDelDirCB = (CheckBox)target;
				break;
			case 2:
				((Button)target).Click += Button_Yes;
				break;
			case 3:
				((Button)target).Click += Button_No;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
