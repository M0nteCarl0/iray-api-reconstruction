namespace iDetector
{
	public class WifiClientParam : NotifyObject
	{
		private string _SSID;

		private string _Key;

		private bool _SelectedFlag;

		private string _BKColor;

		public string SSID
		{
			get
			{
				return _SSID;
			}
			set
			{
				_SSID = value;
				NotifyPropertyChanged("SSID");
			}
		}

		public string Key
		{
			get
			{
				return _Key;
			}
			set
			{
				_Key = value;
				NotifyPropertyChanged("Key");
			}
		}

		public bool SelectedFlag
		{
			get
			{
				return _SelectedFlag;
			}
			set
			{
				_SelectedFlag = value;
				NotifyPropertyChanged("SelectedFlag");
			}
		}

		public string BKColor
		{
			get
			{
				return _BKColor;
			}
			set
			{
				_BKColor = value;
				NotifyPropertyChanged("BKColor");
			}
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (GetType() != obj.GetType())
			{
				return false;
			}
			WifiClientParam wifiClientParam = obj as WifiClientParam;
			if (SSID.Equals(wifiClientParam.SSID) && Key.Equals(wifiClientParam.Key))
			{
				return true;
			}
			return false;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}
}
