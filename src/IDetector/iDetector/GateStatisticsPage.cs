using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace iDetector
{
	public class GateStatisticsPage : Window, IComponentConnector
	{
		private const int nTotal = 500;

		private RectInfo rc;

		private int nWidth;

		private int nHeight;

		private int nGateIC;

		private int nAfe;

		private int nGateCount;

		private double dwGateSubAve;

		private int[] nGateDataArray;

		private int nAreaWidth = 1250;

		private int nAreaHeight = 500;

		public ushort[] ImgData;

		private double dwGateValue;

		private List<ValueInfo> valueInfoList;

		internal Label LabelTotal;

		internal Label LabelSubAve;

		internal TextBox GateValueTxt;

		internal Button StatisticsButton;

		internal Canvas GateCanvas;

		private bool _contentLoaded;

		public GateStatisticsPage(GateValueInfo info)
		{
			InitializeComponent();
			base.Owner = Application.Current.MainWindow;
			if (Initialize(info) != 0)
			{
				MessageBox.Show("Image size error!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				StatisticsButton.Visibility = Visibility.Collapsed;
			}
			else
			{
				StartStatistics();
			}
		}

		private void GateValueTxt_TextChanged(object sender, TextChangedEventArgs e)
		{
			TextBox textBox = sender as TextBox;
			TextChange[] array = new TextChange[e.Changes.Count];
			e.Changes.CopyTo(array, 0);
			int offset = array[0].Offset;
			if (array[0].AddedLength > 0)
			{
				double result = 0.0;
				if (!double.TryParse(textBox.Text, out result))
				{
					textBox.Text = textBox.Text.Remove(offset, array[0].AddedLength);
					textBox.Select(offset, 0);
				}
			}
		}

		private void StatisticsButton_Click(object sender, RoutedEventArgs e)
		{
			StartStatistics();
		}

		private void Draw()
		{
			GateCanvas.Margin = new Thickness(100.0, 20.0, 500.0, 500.0);
			for (int i = 0; i < nGateIC; i++)
			{
				for (int j = 0; j < nAfe; j++)
				{
					Rectangle rectangle = new Rectangle();
					rectangle.Stroke = new SolidColorBrush(Colors.Silver);
					rectangle.StrokeThickness = 1.0;
					double num = nAreaWidth * j / nAfe;
					double num2 = nAreaHeight * ((i - 1) * 600 + 600) / nHeight;
					double num3 = nAreaWidth * (j + 1) / nAfe;
					double num4 = nAreaHeight * (i * 600 + 600) / nHeight;
					double width = num3 - num;
					double height = num4 - num2;
					rectangle.SetValue(Canvas.LeftProperty, num);
					rectangle.SetValue(Canvas.TopProperty, num2);
					rectangle.Width = width;
					rectangle.Height = height;
					if (nGateDataArray[i * nAfe + j] == 0)
					{
						rectangle.Fill = new SolidColorBrush(Colors.LightYellow);
						GateCanvas.Children.Add(rectangle);
						continue;
					}
					rectangle.Fill = new SolidColorBrush(Colors.Gray);
					TextBlock textBlock = new TextBlock();
					textBlock.Margin = new Thickness(num, num2, num3, num4);
					textBlock.VerticalAlignment = VerticalAlignment.Center;
					textBlock.Width = width;
					textBlock.Height = height;
					double dwOddAVG = valueInfoList[i * nAfe + j].dwOddAVG;
					double dwEvenAVG = valueInfoList[i * nAfe + j].dwEvenAVG;
					double num5 = Math.Abs(valueInfoList[i * nAfe + j].dwOddAVG - valueInfoList[i * nAfe + j].dwEvenAVG);
					textBlock.Inlines.Add("Odd:\n");
					textBlock.Inlines.Add(dwOddAVG.ToString("0.00"));
					textBlock.Inlines.Add("\nEven:\n");
					textBlock.Inlines.Add(dwEvenAVG.ToString("0.00"));
					textBlock.Inlines.Add("\nSub:\n");
					textBlock.Inlines.Add(num5.ToString("0.00"));
					GateCanvas.Children.Add(rectangle);
					GateCanvas.Children.Add(textBlock);
				}
			}
		}

		private void StartStatistics()
		{
			nGateCount = 0;
			dwGateSubAve = 0.0;
			valueInfoList.Clear();
			Array.Clear(nGateDataArray, 0, nGateDataArray.Length);
			int num = nGateIC * nAfe;
			dwGateValue = Convert.ToSingle(GateValueTxt.Text);
			int num2 = 0;
			int num3 = 0;
			if (nWidth == 3072 && nHeight == 3072)
			{
				num2 = 336;
				num3 = 600;
			}
			else if (nWidth == 2304 && nHeight == 2800)
			{
				num2 = 500;
				num3 = 600;
			}
			for (int i = 0; i < nGateIC; i++)
			{
				for (int j = 0; j < nAfe; j++)
				{
					if (i == 0)
					{
						rc.top = 0;
						rc.bottom = num2;
					}
					else
					{
						rc.top = num2 + num3 * (i - 1);
						if (nWidth == 3072 && nHeight == 3072)
						{
							if (i == 5)
							{
								rc.bottom = nHeight;
							}
							else
							{
								rc.bottom = num2 + num3 * i;
							}
						}
						else if (nWidth == 2304 && nHeight == 2800)
						{
							if (i == 4)
							{
								rc.bottom = nHeight;
							}
							else
							{
								rc.bottom = num2 + num3 * i;
							}
						}
					}
					if (j == 0)
					{
						rc.left = 5;
					}
					else
					{
						rc.left = j * nWidth / nAfe;
					}
					if (j == 23)
					{
						rc.right = nWidth - 4;
					}
					else
					{
						rc.right = (j + 1) * nWidth / nAfe;
					}
					ValueInfo info = default(ValueInfo);
					EvaluatePerGate(rc, ref info);
					int num4 = IsValidGate(ref info);
					nGateDataArray[i * nAfe + j] = num4;
					if (num4 == 1)
					{
						nGateCount++;
					}
					valueInfoList.Add(info);
				}
			}
			double num5 = dwGateSubAve / (double)num;
			LabelTotal.Content = nGateCount.ToString();
			LabelSubAve.Content = num5.ToString("0.00");
			Draw();
		}

		private void EvaluatePerGate(RectInfo Rect, ref ValueInfo info)
		{
			int[] array = new int[2];
			double[] array2 = new double[2];
			double[] array3 = new double[2];
			for (int i = Rect.left; i < Rect.right; i++)
			{
				array[i & 1]++;
			}
			array[0] *= Rect.bottom - Rect.top;
			array[1] *= Rect.bottom - Rect.top;
			for (int j = Rect.top; j < Rect.bottom; j++)
			{
				for (int i = Rect.left; i < Rect.right; i++)
				{
					array3[i & 1] += (int)ImgData[j * nWidth + i];
				}
			}
			array3[0] /= array[0];
			array3[1] /= array[1];
			info.dwEvenAVG = array3[0];
			info.dwOddAVG = array3[1];
			for (int j = Rect.top; j < Rect.bottom; j++)
			{
				for (int i = Rect.left; i < Rect.right; i++)
				{
					array2[i & 1] += Math.Pow((double)(int)ImgData[j * nWidth + i] - array3[i & 1], 2.0);
				}
			}
			array2[0] = Math.Sqrt(array2[0] / (double)(array[0] - 1));
			array2[1] = Math.Sqrt(array2[1] / (double)(array[1] - 1));
			info.dwEvenSV = array2[0];
			info.dwOddSV = array2[1];
		}

		private int IsValidGate(ref ValueInfo info)
		{
			double num = Math.Abs(info.dwEvenAVG - info.dwOddAVG);
			dwGateSubAve = num + dwGateSubAve;
			if (num <= dwGateValue)
			{
				return 0;
			}
			return 1;
		}

		private int Initialize(GateValueInfo info)
		{
			nWidth = info.nWidth;
			nHeight = info.nHeight;
			if (nWidth == 3072 && nHeight == 3072)
			{
				nGateIC = 6;
				nAfe = 24;
			}
			else
			{
				if (nWidth != 2304 || nHeight != 2800)
				{
					nWidth = 0;
					nHeight = 0;
					return 1;
				}
				nGateIC = 5;
				nAfe = 18;
			}
			nGateDataArray = new int[500];
			int num = nGateIC * nAfe;
			for (int i = 0; i < num; i++)
			{
				nGateDataArray[i] = 0;
			}
			nGateCount = 0;
			dwGateSubAve = 0.0;
			valueInfoList = new List<ValueInfo>();
			ImgData = new ushort[nWidth * nHeight];
			Array.Copy(info.ImgData, ImgData, ImgData.Length);
			Draw();
			return 0;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/gatestatisticspage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				LabelTotal = (Label)target;
				break;
			case 2:
				LabelSubAve = (Label)target;
				break;
			case 3:
				GateValueTxt = (TextBox)target;
				GateValueTxt.TextChanged += GateValueTxt_TextChanged;
				break;
			case 4:
				StatisticsButton = (Button)target;
				StatisticsButton.Click += StatisticsButton_Click;
				break;
			case 5:
				GateCanvas = (Canvas)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
