using System.Windows.Controls;

namespace iDetector
{
	public class TifList : UserControl
	{
		private Label TifName;

		private Label TifValue;

		private Grid Framework;

		public Enm_ImageTag Id;

		public string Value
		{
			get
			{
				return TifValue.Content.ToString();
			}
			set
			{
				TifValue.Content = value;
			}
		}

		public TifList(Enm_ImageTag id, string name, string value = "")
		{
			Framework = new Grid();
			Framework.ColumnDefinitions.Add(new ColumnDefinition());
			Framework.ColumnDefinitions.Add(new ColumnDefinition());
			TifName = new Label
			{
				Width = 100.0,
				Height = 30.0
			};
			TifValue = new Label
			{
				Width = 100.0,
				Height = 30.0
			};
			Framework.Children.Add(TifName);
			Grid.SetColumn(TifName, 0);
			Framework.Children.Add(TifValue);
			Grid.SetColumn(TifValue, 1);
			AddChild(Framework);
			TifName.Content = name;
			TifValue.Content = value;
			Id = id;
		}
	}
}
