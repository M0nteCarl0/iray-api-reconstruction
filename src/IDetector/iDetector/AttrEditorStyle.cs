namespace iDetector
{
	public struct AttrEditorStyle
	{
		public bool bCheckBoxForIntType;

		public int nLabelColWidth;

		public int nReadColWidth;

		public int nWriteColWidth;

		public int nEditorColWidth;

		public int nBtnColWidth;
	}
}
