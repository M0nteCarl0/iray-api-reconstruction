using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace iDetector
{
	public class SDKEnumCollector
	{
		public static ObservableCollection<EnumDisplayItem> LoadParam(string enumName)
		{
			int pCount = 0;
			SdkInterface.GetEnumItemsCount(enumName, ref pCount);
			EnumItem[] arr = new EnumItem[pCount];
			IntPtr intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
			SdkInterface.GetEnumItemList(enumName, intPtr, pCount);
			SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref arr);
			Marshal.FreeHGlobal(intPtr);
			ObservableCollection<EnumDisplayItem> observableCollection = new ObservableCollection<EnumDisplayItem>();
			EnumItem[] array = arr;
			for (int i = 0; i < array.Length; i++)
			{
				EnumItem enumItem = array[i];
				EnumDisplayItem enumDisplayItem = new EnumDisplayItem();
				enumDisplayItem.Label = enumItem.strName;
				enumDisplayItem.Val = enumItem.nVal;
				EnumDisplayItem item = enumDisplayItem;
				observableCollection.Add(item);
			}
			return observableCollection;
		}

		public static EnumDisplayItem GetItem(ObservableCollection<EnumDisplayItem> itemList, int curIndex)
		{
			if (itemList == null)
			{
				return null;
			}
			EnumDisplayItem result = null;
			foreach (EnumDisplayItem item in itemList)
			{
				if (item.Val == curIndex)
				{
					return item;
				}
			}
			return result;
		}
	}
}
