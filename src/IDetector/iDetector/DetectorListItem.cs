using System.ComponentModel;

namespace iDetector
{
	public class DetectorListItem : INotifyPropertyChanged
	{
		public string curConnInfo;

		private string _strBindName;

		private string _strSN;

		private string _strProductType;

		private string _strWorkDir;

		private DetectorState _state;

		private Detector _instance;

		private DetPage _page;

		public string BindName
		{
			get
			{
				return _strBindName;
			}
			set
			{
				_strBindName = value;
				NotifyPropertyChanged("BindName");
			}
		}

		public string SN
		{
			get
			{
				return _strSN;
			}
			set
			{
				_strSN = value;
				NotifyPropertyChanged("SN");
			}
		}

		public string ProductType
		{
			get
			{
				return _strProductType;
			}
			set
			{
				_strProductType = value;
				NotifyPropertyChanged("ProductType");
			}
		}

		public string WorkDir
		{
			get
			{
				return _strWorkDir;
			}
			set
			{
				_strWorkDir = value;
				NotifyPropertyChanged("WorkDir");
			}
		}

		public DetectorState State
		{
			get
			{
				return _state;
			}
			set
			{
				_state = value;
				NotifyPropertyChanged("State");
			}
		}

		public bool IsOccupied
		{
			get;
			set;
		}

		public Detector Instance
		{
			get
			{
				return _instance;
			}
			set
			{
				_instance = value;
			}
		}

		public DetPage Page
		{
			get
			{
				return _page;
			}
			set
			{
				_page = value;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private void NotifyPropertyChanged(string strPropertyName)
		{
			PropertyChangedEventArgs e = new PropertyChangedEventArgs(strPropertyName);
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, e);
			}
		}
	}
}
