namespace iDetector
{
	public class SeqSaveSetSave
	{
		public string FilePath;

		public int Frames = 100;

		public int MaxAddFrames = 16;

		public float NoiseCoeff = 0.5f;

		public bool IsEnableCine;

		public bool EnableSuperpostion;

		public bool EnableReduceNoise;

		public int StartIndex;

		public int EndIndex;
	}
}
