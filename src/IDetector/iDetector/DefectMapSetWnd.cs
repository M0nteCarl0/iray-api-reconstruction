using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace iDetector
{
	public class DefectMapSetWnd : Window, IComponentConnector
	{
		private enum Enm_UpdateTarget
		{
			Enm_Point = 1,
			Enm_LineStart,
			Enm_LineEnd
		}

		private byte[] mDefectMapData;

		private string mFilePath;

		public int nWidth;

		public int nHeight;

		private bool mbCompleted;

		private TextBox mXPosText;

		private TextBox mYPosText;

		public bool hasDirtyData;

		private DefectMapInfo mDefectMapInfo;

		private Enm_UpdateTarget meTarget;

		internal DigitalTextBox PointX;

		internal DigitalTextBox PointY;

		internal Button btnAddFromImage;

		internal DigitalTextBox LineX1Pos;

		internal DigitalTextBox LineY1Pos;

		internal DigitalTextBox LineX2Pos;

		internal DigitalTextBox LineY2Pos;

		private bool _contentLoaded;

		public byte[] DefectData
		{
			get
			{
				return mDefectMapData;
			}
		}

		public event Action PaintView;

		public event SaveDefectHandler SaveDefectAction;

		public DefectMapSetWnd(string path)
		{
			mFilePath = path;
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
		}

		public void InitDefectData(ref byte[] defectData, int nW, int nH, string path)
		{
			mFilePath = path;
			nWidth = nW;
			nHeight = nH;
			mDefectMapData = defectData;
			if (defectData != null && defectData.Length == nW * nH)
			{
				mbCompleted = true;
			}
			else
			{
				mbCompleted = false;
			}
			hasDirtyData = false;
			Paint();
		}

		private void Button_AddPoint(object sender, RoutedEventArgs e)
		{
			SetPoint(byte.MaxValue);
		}

		private void Button_DelPoint(object sender, RoutedEventArgs e)
		{
			SetPoint(0);
		}

		private void SetPoint(byte value)
		{
			if (mbCompleted)
			{
				int value2 = PointX.Value;
				int value3 = PointY.Value;
				if (value2 >= nWidth || value3 >= nHeight)
				{
					MessageBox.Show("location is out of range");
					return;
				}
				mDefectMapData[value3 * nWidth + value2] = value;
				Paint();
				hasDirtyData = true;
				btnAddFromImage.IsEnabled = false;
			}
		}

		private void Button_AddLine(object sender, RoutedEventArgs e)
		{
			SetLine(byte.MaxValue);
		}

		private void Button_DelLine(object sender, RoutedEventArgs e)
		{
			SetLine(0);
		}

		private void SetLine(byte value)
		{
			if (!mbCompleted)
			{
				return;
			}
			int value2 = LineX1Pos.Value;
			int value3 = LineY1Pos.Value;
			int value4 = LineX2Pos.Value;
			int value5 = LineY2Pos.Value;
			int num = Math.Min(value2, value4);
			int num2 = Math.Max(value2, value4);
			int num3 = Math.Min(value3, value5);
			int num4 = Math.Max(value3, value5);
			if (num >= nWidth || num3 >= nHeight || num2 >= nWidth || num4 >= nHeight)
			{
				MessageBox.Show("location is out of range");
				return;
			}
			if (num != num2 && num3 != num4)
			{
				MessageBox.Show("Please select vertical or horizontal a line");
				return;
			}
			if (num == num2)
			{
				for (int i = num3; i <= num4; i++)
				{
					mDefectMapData[i * nWidth + num] = value;
				}
			}
			else if (num3 == num4)
			{
				for (int j = num; j <= num2; j++)
				{
					mDefectMapData[num3 * nWidth + j] = value;
				}
			}
			Paint();
			hasDirtyData = true;
			btnAddFromImage.IsEnabled = false;
		}

		public void SaveDefectMap()
		{
			Button_Save(null, null);
		}

		private void Button_Save(object sender, RoutedEventArgs e)
		{
			if (mbCompleted)
			{
				if (this.SaveDefectAction != null)
				{
					this.SaveDefectAction(mDefectMapData);
				}
				hasDirtyData = false;
				btnAddFromImage.IsEnabled = true;
			}
		}

		public void SetSource(object srcObj)
		{
			mXPosText = new TextBox();
			Binding binding = new Binding("XPos");
			binding.Source = srcObj;
			Binding binding2 = binding;
			mXPosText.SetBinding(TextBox.TextProperty, binding2);
			mYPosText = new TextBox();
			Binding binding3 = new Binding("YPos");
			binding3.Source = srcObj;
			Binding binding4 = binding3;
			mYPosText.SetBinding(TextBox.TextProperty, binding4);
			base.DataContext = srcObj;
		}

		private void GotMouseFocus(object sender, RoutedEventArgs e)
		{
			if (sender.Equals(LineX1Pos) || sender.Equals(LineY1Pos))
			{
				meTarget = Enm_UpdateTarget.Enm_LineStart;
			}
			else if (sender.Equals(LineX2Pos) || sender.Equals(LineY2Pos))
			{
				meTarget = Enm_UpdateTarget.Enm_LineEnd;
			}
			else if (sender.Equals(PointX) || sender.Equals(PointY))
			{
				meTarget = Enm_UpdateTarget.Enm_Point;
			}
		}

		public void UpdatePosition()
		{
			if (Enm_UpdateTarget.Enm_LineStart == meTarget)
			{
				LineX1Pos.Text = mXPosText.Text;
				LineY1Pos.Text = mYPosText.Text;
			}
			else if (Enm_UpdateTarget.Enm_LineEnd == meTarget)
			{
				Point adjustPoint = new Point(Convert.ToDouble(mXPosText.Text), Convert.ToDouble(mYPosText.Text));
				AdjustLinePosition(ref adjustPoint, new Point(Convert.ToDouble(LineX1Pos.Text), Convert.ToDouble(LineY1Pos.Text)));
				LineX2Pos.Text = adjustPoint.X.ToString("F0");
				LineY2Pos.Text = adjustPoint.Y.ToString("F0");
			}
			else if (Enm_UpdateTarget.Enm_Point == meTarget)
			{
				PointX.Text = mXPosText.Text;
				PointY.Text = mYPosText.Text;
			}
		}

		private void AdjustLinePosition(ref Point adjustPoint, Point basePoint)
		{
			double num = Math.Abs(adjustPoint.X - basePoint.X);
			double num2 = Math.Abs(adjustPoint.Y - basePoint.Y);
			if (num > num2)
			{
				adjustPoint.Y = basePoint.Y;
			}
			else
			{
				adjustPoint.X = basePoint.X;
			}
		}

		private void Paint()
		{
			if (this.PaintView != null)
			{
				this.PaintView();
			}
		}

		private void Button_AddFromImage(object sender, RoutedEventArgs e)
		{
			int num = nWidth * nHeight;
			mDefectMapInfo.nWidth = nWidth;
			mDefectMapInfo.nHeight = nHeight;
			mDefectMapInfo.defectMapData = new byte[num];
			Buffer.BlockCopy(mDefectMapData, 0, mDefectMapInfo.defectMapData, 0, num);
			DefectFromImageWnd defectFromImageWnd = new DefectFromImageWnd(mDefectMapInfo);
			if (defectFromImageWnd.ShowDialog() == true && this.SaveDefectAction != null)
			{
				string strPath = mFilePath;
				if (!BackupDefectFile(strPath))
				{
					MessageBox.Show("The defect file backup error!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
					return;
				}
				Buffer.BlockCopy(mDefectMapInfo.defectMapData, 0, mDefectMapData, 0, num);
				this.SaveDefectAction(mDefectMapData);
				Paint();
			}
		}

		private bool BackupDefectFile(string strPath)
		{
			if (strPath == null)
			{
				return false;
			}
			long diskFreeSpace = Utility.GetDiskFreeSpace(Utility.GetDiskName(strPath));
			int num = nWidth * nWidth * 2 / 1024 / 1024 + 2;
			if (diskFreeSpace <= num)
			{
				return false;
			}
			try
			{
				DateTime now = DateTime.Now;
				string directoryName = Path.GetDirectoryName(strPath);
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(strPath);
				string extension = Path.GetExtension(strPath);
				string text = now.Year.ToString() + now.Month.ToString() + now.Day.ToString() + now.Hour.ToString() + now.Minute.ToString() + now.Second.ToString();
				string destFileName = directoryName + "\\" + fileNameWithoutExtension + "_" + text + extension;
				File.Copy(strPath, destFileName, true);
			}
			catch (Exception ex)
			{
				ex.ToString();
				return false;
			}
			return true;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/defectmapsetwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				PointX = (DigitalTextBox)target;
				break;
			case 2:
				PointY = (DigitalTextBox)target;
				break;
			case 3:
				((Button)target).Click += Button_AddPoint;
				break;
			case 4:
				((Button)target).Click += Button_DelPoint;
				break;
			case 5:
				btnAddFromImage = (Button)target;
				btnAddFromImage.Click += Button_AddFromImage;
				break;
			case 6:
				LineX1Pos = (DigitalTextBox)target;
				break;
			case 7:
				LineY1Pos = (DigitalTextBox)target;
				break;
			case 8:
				LineX2Pos = (DigitalTextBox)target;
				break;
			case 9:
				LineY2Pos = (DigitalTextBox)target;
				break;
			case 10:
				((Button)target).Click += Button_AddLine;
				break;
			case 11:
				((Button)target).Click += Button_DelLine;
				break;
			case 12:
				((Button)target).Click += Button_Save;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
