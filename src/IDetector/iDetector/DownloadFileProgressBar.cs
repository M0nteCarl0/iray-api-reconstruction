using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace iDetector
{
	public class DownloadFileProgressBar : Window
	{
		private const int TimeIntervalMillisecond = 200;

		public ProgressBar ProgressBar;

		public Label PercentValue;

		private DockPanel MainFramework;

		private Label PercentSymbol;

		private DispatcherTimer Timer;

		private int LoopCount;

		public DownloadFileProgressBar(string title, int timeoutSeconds = 120)
		{
			base.Width = 310.0;
			base.Height = 65.0;
			MainFramework = new DockPanel();
			AddChild(MainFramework);
			ProgressBar = new ProgressBar
			{
				Width = 240.0,
				Height = 25.0,
				VerticalAlignment = VerticalAlignment.Center
			};
			PercentValue = new Label
			{
				Width = 35.0,
				Height = 30.0,
				VerticalAlignment = VerticalAlignment.Center
			};
			PercentSymbol = new Label
			{
				Content = "%",
				Width = 20.0,
				Height = 30.0,
				VerticalAlignment = VerticalAlignment.Center
			};
			MainFramework.Children.Add(ProgressBar);
			MainFramework.Children.Add(PercentValue);
			MainFramework.Children.Add(PercentSymbol);
			base.Title = title;
			base.WindowStyle = WindowStyle.ToolWindow;
			base.ResizeMode = ResizeMode.NoResize;
			base.WindowStartupLocation = WindowStartupLocation.CenterOwner;
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			Timer = new DispatcherTimer();
			Timer.Interval = TimeSpan.FromMilliseconds(200.0);
			Timer.Tick += OnTimer;
			Timer.Start();
			LoopCount = timeoutSeconds * 1000 / 200;
		}

		protected virtual void UpdateProgress()
		{
			throw new NotImplementedException();
		}

		private void OnTimer(object sender, EventArgs e)
		{
			if (LoopCount-- > 0)
			{
				UpdateProgress();
				return;
			}
			Timer.Stop();
			OnClose();
			MessageBox.Show("Task time out!");
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (ProgressBar.Value < 99.0)
			{
				e.Cancel = true;
				return;
			}
			Timer.Stop();
			base.OnClosing(e);
		}

		public void OnClose()
		{
			ProgressBar.Value = 100.0;
			Close();
		}
	}
}
