namespace iDetector
{
	public class WifiChannelTable
	{
		private struct ChannelTable
		{
			public Enm_Wifi_CountryCode CountryCode;

			public Enm_Wifi_Frequency Frequency;

			public Enm_Wifi_BandWidth BandWidth;

			public int[] Channels;
		}

		private static ChannelTable[] tables;

		public static int[] GetValidChannelList(Enm_Wifi_CountryCode countryCode, Enm_Wifi_Frequency freq, Enm_Wifi_BandWidth band)
		{
			ChannelTable[] array = tables;
			for (int i = 0; i < array.Length; i++)
			{
				ChannelTable channelTable = array[i];
				if (channelTable.CountryCode == countryCode && channelTable.Frequency == freq && channelTable.BandWidth == band)
				{
					return channelTable.Channels;
				}
			}
			return null;
		}

		static WifiChannelTable()
		{
			ChannelTable[] array = new ChannelTable[60]
			{
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_CN,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[13]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_CN,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[7]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_CN,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[9]
					{
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_CN,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[5]
					{
						149,
						153,
						157,
						161,
						165
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_CN,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[2]
					{
						149,
						157
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_CN,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[2]
					{
						153,
						161
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_DE,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[13]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_DE,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[9]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_DE,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[9]
					{
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_DE,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[4]
					{
						36,
						40,
						44,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_DE,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[2]
					{
						36,
						44
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_DE,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[2]
					{
						40,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_FR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[13]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_FR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[9]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_FR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[9]
					{
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_FR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[4]
					{
						36,
						40,
						44,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_FR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[2]
					{
						36,
						44
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_FR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[2]
					{
						40,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_GB,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[13]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_GB,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[9]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_GB,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[9]
					{
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_GB,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[4]
					{
						36,
						40,
						44,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_GB,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[2]
					{
						36,
						44
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_GB,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[2]
					{
						40,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_HK,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[11]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_HK,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[7]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_HK,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[7]
					{
						5,
						6,
						7,
						8,
						9,
						10,
						11
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_HK,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[9]
					{
						36,
						40,
						44,
						48,
						149,
						153,
						157,
						161,
						165
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_HK,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[4]
					{
						36,
						44,
						149,
						157
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_HK,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[4]
					{
						40,
						48,
						153,
						161
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_IT,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[13]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_IT,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[9]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_IT,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[9]
					{
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_IT,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[4]
					{
						36,
						40,
						44,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_IT,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
					Channels = new int[2]
					{
						36,
						44
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_IT,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
					Channels = new int[2]
					{
						40,
						48
					}
				},
				new ChannelTable
				{
					CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_KR,
					Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
					BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
					Channels = new int[13]
					{
						1,
						2,
						3,
						4,
						5,
						6,
						7,
						8,
						9,
						10,
						11,
						12,
						13
					}
				},
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable),
				default(ChannelTable)
			};
			ref ChannelTable reference = ref array[37];
			ChannelTable channelTable = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_KR,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus
			};
			int[] array2 = channelTable.Channels = new int[1];
			reference = channelTable;
			ref ChannelTable reference2 = ref array[38];
			ChannelTable channelTable2 = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_KR,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus
			};
			array2 = (channelTable2.Channels = new int[1]);
			reference2 = channelTable2;
			array[39] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_KR,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[8]
				{
					36,
					40,
					44,
					48,
					149,
					153,
					157,
					161
				}
			};
			array[40] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_KR,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[4]
				{
					36,
					44,
					149,
					157
				}
			};
			array[41] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_KR,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[4]
				{
					40,
					48,
					153,
					161
				}
			};
			array[42] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_NL,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[13]
				{
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9,
					10,
					11,
					12,
					13
				}
			};
			array[43] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_NL,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[9]
				{
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9
				}
			};
			array[44] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_NL,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[9]
				{
					5,
					6,
					7,
					8,
					9,
					10,
					11,
					12,
					13
				}
			};
			array[45] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_NL,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[4]
				{
					36,
					40,
					44,
					48
				}
			};
			array[46] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_NL,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[2]
				{
					36,
					44
				}
			};
			array[47] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_NL,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[2]
				{
					40,
					48
				}
			};
			array[48] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_RU,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[13]
				{
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9,
					10,
					11,
					12,
					13
				}
			};
			array[49] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_RU,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[9]
				{
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9
				}
			};
			array[50] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_RU,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[9]
				{
					5,
					6,
					7,
					8,
					9,
					10,
					11,
					12,
					13
				}
			};
			array[51] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_RU,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[9]
				{
					36,
					40,
					44,
					48,
					149,
					153,
					157,
					161,
					165
				}
			};
			array[52] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_RU,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[4]
				{
					36,
					44,
					149,
					157
				}
			};
			array[53] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_RU,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[4]
				{
					40,
					48,
					153,
					161
				}
			};
			array[54] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_US,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[11]
				{
					1,
					2,
					3,
					4,
					5,
					6,
					7,
					8,
					9,
					10,
					11
				}
			};
			array[55] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_US,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[7]
				{
					1,
					2,
					3,
					4,
					5,
					6,
					7
				}
			};
			array[56] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_US,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_2GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[7]
				{
					5,
					6,
					7,
					8,
					9,
					10,
					11
				}
			};
			array[57] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_US,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT20,
				Channels = new int[9]
				{
					36,
					40,
					44,
					48,
					149,
					153,
					157,
					161,
					165
				}
			};
			array[58] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_US,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Plus,
				Channels = new int[4]
				{
					36,
					44,
					149,
					157
				}
			};
			array[59] = new ChannelTable
			{
				CountryCode = Enm_Wifi_CountryCode.Enm_Wifi_Country_US,
				Frequency = Enm_Wifi_Frequency.Enm_Wifi_Freq_5GHz,
				BandWidth = Enm_Wifi_BandWidth.Enm_Wifi_Band_HT40_Minus,
				Channels = new int[4]
				{
					40,
					48,
					153,
					161
				}
			};
			tables = array;
		}
	}
}
