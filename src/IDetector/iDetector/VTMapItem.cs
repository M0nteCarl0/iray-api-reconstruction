namespace iDetector
{
	public class VTMapItem : NotifyObject
	{
		public stVTMapItem vtItem;

		public int PGA
		{
			get
			{
				return vtItem.PGA;
			}
			set
			{
				vtItem.PGA = value;
				NotifyPropertyChanged("PGA");
			}
		}

		public int Binning
		{
			get
			{
				return vtItem.Binning;
			}
			set
			{
				vtItem.Binning = value;
				NotifyPropertyChanged("Binning");
			}
		}

		public float VTValue
		{
			get
			{
				return vtItem.VTValue;
			}
			set
			{
				vtItem.VTValue = value;
				NotifyPropertyChanged("VTValue");
			}
		}
	}
}
