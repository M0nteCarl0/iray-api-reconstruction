using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace iDetector
{
	public class DigitalTextBox : TextBox
	{
		public int Length
		{
			get
			{
				return base.Text.Length;
			}
		}

		public new string Text
		{
			get
			{
				if (base.Text.Length == 0)
				{
					return "0";
				}
				if (!Regex.IsMatch(base.Text, "^[0-9]+$"))
				{
					return "0";
				}
				return base.Text;
			}
			set
			{
				base.Text = value;
			}
		}

		public int Value
		{
			get
			{
				int result = 0;
				int.TryParse(Text, out result);
				return result;
			}
		}

		public DigitalTextBox()
		{
			base.MaxLength = 5;
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			if ((e.Key < Key.D0 || e.Key > Key.D9) && (e.Key < Key.NumPad0 || e.Key > Key.NumPad9) && e.Key != Key.Back && Key.Return != e.Key)
			{
				if (Key.RightCtrl != e.Key && Key.RightShift != e.Key && Key.LeftShift != e.Key)
				{
					MessageBox.Show("Please input a digital!");
				}
				e.Handled = true;
			}
		}

		protected override void OnTextChanged(TextChangedEventArgs e)
		{
			TextChange[] array = new TextChange[e.Changes.Count];
			e.Changes.CopyTo(array, 0);
			int offset = array[0].Offset;
			if (array[0].AddedLength > 0 && !Regex.IsMatch(base.Text, "^[0-9]+$"))
			{
				Text = base.Text.Remove(offset, array[0].AddedLength);
				Select(offset, 0);
			}
			base.OnTextChanged(e);
		}
	}
}
