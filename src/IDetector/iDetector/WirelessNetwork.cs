namespace iDetector
{
	public class WirelessNetwork
	{
		public string SSID
		{
			get;
			set;
		}

		public string SignalLevel
		{
			get;
			set;
		}
	}
}
