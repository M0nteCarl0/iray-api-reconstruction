namespace iDetector
{
	public struct APWriteParams
	{
		public int AttrWriteID;

		public int nValue;

		public string strValue;

		public IRAY_VAR_TYPE type;
	}
}
