using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;

namespace iDetector
{
	public class CtrAttrEditor : UserControl, IComponentConnector
	{
		public delegate void AttrValChangedDelegate(int nAttrID);

		private bool _bIsLoaded;

		private bool _bCheckBoxForIntType;

		private int _nReadAttrID;

		private int _nWriteAttrID;

		private Detector _objDetector;

		private EnumItem[] _enumItemsRead;

		private EnumItem[] _enumItemsWrite;

		private readonly Brush RWValueConflictColor = Brushes.Yellow;

		private readonly Brush TextColor = Brushes.White;

		private readonly Brush ComboxColor;

		internal ColumnDefinition _colDisplayName;

		internal ColumnDefinition _colReadVal;

		internal ColumnDefinition _colWriteVal;

		internal ColumnDefinition _colEdit;

		internal ColumnDefinition _colSetButton;

		internal TextBlock _dispName;

		internal TextBox _txtRead;

		internal TextBox _txtWrite;

		internal TextBox _txtEdit;

		internal Button _btnSet;

		internal CheckBox _checkRead;

		internal CheckBox _checkWrite;

		internal CheckBox _checkEdit;

		internal ComboBox _comboEdit;

		private bool _contentLoaded;

		public int ReadAttrID
		{
			get
			{
				return _nReadAttrID;
			}
		}

		public int WriteAttrID
		{
			get
			{
				return _nWriteAttrID;
			}
		}

		public CtrAttrEditor(string strLabel, int nReadAttrID, int nWriteAttrID, Detector objDetector)
		{
			InitializeComponent();
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				_bIsLoaded = false;
				_bCheckBoxForIntType = false;
				_dispName.Text = strLabel;
				_nReadAttrID = nReadAttrID;
				_nWriteAttrID = nWriteAttrID;
				_objDetector = objDetector;
				ComboxColor = _comboEdit.Background;
				base.Loaded += OnLoaded;
			}
		}

		public void SetCtrlStyle(AttrEditorStyle style)
		{
			_colDisplayName.Width = new GridLength(style.nLabelColWidth);
			_colReadVal.Width = new GridLength(style.nReadColWidth);
			_colWriteVal.Width = new GridLength(style.nWriteColWidth);
			_colEdit.Width = new GridLength(style.nEditorColWidth);
			_colSetButton.Width = new GridLength(style.nBtnColWidth);
			if (_bCheckBoxForIntType)
			{
				_checkEdit.Visibility = Visibility.Visible;
				_checkRead.Visibility = Visibility.Visible;
				_checkWrite.Visibility = Visibility.Visible;
				_txtEdit.Visibility = Visibility.Collapsed;
				_txtRead.Visibility = Visibility.Collapsed;
				_txtWrite.Visibility = Visibility.Collapsed;
				_comboEdit.Visibility = Visibility.Collapsed;
			}
			if (style.nWriteColWidth == 0)
			{
				_btnSet.Visibility = Visibility.Collapsed;
			}
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			if (_bIsLoaded)
			{
				return;
			}
			_bIsLoaded = true;
			_objDetector.AttrChangingMonitor.AttrValChangedEvent += OnAttrValChanged;
			int pCount = 0;
			string text = "";
			if (_nReadAttrID > 0)
			{
				AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(_nReadAttrID);
				if (attrMonitorItem != null)
				{
					text = attrMonitorItem.info.strUnit;
					if (attrMonitorItem.info.bIsEnum == 1)
					{
						SdkInterface.GetEnumItemsCount(attrMonitorItem.info.strEnumTypeName, ref pCount);
						_enumItemsRead = new EnumItem[pCount];
						IntPtr intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
						SdkInterface.GetEnumItemList(attrMonitorItem.info.strEnumTypeName, intPtr, pCount);
						SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref _enumItemsRead);
						Marshal.FreeHGlobal(intPtr);
					}
				}
			}
			if (_nWriteAttrID > 0)
			{
				AttrMonitorItem attrMonitorItem2 = _objDetector.AttrChangingMonitor.FindItem(_nWriteAttrID);
				if (attrMonitorItem2 != null)
				{
					if (text.Length == 0)
					{
						text = attrMonitorItem2.info.strUnit;
					}
					if (attrMonitorItem2.info.bIsEnum == 1)
					{
						SdkInterface.GetEnumItemsCount(attrMonitorItem2.info.strEnumTypeName, ref pCount);
						_enumItemsWrite = new EnumItem[pCount];
						IntPtr intPtr2 = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
						SdkInterface.GetEnumItemList(attrMonitorItem2.info.strEnumTypeName, intPtr2, pCount);
						SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr2, ref _enumItemsWrite);
						Marshal.FreeHGlobal(intPtr2);
						_comboEdit.Items.Clear();
						EnumDisplayItem selectedItem = null;
						EnumItem[] enumItemsWrite = _enumItemsWrite;
						for (int i = 0; i < enumItemsWrite.Length; i++)
						{
							EnumItem enumItem = enumItemsWrite[i];
							EnumDisplayItem enumDisplayItem = new EnumDisplayItem();
							enumDisplayItem.Val = enumItem.nVal;
							enumDisplayItem.Label = enumItem.strName;
							_comboEdit.Items.Add(enumDisplayItem);
							if (attrMonitorItem2.var.val.nVal == enumItem.nVal)
							{
								selectedItem = enumDisplayItem;
							}
						}
						_comboEdit.SelectedItem = selectedItem;
						_txtEdit.Visibility = Visibility.Collapsed;
						_checkEdit.Visibility = Visibility.Collapsed;
						_comboEdit.Visibility = Visibility.Visible;
					}
				}
			}
			if (text.Length != 0)
			{
				TextBlock dispName = _dispName;
				dispName.Text = dispName.Text + " (" + text + ")";
			}
			UpdateAttrVal(_nReadAttrID);
			UpdateAttrVal(_nWriteAttrID);
			_txtEdit.Text = _txtWrite.Text;
			_checkEdit.IsChecked = _checkWrite.IsChecked;
		}

		public void OnClose()
		{
			if (_objDetector != null)
			{
				_objDetector.AttrChangingMonitor.AttrValChangedEvent -= OnAttrValChanged;
			}
		}

		private void UpdateAttrVal(int nAttrID)
		{
			if (nAttrID <= 0 || (nAttrID != _nReadAttrID && nAttrID != _nWriteAttrID))
			{
				return;
			}
			AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(nAttrID);
			if (attrMonitorItem == null)
			{
				return;
			}
			if (nAttrID == _nReadAttrID)
			{
				if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_STR)
				{
					_txtRead.Text = attrMonitorItem.var.val.strVal;
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_FLT)
				{
					_txtRead.Text = attrMonitorItem.var.val.fVal.ToString("F2");
				}
				else if (attrMonitorItem.info.bIsEnum == 1)
				{
					_txtRead.Text = attrMonitorItem.var.val.nVal.ToString();
					EnumItem[] enumItemsRead = _enumItemsRead;
					for (int i = 0; i < enumItemsRead.Length; i++)
					{
						EnumItem enumItem = enumItemsRead[i];
						if (enumItem.nVal == attrMonitorItem.var.val.nVal)
						{
							_txtRead.Text = enumItem.strName;
							break;
						}
					}
				}
				else if (_bCheckBoxForIntType)
				{
					_checkRead.IsChecked = (attrMonitorItem.var.val.nVal != 0);
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_INT)
				{
					_txtRead.Text = attrMonitorItem.var.val.nVal.ToString();
				}
			}
			else if (nAttrID == _nWriteAttrID)
			{
				if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_STR)
				{
					_txtWrite.Text = attrMonitorItem.var.val.strVal;
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_FLT)
				{
					_txtWrite.Text = attrMonitorItem.var.val.fVal.ToString("F2");
				}
				else if (attrMonitorItem.info.bIsEnum == 1)
				{
					_txtWrite.Text = attrMonitorItem.var.val.nVal.ToString();
					if (_enumItemsWrite != null)
					{
						EnumItem[] enumItemsWrite = _enumItemsWrite;
						for (int j = 0; j < enumItemsWrite.Length; j++)
						{
							EnumItem enumItem2 = enumItemsWrite[j];
							if (enumItem2.nVal == attrMonitorItem.var.val.nVal)
							{
								_txtWrite.Text = enumItem2.strName;
								break;
							}
						}
					}
				}
				else if (_bCheckBoxForIntType)
				{
					_checkWrite.IsChecked = (attrMonitorItem.var.val.nVal != 0);
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_INT)
				{
					_txtWrite.Text = attrMonitorItem.var.val.nVal.ToString();
				}
			}
			CheckRWValueConfilct();
		}

		public void RefillEditValue()
		{
			if (_nWriteAttrID == 0)
			{
				return;
			}
			UpdateAttrVal(_nWriteAttrID);
			AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(_nWriteAttrID);
			if (attrMonitorItem != null)
			{
				if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_STR)
				{
					_txtEdit.Text = _txtWrite.Text;
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_FLT)
				{
					_txtEdit.Text = _txtWrite.Text;
				}
				else if (attrMonitorItem.info.bIsEnum == 1)
				{
					_comboEdit.SelectedValue = attrMonitorItem.var.val.nVal;
				}
				else if (_bCheckBoxForIntType)
				{
					_checkEdit.IsChecked = _checkWrite.IsChecked;
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_INT)
				{
					_txtEdit.Text = _txtWrite.Text;
				}
			}
		}

		public void RefillEditValue(int newValue)
		{
			AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(_nWriteAttrID);
			if (attrMonitorItem != null)
			{
				if (attrMonitorItem.info.bIsEnum == 1)
				{
					_comboEdit.SelectedValue = newValue;
				}
				else if (_bCheckBoxForIntType)
				{
					_checkEdit.IsChecked = _checkWrite.IsChecked;
				}
				else if (attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_INT)
				{
					_txtEdit.Text = newValue.ToString();
				}
			}
		}

		private bool GetEditVal(ref IRayVariant var)
		{
			AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(_nWriteAttrID);
			if (attrMonitorItem == null)
			{
				return false;
			}
			if (_txtEdit.Text.Length == 0)
			{
				MessageBox.Show(attrMonitorItem.info.strDisplayName + ": Value cannot be empty!");
				return false;
			}
			var.vt = attrMonitorItem.var.vt;
			if (var.vt == IRAY_VAR_TYPE.IVT_STR)
			{
				var.val.strVal = _txtEdit.Text;
			}
			else if (var.vt == IRAY_VAR_TYPE.IVT_FLT)
			{
				if (!float.TryParse(_txtEdit.Text, out var.val.fVal))
				{
					MessageBox.Show(attrMonitorItem.info.strDisplayName + ": incorrect format! Please input a float.");
					return false;
				}
			}
			else if (attrMonitorItem.info.bIsEnum == 1)
			{
				if (-1 == _comboEdit.SelectedIndex)
				{
					MessageBox.Show(attrMonitorItem.info.strDisplayName + ": no value selected!");
					return false;
				}
				var.val.nVal = (int)_comboEdit.SelectedValue;
			}
			else if (_bCheckBoxForIntType)
			{
				var.val.nVal = ((_checkEdit.IsChecked == true) ? 1 : 0);
			}
			else if (var.vt == IRAY_VAR_TYPE.IVT_INT && !int.TryParse(_txtEdit.Text, out var.val.nVal))
			{
				MessageBox.Show(attrMonitorItem.info.strDisplayName + ": incorrect format! Please input a Integer.");
				return false;
			}
			if (!Valify(ref var, ref attrMonitorItem.info))
			{
				return false;
			}
			return true;
		}

		private bool Valify(ref IRayVariant var, ref AttrInfo info)
		{
			switch (info.eValidator)
			{
			case PARAM_VALIDATOR.Validator_Null:
				return true;
			case PARAM_VALIDATOR.Validator_MinMax:
				if (var.vt == IRAY_VAR_TYPE.IVT_INT)
				{
					int num = (int)Math.Round(info.fMinValue, 0);
					int num2 = (int)Math.Round(info.fMaxValue, 0);
					if (var.val.nVal > num2 || var.val.nVal < num)
					{
						string messageBoxText2 = string.Format("Value of '{0}' should be in the range[{1}, {2}]", _dispName.Text, num, num2);
						MessageBox.Show(messageBoxText2, "iDetector", MessageBoxButton.OK, MessageBoxImage.Exclamation);
						return false;
					}
				}
				else if (var.vt == IRAY_VAR_TYPE.IVT_FLT)
				{
					float num3 = info.fMinValue;
					float num4 = info.fMaxValue;
					if (var.val.fVal > num4 || var.val.fVal < num3)
					{
						string messageBoxText3 = string.Format("Value of '{0}' should be in the range[{1}, {2}]", _dispName.Text, num3, num4);
						MessageBox.Show(messageBoxText3, "iDetector", MessageBoxButton.OK, MessageBoxImage.Exclamation);
						return false;
					}
				}
				break;
			case PARAM_VALIDATOR.Validator_FilePath:
				if (var.vt == IRAY_VAR_TYPE.IVT_STR)
				{
					Regex regex2 = new Regex("[*,;?<>|\"]");
					if (regex2.IsMatch(var.val.strVal))
					{
						string messageBoxText4 = string.Format("Value of '{0} : {1}' is not a valid file path", _dispName.Text, var.val.strVal);
						MessageBox.Show(messageBoxText4, "iDetector", MessageBoxButton.OK, MessageBoxImage.Exclamation);
						return false;
					}
				}
				break;
			case PARAM_VALIDATOR.Validator_IP:
				if (var.vt == IRAY_VAR_TYPE.IVT_STR)
				{
					Regex regex = new Regex("^((\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])\\.){3}(\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])$");
					if (!regex.IsMatch(var.val.strVal))
					{
						string messageBoxText = string.Format("Value of '{0} : {1}' is not a valid IP address", _dispName.Text, var.val.strVal);
						MessageBox.Show(messageBoxText, "iDetector", MessageBoxButton.OK, MessageBoxImage.Exclamation);
						return false;
					}
				}
				break;
			case PARAM_VALIDATOR.Validator_FpdSN:
				if (var.vt != IRAY_VAR_TYPE.IVT_STR)
				{
				}
				break;
			case PARAM_VALIDATOR.Validator_MAC:
			{
				IRAY_VAR_TYPE vt = var.vt;
				int num5 = 2;
				break;
			}
			}
			return true;
		}

		public bool Accept()
		{
			if (_nWriteAttrID == 0)
			{
				return true;
			}
			IRayVariant var = default(IRayVariant);
			if (GetEditVal(ref var))
			{
				int num = SdkInterface.SetAttr(_objDetector.ID, _nWriteAttrID, ref var);
				if (num != 0)
				{
					ErrorInfo pInfo = default(ErrorInfo);
					SdkInterface.GetErrInfo(num, ref pInfo);
					AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(_nWriteAttrID);
					MessageBox.Show(attrMonitorItem.info.strDisplayName + " Set Failed! Err = " + pInfo.strDescription);
					return false;
				}
				return true;
			}
			return false;
		}

		private void OnAttrValChanged(int nAttrID, string strAttrName)
		{
			UpdateAttrVal(nAttrID);
		}

		private void _btnSet_Click(object sender, RoutedEventArgs e)
		{
			Accept();
		}

		private void OnEditValueChanged(object sender, TextChangedEventArgs e)
		{
			CheckRWValueConfilct();
		}

		private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			CheckRWValueConfilct();
		}

		private void CheckRWValueConfilct()
		{
			if (_objDetector == null || (_txtEdit.Visibility != 0 && _comboEdit.Visibility != 0))
			{
				return;
			}
			AttrMonitorItem attrMonitorItem = _objDetector.AttrChangingMonitor.FindItem(_nWriteAttrID);
			if (attrMonitorItem == null)
			{
				return;
			}
			bool flag = false;
			Control control = _txtEdit;
			Brush background = TextColor;
			TextBox textBox = _txtRead;
			if (_nReadAttrID == 0)
			{
				textBox = _txtWrite;
			}
			if (_comboEdit.Visibility == Visibility.Visible)
			{
				if (_comboEdit.SelectedItem is EnumDisplayItem)
				{
					flag = !textBox.Text.Equals((_comboEdit.SelectedItem as EnumDisplayItem).Label);
				}
				control = _comboEdit;
				background = ComboxColor;
			}
			else if (attrMonitorItem != null && IRAY_VAR_TYPE.IVT_FLT == attrMonitorItem.var.vt)
			{
				float result = 0f;
				float result2 = 0f;
				if (float.TryParse(_txtEdit.Text, out result))
				{
					float.TryParse(textBox.Text, out result2);
					if (result.CompareTo(result2) != 0)
					{
						flag = true;
					}
				}
				else
				{
					flag = true;
				}
			}
			else if (attrMonitorItem != null && attrMonitorItem.var.vt == IRAY_VAR_TYPE.IVT_INT)
			{
				int result3 = 0;
				int result4 = 0;
				if (int.TryParse(_txtEdit.Text, out result3))
				{
					int.TryParse(textBox.Text, out result4);
					if (result3.CompareTo(result4) != 0)
					{
						flag = true;
					}
				}
				else
				{
					flag = true;
				}
			}
			else
			{
				flag = !_txtEdit.Text.Equals(textBox.Text);
			}
			if (flag)
			{
				control.Background = RWValueConflictColor;
			}
			else
			{
				control.Background = background;
			}
		}

		public static double MeasureTextWidth(string text, double fontSize, string fontFamily)
		{
			FormattedText formattedText = new FormattedText(text, CultureInfo.InvariantCulture, FlowDirection.LeftToRight, new Typeface(fontFamily), fontSize, Brushes.Black);
			return formattedText.WidthIncludingTrailingWhitespace;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/usercontrol/ctrattreditor.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_colDisplayName = (ColumnDefinition)target;
				break;
			case 2:
				_colReadVal = (ColumnDefinition)target;
				break;
			case 3:
				_colWriteVal = (ColumnDefinition)target;
				break;
			case 4:
				_colEdit = (ColumnDefinition)target;
				break;
			case 5:
				_colSetButton = (ColumnDefinition)target;
				break;
			case 6:
				_dispName = (TextBlock)target;
				break;
			case 7:
				_txtRead = (TextBox)target;
				break;
			case 8:
				_txtWrite = (TextBox)target;
				break;
			case 9:
				_txtEdit = (TextBox)target;
				_txtEdit.TextChanged += OnEditValueChanged;
				break;
			case 10:
				_btnSet = (Button)target;
				_btnSet.Click += _btnSet_Click;
				break;
			case 11:
				_checkRead = (CheckBox)target;
				break;
			case 12:
				_checkWrite = (CheckBox)target;
				break;
			case 13:
				_checkEdit = (CheckBox)target;
				break;
			case 14:
				_comboEdit = (ComboBox)target;
				_comboEdit.SelectionChanged += OnSelectionChanged;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
