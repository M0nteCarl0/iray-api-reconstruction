using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace iDetector
{
	internal class PageButtonControl
	{
		private Button CurrentButton;

		private Brush NormalBorderBrush;

		private Brush NormalForegroundBrush;

		private Brush LightBrush;

		public PageButtonControl(Button button)
		{
			LightBrush = new SolidColorBrush(Color.FromRgb(28, 160, 136));
			NormalBorderBrush = button.BorderBrush;
			NormalForegroundBrush = button.Foreground;
			LightButton(button);
			CurrentButton = button;
		}

		public void LightButton(Button button)
		{
			if (CurrentButton != button)
			{
				if (CurrentButton != null)
				{
					CurrentButton.BorderBrush = NormalBorderBrush;
					CurrentButton.Foreground = NormalForegroundBrush;
					CurrentButton.FontWeight = FontWeights.Normal;
				}
				CurrentButton = button;
				CurrentButton.BorderBrush = LightBrush;
				CurrentButton.Foreground = LightBrush;
				CurrentButton.FontWeight = FontWeights.Bold;
			}
		}
	}
}
