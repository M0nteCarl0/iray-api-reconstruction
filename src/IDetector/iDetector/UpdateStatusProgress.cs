using System;
using System.Windows;

namespace iDetector
{
	public class UpdateStatusProgress : IProgress
	{
		private MessageWnd messageBox;

		private MMTimer timer;

		private int timeOutCount;

		public bool IsTaskRunning
		{
			get;
			set;
		}

		public UpdateStatusProgress(Window owner, string title, int intervalMillsecond, int timeoutMillsecond)
		{
			messageBox = new MessageWnd(title, owner);
			timer = new MMTimer(intervalMillsecond, UpdateStatus);
			timeOutCount = timeoutMillsecond / intervalMillsecond;
		}

		public int RunTask()
		{
			IsTaskRunning = true;
			timer.Start();
			messageBox.Dispatcher.BeginInvoke((Action)delegate
			{
				messageBox.ShowDialog();
			});
			return 0;
		}

		public void StopTask()
		{
			IsTaskRunning = false;
			TerminateTask();
		}

		public void SetStatus(string status)
		{
			if (messageBox != null)
			{
				messageBox.SetMessage(status);
			}
		}

		private void TerminateTask()
		{
			timer.Close();
			messageBox.Dispatcher.Invoke((Action)delegate
			{
				messageBox.Close();
			}, new object[0]);
		}

		private void UpdateStatus()
		{
			if (timeOutCount-- <= 0)
			{
				TerminateTask();
			}
		}
	}
}
