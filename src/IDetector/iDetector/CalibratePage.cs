using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class CalibratePage : UserControl, IDetSubPage, IComponentConnector
	{
		private enum Enm_CalibrateType
		{
			Enm_Offset = 1,
			Enm_StaicGain,
			Enm_Gain,
			Enm_Defect,
			Enm_GainDefect,
			Enm_Lag
		}

		private enum Enm_CreateStage
		{
			Enm_Init = 1,
			Enm_Acquire,
			Enm_GotImage,
			Enm_GotImageFailed,
			Enm_Select,
			Enm_Create,
			Enm_AcquireFinished,
			Enm_Abort,
			Enm_Finished
		}

		private DlgTaskTimeOut MessageBoxTimer;

		private int TaskTimeoutSeconds = 10;

		private ModeList mCurSelMode;

		private SDKInvokeProxy SdkInvokeProxy;

		public ToolTipMsgDel ShowToolTip;

		private Detector instance;

		private DownloadCorrProgressWnd DownloadWnd;

		private string TemplatePath = "Correct";

		private string[] templateStateInfo = new string[5]
		{
			"absent",
			"valid",
			"valid",
			"invalid",
			"unmatched"
		};

		private string[] fileTypeName = new string[5]
		{
			"",
			"Offset",
			"Gain",
			"MostGain",
			"Defect"
		};

		internal ColumnDefinition xTemplateColumn;

		internal ColumnDefinition xSplitter;

		internal StackPanel xTemplatePanel;

		internal Grid xApplicationMode;

		internal ComboBox ModeCombox;

		internal ListView _ModeList;

		internal GridView xModeListView;

		internal GridViewColumn xPGAHeader;

		internal GridViewColumn xFullWellHeader;

		internal GridViewColumn xZoomHeader;

		internal GridViewColumn xROIHeader;

		internal GridViewColumn xExpMode;

		internal GroupBox xMgrCorrFilePanel;

		internal TabControl xHWFileList;

		internal Button xUpdateHWPreoffsetBtn;

		internal Button xUpdateHWFreqCoeffBtn;

		internal Button xReverseVCOM;

		internal Grid xPopWindow;

		internal GroupBox xPresetOffset;

		internal RadioButton xSWPreOffsetRB;

		internal RadioButton xSWPostOffsetRB;

		private bool _contentLoaded;

		public bool bHandled
		{
			get;
			set;
		}

		private bool mbCanExit
		{
			get;
			set;
		}

		private bool bInitedLayout
		{
			get;
			set;
		}

		public virtual Detector Instance
		{
			get
			{
				return instance;
			}
			set
			{
				instance = value;
				if (instance != null)
				{
					if (this.SdkEventHandler == null)
					{
						this.SdkEventHandler = ProcessEvent;
					}
					instance.SubscribeCBEvent(OnSdkCallback);
				}
			}
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		public event Action UpdateDetPageParams;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public CalibratePage()
		{
			InitializeComponent();
			bInitedLayout = false;
			mbCanExit = true;
			base.IsVisibleChanged += VisibleChanged;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.NewValue.Equals(true))
			{
				if (xApplicationMode.Visibility == Visibility.Visible && xApplicationMode.IsEnabled)
				{
					base.Dispatcher.BeginInvoke((Action)delegate
					{
						UpdateApplicationMode();
					});
				}
				OpenPresetOffsetHint();
			}
		}

		private void InitUILayout()
		{
			if (Instance == null || bInitedLayout)
			{
				return;
			}
			bInitedLayout = true;
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 50:
			case 56:
				xHWFileList.Items.Clear();
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Gain",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Gain)
				});
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Defect",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Defect)
				});
				xUpdateHWPreoffsetBtn.Visibility = Visibility.Visible;
				xUpdateHWFreqCoeffBtn.Visibility = Visibility.Visible;
				if (49 == Instance.Prop_Attr_UROM_ProductNo || 50 == Instance.Prop_Attr_UROM_ProductNo)
				{
					xReverseVCOM.Visibility = Visibility.Visible;
				}
				break;
			case 68:
			case 73:
			case 74:
				xMgrCorrFilePanel.Visibility = Visibility.Collapsed;
				xReverseVCOM.Visibility = Visibility.Visible;
				break;
			case 46:
			case 87:
				xMgrCorrFilePanel.Visibility = Visibility.Collapsed;
				break;
			case 32:
			case 37:
			case 39:
			case 59:
			case 62:
				xHWFileList.Items.Clear();
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Gain",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Gain)
				});
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Defect",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Defect)
				});
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Lag",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Lag)
				});
				xApplicationMode.Visibility = Visibility.Collapsed;
				xUpdateHWPreoffsetBtn.Visibility = Visibility.Visible;
				break;
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 60:
			case 61:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				xHWFileList.Items.Clear();
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Gain",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Gain)
				});
				xHWFileList.Items.Add(new TabItem
				{
					Header = "Defect",
					Content = new HWFileList(Instance.Prop_Attr_UROM_ProductNo, Enm_FileTypes.Enm_File_Defect)
				});
				xApplicationMode.Visibility = Visibility.Collapsed;
				xUpdateHWPreoffsetBtn.Visibility = Visibility.Visible;
				break;
			default:
				xTemplatePanel.IsEnabled = false;
				break;
			}
		}

		public void InitImageParam()
		{
			Instance.SubscribeCBEvent(OnSdkCallback);
			SdkInvokeProxy = new SDKInvokeProxy(Instance, new Shell(Application.Current.MainWindow));
			InitApplicationModeStatus();
			InitUILayout();
		}

		private bool IsSelectModeEqualCurrentMode(ApplicationMode selMode)
		{
			bool flag = false;
			int prop_Attr_UROM_SequenceIntervalTime = Instance.Prop_Attr_UROM_SequenceIntervalTime;
			flag = (selMode.PGA == Instance.Prop_Attr_UROM_PGA && selMode.Zoom == Instance.Prop_Attr_UROM_ZoomMode && selMode.Binning == Instance.Prop_Attr_UROM_BinningMode && 0.0 != selMode.Freq && ((int)(1000.0 / selMode.Freq) == prop_Attr_UROM_SequenceIntervalTime || ((1000 / prop_Attr_UROM_SequenceIntervalTime == (int)selMode.Freq) ? true : false)));
			if (flag)
			{
				Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
				if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0406X || prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0900X)
				{
					string text = selMode.ROIRange.Replace(" ", "");
					string value = string.Format("({0},{1},{2},{3})", Instance.Prop_Attr_UROM_ROIColStartPos, Instance.Prop_Attr_UROM_ROIRowStartPos, Instance.Prop_Attr_UROM_ROIColEndPos, Instance.Prop_Attr_UROM_ROIRowEndPos);
					if (!text.Equals(value))
					{
						flag = false;
					}
				}
				else
				{
					if (selMode.ExistedROIRange)
					{
						string text2 = selMode.ROIRange.Replace(" ", "");
						string value2 = string.Format("({0},{1},{2},{3})", Instance.Prop_Attr_UROM_ROIColStartPos, Instance.Prop_Attr_UROM_ROIRowStartPos, Instance.Prop_Attr_UROM_ROIColEndPos, Instance.Prop_Attr_UROM_ROIRowEndPos);
						if (!text2.Equals(value2))
						{
							flag = false;
						}
					}
					if (selMode.ExistedExpMode && !selMode.ExpMode.Equals(((Enm_ExpMode)Instance.Prop_Attr_UROM_ExpMode).ToString().Replace("Enm_ExpMode_", "")))
					{
						flag = false;
					}
				}
			}
			return flag;
		}

		private void InitApplicationModeStatus()
		{
			bool flag = false;
			ModeListArr modeListArr = new ModeListArr();
			ModeCombox.ItemsSource = modeListArr;
			ModeCombox.DisplayMemberPath = "Disp";
			ModeCombox.SelectedValuePath = "Mode";
			List<ApplicationMode> list = ApplicationMode.ParseIni(Instance.Prop_Attr_WorkDir);
			if (list.Count > 0)
			{
				if (list[0].NoZoom)
				{
					xModeListView.Columns.Remove(xZoomHeader);
				}
				if (!list[0].ExistedExpMode)
				{
					xModeListView.Columns.Remove(xExpMode);
				}
				if (!list[0].ExistedROIRange)
				{
					xModeListView.Columns.Remove(xROIHeader);
				}
			}
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0406X || prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0900X)
			{
				xModeListView.Columns.Remove(xPGAHeader);
				xModeListView.Columns.Remove(xZoomHeader);
			}
			else
			{
				xModeListView.Columns.Remove(xFullWellHeader);
			}
			for (int i = 0; i < list.Count; i++)
			{
				modeListArr.AddItem(list[i], (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo);
				if (!flag && IsSelectModeEqualCurrentMode(list[i]))
				{
					flag = true;
					mCurSelMode = modeListArr[i];
				}
			}
			if (flag)
			{
				if (SdkInvokeProxy.Invoke(7, mCurSelMode.Mode.Subset) == 0)
				{
					SwitchApplicationMode(mCurSelMode);
				}
				ModeCombox.SelectedItem = mCurSelMode;
				_ModeList.Items.Clear();
				_ModeList.Items.Add(mCurSelMode.Mode);
			}
		}

		private void OnSelectApplicationMode(object sender, SelectionChangedEventArgs e)
		{
			ComboBox comboBox = sender as ComboBox;
			ModeList modeList = comboBox.SelectedItem as ModeList;
			if (modeList != null && modeList.Mode.Freq != 0.0 && mCurSelMode != modeList)
			{
				if (modeList.Mode.Freq <= 0.0)
				{
					MessageBoxShow("Frequency cannot be 0!");
					ModeCombox.SelectedItem = mCurSelMode;
				}
				else
				{
					SwitchCaliSubSet((ModeCombox.SelectedItem as ModeList).Mode.Subset);
				}
			}
		}

		private void SwitchApplicationMode(ModeList curMode)
		{
			if (curMode == null)
			{
				ModeCombox.SelectedItem = null;
				return;
			}
			ModeCombox.SelectedItem = curMode;
			_ModeList.Items.Clear();
			_ModeList.Items.Add(curMode.Mode);
			mCurSelMode = curMode;
		}

		private void SwitchCaliSubSet(string subDir)
		{
			int num = SdkInvokeProxy.Invoke(7, subDir);
			if (num == 0)
			{
				UpdateApplicationModeEx();
			}
			else if (1 == num)
			{
				ShowMessageBox("Switch Progress", "Switching Application mode...");
			}
		}

		private void UpdateApplicationModeEx()
		{
			CloseMessageBox();
			SwitchApplicationMode(ModeCombox.SelectedItem as ModeList);
			UpdateTemplateFileValidityState();
			if (this.UpdateDetPageParams != null)
			{
				this.UpdateDetPageParams();
			}
		}

		private void UpdateApplicationMode()
		{
			ModeList modeList = ModeCombox.SelectedItem as ModeList;
			ApplicationMode applicationMode = null;
			if (modeList != null)
			{
				applicationMode = modeList.Mode;
				if (IsSelectModeEqualCurrentMode(applicationMode))
				{
					return;
				}
			}
			for (int i = 0; i < ModeCombox.Items.Count; i++)
			{
				ModeList modeList2 = ModeCombox.Items[i] as ModeList;
				applicationMode = modeList2.Mode;
				if (IsSelectModeEqualCurrentMode(applicationMode))
				{
					ModeCombox.SelectedIndex = i;
					SwitchApplicationMode(modeList2);
					return;
				}
			}
			if (-1 != ModeCombox.SelectedIndex || mCurSelMode != null)
			{
				ModeCombox.SelectedIndex = -1;
				_ModeList.Items.Clear();
				mCurSelMode = null;
				SwitchCaliSubSet("Default");
			}
		}

		private bool CanCreateTemplate(Enm_CalibrateType type)
		{
			if (Instance == null)
			{
				MessageBoxShow("Not initilized!");
				return false;
			}
			bool result = true;
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 68:
			case 73:
			case 74:
			case 87:
				if (-1 == ModeCombox.SelectedIndex)
				{
					MessageBoxShow("Please select a Application mode");
					result = false;
					break;
				}
				switch (type)
				{
				case Enm_CalibrateType.Enm_Offset:
					if (Instance.Prop_Attr_OffsetTotalFrames == 0)
					{
						MessageBoxShow("Frame number cannot be 0, Please reset the value in configure file!");
						result = false;
					}
					break;
				case Enm_CalibrateType.Enm_GainDefect:
					if (Instance.Prop_Attr_GainTotalFrames == 0)
					{
						MessageBoxShow("Frame number cannot be 0, Please reset the value in configure file!");
						result = false;
					}
					break;
				}
				break;
			case 11:
			{
				int prop_Attr_UROM_ExpMode = Instance.Prop_Attr_UROM_ExpMode;
				if (prop_Attr_UROM_ExpMode == 1 || prop_Attr_UROM_ExpMode == 2)
				{
					MessageBoxShow("Correction template cann't be created in the Manual/AEC exposure mode!");
					result = false;
				}
				break;
			}
			}
			return result;
		}

		private void OnDownLoadFPDCorrFile(object sender, RoutedEventArgs e)
		{
			UpdateCorrFileWnd updateCorrFileWnd = new UpdateCorrFileWnd();
			updateCorrFileWnd.SubCaliPath = Instance.Prop_Attr_WorkDir + TemplatePath;
			updateCorrFileWnd.ShowDialog();
			if (updateCorrFileWnd.DiagResult)
			{
				int num = SdkInvokeProxy.Invoke(3017, updateCorrFileWnd.FileType, updateCorrFileWnd.FileIndex, updateCorrFileWnd.FilePath, updateCorrFileWnd.DespInfo);
				if (1 == num || num == 0)
				{
					DownloadWnd = new DownloadCorrProgressWnd(Instance, "Download File Progress");
					DownloadWnd.ShowDialog();
					Instance.AttrChangingMonitor.AddRef(5016);
				}
			}
		}

		private void OnUpLoadFPDCorrFile(object sender, RoutedEventArgs e)
		{
			HWFileList hWFileList = (xHWFileList.SelectedItem as TabItem).Content as HWFileList;
			int num = 0;
			string arg = "";
			string arg2 = "";
			if (hWFileList.FileType == Enm_FileTypes.Enm_File_Lag)
			{
				num = 1;
			}
			else
			{
				if (hWFileList.FileList == null)
				{
					MessageBoxShow("Please select a item!");
					return;
				}
				num = Convert.ToInt32(hWFileList.FileList.Index);
				arg = hWFileList.FileList.Binning;
				arg2 = hWFileList.FileList.Zoom;
			}
			UpdateCorrFileWnd updateCorrFileWnd = new UpdateCorrFileWnd(UpdateCorrFileWnd.Enm_WndType.UpLoad, num);
			updateCorrFileWnd.SubCaliPath = Instance.Prop_Attr_WorkDir + TemplatePath;
			updateCorrFileWnd.ConfigFileName = Instance.Prop_Attr_WorkDir + "\\iDetectorConfig.ini";
			if (48 == Instance.Prop_Attr_UROM_ProductNo)
			{
				updateCorrFileWnd.FileSizeInfo = string.Format("Binning{0}Zoom{1}", arg, arg2);
			}
			else
			{
				updateCorrFileWnd.FileSizeInfo = string.Format("Binning{0}", arg);
			}
			updateCorrFileWnd.FileType = (int)hWFileList.FileType;
			updateCorrFileWnd.ShowDialog();
			if (updateCorrFileWnd.DiagResult)
			{
				int num2 = SdkInvokeProxy.Invoke(3018, updateCorrFileWnd.FileType, updateCorrFileWnd.FileIndex, updateCorrFileWnd.FilePath);
				if (1 == num2)
				{
				}
			}
		}

		private void OnSelectFPDCorrFile(object sender, RoutedEventArgs e)
		{
			if (xHWFileList.Items.Count == 0)
			{
				return;
			}
			Enm_FileTypes[] array = new Enm_FileTypes[3]
			{
				Enm_FileTypes.Enm_File_Gain,
				Enm_FileTypes.Enm_File_Defect,
				Enm_FileTypes.Enm_File_Lag
			};
			FPDCorrFileList fileList = ((xHWFileList.SelectedItem as TabItem).Content as HWFileList).FileList;
			if (fileList == null)
			{
				MessageBoxShow("Please select a item!");
				return;
			}
			int num = SdkInvokeProxy.Invoke(3019, (int)array[xHWFileList.SelectedIndex], Convert.ToInt32(fileList.Index));
			if (1 == num)
			{
			}
		}

		private void OnReadFPDCorrFile(object sender, RoutedEventArgs e)
		{
			UpdateFPDCorrFileStatus();
		}

		private void OnUpdateHWPreoffset(object sender, RoutedEventArgs e)
		{
			int num = SdkInvokeProxy.Invoke(3020);
			if (1 == num)
			{
				EnableView(false);
			}
		}

		private void OnUpdateHWFreqCoeff(object sender, RoutedEventArgs e)
		{
			int num = SdkInvokeProxy.Invoke(3032);
			if (1 == num)
			{
				EnableView(false);
			}
		}

		private void OnReverseVCOM(object sender, RoutedEventArgs e)
		{
			int num = SdkInvokeProxy.Invoke(3034);
			if (1 == num)
			{
				EnableView(false);
			}
		}

		private void OpenPresetOffsetHint()
		{
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
			case 87:
				xSWPreOffsetRB.IsChecked = false;
				xSWPostOffsetRB.IsChecked = false;
				return;
			}
			if ((Instance.Prop_Attr_CurrentCorrectOption & 0x30003) == 0)
			{
				Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
				if (Enm_ProdType.Enm_Prd_Mars1417XF == prop_Attr_UROM_ProductNo || Enm_ProdType.Enm_Prd_Mars1717XF == prop_Attr_UROM_ProductNo)
				{
					xPresetOffset.Visibility = Visibility.Visible;
					xSWPostOffsetRB.Visibility = Visibility.Visible;
					xSWPostOffsetRB.IsChecked = true;
					if (1 == Instance.Prop_Attr_UROM_TriggerMode || (3 == Instance.Prop_Attr_UROM_TriggerMode && 1 == Instance.Prop_Attr_UROM_PrepCapMode))
					{
						xSWPreOffsetRB.Visibility = Visibility.Collapsed;
					}
					else
					{
						xSWPreOffsetRB.Visibility = Visibility.Visible;
					}
					return;
				}
				if (1 == Instance.Prop_Attr_UROM_PrepCapMode)
				{
					xPresetOffset.Visibility = Visibility.Visible;
					xSWPostOffsetRB.Visibility = Visibility.Collapsed;
					xSWPreOffsetRB.Visibility = Visibility.Visible;
					xSWPreOffsetRB.IsChecked = true;
					return;
				}
				xSWPostOffsetRB.Visibility = Visibility.Visible;
				xSWPreOffsetRB.Visibility = Visibility.Visible;
				xSWPreOffsetRB.IsChecked = true;
				xPresetOffset.Visibility = Visibility.Visible;
				switch (prop_Attr_UROM_ProductNo)
				{
				case Enm_ProdType.Enm_Prd_Mammo1012F:
					xSWPreOffsetRB.Visibility = Visibility.Collapsed;
					xSWPostOffsetRB.IsChecked = true;
					break;
				case Enm_ProdType.Enm_Prd_Mars1417V:
				case Enm_ProdType.Enm_Prd_Mars1717V:
					xSWPreOffsetRB.Visibility = Visibility.Visible;
					xSWPreOffsetRB.IsChecked = true;
					break;
				case Enm_ProdType.Enm_Prd_Mars1012X:
				case Enm_ProdType.Enm_Prd_Mars1417X:
				case Enm_ProdType.Enm_Prd_Mars1417V2:
				case Enm_ProdType.Enm_Prd_Venu1012V:
				case Enm_ProdType.Enm_Prd_Mars1012VN:
				case Enm_ProdType.Enm_Prd_Mars1717V2:
				case Enm_ProdType.Enm_Prd_Venu1717X:
				case Enm_ProdType.Enm_Prd_Venu1717MX:
				case Enm_ProdType.Enm_Prd_Venu1012VD:
				case Enm_ProdType.Enm_Prd_Venu1717XV:
				case Enm_ProdType.Enm_Prd_Luna1417XM:
					if (5 == Instance.Prop_Attr_UROM_TriggerMode && 3 == Instance.Prop_Attr_UROM_FreesyncSubFlow)
					{
						xSWPreOffsetRB.Visibility = Visibility.Collapsed;
						xSWPostOffsetRB.IsChecked = true;
					}
					else
					{
						xSWPreOffsetRB.Visibility = Visibility.Visible;
						xSWPreOffsetRB.IsChecked = true;
					}
					break;
				}
			}
			else
			{
				xSWPreOffsetRB.IsChecked = false;
				xSWPostOffsetRB.IsChecked = false;
				xPresetOffset.Visibility = Visibility.Collapsed;
			}
		}

		private void ClosePresetoffsetHint()
		{
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
			case 87:
				return;
			}
			if ((Instance.Prop_Attr_CurrentCorrectOption & 0x30003) != 0)
			{
				xPresetOffset.Visibility = Visibility.Collapsed;
			}
		}

		private void OnCreateTemplate(object sender, RoutedEventArgs e)
		{
			if (CanCreateTemplate(Enm_CalibrateType.Enm_Offset))
			{
				PresetOffsetOption offsetOption = PresetOffsetOption.None;
				if (xSWPreOffsetRB.IsChecked == true)
				{
					offsetOption = PresetOffsetOption.SWPreOffset;
				}
				else if (xSWPostOffsetRB.IsChecked == true)
				{
					offsetOption = PresetOffsetOption.SWPostOffset;
				}
				using (CreateTemplateWnd createTemplateWnd = new CreateTemplateWnd(Instance, offsetOption))
				{
					createTemplateWnd.Owner = Application.Current.MainWindow;
					try
					{
						createTemplateWnd.ShowDialog();
					}
					catch (Exception ex)
					{
						Log.Instance().Write("CreateTemplateWnd failed:{0}", ex.Message);
					}
				}
				GC.Collect();
				UpdateTemplateFileValidityState();
				ClosePresetoffsetHint();
			}
		}

		public void ParseTemplateList(string xmlInfo)
		{
			InitUILayout();
			if (xmlInfo == null)
			{
				return;
			}
			XmlParser xmlParser = new XmlParser();
			xmlParser.LoadXml(xmlInfo);
			List<string> nodes = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Index");
			if (nodes == null || nodes.Count == 0)
			{
				MessageNotify("ParseTemplateList: HWFile list is null");
				return;
			}
			string singleNoteValue = xmlParser.GetSingleNoteValue("HWCalibrationFileList/FileItem/Type");
			List<string> nodes2 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/PGA");
			List<string> nodes3 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Binning");
			List<string> nodes4 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Zoom");
			List<string> nodes5 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Activity");
			List<string> nodes6 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Validity");
			List<string> nodes7 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/DespInfo");
			Enm_FileTypes enm_FileTypes = (Enm_FileTypes)Convert.ToInt32(singleNoteValue);
			ItemCollection itemCollection = null;
			foreach (TabItem item in (IEnumerable)xHWFileList.Items)
			{
				HWFileList hWFileList = item.Content as HWFileList;
				if (hWFileList != null && hWFileList.FileType == enm_FileTypes)
				{
					itemCollection = hWFileList.ItemCollection;
					break;
				}
			}
			if (itemCollection == null)
			{
				MessageNotify("ParseTemplateList: Cannot find the right file type");
				return;
			}
			itemCollection.Clear();
			for (int i = 0; i < nodes.Count; i++)
			{
				FPDCorrFileList fPDCorrFileList = new FPDCorrFileList();
				if (i < nodes.Count)
				{
					fPDCorrFileList.Index = nodes[i];
				}
				if (i < nodes2.Count)
				{
					fPDCorrFileList.PGA = nodes2[i];
				}
				if (i < nodes3.Count)
				{
					fPDCorrFileList.Binning = nodes3[i];
				}
				if (i < nodes4.Count)
				{
					fPDCorrFileList.Zoom = nodes4[i];
				}
				if (i < nodes5.Count)
				{
					fPDCorrFileList.Activity = nodes5[i];
				}
				if (i < nodes6.Count)
				{
					fPDCorrFileList.Validity = nodes6[i];
				}
				if (i < nodes7.Count)
				{
					fPDCorrFileList.DespInfo = nodes7[i];
				}
				itemCollection.Add(fPDCorrFileList);
			}
		}

		private void CheckHWTemplateStatus()
		{
			bool flag = false;
			bool flag2 = false;
			foreach (TabItem item in (IEnumerable)xHWFileList.Items)
			{
				HWFileList hWFileList = item.Content as HWFileList;
				foreach (FPDCorrFileList item2 in (IEnumerable)hWFileList.ItemCollection)
				{
					if ("enable" == item2.Activity)
					{
						int num = Convert.ToInt32(item2.PGA);
						int num2 = Convert.ToInt32(item2.Binning);
						if (num != Instance.Prop_Attr_UROM_PGA_W || num2 != Instance.Prop_Attr_UROM_BinningMode_W)
						{
							if (Enm_FileTypes.Enm_File_Gain == hWFileList.FileType)
							{
								flag = true;
							}
							else if (Enm_FileTypes.Enm_File_Defect == hWFileList.FileType)
							{
								flag2 = true;
							}
						}
					}
				}
			}
			if (flag && flag2)
			{
				ShowToolTip("Selected HWGain & HWDefect template doesn't match with current parameter!");
			}
			else if (flag)
			{
				ShowToolTip("Selected HWGain template doesn't match with current parameter!");
			}
			else if (flag2)
			{
				ShowToolTip("Selected HWDefect template doesn't match with current parameter!");
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			object obj = null;
			if (nEventID == 12)
			{
				string text = Marshal.PtrToStringAuto(pParam);
				obj = text;
			}
			base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, obj);
		}

		private bool IsPopWindow()
		{
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
			case 87:
				return true;
			default:
				return true;
			}
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			bHandled = true;
			switch (nEventID)
			{
			case 1:
				bHandled = false;
				break;
			case 2006:
				UpdateTemplateFileValidityState();
				if (ShowToolTip != null)
				{
					if (nParam2 == 0)
					{
						ShowToolTip(sender.Prop_Attr_UROM_SerialNo + ": " + fileTypeName[nParam1] + " has expired!");
					}
					else
					{
						ShowToolTip(sender.Prop_Attr_UROM_SerialNo + ": " + fileTypeName[nParam1] + " will expire in " + nParam2 + " minute");
					}
				}
				break;
			case 4:
				switch (nParam1)
				{
				case 7:
					EnableView(true);
					UpdateApplicationModeEx();
					CheckHWTemplateStatus();
					break;
				case 2001:
					EnableView(true);
					UpdateApplicationModeEx();
					break;
				case 3017:
					if (DownloadWnd != null)
					{
						DownloadWnd.OnClose();
					}
					MessageBoxShow("Download succeed! Recommend Read Status.");
					break;
				case 3019:
					DelayUpdateFPDCorrFileStatus();
					break;
				default:
					EnableView(true);
					break;
				case 2002:
				case 3018:
				case 3021:
					break;
				}
				Instance.TASK = "No Task";
				break;
			case 5:
				switch (nParam1)
				{
				case 7:
					CloseMessageBox();
					EnableView(true);
					SwitchApplicationMode(mCurSelMode);
					break;
				case 2001:
				case 2002:
					CloseMessageBox();
					EnableView(true);
					SwitchApplicationMode(mCurSelMode);
					break;
				case 3017:
					if (DownloadWnd != null)
					{
						DownloadWnd.OnClose();
					}
					MessageBoxShow("Download failed! Err = " + ErrorHelper.GetErrorDesp(nParam2));
					break;
				case 3018:
					MessageBoxShow("Upload failed! Err = " + ErrorHelper.GetErrorDesp(nParam2));
					break;
				case 3021:
					MessageBoxShow("Query Template FileList failed! Err = " + ErrorHelper.GetErrorDesp(nParam2));
					break;
				case 3019:
					MessageBoxShow("Select failed! Err = " + ErrorHelper.GetErrorDesp(nParam2));
					break;
				default:
					EnableView(true);
					break;
				case 2:
					break;
				}
				Instance.TASK = "No Task";
				break;
			case 12:
				ParseTemplateList(pParam as string);
				break;
			}
		}

		private void DelayUpdateFPDCorrFileStatus()
		{
			new UIDelayTimer(100, delegate
			{
				UpdateFPDCorrFileStatus();
			}).Start();
		}

		private void UpdateFPDCorrFileStatus()
		{
			if (xHWFileList.Items.Count != 0)
			{
				Enm_FileTypes[] array = new Enm_FileTypes[3]
				{
					Enm_FileTypes.Enm_File_Gain,
					Enm_FileTypes.Enm_File_Defect,
					Enm_FileTypes.Enm_File_Lag
				};
				int num = SdkInvokeProxy.Invoke(3021, (int)array[xHWFileList.SelectedIndex]);
				if (1 == num)
				{
				}
			}
		}

		public void UpdateTemplateFileValidityState()
		{
			ModeList modeList = ModeCombox.SelectedItem as ModeList;
			if (modeList != null)
			{
				int prop_Attr_OffsetValidityState = instance.Prop_Attr_OffsetValidityState;
				int prop_Attr_GainValidityState = instance.Prop_Attr_GainValidityState;
				int prop_Attr_DefectValidityState = instance.Prop_Attr_DefectValidityState;
				modeList.Mode.OffsetValidity = templateStateInfo[prop_Attr_OffsetValidityState];
				modeList.Mode.GainValidity = templateStateInfo[prop_Attr_GainValidityState];
				modeList.Mode.DefectValidity = templateStateInfo[prop_Attr_DefectValidityState];
			}
		}

		private void MessageNotify(string info)
		{
			if (this.MessageNotifyProxy != null)
			{
				this.MessageNotifyProxy(info);
			}
		}

		private void EnableView(bool bEnable)
		{
			base.IsEnabled = bEnable;
			mbCanExit = bEnable;
		}

		private void MessageBoxShow(string content)
		{
			MessageBox.Show(Application.Current.MainWindow, content);
		}

		protected void ShowMessageBox(string title, string content)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TaskTimeoutSeconds, null, CurTaskTimeOut);
			MessageBoxTimer.RunTask();
		}

		private void CurTaskTimeOut()
		{
			MessageBoxShow("Current task Timeout !");
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		public void NotifyReconnected()
		{
			EnableView(true);
		}

		public void OnClose()
		{
			Instance.UnSubscribeCBEvent(OnSdkCallback);
		}

		public bool Exit()
		{
			if (!mbCanExit && base.Visibility != 0)
			{
				MessageBoxShow("Please finish or abort current task!");
			}
			return mbCanExit;
		}

		public bool Enter()
		{
			return true;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/calibratepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xTemplateColumn = (ColumnDefinition)target;
				break;
			case 2:
				xSplitter = (ColumnDefinition)target;
				break;
			case 3:
				xTemplatePanel = (StackPanel)target;
				break;
			case 4:
				xApplicationMode = (Grid)target;
				break;
			case 5:
				ModeCombox = (ComboBox)target;
				ModeCombox.SelectionChanged += OnSelectApplicationMode;
				break;
			case 6:
				_ModeList = (ListView)target;
				break;
			case 7:
				xModeListView = (GridView)target;
				break;
			case 8:
				xPGAHeader = (GridViewColumn)target;
				break;
			case 9:
				xFullWellHeader = (GridViewColumn)target;
				break;
			case 10:
				xZoomHeader = (GridViewColumn)target;
				break;
			case 11:
				xROIHeader = (GridViewColumn)target;
				break;
			case 12:
				xExpMode = (GridViewColumn)target;
				break;
			case 13:
				xMgrCorrFilePanel = (GroupBox)target;
				break;
			case 14:
				((Button)target).Click += OnDownLoadFPDCorrFile;
				break;
			case 15:
				((Button)target).Click += OnUpLoadFPDCorrFile;
				break;
			case 16:
				((Button)target).Click += OnSelectFPDCorrFile;
				break;
			case 17:
				((Button)target).Click += OnReadFPDCorrFile;
				break;
			case 18:
				xHWFileList = (TabControl)target;
				break;
			case 19:
				xUpdateHWPreoffsetBtn = (Button)target;
				xUpdateHWPreoffsetBtn.Click += OnUpdateHWPreoffset;
				break;
			case 20:
				xUpdateHWFreqCoeffBtn = (Button)target;
				xUpdateHWFreqCoeffBtn.Click += OnUpdateHWFreqCoeff;
				break;
			case 21:
				xReverseVCOM = (Button)target;
				xReverseVCOM.Click += OnReverseVCOM;
				break;
			case 22:
				xPopWindow = (Grid)target;
				break;
			case 23:
				xPresetOffset = (GroupBox)target;
				break;
			case 24:
				xSWPreOffsetRB = (RadioButton)target;
				break;
			case 25:
				xSWPostOffsetRB = (RadioButton)target;
				break;
			case 26:
				((Button)target).Click += OnCreateTemplate;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
