using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Threading;

namespace iDetector
{
	public class FirmwareUpgradeExWnd : Window, IComponentConnector
	{
		private Detector _det;

		private FrmFileParser _frmFile;

		private DispatcherTimer timerUpdate;

		private int timeUpdateDelay = 1000;

		private int nUpdateCount;

		private int nUpdateCur = -1;

		private List<stUpdateFileInfo> updateInfoList;

		private DateTime StartTime;

		private bool bIsShowWindow;

		private string strProductName = "";

		private bool bExitUpgrade;

		private bool bIsUpgradeFinish;

		internal FirmwareUpgradeExWnd FirmwareUpgradeExWnd1;

		internal TextBox txtFullPath;

		internal Button btnBrowse;

		internal ProgressBar updateProgressBar;

		internal Button btnStartUpgrade;

		internal TextBox txtPackDesc;

		internal Label txtProgressBar;

		internal TextBox txtCurVersionInfo;

		private bool _contentLoaded;

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
			}
		}

		public FirmwareUpgradeExWnd(Detector det)
		{
			InitializeComponent();
			Init(det);
		}

		private void Init(Detector det)
		{
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				_det = det;
				if (_frmFile == null)
				{
					_frmFile = new FrmFileParser();
				}
				updateInfoList = new List<stUpdateFileInfo>();
				btnStartUpgrade.IsEnabled = false;
				string text = string.Format("MainFPGA: {0}\r\nFPGARead1: {1}\r\nMCU: {2}\r\nARM: {3}\r\nKernel: {4}", _det.Prop_Attr_UROM_MainVersion.ToString(), _det.Prop_Attr_UROM_ReadVersion.ToString(), _det.Prop_Attr_UROM_McuVersion.ToString(), _det.Prop_Attr_UROM_ArmVersion.ToString(), _det.Prop_Attr_UROM_KernelVersion.ToString());
				txtCurVersionInfo.Text = text;
			}
		}

		private void FirmwareUpgradeExWnd1_Closing(object sender, CancelEventArgs e)
		{
			if (bIsShowWindow)
			{
				e.Cancel = true;
			}
		}

		private void TimerUpdate_Tick(object sender, object e)
		{
			if ((DateTime.Now - StartTime).TotalSeconds > (double)_det.Prop_Cfg_FWUpdTimeOut)
			{
				timerUpdate.Stop();
				MessageBox.Show("Upgrade progress time out! Total time: " + _det.Prop_Cfg_FWUpdTimeOut + " seconds", "iDetector");
				Unload();
			}
			else
			{
				updateProgressBar.Value = _det.Prop_Attr_FWUpdateProgress;
				txtProgressBar.Content = _det.Prop_Attr_FWUpdateProgress.ToString() + "%";
			}
		}

		private void btnBrowse_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Firmware setup files(*.ifrm)|*.ifrm";
			if (openFileDialog.ShowDialog() != true)
			{
				return;
			}
			txtFullPath.Text = openFileDialog.FileName;
			btnBrowse.IsEnabled = false;
			if (!_frmFile.Load(txtFullPath.Text))
			{
				btnBrowse.IsEnabled = true;
				MessageBox.Show("Upgrade file error!");
				return;
			}
			int num = _frmFile.Header.arrProducts[0];
			int num2 = _frmFile.Header.arrProducts[1];
			int num3 = _frmFile.Header.arrProducts[2];
			int num4 = _frmFile.Header.arrProducts[3];
			int num5 = _frmFile.Header.arrProducts[4];
			int num6 = _frmFile.Header.arrProducts[5];
			int num7 = _frmFile.Header.arrProducts[6];
			int num8 = _frmFile.Header.arrProducts[7];
			int prop_Attr_UROM_ProductNo = _det.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo != num && prop_Attr_UROM_ProductNo != num2 && prop_Attr_UROM_ProductNo != num3 && prop_Attr_UROM_ProductNo != num4 && prop_Attr_UROM_ProductNo != num5 && prop_Attr_UROM_ProductNo != num6 && prop_Attr_UROM_ProductNo != num7 && prop_Attr_UROM_ProductNo != num8)
			{
				btnBrowse.IsEnabled = true;
				MessageBox.Show("Product is different, so can not upgrade!");
				return;
			}
			int ucVerByte = _frmFile.Header.ucVerByte0;
			int ucVerByte2 = _frmFile.Header.ucVerByte1;
			int ucVerByte3 = _frmFile.Header.ucVerByte2;
			int ucVerByte4 = _frmFile.Header.ucVerByte3;
			string arg = string.Format("Package Version: {0}.{1}.{2}.{3}\r\n", ucVerByte, ucVerByte2, ucVerByte3, ucVerByte4);
			string text = "";
			if (num > 0)
			{
				text = string.Format("{0}", GetProductName(num));
			}
			if (num2 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num2));
			}
			if (num3 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num3));
			}
			if (num4 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num4));
			}
			if (num5 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num5));
			}
			if (num6 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num6));
			}
			if (num7 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num7));
			}
			if (num8 > 0)
			{
				text = string.Format("{0},{1}", text, GetProductName(num8));
			}
			arg = string.Format("{0}Product: {1}\r\n", arg, text);
			string strDescription = _frmFile.Header.strDescription;
			arg = string.Format("{0}Description: {1}\r\n", arg, strDescription);
			FirmwarePackSubFileInfo[] subFileInfo = _frmFile.SubFileInfo;
			int num9 = subFileInfo.Length;
			updateInfoList.Clear();
			nUpdateCount = 0;
			nUpdateCur = -1;
			bIsUpgradeFinish = false;
			for (int i = 0; i < num9; i++)
			{
				if (subFileInfo[i].ucDataType != 0 || subFileInfo[i].ucDeviceType != byte.MaxValue)
				{
					nUpdateCount++;
					int ucVerByte5 = subFileInfo[i].ucVerByte0;
					int ucVerByte6 = subFileInfo[i].ucVerByte1;
					int ucVerByte7 = subFileInfo[i].ucVerByte2;
					int ucVerByte8 = subFileInfo[i].ucVerByte3;
					string text2 = string.Format("{0}.{1}.{2}.{3}", ucVerByte5, ucVerByte6, ucVerByte7, ucVerByte8);
					string strDescription2 = subFileInfo[i].strDescription;
					string text3 = "";
					Enm_FW_DeviceType ucDeviceType = (Enm_FW_DeviceType)subFileInfo[i].ucDeviceType;
					switch (ucDeviceType)
					{
					case Enm_FW_DeviceType.Enm_FW_DeviceType_MainFPGA:
						text3 = "MainFPGA";
						break;
					case Enm_FW_DeviceType.Enm_FW_DeviceType_ReadFPGA1:
						text3 = "FPGARead1";
						break;
					case Enm_FW_DeviceType.Enm_FW_DeviceType_ReadFPGA2:
						text3 = "FPGARead2";
						break;
					case Enm_FW_DeviceType.Enm_FW_DeviceType_ControlBox:
						text3 = "ControlBox";
						break;
					case Enm_FW_DeviceType.Enm_FW_DeviceType_MCU1:
						text3 = "MCU1";
						break;
					case Enm_FW_DeviceType.Enm_FW_DeviceType_AllInOne:
						text3 = "AllInOne";
						break;
					default:
						text3 = "Name";
						break;
					}
					arg = ((i == 0) ? ((ucDeviceType != Enm_FW_DeviceType.Enm_FW_DeviceType_AllInOne) ? string.Format("{0}{1}.DeviceType: {2}\r\nVersion: {3}\r\nSourceFile: {4}", arg, nUpdateCount, text3, text2, strDescription2) : string.Format("{0}{1}.DeviceType: {2}\r\nSourceFile: {3}", arg, nUpdateCount, text3, strDescription2)) : ((ucDeviceType != Enm_FW_DeviceType.Enm_FW_DeviceType_AllInOne) ? string.Format("{0}\r\n{1}.DeviceType: {2}\r\nVersion: {3}\r\nSourceFile: {4}", arg, nUpdateCount, text3, text2, strDescription2) : string.Format("{0}\r\n{1}.DeviceType: {2}\r\nSourceFile: {3}", arg, nUpdateCount, text3, strDescription2)));
					stUpdateFileInfo item = default(stUpdateFileInfo);
					item.nCur = nUpdateCount;
					item.nDeviceType = subFileInfo[i].ucDeviceType;
					item.strVersion = text2;
					item.strDesc = strDescription2;
					updateInfoList.Add(item);
				}
			}
			txtPackDesc.Text = arg;
			nUpdateCur = -1;
			btnStartUpgrade.IsEnabled = true;
			btnBrowse.IsEnabled = true;
			updateProgressBar.Value = 0.0;
			txtProgressBar.Content = "";
		}

		private void btnStartUpgrade_Click(object sender, RoutedEventArgs e)
		{
			nUpdateCur = -1;
			bExitUpgrade = false;
			bIsUpgradeFinish = false;
			int num = StartInvokeFirmware();
			if (num < 0)
			{
				Unload();
			}
			else
			{
				StartTimeTick(true);
			}
		}

		private int StartInvokeFirmware()
		{
			if (bExitUpgrade)
			{
				return -1;
			}
			updateProgressBar.Value = 0.0;
			txtProgressBar.Content = "";
			nUpdateCur++;
			if (nUpdateCur == nUpdateCount)
			{
				bExitUpgrade = true;
				return -2;
			}
			switch (updateInfoList[nUpdateCur].nDeviceType)
			{
			case 1:
				if (updateInfoList[nUpdateCur].strVersion == _det.Prop_Attr_UROM_MainVersion.ToString() && MessageBoxResult.No == MessageBox.Show("The Main versions are the same, whether to continue to upgrade?", "Upgrade Firmware", MessageBoxButton.YesNo) && StartInvokeFirmware() == 0)
				{
					return 0;
				}
				break;
			case 2:
				if (updateInfoList[nUpdateCur].strVersion == _det.Prop_Attr_UROM_ReadVersion.ToString() && MessageBoxResult.No == MessageBox.Show("The Read1 versions are the same, whether to continue to upgrade?", "Upgrade Firmware", MessageBoxButton.YesNo) && StartInvokeFirmware() == 0)
				{
					return 0;
				}
				break;
			case 16:
				if (updateInfoList[nUpdateCur].strVersion == _det.Prop_Attr_UROM_McuVersion.ToString() && MessageBoxResult.No == MessageBox.Show("The MCU1 versions are the same, whether to continue to upgrade?", "Upgrade Firmware", MessageBoxButton.YesNo) && StartInvokeFirmware() == 0)
				{
					return 0;
				}
				break;
			}
			if (bExitUpgrade)
			{
				return -3;
			}
			int nDeviceType = updateInfoList[nUpdateCur].nDeviceType;
			int num = Instance.Invoke(2008, nDeviceType, txtFullPath.Text);
			if (num != 0 && num != 1)
			{
				MessageBox.Show("Upgrade firmware failed! Err = " + ErrorHelper.GetErrorDesp(num));
				return -4;
			}
			StartTime = DateTime.Now;
			bIsShowWindow = true;
			btnBrowse.IsEnabled = false;
			btnStartUpgrade.IsEnabled = false;
			bExitUpgrade = true;
			return 0;
		}

		public void UpdateCallback()
		{
			int num = 100;
			updateProgressBar.Value = num;
			txtProgressBar.Content = num.ToString() + "%";
			if (nUpdateCur + 1 == nUpdateCount)
			{
				Unload();
				bIsUpgradeFinish = true;
				return;
			}
			StartTimeTick(false);
			bExitUpgrade = false;
			if (StartInvokeFirmware() < 0)
			{
				Unload();
				return;
			}
			bExitUpgrade = true;
			StartTimeTick(true);
		}

		private void Unload()
		{
			StartTimeTick(false);
			bIsShowWindow = false;
			nUpdateCur = -1;
			btnBrowse.IsEnabled = true;
			btnStartUpgrade.IsEnabled = true;
			bExitUpgrade = false;
		}

		private void StartTimeTick(bool bValue)
		{
			if (bValue)
			{
				if (timerUpdate == null)
				{
					timerUpdate = new DispatcherTimer();
					timerUpdate.Interval = TimeSpan.FromMilliseconds(timeUpdateDelay);
					timerUpdate.Tick += TimerUpdate_Tick;
					timerUpdate.Start();
				}
			}
			else if (timerUpdate != null)
			{
				TimerUpdate_Tick(null, null);
				timerUpdate.Stop();
				timerUpdate = null;
			}
		}

		public void UnloadUpgradeState()
		{
			Unload();
		}

		public bool IsUpgradeFinish()
		{
			return bIsUpgradeFinish;
		}

		private string GetProductName(int nProductID)
		{
			switch (nProductID)
			{
			case 1:
				strProductName = "Venu1717F";
				break;
			case 6:
				strProductName = "Mercu0909F";
				break;
			case 56:
				strProductName = "Mercu0909FN";
				break;
			case 49:
				strProductName = "Mercu1616TE";
				break;
			case 11:
				strProductName = "Mammo1012F";
				break;
			case 12:
				strProductName = "NDT0505F";
				break;
			case 22:
				strProductName = "Venu1417W";
				break;
			case 28:
				strProductName = "Senu1417P";
				break;
			case 29:
				strProductName = "Venu1717M2";
				break;
			case 25:
				strProductName = "Penu1417P";
				break;
			case 32:
				strProductName = "Mars1417V1";
				break;
			case 59:
				strProductName = "Mars1417V2";
				break;
			case 33:
				strProductName = "Penu1417P_plus";
				break;
			case 37:
				strProductName = "Mars1717V1";
				break;
			case 62:
				strProductName = "Mars1717V2";
				break;
			case 38:
				strProductName = "Venu1717MF";
				break;
			case 39:
				strProductName = "Sars1417";
				break;
			case 42:
				strProductName = "Mars1417X";
				break;
			case 45:
				strProductName = "Mars1717XU";
				break;
			case 48:
				strProductName = "Mercu1717V";
				break;
			case 41:
				strProductName = "Mars1012X";
				break;
			case 60:
				strProductName = "Venu1012V";
				break;
			case 52:
				strProductName = "Mars1417XF";
				break;
			case 51:
				strProductName = "Mars1717XF";
				break;
			case 58:
				strProductName = "Venu1717MN";
				break;
			case 72:
				strProductName = "Venu1717X";
				break;
			case 80:
				strProductName = "Venu1717MX";
				break;
			case 88:
				strProductName = "Venu1012VD";
				break;
			case 91:
				strProductName = "Venu1717XV";
				break;
			case 93:
				strProductName = "Luna1417XM";
				break;
			default:
				strProductName = ((Enm_ProdType)nProductID).ToString().Remove(0, "Enm_Prd_".Length);
				break;
			}
			return strProductName;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/firmwareupgradeexwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				FirmwareUpgradeExWnd1 = (FirmwareUpgradeExWnd)target;
				FirmwareUpgradeExWnd1.Closing += FirmwareUpgradeExWnd1_Closing;
				break;
			case 2:
				txtFullPath = (TextBox)target;
				break;
			case 3:
				btnBrowse = (Button)target;
				btnBrowse.Click += btnBrowse_Click;
				break;
			case 4:
				updateProgressBar = (ProgressBar)target;
				break;
			case 5:
				btnStartUpgrade = (Button)target;
				btnStartUpgrade.Click += btnStartUpgrade_Click;
				break;
			case 6:
				txtPackDesc = (TextBox)target;
				break;
			case 7:
				txtProgressBar = (Label)target;
				break;
			case 8:
				txtCurVersionInfo = (TextBox)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
