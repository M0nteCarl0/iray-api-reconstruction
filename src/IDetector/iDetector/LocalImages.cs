using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class LocalImages : UserControl, IComponentConnector
	{
		private LocalImagesVM PageVM;

		private bool _contentLoaded;

		public LocalImages()
		{
			InitializeComponent();
			base.IsVisibleChanged += OnIsVisibleChanged;
		}

		public void Init(Detector detector)
		{
			PageVM = new LocalImagesVM(this, detector);
			base.DataContext = PageVM;
		}

		private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (PageVM != null)
			{
				if ((bool)e.NewValue)
				{
					PageVM.RegisterCallback();
				}
				else
				{
					PageVM.UnRegisterCallback();
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/localimages.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			_contentLoaded = true;
		}
	}
}
