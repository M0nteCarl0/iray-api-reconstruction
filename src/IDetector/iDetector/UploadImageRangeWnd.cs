using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class UploadImageRangeWnd : Window, IComponentConnector
	{
		internal DigitalTextBox xStartIndex;

		internal DigitalTextBox xEndIndex;

		private bool _contentLoaded;

		public int StartIndex
		{
			get
			{
				return Convert.ToInt32(xStartIndex.Text);
			}
		}

		public int EndIndex
		{
			get
			{
				return Convert.ToInt32(xEndIndex.Text);
			}
		}

		private bool Result
		{
			get;
			set;
		}

		public UploadImageRangeWnd()
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			Result = false;
		}

		private void OnOK(object sender, RoutedEventArgs e)
		{
			if (StartIndex < 1 || StartIndex > 200 || EndIndex < 1 || EndIndex > 200 || StartIndex > EndIndex)
			{
				MessageBox.Show("Index out of range!");
				return;
			}
			Result = true;
			Close();
		}

		public bool OnShowDialog()
		{
			ShowDialog();
			return Result;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/uploadimagerangewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xStartIndex = (DigitalTextBox)target;
				break;
			case 2:
				xEndIndex = (DigitalTextBox)target;
				break;
			case 3:
				((Button)target).Click += OnOK;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
