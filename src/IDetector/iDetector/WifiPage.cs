using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class WifiPage : UserControl, IComponentConnector
	{
		private WifiPageVM PageVM;

		internal RadioButton ClientMode;

		internal RadioButton APMode;

		internal ListView xClient;

		private bool _contentLoaded;

		public WifiPage()
		{
			InitializeComponent();
			base.IsVisibleChanged += OnIsVisibleChanged;
		}

		public void Init(Detector detector)
		{
			PageVM = new WifiPageVM(this, detector);
			base.DataContext = PageVM;
			xClient.MouseDoubleClick += PageVM.OnDBClickClient;
		}

		public void Close()
		{
			base.IsVisibleChanged -= OnIsVisibleChanged;
			if (PageVM != null)
			{
				xClient.MouseDoubleClick -= PageVM.OnDBClickClient;
			}
		}

		private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (PageVM != null)
			{
				if ((bool)e.NewValue)
				{
					PageVM.RegisterCallback();
				}
				else
				{
					PageVM.UnRegisterCallback();
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/wifipage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				ClientMode = (RadioButton)target;
				break;
			case 2:
				APMode = (RadioButton)target;
				break;
			case 3:
				xClient = (ListView)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
