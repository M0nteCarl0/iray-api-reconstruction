using System.Runtime.InteropServices;

namespace iDetector
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	public struct stVTMapItem
	{
		public int PGA;

		public int Binning;

		public float VTValue;
	}
}
