namespace iDetector
{
	public class ImageItem : NotifyObject
	{
		private string _Index;

		private string _FileName;

		private string _CreateTime;

		private string _DelayTime;

		private string _ImageAttr;

		private string _UploadStatus;

		public string Index
		{
			get
			{
				return _Index;
			}
			set
			{
				_Index = value;
				NotifyPropertyChanged("Index");
			}
		}

		public string FileName
		{
			get
			{
				return _FileName;
			}
			set
			{
				_FileName = value;
				NotifyPropertyChanged("FileName");
			}
		}

		public string CreateTime
		{
			get
			{
				return _CreateTime;
			}
			set
			{
				_CreateTime = value;
				NotifyPropertyChanged("CreateTime");
			}
		}

		public string DelayTime
		{
			get
			{
				return _DelayTime;
			}
			set
			{
				_DelayTime = value;
				NotifyPropertyChanged("DelayTime");
			}
		}

		public string ImageAttr
		{
			get
			{
				return _ImageAttr;
			}
			set
			{
				_ImageAttr = value;
				NotifyPropertyChanged("ImageAttr");
			}
		}

		public string UploadStatus
		{
			get
			{
				return _UploadStatus;
			}
			set
			{
				_UploadStatus = value;
				NotifyPropertyChanged("UploadStatus");
			}
		}
	}
}
