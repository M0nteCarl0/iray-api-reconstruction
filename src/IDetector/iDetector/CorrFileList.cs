using System;
using System.Collections;
using System.Collections.Generic;

namespace iDetector
{
	public class CorrFileList
	{
		private Dictionary<int, int> _fileIndexMap;

		private ArrayList _filelist;

		public ArrayList FileList
		{
			get
			{
				return _filelist;
			}
		}

		public CorrFileList()
		{
			_filelist = new ArrayList();
			_fileIndexMap = new Dictionary<int, int>();
		}

		public void AddItem(FPDCorrFileList item)
		{
			_fileIndexMap[Convert.ToInt32(item.Index)] = _filelist.Count;
			_filelist.Add(item);
		}
	}
}
