namespace iDetector
{
	public struct GateValueInfo
	{
		public int nWidth;

		public int nHeight;

		public ushort[] ImgData;
	}
}
