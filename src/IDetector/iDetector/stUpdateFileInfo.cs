namespace iDetector
{
	public struct stUpdateFileInfo
	{
		public int nCur;

		public int nDeviceType;

		public string strDesc;

		public string strVersion;
	}
}
