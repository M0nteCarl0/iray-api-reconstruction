namespace iDetector
{
	public struct DefectMapInfo
	{
		public int nWidth;

		public int nHeight;

		public byte[] defectMapData;
	}
}
