namespace iDetector
{
	public struct GateLineInfo
	{
		public int nHeight;

		public int nGateSize;

		public int nGateEdge;
	}
}
