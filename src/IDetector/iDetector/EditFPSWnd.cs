using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class EditFPSWnd : Window, IComponentConnector
	{
		public ApplicationMode editInfo;

		internal TextBox _txtIndex;

		internal TextBox _txtModeName;

		internal Label _labelPGA;

		internal TextBox _txtPGA;

		internal TextBox _txtBinning;

		internal DockPanel _txtZoomPanel;

		internal TextBox _txtZoom;

		internal DockPanel _txtROIPanel;

		internal TextBox _txtROI;

		internal DockPanel _txtExpModePanel;

		internal TextBox _txtExpMode;

		internal TextBox _txtFreq;

		internal Button btnOK;

		internal Button btnCancel;

		private bool _contentLoaded;

		public EditFPSWnd(ApplicationMode modeinfo)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			editInfo = modeinfo;
			_txtIndex.Text = modeinfo.Index.ToString();
			_txtModeName.Text = modeinfo.Subset;
			_txtPGA.Text = modeinfo.PGA.ToString();
			_txtBinning.Text = modeinfo.Binning.ToString();
			if (modeinfo.PGAAsFull)
			{
				_labelPGA.Content = "Full";
			}
			if (modeinfo.NoZoom)
			{
				_txtZoomPanel.Visibility = Visibility.Collapsed;
			}
			else
			{
				_txtZoom.Text = modeinfo.Zoom.ToString();
			}
			if (!modeinfo.ExistedExpMode)
			{
				_txtExpModePanel.Visibility = Visibility.Collapsed;
			}
			else
			{
				_txtExpMode.Text = modeinfo.ExpMode;
			}
			if (!modeinfo.ExistedROIRange)
			{
				_txtROIPanel.Visibility = Visibility.Collapsed;
			}
			else
			{
				_txtROI.Text = modeinfo.ROIRange;
			}
			_txtFreq.Text = modeinfo.Freq.ToString();
		}

		private void btnOK_Click(object sender, RoutedEventArgs e)
		{
			bool value = false;
			int result = 255;
			if (int.TryParse(_txtPGA.Text, out result) && editInfo.PGA != result)
			{
				value = true;
				editInfo.PGA = result;
			}
			result = 255;
			if (int.TryParse(_txtBinning.Text, out result) && editInfo.Binning != result)
			{
				value = true;
				editInfo.Binning = result;
			}
			double result2 = 0.0;
			if (double.TryParse(_txtFreq.Text, out result2) && Math.Abs(editInfo.Freq - result2) >= 0.5)
			{
				value = true;
				editInfo.Freq = Math.Round(result2, 1);
			}
			base.DialogResult = value;
			Close();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
			Close();
		}

		private void FreqValueTxt_TextChanged(object sender, TextChangedEventArgs e)
		{
			TextBox textBox = sender as TextBox;
			TextChange[] array = new TextChange[e.Changes.Count];
			e.Changes.CopyTo(array, 0);
			int offset = array[0].Offset;
			if (array[0].AddedLength > 0)
			{
				double result = 0.0;
				if (!double.TryParse(textBox.Text, out result))
				{
					textBox.Text = textBox.Text.Remove(offset, array[0].AddedLength);
					textBox.Select(offset, 0);
				}
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/editfpswnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_txtIndex = (TextBox)target;
				break;
			case 2:
				_txtModeName = (TextBox)target;
				break;
			case 3:
				_labelPGA = (Label)target;
				break;
			case 4:
				_txtPGA = (TextBox)target;
				break;
			case 5:
				_txtBinning = (TextBox)target;
				break;
			case 6:
				_txtZoomPanel = (DockPanel)target;
				break;
			case 7:
				_txtZoom = (TextBox)target;
				break;
			case 8:
				_txtROIPanel = (DockPanel)target;
				break;
			case 9:
				_txtROI = (TextBox)target;
				break;
			case 10:
				_txtExpModePanel = (DockPanel)target;
				break;
			case 11:
				_txtExpMode = (TextBox)target;
				break;
			case 12:
				_txtFreq = (TextBox)target;
				_txtFreq.TextChanged += FreqValueTxt_TextChanged;
				break;
			case 13:
				btnOK = (Button)target;
				btnOK.Click += btnOK_Click;
				break;
			case 14:
				btnCancel = (Button)target;
				btnCancel.Click += btnCancel_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
