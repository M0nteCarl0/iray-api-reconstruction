using CorrectExUI;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class CreateCorrectTemplateWnd : Window, IDisposable, IComponentConnector
	{
		private const int TaskTimeOutSeconds = 30;

		private UITimer ConnectStateTimer;

		private Detector Instance;

		private DetecInfo _detecInfo;

		public CorrectionExModeFilePage _modeFilePage;

		public CorrectionExModeFileStaticPage _modeFileStaticPage;

		public GenerateExOffsetPage _genOffsetPage;

		public GenerateExDynamicGainDefectPage _genGainDefectPage;

		public GenerateExGainPage _genGainPage;

		public GenerateExDefectPage _genDefectPage;

		public GenerateExDynamicAcqPage _genDynamicGainAcqPage;

		public GenerateExDynamicAcqPage _genDynamicDefectAcqPage;

		private TabItem _currentTabitem;

		private GenerateEventMsg EventMsg;

		private DlgTaskTimeOut CloseCreateWnd;

		public bool bBreakError;

		public bool bIsActiveSubset;

		internal TabControl _tabCalibration;

		internal TabItem _tabItemModeFiles;

		internal TabItem _tabItemModeFilesStatic;

		internal TabItem _tabItemOffset;

		internal TabItem _tabItemGain;

		internal TabItem _tabItemDefect;

		internal TabItem _tabItemDynaGain;

		internal TabItem _tabItemDynaDefect;

		internal TabItem _tabItemGainDefect;

		internal ComboBox xComboMsg;

		private bool _contentLoaded;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public CreateCorrectTemplateWnd(Detector instance)
		{
			InitializeComponent();
			Initialize(instance);
		}

		private void Initialize(Detector instance)
		{
			bBreakError = false;
			Instance = instance;
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			bIsActiveSubset = false;
			_detecInfo = new DetecInfo(instance, new Shell(this));
			_genOffsetPage = new GenerateExOffsetPage(_detecInfo);
			_modeFileStaticPage = new CorrectionExModeFileStaticPage(instance);
			_modeFilePage = new CorrectionExModeFilePage(instance);
			_genGainPage = new GenerateExGainPage(_detecInfo);
			_genDefectPage = new GenerateExDefectPage(_detecInfo);
			_genGainDefectPage = new GenerateExDynamicGainDefectPage(_detecInfo);
			_genDynamicGainAcqPage = new GenerateExDynamicAcqPage(_detecInfo, Enm_FileTypes.Enm_File_Gain);
			_genDynamicDefectAcqPage = new GenerateExDynamicAcqPage(_detecInfo, Enm_FileTypes.Enm_File_Defect);
			_tabItemOffset.Content = _genOffsetPage;
			_tabItemModeFilesStatic.Header = "Mode&Files";
			_tabItemGainDefect.Header = "Create Gain&Defect";
			switch (instance.Prop_Attr_UROM_ProductNo)
			{
			case 11:
			case 32:
			case 37:
			case 38:
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 59:
			case 60:
			case 62:
			case 88:
			case 93:
				_tabItemModeFilesStatic.Content = _modeFileStaticPage;
				_tabItemGain.Content = _genGainPage;
				_tabItemDefect.Content = _genDefectPage;
				_tabCalibration.Items.Remove(_tabItemModeFiles);
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_tabCalibration.Items.Remove(_tabItemDynaGain);
				_tabCalibration.Items.Remove(_tabItemDynaDefect);
				_modeFileStaticPage.Initialize();
				_genGainPage.Initialize();
				_genDefectPage.Initialize();
				_modeFilePage.RemoveCallbackHandler();
				_modeFilePage.Clear();
				break;
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 68:
			case 73:
			case 74:
			case 87:
			{
				_tabItemModeFiles.Content = _modeFilePage;
				_tabItemGainDefect.Content = _genGainDefectPage;
				_tabItemGain.Content = _genGainPage;
				_tabItemDefect.Content = _genDefectPage;
				_tabCalibration.Items.Remove(_tabItemModeFilesStatic);
				_tabCalibration.Items.Remove(_tabItemGain);
				_tabCalibration.Items.Remove(_tabItemDefect);
				_tabCalibration.Items.Remove(_tabItemDynaGain);
				_tabCalibration.Items.Remove(_tabItemDynaDefect);
				CorrectionExModeFilePage modeFilePage = _modeFilePage;
				modeFilePage.IsDynamicModeSelected = (Action<bool>)Delegate.Combine(modeFilePage.IsDynamicModeSelected, new Action<bool>(LoadCalibrationItems4DynamicSerialFPD));
				_modeFilePage.Initialize(instance.Prop_Attr_UROM_ProductNo);
				_genGainDefectPage.Initialize();
				_modeFileStaticPage.RemoveCallbackHandler();
				bIsActiveSubset = true;
				break;
			}
			case 58:
				_tabItemModeFilesStatic.Content = _modeFileStaticPage;
				_tabItemDynaGain.Content = _genDynamicGainAcqPage;
				_tabItemDynaDefect.Content = _genDynamicDefectAcqPage;
				_tabCalibration.Items.Remove(_tabItemModeFiles);
				_tabCalibration.Items.Remove(_tabItemGain);
				_tabCalibration.Items.Remove(_tabItemDefect);
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_modeFileStaticPage.Initialize();
				_genDynamicGainAcqPage.Initialize();
				_genDynamicDefectAcqPage.Initialize();
				_modeFilePage.RemoveCallbackHandler();
				break;
			default:
				_tabItemModeFilesStatic.Content = _modeFileStaticPage;
				_tabItemGain.Content = _genGainPage;
				_tabItemDefect.Content = _genDefectPage;
				_tabCalibration.Items.Remove(_tabItemModeFiles);
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_tabCalibration.Items.Remove(_tabItemDynaGain);
				_tabCalibration.Items.Remove(_tabItemDynaDefect);
				_modeFileStaticPage.Initialize();
				_genGainPage.Initialize();
				_genDefectPage.Initialize();
				_modeFilePage.RemoveCallbackHandler();
				_modeFilePage.Clear();
				break;
			}
			this.SdkEventHandler = ProcessEvent;
			Instance.SdkCallbackEvent += OnSdkCallback;
			EventMsg = new GenerateEventMsg(xComboMsg);
			ConnectStateTimer = new UITimer(800, MonitorChannelStateChange);
			ConnectStateTimer.Start();
		}

		private void LoadCalibrationItems4DynamicSerialFPD(bool isDyanmic)
		{
			if (isDyanmic)
			{
				if (!_tabCalibration.Items.Contains(_tabItemGainDefect))
				{
					_genGainPage.Clear();
					_genDefectPage.Clear();
					_genGainDefectPage.Initialize();
					_tabCalibration.Items.Remove(_tabItemGain);
					_tabCalibration.Items.Remove(_tabItemDefect);
					_tabCalibration.Items.Add(_tabItemGainDefect);
				}
			}
			else if (!_tabCalibration.Items.Contains(_tabItemGain))
			{
				_genGainDefectPage.Clear();
				_genGainPage.Initialize();
				_genDefectPage.Initialize();
				_tabCalibration.Items.Remove(_tabItemGainDefect);
				_tabCalibration.Items.Add(_tabItemGain);
				_tabCalibration.Items.Add(_tabItemDefect);
			}
		}

		private void _tabCalibration_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (!(e.Source is TabControl))
			{
				return;
			}
			TabItem currentTabitem = _tabCalibration.SelectedItem as TabItem;
			if (_currentTabitem != null)
			{
				switch (_currentTabitem.Name)
				{
				case "_tabItemModeFiles":
					if (bIsActiveSubset)
					{
						_genGainDefectPage.SetCurSubset(_modeFilePage.GetActiveSubset());
						return;
					}
					break;
				case "_tabItemOffset":
					if (_genOffsetPage.GetCancelStatus())
					{
						_genOffsetPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemOffset;
						_currentTabitem = _tabItemOffset;
						return;
					}
					break;
				case "_tabItemGain":
					if (_genGainPage.GetCancelStatus())
					{
						_genGainPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemGain;
						_currentTabitem = _tabItemGain;
						return;
					}
					break;
				case "_tabItemDefect":
					if (_genDefectPage.GetCancelStatus())
					{
						_genDefectPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemDefect;
						_currentTabitem = _tabItemDefect;
						return;
					}
					break;
				case "_tabItemGainDefect":
					if (_genGainDefectPage.GetCancelStatus())
					{
						_genGainDefectPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemGainDefect;
						_currentTabitem = _tabItemGainDefect;
						return;
					}
					break;
				case "_tabItemDynaGain":
					if (_genDynamicGainAcqPage.GetCancelStatus())
					{
						_genDynamicGainAcqPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemDynaGain;
						_currentTabitem = _tabItemDynaGain;
						return;
					}
					break;
				case "_tabItemDynaDefect":
					if (_genDynamicDefectAcqPage.GetCancelStatus())
					{
						_genDynamicDefectAcqPage.DoCancel();
						_tabCalibration.SelectedItem = _tabItemDynaDefect;
						_currentTabitem = _tabItemDynaDefect;
						return;
					}
					break;
				}
			}
			_currentTabitem = currentTabitem;
		}

		public bool GetGenExCorrectStatus()
		{
			bool result = false;
			if (_currentTabitem != null)
			{
				switch (_currentTabitem.Name)
				{
				case "_tabItemOffset":
					result = _genOffsetPage.GetCancelStatus();
					break;
				case "_tabItemGain":
					result = _genGainPage.GetCancelStatus();
					break;
				case "_tabItemDefect":
					result = _genDefectPage.GetCancelStatus();
					break;
				case "_tabItemGainDefect":
					result = _genGainDefectPage.GetCancelStatus();
					break;
				case "_tabItemDynaGain":
					result = _genDynamicGainAcqPage.GetCancelStatus();
					break;
				case "_tabItemDynaDefect":
					result = _genDynamicDefectAcqPage.GetCancelStatus();
					break;
				}
			}
			return result;
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
		}

		private void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			EventMsg.AddSdkInfo(sender.ID, (Enm_EventLevel)nEventLevel, nEventID, nParam1, nParam2, strMsg);
			switch (nEventID)
			{
			case 6:
				CloseMessageBox();
				break;
			case 4:
				if (1005 != nParam1)
				{
					CloseMessageBox();
				}
				break;
			case 5:
				CloseMessageBox();
				break;
			case 17:
				if (30 == nParam2)
				{
					ConnectStateTimer.Stop();
					Close();
				}
				break;
			}
		}

		private void CloseMessageBox()
		{
			if (CloseCreateWnd != null)
			{
				CloseCreateWnd.StopTask();
				CloseCreateWnd = null;
			}
		}

		private void MessageBoxShow(string content)
		{
			MessageBox.Show(this, content);
		}

		private void CreateCorrectTemplateWnd_Closing(object sender, CancelEventArgs e)
		{
			if (4 == Instance.Prop_Attr_ConnState || 3 == Instance.Prop_Attr_ConnState)
			{
				int num = Instance.Abort();
				if (1 == num)
				{
					CloseCreateWnd = new DlgTaskTimeOut("Cancel task", "Cancel current task...", 30, this);
					CloseCreateWnd.RunTask();
				}
			}
			if (GetGenExCorrectStatus())
			{
				bBreakError = true;
			}
		}

		private void CreateCorrectTemplateWnd_Closed(object sender, EventArgs e)
		{
			ConnectStateTimer.Stop();
			Instance.SdkCallbackEvent -= OnSdkCallback;
			_modeFileStaticPage.RemoveCallbackHandler();
			_modeFilePage.RemoveCallbackHandler();
			_genOffsetPage.Clear();
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 11:
			case 32:
			case 37:
			case 38:
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 59:
			case 60:
			case 62:
			case 88:
			case 93:
				_modeFileStaticPage.Clear();
				_genGainPage.Clear();
				_genDefectPage.Clear();
				break;
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 68:
			case 73:
			case 74:
			case 87:
				if (_tabCalibration.Items.Contains(_tabItemGain))
				{
					_genGainPage.Clear();
					_genDefectPage.Clear();
				}
				else if (_tabCalibration.Items.Contains(_tabItemGainDefect))
				{
					_genGainDefectPage.Clear();
				}
				break;
			default:
				_modeFileStaticPage.Clear();
				_genGainPage.Clear();
				_genDefectPage.Clear();
				break;
			case 58:
				break;
			}
			GC.Collect();
		}

		private void MonitorChannelStateChange()
		{
			if (Instance != null)
			{
				switch (Instance.Prop_Attr_ConnState)
				{
				case 0:
				case 1:
				case 2:
					Close();
					break;
				}
			}
		}

		public void Dispose()
		{
			CorrectionExModeFilePage modeFilePage = _modeFilePage;
			modeFilePage.IsDynamicModeSelected = (Action<bool>)Delegate.Remove(modeFilePage.IsDynamicModeSelected, new Action<bool>(LoadCalibrationItems4DynamicSerialFPD));
			this.SdkEventHandler = null;
			GC.Collect();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/createcorrecttemplatewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((CreateCorrectTemplateWnd)target).Closed += CreateCorrectTemplateWnd_Closed;
				((CreateCorrectTemplateWnd)target).Closing += CreateCorrectTemplateWnd_Closing;
				break;
			case 2:
				_tabCalibration = (TabControl)target;
				_tabCalibration.SelectionChanged += _tabCalibration_SelectionChanged;
				break;
			case 3:
				_tabItemModeFiles = (TabItem)target;
				break;
			case 4:
				_tabItemModeFilesStatic = (TabItem)target;
				break;
			case 5:
				_tabItemOffset = (TabItem)target;
				break;
			case 6:
				_tabItemGain = (TabItem)target;
				break;
			case 7:
				_tabItemDefect = (TabItem)target;
				break;
			case 8:
				_tabItemDynaGain = (TabItem)target;
				break;
			case 9:
				_tabItemDynaDefect = (TabItem)target;
				break;
			case 10:
				_tabItemGainDefect = (TabItem)target;
				break;
			case 11:
				xComboMsg = (ComboBox)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
