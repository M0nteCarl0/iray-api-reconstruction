using CorrectExUI;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class DownloadToFpdWnd : Window, IComponentConnector
	{
		public int nIndex;

		public string strDesc;

		internal TextBox _labModeName;

		internal ComboBox _combIndex;

		internal TextBox _txtDesc;

		internal TextBox _txtFullPath;

		internal Button btnDownLoad;

		internal Button btnCancel;

		private bool _contentLoaded;

		public DownloadToFpdWnd(List<DownloadInfo> info)
		{
			InitializeComponent();
			Initialize(info);
		}

		private void Initialize(List<DownloadInfo> info)
		{
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			_labModeName.Text = info[0].modeName;
			for (int i = 1; i <= info[0].indexCount; i++)
			{
				_combIndex.Items.Add(i);
			}
			_combIndex.SelectedIndex = info[0].index - 1;
			_txtDesc.Text = info[0].desc;
			string text = "";
			for (int j = 0; j < info.Count(); j++)
			{
				text = text + info[j].fullPath + "\n--------------------------------------------------\n";
			}
			_txtFullPath.Text = text;
		}

		private void btnDownLoad_Click(object sender, RoutedEventArgs e)
		{
			strDesc = _txtDesc.Text.Trim();
			nIndex = _combIndex.SelectedIndex + 1;
			base.DialogResult = true;
			Close();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			base.DialogResult = false;
			Close();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/downloadtofpdwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_labModeName = (TextBox)target;
				break;
			case 2:
				_combIndex = (ComboBox)target;
				break;
			case 3:
				_txtDesc = (TextBox)target;
				break;
			case 4:
				_txtFullPath = (TextBox)target;
				break;
			case 5:
				btnDownLoad = (Button)target;
				btnDownLoad.Click += btnDownLoad_Click;
				break;
			case 6:
				btnCancel = (Button)target;
				btnCancel.Click += btnCancel_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
