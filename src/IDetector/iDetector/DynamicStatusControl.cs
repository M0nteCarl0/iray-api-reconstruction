namespace iDetector
{
	public class DynamicStatusControl : StatusControl
	{
		public override void UpdateStatusAfterStartAcq()
		{
			base.IsSequenceAcq = true;
			base.EnableUpdateImagelist = false;
		}
	}
}
