using System;

namespace iDetector
{
	public class MonitorTask : IProgress
	{
		private MMTimer timer;

		public bool IsTaskRunning
		{
			get;
			set;
		}

		public MonitorTask(int timeOutMilliSeconds, Action taskTerminateAction)
		{
			timer = new MMTimer(timeOutMilliSeconds, taskTerminateAction);
		}

		public int RunTask()
		{
			IsTaskRunning = true;
			timer.Start();
			return 0;
		}

		public void StopTask()
		{
			IsTaskRunning = false;
			timer.Close();
		}
	}
}
