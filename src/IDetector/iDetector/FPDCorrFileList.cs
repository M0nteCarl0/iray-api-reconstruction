using System.ComponentModel;

namespace iDetector
{
	public class FPDCorrFileList : INotifyPropertyChanged
	{
		private string _validity;

		private string _activity;

		private string _despInfo;

		public string Type
		{
			get;
			set;
		}

		public string Index
		{
			get;
			set;
		}

		public string PGA
		{
			get;
			set;
		}

		public string Binning
		{
			get;
			set;
		}

		public string Zoom
		{
			get;
			set;
		}

		public string FullWell
		{
			get;
			set;
		}

		public string ROIRange
		{
			get;
			set;
		}

		public string Validity
		{
			get
			{
				return _validity;
			}
			set
			{
				_validity = value;
			}
		}

		public string Activity
		{
			get
			{
				return _activity;
			}
			set
			{
				_activity = value;
			}
		}

		public string DespInfo
		{
			get
			{
				return _despInfo;
			}
			set
			{
				_despInfo = value;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void NotifyView(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}
	}
}
