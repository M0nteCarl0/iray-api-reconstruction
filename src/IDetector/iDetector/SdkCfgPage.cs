using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class SdkCfgPage : UserControl, IDetSubPage, IComponentConnector
	{
		private Detector _det;

		private bool _bIsLoaded;

		private List<int> _monitorAttrs;

		internal StackPanel _panel;

		internal ComboBox xLogLevelCB;

		private bool _contentLoaded;

		public bool bHandled
		{
			get;
			set;
		}

		public ObservableCollection<EnumDisplayItem> LogLevelList
		{
			get;
			set;
		}

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
			}
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		public SdkCfgPage()
		{
			InitializeComponent();
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				_bIsLoaded = false;
				base.Loaded += OnLoaded;
				base.IsVisibleChanged += OnIsVisibleChanged;
				_monitorAttrs = new List<int>();
			}
		}

		private void OnLoaded(object sender, RoutedEventArgs e)
		{
			if (_bIsLoaded)
			{
				return;
			}
			LogLevelList = SDKEnumCollector.LoadParam("Enm_LogLevel");
			xLogLevelCB.ItemsSource = LogLevelList;
			int pCount = 0;
			SdkInterface.GetAttrsCount(_det.ID, ref pCount);
			int[] array = new int[pCount];
			SdkInterface.GetAttrIDList(_det.ID, array, pCount);
			int[] array2 = array;
			foreach (int nAttrID in array2)
			{
				AttrInfo pInfo = default(AttrInfo);
				if (SdkInterface.GetAttrInfo(_det.ID, nAttrID, ref pInfo) == 0 && pInfo.bIsConfigItem == 1)
				{
					if (pInfo.bIsWritable == 1)
					{
						AddItem(pInfo.strDisplayName, 0, pInfo.nAttrID);
					}
					else
					{
						AddItem(pInfo.strDisplayName, pInfo.nAttrID, 0);
					}
				}
			}
			_bIsLoaded = true;
		}

		private CtrAttrEditor AddItem(string strLabel, int nReadAttrID, int nWriteAttrID, bool bIsBooleanVal = false)
		{
			CtrAttrEditor ctrAttrEditor = new CtrAttrEditor(strLabel, nReadAttrID, nWriteAttrID, _det);
			ctrAttrEditor.Height = 32.0;
			AttrEditorStyle ctrlStyle = default(AttrEditorStyle);
			ctrlStyle.nLabelColWidth = ((strLabel != null && strLabel.Length != 0) ? 200 : 0);
			ctrlStyle.nReadColWidth = ((nReadAttrID != 0) ? 180 : 0);
			ctrlStyle.nWriteColWidth = ((nWriteAttrID != 0) ? 180 : 0);
			ctrlStyle.nEditorColWidth = ((nWriteAttrID != 0) ? 190 : 0);
			ctrlStyle.bCheckBoxForIntType = bIsBooleanVal;
			ctrlStyle.nBtnColWidth = 80;
			ctrAttrEditor.SetCtrlStyle(ctrlStyle);
			_panel.Children.Add(ctrAttrEditor);
			if (nReadAttrID > 0)
			{
				_monitorAttrs.Add(nReadAttrID);
			}
			if (nWriteAttrID > 0)
			{
				_monitorAttrs.Add(nWriteAttrID);
			}
			return ctrAttrEditor;
		}

		private void OnSetLogLevel(object sender, RoutedEventArgs e)
		{
			if (Instance != null)
			{
				int val = (xLogLevelCB.SelectedItem as EnumDisplayItem).Val;
				int num = Instance.Invoke(1, val);
				if (1 != num && num != 0)
				{
					MessageBox.Show("Set log level failed! Error=" + ErrorHelper.GetErrorDesp(num));
				}
			}
			else
			{
				MessageBox.Show("Detector had not been initialized!");
			}
		}

		private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (_bIsLoaded && Instance != null)
			{
				if (e.NewValue.Equals(true))
				{
					xLogLevelCB.SelectedItem = SDKEnumCollector.GetItem(LogLevelList, Instance.Prop_Cfg_LogLevel);
					_det.AttrChangingMonitor.AddRef(Enumerable.ToArray(_monitorAttrs));
				}
				else
				{
					_det.AttrChangingMonitor.ReleaseRef(Enumerable.ToArray(_monitorAttrs));
				}
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IRayImageData pParam)
		{
		}

		public void OnClose()
		{
			_panel.Children.Clear();
		}

		public bool Exit()
		{
			return true;
		}

		public bool Enter()
		{
			return true;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/sdkcfgpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_panel = (StackPanel)target;
				break;
			case 2:
				xLogLevelCB = (ComboBox)target;
				break;
			case 3:
				((Button)target).Click += OnSetLogLevel;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
