using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;

namespace iDetector
{
	public class SyncboxWnd : Window, IComponentConnector
	{
		private Syncbox _sync;

		private int nSyncboxState = -1;

		private DispatcherTimer timerState;

		private int timeStateDelay = 1000;

		private List<DeteBindInfo> deteBindList;

		private int nBindId;

		private int nBindSelectItem;

		private string strBindName;

		internal SyncboxWnd SyncboxWnd1;

		internal Button btnSyncConncet;

		internal Button btnSyncDisconnect;

		internal Label labState;

		internal TextBox txtTubeReadyTimeCur;

		internal ComboBox comBindDetector;

		internal Button btnSyncWrite;

		internal TextBox txtTubeReadyTime;

		internal TextBox txtBindDetectorCur;

		internal Button btnSyncBind;

		internal Button btnSyncClose;

		private bool _contentLoaded;

		public SyncboxWnd()
		{
			InitializeComponent();
			Initialize();
		}

		private void Initialize()
		{
			labState.FontWeight = FontWeights.Bold;
			labState.FontSize = 16.0;
			txtTubeReadyTimeCur.VerticalContentAlignment = VerticalAlignment.Center;
			txtTubeReadyTime.VerticalContentAlignment = VerticalAlignment.Center;
			txtBindDetectorCur.VerticalContentAlignment = VerticalAlignment.Center;
			comBindDetector.VerticalContentAlignment = VerticalAlignment.Center;
			labState.VerticalContentAlignment = VerticalAlignment.Center;
			SetSyncboxInfo(0);
			_sync = new Syncbox();
		}

		public int SetBindInfo(List<DeteBindInfo> deteBindInfo)
		{
			if (deteBindList == null)
			{
				deteBindList = new List<DeteBindInfo>();
			}
			deteBindList.Clear();
			comBindDetector.Items.Clear();
			int num = deteBindInfo.Count();
			for (int i = 0; i < num; i++)
			{
				DeteBindInfo item = default(DeteBindInfo);
				item.strBindName = deteBindInfo[i].strBindName;
				item.nDetectorID = deteBindInfo[i].nDetectorID;
				deteBindList.Add(item);
				comBindDetector.Items.Add(item.strBindName);
			}
			return 0;
		}

		private void CreateBindItem()
		{
			int num = deteBindList.Count();
			if (num > 0)
			{
				comBindDetector.SelectedIndex = nBindSelectItem;
				btnSyncBind.IsEnabled = true;
				comBindDetector.IsEnabled = true;
				_sync.SyncBoxBindId(ref nBindId);
				if (nBindId <= 0)
				{
					txtBindDetectorCur.Text = "";
				}
				else
				{
					txtBindDetectorCur.Text = strBindName;
				}
			}
			else
			{
				btnSyncBind.IsEnabled = false;
				comBindDetector.IsEnabled = false;
				txtBindDetectorCur.Text = "";
				comBindDetector.Text = "";
			}
		}

		private void SetSyncboxInfo(int nMode)
		{
			if (nMode == 0)
			{
				txtTubeReadyTime.IsEnabled = false;
				comBindDetector.IsEnabled = false;
				btnSyncWrite.IsEnabled = false;
				btnSyncBind.IsEnabled = false;
				btnSyncConncet.IsEnabled = true;
				btnSyncDisconnect.IsEnabled = false;
				labState.Content = "";
				txtTubeReadyTimeCur.Text = "";
				txtTubeReadyTime.Text = "";
				txtBindDetectorCur.Text = "";
				comBindDetector.Text = "";
				strBindName = "";
				return;
			}
			_sync.SyncBoxState(ref nSyncboxState);
			if (nSyncboxState == 0)
			{
				labState.Content = "unknown";
				labState.Foreground = Brushes.Red;
				comBindDetector.IsEnabled = true;
				btnSyncBind.IsEnabled = true;
				txtTubeReadyTime.IsEnabled = false;
				btnSyncWrite.IsEnabled = false;
				btnSyncConncet.IsEnabled = false;
				btnSyncDisconnect.IsEnabled = true;
				CreateBindItem();
			}
			else if (nSyncboxState == 2)
			{
				labState.Content = "busy";
				labState.Foreground = Brushes.DarkOrange;
				comBindDetector.IsEnabled = false;
				btnSyncBind.IsEnabled = false;
				txtTubeReadyTime.IsEnabled = false;
				btnSyncWrite.IsEnabled = false;
				btnSyncConncet.IsEnabled = false;
				btnSyncDisconnect.IsEnabled = true;
			}
			else if (nSyncboxState == 1)
			{
				labState.Content = "ready";
				labState.Foreground = Brushes.Green;
				txtTubeReadyTime.IsEnabled = true;
				btnSyncWrite.IsEnabled = true;
				btnSyncConncet.IsEnabled = false;
				btnSyncDisconnect.IsEnabled = true;
				int nTime = 0;
				_sync.SyncBoxRead(ref nTime);
				txtTubeReadyTimeCur.Text = nTime.ToString();
				CreateBindItem();
			}
		}

		private void TimerState_Tick(object sender, object e)
		{
			SetSyncboxInfo(1);
		}

		private void SyncboxDisconnect()
		{
			if (timerState != null)
			{
				if (_sync.SyncboxDisconncet() == 0)
				{
					timerState.Stop();
					timerState = null;
					SetSyncboxInfo(0);
					Log.Instance().Write("Syncbox disconnect successfully!");
				}
				else
				{
					Log.Instance().Write("Syncbox disconnect failed!");
				}
			}
		}

		private void btnSyncConncet_Click(object sender, RoutedEventArgs e)
		{
			if (_sync.SyncboxConncet() == 0)
			{
				if (timerState == null)
				{
					timerState = new DispatcherTimer();
					timerState.Interval = TimeSpan.FromMilliseconds(timeStateDelay);
					timerState.Tick += TimerState_Tick;
					timerState.Start();
				}
				Log.Instance().Write("Syncbox connect successfully!");
			}
			else
			{
				Log.Instance().Write("Syncbox connect failed!");
			}
		}

		private void btnSyncDisconnect_Click(object sender, RoutedEventArgs e)
		{
			SyncboxDisconnect();
		}

		private void btnSyncWrite_Click(object sender, RoutedEventArgs e)
		{
			if (txtTubeReadyTime.Text.Trim() == "")
			{
				MessageBox.Show("The tube ready time value can not be empty!");
				return;
			}
			int num = (int)Convert.ToSingle(txtTubeReadyTime.Text);
			if (num < 0 || num > 10000)
			{
				MessageBox.Show("The tube ready time is error, the value is 0 ~ 10000ms!");
				return;
			}
			int num2 = _sync.SyncBoxWrite(num);
			if (num2 != 0)
			{
				Log.Instance().Write("Syncbox write failed:{0}!", num2);
				MessageBox.Show("Syncbox write failed!");
			}
		}

		private void btnSyncBind_Click(object sender, RoutedEventArgs e)
		{
			int num = deteBindList.Count();
			int num2 = 0;
			while (true)
			{
				if (num2 < num)
				{
					if (deteBindList[num2].strBindName == comBindDetector.Text.Trim())
					{
						break;
					}
					num2++;
					continue;
				}
				return;
			}
			int num3 = _sync.SyncBoxBind(deteBindList[num2].nDetectorID);
			if (num3 == 0)
			{
				strBindName = comBindDetector.Text.Trim();
				txtBindDetectorCur.Text = strBindName;
				MessageBox.Show("Detector bind successfully!");
			}
			else
			{
				ErrorInfo pInfo = default(ErrorInfo);
				SdkInterface.GetErrInfo(num3, ref pInfo);
				string str = "Detector bind failed!" + num3;
				Log.Instance().Write(str + pInfo.strDescription);
				MessageBox.Show("Detector bind failed!");
			}
		}

		private void btnSyncClose_Click(object sender, RoutedEventArgs e)
		{
			base.Visibility = Visibility.Hidden;
		}

		private void SyncboxWnd1_Loaded(object sender, RoutedEventArgs e)
		{
		}

		private void SyncboxWnd1_Closing(object sender, CancelEventArgs e)
		{
			e.Cancel = true;
			base.Visibility = Visibility.Hidden;
		}

		private void SyncboxWnd1_Closed(object sender, EventArgs e)
		{
			SyncboxDisconnect();
		}

		private void txtTubeReadyTime_TextChanged(object sender, TextChangedEventArgs e)
		{
			TextBox textBox = sender as TextBox;
			TextChange[] array = new TextChange[e.Changes.Count];
			e.Changes.CopyTo(array, 0);
			int offset = array[0].Offset;
			if (array[0].AddedLength > 0)
			{
				double result = 0.0;
				if (!double.TryParse(textBox.Text, out result))
				{
					textBox.Text = textBox.Text.Remove(offset, array[0].AddedLength);
					textBox.Select(offset, 0);
				}
			}
		}

		private void comBindDetector_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			nBindSelectItem = comBindDetector.SelectedIndex;
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/syncboxwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				SyncboxWnd1 = (SyncboxWnd)target;
				SyncboxWnd1.Loaded += SyncboxWnd1_Loaded;
				SyncboxWnd1.Closed += SyncboxWnd1_Closed;
				SyncboxWnd1.Closing += SyncboxWnd1_Closing;
				break;
			case 2:
				btnSyncConncet = (Button)target;
				btnSyncConncet.Click += btnSyncConncet_Click;
				break;
			case 3:
				btnSyncDisconnect = (Button)target;
				btnSyncDisconnect.Click += btnSyncDisconnect_Click;
				break;
			case 4:
				labState = (Label)target;
				break;
			case 5:
				txtTubeReadyTimeCur = (TextBox)target;
				break;
			case 6:
				comBindDetector = (ComboBox)target;
				comBindDetector.SelectionChanged += comBindDetector_SelectionChanged;
				break;
			case 7:
				btnSyncWrite = (Button)target;
				btnSyncWrite.Click += btnSyncWrite_Click;
				break;
			case 8:
				txtTubeReadyTime = (TextBox)target;
				txtTubeReadyTime.TextChanged += txtTubeReadyTime_TextChanged;
				break;
			case 9:
				txtBindDetectorCur = (TextBox)target;
				break;
			case 10:
				btnSyncBind = (Button)target;
				btnSyncBind.Click += btnSyncBind_Click;
				break;
			case 11:
				btnSyncClose = (Button)target;
				btnSyncClose.Click += btnSyncClose_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
