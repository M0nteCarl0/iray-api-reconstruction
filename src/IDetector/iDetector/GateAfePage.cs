using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class GateAfePage : Window, IComponentConnector
	{
		private int nWidth;

		private int nHeight;

		private int nLength;

		private byte[] imageData;

		private List<AfeLineInfo> afeLineInfoList;

		private List<GateLineInfo> gateLineInfoList;

		internal CtrImagePresent ImageViewCtrl;

		internal ComboBox GicComBox;

		internal CheckBox GicCheckBox;

		internal ComboBox AfeComBox;

		internal CheckBox AfeCheckBox;

		private bool _contentLoaded;

		public GateAfePage(GateValueInfo info)
		{
			InitializeComponent();
			base.Owner = Application.Current.MainWindow;
			Initialize(info);
		}

		private int Initialize(GateValueInfo info)
		{
			nWidth = info.nWidth;
			nHeight = info.nHeight;
			nLength = nWidth * nHeight * 2;
			afeLineInfoList = new List<AfeLineInfo>();
			gateLineInfoList = new List<GateLineInfo>();
			int pCount = 0;
			SdkInterface.GetProdCount(ref pCount);
			ProdInfo[] arr = new ProdInfo[pCount];
			IntPtr intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(ProdInfo)));
			SdkInterface.GetProdList(intPtr, pCount);
			SdkParamConvertor<ProdInfo>.IntPtrToStructArray(intPtr, ref arr);
			Marshal.FreeHGlobal(intPtr);
			int num = 0;
			ProdInfo[] array = arr;
			for (int i = 0; i < array.Length; i++)
			{
				ProdInfo prodInfo = array[i];
				SdkInterface.ProdInfoEx pInfo = default(SdkInterface.ProdInfoEx);
				SdkInterface.GetProdInfoEx(prodInfo.nProdNo, ref pInfo);
				if (nWidth == pInfo.nFullWidth)
				{
					AfeLineInfo item = default(AfeLineInfo);
					item.nWidth = nWidth;
					item.nAfeSize = pInfo.nAfeChSize;
					afeLineInfoList.Add(item);
					num++;
				}
				if (nHeight == pInfo.nFullHeight)
				{
					GateLineInfo item2 = default(GateLineInfo);
					item2.nHeight = nHeight;
					item2.nGateSize = pInfo.nGateSize;
					item2.nGateEdge = pInfo.nGateEdge;
					gateLineInfoList.Add(item2);
					num++;
				}
			}
			afeLineInfoList = afeLineInfoList.Distinct().ToList();
			gateLineInfoList = gateLineInfoList.Distinct().ToList();
			AddGicComBox();
			AddAfeComBox();
			ImageViewCtrl.ShowImageListView(false);
			ImageViewCtrl.CollapsedOperationImageButton(true);
			ImageViewCtrl.CollapsedFpsAndFramesGrid();
			imageData = new byte[nLength];
			Buffer.BlockCopy(info.ImgData, 0, imageData, 0, nLength);
			ImageViewCtrl.ImgWidth = nWidth;
			ImageViewCtrl.ImgHeight = nHeight;
			ImageViewCtrl.DisplayImage(imageData, nWidth, nHeight);
			if (num <= 1)
			{
				GicCheckBox.IsEnabled = false;
				AfeCheckBox.IsEnabled = false;
			}
			return 0;
		}

		private void GicCheckBox_Click(object sender, RoutedEventArgs e)
		{
			if (GicCheckBox.IsChecked == true)
			{
				GicComBox.IsEnabled = false;
				ImageViewCtrl.StartGICLine(true, gateLineInfoList[GicComBox.SelectedIndex].nHeight, gateLineInfoList[GicComBox.SelectedIndex].nGateSize, gateLineInfoList[GicComBox.SelectedIndex].nGateEdge);
			}
			else
			{
				GicComBox.IsEnabled = true;
				ImageViewCtrl.StartGICLine(false, 0, 0, 0);
			}
		}

		private void AfeCheckBox_Click(object sender, RoutedEventArgs e)
		{
			if (AfeCheckBox.IsChecked == true)
			{
				AfeComBox.IsEnabled = false;
				ImageViewCtrl.StartAFELine(true, afeLineInfoList[AfeComBox.SelectedIndex].nWidth, afeLineInfoList[AfeComBox.SelectedIndex].nAfeSize);
			}
			else
			{
				AfeComBox.IsEnabled = true;
				ImageViewCtrl.StartAFELine(false, 0, 0);
			}
		}

		private void AddAfeComBox()
		{
			int num = afeLineInfoList.Count();
			for (int i = 0; i < num; i++)
			{
				if (afeLineInfoList[i].nWidth % afeLineInfoList[i].nAfeSize == 0)
				{
					int num2 = afeLineInfoList[i].nWidth / afeLineInfoList[i].nAfeSize;
					AfeComBox.Items.Add(afeLineInfoList[i].nAfeSize.ToString() + "*" + num2.ToString());
				}
				else if (2448 == afeLineInfoList[i].nWidth && 256 == afeLineInfoList[i].nAfeSize)
				{
					AfeComBox.Items.Add("200+" + afeLineInfoList[i].nAfeSize.ToString() + "*8+" + 200);
				}
				else
				{
					int num3 = afeLineInfoList[i].nWidth / afeLineInfoList[i].nAfeSize;
					int num4 = afeLineInfoList[i].nWidth % afeLineInfoList[i].nAfeSize;
					AfeComBox.Items.Add(afeLineInfoList[i].nAfeSize.ToString() + "*" + num3.ToString() + "+" + num4.ToString());
				}
			}
		}

		private void AddGicComBox()
		{
			int num = gateLineInfoList.Count();
			for (int i = 0; i < num; i++)
			{
				int num2 = (gateLineInfoList[i].nHeight - 2 * gateLineInfoList[i].nGateEdge) / gateLineInfoList[i].nGateSize;
				if (gateLineInfoList[i].nGateEdge != 0)
				{
					GicComBox.Items.Add(gateLineInfoList[i].nGateEdge.ToString() + "+" + gateLineInfoList[i].nGateSize.ToString() + "*" + num2.ToString() + "+" + gateLineInfoList[i].nGateEdge.ToString());
				}
				else
				{
					GicComBox.Items.Add(gateLineInfoList[i].nGateSize.ToString() + "*" + num2.ToString());
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/gateafepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				ImageViewCtrl = (CtrImagePresent)target;
				break;
			case 2:
				GicComBox = (ComboBox)target;
				break;
			case 3:
				GicCheckBox = (CheckBox)target;
				GicCheckBox.Click += GicCheckBox_Click;
				break;
			case 4:
				AfeComBox = (ComboBox)target;
				break;
			case 5:
				AfeCheckBox = (CheckBox)target;
				AfeCheckBox.Click += AfeCheckBox_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
