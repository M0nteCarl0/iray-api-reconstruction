using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace iDetector
{
	public class MainWindow : Window, IComponentConnector
	{
		public delegate void SdkInitHandler(Detector sender, int nEventID, int nParam1, int nParam2, string message);

		public delegate void ConnectDelegate(int index, string path);

		public delegate void DisconnectDelegate(DetectorListItem item, string path);

		private const int ConnectTimeout = 300000;

		private const int DisconnectTimeout = 40000;

		private SdkInitHandler sdkCBHandler;

		private bool mbCanClosed = true;

		private PageButtonControl PageBtnCtrl;

		private IProgress taskTerminate;

		private SyncboxWnd _syncboxWnd;

		private List<DeteBindInfo> _deteInfoList;

		public FpdMgr _detMgr;

		private BindRecords _bindRecords;

		private DetPage _currDetPage;

		private DispatcherTimer _timer;

		private UITimer ConnectStateTimer;

		private DateTime _tLastAttrValCheck;

		private ToolTip mTipWnd;

		internal TextBlock _CurrDateTime;

		internal Label xDetName;

		internal Grid _gridPageContainer;

		internal HomePage _homePage;

		internal FileOperPage _fileOperPage;

		internal Button _btnHome;

		internal Button _btnAcq;

		internal Button _btnFactory;

		internal Button _btnSdkCfg;

		internal Button _btnDetCfg;

		internal Button _btnDetCali;

		internal Button _btnFileOper;

		private bool _contentLoaded;

		public ObservableCollection<DetectorListItem> DisplayList
		{
			get;
			set;
		}

		private bool ExistInstance()
		{
			bool createdNew = false;
			new Mutex(true, "iDetector", out createdNew);
			return !createdNew;
		}

		public MainWindow()
		{
			StringResource.LoadLanguage("en-US");
			InitializeComponent();
			if (Utility.IsFileExisted("res\\logo.ico"))
			{
				base.Icon = new BitmapImage(new Uri("res\\logo.ico", UriKind.Relative));
			}
			_detMgr = new FpdMgr();
			_detMgr.ScanResultNotify += OnScanResult;
			_bindRecords = new BindRecords();
			_bindRecords.Load();
			DisplayList = new ObservableCollection<DetectorListItem>();
			RefreshDisplayList();
			_homePage.DataContext = this;
			_homePage._btnScan.Click += _btnScan_Click;
			_homePage._btnBind.Click += _btnBind_Click;
			_homePage._btnRemove.Click += _btnRemove_Click;
			_homePage._btnConnect.Click += _btnConnect_Click;
			_homePage._btnClose.Click += _btnClose_Click;
			_homePage._btnDisplay.Click += _btnDisplay_Click;
			_homePage._btnAddWorkDir.Click += _btnAddWorkDir_Click;
			_homePage._btnDelWorkDir.Click += _btnDelWorkDir_Click;
			_homePage._btnSyncbox.Click += _btnSyncbox_Click;
			_homePage._listView.SelectionChanged += _homePage_SelectDetectorItem;
			_homePage._listView.MouseDoubleClick += _homePage_MouseDoubleClick;
			_homePage.Visibility = Visibility.Visible;
			_homePage.SWVersion.Content = _detMgr.GetVersion();
			int nAuthority = 0;
			SdkInterface.GetAuthority(ref nAuthority);
			if ((0x4000 & nAuthority) != 0)
			{
				_btnFactory.Visibility = Visibility.Visible;
			}
			else
			{
				_btnFactory.Visibility = Visibility.Collapsed;
			}
			_fileOperPage.Visibility = Visibility.Collapsed;
			UpdateButtonState();
			_tLastAttrValCheck = DateTime.Now;
			_timer = new DispatcherTimer();
			_timer.Interval = TimeSpan.FromMilliseconds(200.0);
			_timer.Tick += OnTimer;
			_timer.Start();
			ConnectStateTimer = new UITimer(500, MonitorChannelStateChange);
			sdkCBHandler = ProcessEvent;
			_fileOperPage.UpdateTitleHandle += UpdateTitle;
			base.LocationChanged += OnLocationChanged;
			PageBtnCtrl = new PageButtonControl(_btnHome);
		}

		private void OnTimer(object sender, EventArgs e)
		{
			_CurrDateTime.Text = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
			DateTime now = DateTime.Now;
			if (now.Subtract(_tLastAttrValCheck) > TimeSpan.FromMilliseconds(1000.0))
			{
				_tLastAttrValCheck = now;
				_detMgr.DoCheckAttrValChanging();
			}
		}

		private void MonitorChannelStateChange()
		{
			DetectorListItem detectorListItem = null;
			foreach (DetectorListItem display in DisplayList)
			{
				if (display.Instance != null && display.IsOccupied)
				{
					Enm_ConnectionState prop_Attr_ConnState = (Enm_ConnectionState)display.Instance.Prop_Attr_ConnState;
					if (display.Instance.ConnectState != prop_Attr_ConnState)
					{
						detectorListItem = display;
						switch (prop_Attr_ConnState)
						{
						case Enm_ConnectionState.Enm_ConnState_LowRate:
						case Enm_ConnectionState.Enm_ConnState_OK:
							if (display.Instance.Prop_Attr_State != 0)
							{
								Log.Instance().Write("channel state changed from {0} to {1}", display.Instance.ConnectState.ToString(), prop_Attr_ConnState.ToString());
								display.Page.AddSdkInfo("Connection is ok");
								DetectorRestoreConnection(ref detectorListItem);
								display.Instance.ConnectState = prop_Attr_ConnState;
							}
							break;
						case Enm_ConnectionState.Enm_ConnState_Unknown:
						case Enm_ConnectionState.Enm_ConnState_HardwareBreak:
						case Enm_ConnectionState.Enm_ConnState_NotConnected:
							Log.Instance().Write("channel state changed from {0} to {1}", display.Instance.ConnectState.ToString(), prop_Attr_ConnState.ToString());
							display.Page.AddSdkInfo("Connection is broken");
							DetectorBreak(ref detectorListItem);
							display.Instance.ConnectState = prop_Attr_ConnState;
							break;
						}
					}
				}
			}
		}

		private bool ShowPage(UIElement page)
		{
			if (_currDetPage != null && _currDetPage.Instance != null && _currDetPage.Instance.Prop_Attr_ConnState == 1 && !page.Equals(_homePage) && !page.Equals(_fileOperPage))
			{
				return false;
			}
			_homePage.Visibility = ((!page.Equals(_homePage)) ? Visibility.Collapsed : Visibility.Visible);
			_fileOperPage.Visibility = ((!page.Equals(_fileOperPage)) ? Visibility.Collapsed : Visibility.Visible);
			if (_currDetPage != null)
			{
				_currDetPage.Visibility = ((!page.Equals(_currDetPage)) ? Visibility.Collapsed : Visibility.Visible);
			}
			base.Title = "iDetector";
			if (_currDetPage != null && _currDetPage.Visibility == Visibility.Visible)
			{
				foreach (DetectorListItem display in DisplayList)
				{
					if (page.Equals(display.Page))
					{
						xDetName.Content = display.BindName.Replace("_", "__");
						break;
					}
				}
			}
			else
			{
				xDetName.Content = "";
			}
			return true;
		}

		private bool ShowSDKPage(UIElement page)
		{
			if (_currDetPage == null)
			{
				return false;
			}
			_homePage.Visibility = Visibility.Collapsed;
			_fileOperPage.Visibility = Visibility.Collapsed;
			_currDetPage.Visibility = Visibility.Visible;
			_fileOperPage.HidePage();
			base.Title = "iDetector";
			foreach (DetectorListItem display in DisplayList)
			{
				if (page.Equals(display.Page))
				{
					xDetName.Content = display.BindName.Replace("_", "__");
					break;
				}
			}
			return true;
		}

		private Button FindButtonByCurPage(UIElement page)
		{
			if (page == _currDetPage._acqPage)
			{
				return _btnAcq;
			}
			if (page == _currDetPage._sdkCfgPage)
			{
				return _btnSdkCfg;
			}
			if (page == _currDetPage._caliPage)
			{
				return _btnDetCali;
			}
			if (page == _currDetPage._caliEx2Page)
			{
				return _btnDetCali;
			}
			if (page == _currDetPage._detCfgPage)
			{
				return _btnDetCfg;
			}
			return null;
		}

		private void _btnHome_Click(object sender, RoutedEventArgs e)
		{
			if (ShowPage(_homePage))
			{
				PageBtnCtrl.LightButton(_btnHome);
			}
		}

		private void _btnFileOper_Click(object sender, RoutedEventArgs e)
		{
			if (ShowPage(_fileOperPage))
			{
				PageBtnCtrl.LightButton(_btnFileOper);
			}
			_fileOperPage.UpdateTitle();
		}

		private void _btnAcq_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null || detectorListItem.Page == null)
			{
				return;
			}
			_currDetPage = detectorListItem.Page;
			if (_currDetPage != null && DetectorState.Ready == detectorListItem.State)
			{
				ShowPage(_currDetPage);
				if (_currDetPage.ShowPage(_currDetPage._acqPage))
				{
					PageBtnCtrl.LightButton(_btnAcq);
				}
				else
				{
					PageBtnCtrl.LightButton(FindButtonByCurPage(_currDetPage.CurSubPage as UIElement));
				}
			}
		}

		private void _btnFactory_Click(object sender, RoutedEventArgs e)
		{
			int selectedIndex = _homePage._listView.SelectedIndex;
			if (-1 == selectedIndex)
			{
				MessageBoxShow("Please select a detector!");
				return;
			}
			DetectorListItem detectorListItem = DisplayList[selectedIndex];
			if (detectorListItem == null)
			{
				MessageBoxShow("Selected detector is empty!");
				return;
			}
			if (detectorListItem.WorkDir.Length == 0)
			{
				MessageBoxShow("Please set a work directory");
				return;
			}
			if (detectorListItem.IsOccupied)
			{
				MessageBoxShow("Please disconnect the detector firstly!");
				return;
			}
			FactoryWnd factoryWnd = new FactoryWnd(detectorListItem.WorkDir);
			factoryWnd.Owner = this;
			factoryWnd.ShowDialog();
		}

		private void _btnSdkCfg_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null || detectorListItem.Page == null)
			{
				return;
			}
			_currDetPage = detectorListItem.Page;
			if (_currDetPage != null)
			{
				ShowSDKPage(_currDetPage);
				if (_currDetPage.ShowPage(_currDetPage._sdkCfgPage))
				{
					PageBtnCtrl.LightButton(_btnSdkCfg);
				}
				else
				{
					PageBtnCtrl.LightButton(FindButtonByCurPage(_currDetPage.CurSubPage as UIElement));
				}
			}
		}

		private void _btnDetCali_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null || detectorListItem.Page == null)
			{
				return;
			}
			_currDetPage = detectorListItem.Page;
			if (_currDetPage != null && DetectorState.Ready == detectorListItem.State)
			{
				ShowPage(_currDetPage);
				if (_currDetPage.ShowPage(_currDetPage._caliEx2Page))
				{
					PageBtnCtrl.LightButton(_btnDetCali);
				}
				else
				{
					PageBtnCtrl.LightButton(FindButtonByCurPage(_currDetPage.CurSubPage as UIElement));
				}
			}
		}

		private void _btnDetCfg_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null || detectorListItem.Page == null)
			{
				return;
			}
			_currDetPage = detectorListItem.Page;
			if (_currDetPage != null && DetectorState.Ready == detectorListItem.State)
			{
				ShowPage(_currDetPage);
				if (_currDetPage.ShowPage(_currDetPage._detCfgPage))
				{
					PageBtnCtrl.LightButton(_btnDetCfg);
				}
				else
				{
					PageBtnCtrl.LightButton(FindButtonByCurPage(_currDetPage.CurSubPage as UIElement));
				}
			}
		}

		private void _btnScan_Click(object sender, RoutedEventArgs e)
		{
			_detMgr.StartScan();
		}

		private void _btnBind_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null)
			{
				detectorListItem = new DetectorListItem();
			}
		}

		private void _btnRemove_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
		}

		private void DisableDetListView()
		{
			_homePage._listView.IsEnabled = false;
			base.Cursor = Cursors.Wait;
			mbCanClosed = false;
		}

		private void EnableDetListView()
		{
			_homePage._listView.IsEnabled = true;
			base.Cursor = Cursors.Arrow;
			mbCanClosed = true;
		}

		private void _btnConnect_Click(object sender, RoutedEventArgs e)
		{
			int SelectedIndex = _homePage._listView.SelectedIndex;
			if (-1 == SelectedIndex)
			{
				MessageBoxShow("Please select a detector!");
				return;
			}
			DetectorListItem item = DisplayList[SelectedIndex];
			if (item == null)
			{
				MessageBoxShow("Selected detector is empty!");
				return;
			}
			if (item.WorkDir.Length == 0)
			{
				MessageBoxShow("Please set a work directory");
				return;
			}
			Log.logPath = item.WorkDir;
			bool flag = false;
			if (item.Instance != null)
			{
				flag = ((item.Instance.Prop_Attr_ConnState == 4 || item.Instance.Prop_Attr_ConnState == 3) && item.Instance.Prop_Attr_State == 1);
			}
			if (item.Instance != null && flag)
			{
				MessageBoxShow("Device has been conneced and instance has been inilitized.");
				Log.Instance().Write("Device has been conneced and instance has been inilitized.");
			}
			else if (item.Instance == null || item.State == DetectorState.Unknown)
			{
				_homePage.UpdateConnectInfo("Connecting...");
				DisableDetListView();
				Thread thread = new Thread((ThreadStart)delegate
				{
					ConnectThread(this, SelectedIndex, item.WorkDir);
				});
				Log.Instance().Write("--------------start---------------");
				thread.Start();
			}
			else
			{
				Log.Instance().Write("Instance had been created and state is {0}", item.State.ToString());
			}
		}

		private void ConnectThread(MainWindow obj, int itemIndex, string path)
		{
			int nDetectorID = 0;
			int nResult = 0;
			base.Dispatcher.Invoke((Action)delegate
			{
				DetectorListItem detectorListItem = DisplayList[itemIndex];
				if (detectorListItem.Instance == null)
				{
					try
					{
						nResult = obj._detMgr.Create(path, ref nDetectorID);
					}
					catch (Exception ex)
					{
						MessageBoxShow("Create failed!" + path + ":" + ex.ToString());
						Log.Instance().Write("Create failed!" + path + ":" + ex.ToString());
						goto IL_0209;
					}
					if (nResult != 0)
					{
						MessageBox.Show(this, "Create detector instance failed!", "iDetector", MessageBoxButton.OK, MessageBoxImage.Exclamation);
						Log.Instance().Write("Create detector instance failed!");
						goto IL_0209;
					}
					detectorListItem.Instance = obj._detMgr.FindDetector(nDetectorID);
					detectorListItem.State = DetectorState.Unknown;
					detectorListItem.Instance.SubscribeCBEvent(obj.OnSdkCallback);
					CurrentDetectorSingleton.Instance().ActiveDetector = detectorListItem.Instance;
					Log.Instance().Write("Main window add sdkcallback.");
				}
				nResult = SdkInterface.Invoke(detectorListItem.Instance.ID, 2, null, 0);
				if (nResult == 1)
				{
					TerminateConnectTimer();
					taskTerminate = new MonitorTask(300000, ConnectFailed);
					taskTerminate.RunTask();
					Log.Instance().Write("Start connection-monitor-timer. HasCode:{0}", taskTerminate.GetHashCode());
					DetPage page = new DetPage
					{
						Instance = detectorListItem.Instance,
						Visibility = Visibility.Collapsed
					};
					detectorListItem.Page = page;
					AddPage(ref page);
					return;
				}
				if (nResult == 0)
				{
					return;
				}
				Log.Instance().Write("Connect to device failed!Err={0}", ErrorHelper.GetErrorDesp(nResult));
				goto IL_0209;
				IL_0209:
				EnableDetListView();
				_homePage.UpdateConnectInfo("Connect failed!");
			}, new object[0]);
		}

		private void ConnectFailed()
		{
			TerminateConnectTimer();
			base.Dispatcher.Invoke((Action)delegate
			{
				EnableDetListView();
				_homePage.UpdateConnectInfo("Connect failed! Timeout");
				if (taskTerminate != null)
				{
					Log.Instance().Write("Connect failed! Timeout! Hascode:{0}", taskTerminate.GetHashCode());
				}
			}, new object[0]);
		}

		private void DisconnectFailed()
		{
			base.Dispatcher.Invoke((Action)delegate
			{
				_homePage.UpdateConnectInfo("Disconnect failed! Timeout");
				Log.Instance().Write("Disconnect failed! Timeout");
			}, new object[0]);
		}

		private void TerminateConnectTimer()
		{
			if (taskTerminate != null)
			{
				taskTerminate.StopTask();
				Log.Instance().Write("Stop connection-monitor-timer. HasCode:{0}", taskTerminate.GetHashCode());
				taskTerminate = null;
			}
		}

		private void _btnClose_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null)
			{
				if (sender != null)
				{
					MessageBoxShow("Please select a detector!");
				}
			}
			else if (detectorListItem.Instance != null)
			{
				MonitorTask monitorTask = new MonitorTask(40000, DisconnectFailed);
				monitorTask.RunTask();
				int num = _detMgr.Destroy(detectorListItem.Instance.ID);
				monitorTask.StopTask();
				EnableDetListView();
				if (num != 0)
				{
					Log.Instance().Write("Close failed! Err:" + ErrorHelper.GetErrorDesp(num));
					MessageBoxShow("Close failed! Err:" + ErrorHelper.GetErrorDesp(num));
					return;
				}
				CloseUserControlPage(detectorListItem.Page);
				base.Cursor = Cursors.Wait;
				CurrentDetectorSingleton.Instance().ActiveDetector = null;
				detectorListItem.curConnInfo = "Disconnect Succeed";
				_homePage.UpdateConnectInfo(detectorListItem.curConnInfo);
				Log.Instance().Write(string.Format("Detector[{0}] Disconnect Succeed!", detectorListItem.Instance.ID));
				Log.Instance().Write("--------------end---------------");
				detectorListItem.Instance = null;
				detectorListItem.State = DetectorState.Bind;
				detectorListItem.IsOccupied = false;
				RemovePage(detectorListItem.Page);
				detectorListItem.Page = null;
				GC.Collect();
				base.Cursor = Cursors.Arrow;
				UpdateButtonState();
			}
		}

		private void _btnDisplay_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null || detectorListItem.Instance == null || DetectorState.Ready != detectorListItem.State)
			{
				return;
			}
			DetPage page = detectorListItem.Page;
			if (page != null)
			{
				_currDetPage = page;
				if (ShowPage(_currDetPage))
				{
					PageBtnCtrl.LightButton(FindButtonByCurPage(_currDetPage.CurSubPage as UIElement));
				}
			}
		}

		private void _btnAddWorkDir_Click(object sender, RoutedEventArgs e)
		{
			string defaultWorkDir = ".";
			if (DisplayList.Count > 0)
			{
				defaultWorkDir = DisplayList.First().WorkDir;
				defaultWorkDir = defaultWorkDir.Substring(0, defaultWorkDir.LastIndexOf("\\"));
			}
			AddWorkDirWnd addWorkDirWnd = new AddWorkDirWnd(0, defaultWorkDir, "");
			addWorkDirWnd.AddBindItem += AddDetectorItem;
			addWorkDirWnd.Owner = this;
			addWorkDirWnd.ShowDialog();
		}

		private void _btnDelWorkDir_Click(object sender, RoutedEventArgs e)
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == null)
			{
				MessageBoxShow("Please select a detector!");
				return;
			}
			if (detectorListItem.IsOccupied)
			{
				MessageBoxShow("In used, cannot be deleted!");
				return;
			}
			DelWorkDirWnd delWorkDirWnd = new DelWorkDirWnd();
			delWorkDirWnd.Owner = this;
			delWorkDirWnd.ShowDialog();
			if (delWorkDirWnd.DelBind)
			{
				DisplayList.Remove(detectorListItem);
				foreach (DetectorBind bind in _bindRecords.BindList)
				{
					if (bind.strBindName.Equals(detectorListItem.BindName) && Utility.IsSameDirPath(bind.strWorkDir, detectorListItem.WorkDir) && bind.strProductType.Equals(detectorListItem.ProductType))
					{
						_bindRecords.BindList.Remove(bind);
						_bindRecords.Save();
						if (detectorListItem.Instance != null)
						{
							base.Cursor = Cursors.Wait;
							_detMgr.Destroy(detectorListItem.Instance.ID);
							RemovePage(detectorListItem.Page);
							base.Cursor = Cursors.Arrow;
						}
						break;
					}
				}
				if (delWorkDirWnd.DelDirectorty)
				{
					Utility.DeleteDirectory(detectorListItem.WorkDir);
				}
			}
		}

		private void _homePage_SelectDetectorItem(object sender, SelectionChangedEventArgs e)
		{
			UpdateButtonState();
		}

		private void _homePage_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			IInputElement directlyOver = Mouse.DirectlyOver;
			if (!(directlyOver is ScrollViewer))
			{
				_btnDisplay_Click(sender, e);
			}
		}

		private void OnScanResult(object sender, EventArgs e)
		{
		}

		private void AddPage(ref DetPage page)
		{
			try
			{
				foreach (UserControl child in _gridPageContainer.Children)
				{
					DetPage detPage = child as DetPage;
					if (detPage != null && detPage.Instance.ID == page.Instance.ID)
					{
						detPage.Release();
						_gridPageContainer.Children.Remove(detPage);
						break;
					}
				}
			}
			catch
			{
			}
			_gridPageContainer.Children.Add(page);
			_currDetPage = page;
			DetPage currDetPage = _currDetPage;
			currDetPage.ShowToolTip = (ToolTipMsgDel)Delegate.Combine(currDetPage.ShowToolTip, new ToolTipMsgDel(ToolTipMessage));
			AcquirePage acqPage = _currDetPage._acqPage;
			acqPage.ShowToolTip = (ToolTipMsgDel)Delegate.Combine(acqPage.ShowToolTip, new ToolTipMsgDel(ToolTipMessage));
			CalibratePage caliPage = _currDetPage._caliPage;
			caliPage.ShowToolTip = (ToolTipMsgDel)Delegate.Combine(caliPage.ShowToolTip, new ToolTipMsgDel(ToolTipMessage));
		}

		private void RemovePage(DetPage page)
		{
			if (page != null)
			{
				if (_currDetPage != null)
				{
					DetPage currDetPage = _currDetPage;
					currDetPage.ShowToolTip = (ToolTipMsgDel)Delegate.Remove(currDetPage.ShowToolTip, new ToolTipMsgDel(ToolTipMessage));
					AcquirePage acqPage = _currDetPage._acqPage;
					acqPage.ShowToolTip = (ToolTipMsgDel)Delegate.Remove(acqPage.ShowToolTip, new ToolTipMsgDel(ToolTipMessage));
					CalibratePage caliPage = _currDetPage._caliPage;
					caliPage.ShowToolTip = (ToolTipMsgDel)Delegate.Remove(caliPage.ShowToolTip, new ToolTipMsgDel(ToolTipMessage));
				}
				GC.Collect();
			}
		}

		private void RefreshDisplayList()
		{
			DisplayList.Clear();
			List<DetectorBind> list = new List<DetectorBind>();
			foreach (DetectorBind bind in _bindRecords.BindList)
			{
				DetectorListItem detectorListItem = new DetectorListItem();
				detectorListItem.BindName = bind.strBindName;
				detectorListItem.SN = bind.strSN;
				detectorListItem.ProductType = bind.strProductType;
				try
				{
					detectorListItem.WorkDir = Path.GetFullPath(bind.strWorkDir);
				}
				catch
				{
					list.Add(bind);
					continue;
				}
				detectorListItem.State = DetectorState.Bind;
				detectorListItem.IsOccupied = false;
				detectorListItem.curConnInfo = "";
				DisplayList.Add(detectorListItem);
			}
			foreach (DetectorBind item in list)
			{
				_bindRecords.BindList.Remove(item);
			}
		}

		private void AddNewWorkDir(int productNo, string workDir, string serialNo)
		{
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				_btnClose_Click(_homePage._btnClose, null);
				AddWorkDirWnd addWorkDirWnd = new AddWorkDirWnd(productNo, workDir, serialNo);
				addWorkDirWnd.AddBindItem += AddDetectorItem;
				addWorkDirWnd.Owner = this;
				addWorkDirWnd.ShowDialog();
			});
		}

		private void AddDetectorItem(DetectorBind newItem)
		{
			foreach (DetectorBind bind in _bindRecords.BindList)
			{
				if (bind.strBindName == newItem.strBindName && bind.strSN == newItem.strSN && bind.strProductType == newItem.strProductType && Utility.IsSameDirPath(bind.strWorkDir, newItem.strWorkDir))
				{
					return;
				}
			}
			DetectorListItem detectorListItem = new DetectorListItem();
			detectorListItem.BindName = newItem.strBindName;
			detectorListItem.SN = newItem.strSN;
			detectorListItem.ProductType = newItem.strProductType;
			detectorListItem.WorkDir = newItem.strWorkDir;
			detectorListItem.State = DetectorState.Bind;
			detectorListItem.IsOccupied = false;
			detectorListItem.curConnInfo = "";
			DisplayList.Add(detectorListItem);
			_bindRecords.BindList.Add(newItem);
			_bindRecords.Save();
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			if (!mbCanClosed)
			{
				e.Cancel = true;
				return;
			}
			ConnectStateTimer.Stop();
			_timer.Stop();
			if (_fileOperPage != null)
			{
				_fileOperPage.ClosePage();
			}
			if (mTipWnd != null && mTipWnd.IsVisible)
			{
				mTipWnd.Close();
			}
			foreach (DetectorListItem item in (IEnumerable)_homePage._listView.Items)
			{
				if (item.Page != null)
				{
					item.Instance.UnSubscribeCBEvent(OnSdkCallback);
					item.Page.ClosePage();
				}
			}
			base.OnClosing(e);
			Log.Instance().Write("Closing the app.");
		}

		private void OnClosed(object sender, EventArgs e)
		{
			PlugInsManager.Destory();
			CurrentDetectorSingleton.Instance().ActiveDetector = null;
			foreach (DetectorListItem item in (IEnumerable)_homePage._listView.Items)
			{
				if (item != null && item.Instance != null)
				{
					_detMgr.Destroy(item.Instance.ID);
				}
			}
			Log.Instance().Write("Closed the app.");
		}

		private void CloseUserControlPage(DetPage curDetPage)
		{
			if (curDetPage != null)
			{
				curDetPage.Instance.UnSubscribeCBEvent(OnSdkCallback);
				Log.Instance().Write("Main window remove sdkcallback.");
				curDetPage.ClosePage();
				curDetPage = null;
			}
			if (mTipWnd != null && mTipWnd.IsVisible)
			{
				mTipWnd.Close();
				mTipWnd = null;
			}
		}

		private void UpdateButtonState()
		{
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem != null)
			{
				_homePage.UpdateConnectInfo(detectorListItem.curConnInfo);
				_homePage._btnConnect.IsEnabled = !detectorListItem.IsOccupied;
				_homePage._btnClose.IsEnabled = detectorListItem.IsOccupied;
				if (detectorListItem.Instance != null && DetectorState.Ready == detectorListItem.State)
				{
					CurrentDetectorSingleton.Instance().ActiveDetector = detectorListItem.Instance;
				}
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (2001 == nEventID || 4 == nEventID || 5 == nEventID)
			{
				base.Dispatcher.BeginInvoke(sdkCBHandler, sender, nEventID, nParam1, nParam2, strMsg);
			}
		}

		private void ProcessEvent(Detector sender, int nEventID, int nParam1, int nParam2, string message)
		{
			DetectorListItem listItem = GetListItem(sender);
			if (listItem == null)
			{
				return;
			}
			switch (nEventID)
			{
			case 2001:
				_homePage.UpdateConnectInfo(message);
				break;
			case 4:
				switch (nParam1)
				{
				case 2:
					TerminateConnectTimer();
					if (listItem.State != DetectorState.Ready)
					{
						ConnectStateTimer.Start();
						Log.Instance().Write(string.Format("Detector[{0}] Connect succeed. name:{1}, dir:{2}", sender.ID, listItem.BindName, listItem.WorkDir));
						listItem.curConnInfo = "Connect Succeed!";
						_homePage.UpdateConnectInfo(listItem.curConnInfo);
						listItem.State = DetectorState.Ready;
						listItem.IsOccupied = true;
						listItem.SN = sender.Prop_Attr_UROM_SerialNo;
						_currDetPage.InitSubPageParam();
						_btnAcq_Click(null, null);
						sender.ConnectState = Enm_ConnectionState.Enm_ConnState_OK;
						UpdateButtonState();
						EnableDetListView();
						AddToolTip();
						if (_currDetPage != null && _currDetPage._caliEx2Page != null && _currDetPage._caliEx2Page._preparePage.GetCorrectBreakError())
						{
							CurrentDetectorSingleton.Instance().ActiveDetector.Abort();
						}
					}
					break;
				case 3:
					sender.UnSubscribeCBEvent(OnSdkCallback);
					break;
				}
				break;
			case 5:
				if (nParam1 != 2)
				{
					break;
				}
				TerminateConnectTimer();
				if (listItem.State != DetectorState.Ready)
				{
					switch (nParam2)
					{
					case 24:
						listItem.curConnInfo = "FPD no response!";
						break;
					case 1007:
						listItem.curConnInfo = "FPD busy!";
						break;
					case 23:
						listItem.curConnInfo = "Product info doesn't match!";
						break;
					case 30:
						listItem.curConnInfo = "Image Chanel isn't ok!";
						break;
					case 34:
						listItem.curConnInfo = "Cannot find device!";
						break;
					case 35:
						listItem.curConnInfo = "Device is beeing occupied!";
						break;
					case 36:
						listItem.curConnInfo = "Param error, please check IP address!";
						break;
					case 33:
					{
						listItem.curConnInfo = "Current detector's SN doesn't match with the config!";
						string workDir = listItem.WorkDir.Substring(0, listItem.WorkDir.LastIndexOf("\\"));
						AddNewWorkDir(sender.Prop_Attr_UROM_ProductNo, workDir, sender.Prop_Attr_UROM_SerialNo);
						break;
					}
					default:
						listItem.curConnInfo = "Connect failed! " + ErrorHelper.GetErrorDesp(nParam2);
						break;
					}
					if (listItem == _homePage._listView.SelectedItem as DetectorListItem)
					{
						_homePage.UpdateConnectInfo(listItem.curConnInfo);
					}
					EnableDetListView();
					RemovePage(listItem.Page);
				}
				break;
			}
		}

		private void DetectorBreak(ref DetectorListItem item)
		{
			item.State = DetectorState.Unknown;
			item.curConnInfo = "Offline";
			if (_currDetPage == item.Page)
			{
				ShowPage(_homePage);
				PageBtnCtrl.LightButton(_btnHome);
				if (item.State != DetectorState.Ready)
				{
					EnableDetListView();
				}
			}
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == item)
			{
				_homePage.UpdateConnectInfo(item.curConnInfo);
			}
		}

		private void DetectorRestoreConnection(ref DetectorListItem item)
		{
			item.State = DetectorState.Ready;
			item.curConnInfo = "Connect Succeed!";
			item.SN = item.Instance.Prop_Attr_UROM_SerialNo;
			DetectorListItem detectorListItem = _homePage._listView.SelectedItem as DetectorListItem;
			if (detectorListItem == item)
			{
				_homePage.UpdateConnectInfo(item.curConnInfo);
			}
			if (_currDetPage != null)
			{
				_currDetPage._acqPage.NotifyReconnected();
				_currDetPage._detCfgPage.NotifyReconnected();
				_currDetPage._caliPage.NotifyReconnected();
			}
		}

		private DetectorListItem GetListItem(Detector dector)
		{
			DetectorListItem result = null;
			foreach (DetectorListItem display in DisplayList)
			{
				if (display.Instance != null && display.Instance.Equals(dector))
				{
					return display;
				}
			}
			return result;
		}

		private void UpdateTitle(string title)
		{
			base.Title = title;
		}

		private void MessageBoxShow(string content)
		{
			MessageBox.Show(this, content);
		}

		private void AddToolTip()
		{
			if (mTipWnd == null)
			{
				mTipWnd = new ToolTip
				{
					VerticalAlignment = VerticalAlignment.Bottom,
					HorizontalAlignment = HorizontalAlignment.Right
				};
				_gridPageContainer.Children.Add(mTipWnd);
			}
			else
			{
				_gridPageContainer.Children.Remove(mTipWnd);
				_gridPageContainer.Children.Add(mTipWnd);
			}
		}

		private void RemoveToolTip()
		{
			ToolTip mTipWnd2 = mTipWnd;
		}

		private void ToolTipMessage(string msg)
		{
			if (mTipWnd != null)
			{
				mTipWnd.Close();
				mTipWnd.ShowTip(msg);
			}
		}

		private void SetToolTipPosition()
		{
		}

		protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
		{
			SetToolTipPosition();
			base.OnRenderSizeChanged(sizeInfo);
		}

		private void OnLocationChanged(object sender, EventArgs e)
		{
			SetToolTipPosition();
			if (_currDetPage != null)
			{
				_currDetPage.OnLocationChanged();
			}
			if (_fileOperPage != null)
			{
				_fileOperPage.OnLocationChanged();
			}
		}

		private void _btnSyncbox_Click(object sender, RoutedEventArgs e)
		{
			if (_syncboxWnd == null)
			{
				_syncboxWnd = new SyncboxWnd();
			}
			if (_deteInfoList == null)
			{
				_deteInfoList = new List<DeteBindInfo>();
			}
			_deteInfoList.Clear();
			foreach (DetectorListItem display in DisplayList)
			{
				if (display.Instance != null && DetectorState.Ready == display.State)
				{
					DeteBindInfo item = default(DeteBindInfo);
					item.strBindName = display.BindName;
					item.nDetectorID = display.Instance.ID;
					_deteInfoList.Add(item);
				}
			}
			_syncboxWnd.SetBindInfo(_deteInfoList);
			_syncboxWnd.Owner = this;
			_syncboxWnd.ShowDialog();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/mainwindow.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((MainWindow)target).Closed += OnClosed;
				break;
			case 2:
				_CurrDateTime = (TextBlock)target;
				break;
			case 3:
				xDetName = (Label)target;
				break;
			case 4:
				_gridPageContainer = (Grid)target;
				break;
			case 5:
				_homePage = (HomePage)target;
				break;
			case 6:
				_fileOperPage = (FileOperPage)target;
				break;
			case 7:
				_btnHome = (Button)target;
				_btnHome.Click += _btnHome_Click;
				break;
			case 8:
				_btnAcq = (Button)target;
				_btnAcq.Click += _btnAcq_Click;
				break;
			case 9:
				_btnFactory = (Button)target;
				_btnFactory.Click += _btnFactory_Click;
				break;
			case 10:
				_btnSdkCfg = (Button)target;
				_btnSdkCfg.Click += _btnSdkCfg_Click;
				break;
			case 11:
				_btnDetCfg = (Button)target;
				_btnDetCfg.Click += _btnDetCfg_Click;
				break;
			case 12:
				_btnDetCali = (Button)target;
				_btnDetCali.Click += _btnDetCali_Click;
				break;
			case 13:
				_btnFileOper = (Button)target;
				_btnFileOper.Click += _btnFileOper_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
