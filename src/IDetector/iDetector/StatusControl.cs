namespace iDetector
{
	public abstract class StatusControl
	{
		public bool IsSequenceAcq
		{
			get;
			set;
		}

		public bool EnableUpdateImagelist
		{
			get;
			set;
		}

		public virtual void UpdateStatusAfterStartAcq()
		{
		}

		public void UpdateStatusAfterSeqAcq()
		{
			IsSequenceAcq = true;
			EnableUpdateImagelist = false;
		}

		public void UpdateStatusAfterStopAcq()
		{
			IsSequenceAcq = false;
			EnableUpdateImagelist = false;
		}
	}
}
