namespace iDetector
{
	public enum PresetOffsetOption
	{
		SWPreOffset = 0x10000,
		SWPostOffset = 0x20000,
		HWPreOffset = 1,
		HWPostOffset = 2,
		None = 0
	}
}
