namespace iDetector
{
	public struct OQCSeqSaveParam
	{
		public bool EnableSave;

		public int TotalFrames;

		public int SavedFrames;

		public string Path;
	}
}
