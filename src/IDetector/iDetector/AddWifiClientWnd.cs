using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class AddWifiClientWnd : Window, IComponentConnector
	{
		internal TextBox xSSID;

		internal TextBox xKey;

		private bool _contentLoaded;

		public string SSID
		{
			get
			{
				return xSSID.Text;
			}
			set
			{
				xSSID.Text = value;
			}
		}

		public string Key
		{
			get
			{
				return xKey.Text;
			}
			set
			{
				xKey.Text = value;
			}
		}

		public bool Result
		{
			get;
			set;
		}

		public AddWifiClientWnd()
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			DesignerProperties.GetIsInDesignMode(this);
		}

		public AddWifiClientWnd(string ssid, string key)
			: this()
		{
			SSID = ssid;
			Key = key;
		}

		private void OnApply(object sender, RoutedEventArgs e)
		{
			if (SSID.Length == 0)
			{
				MessageBox.Show("SSID cannot be empty!");
				return;
			}
			if (Key.Length < 8)
			{
				MessageBox.Show("The length of Key must be greater than or equal to 8 bytes!");
				return;
			}
			Result = true;
			Close();
		}

		private void OnCancel(object sender, RoutedEventArgs e)
		{
			Result = false;
			Close();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/addwificlientwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xSSID = (TextBox)target;
				break;
			case 2:
				xKey = (TextBox)target;
				break;
			case 3:
				((Button)target).Click += OnApply;
				break;
			case 4:
				((Button)target).Click += OnCancel;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
