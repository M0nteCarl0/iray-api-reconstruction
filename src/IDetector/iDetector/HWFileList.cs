using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class HWFileList : UserControl, IComponentConnector
	{
		private Enm_FileTypes fileType;

		internal ListView xListView;

		internal GridView xView;

		internal GridViewColumn xIndex;

		internal GridViewColumn xPGA;

		internal GridViewColumn xFullWell;

		internal GridViewColumn xBinning;

		internal GridViewColumn xZoom;

		internal GridViewColumn xROIRange;

		internal GridViewColumn xActivity;

		internal GridViewColumn xValidity;

		internal GridViewColumn xDesp;

		private bool _contentLoaded;

		public Enm_FileTypes FileType
		{
			get
			{
				return fileType;
			}
		}

		public FPDCorrFileList FileList
		{
			get
			{
				return xListView.SelectedItem as FPDCorrFileList;
			}
		}

		public ItemCollection ItemCollection
		{
			get
			{
				return xListView.Items;
			}
		}

		public HWFileList(int productID, Enm_FileTypes _fileType)
		{
			InitializeComponent();
			fileType = _fileType;
			InitPanel(productID);
		}

		private void InitPanel(int productID)
		{
			switch (productID)
			{
			case 6:
			case 49:
			case 50:
			case 56:
				xView.Columns.Remove(xZoom);
				break;
			case 46:
			case 87:
				xView.Columns.Remove(xPGA);
				xView.Columns.Remove(xZoom);
				break;
			case 32:
			case 37:
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 59:
			case 60:
			case 61:
			case 62:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				xView.Columns.Remove(xPGA);
				xView.Columns.Remove(xBinning);
				xView.Columns.Remove(xZoom);
				xView.Columns.Remove(xValidity);
				xView.Columns.Remove(xDesp);
				break;
			}
			if (productID != 46)
			{
				xView.Columns.Remove(xFullWell);
				xView.Columns.Remove(xROIRange);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/usercontrol/hwfilelist.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xListView = (ListView)target;
				break;
			case 2:
				xView = (GridView)target;
				break;
			case 3:
				xIndex = (GridViewColumn)target;
				break;
			case 4:
				xPGA = (GridViewColumn)target;
				break;
			case 5:
				xFullWell = (GridViewColumn)target;
				break;
			case 6:
				xBinning = (GridViewColumn)target;
				break;
			case 7:
				xZoom = (GridViewColumn)target;
				break;
			case 8:
				xROIRange = (GridViewColumn)target;
				break;
			case 9:
				xActivity = (GridViewColumn)target;
				break;
			case 10:
				xValidity = (GridViewColumn)target;
				break;
			case 11:
				xDesp = (GridViewColumn)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
