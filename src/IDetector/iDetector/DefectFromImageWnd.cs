using Microsoft.Win32;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace iDetector
{
	public class DefectFromImageWnd : Window, IComponentConnector
	{
		private DefectMapInfo defectInfo;

		private readonly int[,] imageSize = new int[7, 2]
		{
			{
				3072,
				3072
			},
			{
				2448,
				2048
			},
			{
				2342,
				2840
			},
			{
				2816,
				3528
			},
			{
				2304,
				2800
			},
			{
				1024,
				1024
			},
			{
				512,
				512
			}
		};

		private int nWidth;

		private int nHeight;

		private int nLength;

		private byte[] imageData;

		private byte[] imageBkData;

		private Point CurPos;

		private int nImgPosX;

		private int nImgPosY;

		private byte[] imgDefectMapData;

		private bool bIsGetImgData;

		internal CtrImagePresent ImageDefectViewCtrl;

		internal TextBox txtPath;

		internal Button btnOpenFile;

		internal DigitalTextBox txtDummyTop;

		internal DigitalTextBox txtDummyBottom;

		internal DigitalTextBox txtDummyLeft;

		internal DigitalTextBox txtDummyRight;

		internal Button btnAutoTagging;

		internal ListView listviewDefect;

		internal Button btnConfirm;

		private bool _contentLoaded;

		public DefectFromImageWnd(DefectMapInfo info)
		{
			InitializeComponent();
			base.Owner = Application.Current.MainWindow;
			defectInfo = info;
			nWidth = defectInfo.nWidth;
			nHeight = defectInfo.nHeight;
			nLength = nWidth * nHeight;
			imageData = new byte[nLength * 2];
			imageBkData = new byte[nLength * 2];
			imgDefectMapData = new byte[nLength];
			ImageDefectViewCtrl.ShowImageListView(false);
			ImageDefectViewCtrl.CollapsedOperationImageButton(true);
			btnConfirm.IsEnabled = false;
			btnAutoTagging.IsEnabled = false;
			listviewDefect.Visibility = Visibility.Collapsed;
			bIsGetImgData = false;
		}

		private void GetRawWidthAndHeight(long fileSize, ref int width, ref int height)
		{
			int num = imageSize.Length / imageSize.Rank;
			int num2 = 0;
			while (true)
			{
				if (num2 < num)
				{
					int num3 = imageSize[num2, 0] * imageSize[num2, 1] * 2;
					if (0 == fileSize % num3)
					{
						break;
					}
					num2++;
					continue;
				}
				return;
			}
			width = imageSize[num2, 0];
			height = imageSize[num2, 1];
		}

		private void btnOpenFile_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "Files(*.dcm; *.raw;)|*.dcm; *.raw;)";
			if (openFileDialog.ShowDialog() == false)
			{
				return;
			}
			string fileName = openFileDialog.FileName;
			string text = openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf(".") + 1, openFileDialog.FileName.Length - openFileDialog.FileName.LastIndexOf(".") - 1);
			ImageDefectViewCtrl.StartTimer();
			if (text.ToLower().Equals("raw"))
			{
				int width = 0;
				int height = 0;
				long fileSize = Utility.GetFileSize(openFileDialog.FileName);
				if (0 == fileSize)
				{
					MessageBox.Show("Cannot open the file or file is incomplete!");
					return;
				}
				GetRawWidthAndHeight(fileSize, ref width, ref height);
				if (width != nWidth || height != nHeight)
				{
					MessageBox.Show("The image size and the defect template is not matching!");
					return;
				}
			}
			else
			{
				if (!text.ToLower().Equals("dcm"))
				{
					MessageBox.Show("Not support this file!");
					return;
				}
				DicomInfo dicomInfo = default(DicomInfo);
				if (IDicom.DCM_LoadDicomFileInfo(fileName, ref dicomInfo) == 0 && (dicomInfo.nWidth != nWidth || dicomInfo.nHeight != nHeight))
				{
					MessageBox.Show("The image size and the defect template is not matching!");
					return;
				}
			}
			if (ImageDefectViewCtrl.StartPlayCine(openFileDialog.FileName, nWidth, nHeight))
			{
				txtPath.Text = openFileDialog.FileName;
				base.Title = "DefectFromImageWnd: [" + openFileDialog.FileName + "]";
				btnConfirm.IsEnabled = false;
				btnAutoTagging.IsEnabled = true;
				bIsGetImgData = false;
				ImageDefectViewCtrl.EnablePlayBarVisibilityChange(true);
				Array.Clear(imgDefectMapData, 0, imgDefectMapData.Length);
			}
			else
			{
				MessageBox.Show("Cannot open the file or file is incomplete!");
			}
		}

		private void SetPoint(byte value)
		{
			int num = nImgPosX;
			int num2 = nImgPosY;
			if (num < 0 || num >= nWidth || num2 < 0 || num2 >= nHeight)
			{
				MessageBox.Show("location is out of range!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}
			if (!bIsGetImgData)
			{
				ushort[] oriGray16ImgBuffer = ImageDefectViewCtrl.ImgView.OriGray16ImgBuffer;
				Buffer.BlockCopy(oriGray16ImgBuffer, 0, imageBkData, 0, nLength * 2);
				bIsGetImgData = true;
				ImageDefectViewCtrl.EnablePlayBarVisibilityChange(false);
			}
			if (imgDefectMapData[num2 * nWidth + num] == byte.MaxValue)
			{
				imgDefectMapData[num2 * nWidth + num] = 0;
			}
			else
			{
				imgDefectMapData[num2 * nWidth + num] = byte.MaxValue;
			}
			Buffer.BlockCopy(imageBkData, 0, imageData, 0, nLength * 2);
			for (int i = 0; i < nHeight; i++)
			{
				for (int j = 0; j < nWidth; j++)
				{
					if (imgDefectMapData[i * nWidth + j] == byte.MaxValue)
					{
						imageData[i * 2 * nWidth + j * 2] = byte.MaxValue;
						imageData[i * 2 * nWidth + j * 2 + 1] = byte.MaxValue;
					}
				}
			}
			ImageDefectViewCtrl.DisplayImage(imageData, nWidth, nHeight);
			btnConfirm.IsEnabled = true;
		}

		private void btnConfirm_Click(object sender, RoutedEventArgs e)
		{
			for (int i = 0; i < nHeight; i++)
			{
				for (int j = 0; j < nWidth; j++)
				{
					if (imgDefectMapData[i * nWidth + j] == byte.MaxValue)
					{
						defectInfo.defectMapData[i * nWidth + j] = imgDefectMapData[i * nWidth + j];
					}
				}
			}
			base.DialogResult = true;
			Close();
		}

		protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
		{
			Image image = e.OriginalSource as Image;
			if (image != null)
			{
				Point position = Mouse.GetPosition(ImageDefectViewCtrl);
				CurPos.X = position.X;
				CurPos.Y = position.Y;
			}
			base.OnMouseRightButtonDown(e);
		}

		protected override void OnMouseRightButtonUp(MouseButtonEventArgs e)
		{
			Point position = Mouse.GetPosition(ImageDefectViewCtrl);
			if (CurPos.X == position.X && CurPos.Y == position.Y)
			{
				GetImagePosition();
				SetPoint(byte.MaxValue);
			}
			base.OnMouseRightButtonUp(e);
		}

		private bool GetImagePosition()
		{
			nImgPosX = ImageDefectViewCtrl.ImgView.XPos;
			nImgPosY = ImageDefectViewCtrl.ImgView.YPos;
			return true;
		}

		private void btnAutoTagging_Click(object sender, RoutedEventArgs e)
		{
			ushort num = (ushort)Convert.ToSingle(txtDummyTop.Text);
			ushort num2 = (ushort)Convert.ToSingle(txtDummyBottom.Text);
			ushort num3 = (ushort)Convert.ToSingle(txtDummyLeft.Text);
			ushort num4 = (ushort)Convert.ToSingle(txtDummyRight.Text);
			if (num3 > 32 || num4 > 32 || num > 32 || num2 > 32)
			{
				MessageBox.Show("The dummy can not be greater than 32!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
				return;
			}
			if (!bIsGetImgData)
			{
				ushort[] oriGray16ImgBuffer = ImageDefectViewCtrl.ImgView.OriGray16ImgBuffer;
				Buffer.BlockCopy(oriGray16ImgBuffer, 0, imageBkData, 0, nLength * 2);
				bIsGetImgData = true;
				ImageDefectViewCtrl.EnablePlayBarVisibilityChange(false);
			}
			IntPtr pRows = default(IntPtr);
			IntPtr pCols = default(IntPtr);
			IntPtr intPtr = Marshal.AllocHGlobal(nLength * 2);
			Marshal.Copy(imageBkData, 0, intPtr, nLength * 2);
			IntPtr intPtr2 = Marshal.AllocHGlobal(nLength);
			if (SdkInterface.CalcDefectWithImageData(intPtr, (ushort)nWidth, (ushort)nHeight, num, num2, num3, num4, intPtr2, pRows, pCols) == 0)
			{
				byte[] array = new byte[nLength];
				Marshal.Copy(intPtr2, array, 0, nLength);
				for (int i = 0; i < nHeight; i++)
				{
					for (int j = 0; j < nWidth; j++)
					{
						if (array[i * nWidth + j] != 0)
						{
							imgDefectMapData[i * nWidth + j] = byte.MaxValue;
						}
					}
				}
				Buffer.BlockCopy(imageBkData, 0, imageData, 0, nLength * 2);
				for (int k = 0; k < nHeight; k++)
				{
					for (int l = 0; l < nWidth; l++)
					{
						if (imgDefectMapData[k * nWidth + l] == byte.MaxValue)
						{
							imageData[k * 2 * nWidth + l * 2] = byte.MaxValue;
							imageData[k * 2 * nWidth + l * 2 + 1] = byte.MaxValue;
						}
					}
				}
				ImageDefectViewCtrl.DisplayImage(imageData, nWidth, nHeight);
				btnConfirm.IsEnabled = true;
				btnAutoTagging.IsEnabled = false;
			}
			else
			{
				MessageBox.Show("Auto tagging failed!", "warning", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			}
			Marshal.FreeHGlobal(intPtr);
			Marshal.FreeHGlobal(intPtr2);
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			ImageDefectViewCtrl.OnClose();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/defectfromimagewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((DefectFromImageWnd)target).Closing += Window_Closing;
				break;
			case 2:
				ImageDefectViewCtrl = (CtrImagePresent)target;
				break;
			case 3:
				txtPath = (TextBox)target;
				break;
			case 4:
				btnOpenFile = (Button)target;
				btnOpenFile.Click += btnOpenFile_Click;
				break;
			case 5:
				txtDummyTop = (DigitalTextBox)target;
				break;
			case 6:
				txtDummyBottom = (DigitalTextBox)target;
				break;
			case 7:
				txtDummyLeft = (DigitalTextBox)target;
				break;
			case 8:
				txtDummyRight = (DigitalTextBox)target;
				break;
			case 9:
				btnAutoTagging = (Button)target;
				btnAutoTagging.Click += btnAutoTagging_Click;
				break;
			case 10:
				listviewDefect = (ListView)target;
				break;
			case 11:
				btnConfirm = (Button)target;
				btnConfirm.Click += btnConfirm_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
