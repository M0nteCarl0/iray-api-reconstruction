using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class BinningZoomSetWnd : Window, IComponentConnector
	{
		public bool Result;

		private Detector _det;

		internal ComboBox _comboBinning;

		internal ComboBox _comboZoom;

		private bool _contentLoaded;

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
			}
		}

		public BinningZoomSetWnd(Detector det)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			if (DesignerProperties.GetIsInDesignMode(this))
			{
				return;
			}
			_det = det;
			int pCount = 0;
			SdkInterface.GetEnumItemsCount("Enm_Binning", ref pCount);
			EnumItem[] arr = new EnumItem[pCount];
			IntPtr intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
			SdkInterface.GetEnumItemList("Enm_Binning", intPtr, pCount);
			SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref arr);
			Marshal.FreeHGlobal(intPtr);
			EnumDisplayItem selectedItem = null;
			EnumItem[] array = arr;
			for (int i = 0; i < array.Length; i++)
			{
				EnumItem enumItem = array[i];
				EnumDisplayItem enumDisplayItem = new EnumDisplayItem
				{
					Val = enumItem.nVal,
					Label = enumItem.strName
				};
				_comboBinning.Items.Add(enumDisplayItem);
				if (enumItem.nVal == det.Prop_Attr_UROM_BinningMode)
				{
					selectedItem = enumDisplayItem;
				}
			}
			_comboBinning.SelectedItem = selectedItem;
			SdkInterface.GetEnumItemsCount("Enm_Zoom", ref pCount);
			EnumItem[] arr2 = new EnumItem[pCount];
			intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
			SdkInterface.GetEnumItemList("Enm_Zoom", intPtr, pCount);
			SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref arr2);
			Marshal.FreeHGlobal(intPtr);
			EnumItem[] array2 = arr2;
			for (int j = 0; j < array2.Length; j++)
			{
				EnumItem enumItem2 = array2[j];
				EnumDisplayItem enumDisplayItem2 = new EnumDisplayItem
				{
					Val = enumItem2.nVal,
					Label = enumItem2.strName
				};
				_comboZoom.Items.Add(enumDisplayItem2);
				if (enumItem2.nVal == det.Prop_Attr_UROM_ZoomMode)
				{
					selectedItem = enumDisplayItem2;
				}
			}
			_comboZoom.SelectedItem = selectedItem;
		}

		private void Button_Click_OK(object sender, RoutedEventArgs e)
		{
			Enm_Binning val = (Enm_Binning)(_comboBinning.SelectedItem as EnumDisplayItem).Val;
			Enm_Zoom val2 = (Enm_Zoom)(_comboZoom.SelectedItem as EnumDisplayItem).Val;
			if (Instance.Prop_Attr_UROM_BinningMode == (int)val)
			{
				int prop_Attr_UROM_ZoomMode = Instance.Prop_Attr_UROM_ZoomMode;
			}
			Close();
		}

		private void Button_Click_Cancel(object sender, RoutedEventArgs e)
		{
			Result = false;
			Close();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/binningzoomsetwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_comboBinning = (ComboBox)target;
				break;
			case 2:
				_comboZoom = (ComboBox)target;
				break;
			case 3:
				((Button)target).Click += Button_Click_OK;
				break;
			case 4:
				((Button)target).Click += Button_Click_Cancel;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
