namespace iDetector
{
	public struct RectInfo
	{
		public int top;

		public int bottom;

		public int left;

		public int right;
	}
}
