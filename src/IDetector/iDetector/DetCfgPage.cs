using PlugInInterface;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class DetCfgPage : UserControl, IDetSubPage, IComponentConnector
	{
		private const string CRCInfoPlugInName = "VoltageCRCInfo";

		private SDKInvokeProxy SdkInvokeProxy;

		private DlgTaskTimeOut MessageBoxTimer;

		private int TaskTimeoutSeconds = 10;

		private bool bInited;

		private Detector _det;

		private List<int> _monitorAttrs;

		private Button btnReadShockLog;

		private Button btnClearShockLog;

		private Button btnSetTimeShock;

		private Label labSetTimeShock;

		private CtrSensorEditor itemShockThreshold;

		private FirmwareUpgradeExWnd _firmwareUpgradeExWnd;

		private PlugInsManager mPlugin;

		private IPlugIn CRCInfoPlugIn;

		private object CRCInfoComp;

		private ResetProgress resetProgress;

		internal StackPanel _attrsPanel;

		internal Button xWriteRAMBtn;

		internal Button xSetBinningBtn;

		internal Button xSetChannelBtn;

		internal Button xUploadLog;

		internal TabItem _sensorTab;

		internal StackPanel _sensorPanel;

		internal TabItem _wifiTab;

		internal WifiPage xWifiPage;

		internal TabItem _imagesTab;

		internal LocalImages xImagesPage;

		private bool _contentLoaded;

		public bool bHandled
		{
			get;
			set;
		}

		private bool bCanExit
		{
			get;
			set;
		}

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
			}
		}

		public event SubPageMessageHandler MessageNotifyProxy;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public DetCfgPage()
		{
			InitializeComponent();
			if (!DesignerProperties.GetIsInDesignMode(this))
			{
				bCanExit = true;
				base.IsVisibleChanged += OnIsVisibleChanged;
				_monitorAttrs = new List<int>();
			}
		}

		public void InitPanel()
		{
			if (bInited)
			{
				return;
			}
			int nAuthority = 0;
			bInited = true;
			_det.SubscribeCBEvent(OnSdkCallback);
			SdkInvokeProxy = new SDKInvokeProxy(_det, new Shell(Application.Current.MainWindow));
			this.SdkEventHandler = ProcessEvent;
			List<AttrItem> list = DetectorParamsFactory.LoadAttr(Instance.Prop_Attr_UROM_ProductNo);
			foreach (AttrItem item in list)
			{
				AddItem(item.Label, item.ReadAttrID, item.WriteAttrID, item.IsBooleanVal);
			}
			AddFPGADebugItem();
			AddSeneorItem();
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 32:
			case 37:
			case 59:
			case 62:
				xWifiPage.Init(Instance);
				xImagesPage.Init(Instance);
				xSetBinningBtn.Visibility = Visibility.Collapsed;
				xUploadLog.Visibility = Visibility.Visible;
				itemShockThreshold.Visibility = Visibility.Visible;
				btnReadShockLog.Visibility = Visibility.Visible;
				btnClearShockLog.Visibility = Visibility.Visible;
				SdkInterface.GetAuthority(ref nAuthority);
				if ((0x2000 & nAuthority) != 0)
				{
					btnSetTimeShock.Visibility = Visibility.Visible;
					labSetTimeShock.Visibility = Visibility.Visible;
				}
				break;
			case 39:
			case 41:
			case 42:
			case 45:
			case 51:
			case 52:
			case 61:
			case 93:
				xWifiPage.Init(Instance);
				xImagesPage.Init(Instance);
				xSetBinningBtn.Visibility = Visibility.Collapsed;
				xUploadLog.Visibility = Visibility.Visible;
				SdkInterface.GetAuthority(ref nAuthority);
				if ((0x2000 & nAuthority) != 0)
				{
					btnSetTimeShock.Visibility = Visibility.Visible;
					labSetTimeShock.Visibility = Visibility.Visible;
				}
				break;
			case 60:
			case 72:
			case 80:
			case 88:
			case 91:
				xImagesPage.Init(Instance);
				_wifiTab.Visibility = Visibility.Collapsed;
				xSetBinningBtn.Visibility = Visibility.Collapsed;
				xUploadLog.Visibility = Visibility.Visible;
				SdkInterface.GetAuthority(ref nAuthority);
				if ((0x2000 & nAuthority) != 0)
				{
					btnSetTimeShock.Visibility = Visibility.Visible;
					labSetTimeShock.Visibility = Visibility.Visible;
				}
				break;
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
			case 87:
				_imagesTab.Visibility = Visibility.Hidden;
				_wifiTab.Visibility = Visibility.Hidden;
				xSetBinningBtn.Visibility = Visibility.Collapsed;
				break;
			case 11:
				_imagesTab.Visibility = Visibility.Hidden;
				_wifiTab.Visibility = Visibility.Hidden;
				break;
			default:
				_imagesTab.Visibility = Visibility.Hidden;
				_wifiTab.Visibility = Visibility.Hidden;
				break;
			}
			EnableView(true);
		}

		private void AddFPGADebugItem()
		{
		}

		private CtrAttrEditor AddItem(string strLabel, int nReadAttrID, int nWriteAttrID, bool bIsBooleanVal = false)
		{
			CtrAttrEditor ctrAttrEditor = new CtrAttrEditor(strLabel, nReadAttrID, nWriteAttrID, _det);
			ctrAttrEditor.Height = 32.0;
			AttrEditorStyle ctrlStyle = default(AttrEditorStyle);
			ctrlStyle.nLabelColWidth = ((strLabel != null && strLabel.Length != 0) ? 200 : 0);
			ctrlStyle.nReadColWidth = ((nReadAttrID != 0) ? 180 : 0);
			ctrlStyle.nWriteColWidth = 0;
			ctrlStyle.nEditorColWidth = ((nWriteAttrID != 0) ? 190 : 0);
			ctrlStyle.bCheckBoxForIntType = bIsBooleanVal;
			ctrlStyle.nBtnColWidth = 0;
			ctrAttrEditor.SetCtrlStyle(ctrlStyle);
			_attrsPanel.Children.Add(ctrAttrEditor);
			if (nReadAttrID > 0)
			{
				_monitorAttrs.Add(nReadAttrID);
			}
			if (nWriteAttrID > 0)
			{
				_monitorAttrs.Add(nWriteAttrID);
			}
			return ctrAttrEditor;
		}

		private void AddSeneorItem()
		{
			CtrSensorEditor element = new CtrSensorEditor("Temperature", SensorType.ReadOnly, 2010);
			CtrSensorEditor element2 = new CtrSensorEditor("Humidity", SensorType.ReadOnly, 2011);
			_sensorPanel.Children.Add(element);
			_sensorPanel.Children.Add(element2);
			if (45 == Instance.Prop_Attr_UROM_ProductNo || 41 == Instance.Prop_Attr_UROM_ProductNo || 61 == Instance.Prop_Attr_UROM_ProductNo || 42 == Instance.Prop_Attr_UROM_ProductNo || 93 == Instance.Prop_Attr_UROM_ProductNo)
			{
				CtrSensorEditor element3 = new CtrSensorEditor("Battery", SensorType.ReadOnly, 2017);
				_sensorPanel.Children.Add(element3);
			}
			itemShockThreshold = new CtrSensorEditor("Threshold", SensorType.ReadWrite, 2016, 2015);
			itemShockThreshold.Visibility = Visibility.Collapsed;
			_sensorPanel.Children.Add(itemShockThreshold);
			foreach (CtrSensorEditor child in _sensorPanel.Children)
			{
				child.ReadHandler += ReadSensorValue;
				child.WriteHandler += WriteSensorValue;
			}
			AddCRCPlugin();
			btnReadShockLog = new Button();
			btnReadShockLog.Margin = new Thickness(10.0, 10.0, 0.0, 0.0);
			btnReadShockLog.Width = 120.0;
			btnReadShockLog.Height = 28.0;
			btnReadShockLog.Content = "Read Shock Log";
			btnReadShockLog.HorizontalAlignment = HorizontalAlignment.Left;
			btnReadShockLog.Click += btnReadShockLog_Click;
			btnReadShockLog.Visibility = Visibility.Collapsed;
			_sensorPanel.Children.Add(btnReadShockLog);
			btnClearShockLog = new Button();
			btnClearShockLog.Margin = new Thickness(10.0, 10.0, 0.0, 0.0);
			btnClearShockLog.Width = 120.0;
			btnClearShockLog.Height = 28.0;
			btnClearShockLog.Content = "Clear Shock Log";
			btnClearShockLog.HorizontalAlignment = HorizontalAlignment.Left;
			btnClearShockLog.Click += btnClearShockLog_Click;
			btnClearShockLog.Visibility = Visibility.Collapsed;
			_sensorPanel.Children.Add(btnClearShockLog);
			labSetTimeShock = new Label();
			labSetTimeShock.Margin = new Thickness(10.0, 10.0, 0.0, 0.0);
			labSetTimeShock.Width = 360.0;
			labSetTimeShock.Height = 28.0;
			labSetTimeShock.Content = "Click following button to set FPD time, which is based on PC time:";
			labSetTimeShock.HorizontalAlignment = HorizontalAlignment.Left;
			labSetTimeShock.Visibility = Visibility.Collapsed;
			_sensorPanel.Children.Add(labSetTimeShock);
			btnSetTimeShock = new Button();
			btnSetTimeShock.Margin = new Thickness(10.0, 10.0, 0.0, 0.0);
			btnSetTimeShock.Width = 120.0;
			btnSetTimeShock.Height = 28.0;
			btnSetTimeShock.Content = "Sync Time";
			btnSetTimeShock.HorizontalAlignment = HorizontalAlignment.Left;
			btnSetTimeShock.Click += btnSetTimeShock_Click;
			btnSetTimeShock.Visibility = Visibility.Collapsed;
			_sensorPanel.Children.Add(btnSetTimeShock);
		}

		private void AddCRCPlugin()
		{
			int nAuthority = 0;
			SdkInterface.GetAuthority(ref nAuthority);
			if ((0x2000 & nAuthority) != 0)
			{
				mPlugin = PlugInsManager.Creator();
				CRCInfoComp = mPlugin.GetPlugElementByClassName("VoltageCRCInfo");
				if (CRCInfoComp != null && CRCInfoComp is FrameworkElement)
				{
					CRCInfoPlugIn = mPlugin.GetPlugObjByClassName("VoltageCRCInfo");
				}
			}
			if (CRCInfoPlugIn != null && CRCInfoPlugIn.InitDetectorInstance(Instance) && !_sensorPanel.Children.Contains(CRCInfoComp as UIElement))
			{
				_sensorPanel.Children.Add(CRCInfoComp as FrameworkElement);
			}
		}

		private void RemoveCRCPlugin()
		{
			if (CRCInfoComp != null && _sensorPanel.Children.Contains(CRCInfoComp as UIElement))
			{
				_sensorPanel.Children.Remove(CRCInfoComp as FrameworkElement);
			}
			if (mPlugin != null)
			{
				mPlugin.Clear();
			}
		}

		private CtrSensorEditor ParseSensorItem(int cmdId)
		{
			foreach (CtrSensorEditor child in _sensorPanel.Children)
			{
				if (child.ReadCmd == cmdId)
				{
					return child;
				}
			}
			return null;
		}

		private void ReadSensorValue(object sender)
		{
			CtrSensorEditor ctrSensorEditor = sender as CtrSensorEditor;
			if (ctrSensorEditor != null)
			{
				SdkInvokeProxy.Invoke(ctrSensorEditor.ReadCmd);
			}
		}

		private void WriteSensorValue(object sender)
		{
			CtrSensorEditor ctrSensorEditor = sender as CtrSensorEditor;
			if (ctrSensorEditor != null)
			{
				SdkInvokeProxy.Invoke(ctrSensorEditor.WriteCmd, ctrSensorEditor.Value);
			}
		}

		private bool AcceptAll()
		{
			foreach (UIElement child in _attrsPanel.Children)
			{
				if (child is CtrAttrEditor)
				{
					CtrAttrEditor ctrAttrEditor = child as CtrAttrEditor;
					if (!ctrAttrEditor.Accept())
					{
						ResumeWriteAttributeValue(ctrAttrEditor.WriteAttrID);
						return false;
					}
				}
			}
			bool flag = false;
			Enm_TriggerMode prop_Attr_UROM_TriggerMode_W = (Enm_TriggerMode)_det.Prop_Attr_UROM_TriggerMode_W;
			switch (_det.Prop_Attr_UROM_ProductNo)
			{
			case 51:
			case 52:
			{
				if (prop_Attr_UROM_TriggerMode_W != Enm_TriggerMode.Enm_TriggerMode_Inner && (prop_Attr_UROM_TriggerMode_W != Enm_TriggerMode.Enm_TriggerMode_Prep || 1 != _det.Prop_Attr_UROM_PrepCapMode_W))
				{
					break;
				}
				bool flag2 = Enm_TriggerMode.Enm_TriggerMode_Inner == prop_Attr_UROM_TriggerMode_W && 1 == _det.Prop_Attr_UROM_InnerSubFlow_W;
				switch (flag2 ? _det.Prop_Attr_UROM_SetDelayTime_W : _det.Prop_Attr_UROM_ExpWindowTime_W)
				{
				default:
					if (flag2)
					{
						MessageBox.Show("SetDelayTime must among {700,1200,2200,3200,4200} in this mode.");
					}
					else
					{
						MessageBox.Show("ExpWindowTime must among {700,1200,2200,3200,4200} in this mode.");
					}
					flag = true;
					break;
				case 700:
				case 1200:
				case 2200:
				case 3200:
				case 4200:
					break;
				}
				break;
			}
			}
			if (flag)
			{
				ResumeWriteAttributeValue(-1);
				return false;
			}
			return true;
		}

		public void UpdateWriteConfList()
		{
			foreach (UIElement child in _attrsPanel.Children)
			{
				if (child is CtrAttrEditor)
				{
					CtrAttrEditor ctrAttrEditor = child as CtrAttrEditor;
					ctrAttrEditor.RefillEditValue();
				}
			}
		}

		private void OnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.NewValue.Equals(true))
			{
				_det.AttrChangingMonitor.AddRef(Enumerable.ToArray(_monitorAttrs));
			}
			else
			{
				_det.AttrChangingMonitor.ReleaseRef(Enumerable.ToArray(_monitorAttrs));
			}
		}

		private void Button_Click_ReadRAM(object sender, RoutedEventArgs e)
		{
			Instance.TASK = "Reading RAM";
			int num = SdkInvokeProxy.Invoke(2001);
			if (num == 1)
			{
				EnableView(false);
			}
		}

		private void Button_Click_WriteRAM(object sender, RoutedEventArgs e)
		{
			if (AcceptAll())
			{
				Instance.TASK = "Writing detector params";
				int num = SdkInvokeProxy.Invoke(2031);
				if (num == 1)
				{
					EnableView(false);
				}
			}
		}

		private void Button_Click_WriteROM(object sender, RoutedEventArgs e)
		{
			if (AcceptAll() && (11 != Instance.Prop_Attr_UROM_ProductNo || MammoCheck()))
			{
				Instance.TASK = "Writing detector params";
				int num = SdkInvokeProxy.Invoke(2002);
				if (num == 1)
				{
					EnableView(false);
				}
			}
		}

		private void ResumeWriteAttributeValue(int nlastErrorWriteID)
		{
			foreach (UIElement child in _attrsPanel.Children)
			{
				if (child is CtrAttrEditor)
				{
					CtrAttrEditor ctrAttrEditor = child as CtrAttrEditor;
					if (nlastErrorWriteID == ctrAttrEditor.WriteAttrID)
					{
						break;
					}
					if (ctrAttrEditor.WriteAttrID != 0)
					{
						IRayVariant pVar = default(IRayVariant);
						Instance.GetAttr(ctrAttrEditor.ReadAttrID, ref pVar);
						if (pVar.vt == IRAY_VAR_TYPE.IVT_STR)
						{
							SdkInterface.SetAttr(Instance.ID, ctrAttrEditor.WriteAttrID, pVar.val.strVal);
						}
						else if (pVar.vt == IRAY_VAR_TYPE.IVT_FLT)
						{
							SdkInterface.SetAttr(Instance.ID, ctrAttrEditor.WriteAttrID, pVar.val.fVal);
						}
						else if (pVar.vt == IRAY_VAR_TYPE.IVT_INT)
						{
							SdkInterface.SetAttr(Instance.ID, ctrAttrEditor.WriteAttrID, pVar.val.nVal);
						}
					}
				}
			}
		}

		private void Button_Click_Reset(object sender, RoutedEventArgs e)
		{
			Instance.TASK = "Reset detector";
			int num = SdkInvokeProxy.Invoke(8);
			if (num == 1)
			{
				EnableView(false);
				resetProgress = new ResetProgress(GetResetTimeoutSeconds());
				resetProgress.RunTask();
			}
		}

		private int GetResetTimeoutSeconds()
		{
			int num = Instance.Prop_Cfg_ResetTimeout;
			switch (Instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
			case 87:
			{
				double num2 = 0.0;
				List<ApplicationMode> list = ApplicationMode.ParseIni(Instance.Prop_Attr_WorkDir);
				foreach (ApplicationMode item in list)
				{
					num2 += ((item.Freq == 0.0) ? 0.0 : (1000.0 / item.Freq));
				}
				num += Convert.ToInt32(Math.Ceiling(num2 * 9.0 / 1000.0));
				break;
			}
			}
			return num;
		}

		private void Button_Click_SetBinningZoom(object sender, RoutedEventArgs e)
		{
			BinningZoomSetWnd binningZoomSetWnd = new BinningZoomSetWnd(Instance);
			binningZoomSetWnd.ShowDialog();
			if (binningZoomSetWnd.Result)
			{
				ShowMessageBox("SetBinningZoom", "Setting BinningZoom...");
			}
		}

		private void Button_Click_SetImageChannel(object sender, RoutedEventArgs e)
		{
			ImgChSetWnd imgChSetWnd = new ImgChSetWnd(Instance);
			imgChSetWnd.ShowDialog();
		}

		private void Button_Click_UpgradeFirmware(object sender, RoutedEventArgs e)
		{
			if (_firmwareUpgradeExWnd != null)
			{
				_firmwareUpgradeExWnd.Close();
				_firmwareUpgradeExWnd = null;
			}
			_firmwareUpgradeExWnd = new FirmwareUpgradeExWnd(Instance);
			_firmwareUpgradeExWnd.Owner = Application.Current.MainWindow;
			_firmwareUpgradeExWnd.ShowDialog();
		}

		private void Button_Click_UploadLog(object sender, RoutedEventArgs e)
		{
			Instance.TASK = "Upload Log";
			int num = SdkInvokeProxy.Invoke(2012);
			if (num == 1)
			{
				EnableView(false);
			}
		}

		private void OnCloseUpgradeBar()
		{
		}

		private void EnableView(bool bEnable)
		{
			base.IsEnabled = bEnable;
			bCanExit = bEnable;
			if (bEnable && Instance != null)
			{
				Instance.TASK = "No Task";
			}
		}

		public void UpdateEditValueByApplicationMode()
		{
			int[] array = new int[3]
			{
				2505,
				2513,
				2525
			};
			int[] array2 = new int[3]
			{
				Instance.Prop_Attr_UROM_SequenceIntervalTime_W,
				Instance.Prop_Attr_UROM_PGA_W,
				Instance.Prop_Attr_UROM_BinningMode_W
			};
			for (int i = 0; i < array.Length; i++)
			{
				Instance.AttrChangingMonitor.FindItem(array[i]);
				foreach (UIElement child in _attrsPanel.Children)
				{
					if (child is CtrAttrEditor)
					{
						CtrAttrEditor ctrAttrEditor = child as CtrAttrEditor;
						if (array[i] == ctrAttrEditor.WriteAttrID)
						{
							ctrAttrEditor.RefillEditValue(array2[i]);
							break;
						}
					}
				}
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (IntPtr.Zero == pParam)
			{
				base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
			}
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			switch (nEventID)
			{
			case 4:
				switch (nParam1)
				{
				case 2:
					UpdateWriteConfList();
					break;
				case 8:
					if (resetProgress != null)
					{
						resetProgress.StopTask();
					}
					break;
				case 2011:
				{
					CtrSensorEditor ctrSensorEditor4 = ParseSensorItem(2011);
					if (ctrSensorEditor4 != null)
					{
						ctrSensorEditor4.StrValue = sender.Prop_Attr_RdResult_Humidity.ToString("0.0") + "%";
					}
					break;
				}
				case 2010:
				{
					CtrSensorEditor ctrSensorEditor3 = ParseSensorItem(2010);
					if (ctrSensorEditor3 == null)
					{
						return;
					}
					switch (sender.Prop_Attr_UROM_ProductNo)
					{
					case 32:
					case 37:
					case 39:
					case 59:
					case 62:
						ctrSensorEditor3.StrValue = sender.Prop_Attr_RdResult_T1.ToString("0.0") + "/" + sender.Prop_Attr_RdResult_T2.ToString("0.0");
						break;
					case 41:
					case 42:
					case 45:
					case 51:
					case 52:
					case 60:
					case 61:
					case 72:
					case 80:
					case 88:
					case 91:
					case 93:
						ctrSensorEditor3.StrValue = sender.Prop_Attr_RdResult_T1.ToString("0.0");
						break;
					default:
						ctrSensorEditor3.StrValue = sender.Prop_Attr_RdResult_T2.ToString("0.0");
						break;
					}
					break;
				}
				case 2008:
					if (_firmwareUpgradeExWnd != null)
					{
						_firmwareUpgradeExWnd.UpdateCallback();
						if (_firmwareUpgradeExWnd.IsUpgradeFinish())
						{
							_firmwareUpgradeExWnd.Close();
							_firmwareUpgradeExWnd = null;
							MessageBox.Show("Upgrade succeed");
						}
					}
					break;
				case 2016:
				{
					CtrSensorEditor ctrSensorEditor2 = ParseSensorItem(2016);
					if (ctrSensorEditor2 != null)
					{
						ctrSensorEditor2.StrValue = sender.Prop_Attr_RdResult_Shock_Threshold.ToString("0");
					}
					break;
				}
				case 2017:
				{
					CtrSensorEditor ctrSensorEditor = ParseSensorItem(2017);
					if (ctrSensorEditor == null)
					{
						return;
					}
					if (sender.Prop_Attr_Battery_Exist != 0)
					{
						ctrSensorEditor.StrValue = string.Format("{0}%{1}", sender.Prop_Attr_Battery_Remaining, (sender.Prop_Attr_Battery_ChargingStatus != 0) ? "(Charging)" : ((sender.Prop_Attr_Battery_ExternalPower != 0) ? "(ExtPower)" : ""));
					}
					else
					{
						ctrSensorEditor.StrValue = "No Battery Exist";
					}
					break;
				}
				}
				EnableView(true);
				break;
			case 5:
				switch (nParam1)
				{
				case 8:
					if (resetProgress != null)
					{
						resetProgress.StopTask();
					}
					break;
				case 2008:
					if (_firmwareUpgradeExWnd == null)
					{
						return;
					}
					if (nParam2 == 39)
					{
						_firmwareUpgradeExWnd.UpdateCallback();
						if (_firmwareUpgradeExWnd.IsUpgradeFinish())
						{
							_firmwareUpgradeExWnd.Close();
							_firmwareUpgradeExWnd = null;
							MessageBox.Show("Upgrade succeed");
						}
					}
					else
					{
						_firmwareUpgradeExWnd.UnloadUpgradeState();
						MessageBox.Show("Upgrade failed! Err = " + ErrorHelper.GetErrorDesp(nParam2));
					}
					break;
				}
				EnableView(true);
				break;
			case 6:
				if (nParam1 == 2008 && _firmwareUpgradeExWnd != null)
				{
					_firmwareUpgradeExWnd.UnloadUpgradeState();
				}
				EnableView(true);
				break;
			}
		}

		private void MessageNotify(string info)
		{
			if (this.MessageNotifyProxy != null)
			{
				this.MessageNotifyProxy(info);
			}
		}

		private void MessageBoxShow(string content)
		{
			MessageBox.Show(Application.Current.MainWindow, content);
		}

		protected void ShowMessageBox(string title, string content)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TaskTimeoutSeconds, null, CurTaskTimeOut);
			MessageBoxTimer.RunTask();
		}

		private void CurTaskTimeOut()
		{
			MessageBoxShow("Current task Timeout !");
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		public void NotifyReconnected()
		{
			EnableView(true);
		}

		public void OnClose()
		{
			RemoveCRCPlugin();
			xWifiPage.Close();
			Instance.UnSubscribeCBEvent(OnSdkCallback);
			foreach (UIElement child in _attrsPanel.Children)
			{
				if (child is CtrAttrEditor)
				{
					(child as CtrAttrEditor).OnClose();
				}
			}
			_attrsPanel.Children.Clear();
		}

		public bool Exit()
		{
			return bCanExit;
		}

		public bool Enter()
		{
			return true;
		}

		private bool MammoCheck()
		{
			int prop_Attr_UROM_TriggerMode_W = Instance.Prop_Attr_UROM_TriggerMode_W;
			int prop_Attr_UROM_ExpMode_W = Instance.Prop_Attr_UROM_ExpMode_W;
			int prop_Attr_UROM_PreviewImgMode_W = Instance.Prop_Attr_UROM_PreviewImgMode_W;
			int prop_Attr_UROM_HWOffsetType_W = Instance.Prop_Attr_UROM_HWOffsetType_W;
			int prop_Attr_UROM_DynaOffsetEnable_W = Instance.Prop_Attr_UROM_DynaOffsetEnable_W;
			if (prop_Attr_UROM_ExpMode_W == 2 && prop_Attr_UROM_TriggerMode_W == 2)
			{
				MessageBoxShow("The [AEC] Exp Mode and the [Software] Trigger Mode is not matching!");
				return false;
			}
			if (prop_Attr_UROM_ExpMode_W == 1 && prop_Attr_UROM_TriggerMode_W == 1)
			{
				MessageBoxShow("The [Manual] Exp Mode and the [Inner] Trigger Mode is not matching!");
				return false;
			}
			if (prop_Attr_UROM_ExpMode_W == 2 && prop_Attr_UROM_TriggerMode_W == 1)
			{
				MessageBoxShow("The [AEC] Exp Mode and the [Inner] Trigger Mode is not matching!");
				return false;
			}
			if (prop_Attr_UROM_ExpMode_W == 0 && prop_Attr_UROM_PreviewImgMode_W != 0)
			{
				MessageBoxShow("The [Normal] Exp Mode and the Preview Image Mode is not matching!");
				return false;
			}
			if (prop_Attr_UROM_ExpMode_W == 0 && prop_Attr_UROM_DynaOffsetEnable_W != 0)
			{
				MessageBoxShow("The [Normal] Exp Mode and the Dyna Offset Mode is not matching!");
				return false;
			}
			if ((prop_Attr_UROM_ExpMode_W == 1 && prop_Attr_UROM_PreviewImgMode_W != 1) || (prop_Attr_UROM_ExpMode_W == 2 && prop_Attr_UROM_PreviewImgMode_W != 1))
			{
				MessageBoxShow("The [Normal]/[AEC] Exp Mode and the Preview Image Mode is not matching!");
				return false;
			}
			if ((prop_Attr_UROM_ExpMode_W == 1 && prop_Attr_UROM_HWOffsetType_W != 1) || (prop_Attr_UROM_ExpMode_W == 2 && prop_Attr_UROM_HWOffsetType_W != 1))
			{
				MessageBoxShow("The [Normal]/[AEC] Exp Mode and the HW Offset Mode is not matching!");
				return false;
			}
			if ((prop_Attr_UROM_ExpMode_W == 1 && prop_Attr_UROM_DynaOffsetEnable_W != 1) || (prop_Attr_UROM_ExpMode_W == 2 && prop_Attr_UROM_DynaOffsetEnable_W != 1))
			{
				MessageBoxShow("The [Normal]/[AEC] Exp Mode and the Dyna Offset Mode is not matching!");
				return false;
			}
			return true;
		}

		private void btnReadShockLog_Click(object sender, RoutedEventArgs e)
		{
			Instance.TASK = "Read Shock Log";
			int num = SdkInvokeProxy.Invoke(2013);
			if (num == 1)
			{
				EnableView(false);
			}
		}

		private void btnClearShockLog_Click(object sender, RoutedEventArgs e)
		{
			Instance.TASK = "Clear Shock Log";
			int num = SdkInvokeProxy.Invoke(2014);
			if (num == 1)
			{
				EnableView(false);
			}
		}

		private void btnSetTimeShock_Click(object sender, RoutedEventArgs e)
		{
			Instance.TASK = "Set Time Shock";
			int num = SdkInvokeProxy.Invoke(2018, 0);
			if (num == 1)
			{
				EnableView(false);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/detsubpages/detcfgpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_attrsPanel = (StackPanel)target;
				break;
			case 2:
				((Button)target).Click += Button_Click_Reset;
				break;
			case 3:
				((Button)target).Click += Button_Click_ReadRAM;
				break;
			case 4:
				((Button)target).Click += Button_Click_WriteROM;
				break;
			case 5:
				xWriteRAMBtn = (Button)target;
				xWriteRAMBtn.Click += Button_Click_WriteRAM;
				break;
			case 6:
				xSetBinningBtn = (Button)target;
				xSetBinningBtn.Click += Button_Click_SetBinningZoom;
				break;
			case 7:
				xSetChannelBtn = (Button)target;
				xSetChannelBtn.Click += Button_Click_SetImageChannel;
				break;
			case 8:
				((Button)target).Click += Button_Click_UpgradeFirmware;
				break;
			case 9:
				xUploadLog = (Button)target;
				xUploadLog.Click += Button_Click_UploadLog;
				break;
			case 10:
				_sensorTab = (TabItem)target;
				break;
			case 11:
				_sensorPanel = (StackPanel)target;
				break;
			case 12:
				_wifiTab = (TabItem)target;
				break;
			case 13:
				xWifiPage = (WifiPage)target;
				break;
			case 14:
				_imagesTab = (TabItem)target;
				break;
			case 15:
				xImagesPage = (LocalImages)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
