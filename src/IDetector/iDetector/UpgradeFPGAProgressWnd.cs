using System;
using System.Windows;

namespace iDetector
{
	public class UpgradeFPGAProgressWnd : DownloadFileProgressBar
	{
		private Detector Dectector;

		private DateTime StartTime;

		public UpgradeFPGAProgressWnd(Detector detector, string title)
			: base(title, detector.Prop_Cfg_FWUpdTimeOut)
		{
			Dectector = detector;
			if (Dectector == null)
			{
				throw new NotImplementedException("No detector instance");
			}
			StartTime = DateTime.Now;
		}

		protected override void UpdateProgress()
		{
			if ((DateTime.Now - StartTime).TotalSeconds > (double)Dectector.Prop_Cfg_FWUpdTimeOut)
			{
				MessageBox.Show("Upgrade progress time out! Total time: " + Dectector.Prop_Cfg_FWUpdTimeOut + " seconds", "iDetector");
				Dectector.Abort();
				OnClose();
				return;
			}
			if (Dectector.Prop_Attr_FWUpdateProgress != Convert.ToInt32(ProgressBar.Value))
			{
				StartTime = DateTime.Now;
			}
			ProgressBar.Value = Dectector.Prop_Attr_FWUpdateProgress;
			PercentValue.Content = Dectector.Prop_Attr_FWUpdateProgress.ToString();
		}
	}
}
