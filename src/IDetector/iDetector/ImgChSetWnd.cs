using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class ImgChSetWnd : Window, IComponentConnector
	{
		private Detector _det;

		internal ComboBox _comboImgCh;

		internal ComboBox _comboEthProtocol;

		private bool _contentLoaded;

		public virtual Detector Instance
		{
			get
			{
				return _det;
			}
			set
			{
				_det = value;
			}
		}

		public ImgChSetWnd(Detector det)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			if (DesignerProperties.GetIsInDesignMode(this))
			{
				return;
			}
			_det = det;
			int pCount = 0;
			SdkInterface.GetEnumItemsCount("Enm_ImgChType", ref pCount);
			EnumItem[] arr = new EnumItem[pCount];
			IntPtr intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
			SdkInterface.GetEnumItemList("Enm_ImgChType", intPtr, pCount);
			SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref arr);
			Marshal.FreeHGlobal(intPtr);
			EnumDisplayItem selectedItem = null;
			EnumItem[] array = arr;
			for (int i = 0; i < array.Length; i++)
			{
				EnumItem enumItem = array[i];
				EnumDisplayItem enumDisplayItem = new EnumDisplayItem
				{
					Val = enumItem.nVal,
					Label = enumItem.strName
				};
				_comboImgCh.Items.Add(enumDisplayItem);
				if (enumItem.nVal == det.Prop_Attr_UROM_BinningMode)
				{
					selectedItem = enumDisplayItem;
				}
			}
			_comboImgCh.SelectedItem = selectedItem;
			SdkInterface.GetEnumItemsCount("Enm_EthernetProtocol", ref pCount);
			EnumItem[] arr2 = new EnumItem[pCount];
			intPtr = Marshal.AllocHGlobal(pCount * Marshal.SizeOf(typeof(EnumItem)));
			SdkInterface.GetEnumItemList("Enm_EthernetProtocol", intPtr, pCount);
			SdkParamConvertor<EnumItem>.IntPtrToStructArray(intPtr, ref arr2);
			Marshal.FreeHGlobal(intPtr);
			EnumItem[] array2 = arr2;
			for (int j = 0; j < array2.Length; j++)
			{
				EnumItem enumItem2 = array2[j];
				EnumDisplayItem enumDisplayItem2 = new EnumDisplayItem
				{
					Val = enumItem2.nVal,
					Label = enumItem2.strName
				};
				_comboEthProtocol.Items.Add(enumDisplayItem2);
				if (enumItem2.nVal == det.Prop_Attr_UROM_ZoomMode)
				{
					selectedItem = enumDisplayItem2;
				}
			}
			_comboEthProtocol.SelectedItem = selectedItem;
		}

		private void Button_Click_OK(object sender, RoutedEventArgs e)
		{
			Enm_ImgChType val = (Enm_ImgChType)(_comboImgCh.SelectedItem as EnumDisplayItem).Val;
			Enm_EthernetProtocol val2 = (Enm_EthernetProtocol)(_comboEthProtocol.SelectedItem as EnumDisplayItem).Val;
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = (int)val;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = (int)val2;
			int num = Instance.Invoke(2009, array, 2);
			if (num != 0 && num != 1)
			{
				MessageBox.Show("Set Image channel parameters failed! Err = " + ErrorHelper.GetErrorDesp(num));
			}
		}

		private void Button_Click_Cancel(object sender, RoutedEventArgs e)
		{
			Close();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/imgchsetwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_comboImgCh = (ComboBox)target;
				break;
			case 2:
				_comboEthProtocol = (ComboBox)target;
				break;
			case 3:
				((Button)target).Click += Button_Click_OK;
				break;
			case 4:
				((Button)target).Click += Button_Click_Cancel;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
