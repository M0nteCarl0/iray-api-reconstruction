using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class CtrSensorEditor : UserControl, IComponentConnector
	{
		public int ReadCmd;

		public int WriteCmd;

		internal Label _dispName;

		internal TextBox _txtValue;

		internal Button _btnGet;

		internal Button _btnSet;

		private bool _contentLoaded;

		public int Value
		{
			get
			{
				if (_txtValue.Text.Length == 0)
				{
					return 0;
				}
				return Convert.ToInt32(_txtValue.Text);
			}
		}

		public string StrValue
		{
			set
			{
				_txtValue.Text = value;
			}
		}

		public event SensorEventHandler ReadHandler;

		public event SensorEventHandler WriteHandler;

		public CtrSensorEditor(string dispname, SensorType type, int readCmd, int writeCmd = 0)
		{
			InitializeComponent();
			if (type == SensorType.ReadOnly)
			{
				_btnSet.Visibility = Visibility.Hidden;
				_txtValue.IsEnabled = false;
			}
			else
			{
				_txtValue.IsReadOnly = false;
			}
			_dispName.Content = dispname;
			ReadCmd = readCmd;
			WriteCmd = writeCmd;
		}

		private void _btnGet_Click(object sender, RoutedEventArgs e)
		{
			if (this.ReadHandler != null)
			{
				this.ReadHandler(this);
			}
		}

		private void _btnSet_Click(object sender, RoutedEventArgs e)
		{
			if (this.WriteHandler != null)
			{
				this.WriteHandler(this);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/usercontrol/ctrsensoreditor.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_dispName = (Label)target;
				break;
			case 2:
				_txtValue = (TextBox)target;
				break;
			case 3:
				_btnGet = (Button)target;
				_btnGet.Click += _btnGet_Click;
				break;
			case 4:
				_btnSet = (Button)target;
				_btnSet.Click += _btnSet_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
