using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class HomePage : UserControl, IComponentConnector
	{
		internal ListView _listView;

		internal Button _btnScan;

		internal Button _btnBind;

		internal Button _btnRemove;

		internal Button _btnConnect;

		internal Button _btnClose;

		internal Button _btnAddWorkDir;

		internal Button _btnDelWorkDir;

		internal Button _btnDisplay;

		internal Button _btnSyncbox;

		internal Label ConnectState;

		internal Label SWVersion;

		private bool _contentLoaded;

		public HomePage()
		{
			InitializeComponent();
		}

		public void UpdateConnectInfo(string info)
		{
			ConnectState.Content = info;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/homepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_listView = (ListView)target;
				break;
			case 2:
				_btnScan = (Button)target;
				break;
			case 3:
				_btnBind = (Button)target;
				break;
			case 4:
				_btnRemove = (Button)target;
				break;
			case 5:
				_btnConnect = (Button)target;
				break;
			case 6:
				_btnClose = (Button)target;
				break;
			case 7:
				_btnAddWorkDir = (Button)target;
				break;
			case 8:
				_btnDelWorkDir = (Button)target;
				break;
			case 9:
				_btnDisplay = (Button)target;
				break;
			case 10:
				_btnSyncbox = (Button)target;
				break;
			case 11:
				ConnectState = (Label)target;
				break;
			case 12:
				SWVersion = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
