using System.Windows;

namespace iDetector
{
	public class SDKInvokeProxy
	{
		private Detector mDetector;

		private Shell shell;

		public SDKInvokeProxy(Detector _det, Shell _shell)
		{
			mDetector = _det;
			shell = _shell;
		}

		public int Invoke(int nCmdId)
		{
			return Invoke(nCmdId, null, 0);
		}

		public int Invoke(int nCmdId, int nPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[1];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, int nPara1, int nPara2)
		{
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, int nPara1, int nPara2, int nPara3)
		{
			IRayCmdParam[] array = new IRayCmdParam[3];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			array[2].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[2].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[2].var.val.nVal = nPara3;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, string strPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[1];
			array[0].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[0].var.val.strVal = strPara;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, int nPara, string strPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[1].var.val.strVal = strPara;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, int noop, string strPara, int nPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[2];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[0].var.val.strVal = strPara;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, int nPara1, int nPara2, string strPara)
		{
			IRayCmdParam[] array = new IRayCmdParam[3];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			array[2].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[2].var = default(IRayVariant);
			array[2].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[2].var.val.strVal = strPara;
			return Invoke(nCmdId, array, array.Length);
		}

		public int Invoke(int nCmdId, int nPara1, int nPara2, string strPara1, string strPara2)
		{
			IRayCmdParam[] array = new IRayCmdParam[4];
			array[0].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[0].var = default(IRayVariant);
			array[0].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[0].var.val.nVal = nPara1;
			array[1].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[1].var = default(IRayVariant);
			array[1].var.vt = IRAY_VAR_TYPE.IVT_INT;
			array[1].var.val.nVal = nPara2;
			array[2].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[2].var = default(IRayVariant);
			array[2].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[2].var.val.strVal = strPara1;
			array[3].pt = IRAY_PARAM_TYPE.IPT_VARIANT;
			array[3].var = default(IRayVariant);
			array[3].var.vt = IRAY_VAR_TYPE.IVT_STR;
			array[3].var.val.strVal = strPara2;
			return Invoke(nCmdId, array, array.Length);
		}

		private int Invoke(int nCmdId, IRayCmdParam[] aParas, int nParCount)
		{
			if (mDetector == null)
			{
				return 2;
			}
			int num = mDetector.Invoke(nCmdId, aParas, nParCount);
			if (num != 0 && num != 1)
			{
				if (shell != null && shell.ParentUI != null)
				{
					MessageBox.Show(shell.ParentUI, "Failed! Err = " + ErrorHelper.GetErrorDesp(num));
				}
				else
				{
					MessageBox.Show("Invoke Failed! Err = " + ErrorHelper.GetErrorDesp(num));
				}
			}
			return num;
		}
	}
}
