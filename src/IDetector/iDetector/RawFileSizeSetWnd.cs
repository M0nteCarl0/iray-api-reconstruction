using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector
{
	public class RawFileSizeSetWnd : Window, IComponentConnector
	{
		public bool ResultOK;

		internal DigitalTextBox _imgWidth;

		internal DigitalTextBox _imgHeight;

		private bool _contentLoaded;

		public int ImgWidth
		{
			get
			{
				if (_imgWidth.Text.Length != 0)
				{
					return _imgWidth.Value;
				}
				return 0;
			}
		}

		public int ImgHeight
		{
			get
			{
				if (_imgHeight.Text.Length != 0)
				{
					return _imgHeight.Value;
				}
				return 0;
			}
		}

		public RawFileSizeSetWnd(int width, int height)
		{
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			_imgWidth.Text = width.ToString();
			_imgHeight.Text = height.ToString();
			DesignerProperties.GetIsInDesignMode(this);
		}

		private void Button_Click_OK(object sender, RoutedEventArgs e)
		{
			if (_imgWidth.Value == 0 || _imgHeight.Value == 0)
			{
				MessageBox.Show("Image width or hight cannot be empty or 0!");
				return;
			}
			ResultOK = true;
			Close();
		}

		private void Button_Click_Cancel(object sender, RoutedEventArgs e)
		{
			ResultOK = false;
			Close();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/rawfilesizesetwnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_imgWidth = (DigitalTextBox)target;
				break;
			case 2:
				_imgHeight = (DigitalTextBox)target;
				break;
			case 3:
				((Button)target).Click += Button_Click_OK;
				break;
			case 4:
				((Button)target).Click += Button_Click_Cancel;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
