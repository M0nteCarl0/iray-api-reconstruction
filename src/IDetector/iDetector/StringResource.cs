using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace iDetector
{
	internal class StringResource
	{
		public static void LoadLanguage(string Culture)
		{
			List<ResourceDictionary> list = new List<ResourceDictionary>();
			foreach (ResourceDictionary mergedDictionary in Application.Current.Resources.MergedDictionaries)
			{
				list.Add(mergedDictionary);
			}
			string requestedCulture = string.Format("Resources\\StringResource.{0}.xaml", Culture);
			ResourceDictionary resourceDictionary = list.FirstOrDefault((ResourceDictionary d) => d.Source.OriginalString.Equals(requestedCulture));
			if (resourceDictionary == null)
			{
				requestedCulture = "Resources\\StringResource.en-US.xaml";
				resourceDictionary = list.FirstOrDefault((ResourceDictionary d) => d.Source.OriginalString.Equals(requestedCulture));
			}
			if (resourceDictionary != null)
			{
				Application.Current.Resources.MergedDictionaries.Remove(resourceDictionary);
				Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
			}
		}

		public static string FindString(object resourcekey)
		{
			object obj = Application.Current.TryFindResource(resourcekey);
			if (obj == null)
			{
				return resourcekey as string;
			}
			return obj.ToString();
		}
	}
}
