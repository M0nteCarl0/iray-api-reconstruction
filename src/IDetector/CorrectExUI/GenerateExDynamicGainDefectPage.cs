using Cinema;
using CorrectExUI.ViewModel;
using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class GenerateExDynamicGainDefectPage : UserControl, IComponentConnector
	{
		private DetecInfo Instance;

		private GenerateExDynamicGainDefectPageVM generateExDynamicGainDefectPageVM;

		private int GroupNumber = 3;

		private string[] CineBufferName;

		private int GroupLightFrames;

		private CorrectionTemplateImageList ImageListVM;

		private ReceiveImageCtrl RecvImgProcessor;

		private CinePlayerCtrl mCinePlayCtrl;

		internal GenerateExTemplatePage xExDynamicGainDefectImage;

		private bool _contentLoaded;

		public GenerateExDynamicGainDefectPage(DetecInfo instance)
		{
			InitializeComponent();
			Instance = instance;
		}

		public void Initialize()
		{
			xExDynamicGainDefectImage.xLoadFile.Visibility = Visibility.Visible;
			CineBufferName = new string[GroupNumber];
			GroupLightFrames = Instance.Detector.Prop_Attr_DefectTotalFrames / GroupNumber / 2;
			string text = Instance.Detector.Prop_Attr_WorkDir + "temp\\";
			for (int i = 0; i < CineBufferName.Length; i++)
			{
				CineBufferName[i] = text + i.ToString() + "-" + GroupLightFrames.ToString() + "Cine.cin";
			}
			if (!Utility.IsDirectoryExisted(text))
			{
				Utility.CreateDirectory(text);
			}
			ImageListVM = new CorrectionCineListVM(CineBufferName, Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
			ImageListVM.DisplayThumb += ShowImage;
			generateExDynamicGainDefectPageVM = new GenerateExDynamicGainDefectPageVM(Instance);
			generateExDynamicGainDefectPageVM.ClosePlaybar += ClosePlaybar;
			generateExDynamicGainDefectPageVM.StartStoreImage += StartStoreCineData;
			generateExDynamicGainDefectPageVM.StopStoreImage += StopStoreCineData;
			base.DataContext = generateExDynamicGainDefectPageVM;
			generateExDynamicGainDefectPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			RecvImgProcessor = new ReceiveImageCtrl();
			RecvImgProcessor.PanitImage += PaintImage;
			RecvImgProcessor.AdjustWinLevelAndWidth += AdjustWinLevelAndWinWidth;
			InitImageSize();
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				InitImageSize();
				generateExDynamicGainDefectPageVM.AddCallbackHandler();
				xExDynamicGainDefectImage.xImageView.xImageView.StartPaintTimer();
				RecvImgProcessor.StartReceive();
			}
			else
			{
				RecvImgProcessor.Close();
				xExDynamicGainDefectImage.xImageView.xImageView.StopPaintTimer();
				ReleaseResource();
				generateExDynamicGainDefectPageVM.RemoveCallbackHandler();
			}
		}

		private void ShowImage(byte[] data, int width, int height)
		{
			ShowImage(new IRayImageData(data, width, height));
		}

		private void ShowImage(IRayImageData image)
		{
			RecvImgProcessor.ReceiveImage(image);
		}

		private void PaintImage(IRayImageData image)
		{
			xExDynamicGainDefectImage.xImageView.UpdateImage(image);
		}

		private void AdjustWinLevelAndWinWidth()
		{
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				ushort[] oriGray16ImgBuffer = xExDynamicGainDefectImage.xImageView.xImageView.OriGray16ImgBuffer;
				int winLevel = 0;
				int winWidth = 0;
				CalcWinLevelWinWidth.GetWinLevelAndWidth(oriGray16ImgBuffer, ref winLevel, ref winWidth, Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
				xExDynamicGainDefectImage.xImageView.xImageView.SetWindow(winWidth, winLevel);
			});
		}

		private void ClosePlaybar()
		{
			if (mCinePlayCtrl != null)
			{
				xExDynamicGainDefectImage.xImageView.xPlayerBar.Close();
				xExDynamicGainDefectImage.xImageView.xPlayerBar.SetDataContext(null);
				mCinePlayCtrl.Close();
				mCinePlayCtrl = null;
			}
		}

		private void StartStoreCineData(int groupNo)
		{
			RecvImgProcessor.StartBufferCine(CineBufferName[ImageListVM.SelectedIndex], GroupLightFrames, Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
		}

		private void StopStoreCineData(bool updateCover = true)
		{
			RecvImgProcessor.StopBufferCine();
			if (updateCover)
			{
				base.Dispatcher.Invoke((Action)delegate
				{
					ImageListVM.UpdateImage(ImageListVM.SelectedIndex, RecvImgProcessor.LastImage);
				}, new object[0]);
			}
		}

		private void ReleaseResource()
		{
			string[] cineBufferName = CineBufferName;
			foreach (string path in cineBufferName)
			{
				if (File.Exists(path))
				{
					File.Delete(path);
				}
			}
		}

		public bool GetCancelStatus()
		{
			return generateExDynamicGainDefectPageVM.GetCancelStatus();
		}

		public void SetCancelStatus(bool bValue)
		{
			generateExDynamicGainDefectPageVM.SetCancelStatus(bValue);
		}

		public void DoCancel()
		{
			generateExDynamicGainDefectPageVM.DoCancel();
		}

		private void InitImageSize()
		{
			int num = Math.Min(Instance.Detector.Prop_Attr_Width, 640);
			int height = Instance.Detector.Prop_Attr_Height * num / Instance.Detector.Prop_Attr_Width;
			xExDynamicGainDefectImage.xImageView.SetDisplaySize(num, height);
		}

		public void SetCurSubset(string strSubset)
		{
			generateExDynamicGainDefectPageVM.SetCurSubset(strSubset);
		}

		public void Clear()
		{
			ImageListVM.ClearThumbs();
			xExDynamicGainDefectImage.xImageView.xPlayerBar.Close();
			xExDynamicGainDefectImage.xImageView.xPlayerBar.SetDataContext(null);
			ImageListVM.DisplayThumb -= ShowImage;
			generateExDynamicGainDefectPageVM.ShowImage -= ShowImage;
			base.IsVisibleChanged -= VisibleChanged;
			xExDynamicGainDefectImage.xImageView.SetDisplaySize(4, 4);
			xExDynamicGainDefectImage.xImageView.OnClose();
			GC.Collect();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateexdynamicgaindefectpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xExDynamicGainDefectImage = (GenerateExTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
