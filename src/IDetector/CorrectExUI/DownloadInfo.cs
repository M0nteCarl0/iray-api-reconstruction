using iDetector;

namespace CorrectExUI
{
	public struct DownloadInfo
	{
		public int index;

		public Enm_FileTypes fileType;

		public string modeName;

		public string fullPath;

		public string desc;

		public int indexCount;
	}
}
