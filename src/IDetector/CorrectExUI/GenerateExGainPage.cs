using CorrectExUI.ViewModel;
using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class GenerateExGainPage : UserControl, IComponentConnector
	{
		private DetecInfo Instance;

		private GenerateExGainPageVM generateExGainPageVM;

		private CorrectionImageListVM ImageListVM;

		internal GenerateExTemplatePage xExGainAcquireImage;

		private bool _contentLoaded;

		public GenerateExGainPage(DetecInfo instance)
		{
			InitializeComponent();
			Instance = instance;
		}

		public void Initialize()
		{
			ImageListVM = new CorrectionImageListVM();
			ImageListVM.DisplayThumb += ShowImage;
			generateExGainPageVM = new GenerateExGainPageVM(Instance);
			base.DataContext = generateExGainPageVM;
			generateExGainPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			xExGainAcquireImage.xImageView.SetDisplaySize(Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
			generateExGainPageVM.exposeWindowControl = xExGainAcquireImage.xExposeWindowWnd;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				generateExGainPageVM.AddCallbackHandler();
				generateExGainPageVM.UpdateOffsetType();
				xExGainAcquireImage.xImageView.SetDisplaySize(Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
			}
			else
			{
				generateExGainPageVM.RemoveCallbackHandler();
				xExGainAcquireImage.xImageView.SetDisplaySize(4, 4);
			}
		}

		private void ShowImage(IRayImageData image)
		{
			xExGainAcquireImage.xImageView.DisplayImage(image);
		}

		public bool GetCancelStatus()
		{
			return generateExGainPageVM.GetCancelStatus();
		}

		public void SetCancelStatus(bool bValue)
		{
			generateExGainPageVM.SetCancelStatus(bValue);
		}

		public void DoCancel()
		{
			generateExGainPageVM.DoCancel();
		}

		public void Clear()
		{
			ImageListVM.ClearThumbs();
			xExGainAcquireImage.xImageView.xPlayerBar.Close();
			xExGainAcquireImage.xImageView.xPlayerBar.SetDataContext(null);
			ImageListVM.DisplayThumb -= ShowImage;
			generateExGainPageVM.ShowImage -= ShowImage;
			generateExGainPageVM.Release();
			base.IsVisibleChanged -= VisibleChanged;
			xExGainAcquireImage.xImageView.SetDisplaySize(4, 4);
			xExGainAcquireImage.xImageView.OnClose();
			GC.Collect();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateexgainpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xExGainAcquireImage = (GenerateExTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
