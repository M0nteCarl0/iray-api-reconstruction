using iDetector;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class CorrectionExModeFileStaticPage : UserControl, INotifyPropertyChanged, IComponentConnector
	{
		private string modeMessageColor;

		private string fileMessageColor;

		private Detector Instance;

		private SDKInvokeProxy SdkInvokeProxy;

		public ToolTipMsgDel ShowToolTip;

		private string[] templateStateInfo = new string[5]
		{
			"absent",
			"valid",
			"valid",
			"invalid",
			"unmatched"
		};

		private string[] fileTypeName = new string[6]
		{
			"",
			"Offset",
			"Gain",
			"MostGain",
			"Defect",
			"Lag"
		};

		private DlgTaskTimeOut MessageBoxTimer;

		private int _nIndex;

		private string _strDesc;

		private int _autoUpdateStatus;

		private int _loadFileStatus;

		private List<DownloadInfo> _downloadList;

		private DownloadCorrProgressWnd _downloadWnd;

		private string TemplatePath = "Correct";

		private UploadFileInfo _uploadInfo;

		private string WarningColor = "#EE7600";

		private string NormalColor = "";

		internal ListView _ModeList;

		internal GridView xSubsetListView;

		internal Button btnImportToWorkdir;

		internal Button btnModeDownloadToFPD;

		internal TextBlock _txtModeMessage;

		internal Button btnReadSubsetStatus;

		internal GroupBox gBoxFpdTempFile;

		internal ListView xListView;

		internal GridView xView;

		internal GridViewColumn xType;

		internal GridViewColumn xIndex;

		internal GridViewColumn xActivity;

		internal GridViewColumn xDesp;

		internal Button btnFileUploadWorkdir;

		internal Button btnFileUploadLag;

		internal Button btnFileActive;

		internal Button btnFileUpdateHWPreOffset;

		internal TextBlock _txtFileMessage;

		internal Button btnReadStatus;

		private bool _contentLoaded;

		public string ModeMessageColor
		{
			get
			{
				return modeMessageColor;
			}
			set
			{
				modeMessageColor = value;
				NotifyPropertyChanged("ModeMessageColor");
			}
		}

		public string FileMessageColor
		{
			get
			{
				return fileMessageColor;
			}
			set
			{
				fileMessageColor = value;
				NotifyPropertyChanged("FileMessageColor");
			}
		}

		private bool bInitedLayout
		{
			get;
			set;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public event Action UpdateDetPageParams;

		public void NotifyPropertyChanged(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		public CorrectionExModeFileStaticPage(Detector instance)
		{
			InitializeComponent();
			Instance = instance;
			if (this.SdkEventHandler == null)
			{
				this.SdkEventHandler = ProcessEvent;
			}
			Instance.SubscribeCBEvent(OnSdkCallback);
			SdkInvokeProxy = new SDKInvokeProxy(Instance, new Shell(Application.Current.MainWindow));
			_loadFileStatus = 0;
			bInitedLayout = false;
			_downloadList = new List<DownloadInfo>();
			_autoUpdateStatus = 3;
			base.IsVisibleChanged += VisibleChanged;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.NewValue.Equals(true))
			{
				if (Instance != null)
				{
					InitSubsetSettings();
					Instance.SubscribeCBEvent(OnSdkCallback);
				}
			}
			else
			{
				Instance.UnSubscribeCBEvent(OnSdkCallback);
			}
		}

		public void Clear()
		{
			base.IsVisibleChanged -= VisibleChanged;
			this.SdkEventHandler = null;
			SdkInvokeProxy = null;
			this.UpdateDetPageParams = null;
			GC.Collect();
		}

		public void Initialize()
		{
			InitUILayout();
		}

		private void InitSubsetSettings()
		{
			_ModeList.Items.Clear();
			bool flag = false;
			string path = Instance.Prop_Attr_WorkDir + TemplatePath + "\\";
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			DirectoryInfo[] directories = directoryInfo.GetDirectories();
			foreach (DirectoryInfo directoryInfo2 in directories)
			{
				bool flag2 = false;
				bool flag3 = false;
				bool flag4 = false;
				bool flag5 = false;
				SubsetSettingsList subsetSettingsList = new SubsetSettingsList();
				subsetSettingsList.SubsetName = directoryInfo2.Name;
				string path2 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + directoryInfo2.Name + "\\";
				string text = Instance.Prop_Cfg_DefaultSubset;
				if (string.IsNullOrEmpty(text))
				{
					text = "Default";
				}
				DirectoryInfo directoryInfo3 = new DirectoryInfo(path2);
				FileInfo[] files = directoryInfo3.GetFiles();
				foreach (FileInfo fileInfo in files)
				{
					string name = fileInfo.Name;
					string text2 = name.Substring(name.LastIndexOf(".") + 1, name.Length - name.LastIndexOf(".") - 1);
					if (text2.ToLower().Equals("off"))
					{
						subsetSettingsList.OffsetValidity = "valid";
						flag2 = true;
					}
					string gainName = GetGainName(Instance);
					string path3 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + gainName;
					if (!string.IsNullOrEmpty(gainName) && File.Exists(path3))
					{
						subsetSettingsList.GainValidity = "valid";
						flag3 = true;
					}
					gainName = GetDefectName(Instance);
					path3 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + gainName;
					if (!string.IsNullOrEmpty(gainName) && File.Exists(path3))
					{
						subsetSettingsList.DefectValidity = "valid";
						flag4 = true;
					}
					gainName = GetLagName(Instance);
					path3 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + gainName;
					if (!string.IsNullOrEmpty(gainName) && File.Exists(path3))
					{
						subsetSettingsList.LagValidity = "valid";
						flag5 = true;
					}
				}
				if (!flag2)
				{
					subsetSettingsList.OffsetValidity = "absent";
				}
				if (!flag3)
				{
					subsetSettingsList.GainValidity = "absent";
				}
				if (!flag4)
				{
					subsetSettingsList.DefectValidity = "absent";
				}
				if (!flag5)
				{
					subsetSettingsList.LagValidity = "absent";
				}
				if (!flag && text == directoryInfo2.Name)
				{
					subsetSettingsList.Activity = "enable";
					flag = true;
				}
				_ModeList.Items.Add(subsetSettingsList);
			}
		}

		private void InitUILayout()
		{
			if (Instance != null && !bInitedLayout)
			{
				bInitedLayout = true;
				switch (Instance.Prop_Attr_UROM_ProductNo)
				{
				case 32:
				case 37:
				case 39:
				case 41:
				case 42:
				case 45:
				case 51:
				case 52:
				case 59:
				case 60:
				case 61:
				case 62:
				case 72:
				case 80:
				case 88:
				case 91:
				case 93:
					return;
				}
				gBoxFpdTempFile.Visibility = Visibility.Collapsed;
				btnModeDownloadToFPD.Visibility = Visibility.Collapsed;
			}
		}

		private void EnableView(bool bEnable)
		{
			base.IsEnabled = bEnable;
		}

		private void UpdateApplicationModeEx()
		{
			CloseMessageBox();
			if (this.UpdateDetPageParams != null)
			{
				this.UpdateDetPageParams();
			}
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		public void ParseTemplateList(string xmlInfo)
		{
			if (xmlInfo == null)
			{
				return;
			}
			XmlParser xmlParser = new XmlParser();
			xmlParser.LoadXml(xmlInfo);
			List<string> nodes = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Index");
			if (nodes == null || nodes.Count == 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "ParseTemplateList: HWFile list is null";
				return;
			}
			string singleNoteValue = xmlParser.GetSingleNoteValue("HWCalibrationFileList/FileItem/Type");
			List<string> nodes2 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Activity");
			List<string> nodes3 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Validity");
			List<string> nodes4 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/DespInfo");
			string type = "";
			switch (Convert.ToInt32(singleNoteValue))
			{
			case 2:
				type = "Gain";
				break;
			case 4:
				type = "Defect";
				break;
			case 5:
				type = "Lag";
				break;
			}
			for (int i = 0; i < nodes.Count; i++)
			{
				FPDCorrFileList fPDCorrFileList = new FPDCorrFileList();
				fPDCorrFileList.Type = type;
				if (i < nodes.Count)
				{
					fPDCorrFileList.Index = nodes[i];
				}
				if (i < nodes2.Count)
				{
					fPDCorrFileList.Activity = nodes2[i];
				}
				if (i < nodes3.Count)
				{
					fPDCorrFileList.Validity = nodes3[i];
				}
				if (i < nodes4.Count)
				{
					fPDCorrFileList.DespInfo = nodes4[i];
				}
				xListView.Items.Add(fPDCorrFileList);
			}
		}

		private void UpdateFPDCorrFileStatus(Enm_FileTypes FileType)
		{
			int num = SdkInvokeProxy.Invoke(3021, (int)FileType);
			if (1 == num)
			{
			}
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			switch (nEventID)
			{
			case 2006:
				if (ShowToolTip != null)
				{
					if (nParam2 == 0)
					{
						ShowToolTip(sender.Prop_Attr_UROM_SerialNo + ": " + fileTypeName[nParam1] + " has expired!");
					}
					else
					{
						ShowToolTip(sender.Prop_Attr_UROM_SerialNo + ": " + fileTypeName[nParam1] + " will expire in " + nParam2 + " minute");
					}
				}
				break;
			case 4:
				switch (nParam1)
				{
				case 7:
					EnableView(true);
					UpdateApplicationModeEx();
					break;
				case 2001:
					EnableView(true);
					UpdateApplicationModeEx();
					break;
				case 3017:
					if (_loadFileStatus > 0)
					{
						_loadFileStatus--;
						int fileType = (int)_downloadList[_loadFileStatus].fileType;
						string fullPath = _downloadList[_loadFileStatus].fullPath;
						int num = 0;
						num = ((fileType == 5) ? 1 : _nIndex);
						int num2 = SdkInvokeProxy.Invoke(3017, fileType, num, fullPath, _strDesc);
						if (1 != num2 && num2 != 0)
						{
							if (_downloadWnd != null)
							{
								_downloadWnd.OnClose();
							}
							ModeMessageColor = WarningColor;
							_txtModeMessage.Text = "Download failed!";
							return;
						}
						switch (fileType)
						{
						case 2:
							ModeMessageColor = NormalColor;
							_txtModeMessage.Text = "Downloading gain ...";
							break;
						case 4:
							ModeMessageColor = NormalColor;
							_txtModeMessage.Text = "Downloading defect ...";
							break;
						case 5:
							ModeMessageColor = NormalColor;
							_txtModeMessage.Text = "Downloading lag ...";
							break;
						}
					}
					else
					{
						if (_downloadWnd != null)
						{
							_downloadWnd.OnClose();
						}
						ModeMessageColor = NormalColor;
						_txtModeMessage.Text = "Download succeed!";
					}
					break;
				case 3018:
					FileMessageColor = NormalColor;
					_txtFileMessage.Text = "Upload FPD file succeed!";
					InitSubsetSettings();
					break;
				case 3021:
					_autoUpdateStatus--;
					if (_autoUpdateStatus == 2)
					{
						UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Defect);
					}
					if (_autoUpdateStatus == 1)
					{
						UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Lag);
					}
					else if (_autoUpdateStatus == 0)
					{
						EnableView(true);
						FileMessageColor = NormalColor;
						_txtFileMessage.Text = "Query FPD file succeed!";
					}
					break;
				case 3019:
					xListView.Items.Clear();
					UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Gain);
					break;
				default:
					EnableView(true);
					break;
				case 2002:
					break;
				}
				Instance.TASK = "No Task";
				break;
			case 5:
				switch (nParam1)
				{
				case 7:
					CloseMessageBox();
					EnableView(true);
					break;
				case 2001:
				case 2002:
					CloseMessageBox();
					EnableView(true);
					break;
				case 3017:
					if (_downloadWnd != null)
					{
						_downloadWnd.OnClose();
					}
					_loadFileStatus = 0;
					ModeMessageColor = WarningColor;
					_txtModeMessage.Text = "Download failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 3018:
					FileMessageColor = WarningColor;
					_txtFileMessage.Text = "Upload failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 3021:
					EnableView(true);
					FileMessageColor = WarningColor;
					_txtFileMessage.Text = "Query Template FileList failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 3019:
					EnableView(true);
					FileMessageColor = WarningColor;
					_txtFileMessage.Text = "Select failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				default:
					EnableView(true);
					break;
				case 2:
					break;
				}
				Instance.TASK = "No Task";
				break;
			case 12:
				ParseTemplateList(pParam as string);
				break;
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			object obj = null;
			if (nEventID == 12)
			{
				string text = Marshal.PtrToStringAuto(pParam);
				obj = text;
			}
			base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, obj);
		}

		public void RemoveCallbackHandler()
		{
			Instance.UnSubscribeCBEvent(OnSdkCallback);
		}

		private void btnImportToWorkdir_Click(object sender, RoutedEventArgs e)
		{
			ModeMessageColor = NormalColor;
			_txtModeMessage.Text = "";
			if (_ModeList.SelectedIndex < 0)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Please select a item!";
				return;
			}
			SubsetSettingsList subsetSettingsList = _ModeList.SelectedItem as SubsetSettingsList;
			int prop_Attr_Width = Instance.Prop_Attr_Width;
			int prop_Attr_Height = Instance.Prop_Attr_Height;
			string path = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\";
			ImportToWorkdirWnd importToWorkdirWnd = new ImportToWorkdirWnd(path, prop_Attr_Width, prop_Attr_Height);
			importToWorkdirWnd.ShowDialog();
			InitSubsetSettings();
		}

		private void btnModeDownloadToFPD_Click(object sender, RoutedEventArgs e)
		{
			_loadFileStatus = 0;
			ModeMessageColor = NormalColor;
			_txtModeMessage.Text = "";
			int selectedIndex = _ModeList.SelectedIndex;
			if (selectedIndex < 0)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Please select a item!";
				return;
			}
			SubsetSettingsList subsetSettingsList = _ModeList.SelectedItem as SubsetSettingsList;
			InitSubsetSettings();
			int count = _ModeList.Items.Count;
			if (count < 1)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "No subsetSet item!";
				return;
			}
			_downloadList.Clear();
			if (subsetSettingsList.GainValidity != "absent")
			{
				string gainName = GetGainName(Instance);
				string text = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + gainName;
				if (!string.IsNullOrEmpty(gainName) && File.Exists(text))
				{
					FileInfo fileInfo = new FileInfo(text);
					DownloadInfo item = default(DownloadInfo);
					item.index = 1;
					item.modeName = subsetSettingsList.SubsetName;
					item.fileType = Enm_FileTypes.Enm_File_Gain;
					item.fullPath = text;
					item.desc = fileInfo.LastWriteTime.ToString();
					item.indexCount = 10;
					_downloadList.Add(item);
					_loadFileStatus++;
				}
			}
			if (subsetSettingsList.DefectValidity != "absent")
			{
				string defectName = GetDefectName(Instance);
				string text2 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + defectName;
				if (!string.IsNullOrEmpty(defectName) && File.Exists(text2))
				{
					FileInfo fileInfo2 = new FileInfo(text2);
					DownloadInfo item2 = default(DownloadInfo);
					item2.index = 1;
					item2.modeName = subsetSettingsList.SubsetName;
					item2.fileType = Enm_FileTypes.Enm_File_Defect;
					item2.fullPath = text2;
					item2.desc = fileInfo2.LastWriteTime.ToString();
					item2.indexCount = 10;
					_downloadList.Add(item2);
					_loadFileStatus++;
				}
			}
			if (subsetSettingsList.LagValidity != "absent")
			{
				string lagName = GetLagName(Instance);
				string text3 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + lagName;
				if (!string.IsNullOrEmpty(lagName) && File.Exists(text3))
				{
					FileInfo fileInfo3 = new FileInfo(text3);
					DownloadInfo item3 = default(DownloadInfo);
					item3.index = 1;
					item3.modeName = subsetSettingsList.SubsetName;
					item3.fileType = Enm_FileTypes.Enm_File_Lag;
					item3.fullPath = text3;
					item3.desc = fileInfo3.LastWriteTime.ToString();
					item3.indexCount = 10;
					_downloadList.Add(item3);
					_loadFileStatus++;
				}
			}
			if (_loadFileStatus == 0)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Template file does not exist!";
				return;
			}
			DownloadToFpdWnd downloadToFpdWnd = new DownloadToFpdWnd(_downloadList);
			if (downloadToFpdWnd.ShowDialog() != true)
			{
				return;
			}
			_loadFileStatus--;
			_nIndex = downloadToFpdWnd.nIndex;
			_strDesc = downloadToFpdWnd.strDesc;
			int fileType = (int)_downloadList[_loadFileStatus].fileType;
			string fullPath = _downloadList[_loadFileStatus].fullPath;
			int num = 0;
			num = ((fileType == 5) ? 1 : _nIndex);
			int num2 = SdkInvokeProxy.Invoke(3017, fileType, num, fullPath, _strDesc);
			if (1 == num2 || num2 == 0)
			{
				switch (fileType)
				{
				case 2:
					ModeMessageColor = NormalColor;
					_txtModeMessage.Text = "Downloading gain ...";
					break;
				case 4:
					ModeMessageColor = NormalColor;
					_txtModeMessage.Text = "Downloading defect ...";
					break;
				case 5:
					ModeMessageColor = NormalColor;
					_txtModeMessage.Text = "Downloading lag ...";
					break;
				}
				_downloadWnd = new DownloadCorrProgressWnd(Instance, "Download File Progress");
				_downloadWnd.ShowDialog();
				Instance.AttrChangingMonitor.AddRef(5016);
			}
		}

		private void btnFileUploadWorkdir_Click(object sender, RoutedEventArgs e)
		{
			if (_ModeList.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item in the subset settings!";
				return;
			}
			if (xListView.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item in the fpd template file!";
				return;
			}
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			SubsetSettingsList subsetSettingsList = _ModeList.SelectedItem as SubsetSettingsList;
			FPDCorrFileList fPDCorrFileList = xListView.SelectedItem as FPDCorrFileList;
			int num = int.Parse(fPDCorrFileList.Index);
			int nPara = 0;
			if (fPDCorrFileList.Type == "Gain")
			{
				nPara = 2;
			}
			else if (fPDCorrFileList.Type == "Defect")
			{
				nPara = 4;
			}
			else if (fPDCorrFileList.Type == "Lag")
			{
				num = 1;
				nPara = 5;
			}
			string binning = fPDCorrFileList.Binning;
			_uploadInfo.uploadInfo = "index" + num + ": " + fPDCorrFileList.Type;
			_uploadInfo.modeName = subsetSettingsList.SubsetName;
			string lpFileName = Instance.Prop_Attr_WorkDir + "\\iDetectorConfig.ini";
			string lpKeyName = string.Format("Binning_{0}", fPDCorrFileList.Type);
			string privateProfileString = IniParser.GetPrivateProfileString("UploadFileName", lpKeyName, "", lpFileName);
			string text = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + privateProfileString;
			if (File.Exists(text))
			{
				_uploadInfo.noticeInfo = "Overwrite exits file!";
			}
			else
			{
				_uploadInfo.noticeInfo = "";
			}
			UploadToWorkdirWnd uploadToWorkdirWnd = new UploadToWorkdirWnd(_uploadInfo);
			if (uploadToWorkdirWnd.ShowDialog() == true)
			{
				int num2 = SdkInvokeProxy.Invoke(3018, nPara, num, text);
				if (1 == num2)
				{
				}
			}
		}

		private void btnReadStatus_Click(object sender, RoutedEventArgs e)
		{
			_autoUpdateStatus = 3;
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			xListView.Items.Clear();
			EnableView(false);
			UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Gain);
		}

		private void btnFileActive_Click(object sender, RoutedEventArgs e)
		{
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			if (xListView.Items.Count == 0)
			{
				return;
			}
			if (xListView.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item!";
				return;
			}
			_autoUpdateStatus = 3;
			FPDCorrFileList fPDCorrFileList = xListView.SelectedItem as FPDCorrFileList;
			int nPara = 0;
			if (fPDCorrFileList.Type == "Gain")
			{
				nPara = 2;
			}
			else if (fPDCorrFileList.Type == "Defect")
			{
				nPara = 4;
			}
			else if (fPDCorrFileList.Type == "Lag")
			{
				nPara = 5;
			}
			EnableView(false);
			int num = SdkInvokeProxy.Invoke(3019, nPara, Convert.ToInt32(fPDCorrFileList.Index));
			if (1 == num)
			{
			}
		}

		private void btnFileUpdateHWPreOffset_Click(object sender, RoutedEventArgs e)
		{
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			int num = SdkInvokeProxy.Invoke(3020);
			if (1 == num)
			{
				EnableView(false);
			}
		}

		private string GetGainName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("gain_{0}x{1}.gn", width, height);
		}

		private string GetDefectName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("defect_{0}x{1}.dft", width, height);
		}

		private string GetLagName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("lag_{0}x{1}_matrix.lag", width, height);
		}

		private bool GetWidthAndHeight(Detector instance, out int width, out int height)
		{
			width = 0;
			height = 0;
			switch (instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 56:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					width = 1024;
					height = 1024;
					break;
				case 1:
					width = 512;
					height = 512;
					break;
				default:
					return false;
				}
				break;
			case 49:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					width = 2048;
					height = 2048;
					break;
				case 1:
					width = 1024;
					height = 1024;
					break;
				case 3:
					width = 512;
					height = 512;
					break;
				default:
					return false;
				}
				break;
			case 48:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					switch (instance.Prop_Attr_UROM_ZoomMode)
					{
					case 0:
					case 8:
						width = 3072;
						height = 3072;
						break;
					case 9:
						width = 2048;
						height = 2048;
						break;
					default:
						return false;
					}
					break;
				case 1:
					switch (instance.Prop_Attr_UROM_ZoomMode)
					{
					case 0:
					case 10:
						width = 1536;
						height = 1536;
						break;
					case 11:
						width = 1024;
						height = 1024;
						break;
					default:
						return false;
					}
					break;
				case 2:
				{
					Enm_Zoom prop_Attr_UROM_ZoomMode = (Enm_Zoom)instance.Prop_Attr_UROM_ZoomMode;
					if (prop_Attr_UROM_ZoomMode == Enm_Zoom.Enm_Zoom_Null || prop_Attr_UROM_ZoomMode == Enm_Zoom.Enm_Zoom_1024x1024)
					{
						width = 1024;
						height = 1024;
						break;
					}
					return false;
				}
				default:
					return false;
				}
				break;
			default:
				width = instance.Prop_Attr_Width;
				height = instance.Prop_Attr_Height;
				break;
			}
			return true;
		}

		private void btnReadSubsetStatus_Click(object sender, RoutedEventArgs e)
		{
			InitSubsetSettings();
		}

		private void btnFileUploadLag_Click(object sender, RoutedEventArgs e)
		{
			if (_ModeList.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item in the subset settings!";
				return;
			}
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			SubsetSettingsList subsetSettingsList = _ModeList.SelectedItem as SubsetSettingsList;
			int num = 1;
			int nPara = 5;
			string text = "Lag";
			_uploadInfo.uploadInfo = "index" + num + ": " + text;
			_uploadInfo.modeName = subsetSettingsList.SubsetName;
			string lpFileName = Instance.Prop_Attr_WorkDir + "\\iDetectorConfig.ini";
			string lpKeyName = string.Format("Binning_{0}", text);
			string privateProfileString = IniParser.GetPrivateProfileString("UploadFileName", lpKeyName, "", lpFileName);
			string text2 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + subsetSettingsList.SubsetName + "\\" + privateProfileString;
			if (File.Exists(text2))
			{
				_uploadInfo.noticeInfo = "Overwrite exits file!";
			}
			else
			{
				_uploadInfo.noticeInfo = "";
			}
			UploadToWorkdirWnd uploadToWorkdirWnd = new UploadToWorkdirWnd(_uploadInfo);
			if (uploadToWorkdirWnd.ShowDialog() == true)
			{
				int num2 = SdkInvokeProxy.Invoke(3018, nPara, num, text2);
				if (1 == num2)
				{
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionexmodefilestaticpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_ModeList = (ListView)target;
				break;
			case 2:
				xSubsetListView = (GridView)target;
				break;
			case 3:
				btnImportToWorkdir = (Button)target;
				btnImportToWorkdir.Click += btnImportToWorkdir_Click;
				break;
			case 4:
				btnModeDownloadToFPD = (Button)target;
				btnModeDownloadToFPD.Click += btnModeDownloadToFPD_Click;
				break;
			case 5:
				_txtModeMessage = (TextBlock)target;
				break;
			case 6:
				btnReadSubsetStatus = (Button)target;
				btnReadSubsetStatus.Click += btnReadSubsetStatus_Click;
				break;
			case 7:
				gBoxFpdTempFile = (GroupBox)target;
				break;
			case 8:
				xListView = (ListView)target;
				break;
			case 9:
				xView = (GridView)target;
				break;
			case 10:
				xType = (GridViewColumn)target;
				break;
			case 11:
				xIndex = (GridViewColumn)target;
				break;
			case 12:
				xActivity = (GridViewColumn)target;
				break;
			case 13:
				xDesp = (GridViewColumn)target;
				break;
			case 14:
				btnFileUploadWorkdir = (Button)target;
				btnFileUploadWorkdir.Click += btnFileUploadWorkdir_Click;
				break;
			case 15:
				btnFileUploadLag = (Button)target;
				btnFileUploadLag.Click += btnFileUploadLag_Click;
				break;
			case 16:
				btnFileActive = (Button)target;
				btnFileActive.Click += btnFileActive_Click;
				break;
			case 17:
				btnFileUpdateHWPreOffset = (Button)target;
				btnFileUpdateHWPreOffset.Click += btnFileUpdateHWPreOffset_Click;
				break;
			case 18:
				_txtFileMessage = (TextBlock)target;
				break;
			case 19:
				btnReadStatus = (Button)target;
				btnReadStatus.Click += btnReadStatus_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
