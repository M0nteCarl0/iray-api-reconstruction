using CorrectUI;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class GenerateExTemplatePage : UserControl, IComponentConnector
	{
		internal Label _txtMessage;

		internal Button btnStart;

		internal Button btnCancel;

		internal Button btnGenerate;

		internal CheckBox btnCheckDownload;

		internal Label labCurOffsetType;

		internal CorrectionImageView xImageView;

		internal ExposeWndUserControl xExposeWindowWnd;

		internal TextBox labStage;

		internal TextBox labKV;

		internal TextBox labExpValue;

		internal Button btnAcquire;

		internal TextBox labAcquiedProgress;

		internal TextBox labCurValue;

		internal Button btnAccept;

		internal Button xLoadFile;

		private bool _contentLoaded;

		public GenerateExTemplatePage()
		{
			InitializeComponent();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateextemplatepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		internal Delegate _CreateDelegate(Type delegateType, string handler)
		{
			return Delegate.CreateDelegate(delegateType, this, handler);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_txtMessage = (Label)target;
				break;
			case 2:
				btnStart = (Button)target;
				break;
			case 3:
				btnCancel = (Button)target;
				break;
			case 4:
				btnGenerate = (Button)target;
				break;
			case 5:
				btnCheckDownload = (CheckBox)target;
				break;
			case 6:
				labCurOffsetType = (Label)target;
				break;
			case 7:
				xImageView = (CorrectionImageView)target;
				break;
			case 8:
				xExposeWindowWnd = (ExposeWndUserControl)target;
				break;
			case 9:
				labStage = (TextBox)target;
				break;
			case 10:
				labKV = (TextBox)target;
				break;
			case 11:
				labExpValue = (TextBox)target;
				break;
			case 12:
				btnAcquire = (Button)target;
				break;
			case 13:
				labAcquiedProgress = (TextBox)target;
				break;
			case 14:
				labCurValue = (TextBox)target;
				break;
			case 15:
				btnAccept = (Button)target;
				break;
			case 16:
				xLoadFile = (Button)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
