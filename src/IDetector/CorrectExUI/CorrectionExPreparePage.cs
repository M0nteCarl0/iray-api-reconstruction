using CorrectExUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class CorrectionExPreparePage : UserControl, IComponentConnector
	{
		private Detector Instance;

		private CorrectionExPreparePageVM correctionExPreparePageVM;

		private bool _bCorrectError;

		private bool _contentLoaded;

		public CorrectionExPreparePage()
		{
		}

		public CorrectionExPreparePage(Detector instance)
		{
			InitializeComponent();
			Instance = instance;
		}

		public void Initialize()
		{
			correctionExPreparePageVM = new CorrectionExPreparePageVM(Instance);
			correctionExPreparePageVM.InitializeGui();
			base.DataContext = correctionExPreparePageVM;
			_bCorrectError = false;
		}

		private void btnStartCreateTemplate(object sender, RoutedEventArgs e)
		{
			using (CreateCorrectTemplateWnd createCorrectTemplateWnd = new CreateCorrectTemplateWnd(Instance))
			{
				try
				{
					createCorrectTemplateWnd.ShowDialog();
					_bCorrectError = createCorrectTemplateWnd.bBreakError;
				}
				catch (Exception ex)
				{
					Log.Instance().Write("CreateTemplateWnd failed:{0}", ex.Message);
				}
			}
			GC.Collect();
		}

		public bool GetCorrectBreakError()
		{
			return _bCorrectError;
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionexpreparepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((Button)target).Click += btnStartCreateTemplate;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
