using CorrectExUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class GenerateExOffsetPage : UserControl, IComponentConnector
	{
		private DetecInfo Instance;

		private GenerateExOffsetPageVM offsetExGenPageVM;

		private bool bEnableSkip;

		internal Button btnStartCreate;

		internal Button btnCancel;

		internal Label xInfo;

		private bool _contentLoaded;

		public GenerateExOffsetPage(DetecInfo instance)
		{
			InitializeComponent();
			Initialize(instance);
		}

		private void Initialize(DetecInfo instance)
		{
			Instance = instance;
			offsetExGenPageVM = new GenerateExOffsetPageVM(Instance);
			base.DataContext = offsetExGenPageVM;
			base.IsVisibleChanged += VisibleChanged;
			bEnableSkip = true;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				offsetExGenPageVM.AddCallbackHandler();
			}
			else
			{
				offsetExGenPageVM.RemoveCallbackHandler();
			}
		}

		private void btnStartCreate_Click(object sender, RoutedEventArgs e)
		{
			if (CanCreateTemplate())
			{
				offsetExGenPageVM.StartGenerateOffset(bEnableSkip);
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			offsetExGenPageVM.CancelGenerateOffset();
		}

		private bool CanCreateTemplate()
		{
			bool result = true;
			switch (Instance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 48:
			case 49:
			case 56:
				if (Instance.Detector.Prop_Attr_OffsetTotalFrames == 0)
				{
					MessageBox.Show("Frame number cannot be 0, Please reset the value in configure file!");
					result = false;
				}
				break;
			case 11:
			{
				int prop_Attr_UROM_ExpMode = Instance.Detector.Prop_Attr_UROM_ExpMode;
				if (prop_Attr_UROM_ExpMode == 1 || prop_Attr_UROM_ExpMode == 2)
				{
					MessageBox.Show("Correction template cann't be created in the Manual/AEC exposure mode!");
					result = false;
				}
				break;
			}
			}
			return result;
		}

		public void DoCancel()
		{
			offsetExGenPageVM.DoCancel();
		}

		public bool GetCancelStatus()
		{
			return offsetExGenPageVM.GetCancelStatus();
		}

		public void SetCancelStatus(bool bValue)
		{
			offsetExGenPageVM.SetCancelStatus(bValue);
		}

		public void Clear()
		{
			base.IsVisibleChanged -= VisibleChanged;
			offsetExGenPageVM.Release();
			GC.Collect();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateexoffsetpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				btnStartCreate = (Button)target;
				btnStartCreate.Click += btnStartCreate_Click;
				break;
			case 2:
				btnCancel = (Button)target;
				btnCancel.Click += btnCancel_Click;
				break;
			case 3:
				xInfo = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
