using CorrectExUI.ViewModel;
using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class GenerateExDynamicAcqPage : UserControl, IComponentConnector
	{
		private GenerateExDynamicTemplate GenerateExDynamicAcqPageVM;

		private CorrectionTemplateImageList ImageListVM;

		private DetecInfo Instance;

		private string[] CineBufferName;

		private int GroupNumber = 3;

		private int GroupLightFrames;

		private Enm_FileTypes _fileType;

		internal GenerateExTemplatePage xExDynamicGainAcqPage;

		private bool _contentLoaded;

		public GenerateExDynamicAcqPage(DetecInfo instance, Enm_FileTypes filetype)
		{
			InitializeComponent();
			Instance = instance;
			_fileType = filetype;
		}

		public void Initialize()
		{
			CineBufferName = new string[GroupNumber];
			GroupLightFrames = Instance.Detector.Prop_Attr_DefectTotalFrames / GroupNumber / 2;
			string text = Instance.Detector.Prop_Attr_WorkDir + "temp\\";
			for (int i = 0; i < CineBufferName.Length; i++)
			{
				CineBufferName[i] = text + i.ToString() + "-" + GroupLightFrames.ToString() + "Cine.cin";
			}
			if (!Utility.IsDirectoryExisted(text))
			{
				Utility.CreateDirectory(text);
			}
			ImageListVM = new CorrectionImageListVM();
			ImageListVM.DisplayThumb += ShowImage;
			if (_fileType == Enm_FileTypes.Enm_File_Gain)
			{
				xExDynamicGainAcqPage.xLoadFile.Visibility = Visibility.Hidden;
				GenerateExDynamicAcqPageVM = new GenerateExDynamicGainPageVM(Instance);
				GenerateExDynamicAcqPageVM.SetDynamicType(1);
			}
			else if (_fileType == Enm_FileTypes.Enm_File_Defect)
			{
				xExDynamicGainAcqPage.xLoadFile.Visibility = Visibility.Visible;
				GenerateExDynamicAcqPageVM = new GenerateExDynamicDefectPage4MNVM(Instance);
				GenerateExDynamicAcqPageVM.SetDynamicType(2);
			}
			base.DataContext = GenerateExDynamicAcqPageVM;
			GenerateExDynamicAcqPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			int num = Math.Min(Instance.Detector.Prop_Attr_Width, 640);
			int height = Instance.Detector.Prop_Attr_Height * num / Instance.Detector.Prop_Attr_Width;
			xExDynamicGainAcqPage.xImageView.SetDisplaySize(num, height);
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				GenerateExDynamicAcqPageVM.AddCallbackHandler();
				xExDynamicGainAcqPage.xImageView.xImageView.StartPaintTimer();
			}
			else
			{
				xExDynamicGainAcqPage.xImageView.xImageView.StopPaintTimer();
				GenerateExDynamicAcqPageVM.RemoveCallbackHandler();
				ReleaseResource();
			}
		}

		private void ShowImage(byte[] data, int width, int height)
		{
			ShowImage(new IRayImageData(data, width, height));
		}

		private void ShowImage(IRayImageData image)
		{
			xExDynamicGainAcqPage.xImageView.DisplayImage(image);
		}

		private void PaintImage(IRayImageData image)
		{
			xExDynamicGainAcqPage.xImageView.UpdateImage(image);
		}

		private void AdjustWinLevelAndWinWidth()
		{
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				ushort[] oriGray16ImgBuffer = xExDynamicGainAcqPage.xImageView.xImageView.OriGray16ImgBuffer;
				int winLevel = 0;
				int winWidth = 0;
				CalcWinLevelWinWidth.GetWinLevelAndWidth(oriGray16ImgBuffer, ref winLevel, ref winWidth, Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
				xExDynamicGainAcqPage.xImageView.xImageView.SetWindow(winWidth, winLevel);
			});
		}

		private void ReleaseResource()
		{
			string[] cineBufferName = CineBufferName;
			foreach (string path in cineBufferName)
			{
				if (File.Exists(path))
				{
					File.Delete(path);
				}
			}
		}

		public bool GetCancelStatus()
		{
			return GenerateExDynamicAcqPageVM.GetCancelStatus();
		}

		public void SetCancelStatus(bool bValue)
		{
			GenerateExDynamicAcqPageVM.SetCancelStatus(bValue);
		}

		public void DoCancel()
		{
			GenerateExDynamicAcqPageVM.DoCancel();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateexdynamicacqpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xExDynamicGainAcqPage = (GenerateExTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
