using CorrectExUI.ViewModel;
using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class GenerateExDefectPage : UserControl, IComponentConnector
	{
		private DetecInfo Instance;

		private GenerateExDefectPageVM generateExDefectPageVM;

		private CorrectionImageListVM ImageListVM;

		internal GenerateExTemplatePage xExDefectAcquireImage;

		private bool _contentLoaded;

		public GenerateExDefectPage(DetecInfo instance)
		{
			InitializeComponent();
			Instance = instance;
		}

		public void Initialize()
		{
			xExDefectAcquireImage.xLoadFile.Visibility = Visibility.Visible;
			ImageListVM = new CorrectionImageListVM();
			ImageListVM.DisplayThumb += ShowImage;
			generateExDefectPageVM = new GenerateExDefectPageVM(Instance);
			base.DataContext = generateExDefectPageVM;
			generateExDefectPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			generateExDefectPageVM.exposeWindowControl = xExDefectAcquireImage.xExposeWindowWnd;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				generateExDefectPageVM.AddCallbackHandler();
				xExDefectAcquireImage.xImageView.SetDisplaySize(Instance.Detector.Prop_Attr_Width, Instance.Detector.Prop_Attr_Height);
			}
			else
			{
				generateExDefectPageVM.RemoveCallbackHandler();
				xExDefectAcquireImage.xImageView.SetDisplaySize(4, 4);
			}
		}

		private void ShowImage(IRayImageData image)
		{
			xExDefectAcquireImage.xImageView.DisplayImage(image);
		}

		public bool GetCancelStatus()
		{
			return generateExDefectPageVM.GetCancelStatus();
		}

		public void SetCancelStatus(bool bValue)
		{
			generateExDefectPageVM.SetCancelStatus(bValue);
		}

		public void DoCancel()
		{
			generateExDefectPageVM.DoCancel();
		}

		public void Clear()
		{
			ImageListVM.ClearThumbs();
			xExDefectAcquireImage.xImageView.xPlayerBar.Close();
			xExDefectAcquireImage.xImageView.xPlayerBar.SetDataContext(null);
			ImageListVM.DisplayThumb -= ShowImage;
			generateExDefectPageVM.ShowImage -= ShowImage;
			base.IsVisibleChanged -= VisibleChanged;
			xExDefectAcquireImage.xImageView.SetDisplaySize(4, 4);
			xExDefectAcquireImage.xImageView.OnClose();
			generateExDefectPageVM.Clear();
			GC.Collect();
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateexdefectpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xExDefectAcquireImage = (GenerateExTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
