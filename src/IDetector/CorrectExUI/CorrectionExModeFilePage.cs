using iDetector;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectExUI
{
	public class CorrectionExModeFilePage : UserControl, INotifyPropertyChanged, IComponentConnector
	{
		private string modeMessageColor;

		private string fileMessageColor;

		private Detector Instance;

		private ApplicationMode _mCurMode;

		private ApplicationMode _mActiveMode;

		private SDKInvokeProxy SdkInvokeProxy;

		public Action<bool> IsDynamicModeSelected;

		public ToolTipMsgDel ShowToolTip;

		private string[] templateStateInfo = new string[5]
		{
			"absent",
			"valid",
			"valid",
			"invalid",
			"unmatched"
		};

		private string[] fileTypeName = new string[5]
		{
			"",
			"Offset",
			"Gain",
			"MostGain",
			"Defect"
		};

		private DlgTaskTimeOut MessageBoxTimer;

		private List<DownloadInfo> _downloadList;

		private DownloadCorrProgressWnd _downloadWnd;

		private string TemplatePath = "Correct";

		private UploadFileInfo _uploadInfo;

		private int _loadFileStatus;

		private int _nIndex;

		private string _strDesc;

		private int _autoUpdateStatus;

		private string WarningColor = "#EE7600";

		private string NormalColor = "";

		public string _strActiveSubset = "";

		internal ListView _ModeList;

		internal GridView xModeListView;

		internal GridViewColumn xPGAHeader;

		internal GridViewColumn xFullWellHeader;

		internal GridViewColumn xZoomHeader;

		internal GridViewColumn xROIHeader;

		internal GridViewColumn xExpMode;

		internal Button btnModeDownloadToFPD;

		internal Button btnModeActive;

		internal TextBlock _txtModeMessage;

		internal Button btnReadSettingStatusDyna;

		internal Button btnEditFPS;

		internal GroupBox xGBoxTemplateFile;

		internal ListView xListView;

		internal GridView xView;

		internal GridViewColumn xType;

		internal GridViewColumn xIndex;

		internal GridViewColumn xActivity;

		internal GridViewColumn xPGA;

		internal GridViewColumn xBinning;

		internal GridViewColumn xValidity;

		internal GridViewColumn xDesp;

		internal Button btnFileUploadWorkdir;

		internal Button btnFileActive;

		internal Button btnFileUpdateHWPreOffset;

		internal Button btnFileUpdateHWFreqCoeff;

		internal TextBlock _txtFileMessage;

		internal Button btnReadTempFileStatusDyna;

		private bool _contentLoaded;

		public string ModeMessageColor
		{
			get
			{
				return modeMessageColor;
			}
			set
			{
				modeMessageColor = value;
				NotifyPropertyChanged("ModeMessageColor");
			}
		}

		public string FileMessageColor
		{
			get
			{
				return fileMessageColor;
			}
			set
			{
				fileMessageColor = value;
				NotifyPropertyChanged("FileMessageColor");
			}
		}

		private bool bInitedLayout
		{
			get;
			set;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public event Action UpdateDetPageParams;

		public void NotifyPropertyChanged(string property)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(property));
			}
		}

		public CorrectionExModeFilePage(Detector instance)
		{
			InitializeComponent();
			Instance = instance;
			if (this.SdkEventHandler == null)
			{
				this.SdkEventHandler = ProcessEvent;
				Instance.SubscribeCBEvent(OnSdkCallback);
				SdkInvokeProxy = new SDKInvokeProxy(Instance, new Shell(Application.Current.MainWindow));
			}
			_loadFileStatus = 0;
			bInitedLayout = false;
			_downloadList = new List<DownloadInfo>();
			_autoUpdateStatus = 2;
			base.IsVisibleChanged += VisibleChanged;
			base.DataContext = this;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (Instance != null && _mCurMode != null)
			{
				if (e.NewValue.Equals(true))
				{
					Instance.SubscribeCBEvent(OnSdkCallback);
					SwitchApplicationMode(_mCurMode);
				}
				else
				{
					Instance.UnSubscribeCBEvent(OnSdkCallback);
				}
			}
		}

		public void Clear()
		{
			base.IsVisibleChanged -= VisibleChanged;
			this.SdkEventHandler = null;
			SdkInvokeProxy = null;
			this.UpdateDetPageParams = null;
			GC.Collect();
		}

		public void Initialize(int nProductNo)
		{
			switch (nProductNo)
			{
			case 46:
			case 68:
			case 73:
			case 74:
			case 87:
				btnFileUploadWorkdir.Visibility = Visibility.Collapsed;
				btnFileActive.Visibility = Visibility.Collapsed;
				btnReadTempFileStatusDyna.Visibility = Visibility.Collapsed;
				btnFileUpdateHWPreOffset.Visibility = Visibility.Collapsed;
				btnFileUpdateHWFreqCoeff.Visibility = Visibility.Collapsed;
				xListView.Visibility = Visibility.Collapsed;
				_txtFileMessage.Visibility = Visibility.Collapsed;
				xGBoxTemplateFile.Visibility = Visibility.Collapsed;
				break;
			}
			InitApplicationModeStatus();
			_strActiveSubset = Instance.Prop_Cfg_DefaultSubset;
			if (string.IsNullOrEmpty(_strActiveSubset))
			{
				_strActiveSubset = "";
			}
			if (nProductNo != 49 && nProductNo != 74)
			{
				btnEditFPS.Visibility = Visibility.Collapsed;
			}
		}

		private void InitApplicationModeStatus(bool IsAfterChangeCurModeInfo = false)
		{
			bool flag = false;
			ModeListArr modeListArr = new ModeListArr();
			List<ApplicationMode> list = ApplicationMode.ParseIni(Instance.Prop_Attr_WorkDir);
			if (list.Count > 0)
			{
				if (list[0].NoZoom)
				{
					xModeListView.Columns.Remove(xZoomHeader);
				}
				if (!list[0].ExistedExpMode)
				{
					xModeListView.Columns.Remove(xExpMode);
				}
				if (!list[0].ExistedROIRange)
				{
					xModeListView.Columns.Remove(xROIHeader);
				}
			}
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
			if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0406X || prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0900X)
			{
				xModeListView.Columns.Remove(xPGAHeader);
				xModeListView.Columns.Remove(xZoomHeader);
			}
			else
			{
				xModeListView.Columns.Remove(xFullWellHeader);
			}
			_ModeList.Items.Clear();
			for (int i = 0; i < list.Count; i++)
			{
				modeListArr.AddItem(list[i], (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo);
				if (IsSelectModeEqualCurrentMode(list[i]) && !flag)
				{
					flag = true;
					_mCurMode = list[i];
					_mActiveMode = list[i];
				}
				_ModeList.Items.Add(list[i]);
			}
			if (flag && IsAfterChangeCurModeInfo)
			{
				UpdateModeActivityState();
				_ModeList.SelectedItem = _mCurMode;
				_mCurMode.OffsetValidity = templateStateInfo[Instance.Prop_Attr_OffsetValidityState];
				_mCurMode.GainValidity = templateStateInfo[Instance.Prop_Attr_GainValidityState];
				_mCurMode.DefectValidity = templateStateInfo[Instance.Prop_Attr_DefectValidityState];
			}
			if (flag && !IsAfterChangeCurModeInfo && SdkInvokeProxy.Invoke(7, _mCurMode.Subset) == 0)
			{
				SwitchApplicationMode(_mCurMode);
			}
		}

		private bool IsSelectModeEqualCurrentMode(ApplicationMode selMode)
		{
			bool flag = false;
			int prop_Attr_UROM_SequenceIntervalTime = Instance.Prop_Attr_UROM_SequenceIntervalTime;
			flag = (selMode.PGA == Instance.Prop_Attr_UROM_PGA && selMode.Zoom == Instance.Prop_Attr_UROM_ZoomMode && selMode.Binning == Instance.Prop_Attr_UROM_BinningMode && 0.0 != selMode.Freq && ((int)(1000.0 / selMode.Freq) == prop_Attr_UROM_SequenceIntervalTime || ((1000 / prop_Attr_UROM_SequenceIntervalTime == (int)selMode.Freq) ? true : false)));
			if (flag)
			{
				Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
				if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0406X || prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Pluto0900X)
				{
					string text = selMode.ROIRange.Replace(" ", "");
					string value = string.Format("({0},{1},{2},{3})", Instance.Prop_Attr_UROM_ROIColStartPos, Instance.Prop_Attr_UROM_ROIRowStartPos, Instance.Prop_Attr_UROM_ROIColEndPos, Instance.Prop_Attr_UROM_ROIRowEndPos);
					if (!text.Equals(value))
					{
						flag = false;
					}
				}
				else
				{
					if (selMode.ExistedROIRange)
					{
						string text2 = selMode.ROIRange.Replace(" ", "");
						string value2 = string.Format("({0},{1},{2},{3})", Instance.Prop_Attr_UROM_ROIColStartPos, Instance.Prop_Attr_UROM_ROIRowStartPos, Instance.Prop_Attr_UROM_ROIColEndPos, Instance.Prop_Attr_UROM_ROIRowEndPos);
						if (!text2.Equals(value2))
						{
							flag = false;
						}
					}
					if (selMode.ExistedExpMode && !selMode.ExpMode.Equals(((Enm_ExpMode)Instance.Prop_Attr_UROM_ExpMode).ToString().Replace("Enm_ExpMode_", "")))
					{
						flag = false;
					}
				}
			}
			return flag;
		}

		private void SwitchApplicationMode(ApplicationMode curMode)
		{
			if (curMode != null)
			{
				_ModeList.SelectedIndex = curMode.Index - 1;
				_mCurMode = curMode;
			}
		}

		private void EnableView(bool bEnable)
		{
			base.IsEnabled = bEnable;
		}

		private void UpdateApplicationModeEx()
		{
			CloseMessageBox();
			SwitchApplicationMode(_mActiveMode);
			UpdateTemplateFileValidityState();
			UpdateModeActivityState();
			if (this.UpdateDetPageParams != null)
			{
				this.UpdateDetPageParams();
			}
		}

		public void UpdateTemplateFileValidityState()
		{
			if (_mCurMode == null)
			{
				return;
			}
			int count = _ModeList.Items.Count;
			for (int i = 0; i < count; i++)
			{
				ApplicationMode applicationMode = _ModeList.Items[i] as ApplicationMode;
				if (applicationMode == _mCurMode)
				{
					int prop_Attr_OffsetValidityState = Instance.Prop_Attr_OffsetValidityState;
					int prop_Attr_GainValidityState = Instance.Prop_Attr_GainValidityState;
					int prop_Attr_DefectValidityState = Instance.Prop_Attr_DefectValidityState;
					applicationMode.OffsetValidity = templateStateInfo[prop_Attr_OffsetValidityState];
					applicationMode.GainValidity = templateStateInfo[prop_Attr_GainValidityState];
					applicationMode.DefectValidity = templateStateInfo[prop_Attr_DefectValidityState];
				}
				else
				{
					applicationMode.OffsetValidity = "";
					applicationMode.GainValidity = "";
					applicationMode.DefectValidity = "";
				}
			}
		}

		private void UpdateModeActivityState()
		{
			if (_mCurMode == null)
			{
				return;
			}
			int count = _ModeList.Items.Count;
			for (int i = 0; i < count; i++)
			{
				ApplicationMode applicationMode = _ModeList.Items[i] as ApplicationMode;
				if (applicationMode == _mCurMode)
				{
					_mCurMode.Activity = "enable";
					if (applicationMode.ExistedExpMode)
					{
						try
						{
							Enm_ExpMode result;
							Enum.TryParse(string.Format("Enm_ExpMode_{0}", applicationMode.ExpMode), out result);
							IsDynamicModeSelected(result >= Enm_ExpMode.Enm_ExpMode_Pulse);
						}
						catch
						{
						}
					}
				}
				else
				{
					applicationMode.Activity = "";
				}
			}
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		public void ParseTemplateList(string xmlInfo)
		{
			if (xmlInfo == null)
			{
				return;
			}
			XmlParser xmlParser = new XmlParser();
			xmlParser.LoadXml(xmlInfo);
			List<string> nodes = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Index");
			if (nodes == null || nodes.Count == 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "ParseTemplateList: HWFile list is null";
				return;
			}
			string singleNoteValue = xmlParser.GetSingleNoteValue("HWCalibrationFileList/FileItem/Type");
			List<string> nodes2 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/PGA");
			List<string> nodes3 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Binning");
			List<string> nodes4 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Zoom");
			List<string> nodes5 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Activity");
			List<string> nodes6 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/Validity");
			List<string> nodes7 = xmlParser.GetNodes("HWCalibrationFileList/FileItem/DespInfo");
			Enm_FileTypes enm_FileTypes = (Enm_FileTypes)Convert.ToInt32(singleNoteValue);
			string type = (enm_FileTypes != Enm_FileTypes.Enm_File_Gain) ? "Defect" : "Gain";
			for (int i = 0; i < nodes.Count; i++)
			{
				FPDCorrFileList fPDCorrFileList = new FPDCorrFileList();
				fPDCorrFileList.Type = type;
				if (i < nodes.Count)
				{
					fPDCorrFileList.Index = nodes[i];
				}
				if (i < nodes2.Count)
				{
					fPDCorrFileList.PGA = nodes2[i];
				}
				if (i < nodes3.Count)
				{
					fPDCorrFileList.Binning = nodes3[i];
				}
				if (i < nodes4.Count)
				{
					fPDCorrFileList.Zoom = nodes4[i];
				}
				if (i < nodes5.Count)
				{
					fPDCorrFileList.Activity = nodes5[i];
				}
				if (i < nodes6.Count)
				{
					fPDCorrFileList.Validity = nodes6[i];
				}
				if (i < nodes7.Count)
				{
					fPDCorrFileList.DespInfo = nodes7[i];
				}
				xListView.Items.Add(fPDCorrFileList);
			}
		}

		private void CheckHWTemplateStatus()
		{
		}

		private void UpdateFPDCorrFileStatus(Enm_FileTypes FileType)
		{
			int num = SdkInvokeProxy.Invoke(3021, (int)FileType);
			if (1 == num)
			{
			}
		}

		public void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			switch (nEventID)
			{
			case 2006:
				UpdateTemplateFileValidityState();
				if (ShowToolTip != null)
				{
					if (nParam2 == 0)
					{
						ShowToolTip(sender.Prop_Attr_UROM_SerialNo + ": " + fileTypeName[nParam1] + " has expired!");
					}
					else
					{
						ShowToolTip(sender.Prop_Attr_UROM_SerialNo + ": " + fileTypeName[nParam1] + " will expire in " + nParam2 + " minute");
					}
				}
				break;
			case 4:
				switch (nParam1)
				{
				case 7:
					EnableView(true);
					UpdateApplicationModeEx();
					CheckHWTemplateStatus();
					UpdateActiveSubset();
					break;
				case 2001:
					EnableView(true);
					UpdateApplicationModeEx();
					break;
				case 3017:
					if (_loadFileStatus == 1)
					{
						_loadFileStatus = 0;
						int fileType = (int)_downloadList[1].fileType;
						string fullPath = _downloadList[1].fullPath;
						int num = SdkInvokeProxy.Invoke(3017, fileType, _nIndex, fullPath, _strDesc);
						if (1 != num && num != 0)
						{
							if (_downloadWnd != null)
							{
								_downloadWnd.OnClose();
							}
							ModeMessageColor = WarningColor;
							_txtModeMessage.Text = "Download defect failed!";
							return;
						}
						ModeMessageColor = NormalColor;
						_txtModeMessage.Text = "Downloading defect ...";
					}
					else
					{
						if (_downloadWnd != null)
						{
							_downloadWnd.OnClose();
						}
						ModeMessageColor = NormalColor;
						_txtModeMessage.Text = "Download succeed!";
						_autoUpdateStatus = 2;
						xListView.Items.Clear();
						UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Gain);
						ModeMessageColor = NormalColor;
						_txtModeMessage.Text = "Updating FPD file ...";
					}
					break;
				case 3018:
					FileMessageColor = NormalColor;
					_txtFileMessage.Text = "Upload FPD file succeed!";
					break;
				case 3021:
					_autoUpdateStatus--;
					if (_autoUpdateStatus == 1)
					{
						UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Defect);
					}
					else if (_autoUpdateStatus == 0)
					{
						EnableView(true);
						_autoUpdateStatus = 2;
						FileMessageColor = NormalColor;
						_txtFileMessage.Text = "Update FPD file succeed!";
					}
					break;
				case 3019:
					_autoUpdateStatus = 2;
					xListView.Items.Clear();
					UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Gain);
					break;
				case 2032:
					UpdateApplicationModeAfterChanageCurAppMode();
					EnableView(true);
					ModeMessageColor = NormalColor;
					_txtModeMessage.Text = "Edit CurMode Params succeed!";
					MessageBox.Show("The params of current mode had been changed, Please update template files!", "Information", MessageBoxButton.OK, MessageBoxImage.Asterisk);
					break;
				default:
					EnableView(true);
					break;
				case 2002:
					break;
				}
				Instance.TASK = "No Task";
				break;
			case 5:
				switch (nParam1)
				{
				case 7:
					CloseMessageBox();
					EnableView(true);
					SwitchApplicationMode(_mCurMode);
					break;
				case 2001:
				case 2002:
					CloseMessageBox();
					EnableView(true);
					SwitchApplicationMode(_mCurMode);
					break;
				case 3017:
					if (_downloadWnd != null)
					{
						_downloadWnd.OnClose();
					}
					_loadFileStatus = 0;
					_autoUpdateStatus = 2;
					ModeMessageColor = WarningColor;
					_txtModeMessage.Text = "Download failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 3018:
					FileMessageColor = WarningColor;
					_txtFileMessage.Text = "Upload failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 3021:
					EnableView(true);
					FileMessageColor = WarningColor;
					_txtFileMessage.Text = "Query Template FileList failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 3019:
					EnableView(true);
					FileMessageColor = WarningColor;
					_txtFileMessage.Text = "Select failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				case 2032:
					EnableView(true);
					ModeMessageColor = WarningColor;
					_txtModeMessage.Text = "Edit FPS failed! Err = " + ErrorHelper.GetErrorDesp(nParam2);
					break;
				default:
					EnableView(true);
					break;
				case 2:
					break;
				}
				Instance.TASK = "No Task";
				break;
			case 12:
				ParseTemplateList(pParam as string);
				break;
			}
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			object obj = null;
			if (nEventID == 12)
			{
				string text = Marshal.PtrToStringAuto(pParam);
				obj = text;
			}
			base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, obj);
		}

		public void RemoveCallbackHandler()
		{
			Instance.UnSubscribeCBEvent(OnSdkCallback);
		}

		private void btnModeActive_Click(object sender, RoutedEventArgs e)
		{
			if (_ModeList.SelectedIndex >= 0)
			{
				_mActiveMode = (_ModeList.SelectedItem as ApplicationMode);
				SdkInvokeProxy.Invoke(7, _mActiveMode.Subset);
			}
		}

		private void btnModeDownloadToFPD_Click(object sender, RoutedEventArgs e)
		{
			_loadFileStatus = 0;
			ModeMessageColor = NormalColor;
			_txtModeMessage.Text = "";
			if (_ModeList.SelectedIndex < 0)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Please select a item!";
				return;
			}
			_downloadList.Clear();
			ApplicationMode applicationMode = _ModeList.SelectedItem as ApplicationMode;
			string[] array = applicationMode.SubsetName.Split('-');
			string text = array[0];
			if (applicationMode.GainValidity != "absent")
			{
				string gainName = GetGainName(Instance);
				string text2 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + text + "\\" + gainName;
				if (!string.IsNullOrEmpty(gainName) && File.Exists(text2))
				{
					FileInfo fileInfo = new FileInfo(text2);
					DownloadInfo item = default(DownloadInfo);
					item.index = applicationMode.Index;
					item.modeName = applicationMode.SubsetName;
					item.fileType = Enm_FileTypes.Enm_File_Gain;
					item.fullPath = text2;
					item.desc = fileInfo.LastWriteTime.ToString();
					item.indexCount = 12;
					_downloadList.Add(item);
					_loadFileStatus++;
				}
			}
			if (applicationMode.DefectValidity != "absent")
			{
				string defectName = GetDefectName(Instance);
				string text3 = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + text + "\\" + defectName;
				if (!string.IsNullOrEmpty(defectName) && File.Exists(text3))
				{
					FileInfo fileInfo2 = new FileInfo(text3);
					DownloadInfo item2 = default(DownloadInfo);
					item2.index = applicationMode.Index;
					item2.modeName = applicationMode.SubsetName;
					item2.fileType = Enm_FileTypes.Enm_File_Defect;
					item2.fullPath = text3;
					item2.desc = fileInfo2.LastWriteTime.ToString();
					item2.indexCount = 12;
					_downloadList.Add(item2);
					_loadFileStatus++;
				}
			}
			if (_loadFileStatus == 0)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Template file does not exist!";
				return;
			}
			DownloadToFpdWnd downloadToFpdWnd = new DownloadToFpdWnd(_downloadList);
			if (downloadToFpdWnd.ShowDialog() != true)
			{
				return;
			}
			_loadFileStatus--;
			_nIndex = downloadToFpdWnd.nIndex;
			_strDesc = downloadToFpdWnd.strDesc;
			int fileType = (int)_downloadList[0].fileType;
			string fullPath = _downloadList[0].fullPath;
			int num = SdkInvokeProxy.Invoke(3017, fileType, _nIndex, fullPath, _strDesc);
			if (1 == num || num == 0)
			{
				switch (fileType)
				{
				case 2:
					ModeMessageColor = NormalColor;
					_txtModeMessage.Text = "Downloading gain ...";
					break;
				case 4:
					ModeMessageColor = NormalColor;
					_txtModeMessage.Text = "Downloading defect ...";
					break;
				}
				_downloadWnd = new DownloadCorrProgressWnd(Instance, "Download File Progress");
				_downloadWnd.ShowDialog();
				Instance.AttrChangingMonitor.AddRef(5016);
			}
		}

		private void btnFileActive_Click(object sender, RoutedEventArgs e)
		{
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			if (xListView.Items.Count == 0)
			{
				return;
			}
			if (xListView.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item!";
				return;
			}
			FPDCorrFileList fPDCorrFileList = xListView.SelectedItem as FPDCorrFileList;
			int nPara = (!(fPDCorrFileList.Type == "Gain")) ? 4 : 2;
			EnableView(false);
			int num = SdkInvokeProxy.Invoke(3019, nPara, Convert.ToInt32(fPDCorrFileList.Index));
			if (1 == num)
			{
			}
		}

		private void btnFileUploadWorkdir_Click(object sender, RoutedEventArgs e)
		{
			if (_ModeList.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item in the Application Mode Setting!";
				return;
			}
			if (xListView.SelectedIndex < 0)
			{
				FileMessageColor = WarningColor;
				_txtFileMessage.Text = "Please select a item in the Fpd Template File!";
				return;
			}
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			ApplicationMode applicationMode = _ModeList.SelectedItem as ApplicationMode;
			FPDCorrFileList fPDCorrFileList = xListView.SelectedItem as FPDCorrFileList;
			_uploadInfo.uploadInfo = "index " + fPDCorrFileList.Index + ":" + fPDCorrFileList.Type;
			_uploadInfo.modeName = applicationMode.SubsetName;
			int nPara = int.Parse(fPDCorrFileList.Index);
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
			string lpKeyName = (prop_Attr_UROM_ProductNo != Enm_ProdType.Enm_Prd_Mercu1717V) ? string.Format("Binning{0}_{1}", fPDCorrFileList.Binning, fPDCorrFileList.Type) : string.Format("Binning{0}Zoom{1}_{2}", fPDCorrFileList.Binning, fPDCorrFileList.Zoom, fPDCorrFileList.Type);
			string lpFileName = Instance.Prop_Attr_WorkDir + "\\iDetectorConfig.ini";
			string privateProfileString = IniParser.GetPrivateProfileString("UploadFileName", lpKeyName, "", lpFileName);
			string text = Instance.Prop_Attr_WorkDir + TemplatePath + "\\" + applicationMode.SubsetName + "\\" + privateProfileString;
			if (File.Exists(text))
			{
				_uploadInfo.noticeInfo = "Overwrite exits file!";
			}
			else
			{
				_uploadInfo.noticeInfo = "";
			}
			int nPara2 = (!(fPDCorrFileList.Type == "Gain")) ? 4 : 2;
			UploadToWorkdirWnd uploadToWorkdirWnd = new UploadToWorkdirWnd(_uploadInfo);
			if (uploadToWorkdirWnd.ShowDialog() == true)
			{
				int num = SdkInvokeProxy.Invoke(3018, nPara2, nPara, text);
				if (1 == num)
				{
				}
			}
		}

		private void btnFileUpdateHWPreOffset_Click(object sender, RoutedEventArgs e)
		{
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			int num = SdkInvokeProxy.Invoke(3020);
			if (1 == num)
			{
				EnableView(false);
			}
		}

		private void btnFileUpdateHWFreqCoeff_Click(object sender, RoutedEventArgs e)
		{
			FileMessageColor = NormalColor;
			_txtFileMessage.Text = "";
			int num = SdkInvokeProxy.Invoke(3032);
			if (1 == num)
			{
				EnableView(false);
			}
		}

		private string GetGainName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("gain_{0}x{1}.gn", width, height);
		}

		private string GetDefectName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("defect_{0}x{1}.dft", width, height);
		}

		private string GetLagName(Detector instance)
		{
			int width;
			int height;
			if (!GetWidthAndHeight(instance, out width, out height))
			{
				return null;
			}
			return string.Format("lag_{0}x{1}_matrix.lag", width, height);
		}

		private bool GetWidthAndHeight(Detector instance, out int width, out int height)
		{
			width = 0;
			height = 0;
			switch (instance.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 56:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					width = 1024;
					height = 1024;
					break;
				case 1:
					width = 512;
					height = 512;
					break;
				default:
					return false;
				}
				break;
			case 49:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					width = 2048;
					height = 2048;
					break;
				case 1:
					width = 1024;
					height = 1024;
					break;
				case 3:
					width = 512;
					height = 512;
					break;
				default:
					return false;
				}
				break;
			case 48:
				switch (instance.Prop_Attr_UROM_BinningMode)
				{
				case 0:
					switch (instance.Prop_Attr_UROM_ZoomMode)
					{
					case 0:
					case 8:
						width = 3072;
						height = 3072;
						break;
					case 9:
						width = 2048;
						height = 2048;
						break;
					default:
						return false;
					}
					break;
				case 1:
					switch (instance.Prop_Attr_UROM_ZoomMode)
					{
					case 0:
					case 10:
						width = 1536;
						height = 1536;
						break;
					case 11:
						width = 1024;
						height = 1024;
						break;
					default:
						return false;
					}
					break;
				case 2:
				{
					Enm_Zoom prop_Attr_UROM_ZoomMode = (Enm_Zoom)instance.Prop_Attr_UROM_ZoomMode;
					if (prop_Attr_UROM_ZoomMode == Enm_Zoom.Enm_Zoom_Null || prop_Attr_UROM_ZoomMode == Enm_Zoom.Enm_Zoom_1024x1024)
					{
						width = 1024;
						height = 1024;
						break;
					}
					return false;
				}
				default:
					return false;
				}
				break;
			default:
				width = instance.Prop_Attr_Width;
				height = instance.Prop_Attr_Height;
				break;
			}
			return true;
		}

		private void btnReadSettingStatusDyna_Click(object sender, RoutedEventArgs e)
		{
			InitApplicationModeStatus();
		}

		private void btnReadTempFileStatusDyna_Click(object sender, RoutedEventArgs e)
		{
			EnableView(false);
			_autoUpdateStatus = 2;
			xListView.Items.Clear();
			UpdateFPDCorrFileStatus(Enm_FileTypes.Enm_File_Gain);
			ModeMessageColor = NormalColor;
		}

		private void UpdateActiveSubset()
		{
			_strActiveSubset = _mCurMode.Subset;
		}

		public string GetActiveSubset()
		{
			return _strActiveSubset;
		}

		private void btnEditFPS_Click(object sender, RoutedEventArgs e)
		{
			ModeMessageColor = NormalColor;
			_txtModeMessage.Text = "";
			if (_ModeList.SelectedIndex < 0)
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Please select a item!";
				return;
			}
			ApplicationMode applicationMode = _ModeList.SelectedItem as ApplicationMode;
			if (string.IsNullOrEmpty(applicationMode.Activity))
			{
				ModeMessageColor = WarningColor;
				_txtModeMessage.Text = "Please select the enabled mode";
				return;
			}
			EditFPSWnd editFPSWnd = new EditFPSWnd(applicationMode);
			if (editFPSWnd.ShowDialog() == true)
			{
				int nPara = (0.0 != editFPSWnd.editInfo.Freq) ? ((int)(1000000.0 / editFPSWnd.editInfo.Freq)) : 0;
				int num = SdkInvokeProxy.Invoke(2032, applicationMode.PGA, applicationMode.Binning, nPara);
				if (num == 1)
				{
					EnableView(false);
				}
			}
		}

		private void UpdateApplicationModeAfterChanageCurAppMode()
		{
			ApplicationMode applicationMode = _ModeList.SelectedItem as ApplicationMode;
			applicationMode.PGA = Instance.Prop_Attr_UROM_PGA;
			applicationMode.Binning = Instance.Prop_Attr_UROM_BinningMode;
			applicationMode.Freq = ((Instance.Prop_Attr_UROM_SequenceIntervalTime_HighPrecision != 0) ? (1000000 / Instance.Prop_Attr_UROM_SequenceIntervalTime_HighPrecision) : 0);
			ApplicationMode.UpdateAppMode2File(Instance.Prop_Attr_WorkDir, applicationMode);
			InitApplicationModeStatus(true);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionexmodefilepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_ModeList = (ListView)target;
				break;
			case 2:
				xModeListView = (GridView)target;
				break;
			case 3:
				xPGAHeader = (GridViewColumn)target;
				break;
			case 4:
				xFullWellHeader = (GridViewColumn)target;
				break;
			case 5:
				xZoomHeader = (GridViewColumn)target;
				break;
			case 6:
				xROIHeader = (GridViewColumn)target;
				break;
			case 7:
				xExpMode = (GridViewColumn)target;
				break;
			case 8:
				btnModeDownloadToFPD = (Button)target;
				btnModeDownloadToFPD.Click += btnModeDownloadToFPD_Click;
				break;
			case 9:
				btnModeActive = (Button)target;
				btnModeActive.Click += btnModeActive_Click;
				break;
			case 10:
				_txtModeMessage = (TextBlock)target;
				break;
			case 11:
				btnReadSettingStatusDyna = (Button)target;
				btnReadSettingStatusDyna.Click += btnReadSettingStatusDyna_Click;
				break;
			case 12:
				btnEditFPS = (Button)target;
				btnEditFPS.Click += btnEditFPS_Click;
				break;
			case 13:
				xGBoxTemplateFile = (GroupBox)target;
				break;
			case 14:
				xListView = (ListView)target;
				break;
			case 15:
				xView = (GridView)target;
				break;
			case 16:
				xType = (GridViewColumn)target;
				break;
			case 17:
				xIndex = (GridViewColumn)target;
				break;
			case 18:
				xActivity = (GridViewColumn)target;
				break;
			case 19:
				xPGA = (GridViewColumn)target;
				break;
			case 20:
				xBinning = (GridViewColumn)target;
				break;
			case 21:
				xValidity = (GridViewColumn)target;
				break;
			case 22:
				xDesp = (GridViewColumn)target;
				break;
			case 23:
				btnFileUploadWorkdir = (Button)target;
				btnFileUploadWorkdir.Click += btnFileUploadWorkdir_Click;
				break;
			case 24:
				btnFileActive = (Button)target;
				btnFileActive.Click += btnFileActive_Click;
				break;
			case 25:
				btnFileUpdateHWPreOffset = (Button)target;
				btnFileUpdateHWPreOffset.Click += btnFileUpdateHWPreOffset_Click;
				break;
			case 26:
				btnFileUpdateHWFreqCoeff = (Button)target;
				btnFileUpdateHWFreqCoeff.Click += btnFileUpdateHWFreqCoeff_Click;
				break;
			case 27:
				_txtFileMessage = (TextBlock)target;
				break;
			case 28:
				btnReadTempFileStatusDyna = (Button)target;
				btnReadTempFileStatusDyna.Click += btnReadTempFileStatusDyna_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
