namespace iDetector.PopupWnds
{
	public enum FPDSTATE
	{
		STA_OFFLINE,
		STA_SLEEP,
		STA_WAKEUP,
		STA_OPERATION
	}
}
