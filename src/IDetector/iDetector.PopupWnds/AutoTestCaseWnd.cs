using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;
using System.Xml.XPath;

namespace iDetector.PopupWnds
{
	public class AutoTestCaseWnd : Window, IComponentConnector
	{
		public const int BATTERY_LIMIT = 10;

		private Detector Instance;

		private string m_strXMLContents;

		private XmlDocument m_XmlDoc = new XmlDocument();

		private int m_crntStepIndex;

		private int m_nCrntCycle;

		private string m_strCrntPreCmd;

		private EXECUCMD m_PreCmd;

		private int m_nCrntExecuItemNum;

		private EXECUITEM[] m_CrntExecuItem = new EXECUITEM[50];

		private int m_nCrntExecuItemPtr;

		private UITimer m_nTimer;

		private UITimer m_nTestCaseTimer;

		private DateTime m_nExecuItemClock;

		private bool m_bRunning;

		private bool m_bInitRunning;

		private DateTime m_nImageAcqCmdSendTime;

		private DateTime m_nExpProhibitTime;

		private DateTime m_nExpEnableTime;

		private DateTime m_nImageAcquisitionBegin;

		private DateTime m_nFirstPacketRecvTime;

		private DateTime m_nLastPacketRecvTime;

		private int m_nSleep2WakeupTimes;

		private int m_nWakeup2SleepTimes;

		private int m_nSwitchFilureTimes;

		private DateTime m_nSleepStartTime;

		private DateTime m_nSleepEndTime;

		private DateTime m_nWakeupStartTime;

		private DateTime m_nWakeupEndTime;

		private FPDSTATE m_staLast;

		private FPDSTATE m_staNext;

		private FPDSTATE m_staNow;

		private DateTime m_nTimeoutCnt;

		private long m_nLoopTimes;

		private int m_nFpdBusyCnt;

		private int m_nContiSendFailed;

		private int m_nCrntFrameCount;

		private int m_nFpdImageLoss;

		private int m_nContiCmdSendFailed;

		public int m_cmdInvoked;

		public CMDSTATE m_cmdState;

		private int m_nLastImageTransProgres = 99;

		private TextWriter m_TxtLog;

		internal Button btnRun;

		internal Button btnStop;

		internal ComboBox cboTestCase;

		internal TextBox txtHint;

		private bool _contentLoaded;

		private event SdkCallbackEventHandler_WithImgParam SdkEventHandler;

		public AutoTestCaseWnd(Detector instance)
		{
			Instance = instance;
			InitializeComponent();
			base.ShowInTaskbar = false;
			base.Owner = Application.Current.MainWindow;
			string prop_Attr_WorkDir = Instance.Prop_Attr_WorkDir;
			string str = DateTime.Now.ToString("yyyyMMdd") + " TestCase.log";
			string text = prop_Attr_WorkDir + "\\";
			Utility.CreateDirectory(text);
			m_TxtLog = new StreamWriter(text + str, true, Encoding.ASCII);
			this.SdkEventHandler = ProcessEvent;
			m_nTimer = new UITimer(20, OnTimer);
			m_nTestCaseTimer = new UITimer(1000, OnTestCaseTimer);
			m_nTimer.Start();
		}

		private void Window_Initialized(object sender, EventArgs e)
		{
			Instance.SubscribeCBEvent(OnSdkCallback);
			LoadFileList();
			btnStop.IsEnabled = false;
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			if (btnStop.IsEnabled)
			{
				StopButton_Click(null, null);
			}
			m_nTimer.Stop();
			Instance.UnSubscribeCBEvent(OnSdkCallback);
			m_TxtLog.Close();
			m_TxtLog.Dispose();
		}

		private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			m_crntStepIndex = cboTestCase.SelectedIndex;
			LoadTestStep(m_crntStepIndex);
		}

		private void RunButton_Click(object sender, RoutedEventArgs e)
		{
			cboTestCase.IsEnabled = false;
			btnRun.IsEnabled = false;
			btnStop.IsEnabled = true;
			switch (m_PreCmd)
			{
			case EXECUCMD.WZD_SLEEP:
				if (m_staNow != FPDSTATE.STA_SLEEP)
				{
					m_bInitRunning = true;
					m_staNext = FPDSTATE.STA_SLEEP;
					InstanceInvoke(4);
				}
				else
				{
					DoRunTestCase();
				}
				break;
			case EXECUCMD.WZD_WAKEUP:
				if (m_staNow != FPDSTATE.STA_WAKEUP)
				{
					m_bInitRunning = true;
					m_staNext = FPDSTATE.STA_WAKEUP;
					InstanceInvoke(5);
				}
				else
				{
					DoRunTestCase();
				}
				break;
			default:
				DoRunTestCase();
				break;
			}
		}

		private void StopButton_Click(object sender, RoutedEventArgs e)
		{
			SetMessageInfo("====Test_End====");
			m_bRunning = false;
			m_nTestCaseTimer.Stop();
			cboTestCase.IsEnabled = true;
			btnRun.IsEnabled = true;
			btnStop.IsEnabled = false;
		}

		private int InstanceInvoke(int cmd)
		{
			m_cmdInvoked = cmd;
			int num = Instance.Invoke(cmd);
			switch (num)
			{
			case 0:
				m_cmdState = CMDSTATE.Proc_Succeed;
				break;
			case 1:
				m_cmdState = CMDSTATE.Proc_Called;
				break;
			default:
				m_cmdState = CMDSTATE.Proc_Failed;
				break;
			}
			return num;
		}

		public void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			switch (nEventID)
			{
			case 1001:
			case 1002:
			case 1012:
			{
				IRayImage image = (IRayImage)Marshal.PtrToStructure(pParam, typeof(IRayImage));
				IRayImageData rayImageData = new IRayImageData(ref image);
				base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, rayImageData);
				break;
			}
			default:
				base.Dispatcher.BeginInvoke(this.SdkEventHandler, sender, nEventID, nEventLevel, strMsg, nParam1, nParam2, nPtrParamLen, null);
				break;
			}
		}

		private void ProcessEvent(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, object pParam)
		{
			switch (nEventID)
			{
			case 1004:
				m_nExpProhibitTime = DateTime.Now;
				break;
			case 1005:
				m_nExpEnableTime = DateTime.Now;
				break;
			case 1008:
				m_nImageAcquisitionBegin = DateTime.Now;
				m_nFirstPacketRecvTime = DateTime.Now;
				break;
			case 1002:
				if (m_bRunning)
				{
					SetMessageInfo(string.Format("FD_Acquisition_Preview, Transmission Time: {0}", m_nLastPacketRecvTime - m_nFirstPacketRecvTime));
				}
				break;
			case 1001:
				if (!m_bRunning)
				{
					break;
				}
				m_nCrntFrameCount++;
				SetMessageInfo(string.Format("FD_Acquisition_End: Image {0}, Transmission Time: {1}, Acquisition Time: {2}", m_nCrntFrameCount, m_nLastPacketRecvTime - m_nFirstPacketRecvTime, m_nLastPacketRecvTime - m_nImageAcquisitionBegin));
				SetMessageInfo(string.Format("Battery Capacity: {0}-{1}, Net Mode: {2} , WiFi Link Quality: {3} (ESSID: {4})", Instance.Prop_Attr_Battery_Remaining, Instance.Prop_Attr_Battery_ChargingStatus, (Instance.Prop_Attr_NetworkInterface == 2) ? "WiFi" : "Other", Instance.Prop_Attr_WifiStatu_WorkingLinkQuality, Instance.Prop_Attr_WifiStatu_LinkedAP));
				if (m_CrntExecuItem[m_nCrntExecuItemPtr].cycle > 0)
				{
					int num = m_CrntExecuItem[m_nCrntExecuItemPtr].cycle * 1000 - (DateTime.Now - m_nWakeupEndTime).Milliseconds;
					if (num > 10000)
					{
						m_nExecuItemClock = DateTime.Now.AddMilliseconds(num);
					}
					else
					{
						m_nExecuItemClock = DateTime.Now.AddMilliseconds(10000.0);
					}
				}
				else if (m_nCrntExecuItemPtr < m_nCrntExecuItemNum - 1)
				{
					m_nExecuItemClock = DateTime.Now.AddMilliseconds(10000.0);
				}
				break;
			case 4:
				if (m_cmdInvoked == nParam1)
				{
					m_cmdState = CMDSTATE.Proc_Succeed;
				}
				break;
			case 5:
				if (m_cmdInvoked == nParam1)
				{
					m_cmdState = CMDSTATE.Proc_Failed;
				}
				break;
			}
		}

		private void LoadFileList()
		{
			string filename = AppDomain.CurrentDomain.BaseDirectory + "TestCaseScript.xml";
			m_XmlDoc.Load(filename);
			XmlNode xmlNode = m_XmlDoc.SelectSingleNode("/TestCaseScript");
			if (xmlNode == null)
			{
				MessageBox.Show("XML file ERROR!");
				return;
			}
			for (XmlNode xmlNode2 = xmlNode.FirstChild; xmlNode2 != null; xmlNode2 = xmlNode2.NextSibling)
			{
				int num = Convert.ToInt32(xmlNode2.Attributes.GetNamedItem("index").Value);
				string value = xmlNode2.Attributes.GetNamedItem("name").Value;
				cboTestCase.Items.Add(string.Format("{0} - {1}", num, value));
			}
			cboTestCase.SelectedIndex = 0;
			LoadTestStep(0);
		}

		private void LoadTestStep(int index)
		{
			XPathNavigator xPathNavigator = m_XmlDoc.CreateNavigator();
			XPathNodeIterator xPathNodeIterator = xPathNavigator.Select(string.Format("/TestCaseScript/TestCase[@index={0}]", index + 1));
			if (xPathNodeIterator.MoveNext())
			{
				if (!string.IsNullOrEmpty(xPathNodeIterator.Current.GetAttribute("name", "")))
				{
					xPathNodeIterator.Current.GetAttribute("name", "");
				}
				if (!string.IsNullOrEmpty(xPathNodeIterator.Current.GetAttribute("cycle", "")))
				{
					m_nCrntCycle = Convert.ToInt32(xPathNodeIterator.Current.GetAttribute("cycle", ""));
				}
				if (!string.IsNullOrEmpty(xPathNodeIterator.Current.GetAttribute("precmd", "")))
				{
					m_strCrntPreCmd = xPathNodeIterator.Current.GetAttribute("precmd", "");
					switch (m_strCrntPreCmd)
					{
					case "sleep":
						m_PreCmd = EXECUCMD.WZD_SLEEP;
						break;
					case "wakeup":
						m_PreCmd = EXECUCMD.WZD_WAKEUP;
						break;
					default:
						m_PreCmd = EXECUCMD.WZD_EMPTY;
						break;
					}
				}
				m_nCrntExecuItemNum = 0;
				xPathNodeIterator = xPathNodeIterator.Current.SelectChildren(XPathNodeType.Element);
				while (xPathNodeIterator.MoveNext())
				{
					ClearExecuItem(m_nCrntExecuItemNum);
					switch (xPathNodeIterator.Current.GetAttribute("cmd", ""))
					{
					case "wakeup":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_WAKEUP;
						break;
					case "sleep":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_SLEEP;
						break;
					case "prepcap":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_PREPCAP;
						break;
					case "acq2":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_ACQ2;
						break;
					case "_rec_info":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_REC_INFO;
						break;
					case "uartsend":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_UART_SEND;
						break;
					case "_judge_image":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_JUDGE_IMG;
						break;
					case "writeram":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_WRITE_RAM;
						break;
					case "startacq":
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_STARTACQ;
						break;
					default:
						m_CrntExecuItem[m_nCrntExecuItemNum].cmd = EXECUCMD.WZD_EMPTY;
						break;
					}
					if (!string.IsNullOrEmpty(xPathNodeIterator.Current.GetAttribute("cycle", "")))
					{
						m_CrntExecuItem[m_nCrntExecuItemNum].cycle = Convert.ToInt32(xPathNodeIterator.Current.GetAttribute("cycle", ""));
					}
					if (!string.IsNullOrEmpty(xPathNodeIterator.Current.GetAttribute("timeout", "")))
					{
						m_CrntExecuItem[m_nCrntExecuItemNum].timeout = Convert.ToInt32(xPathNodeIterator.Current.GetAttribute("timeout", ""));
					}
					else
					{
						m_CrntExecuItem[m_nCrntExecuItemNum].timeout = 0;
					}
					if (!string.IsNullOrEmpty(xPathNodeIterator.Current.GetAttribute("param", "")))
					{
						m_CrntExecuItem[m_nCrntExecuItemNum].cmdparam = xPathNodeIterator.Current.GetAttribute("param", "");
					}
					m_nCrntExecuItemNum++;
				}
				m_nCrntExecuItemPtr = 0;
				txtHint.Text = string.Format("Total step: {0}", m_nCrntExecuItemNum);
			}
			else
			{
				MessageBox.Show("NULL");
			}
		}

		private void ClearExecuItem(int index)
		{
			m_CrntExecuItem[index].cmd = EXECUCMD.WZD_EMPTY;
			m_CrntExecuItem[index].cmdparam = "";
			m_CrntExecuItem[index].cycle = 0;
			m_CrntExecuItem[index].timeout = 0;
		}

		private void SetMessageInfo(string message)
		{
			m_TxtLog.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff \t") + message);
			m_TxtLog.Flush();
		}

		private void DoRunTestCase()
		{
			SetMessageInfo(string.Format("\n====Test_Start (Test_Case_{0})====", m_crntStepIndex + 1));
			if (m_nCrntCycle > 0)
			{
				m_nTestCaseTimer.SetIntervalSeconds(m_nCrntCycle);
				m_nTestCaseTimer.Start();
			}
			m_nLoopTimes = 0L;
			m_nSwitchFilureTimes = 0;
			m_staLast = m_staNow;
			m_nCrntExecuItemPtr = -1;
			m_bRunning = true;
			m_nFpdBusyCnt = 0;
			m_nContiSendFailed = 0;
			m_nCrntFrameCount = 0;
			m_nFpdImageLoss = 0;
			m_nContiCmdSendFailed = 0;
			DoLoadNextExecuItem();
		}

		private void DoLoadNextExecuItem()
		{
			if (m_nCrntCycle > 0 && m_nCrntExecuItemPtr == m_nCrntExecuItemNum - 1)
			{
				return;
			}
			m_nCrntExecuItemPtr = (m_nCrntExecuItemPtr + 1) % m_nCrntExecuItemNum;
			if (m_nCrntExecuItemPtr == 0)
			{
				m_nLoopTimes++;
			}
			txtHint.Text = string.Format("Round: {0}, Step: {1}/{2}", m_nLoopTimes, m_nCrntExecuItemPtr + 1, m_nCrntExecuItemNum);
			m_staLast = m_staNow;
			int num = 0;
			switch (m_CrntExecuItem[m_nCrntExecuItemPtr].cmd)
			{
			case EXECUCMD.WZD_ACQ2:
				Thread.Sleep(100);
				SetMessageInfo("FD_Acquisition2_Start");
				m_nImageAcqCmdSendTime = DateTime.Now;
				num = Instance.Invoke(1003);
				if (num == 1)
				{
					m_nFpdBusyCnt = 0;
					m_nContiSendFailed = 0;
					m_nFpdImageLoss = 0;
					m_nContiCmdSendFailed = 0;
				}
				else if (Instance.Prop_Attr_Battery_Remaining <= 10)
				{
					m_nContiSendFailed++;
				}
				break;
			case EXECUCMD.WZD_PREPCAP:
				Thread.Sleep(100);
				SetMessageInfo("FD_Acquisition_Start");
				m_nImageAcqCmdSendTime = DateTime.Now;
				num = Instance.Invoke(1002);
				if (num == 1)
				{
					m_nFpdBusyCnt = 0;
					m_nContiSendFailed = 0;
					m_nFpdImageLoss = 0;
					m_nContiCmdSendFailed = 0;
				}
				else if (Instance.Prop_Attr_Battery_Remaining <= 10)
				{
					m_nContiSendFailed++;
				}
				break;
			case EXECUCMD.WZD_STARTACQ:
				Thread.Sleep(100);
				SetMessageInfo("FD_StartAcq_Start");
				m_nImageAcqCmdSendTime = DateTime.Now;
				num = Instance.Invoke(1004);
				if (num == 1)
				{
					m_nFpdBusyCnt = 0;
					m_nContiSendFailed = 0;
					m_nFpdImageLoss = 0;
					m_nContiCmdSendFailed = 0;
				}
				else if (Instance.Prop_Attr_Battery_Remaining <= 10)
				{
					m_nContiSendFailed++;
				}
				break;
			case EXECUCMD.WZD_SLEEP:
				Thread.Sleep(100);
				SetMessageInfo("FD_Sleep_Start");
				break;
			case EXECUCMD.WZD_WAKEUP:
				Thread.Sleep(100);
				SetMessageInfo("FD_Wakeup_Start");
				break;
			case EXECUCMD.WZD_REC_INFO:
				SetMessageInfo(string.Format("Battery Capacity: {0}-{1}, Net Mode: {2} , WiFi Link Quality: {3} (ESSID: {4})", Instance.Prop_Attr_Battery_Remaining, Instance.Prop_Attr_Battery_ChargingStatus, (Instance.Prop_Attr_NetworkInterface == 2) ? "WiFi" : "Other", Instance.Prop_Attr_WifiStatu_WorkingLinkQuality, Instance.Prop_Attr_WifiStatu_LinkedAP));
				if (m_bRunning)
				{
					DoLoadNextExecuItem();
				}
				break;
			}
			m_nTimeoutCnt = DateTime.Now.AddMilliseconds(m_CrntExecuItem[m_nCrntExecuItemPtr].timeout);
		}

		private void OnTimer()
		{
			if (Instance.Prop_Attr_ImageTransProgress > m_nLastImageTransProgres)
			{
				m_nLastPacketRecvTime = DateTime.Now;
			}
			else if (Instance.Prop_Attr_ImageTransProgress < m_nLastImageTransProgres)
			{
				m_nFirstPacketRecvTime = DateTime.Now;
			}
			m_nLastImageTransProgres = Instance.Prop_Attr_ImageTransProgress;
			if (m_CrntExecuItem[m_nCrntExecuItemPtr].timeout > 0 && (DateTime.Now - m_nTimeoutCnt).TotalMilliseconds >= 0.0 && m_staNow != m_staNext)
			{
				m_nSwitchFilureTimes++;
			}
			if ((DateTime.Now - m_nExecuItemClock).TotalMilliseconds > 0.0)
			{
				if (m_bRunning)
				{
					DoLoadNextExecuItem();
				}
				m_nExecuItemClock = DateTime.MaxValue;
			}
		}

		private void OnTestCaseTimer()
		{
			m_nCrntExecuItemPtr = -1;
			if (m_bRunning)
			{
				DoLoadNextExecuItem();
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/autotestcasewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				((AutoTestCaseWnd)target).Closed += Window_Closed;
				((AutoTestCaseWnd)target).Initialized += Window_Initialized;
				break;
			case 2:
				btnRun = (Button)target;
				btnRun.Click += RunButton_Click;
				break;
			case 3:
				btnStop = (Button)target;
				btnStop.Click += StopButton_Click;
				break;
			case 4:
				cboTestCase = (ComboBox)target;
				cboTestCase.SelectionChanged += ComboBox_SelectionChanged;
				break;
			case 5:
				txtHint = (TextBox)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
