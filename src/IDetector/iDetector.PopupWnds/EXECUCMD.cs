namespace iDetector.PopupWnds
{
	public enum EXECUCMD
	{
		WZD_PREPCAP,
		WZD_ACQ2,
		WZD_SLEEP,
		WZD_WAKEUP,
		WZD_REC_INFO,
		WZD_UART_SEND,
		WZD_JUDGE_IMG,
		WZD_WRITE_RAM,
		WZD_STARTACQ,
		WZD_EMPTY
	}
}
