using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace iDetector.PopupWnds
{
	public class SelectOffsetTypeWnd : Window, IComponentConnector
	{
		private Detector Instance;

		public int _setCorrectOption;

		internal Label _labSelectOffset;

		internal RadioButton xSWPreOffsetRB;

		internal RadioButton xSWPostOffsetRB;

		internal Button btnOk;

		private bool _contentLoaded;

		public SelectOffsetTypeWnd(Detector detector)
		{
			InitializeComponent();
			Instance = detector;
			base.Owner = Application.Current.MainWindow;
			_labSelectOffset.Content = "Please select offset type for gain";
			OpenPresetOffsetHint();
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			PresetOffsetOption presetOffsetOption = PresetOffsetOption.None;
			if (xSWPreOffsetRB.IsChecked == true)
			{
				presetOffsetOption = PresetOffsetOption.SWPreOffset;
			}
			else if (xSWPostOffsetRB.IsChecked == true)
			{
				presetOffsetOption = PresetOffsetOption.SWPostOffset;
			}
			int num = 196611;
			bool flag = ((Instance.Prop_Attr_CurrentCorrectOption & num) != 0) ? true : false;
			bool flag2 = ((Instance.Prop_Attr_CurrentCorrectOption & ~num) == 0) ? true : false;
			if (!flag || !flag2)
			{
				if (flag)
				{
					_setCorrectOption = (Instance.Prop_Attr_CurrentCorrectOption & num);
				}
				else if (presetOffsetOption != 0)
				{
					_setCorrectOption = (int)presetOffsetOption;
				}
				else
				{
					_setCorrectOption = 65536;
				}
				base.DialogResult = true;
			}
		}

		private void OpenPresetOffsetHint()
		{
			Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)Instance.Prop_Attr_UROM_ProductNo;
			if (Enm_ProdType.Enm_Prd_Mars1417XF == prop_Attr_UROM_ProductNo || Enm_ProdType.Enm_Prd_Mars1717XF == prop_Attr_UROM_ProductNo)
			{
				xSWPostOffsetRB.Visibility = Visibility.Visible;
				xSWPostOffsetRB.IsChecked = true;
				if ((5 == Instance.Prop_Attr_UROM_TriggerMode && 3 == Instance.Prop_Attr_UROM_FreesyncSubFlow) || 1 == Instance.Prop_Attr_UROM_TriggerMode || (3 == Instance.Prop_Attr_UROM_TriggerMode && 1 == Instance.Prop_Attr_UROM_PrepCapMode))
				{
					xSWPreOffsetRB.Visibility = Visibility.Collapsed;
				}
				else
				{
					xSWPreOffsetRB.Visibility = Visibility.Visible;
				}
				return;
			}
			if (1 == Instance.Prop_Attr_UROM_PrepCapMode)
			{
				xSWPostOffsetRB.Visibility = Visibility.Collapsed;
				xSWPreOffsetRB.Visibility = Visibility.Visible;
				xSWPreOffsetRB.IsChecked = true;
				return;
			}
			xSWPostOffsetRB.Visibility = Visibility.Visible;
			xSWPreOffsetRB.Visibility = Visibility.Visible;
			xSWPreOffsetRB.IsChecked = true;
			switch (prop_Attr_UROM_ProductNo)
			{
			case Enm_ProdType.Enm_Prd_Mammo1012F:
				xSWPreOffsetRB.Visibility = Visibility.Collapsed;
				xSWPostOffsetRB.IsChecked = true;
				break;
			case Enm_ProdType.Enm_Prd_Mars1417V:
			case Enm_ProdType.Enm_Prd_Mars1717V:
				xSWPreOffsetRB.Visibility = Visibility.Visible;
				xSWPreOffsetRB.IsChecked = true;
				break;
			case Enm_ProdType.Enm_Prd_Mars1012X:
			case Enm_ProdType.Enm_Prd_Mars1417X:
			case Enm_ProdType.Enm_Prd_Mars1417V2:
			case Enm_ProdType.Enm_Prd_Venu1012V:
			case Enm_ProdType.Enm_Prd_Mars1717V2:
			case Enm_ProdType.Enm_Prd_Venu1717X:
			case Enm_ProdType.Enm_Prd_Venu1717MX:
			case Enm_ProdType.Enm_Prd_Venu1012VD:
			case Enm_ProdType.Enm_Prd_Venu1717XV:
			case Enm_ProdType.Enm_Prd_Luna1417XM:
				if (5 == Instance.Prop_Attr_UROM_TriggerMode && 3 == Instance.Prop_Attr_UROM_FreesyncSubFlow)
				{
					xSWPreOffsetRB.Visibility = Visibility.Collapsed;
					xSWPostOffsetRB.IsChecked = true;
				}
				else
				{
					xSWPreOffsetRB.Visibility = Visibility.Visible;
					xSWPreOffsetRB.IsChecked = true;
				}
				break;
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/popupwnds/selectoffsettypewnd.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				_labSelectOffset = (Label)target;
				break;
			case 2:
				xSWPreOffsetRB = (RadioButton)target;
				break;
			case 3:
				xSWPostOffsetRB = (RadioButton)target;
				break;
			case 4:
				btnOk = (Button)target;
				btnOk.Click += btnOk_Click;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
