namespace iDetector.PopupWnds
{
	public enum CMDSTATE
	{
		Proc_Idle,
		Proc_Called,
		Proc_Succeed,
		Proc_Failed
	}
}
