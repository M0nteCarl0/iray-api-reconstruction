using System.Windows;
using System.Windows.Controls;

namespace CorrectUI.ViewModel
{
	internal class ConditionText : TextBlock
	{
		public ConditionText(string str)
		{
			base.TextWrapping = TextWrapping.Wrap;
			base.Margin = new Thickness(5.0);
			base.FontSize = 16.0;
			base.FontWeight = FontWeights.Bold;
			base.Text = str;
		}
	}
}
