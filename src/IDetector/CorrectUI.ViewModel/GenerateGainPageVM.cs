using iDetector;
using System;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class GenerateGainPageVM : GenerateStaticTemplate
	{
		private DoseInfo GainDose;

		private DoseAlgorithm DoseAlgorithm;

		private int ExpectedGray = 5500;

		private uint DoseKV = 70u;

		private string[] KVList;

		public GenerateGainPageVM(DetectorInfo detInstance, CorrectionImageListVM imagelist, DoseAlgorithm doseAlgorithm)
			: base(detInstance, imagelist)
		{
			DoseAlgorithm = doseAlgorithm;
			base.CorrectionMode = StringResource.FindString("CreateGain");
			KVList = new string[Math.Max(DetInstance.Detector.Prop_Attr_GainTotalFrames, 1)];
			LoadKVList(KVList);
			ExpectedGray = GetExpectedGrayValue(detInstance.Detector);
			GainDose = new DoseInfo(string.Format("{0}KV / {1}mAs", KVList[0], doseAlgorithm.GetDosemAs(Convert.ToUInt32(KVList[0]), ExpectedGray)));
		}

		private int GetExpectedGrayValue(Detector detector)
		{
			switch (detector.Prop_Attr_UROM_ProductNo)
			{
			case 45:
				return 40000;
			case 41:
			case 42:
			case 51:
			case 52:
			case 60:
			case 61:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				return 20000;
			case 32:
			case 37:
				if (detector.Prop_Attr_UROM_TriggerMode == 5)
				{
					return 10000;
				}
				return 5500;
			case 39:
			case 59:
			case 62:
				if (detector.Prop_Attr_UROM_TriggerMode == 5)
				{
					if (detector.Prop_Attr_UROM_FreesyncSubFlow == 3)
					{
						return 7000;
					}
					return 10000;
				}
				return 5500;
			default:
				return 5500;
			}
		}

		protected override void InitFinished()
		{
			InitImagelist(KVList);
			PrepareForAcquire();
			base.InitFinished();
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i == 0)
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, DoseKV.ToString(), lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, kvlist[0], lpFileName);
				}
			}
			ExpectedGray = (int)IniParser.GetPrivateProfileInt("Gain", "Gray", ExpectedGray, lpFileName);
		}

		public void StartGainInit(NavigationService navigationService)
		{
			Log.Instance().Write("Start GainInit");
			base.NavigationService = navigationService;
			InitForCreateTemplate(3002, "Initialize to create gain...");
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override void EnterNextCorrection()
		{
			if (base.NavigationService != null)
			{
				if (Next == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GeneratingGainPage(DetInstance, DoseAlgorithm));
				}
				else if (Skip == base.SkipOrNextLabel)
				{
					RemoveCallbackHandler();
					base.NavigationService.Navigate(new GenerateDefectPage(DetInstance, DoseAlgorithm, true));
				}
			}
		}

		protected override void EnterPrevCorrection()
		{
			if (base.NavigationService != null)
			{
				base.NavigationService.Navigate(new CorrectionStudyPage(DetInstance));
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override int SelectImage(int frameIndex)
		{
			return DetInstance.InvokeProxy.Invoke(3004, 0, frameIndex);
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			return ExpectedGray;
		}
	}
}
