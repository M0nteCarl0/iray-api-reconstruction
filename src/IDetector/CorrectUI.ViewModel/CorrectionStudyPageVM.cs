using iDetector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	public class CorrectionStudyPageVM : NotifyObject
	{
		private ImageSource skipOrNextImage;

		private string exposeBtnContent;

		private DoseItem doseSelectedItem;

		private int doseSelectedIndex;

		private string skipOrNextLabel;

		private bool enableStartAcqBtn;

		private bool prevBtnVisibility;

		private bool nextBtnVisibility;

		private bool studyCompletedVisibility;

		private bool enableBrowseBack;

		private bool enableBrowseForward;

		protected DetectorInfo DetInstance;

		private DlgTaskTimeOut MessageBoxTimer;

		protected int TimeoutSeconds = 30;

		protected readonly string PrepStr = StringResource.FindString("PrepareExpose");

		protected readonly string AcquireStr = StringResource.FindString("Acquire");

		protected readonly string Next = StringResource.FindString("NextStep");

		protected readonly string Skip = StringResource.FindString("Skip");

		protected string DoseUnit;

		private string AlgorithmSectionName = "Algorithm";

		private DoseAlgorithm DoseAlgorithm;

		private List<DoseFactor> DoseStudyList;

		private bool Prepared;

		private bool bPostEnable;

		private IRayImageData newImage;

		private bool _startAcqFlag;

		private bool HasRecvStartAcqACK;

		public ImageSource SkipOrNextImage
		{
			get
			{
				return skipOrNextImage;
			}
			set
			{
				skipOrNextImage = value;
				NotifyPropertyChanged("SkipOrNextImage");
			}
		}

		public string ExposeBtnContent
		{
			get
			{
				return exposeBtnContent;
			}
			set
			{
				exposeBtnContent = value;
				NotifyPropertyChanged("ExposeBtnContent");
			}
		}

		public ObservableCollection<DoseItem> DoseList
		{
			get;
			set;
		}

		public DoseItem DoseSelectedItem
		{
			get
			{
				return doseSelectedItem;
			}
			set
			{
				doseSelectedItem = value;
				doseSelectedIndex = DoseList.IndexOf(doseSelectedItem);
				if (doseSelectedIndex == 0)
				{
					PrevBtnVisibility = false;
					NextBtnVisibility = true;
				}
				else if (doseSelectedIndex == DoseList.Count - 1)
				{
					PrevBtnVisibility = true;
					NextBtnVisibility = false;
				}
				else
				{
					PrevBtnVisibility = true;
					NextBtnVisibility = true;
				}
				NotifyPropertyChanged("DoseSelectedItem");
			}
		}

		private int DoseSelectedIndex
		{
			get
			{
				return doseSelectedIndex;
			}
			set
			{
				if (value >= 0 && value < DoseList.Count)
				{
					doseSelectedIndex = value;
					DoseSelectedItem = DoseList[value];
				}
			}
		}

		public string SkipOrNextLabel
		{
			get
			{
				return skipOrNextLabel;
			}
			set
			{
				skipOrNextLabel = value;
				NotifyPropertyChanged("SkipOrNextLabel");
			}
		}

		public bool EnableStartAcqBtn
		{
			get
			{
				return enableStartAcqBtn;
			}
			set
			{
				enableStartAcqBtn = value;
				NotifyPropertyChanged("EnableStartAcqBtn");
			}
		}

		public bool PrevBtnVisibility
		{
			get
			{
				return prevBtnVisibility;
			}
			set
			{
				prevBtnVisibility = value;
				NotifyPropertyChanged("PrevBtnVisibility");
			}
		}

		public bool NextBtnVisibility
		{
			get
			{
				return nextBtnVisibility;
			}
			set
			{
				nextBtnVisibility = value;
				NotifyPropertyChanged("NextBtnVisibility");
			}
		}

		public bool StudyCompletedVisibility
		{
			get
			{
				return studyCompletedVisibility;
			}
			set
			{
				studyCompletedVisibility = value;
				NotifyPropertyChanged("StudyCompletedVisibility");
			}
		}

		public bool EnableBrowseBack
		{
			get
			{
				return enableBrowseBack;
			}
			set
			{
				enableBrowseBack = value;
				NotifyPropertyChanged("EnableBrowseBack");
			}
		}

		public bool EnableBrowseForward
		{
			get
			{
				return enableBrowseForward;
			}
			set
			{
				enableBrowseForward = value;
				NotifyPropertyChanged("EnableBrowseForward");
			}
		}

		public DelegateCommand PrevFrame
		{
			get;
			set;
		}

		public DelegateCommand StartAcquire
		{
			get;
			set;
		}

		public DelegateCommand NextFrame
		{
			get;
			set;
		}

		public DelegateCommand BrowseForward
		{
			get;
			set;
		}

		public DelegateCommand BrowseBack
		{
			get;
			set;
		}

		protected NavigationService NavigationService
		{
			get;
			set;
		}

		public CircularProgressBar ProgressTimer
		{
			get;
			set;
		}

		public CorrectionStudyPageVM(DetectorInfo instance)
		{
			DetInstance = instance;
			PrevFrame = new DelegateCommand(OnPrevFrame);
			StartAcquire = new DelegateCommand(OnStartAcquire);
			NextFrame = new DelegateCommand(OnNextFrame);
			BrowseForward = new DelegateCommand(OnBrowseForward);
			BrowseBack = new DelegateCommand(OnBrowseBack);
			DoseAlgorithm = new DoseAlgorithm();
			EnableStartAcqBtn = true;
			Prepared = false;
			InitDoseUint();
			switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 68:
			case 73:
			case 74:
			case 87:
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					AlgorithmSectionName = "SyncInAlgorithm";
				}
				else if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					AlgorithmSectionName = "SyncOutAlgorithm";
				}
				break;
			}
			InitDoseInfo();
			ExposeBtnContent = PrepStr;
			newImage = new IRayImageData();
			_startAcqFlag = true;
			EnableEnterNextPage(true);
		}

		protected virtual void InitDoseUint()
		{
			DoseUnit = "mAs";
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (1001 == nEventID || 1012 == nEventID)
			{
				IRayImage image = (IRayImage)Marshal.PtrToStructure(pParam, typeof(IRayImage));
				newImage.Write(ref image);
				if (newImage.ImgData == null)
				{
					return;
				}
			}
			DetInstance.Shell.ParentUI.Dispatcher.BeginInvoke((Action)delegate
			{
				switch (nEventID)
				{
				case 1001:
				case 1012:
					StopExposeWindowTimerAfterRecvImage(sender);
					AcquiredImage(newImage);
					break;
				case 1002:
					StopExposeWindowTimerAfterRecvImage(sender);
					break;
				case 1005:
					if (!bPostEnable)
					{
						StartExposeWindowTimerAfterExpEnable(nParam1);
					}
					break;
				case 5:
				case 1003:
					ProcessFailedEvent(nParam1, nParam2);
					break;
				case 1008:
					StopExposeWindowTimer();
					break;
				case 4:
					ProcessSuceedEvent(nParam1);
					break;
				case 7:
					if (1004 == nParam1)
					{
						EnableStartAcqBtn = false;
						PrevBtnVisibility = false;
						NextBtnVisibility = false;
						EnableEnterNextPage(false);
						StartExposeWindowTimer();
					}
					break;
				case 1006:
					_startAcqFlag = false;
					ExposeBtnContent = PrepStr;
					EnableStartAcqBtn = true;
					Prepared = false;
					PrevBtnVisibility = true;
					NextBtnVisibility = true;
					EnableEnterNextPage(true);
					StopExposeWindowTimer();
					CloseMessageBox();
					break;
				case 1011:
					Prepared = false;
					PrevBtnVisibility = true;
					NextBtnVisibility = true;
					EnableStartAcqBtn = true;
					EnableEnterNextPage(true);
					StopTimer();
					break;
				}
			});
		}

		protected virtual void ProcessSuceedEvent(int cmdID)
		{
			switch (cmdID)
			{
			case 1002:
				break;
			case 1001:
				ExposeBtnContent = AcquireStr;
				EnableStartAcqBtn = true;
				Prepared = true;
				break;
			case 1003:
			case 1004:
			case 1006:
				HasRecvStartAcqACK = true;
				EnableStartAcqBtn = true;
				ExposeBtnContent = PrepStr;
				Prepared = false;
				EnableEnterNextPage(true);
				break;
			case 1005:
				CloseMessageBox();
				EnableStartAcqBtn = true;
				break;
			}
		}

		protected virtual void ProcessFailedEvent(int cmdID, int errorCode)
		{
			switch (cmdID)
			{
			case 1001:
			case 1003:
			case 1004:
			case 1006:
				StopTimer();
				ExposeBtnContent = PrepStr;
				EnableStartAcqBtn = true;
				Prepared = false;
				EnableEnterNextPage(true);
				break;
			case 1012:
				_startAcqFlag = false;
				ExposeBtnContent = PrepStr;
				EnableStartAcqBtn = true;
				Prepared = false;
				EnableEnterNextPage(true);
				StopExposeWindowTimer();
				break;
			case 6:
				EnableStartAcqBtn = false;
				NextBtnVisibility = false;
				break;
			case 1005:
				EnableStartAcqBtn = true;
				break;
			default:
				EnableStartAcqBtn = true;
				break;
			}
			CloseMessageBox();
			int num = 1003;
		}

		protected void EnableEnterNextPage(bool enable)
		{
			EnableBrowseBack = enable;
			EnableBrowseForward = enable;
		}

		private bool IsSoftwareMode()
		{
			if (DetInstance.Detector == null)
			{
				return false;
			}
			if (2 != DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
				return false;
			}
			return true;
		}

		protected int GetCenterAverageValue(IRayImageData image)
		{
			int result = 0;
			IRayVariantMapItem[] @params = image.Params;
			for (int i = 0; i < @params.Length; i++)
			{
				IRayVariantMapItem rayVariantMapItem = @params[i];
				if (rayVariantMapItem.nMapKey == 32788)
				{
					result = rayVariantMapItem.varMapVal.val.nVal;
					break;
				}
			}
			return result;
		}

		protected virtual void StartExposeWindowTimerAfterStartAcquire()
		{
			if (DetInstance.Detector == null)
			{
				return;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 1:
			case 2:
			case 4:
			case 5:
				break;
			case 0:
			case 3:
				if (52 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 51 != DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
					StartExposeWindowTimer();
				}
				break;
			}
		}

		private void StartExposeWindowTimerAfterExpEnable(int nCallbackDelayTime)
		{
			if (DetInstance.Detector == null)
			{
				return;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 4:
				break;
			case 0:
			case 3:
				if (52 == DetInstance.Detector.Prop_Attr_UROM_ProductNo || 51 == DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
					StartExposeWindowTimer(nCallbackDelayTime);
				}
				break;
			case 1:
			case 2:
			case 5:
				StartExposeWindowTimer(nCallbackDelayTime);
				break;
			}
		}

		private void StartExposeWindowTimer(int nCallbackDelayTime = -1)
		{
			if (ProgressTimer != null)
			{
				ProgressTimer.StartTimer((nCallbackDelayTime < 0) ? GetExposeWindowLength() : ((double)nCallbackDelayTime / 1000.0), ExposeTimerEndCallback);
			}
		}

		protected void StopExposeWindowTimer()
		{
			if (ProgressTimer != null)
			{
				ProgressTimer.AbortTimer();
			}
		}

		protected void StopExposeWindowTimerAfterRecvImage(Detector sender)
		{
			if (sender.Prop_Attr_UROM_ExpMode != 2 && sender.Prop_Attr_UROM_ExpMode != 1 && ProgressTimer != null)
			{
				ProgressTimer.StopTimer();
			}
		}

		private double GetExposeWindowLength()
		{
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 0:
			case 3:
				return 10.0;
			case 1:
			case 2:
				return (double)DetInstance.Detector.Prop_Attr_UROM_ExpWindowTime / 1000.0;
			case 5:
				return (double)DetInstance.Detector.Prop_Cfg_ClearAcqParam_DelayTime / 1000.0;
			default:
				return 0.0;
			}
		}

		private void ResumePrepStage()
		{
			Prepared = false;
			ExposeBtnContent = PrepStr;
		}

		protected virtual void ExposeTimerEndCallback()
		{
			if (!_startAcqFlag)
			{
				EnableEnterNextPage(true);
				ResumePrepStage();
			}
			else
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("WaitImage"), WaitImageTimeOut);
			}
		}

		protected void ShowMessageBox(string title, string content, Action timeOutHandler)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TimeoutSeconds, DetInstance.Shell.ParentUI, timeOutHandler);
			MessageBoxTimer.RunTask();
		}

		protected void ShowMessageBox(string title, string content, bool canCancel, Action cancelWait)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, canCancel, DetInstance.Shell.ParentUI, cancelWait);
			MessageBoxTimer.RunTask();
		}

		private void WaitImageTimeOut()
		{
			DetInstance.Shell.ShowMessage(StringResource.FindString("WaitImageTimeOut"));
			EnableStartAcqBtn = true;
		}

		protected void UpdateMessage(string content)
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.SetStatus(content);
			}
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		private void InitDoseInfo()
		{
			DoseStudyList = new List<DoseFactor>();
			DoseList = new ObservableCollection<DoseItem>();
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			uint privateProfileInt = IniParser.GetPrivateProfileInt("Study", "DoseCount", 3, lpFileName);
			for (int i = 0; i < privateProfileInt; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Study", lpKeyName, 0, lpFileName);
				lpKeyName = DoseUnit + (i + 1).ToString() + "_1";
				double privateProfileDouble = IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = DoseUnit + (i + 1).ToString() + "_2";
				double privateProfileDouble2 = IniParser.GetPrivateProfileDouble("Study", lpKeyName, 0.0, lpFileName);
				lpKeyName = "Coeff" + (i + 1).ToString();
				double privateProfileDouble3 = IniParser.GetPrivateProfileDouble(AlgorithmSectionName, lpKeyName, 0.0, lpFileName);
				lpKeyName = "Intercept" + (i + 1).ToString();
				double privateProfileDouble4 = IniParser.GetPrivateProfileDouble(AlgorithmSectionName, lpKeyName, 0.0, lpFileName);
				DoseAlgorithm.AddCoeffItem(privateProfileInt2, privateProfileDouble3, privateProfileDouble4);
				DoseStudyList.Add(new DoseFactor(privateProfileInt2, privateProfileDouble, privateProfileDouble2));
				DoseList.Add(new DoseItem(privateProfileInt2.ToString() + "KV, " + privateProfileDouble + DoseUnit));
				DoseList.Add(new DoseItem(privateProfileInt2.ToString() + "KV, " + privateProfileDouble2 + DoseUnit));
			}
			DoseAlgorithm.UpdateTimeCoeff(IniParser.GetPrivateProfileDouble(AlgorithmSectionName, "TimeCoeff", 1.0, lpFileName));
		}

		private void OnBrowseBack(object obj)
		{
			if (NavigationService != null)
			{
				Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
				if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Mammo1012F)
				{
					NavigationService.Navigate(new CorrectionPreparePage());
				}
				else
				{
					NavigationService.Navigate(new GenerateOffsetPage(DetInstance));
				}
			}
		}

		private void OnBrowseForward(object obj)
		{
			if (NavigationService != null)
			{
				switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
				case 6:
				case 46:
				case 48:
				case 49:
				case 50:
				case 56:
				case 68:
				case 73:
				case 74:
				case 87:
					NavigationService.Navigate(new GenerateDynamicGainDefectPage(DetInstance, DoseAlgorithm));
					break;
				case 58:
					NavigationService.Navigate(new GenerateDynamicAcqPage(DetInstance, DoseAlgorithm, Enm_FileTypes.Enm_File_Gain));
					break;
				default:
					NavigationService.Navigate(new GenerateGainPage(DetInstance, DoseAlgorithm));
					break;
				}
			}
		}

		protected virtual void InitParams()
		{
			PrevBtnVisibility = false;
			NextBtnVisibility = false;
		}

		private void OnNextFrame(object obj)
		{
			DoseSelectedIndex++;
		}

		private void OnPrevFrame(object obj)
		{
			DoseSelectedIndex--;
		}

		private void OnStartAcquire(object obj)
		{
			InitParams();
			int num = 0;
			DoseSelectedItem.IsChecked = false;
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 2:
			{
				bPostEnable = Prepared;
				Enm_ProdType prop_Attr_UROM_ProductNo = (Enm_ProdType)DetInstance.Detector.Prop_Attr_UROM_ProductNo;
				if (prop_Attr_UROM_ProductNo == Enm_ProdType.Enm_Prd_Venu1717MN)
				{
					num = DetInstance.InvokeProxy.Invoke(1004);
				}
				else if (Prepared)
				{
					_startAcqFlag = true;
					num = DetInstance.InvokeProxy.Invoke(1004);
				}
				else
				{
					_startAcqFlag = false;
					num = DetInstance.InvokeProxy.Invoke(1001);
				}
				break;
			}
			case 3:
				_startAcqFlag = true;
				switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
				case 45:
					num = DetInstance.InvokeProxy.Invoke(1012);
					break;
				case 41:
				case 61:
					num = DetInstance.InvokeProxy.Invoke(3030);
					break;
				case 51:
				case 52:
					num = DetInstance.InvokeProxy.Invoke(1003);
					break;
				}
				break;
			default:
				_startAcqFlag = true;
				num = DetInstance.InvokeProxy.Invoke(1004);
				break;
			}
			if (1 == num || num == 0)
			{
				EnableStartAcqBtn = false;
				if (1 == num)
				{
					EnableEnterNextPage(false);
				}
				StartExposeWindowTimerAfterStartAcquire();
			}
			if (bPostEnable)
			{
				StopExposeWindowTimer();
			}
		}

		protected virtual void AcquiredImage(IRayImageData image)
		{
			if (HasRecvStartAcqACK)
			{
				HasRecvStartAcqACK = false;
				EnableStartAcqBtn = true;
			}
			StopTimer();
			CloseMessageBox();
			GroupAcquireFinised(GetCenterAverageValue(image));
		}

		protected void GroupAcquireFinised(int grayValue)
		{
			DoseSelectedItem.IsChecked = true;
			DoseSelectedIndex = DoseSelectedIndex;
			if (DoseSelectedIndex % 2 == 0)
			{
				DoseFactor doseFactor = DoseStudyList[DoseSelectedIndex / 2];
			}
			else
			{
				DoseFactor item = DoseStudyList[DoseSelectedIndex / 2];
				item.Gray2 = grayValue;
				Coefficient coefficient = StudyAlgorithm.CalculateCoeff(item);
				DoseAlgorithm.UpdateCoeff(item.KV, coefficient.Coeff, coefficient.Intercept);
				UpdateIniFile(DoseSelectedIndex / 2, coefficient.Coeff, coefficient.Intercept);
			}
			if (IsStudyCompleted())
			{
				StudyCompletedVisibility = true;
				SkipOrNextLabel = Next;
				SkipOrNextImage = new BitmapImage(new Uri("/Images/next.png", UriKind.Relative));
				UpdateIntervalTime();
			}
		}

		private void UpdateIntervalTime()
		{
			double timeCoeff = DetInstance.Detector.Prop_Attr_UROM_SyncExpTime;
			DoseAlgorithm.UpdateTimeCoeff(timeCoeff);
			IniParser.WritePrivateProfileString(AlgorithmSectionName, "TimeCoeff", timeCoeff.ToString("F1"), DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini");
		}

		private void UpdateIniFile(int index, double coeff, double intercept)
		{
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			string lpKeyName = "Coeff" + (index + 1).ToString();
			IniParser.WritePrivateProfileString(AlgorithmSectionName, lpKeyName, coeff.ToString(), lpFileName);
			lpKeyName = "Intercept" + (index + 1).ToString();
			IniParser.WritePrivateProfileString(AlgorithmSectionName, lpKeyName, intercept.ToString(), lpFileName);
		}

		protected bool IsStudyCompleted()
		{
			foreach (DoseItem dose in DoseList)
			{
				if (dose.IsChecked == false)
				{
					return false;
				}
			}
			return true;
		}

		public void InitNavigationService(NavigationService navigationService)
		{
			NavigationService = navigationService;
			DoseSelectedIndex = 0;
			SkipOrNextLabel = Skip;
			SkipOrNextImage = new BitmapImage(new Uri("/Images/skip.png", UriKind.Relative));
			foreach (DoseItem dose in DoseList)
			{
				dose.IsChecked = false;
			}
			StudyCompletedVisibility = false;
			EnableEnterNextPage(true);
			EnableBrowseForward = true;
			ExposeBtnContent = PrepStr;
			Prepared = false;
		}

		protected void StopTimer()
		{
			if (ProgressTimer != null)
			{
				ProgressTimer.StopTimer();
			}
		}

		public void AddCallbackHandler()
		{
			DetInstance.Detector.SubscribeCBEvent(OnSdkCallback);
		}

		public void RemoveCallbackHandler()
		{
			StopTimer();
			DetInstance.Detector.UnSubscribeCBEvent(OnSdkCallback);
		}
	}
}
