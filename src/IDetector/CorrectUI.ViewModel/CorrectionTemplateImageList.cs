using iDetector;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;

namespace CorrectUI.ViewModel
{
	public class CorrectionTemplateImageList : NotifyObject
	{
		private ImageListItem selectedItem;

		private int selectedInex;

		public ObservableCollection<ImageListItem> ThumbList
		{
			get;
			set;
		}

		public ImageListItem SelectedItem
		{
			get
			{
				return selectedItem;
			}
			set
			{
				selectedItem = value;
				selectedInex = ThumbList.IndexOf(selectedItem);
				NotifyPropertyChanged("SelectedItem");
				if (this.SelectItemChanged != null)
				{
					this.SelectItemChanged(selectedInex);
				}
			}
		}

		public int SelectedIndex
		{
			get
			{
				return selectedInex;
			}
			set
			{
				if (value >= 0 && value < ThumbList.Count)
				{
					selectedInex = value;
					SelectedItem = ThumbList[value];
				}
			}
		}

		public ImageSource SelectedImage
		{
			get
			{
				return SelectedItem.xImage.Source;
			}
		}

		public event Action<int> SelectItemChanged;

		public event Action<IRayImageData> DisplayThumb;

		public void DoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (this.DisplayThumb != null)
			{
				OnDisplayThumbImage();
			}
		}

		public void ClearThumbs()
		{
			foreach (ImageListItem thumb in ThumbList)
			{
				thumb.ClearThumb();
			}
		}

		public void ClearThumb(int index)
		{
			if (index < ThumbList.Count)
			{
				ThumbList[index].ClearThumb();
			}
		}

		public virtual void InitThumbList(string[] kvArray, int imageSize)
		{
		}

		public virtual void DeInitThumbList()
		{
		}

		public virtual void UpdateImage(int index, IRayImageData image)
		{
		}

		public virtual void GroupAcquireFinished(int groupNo, IRayImageData lastImage)
		{
		}

		protected virtual void OnDisplayThumbImage()
		{
		}

		protected void DisplayThumbImage(IRayImageData image)
		{
			if (this.DisplayThumb != null && image != null)
			{
				this.DisplayThumb(image);
			}
		}

		public void SetImageValidility(ImageStatus status, int fileIndex)
		{
			ThumbList[fileIndex].SetImageValidility(status);
		}
	}
}
