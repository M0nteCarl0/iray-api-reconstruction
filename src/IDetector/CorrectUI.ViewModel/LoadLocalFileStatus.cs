namespace CorrectUI.ViewModel
{
	public enum LoadLocalFileStatus
	{
		None = 1,
		Incomplete,
		Ok
	}
}
