using iDetector;
using System;
using System.Threading;

namespace CorrectUI.ViewModel
{
	public class SyncLoadLocalDefectTemporaryFile
	{
		protected bool[,] LocalTemplateFileStatus;

		private UpdateStatusProgress UpdateLoadLocalStatus;

		private int OneGroupDefectImageNumber;

		private int GroupNumber;

		private int TotalDefectFiles;

		private DetectorInfo DetInstance;

		private int TaskTimeOutSeconds;

		private string LocalFilePath;

		public event Action<bool, int, LoadLocalFileStatus> GroupLoadFinished;

		public SyncLoadLocalDefectTemporaryFile(DetectorInfo detector, string filePath, int groupNumber, int oneGroupDefectImageNumber, int timeoutSeconds)
		{
			DetInstance = detector;
			LocalFilePath = filePath;
			GroupNumber = groupNumber;
			TaskTimeOutSeconds = timeoutSeconds;
			OneGroupDefectImageNumber = oneGroupDefectImageNumber;
			TotalDefectFiles = GroupNumber * OneGroupDefectImageNumber;
			LocalTemplateFileStatus = new bool[groupNumber, oneGroupDefectImageNumber];
			Array.Clear(LocalTemplateFileStatus, 0, LocalTemplateFileStatus.Length);
		}

		public void StartLoadLocalFile()
		{
			UpdateLoadLocalStatus = new UpdateStatusProgress(DetInstance.Shell.ParentUI, "Load local file", 100, TaskTimeOutSeconds * 1000);
			UpdateLoadLocalStatus.RunTask();
			UpdateLoadLocalStatus.SetStatus("Start load local template file");
			new Thread((ThreadStart)delegate
			{
				SyncLoadLocalDefectTemporaryFile syncLoadLocalDefectTemporaryFile = this;
				int groupIndex = default(int);
				int totalIndex = default(int);
				bool retResult = default(bool);
				for (groupIndex = 0; groupIndex < GroupNumber; groupIndex++)
				{
					for (int i = 0; i < OneGroupDefectImageNumber; i++)
					{
						totalIndex = groupIndex * OneGroupDefectImageNumber + i;
						retResult = LoadTemporaryDefectFile(totalIndex);
						DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
						{
							syncLoadLocalDefectTemporaryFile.UpdateLoadProgress(retResult, totalIndex + 1);
						}, new object[0]);
						LocalTemplateFileStatus[groupIndex, i] = retResult;
					}
					Thread.Sleep(50);
					DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
					{
						if (groupIndex == syncLoadLocalDefectTemporaryFile.GroupNumber - 1)
						{
							syncLoadLocalDefectTemporaryFile.StopLoadLocalFile();
							syncLoadLocalDefectTemporaryFile.GroupLoadFinished(true, groupIndex, syncLoadLocalDefectTemporaryFile.CheckLocalFileCompleteness(groupIndex));
						}
						else
						{
							syncLoadLocalDefectTemporaryFile.GroupLoadFinished(false, groupIndex, syncLoadLocalDefectTemporaryFile.CheckLocalFileCompleteness(groupIndex));
						}
					}, new object[0]);
				}
			}).Start();
		}

		public void UpdateLoadProgress(bool prevStatus, int curIndex)
		{
			string str = "Load local file:" + curIndex.ToString() + "/" + TotalDefectFiles.ToString();
			str = ((!prevStatus) ? (str + " - Failed") : (str + " - Succeed"));
			UpdateLoadLocalStatus.SetStatus(str);
		}

		public void StopLoadLocalFile()
		{
			UpdateLoadLocalStatus.StopTask();
		}

		public LoadLocalFileStatus CheckLocalFileCompleteness(int groupIndex)
		{
			int num = 0;
			for (int i = 0; i < OneGroupDefectImageNumber; i++)
			{
				if (!LocalTemplateFileStatus[groupIndex, i])
				{
					num++;
				}
			}
			if (num == OneGroupDefectImageNumber)
			{
				return LoadLocalFileStatus.None;
			}
			if (num == 0)
			{
				return LoadLocalFileStatus.Ok;
			}
			return LoadLocalFileStatus.Incomplete;
		}

		private bool LoadTemporaryDefectFile(int fileIndex)
		{
			if (fileIndex >= DetInstance.Detector.Prop_Attr_DefectTotalFrames)
			{
				return false;
			}
			lock (this)
			{
				int num = DetInstance.Detector.Invoke(3008, 0, LocalFilePath, fileIndex);
				return 0 == num;
			}
		}
	}
}
