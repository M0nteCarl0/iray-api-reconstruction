using iDetector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class GenerateDynamicDefectPage4MNVM : GenerateDynamicTemplate
	{
		private const int DoseCount = 4;

		private DoseAlgorithm DoseAlgorithm;

		private string[] KVList;

		private DoseInfo[] DefectDoseList;

		private List<GrayMap> GrayList;

		private int mReceivedValidFrames;

		public GenerateDynamicDefectPage4MNVM(DetectorInfo instance, CorrectionTemplateImageList imagelist, DoseAlgorithm doseAlgorithm)
			: base(instance, imagelist)
		{
			DoseAlgorithm = doseAlgorithm;
			base.CorrectionMode = StringResource.FindString("CreateDefect");
			int num = instance.Detector.Prop_Attr_DefectTotalFrames / 2;
			num = ((num == 0) ? 8 : num);
			KVList = new string[num];
			LoadKVList(KVList);
			DefectDoseList = new DoseInfo[num];
			int num2 = 0;
			for (int i = 0; i < GrayList.Count; i++)
			{
				for (int j = 0; j < GrayList[i].Frames; j++)
				{
					DefectDoseList[num2] = new DoseInfo(string.Format("{0}kV / {1}mAs", GrayList[i].KVValue, DoseAlgorithm.GetDosemAs((uint)GrayList[i].KVValue, GrayList[i].GrayValue)));
					num2++;
				}
			}
			InitImagelist(KVList);
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			int privateProfileInt = (int)IniParser.GetPrivateProfileInt("defect", "Cfg_FramesOfHighestDose", 5, DetInstance.Detector.Prop_Attr_WorkDir + "calib.ini");
			GrayList = new List<GrayMap>
			{
				new GrayMap(1000, 70, 1),
				new GrayMap(5000, 140, 1),
				new GrayMap(10000, 110, 1),
				new GrayMap(2000, 90, privateProfileInt)
			};
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			int num = GrayList.Count();
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i < num)
				{
					uint privateProfileInt2 = IniParser.GetPrivateProfileInt("Defect", lpKeyName, GrayList[i].KVValue, lpFileName);
					kvlist[i] = privateProfileInt2.ToString();
					GrayList[i].KVValue = (int)privateProfileInt2;
					GrayList[i].GrayValue = (int)IniParser.GetPrivateProfileInt("Defect", string.Format("Gray{0}", i + 1), GrayList[i].KVValue, lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileInt("Defect", lpKeyName, GrayList[num - 1].KVValue, lpFileName).ToString();
				}
			}
		}

		protected override void GetGroupInfo(out int groupNumbers, out int eachGroupDefectImageNumber)
		{
			groupNumbers = KVList.Length;
			eachGroupDefectImageNumber = 1;
		}

		public override void StartInit(NavigationService navigationService)
		{
			base.NavigationService = navigationService;
			InitForCreateTemplate(3007, "Initialize to create defect...");
			SetDoseInfo(DefectDoseList[0].GetDoseInfo());
		}

		protected override int StartSelectImages()
		{
			int groupIndex = GetGroupIndex();
			int result = DetInstance.InvokeProxy.Invoke(3033, groupIndex, GrayList[groupIndex].Frames);
			mReceivedValidFrames = DetInstance.Detector.Prop_Attr_DefectValidFrames;
			return result;
		}

		protected override void EnterExposeView()
		{
			int groupIndex = GetGroupIndex();
			int num = 0;
			for (int i = 0; i < groupIndex; i++)
			{
				num += GrayList[i].Frames;
			}
			ImageListVM.SelectedIndex = num;
			if (GrayList[groupIndex].Frames > 1)
			{
				for (int j = 0; j < GrayList[groupIndex].Frames; j++)
				{
					ImageListVM.ClearThumb(num + j);
					AcquiredStatus[num + j] = false;
				}
			}
			base.EnterExposeView();
		}

		protected override void AcquiredImage()
		{
			base.AcquiredImage();
			if (EnableBufferCine && DetInstance.Detector.Prop_Attr_DefectValidFrames - mReceivedValidFrames > 0)
			{
				mReceivedValidFrames = DetInstance.Detector.Prop_Attr_DefectValidFrames;
				ImageListVM.UpdateImage(ImageListVM.SelectedIndex, newImage);
				UpdateValidImage();
				base.EnableStartAcqBtn = false;
				Convert.ToInt32(KVList[ImageListVM.SelectedIndex]);
				int groupIndex = GetGroupIndex();
				if (GrayList[groupIndex].Frames > 1 && ImageListVM.SelectedIndex < KVList.Length - 1)
				{
					ImageListVM.SelectedIndex++;
				}
			}
			base.PrevBtnVisibility = false;
			base.NextBtnVisibility = false;
		}

		private int GetGroupIndex()
		{
			int result = ImageListVM.SelectedIndex;
			if (ImageListVM.SelectedIndex >= GrayList.Count)
			{
				result = GrayList.Count - 1;
			}
			return result;
		}

		private void UpdateValidImage()
		{
			AcquiredStatus[ImageListVM.SelectedIndex] = true;
			if (GrayValueBeyondExpected)
			{
				ImageListVM.SelectedItem.SetImageValidility(ImageStatus.OK);
			}
			else
			{
				ImageListVM.SelectedItem.SetImageValidility(ImageStatus.QUESTION);
			}
		}

		protected override void UpdateValidLightImageStatus()
		{
			Convert.ToInt32(KVList[ImageListVM.SelectedIndex]);
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			int groupIndex = GetGroupIndex();
			if (num <= GrayList[groupIndex].Frames)
			{
				UpdateProgressInner(num);
				if (num >= GrayList[groupIndex].Frames && GrayList[groupIndex].Frames == num)
				{
					StopCheckFramesMonitor();
					AcquireDarkImages();
					EnableBufferCine = false;
				}
			}
		}

		protected override void UpdateValidDarkImageStatus()
		{
			Convert.ToInt32(KVList[ImageListVM.SelectedIndex]);
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			UpdateProgressInner(num);
			int groupIndex = GetGroupIndex();
			if (num >= GrayList[groupIndex].Frames * 2 && GrayList[groupIndex].Frames * 2 == num)
			{
				StopCheckFramesMonitor();
				DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
				{
					base.EnableLoadFileBtn = true;
					GroupAcquireFinished();
				}, new object[0]);
				EnterNextStage(Enm_CreateStage.SelectFinished);
			}
		}

		protected override void EnterNextCorrection()
		{
			if (base.NavigationService != null)
			{
				if (Next == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GeneratingDefectPage(DetInstance, DoseAlgorithm));
				}
				else if (Skip == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GenerateTemplateEnd(DetInstance, DoseAlgorithm));
				}
			}
		}

		protected override void EnterPrevCorrection()
		{
			if (base.NavigationService != null)
			{
				base.NavigationService.Navigate(new GenerateDynamicAcqPage(DetInstance, DoseAlgorithm, Enm_FileTypes.Enm_File_Gain));
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			if (frameIndex < DefectDoseList.Length)
			{
				SetDoseInfo(DefectDoseList[frameIndex].GetDoseInfo());
			}
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			frameIndex = ((frameIndex < 4) ? frameIndex : 3);
			Convert.ToInt32(KVList[frameIndex]);
			GetGroupIndex();
			return GrayList[GetGroupIndex()].GrayValue;
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			if (IsInExposeView)
			{
				Convert.ToInt32(KVList[ImageListVM.SelectedIndex]);
				base.AcquiedProgress = acquiredFrames.ToString() + "/" + GrayList[GetGroupIndex()].Frames * 2;
			}
		}

		private void UpdateProgressInner(int acquiredFrames)
		{
			Convert.ToInt32(KVList[ImageListVM.SelectedIndex]);
			base.AcquiedProgress = acquiredFrames.ToString() + "/" + GrayList[GetGroupIndex()].Frames * 2;
		}
	}
}
