using iDetector;
using System.Windows.Forms;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class GenerateDefectPageVM : GenerateStaticTemplate
	{
		private const int DoseCount = 4;

		private DoseInfo[] DefectDoseList;

		private string[] KVList;

		private int[] ExpectedGrayList;

		private string LocalFilePath;

		private SyncLoadLocalDefectTemporaryFile LoadLocalFile;

		private bool LoadLocalFileFlag;

		private bool AllLoadFinishedFlag;

		private bool SkipGeneratingPage;

		private DoseAlgorithm DoseAlgorithm;

		public GenerateDefectPageVM(DetectorInfo instance, CorrectionImageListVM imagelist, DoseAlgorithm doseAlgorithm, bool skipGeneratingPage)
			: base(instance, imagelist)
		{
			SkipGeneratingPage = skipGeneratingPage;
			DoseAlgorithm = doseAlgorithm;
			base.LoadImageFromFile = new DelegateCommand(OnLoadImageFromFile);
			base.CorrectionMode = StringResource.FindString("CreateDefect");
			ExpectedGrayList = GetExpectedGrayValue(instance.Detector);
			int prop_Attr_DefectTotalFrames = instance.Detector.Prop_Attr_DefectTotalFrames;
			prop_Attr_DefectTotalFrames = ((prop_Attr_DefectTotalFrames == 0) ? 19 : prop_Attr_DefectTotalFrames);
			KVList = new string[prop_Attr_DefectTotalFrames];
			LoadKVList(KVList);
			DefectDoseList = new DoseInfo[4];
			for (int i = 0; i < 4; i++)
			{
				uint result = 0u;
				if (uint.TryParse(KVList[i], out result))
				{
					DefectDoseList[i] = new DoseInfo(string.Format("{0}kV / {1}mAs", KVList[i], DoseAlgorithm.GetDosemAs(result, ExpectedGrayList[i])));
				}
				else
				{
					DefectDoseList[i] = new DoseInfo(KVList[i]);
				}
			}
			base.EnableLoadFileBtn = true;
			LoadLocalFileFlag = false;
			AllLoadFinishedFlag = true;
		}

		private int[] GetExpectedGrayValue(Detector detector)
		{
			switch (detector.Prop_Attr_UROM_ProductNo)
			{
			case 45:
				return new int[4]
				{
					4000,
					4000,
					16000,
					40000
				};
			case 41:
			case 42:
			case 51:
			case 52:
			case 60:
			case 61:
			case 72:
			case 80:
			case 88:
			case 91:
			case 93:
				return new int[4]
				{
					4000,
					4000,
					16000,
					20000
				};
			default:
				return new int[4]
				{
					1000,
					1000,
					4000,
					11000
				};
			}
		}

		protected override void InitFinished()
		{
			InitImagelist(KVList);
			PrepareForAcquire();
			LoadLocalFileFlag = false;
			AllLoadFinishedFlag = true;
			base.InitFinished();
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i < 4)
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Defect", lpKeyName, "", lpFileName);
					ExpectedGrayList[i] = (int)IniParser.GetPrivateProfileInt("Defect", string.Format("Gray{0}", i + 1), ExpectedGrayList[i], lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Defect", lpKeyName, kvlist[3], lpFileName);
				}
			}
		}

		private void OnLoadImageFromFile(object obj)
		{
			FolderBrowserDialogEx folderBrowserDialogEx = new FolderBrowserDialogEx();
			folderBrowserDialogEx.SelectedPath = LocalFilePath;
			folderBrowserDialogEx.Description = "Select a location to load the image files.";
			folderBrowserDialogEx.ShowNewFolderButton = true;
			folderBrowserDialogEx.ShowFullPathInEditBox = true;
			if (DialogResult.OK != folderBrowserDialogEx.ShowDialog())
			{
				return;
			}
			if (folderBrowserDialogEx.SelectedPath == string.Empty)
			{
				DetInstance.Shell.ShowMessage("Path cannot be empty!");
				return;
			}
			if (DetInstance.Detector == null)
			{
				DetInstance.Shell.ShowMessage("Detector instance is null!");
				return;
			}
			base.AcquiedProgress = "";
			for (int i = 0; i < AcquiredStatus.Count; i++)
			{
				AcquiredStatus[i] = false;
			}
			LocalFilePath = folderBrowserDialogEx.SelectedPath;
			LoadLocalFile = new SyncLoadLocalDefectTemporaryFile(DetInstance, folderBrowserDialogEx.SelectedPath, DetInstance.Detector.Prop_Attr_DefectTotalFrames, 1, 500);
			LoadLocalFile.GroupLoadFinished += OnGroupLoadFinished;
			ImageListVM.SelectedIndex = 0;
			AllLoadFinishedFlag = false;
			StartLoadLocalFile();
		}

		private void StartLoadLocalFile()
		{
			LoadLocalFileFlag = true;
			LoadLocalFile.StartLoadLocalFile();
		}

		private void OnGroupLoadFinished(bool bAllFinished, int groupIndex, LoadLocalFileStatus status)
		{
			if (bAllFinished)
			{
				AllLoadFinishedFlag = true;
				LoadLocalFileFlag = false;
			}
			if (LoadLocalFileStatus.Ok != status)
			{
				ImageListVM.SelectedIndex = groupIndex + 1;
				if (groupIndex + 1 == DetInstance.Detector.Prop_Attr_DefectTotalFrames)
				{
					LoadLocalFileFlag = false;
				}
			}
		}

		public void StartDefectInit(NavigationService navigationService)
		{
			InitForCreateTemplate(3007, "Initialize to create defect...");
			base.NavigationService = navigationService;
			SetDoseInfo(DefectDoseList[0].GetDoseInfo());
		}

		protected override void EnterNextCorrection()
		{
			if (base.NavigationService != null)
			{
				if (Next == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GeneratingDefectPage(DetInstance, DoseAlgorithm));
				}
				else if (Skip == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GenerateTemplateEnd(DetInstance, DoseAlgorithm));
				}
			}
		}

		protected override void EnterPrevCorrection()
		{
			if (base.NavigationService != null)
			{
				base.NavigationService.Navigate(new GenerateGainPage(DetInstance, DoseAlgorithm));
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			int num = (frameIndex < 4) ? frameIndex : 3;
			SetDoseInfo(DefectDoseList[num].GetDoseInfo());
		}

		protected override int SelectImage(int frameIndex)
		{
			if (LoadLocalFileFlag)
			{
				return 0;
			}
			return DetInstance.InvokeProxy.Invoke(3009, frameIndex);
		}

		protected override void SelectImageSucceed()
		{
			base.SelectImageSucceed();
			if (LoadLocalFileFlag)
			{
				ImageListVM.SelectedIndex++;
			}
			if (AllLoadFinishedFlag)
			{
				LoadLocalFileFlag = false;
			}
		}

		protected override void SelectImageFailed()
		{
			base.SelectImageFailed();
			if (LoadLocalFileFlag)
			{
				ImageListVM.SelectedIndex++;
			}
			if (AllLoadFinishedFlag)
			{
				LoadLocalFileFlag = false;
			}
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			frameIndex = ((frameIndex < 4) ? frameIndex : 3);
			return ExpectedGrayList[frameIndex];
		}

		protected override void AcquiredImage()
		{
			base.AcquiredImage();
		}
	}
}
