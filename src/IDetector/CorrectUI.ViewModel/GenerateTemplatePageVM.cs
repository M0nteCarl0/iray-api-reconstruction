using iDetector;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	public class GenerateTemplatePageVM : NotifyObject
	{
		public enum GrayValueStatus
		{
			Below,
			Expected,
			Above
		}

		private bool grayHintVisibility;

		private bool warningVisibility;

		private bool normalHintVisibility;

		private bool enableLoadFileBtn;

		private bool enableStartAcqBtn;

		private bool prevBtnVisibility;

		private bool nextBtnVisibility;

		private bool backLinkVisibilityInAcquireMode;

		private bool backLinkVisibilityInDoseMode;

		private bool imageViewVisibility;

		private bool doseViewVisibility;

		private bool splitterVisibility;

		private string correctionMode;

		private string doseValue;

		private string acquiedProgress;

		private string grayValueAlarm;

		private string exposeBtnContent;

		private string skipOrNextLabel;

		private bool enableBrowseBack;

		private bool enableBrowseForward;

		private ImageSource skipOrNextImage;

		private bool enableImagelist;

		private string flagForAutoTest;

		protected readonly string ExposeStr = StringResource.FindString("PrepareExpose");

		protected readonly string ReExposeStr = StringResource.FindString("ReExpose");

		protected readonly string Next = StringResource.FindString("NextStep");

		protected readonly string Skip = StringResource.FindString("Skip");

		protected readonly string CompleteStr = StringResource.FindString("CompletedAcquire");

		protected bool IsInExposeView;

		protected bool IsCanceled;

		private DlgTaskTimeOut MessageBoxTimer;

		protected int TimeoutSeconds = 30;

		protected DetectorInfo DetInstance;

		protected bool GrayValueBeyondExpected;

		protected bool IsBackBtnPressed;

		protected int GroupNumber;

		protected int TotalFrames;

		protected List<bool> AcquiredStatus;

		protected CorrectionTemplateImageList ImageListVM;

		protected bool _startAcqFlag;

		protected IRayImageData newImage;

		protected bool InitedStatus;

		public DelegateCommand BrowseForward
		{
			get;
			set;
		}

		public DelegateCommand BrowseBack
		{
			get;
			set;
		}

		public DelegateCommand PrevFrame
		{
			get;
			set;
		}

		public DelegateCommand StartAcq
		{
			get;
			set;
		}

		public DelegateCommand NextFrame
		{
			get;
			set;
		}

		public DelegateCommand LoadImageFromFile
		{
			get;
			set;
		}

		public bool GrayHintVisibility
		{
			get
			{
				return grayHintVisibility;
			}
			set
			{
				grayHintVisibility = value;
				NotifyPropertyChanged("GrayHintVisibility");
			}
		}

		public bool WarningHintVisibility
		{
			get
			{
				return warningVisibility;
			}
			set
			{
				GrayHintVisibility = true;
				warningVisibility = value;
				NotifyPropertyChanged("WarningHintVisibility");
				NormalHintVisibility = !value;
			}
		}

		public bool NormalHintVisibility
		{
			get
			{
				return normalHintVisibility;
			}
			set
			{
				normalHintVisibility = value;
				NotifyPropertyChanged("NormalHintVisibility");
			}
		}

		public bool EnableLoadFileBtn
		{
			get
			{
				return enableLoadFileBtn;
			}
			set
			{
				enableLoadFileBtn = value;
				NotifyPropertyChanged("EnableLoadFileBtn");
			}
		}

		public bool EnableStartAcqBtn
		{
			get
			{
				return enableStartAcqBtn;
			}
			set
			{
				enableStartAcqBtn = value;
				NotifyPropertyChanged("EnableStartAcqBtn");
			}
		}

		public bool PrevBtnVisibility
		{
			get
			{
				return prevBtnVisibility;
			}
			set
			{
				prevBtnVisibility = value;
				NotifyPropertyChanged("PrevBtnVisibility");
			}
		}

		public bool NextBtnVisibility
		{
			get
			{
				return nextBtnVisibility;
			}
			set
			{
				nextBtnVisibility = value;
				NotifyPropertyChanged("NextBtnVisibility");
				if (!value)
				{
					FlagForAutoTest = "Busy";
				}
			}
		}

		public bool BackLinkVisibilityInAcquireMode
		{
			get
			{
				return backLinkVisibilityInAcquireMode;
			}
			set
			{
				backLinkVisibilityInAcquireMode = value;
				NotifyPropertyChanged("BackLinkVisibilityInAcquireMode");
			}
		}

		public bool BackLinkVisibilityInDoseMode
		{
			get
			{
				return backLinkVisibilityInDoseMode;
			}
			set
			{
				backLinkVisibilityInDoseMode = value;
				NotifyPropertyChanged("BackLinkVisibilityInDoseMode");
			}
		}

		public bool ImageViewVisibility
		{
			get
			{
				return imageViewVisibility;
			}
			set
			{
				imageViewVisibility = value;
				NotifyPropertyChanged("ImageViewVisibility");
			}
		}

		public bool DoseViewVisibility
		{
			get
			{
				return doseViewVisibility;
			}
			set
			{
				doseViewVisibility = value;
				NotifyPropertyChanged("DoseViewVisibility");
			}
		}

		public bool SplitterVisibility
		{
			get
			{
				return splitterVisibility;
			}
			set
			{
				splitterVisibility = value;
				NotifyPropertyChanged("SplitterVisibility");
			}
		}

		public string CorrectionMode
		{
			get
			{
				return correctionMode;
			}
			set
			{
				correctionMode = value;
				NotifyPropertyChanged("CorrectionMode");
			}
		}

		public string DoseValue
		{
			get
			{
				return doseValue;
			}
			set
			{
				doseValue = value;
				NotifyPropertyChanged("DoseValue");
			}
		}

		public string AcquiedProgress
		{
			get
			{
				return acquiedProgress;
			}
			set
			{
				acquiedProgress = value;
				NotifyPropertyChanged("AcquiedProgress");
			}
		}

		public string GrayValueAlarm
		{
			get
			{
				return grayValueAlarm;
			}
			set
			{
				grayValueAlarm = value;
				NotifyPropertyChanged("GrayValueAlarm");
			}
		}

		public string ExposeBtnContent
		{
			get
			{
				return exposeBtnContent;
			}
			set
			{
				exposeBtnContent = value;
				NotifyPropertyChanged("ExposeBtnContent");
			}
		}

		public string SkipOrNextLabel
		{
			get
			{
				return skipOrNextLabel;
			}
			set
			{
				skipOrNextLabel = value;
				NotifyPropertyChanged("SkipOrNextLabel");
			}
		}

		public bool EnableBrowseBack
		{
			get
			{
				return enableBrowseBack;
			}
			set
			{
				enableBrowseBack = value;
				NotifyPropertyChanged("EnableBrowseBack");
			}
		}

		public bool EnableBrowseForward
		{
			get
			{
				return enableBrowseForward;
			}
			set
			{
				enableBrowseForward = value;
				NotifyPropertyChanged("EnableBrowseForward");
			}
		}

		public ImageSource SkipOrNextImage
		{
			get
			{
				return skipOrNextImage;
			}
			set
			{
				skipOrNextImage = value;
				NotifyPropertyChanged("SkipOrNextImage");
			}
		}

		public bool EnableImagelist
		{
			get
			{
				return enableImagelist;
			}
			set
			{
				enableImagelist = value;
				NotifyPropertyChanged("EnableImagelist");
			}
		}

		public string FlagForAutoTest
		{
			get
			{
				return flagForAutoTest;
			}
			set
			{
				flagForAutoTest = value;
				NotifyPropertyChanged("FlagForAutoTest");
			}
		}

		protected int AcquirdFrames
		{
			get
			{
				return GetAcquirdFrames();
			}
		}

		public CircularProgressBar ProgressTimer
		{
			get;
			set;
		}

		protected NavigationService NavigationService
		{
			get;
			set;
		}

		public event Action<IRayImageData> ShowImage;

		protected GenerateTemplatePageVM(DetectorInfo instance, CorrectionTemplateImageList imagelist)
		{
			DetInstance = instance;
			ImageListVM = imagelist;
			ImageListVM.SelectItemChanged += UpdateNextPrevBtnStatus;
			BrowseForward = new DelegateCommand(OnBrowseForward);
			BrowseBack = new DelegateCommand(OnBrowseBack);
			PrevFrame = new DelegateCommand(OnPrevFrame);
			StartAcq = new DelegateCommand(OnStartAcq);
			NextFrame = new DelegateCommand(OnNextFrame);
			EnableStartAcqBtn = false;
			EnableEnterNextPage(true);
			DoseViewVisibility = true;
			ExposeBtnContent = ExposeStr;
			BackLinkVisibilityInDoseMode = true;
			SkipOrNextLabel = Skip;
			SkipOrNextImage = new BitmapImage(new Uri("/Images/skip.png", UriKind.Relative));
			newImage = new IRayImageData();
			InitedStatus = false;
			_startAcqFlag = true;
		}

		private int GetAcquirdFrames()
		{
			if (AcquiredStatus == null)
			{
				return 0;
			}
			int num = 0;
			foreach (bool item in AcquiredStatus)
			{
				if (item)
				{
					num++;
				}
			}
			return num;
		}

		private void UpdateNextPrevBtnStatus(int selectIndex)
		{
			if (!IsInExposeView)
			{
				if (selectIndex == 0)
				{
					PrevBtnVisibility = false;
					NextBtnVisibility = true;
				}
				else if (GroupNumber == selectIndex + 1)
				{
					PrevBtnVisibility = true;
					NextBtnVisibility = false;
				}
				else
				{
					PrevBtnVisibility = true;
					NextBtnVisibility = true;
				}
			}
		}

		protected void InitForCreateTemplate(int cmdID, string message)
		{
			EnableEnterNextPage(true);
			DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
			{
				int num = DetInstance.Detector.Invoke(cmdID);
				if (num == 0)
				{
					InitedStatus = true;
					InitFinished();
				}
				else if (1 != num)
				{
					InitFailed(num);
				}
				else
				{
					ShowMessageBox("Initialize", message, WaitInitAckTimeOut);
				}
			}, new object[0]);
		}

		protected virtual void InitFinished()
		{
			InitedStatus = true;
			EnableLoadFileBtn = true;
			CloseMessageBox();
		}

		protected void InitFailed(int errorCode)
		{
			SkipOrNextLabel = Skip;
			InitedStatus = false;
			EnableLoadFileBtn = false;
			EnableStartAcqBtn = false;
			PrevBtnVisibility = false;
			NextBtnVisibility = false;
			EnterExposeView();
			CloseMessageBox();
			DetInstance.Shell.ShowMessage("Initialization failed! Err = " + ErrorHelper.GetErrorDesp(errorCode));
			DetInstance.Shell.ShowMessage("Please go Back and then restart the task!");
		}

		protected void OnShowImage()
		{
			if (this.ShowImage != null)
			{
				this.ShowImage(newImage);
			}
		}

		private void ClearImagelist()
		{
			AcquiredStatus.Clear();
			for (int i = 0; i < GroupNumber; i++)
			{
				AcquiredStatus.Add(false);
			}
			ImageListVM.SelectedIndex = 0;
			ImageListVM.ClearThumbs();
			UpdateProgress(0);
		}

		protected void InitImagelist(string[] kvArray)
		{
			GroupNumber = kvArray.Length;
			AcquiredStatus = new List<bool>();
			AcquiredStatus.Capacity = GroupNumber;
			ImageListVM.InitThumbList(kvArray, DetInstance.Detector.Prop_Attr_Prod_FullWidth * DetInstance.Detector.Prop_Attr_Prod_FullHeight * 2);
		}

		protected void DeInitImageList()
		{
			ImageListVM.DeInitThumbList();
		}

		protected virtual void ResumePrepStage()
		{
		}

		protected virtual void ExposeTimerEndCallback()
		{
			EnableEnterNextPage(true);
			if (!_startAcqFlag)
			{
				ResumePrepStage();
			}
			else
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("WaitImage"), WaitImageTimerTimeOut);
			}
		}

		protected void ShowMessageBox(string title, string content, Action timeOutHandler)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, TimeoutSeconds, DetInstance.Shell.ParentUI, timeOutHandler);
			MessageBoxTimer.RunTask();
		}

		protected void ShowMessageBox(string title, string content, bool canCancel, Action cancelWait)
		{
			MessageBoxTimer = new DlgTaskTimeOut(title, content, canCancel, DetInstance.Shell.ParentUI, cancelWait);
			MessageBoxTimer.RunTask();
		}

		private void WaitImageTimerTimeOut()
		{
			DetInstance.Shell.ShowMessage(StringResource.FindString("WaitImageTimeOut"));
			EnableStartAcqBtn = true;
		}

		private void WaitInitAckTimeOut()
		{
			DetInstance.Shell.ShowMessage(StringResource.FindString("InitTimeOut"));
			InitFailed(22);
			InitedStatus = true;
		}

		protected void CloseMessageBox()
		{
			if (MessageBoxTimer != null)
			{
				MessageBoxTimer.StopTask();
				MessageBoxTimer = null;
			}
		}

		protected void StopTimer()
		{
			EnableEnterNextPage(true);
			if (ProgressTimer != null)
			{
				ProgressTimer.StopTimer();
			}
		}

		protected virtual void PrepareForAcquire()
		{
			IsCanceled = false;
			EnableStartAcqBtn = true;
			ExposeBtnContent = ExposeStr;
			SkipOrNextLabel = Skip;
			ImageListVM.SelectedIndex = 0;
			UpdateProgress(0);
			EnterExposeView();
			ClearImagelist();
		}

		protected virtual void CancelCurrentTask()
		{
		}

		protected virtual void UpdateProgress(int acquiredFrames)
		{
		}

		protected virtual void EnterExposeView()
		{
		}

		protected virtual void EnterPreViewImageView()
		{
		}

		protected void EnableNextButton()
		{
			UpdateNextPrevBtnStatus(ImageListVM.SelectedIndex);
			FlagForAutoTest = "Ready";
		}

		private void OnNextFrame(object obj)
		{
			if (ImageListVM.SelectedIndex < GroupNumber - 1)
			{
				ImageListVM.SelectedIndex++;
			}
			EnterExposeView();
		}

		private void OnPrevFrame(object obj)
		{
			if (0 < ImageListVM.SelectedIndex)
			{
				ImageListVM.SelectedIndex--;
			}
			EnterExposeView();
		}

		private void OnStartAcq(object obj)
		{
			OnClickAcquireBtn();
		}

		protected virtual void OnClickAcquireBtn()
		{
		}

		protected virtual void StartExposeWindowTimerAfterStartAcquire()
		{
		}

		protected virtual void StartExposeWindowTimerAfterExpEnable(int nCallbackDelayTime)
		{
		}

		protected void StartExposeWindowTimer(int nCallbackDelayTime = -1)
		{
			EnableEnterNextPage(false);
			if (ProgressTimer != null)
			{
				ProgressTimer.StartTimer((nCallbackDelayTime < 0) ? GetExposeWindowLength() : ((double)nCallbackDelayTime / 1000.0), ExposeTimerEndCallback);
			}
		}

		protected void StopExposeWindowTimer()
		{
			if (ProgressTimer != null)
			{
				ProgressTimer.AbortTimer();
			}
		}

		protected void StopExposeWindowTimerAfterRecvImage(Detector sender)
		{
			if (sender.Prop_Attr_UROM_ExpMode != 2 && sender.Prop_Attr_UROM_ExpMode != 1 && ProgressTimer != null)
			{
				ProgressTimer.StopTimer();
			}
		}

		protected virtual double GetExposeWindowLength()
		{
			return 0.1;
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			if (1001 == nEventID || 1012 == nEventID)
			{
				IRayImage image = (IRayImage)Marshal.PtrToStructure(pParam, typeof(IRayImage));
				newImage.Write(ref image);
				if (newImage.ImgData == null)
				{
					return;
				}
			}
			DetInstance.Shell.ParentUI.Dispatcher.BeginInvoke((Action)delegate
			{
				switch (nEventID)
				{
				case 1002:
					StopExposeWindowTimerAfterRecvImage(sender);
					break;
				case 1001:
				case 1012:
					StopExposeWindowTimerAfterRecvImage(sender);
					CollectImage();
					break;
				case 4:
					if (IsCanceled)
					{
						if (IsBackBtnPressed)
						{
							EnterPrevCorrection();
						}
						else
						{
							EnterNextCorrection();
						}
					}
					else
					{
						ProcessSuceedEvent(nParam1);
					}
					break;
				case 5:
					if (IsCanceled)
					{
						EnableEnterNextPage(true);
						if (IsBackBtnPressed)
						{
							EnterPrevCorrection();
						}
						else
						{
							EnterNextCorrection();
						}
					}
					else
					{
						CloseMessageBox();
						ProcessFailedEvent(nParam1, nParam2);
					}
					break;
				case 6:
					if (IsCanceled)
					{
						if (IsBackBtnPressed)
						{
							EnterPrevCorrection();
						}
						else
						{
							EnterNextCorrection();
						}
					}
					EnableStartAcqBtn = true;
					break;
				case 1003:
				case 1006:
					CloseMessageBox();
					WaitImageTimeout();
					FlagForAutoTest = "Ready";
					EnableStartAcqBtn = true;
					EnableLoadFileBtn = true;
					if (nEventID == 1003)
					{
						ExposeBtnContent = ReExposeStr;
					}
					else if (nEventID == 1006)
					{
						ResumePrepStage();
						EnableEnterNextPage(true);
					}
					break;
				case 1005:
					StartExposeWindowTimerAfterExpEnable(nParam1);
					break;
				case 1008:
					StopExposeWindowTimer();
					break;
				case 17:
					DetInstance.Shell.ShowMessage(strMsg + " Error:" + ErrorHelper.GetErrorDesp(nParam2));
					EnableStartAcqBtn = false;
					EnableLoadFileBtn = false;
					break;
				case 7:
					if (1004 == nParam1)
					{
						EnableStartAcqBtn = false;
						StartExposeWindowTimer();
					}
					if (!IsInExposeView)
					{
						DisableAcquireInPreviewPage();
					}
					break;
				case 1011:
					if (!IsInExposeView)
					{
						EnableAcquireInPreviewPage();
					}
					StopTimer();
					break;
				}
			});
		}

		protected virtual void WaitImageTimeout()
		{
		}

		protected virtual void ProcessSuceedEvent(int cmdID)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFinished();
				break;
			case 3004:
			case 3009:
				SelectImageSucceed();
				break;
			case 1001:
				if (DetInstance.Detector.Prop_Attr_UROM_TriggerMode == 2)
				{
					EnableStartAcqBtn = true;
					ExposeBtnContent = StringResource.FindString("Acquire");
				}
				break;
			case 1004:
			case 1006:
				ExposeBtnContent = ExposeStr;
				if (!IsInExposeView)
				{
					EnableAcquireInPreviewPage();
				}
				break;
			}
		}

		protected virtual void ProcessFailedEvent(int cmdID, int errorCode)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFailed(errorCode);
				break;
			case 3004:
			case 3009:
				SelectImageFailed();
				EnableStartAcqBtn = true;
				break;
			case 1001:
				EnableStartAcqBtn = true;
				break;
			case 1004:
				ExposeBtnContent = ExposeStr;
				EnableStartAcqBtn = true;
				if (!IsInExposeView)
				{
					EnableAcquireInPreviewPage();
					StopTimer();
				}
				break;
			case 1012:
				if (IsInExposeView)
				{
					EnableEnterNextPage(true);
					EnableStartAcqBtn = true;
				}
				break;
			default:
				EnableStartAcqBtn = true;
				break;
			}
		}

		protected virtual void SelectImageSucceed()
		{
			GroupAcquireFinished();
			UpdateProgress(AcquirdFrames);
			if (GrayValueBeyondExpected)
			{
				ImageListVM.SelectedItem.SetImageValidility(ImageStatus.OK);
			}
			else
			{
				ImageListVM.SelectedItem.SetImageValidility(ImageStatus.QUESTION);
			}
		}

		protected virtual void SelectImageFailed()
		{
			EnableAcquire();
			ImageListVM.SelectedItem.SetImageValidility(ImageStatus.NO);
		}

		protected virtual void CollectImage()
		{
			if (this.ShowImage != null)
			{
				this.ShowImage(newImage);
			}
			AcquiredImage();
		}

		protected virtual void AcquiredImage()
		{
			int realValue = 0;
			int expectedValue = 0;
			GrayValueStatus grayValueStatus = IsRealGrayBeyondExpected(out realValue, out expectedValue);
			string arg;
			if (GrayValueStatus.Above == grayValueStatus)
			{
				WarningHintVisibility = true;
				arg = StringResource.FindString("GrayValueAboveAlarm");
			}
			else if (grayValueStatus == GrayValueStatus.Below)
			{
				WarningHintVisibility = true;
				arg = StringResource.FindString("GrayValueBelowAlarm");
			}
			else
			{
				WarningHintVisibility = false;
				arg = StringResource.FindString("GrayValueNormal");
			}
			GrayValueAlarm = string.Format("{0}: {1}/{2}", arg, realValue.ToString(), expectedValue.ToString());
		}

		protected void GroupAcquireFinished()
		{
			EnableAcquire();
			AcquiredStatus[ImageListVM.SelectedIndex] = true;
			if (IsAcquireCompleted())
			{
				GrayHintVisibility = false;
				AcquiedProgress = CompleteStr;
				SkipOrNextLabel = Next;
				SkipOrNextImage = new BitmapImage(new Uri("/Images/next.png", UriKind.Relative));
			}
		}

		protected void EnableAcquire()
		{
			EnableStartAcqBtn = true;
			EnableNextButton();
			EnableImagelist = true;
		}

		protected void DisableAcquireInPreviewPage()
		{
			EnableStartAcqBtn = false;
			EnableImagelist = false;
			PrevBtnVisibility = false;
			NextBtnVisibility = false;
		}

		protected void EnableAcquireInPreviewPage()
		{
			EnableAcquire();
		}

		protected void EnableEnterNextPage(bool enable)
		{
			EnableBrowseBack = enable;
			EnableBrowseForward = enable;
		}

		protected int GetCenterAverageValue(IRayImageData image)
		{
			int result = 0;
			IRayVariantMapItem[] @params = image.Params;
			for (int i = 0; i < @params.Length; i++)
			{
				IRayVariantMapItem rayVariantMapItem = @params[i];
				if (rayVariantMapItem.nMapKey == 32788)
				{
					result = rayVariantMapItem.varMapVal.val.nVal;
					break;
				}
			}
			return result;
		}

		private void OnBrowseBack(object obj)
		{
			IsBackBtnPressed = true;
			if (AbortCurrentTask())
			{
				EnterPrevCorrection();
			}
		}

		private void OnBrowseForward(object obj)
		{
			IsBackBtnPressed = false;
			if (IsAcquireCompleted() || AbortCurrentTask())
			{
				EnterNextCorrection();
			}
		}

		private bool AbortCurrentTask()
		{
			if (!InitedStatus)
			{
				return true;
			}
			bool result = false;
			if (MessageBoxResult.No != DetInstance.Shell.ShowYesNoMessage(StringResource.FindString("AbortCreateTemplate"), StringResource.FindString("AbortTask")))
			{
				IsCanceled = true;
				int num = DetInstance.Detector.Abort();
				if (num == 0)
				{
					result = true;
				}
				else if (1 == num)
				{
					EnableEnterNextPage(false);
				}
				else
				{
					DetInstance.Shell.ShowMessage("Abort failed! error:" + ErrorHelper.GetErrorDesp(num));
				}
			}
			return result;
		}

		protected bool IsAcquireCompleted()
		{
			if (AcquirdFrames == GroupNumber)
			{
				return true;
			}
			return false;
		}

		protected GrayValueStatus IsRealGrayBeyondExpected(out int realValue, out int expectedValue)
		{
			realValue = GetCenterAverageValue(newImage);
			expectedValue = GetExpectedGray(ImageListVM.SelectedIndex);
			int num = realValue - expectedValue;
			int num2 = Math.Abs(num);
			GrayValueBeyondExpected = false;
			if (num2 * 10 < GetExpectedGray(ImageListVM.SelectedIndex))
			{
				GrayValueBeyondExpected = true;
				return GrayValueStatus.Expected;
			}
			if (num > 0)
			{
				return GrayValueStatus.Above;
			}
			return GrayValueStatus.Below;
		}

		protected virtual int GetExpectedGray(int frameIndex)
		{
			return 0;
		}

		protected void SetDoseInfo(string doseInfo)
		{
			DoseValue = doseInfo;
		}

		protected virtual void EnterNextCorrection()
		{
			throw new NotImplementedException();
		}

		protected virtual void EnterPrevCorrection()
		{
			throw new NotImplementedException();
		}

		protected virtual void SetDoseInfo(int frameIndex)
		{
			throw new NotImplementedException();
		}

		protected virtual int SelectImage(int frameIndex)
		{
			throw new NotImplementedException();
		}

		public void AddCallbackHandler()
		{
			DetInstance.Detector.SdkCallbackEvent += OnSdkCallback;
		}

		public void RemoveCallbackHandler()
		{
			if (ProgressTimer != null)
			{
				ProgressTimer.StopTimer();
			}
			IsCanceled = false;
			IsBackBtnPressed = false;
			DetInstance.Detector.SdkCallbackEvent -= OnSdkCallback;
			CancelCurrentTask();
			DeInitImageList();
		}
	}
}
