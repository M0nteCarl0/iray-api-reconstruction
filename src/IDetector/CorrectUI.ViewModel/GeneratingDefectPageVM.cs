using iDetector;
using System;

namespace CorrectUI.ViewModel
{
	internal class GeneratingDefectPageVM : GeneratingTemplatePageVM
	{
		private DetectorInfo DetInstance;

		private readonly string GeneratingInfo = StringResource.FindString("GeneratingDefect");

		private readonly string GeneratSucceed = StringResource.FindString("GenerateDefectSuceed");

		private readonly string GeneratFailed = StringResource.FindString("GenerateDefectFailed");

		public GeneratingDefectPageVM(DetectorInfo detInstance, DoseAlgorithm doseAlgorithm)
			: base(detInstance, doseAlgorithm)
		{
			DetInstance = detInstance;
			base.Caption = GeneratingInfo;
		}

		protected override void GenerateTempalteSucceed()
		{
			base.Caption = GeneratSucceed;
			base.GenerateTempalteSucceed();
		}

		protected override void GenerateTempalteFailed(int errorCode)
		{
			base.Caption = GeneratFailed;
			base.GenerateTempalteFailed(errorCode);
		}

		public void StartCreateFile()
		{
			int nRetCode = DetInstance.Detector.Invoke(3010);
			DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
			{
				if (nRetCode == 0)
				{
					GenerateTempalteSucceed();
				}
				else if (1 != nRetCode)
				{
					GenerateTempalteFailed(nRetCode);
					if (TryGenerateAgain())
					{
						StartCreateFile();
					}
				}
			}, new object[0]);
		}
	}
}
