using System;
using System.Windows.Threading;

namespace CorrectUI.ViewModel
{
	public class GenerateStaticTemplate : GenerateTemplatePageVM
	{
		private bool Prepared;

		private bool bPostEnable;

		public GenerateStaticTemplate(DetectorInfo instance, CorrectionImageListVM imagelist)
			: base(instance, imagelist)
		{
			Prepared = false;
		}

		protected override void StartExposeWindowTimerAfterStartAcquire()
		{
			if (DetInstance.Detector != null)
			{
				switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
				{
				}
			}
		}

		protected override void StartExposeWindowTimerAfterExpEnable(int nCallbackDelayTime)
		{
			if (DetInstance.Detector == null)
			{
				return;
			}
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 4:
				break;
			case 0:
			case 3:
				if (52 == DetInstance.Detector.Prop_Attr_UROM_ProductNo || 51 == DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
					StartExposeWindowTimer(nCallbackDelayTime);
				}
				break;
			case 2:
				Prepared = true;
				if (!bPostEnable)
				{
					StartExposeWindowTimer(nCallbackDelayTime);
				}
				break;
			case 1:
			case 5:
				StartExposeWindowTimer(nCallbackDelayTime);
				break;
			}
		}

		protected override double GetExposeWindowLength()
		{
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 0:
			case 3:
				return 10.0;
			case 1:
			case 2:
				return (double)DetInstance.Detector.Prop_Attr_UROM_ExpWindowTime / 1000.0;
			case 5:
				return (double)DetInstance.Detector.Prop_Cfg_ClearAcqParam_DelayTime / 1000.0;
			default:
				return 0.0;
			}
		}

		protected override void EnterExposeView()
		{
			if (AcquiredStatus != null && AcquiredStatus.Count > ImageListVM.SelectedIndex)
			{
				AcquiredStatus[ImageListVM.SelectedIndex] = false;
				ImageListVM.ClearThumb(ImageListVM.SelectedIndex);
				base.SkipOrNextLabel = Skip;
			}
			Prepared = false;
			IsInExposeView = true;
			base.ExposeBtnContent = ExposeStr;
			base.PrevBtnVisibility = false;
			base.NextBtnVisibility = false;
			base.GrayHintVisibility = false;
			base.DoseViewVisibility = true;
			base.ImageViewVisibility = false;
			base.BackLinkVisibilityInDoseMode = true;
			base.BackLinkVisibilityInAcquireMode = false;
			base.SplitterVisibility = true;
			base.EnableImagelist = false;
			SetDoseInfo(ImageListVM.SelectedIndex);
			switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
			{
			case 1:
			case 4:
				break;
			case 0:
			case 3:
				if (45 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 41 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 61 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 52 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 51 != DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
					base.EnableStartAcqBtn = false;
				}
				break;
			case 2:
				base.EnableStartAcqBtn = true;
				break;
			case 5:
				if (this is GenerateGainPageVM)
				{
					base.EnableStartAcqBtn = false;
				}
				else if (this is GenerateDefectPageVM && 3 == DetInstance.Detector.Prop_Attr_UROM_FreesyncSubFlow)
				{
					base.EnableStartAcqBtn = false;
				}
				break;
			}
		}

		protected override void EnterPreViewImageView()
		{
			CloseMessageBox();
			IsInExposeView = false;
			base.ExposeBtnContent = ReExposeStr;
			base.DoseViewVisibility = false;
			base.ImageViewVisibility = true;
			base.BackLinkVisibilityInDoseMode = false;
			base.BackLinkVisibilityInAcquireMode = true;
			base.SplitterVisibility = false;
			base.EnableImagelist = true;
		}

		protected override void OnClickAcquireBtn()
		{
			if (IsInExposeView)
			{
				int num = 0;
				switch (DetInstance.Detector.Prop_Attr_UROM_TriggerMode)
				{
				case 0:
				case 3:
					switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
					{
					case 45:
						num = DetInstance.InvokeProxy.Invoke(1012);
						break;
					case 41:
					case 61:
						num = DetInstance.InvokeProxy.Invoke(3030);
						break;
					case 51:
					case 52:
						num = DetInstance.InvokeProxy.Invoke(1003);
						break;
					}
					break;
				case 1:
					num = ((52 != DetInstance.Detector.Prop_Attr_UROM_ProductNo && 51 != DetInstance.Detector.Prop_Attr_UROM_ProductNo) ? DetInstance.InvokeProxy.Invoke(1001) : ((DetInstance.Detector.Prop_Attr_UROM_InnerSubFlow != 0) ? DetInstance.InvokeProxy.Invoke(1002) : DetInstance.InvokeProxy.Invoke(1004)));
					break;
				case 2:
					bPostEnable = Prepared;
					if (Prepared)
					{
						_startAcqFlag = true;
						num = DetInstance.InvokeProxy.Invoke(1004);
					}
					else
					{
						_startAcqFlag = false;
						num = DetInstance.InvokeProxy.Invoke(1001);
					}
					break;
				case 5:
					num = DetInstance.InvokeProxy.Invoke(1002);
					break;
				default:
					num = DetInstance.InvokeProxy.Invoke(1004);
					break;
				}
				if (1 == num || num == 0)
				{
					StartExposeWindowTimerAfterStartAcquire();
					base.EnableStartAcqBtn = false;
				}
				if (1 == num)
				{
					EnableEnterNextPage(false);
				}
				if (bPostEnable)
				{
					StopExposeWindowTimer();
				}
			}
			else
			{
				EnterExposeView();
			}
		}

		protected override void ResumePrepStage()
		{
			Prepared = false;
			base.ExposeBtnContent = ExposeStr;
		}

		protected override void CollectImage()
		{
			OnShowImage();
			ImageListVM.UpdateImage(ImageListVM.SelectedIndex, newImage);
			AcquiredImage();
		}

		protected override void AcquiredImage()
		{
			StopTimer();
			EnterPreViewImageView();
			ImageListVM.UpdateImage(ImageListVM.SelectedIndex, newImage);
			base.AcquiredImage();
			Dispatcher dispatcher = DetInstance.Shell.ParentUI.Dispatcher;
			Action method = delegate
			{
				int num = SelectImage(ImageListVM.SelectedIndex);
				if (num == 0)
				{
					SelectImageSucceed();
				}
				else if (1 != num)
				{
					SelectImageFailed();
				}
			};
			dispatcher.Invoke(method, new object[0]);
			base.FlagForAutoTest = "Ready";
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			if (IsAcquireCompleted())
			{
				base.AcquiedProgress = CompleteStr;
			}
			else
			{
				base.AcquiedProgress = acquiredFrames.ToString() + "/" + GroupNumber.ToString();
			}
		}
	}
}
