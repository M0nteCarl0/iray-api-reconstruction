using iDetector;
using System;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class CorrectionPreparePageVM : NotifyObject
	{
		private DetectorInfo Instance;

		private ImageSource exposeTableImg;

		public DelegateCommand StartCorrection
		{
			get;
			set;
		}

		public ObservableCollection<object> ShootConditions
		{
			get;
			set;
		}

		public NavigationService NavigationService
		{
			get;
			set;
		}

		public ImageSource ExposeTableImg
		{
			get
			{
				return exposeTableImg;
			}
			set
			{
				exposeTableImg = value;
				NotifyPropertyChanged("ExposeTableImg");
			}
		}

		public CorrectionPreparePageVM(DetectorInfo instance)
		{
			StartCorrection = new DelegateCommand(OnStartCorrection);
			Instance = instance;
			if (!LoadDemoExposeGraphic())
			{
				ExposeTableImg = new BitmapImage(new Uri("/Images/ExposeTable.jpg", UriKind.Relative));
			}
			LoadShootConditions();
		}

		public bool LoadDemoExposeGraphic()
		{
			if (Instance.Detector != null && Utility.IsFileExisted(Instance.Detector.Prop_Attr_WorkDir + "\\Others\\ExposeTable.jpg"))
			{
				ExposeTableImg = new BitmapImage(new Uri(Instance.Detector.Prop_Attr_WorkDir + "\\Others\\ExposeTable.jpg", UriKind.Absolute)).Clone();
				return true;
			}
			return false;
		}

		private void OnStartCorrection(object obj)
		{
		}

		private void LoadShootConditions()
		{
			string lpFileName = Instance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			ShootConditions = new ObservableCollection<object>();
			int num = 1;
			if (IniParser.IsSectionExisted("CreateTemplateCondition", lpFileName))
			{
				while (true)
				{
					string privateProfileString = IniParser.GetPrivateProfileString("CreateTemplateCondition", string.Format("condition{0}", num), "", lpFileName);
					if (string.IsNullOrEmpty(privateProfileString))
					{
						break;
					}
					ShootConditions.Add(new ConditionText(string.Format("{0}.{1}", num, privateProfileString)));
					num++;
				}
			}
			else
			{
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition1")));
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition2")));
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition3")));
				ShootConditions.Add(new ConditionText(StringResource.FindString("Condition4")));
			}
		}
	}
}
