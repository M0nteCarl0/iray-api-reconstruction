using System.Windows;
using System.Windows.Controls;

namespace CorrectUI.ViewModel
{
	public class DoseItem : CheckBox
	{
		public DoseItem(string content)
		{
			base.IsEnabled = false;
			base.Margin = new Thickness(5.0, 5.0, 5.0, 5.0);
			base.Content = content;
		}
	}
}
