using iDetector;
using System;
using System.Windows;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	public class GeneratingTemplatePageVM : NotifyObject
	{
		private DetectorInfo Instance;

		private string caption;

		private string content;

		private bool enableBrowseBack;

		private bool enableBrowseForward;

		private bool startProgressBarRoll;

		private int generateProgressValue;

		private DoseAlgorithm DoseAlgorithm;

		public DelegateCommand BrowseForward
		{
			get;
			set;
		}

		public DelegateCommand BrowseBack
		{
			get;
			set;
		}

		public NavigationService NavigationService
		{
			get;
			set;
		}

		public string Caption
		{
			get
			{
				return caption;
			}
			set
			{
				caption = value;
				NotifyPropertyChanged("Caption");
			}
		}

		public string Content
		{
			get
			{
				return content;
			}
			set
			{
				content = value;
				NotifyPropertyChanged("Content");
			}
		}

		public bool EnableBrowseBack
		{
			get
			{
				return enableBrowseBack;
			}
			set
			{
				enableBrowseBack = value;
				NotifyPropertyChanged("EnableBrowseBack");
			}
		}

		public bool EnableBrowseForward
		{
			get
			{
				return enableBrowseForward;
			}
			set
			{
				enableBrowseForward = value;
				NotifyPropertyChanged("EnableBrowseForward");
			}
		}

		public bool StartProgressBarRoll
		{
			get
			{
				return startProgressBarRoll;
			}
			set
			{
				startProgressBarRoll = value;
				NotifyPropertyChanged("StartProgressBarRoll");
			}
		}

		public int GenerateProgressValue
		{
			get
			{
				return generateProgressValue;
			}
			set
			{
				generateProgressValue = value;
				NotifyPropertyChanged("GenerateProgressValue");
			}
		}

		public GeneratingTemplatePageVM(DetectorInfo instance, DoseAlgorithm doseAlgorithm)
		{
			Instance = instance;
			DoseAlgorithm = doseAlgorithm;
			BrowseForward = new DelegateCommand(OnBrowseForward);
			BrowseBack = new DelegateCommand(OnBrowseBack);
			GenerateProgressValue = 0;
			Content = "GoBack";
			EnableHyperlink(false);
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			Instance.Shell.ParentUI.Dispatcher.BeginInvoke((Action)delegate
			{
				switch (nEventID)
				{
				case 4:
					switch (nParam1)
					{
					case 3006:
					case 3010:
						GenerateTempalteSucceed();
						break;
					case 3015:
						EnableHyperlink(true);
						GenerateProgressValue = 100;
						break;
					}
					break;
				case 5:
					switch (nParam1)
					{
					case 3006:
					case 3010:
						GenerateTempalteFailed(nParam2);
						break;
					case 3015:
						EnableHyperlink(true);
						break;
					}
					break;
				}
			});
		}

		private void EnableHyperlink(bool enable)
		{
			EnableBrowseBack = enable;
			EnableBrowseForward = enable;
			StartProgressBarRoll = !enable;
		}

		protected virtual void GenerateTempalteSucceed()
		{
			int num = Instance.Detector.Invoke(3015);
			if (1 != num)
			{
				EnableHyperlink(true);
				GenerateProgressValue = 100;
			}
		}

		protected virtual void GenerateTempalteFailed(int errorCode)
		{
			Instance.Shell.ShowMessage("Create template failed! Err = " + ErrorHelper.GetErrorDesp(errorCode));
			EnableHyperlink(true);
		}

		protected bool TryGenerateAgain()
		{
			if (MessageBoxResult.No == Instance.Shell.ShowYesNoMessage(StringResource.FindString("GenerateAgain"), StringResource.FindString("Try")))
			{
				int num = Instance.Detector.Invoke(3015);
				if (1 != num)
				{
					EnableHyperlink(true);
				}
				return false;
			}
			return true;
		}

		private void OnBrowseBack(object obj)
		{
			if (NavigationService == null)
			{
				return;
			}
			if (this is GeneratingDynamicGainDefectPageVM)
			{
				NavigationService.Navigate(new CorrectionStudyPage(Instance));
			}
			else if (this is GeneratingGainPageVM)
			{
				NavigationService.Navigate(new CorrectionStudyPage(Instance));
			}
			else if (this is GeneratingDefectPageVM)
			{
				if (Instance.Detector.Prop_Attr_UROM_ProductNo == 58)
				{
					NavigationService.Navigate(new GenerateDynamicAcqPage(Instance, DoseAlgorithm, Enm_FileTypes.Enm_File_Gain));
				}
				else
				{
					NavigationService.Navigate(new GenerateGainPage(Instance, DoseAlgorithm));
				}
			}
		}

		private void OnBrowseForward(object obj)
		{
			if (NavigationService == null)
			{
				return;
			}
			if (this is GeneratingDynamicGainDefectPageVM)
			{
				NavigationService.Navigate(new GenerateTemplateEnd(Instance, DoseAlgorithm));
			}
			else if (this is GeneratingGainPageVM)
			{
				if (Instance.Detector.Prop_Attr_UROM_ProductNo == 58)
				{
					NavigationService.Navigate(new GenerateDynamicAcqPage(Instance, DoseAlgorithm, Enm_FileTypes.Enm_File_Defect));
				}
				else
				{
					NavigationService.Navigate(new GenerateDefectPage(Instance, DoseAlgorithm));
				}
			}
			else if (this is GeneratingDefectPageVM)
			{
				NavigationService.Navigate(new GenerateTemplateEnd(Instance, DoseAlgorithm));
			}
		}

		public void AddCallbackHandler()
		{
			Instance.Detector.SdkCallbackEvent += OnSdkCallback;
		}

		public void RemoveCallbackHandler()
		{
			Instance.Detector.SdkCallbackEvent -= OnSdkCallback;
		}
	}
}
