using iDetector;
using System;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class GenerateDynamicGainPageVM : GenerateDynamicTemplate
	{
		private DoseInfo GainDose;

		private string[] KVList;

		private DoseAlgorithm DoseAlgorithm;

		private int ExpectedGray = 1000;

		private uint DoseKV = 70u;

		public GenerateDynamicGainPageVM(DetectorInfo instance, CorrectionTemplateImageList imagelist, DoseAlgorithm doseAlgorithm)
			: base(instance, imagelist)
		{
			DoseAlgorithm = doseAlgorithm;
			base.CorrectionMode = StringResource.FindString("CreateGain");
			KVList = new string[Math.Max(DetInstance.Detector.Prop_Attr_GainTotalFrames, 1)];
			LoadKVList(KVList);
			GainDose = new DoseInfo(string.Format("{0}KV / {1}mAs", KVList[0], doseAlgorithm.GetDosemAs(Convert.ToUInt32(KVList[0]), ExpectedGray)));
			InitImagelist(KVList);
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist == null || DetInstance.Detector == null)
			{
				return;
			}
			string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
			for (int i = 0; i < kvlist.Length; i++)
			{
				string lpKeyName = "KV" + (i + 1).ToString();
				if (i == 0)
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, DoseKV.ToString(), lpFileName);
				}
				else
				{
					kvlist[i] = IniParser.GetPrivateProfileString("Gain", lpKeyName, kvlist[0], lpFileName);
				}
			}
			ExpectedGray = (int)IniParser.GetPrivateProfileInt("Gain", "Gray", ExpectedGray, lpFileName);
		}

		public override void StartInit(NavigationService navigationService)
		{
			base.NavigationService = navigationService;
			InitForCreateTemplate(3002, "Initialize to create gain and defect...");
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override int StartSelectImages()
		{
			return DetInstance.InvokeProxy.Invoke(3005, ImageListVM.SelectedIndex, KVList.Length);
		}

		protected override void EnterExposeView()
		{
			ImageListVM.SelectedIndex = 0;
			ImageListVM.ClearThumbs();
			for (int i = 0; i < AcquiredStatus.Count; i++)
			{
				AcquiredStatus[i] = false;
			}
			base.EnterExposeView();
		}

		protected override void AcquiredImage()
		{
			base.AcquiredImage();
			if (EnableBufferCine)
			{
				ImageListVM.UpdateImage(ImageListVM.SelectedIndex, newImage);
				SelectImageSucceed();
				base.EnableStartAcqBtn = false;
				if (ImageListVM.SelectedIndex < KVList.Length - 1)
				{
					ImageListVM.SelectedIndex++;
				}
				else
				{
					EnableBufferCine = false;
					EnterNextStage(Enm_CreateStage.SelectFinished);
					if (IsAcquireCompleted())
					{
						base.AcquiedProgress = CompleteStr;
					}
				}
			}
			base.PrevBtnVisibility = false;
			base.NextBtnVisibility = false;
		}

		protected override void EnterNextCorrection()
		{
			if (base.NavigationService != null)
			{
				if (Next == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GeneratingGainPage(DetInstance, DoseAlgorithm));
				}
				else if (Skip == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GenerateDynamicAcqPage(DetInstance, DoseAlgorithm, Enm_FileTypes.Enm_File_Defect));
				}
			}
		}

		protected override void UpdateValidLightImageStatus()
		{
			StopCheckFramesMonitor();
		}

		protected override void EnterPrevCorrection()
		{
			if (base.NavigationService != null)
			{
				base.NavigationService.Navigate(new CorrectionStudyPage(DetInstance));
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			SetDoseInfo(GainDose.GetDoseInfo());
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			return ExpectedGray;
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			base.AcquiedProgress = acquiredFrames.ToString() + "/" + KVList.Length;
		}
	}
}
