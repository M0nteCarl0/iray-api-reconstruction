using iDetector;
using MapFile;
using System.Collections.ObjectModel;

namespace CorrectUI.ViewModel
{
	public class CorrectionImageListVM : CorrectionTemplateImageList
	{
		private MapFileMgr fileMap;

		public CorrectionImageListVM()
		{
			base.ThumbList = new ObservableCollection<ImageListItem>();
		}

		public override void InitThumbList(string[] kvArray, int imageSize)
		{
			if (kvArray != null)
			{
				base.ThumbList.Clear();
				for (int i = 0; i < kvArray.Length; i++)
				{
					base.ThumbList.Add(new ImageListItem(i + 1, kvArray[i]));
				}
				if (fileMap == null)
				{
					fileMap = new MapFileMgr(kvArray.Length + 1, Enum_FileStoreMedium.HardDisk);
					fileMap.CreateFileMap(kvArray.Length, imageSize);
				}
				try
				{
					fileMap.OccupiedImageList(kvArray.Length);
				}
				catch
				{
				}
			}
		}

		public override void DeInitThumbList()
		{
			if (fileMap != null)
			{
				fileMap.DisposeMemoryMapFile();
				fileMap = null;
			}
		}

		public override void UpdateImage(int index, IRayImageData image)
		{
			if (fileMap != null && index >= 0 && index < base.ThumbList.Count)
			{
				base.ThumbList[index].UpdateImage(ref image);
				fileMap.InsertImageToFile(index, image.ImgData, image.nWidth, image.nHeight, image.Params);
			}
		}

		protected override void OnDisplayThumbImage()
		{
			if (fileMap != null)
			{
				DisplayThumbImage(fileMap.ReadImageFromFile(base.SelectedIndex));
			}
		}
	}
}
