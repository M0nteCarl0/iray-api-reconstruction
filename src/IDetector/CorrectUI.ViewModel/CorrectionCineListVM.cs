using iDetector;
using System.Collections.ObjectModel;

namespace CorrectUI.ViewModel
{
	public class CorrectionCineListVM : CorrectionTemplateImageList
	{
		public CorrectionCineListVM(string[] name, int width, int height)
		{
			base.ThumbList = new ObservableCollection<ImageListItem>();
		}

		public override void InitThumbList(string[] kvArray, int imageSize)
		{
			if (kvArray != null)
			{
				base.ThumbList.Clear();
				for (int i = 0; i < kvArray.Length; i++)
				{
					base.ThumbList.Add(new ImageListItem(i + 1, kvArray[i], true));
				}
			}
		}

		public override void UpdateImage(int index, IRayImageData image)
		{
			if (index >= 0 && index < base.ThumbList.Count && image != null)
			{
				base.ThumbList[index].UpdateImage(ref image);
			}
		}

		public override void GroupAcquireFinished(int groupNo, IRayImageData lastImage)
		{
			if (groupNo >= 0 && groupNo < base.ThumbList.Count)
			{
				base.ThumbList[groupNo].UpdateImage(ref lastImage);
			}
		}
	}
}
