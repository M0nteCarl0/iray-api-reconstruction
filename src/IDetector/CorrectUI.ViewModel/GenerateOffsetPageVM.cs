using iDetector;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class GenerateOffsetPageVM : NotifyObject
	{
		private string generatingInfo;

		private int generateProgressValue;

		private bool startProgressBarRoll;

		private int prepareTime;

		private bool prepareTimeVisibity;

		private bool enableBrowseBack;

		private bool enableBrowseForward;

		private string skipOrNextLabel;

		private ImageSource skipOrNextImage;

		private DetectorInfo Instance;

		private readonly string Next = StringResource.FindString("NextStep");

		private readonly string Skip = StringResource.FindString("Skip");

		private readonly string PrepareInfo = StringResource.FindString("PrepareToCrateOffset");

		private readonly string InitInfo = StringResource.FindString("CreatingOffset");

		private readonly string FinishedInfo = StringResource.FindString("GenerateOffsetSucceed");

		private readonly string FailedInfo = StringResource.FindString("GenerateOffsetFailed");

		private int TotalTime = 5;

		private UITimer timer;

		public bool FinishedFlag;

		public DelegateCommand BrowseForward
		{
			get;
			set;
		}

		public DelegateCommand BrowseBack
		{
			get;
			set;
		}

		public string GeneratingInfo
		{
			get
			{
				return generatingInfo;
			}
			set
			{
				generatingInfo = value;
				NotifyPropertyChanged("GeneratingInfo");
			}
		}

		public int GenerateProgressValue
		{
			get
			{
				return generateProgressValue;
			}
			set
			{
				generateProgressValue = value;
				NotifyPropertyChanged("GenerateProgressValue");
			}
		}

		public bool StartProgressBarRoll
		{
			get
			{
				return startProgressBarRoll;
			}
			set
			{
				startProgressBarRoll = value;
				NotifyPropertyChanged("StartProgressBarRoll");
			}
		}

		public int PrepareTime
		{
			get
			{
				return prepareTime;
			}
			set
			{
				prepareTime = value;
				NotifyPropertyChanged("PrepareTime");
			}
		}

		public bool PrepareTimeVisibity
		{
			get
			{
				return prepareTimeVisibity;
			}
			set
			{
				prepareTimeVisibity = value;
				NotifyPropertyChanged("PrepareTimeVisibity");
			}
		}

		public bool EnableBrowseBack
		{
			get
			{
				return enableBrowseBack;
			}
			set
			{
				enableBrowseBack = value;
				NotifyPropertyChanged("EnableBrowseBack");
			}
		}

		public bool EnableBrowseForward
		{
			get
			{
				return enableBrowseForward;
			}
			set
			{
				enableBrowseForward = value;
				NotifyPropertyChanged("EnableBrowseForward");
			}
		}

		public string SkipOrNextLabel
		{
			get
			{
				return skipOrNextLabel;
			}
			set
			{
				skipOrNextLabel = value;
				NotifyPropertyChanged("SkipOrNextLabel");
			}
		}

		public ImageSource SkipOrNextImage
		{
			get
			{
				return skipOrNextImage;
			}
			set
			{
				skipOrNextImage = value;
				NotifyPropertyChanged("SkipOrNextImage");
			}
		}

		private NavigationService NavigationService
		{
			get;
			set;
		}

		public GenerateOffsetPageVM(DetectorInfo instance)
		{
			BrowseForward = new DelegateCommand(OnEnterStudyPage);
			BrowseBack = new DelegateCommand(OnBrowseBack);
			Instance = instance;
			timer = new UITimer(1000, TimerTick);
			SkipOrNextLabel = Skip;
			SkipOrNextImage = new BitmapImage(new Uri("/Images/skip.png", UriKind.Relative));
			FinishedFlag = true;
		}

		private void TimerTick()
		{
			lock (this)
			{
				if (PrepareTime-- == 0)
				{
					timer.Stop();
					CompletePrepare();
				}
			}
		}

		public void StartGenerateOffset(NavigationService navigationService, bool bEnableSkip)
		{
			FinishedFlag = true;
			NavigationService = navigationService;
			Prepare(bEnableSkip);
			TotalTime = 5;
			PrepareTime = TotalTime;
			timer.Start();
		}

		private void Prepare(bool bEnableSkip)
		{
			PrepareTimeVisibity = true;
			GenerateProgressValue = 0;
			StartProgressBarRoll = true;
			EnableBrowseBack = true;
			EnableBrowseForward = bEnableSkip;
			GeneratingInfo = PrepareInfo;
		}

		private void CompletePrepare()
		{
			PrepareTimeVisibity = false;
			FinishedFlag = false;
			EnableBrowseBack = false;
			EnableBrowseForward = false;
			GeneratingInfo = InitInfo;
			StartCrate();
		}

		private void StartCrate()
		{
			if (Instance.InvokeProxy != null)
			{
				int num = Instance.InvokeProxy.Invoke(3001);
				if (num != 0 && 1 != num)
				{
					GenerateFailed(num);
				}
				else
				{
					StartProgressBarRoll = true;
				}
			}
		}

		private void GenerateSucceed()
		{
			FinishedFlag = true;
			SkipOrNextLabel = Next;
			SkipOrNextImage = new BitmapImage(new Uri("/Images/next.png", UriKind.Relative));
			GeneratingInfo = FinishedInfo;
			GenerateProgressValue = 100;
			StartProgressBarRoll = false;
			EnableBrowseBack = true;
			EnableBrowseForward = true;
		}

		private void GenerateFailed(int errorCode)
		{
			FinishedFlag = true;
			StartProgressBarRoll = false;
			GeneratingInfo = FailedInfo;
			if (7 == errorCode)
			{
				OnEnterStudyPage(null);
			}
			else if (MessageBoxResult.Yes == Instance.Shell.ShowYesNoMessage("Create offset template failed!\r\nDo you want to create it again?", "Create file"))
			{
				GeneratingInfo = InitInfo;
				StartProgressBarRoll = true;
				StartCrate();
			}
			else
			{
				EnableBrowseBack = true;
				EnableBrowseForward = true;
			}
		}

		private void OnBrowseBack(object obj)
		{
			timer.Stop();
			if (NavigationService != null)
			{
				NavigationService.Navigate(new CorrectionPreparePage());
			}
		}

		private void OnEnterStudyPage(object obj)
		{
			timer.Stop();
			if (NavigationService != null)
			{
				NavigationService.Navigate(new CorrectionStudyPage(Instance));
			}
		}

		public void AddCallbackHandler()
		{
			Instance.Detector.SdkCallbackEvent += OnSdkCallback;
		}

		public void RemoveCallbackHandler()
		{
			timer.Stop();
			Instance.Detector.SdkCallbackEvent -= OnSdkCallback;
		}

		private void OnSdkCallback(Detector sender, int nEventID, int nEventLevel, string strMsg, int nParam1, int nParam2, int nPtrParamLen, IntPtr pParam)
		{
			Instance.Shell.ParentUI.Dispatcher.BeginInvoke((Action)delegate
			{
				switch (nEventID)
				{
				case 4:
					if (nParam1 == 3001)
					{
						GenerateSucceed();
					}
					break;
				case 5:
					if (nParam1 == 3001)
					{
						GenerateFailed(nParam2);
					}
					break;
				}
			});
		}
	}
}
