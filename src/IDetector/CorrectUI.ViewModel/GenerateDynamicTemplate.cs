using iDetector;
using System;
using System.Windows.Forms;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	public class GenerateDynamicTemplate : GenerateTemplatePageVM
	{
		protected enum Enm_CreateStage
		{
			PrepExpose = 1,
			Acquiring,
			Selecting,
			SelectFinished,
			Finished
		}

		private const int MonitorTime = 40;

		public const int GroupNumbers = 3;

		protected readonly string SelectImageForGain = StringResource.FindString("SelectImageForGain");

		protected Enm_CreateStage CurStage;

		private IProgress ValidImageMonitor;

		private int OneGroupDefectImageNumber;

		private bool IsAcquireLightImage;

		private string LocalFilePath;

		private SyncLoadLocalDefectTemporaryFile LoadLocalFile;

		protected int StartValidDftFrameNumber;

		protected bool EnableBufferCine;

		private int ValidCount;

		public event Action ClosePlaybar;

		public event Action<int> StartStoreImage;

		public event Action<bool> StopStoreImage;

		public GenerateDynamicTemplate(DetectorInfo instance, CorrectionTemplateImageList imagelist)
			: base(instance, imagelist)
		{
			EnterPrepExposeStage(true);
			base.EnableLoadFileBtn = true;
			base.LoadImageFromFile = new DelegateCommand(OnLoadImageFromFile);
		}

		protected override void InitFinished()
		{
			OneGroupDefectImageNumber = DetInstance.Detector.Prop_Attr_DefectTotalFrames / 3;
			PrepareForAcquire();
			base.InitFinished();
		}

		private void OnLoadImageFromFile(object obj)
		{
			FolderBrowserDialogEx folderBrowserDialogEx = new FolderBrowserDialogEx();
			folderBrowserDialogEx.SelectedPath = LocalFilePath;
			folderBrowserDialogEx.Description = "Select a location to load the image files.";
			folderBrowserDialogEx.ShowNewFolderButton = true;
			folderBrowserDialogEx.ShowFullPathInEditBox = true;
			if (DialogResult.OK != folderBrowserDialogEx.ShowDialog())
			{
				return;
			}
			if (folderBrowserDialogEx.SelectedPath == string.Empty)
			{
				DetInstance.Shell.ShowMessage("Path cannot be empty!");
				return;
			}
			if (DetInstance.Detector == null)
			{
				DetInstance.Shell.ShowMessage("Detector instance is null!");
				return;
			}
			base.AcquiedProgress = "";
			for (int i = 0; i < AcquiredStatus.Count; i++)
			{
				AcquiredStatus[i] = false;
			}
			LocalFilePath = folderBrowserDialogEx.SelectedPath;
			int groupNumbers = 0;
			int eachGroupDefectImageNumber = 0;
			GetGroupInfo(out groupNumbers, out eachGroupDefectImageNumber);
			LoadLocalFile = new SyncLoadLocalDefectTemporaryFile(DetInstance, folderBrowserDialogEx.SelectedPath, groupNumbers, eachGroupDefectImageNumber, 500);
			LoadLocalFile.GroupLoadFinished += OnGroupLoadFinished;
			ImageListVM.SelectedIndex = 0;
			StartLoadLocalFile();
			EnableBufferCine = true;
			ValidCount = 0;
		}

		protected virtual void GetGroupInfo(out int groupNumbers, out int eachGroupDefectImageNumber)
		{
			groupNumbers = 3;
			eachGroupDefectImageNumber = OneGroupDefectImageNumber / 2;
		}

		private void OnAllLoadFinised(LoadLocalFileStatus status)
		{
			if (LoadLocalFileStatus.Ok == status)
			{
				StopStore(false);
				GroupAcquireFinished();
			}
			else
			{
				AcquiredStatus[ImageListVM.SelectedIndex] = false;
				StopStore(false);
			}
		}

		private void OnGroupLoadFinished(bool bAllFinished, int groupIndex, LoadLocalFileStatus status)
		{
			switch (status)
			{
			case LoadLocalFileStatus.Ok:
				StopStore();
				GroupAcquireFinished();
				break;
			case LoadLocalFileStatus.None:
				StopStore(false);
				break;
			case LoadLocalFileStatus.Incomplete:
				base.AcquiedProgress = "Local file is incomplete";
				AcquiredStatus[ImageListVM.SelectedIndex] = false;
				StopStore(false);
				break;
			}
			if (bAllFinished)
			{
				EnterSelectFinsihedStage(true);
				ImageListVM.SelectedIndex = 0;
				base.EnableImagelist = true;
			}
			else
			{
				ImageListVM.SelectedIndex = groupIndex + 1;
				StartStore();
			}
		}

		private void StartLoadLocalFile()
		{
			StartStore();
			LoadLocalFile.StartLoadLocalFile();
		}

		protected override void ExposeTimerEndCallback()
		{
			base.EnableBrowseBack = true;
			base.EnableBrowseForward = true;
			if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("WaitImage"), true, TaskCanceledHandler);
			}
			else
			{
				base.ExposeTimerEndCallback();
			}
		}

		private void TaskCanceledHandler()
		{
			EnterPrepExposeStage(true);
			int num = DetInstance.InvokeProxy.Invoke(1005);
			if (num == 1)
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("StopCurTask"), StopGenerateTemplate);
			}
		}

		private void StopGenerateTemplate()
		{
			base.EnableStartAcqBtn = false;
			base.EnableBrowseBack = true;
			base.EnableBrowseForward = true;
		}

		protected override void StartExposeWindowTimerAfterStartAcquire()
		{
			ExposeTimerEndCallback();
		}

		protected override double GetExposeWindowLength()
		{
			return 0.1;
		}

		protected override void EnterExposeView()
		{
			if (AcquiredStatus.Count > ImageListVM.SelectedIndex)
			{
				AcquiredStatus[ImageListVM.SelectedIndex] = false;
				ImageListVM.ClearThumb(ImageListVM.SelectedIndex);
				base.SkipOrNextLabel = Skip;
			}
			if (this.ClosePlaybar != null)
			{
				this.ClosePlaybar();
			}
			IsInExposeView = true;
			base.ExposeBtnContent = ExposeStr;
			EnterPrepExposeStage(true);
			UpdateProgress(0);
			base.PrevBtnVisibility = false;
			base.NextBtnVisibility = false;
			base.GrayHintVisibility = false;
			base.DoseViewVisibility = true;
			base.ImageViewVisibility = false;
			base.BackLinkVisibilityInDoseMode = true;
			base.BackLinkVisibilityInAcquireMode = false;
			base.SplitterVisibility = true;
			base.EnableImagelist = false;
			IsAcquireLightImage = true;
			SetDoseInfo(ImageListVM.SelectedIndex);
		}

		protected override void EnterPreViewImageView()
		{
			CloseMessageBox();
			IsInExposeView = false;
			base.PrevBtnVisibility = false;
			base.NextBtnVisibility = false;
			base.DoseViewVisibility = false;
			base.ImageViewVisibility = true;
			base.BackLinkVisibilityInDoseMode = false;
			base.BackLinkVisibilityInAcquireMode = true;
			base.SplitterVisibility = false;
		}

		protected void EnterNextStage(Enm_CreateStage nextStage)
		{
			switch (nextStage)
			{
			case Enm_CreateStage.PrepExpose:
				EnterPrepExposeStage();
				break;
			case Enm_CreateStage.Acquiring:
				if (EnterAcquingStage())
				{
					base.EnableLoadFileBtn = false;
					base.ExposeBtnContent = SelectImageForGain;
				}
				break;
			case Enm_CreateStage.Selecting:
				EnterSelectingStage();
				break;
			case Enm_CreateStage.SelectFinished:
				if (EnterSelectFinsihedStage())
				{
					base.ExposeBtnContent = ExposeStr;
					base.EnableStartAcqBtn = true;
				}
				break;
			}
		}

		private bool EnterPrepExposeStage(bool forceEnter = false)
		{
			bool result = false;
			if (Enm_CreateStage.SelectFinished == CurStage || forceEnter)
			{
				CurStage = Enm_CreateStage.PrepExpose;
				result = true;
			}
			return result;
		}

		private bool EnterAcquingStage()
		{
			bool result = false;
			if (Enm_CreateStage.PrepExpose == CurStage)
			{
				CurStage = Enm_CreateStage.Acquiring;
				result = true;
			}
			return result;
		}

		private bool EnterSelectingStage()
		{
			bool result = false;
			if (Enm_CreateStage.Acquiring == CurStage)
			{
				CurStage = Enm_CreateStage.Selecting;
				result = true;
			}
			return result;
		}

		protected bool EnterSelectFinsihedStage(bool forceEnter = false)
		{
			bool result = false;
			if (Enm_CreateStage.Selecting == CurStage || forceEnter)
			{
				if (IsInExposeView)
				{
					CurStage = Enm_CreateStage.PrepExpose;
				}
				else
				{
					CurStage = Enm_CreateStage.SelectFinished;
				}
				result = true;
			}
			return result;
		}

		protected override void OnClickAcquireBtn()
		{
			int num = 0;
			switch (CurStage)
			{
			case Enm_CreateStage.Selecting:
				break;
			case Enm_CreateStage.PrepExpose:
				num = DetInstance.InvokeProxy.Invoke(1004);
				if (1 == num || num == 0)
				{
					EnterNextStage(Enm_CreateStage.Acquiring);
					StartExposeWindowTimerAfterStartAcquire();
				}
				break;
			case Enm_CreateStage.Acquiring:
				if (StartSelectImages() == 0)
				{
					AcquireLightImages();
					EnterNextStage(Enm_CreateStage.Selecting);
				}
				else
				{
					SelelctImageFailed();
				}
				break;
			case Enm_CreateStage.SelectFinished:
				EnterExposeView();
				break;
			}
		}

		protected virtual int StartSelectImages()
		{
			return DetInstance.InvokeProxy.Invoke(3005, ImageListVM.SelectedIndex, OneGroupDefectImageNumber);
		}

		private void ResetAcquireState()
		{
			if (IsInExposeView)
			{
				EnterPrepExposeStage(true);
			}
			else
			{
				EnterSelectFinsihedStage(true);
			}
			StopCheckFramesMonitor();
		}

		private void SelelctImageFailed()
		{
			base.FlagForAutoTest = "Ready";
			base.EnableStartAcqBtn = true;
			base.EnableLoadFileBtn = true;
			base.EnableImagelist = true;
			base.ExposeBtnContent = ReExposeStr;
			EnterSelectFinsihedStage(true);
			ResetAcquireState();
		}

		protected override void WaitImageTimeout()
		{
			ResetAcquireState();
		}

		protected override void ProcessSuceedEvent(int cmdID)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFinished();
				break;
			case 1005:
				StopGenerateTemplate();
				CloseMessageBox();
				break;
			}
		}

		protected override void ProcessFailedEvent(int cmdID, int errorCode)
		{
			switch (cmdID)
			{
			case 3002:
			case 3007:
				InitFailed(errorCode);
				break;
			case 3005:
				SelelctImageFailed();
				break;
			case 1004:
			case 1010:
				SelelctImageFailed();
				break;
			case 1005:
				StopGenerateTemplate();
				CloseMessageBox();
				break;
			}
			StopCheckFramesMonitor();
		}

		protected override void CancelCurrentTask()
		{
			StopCheckFramesMonitor();
			if (this.ClosePlaybar != null)
			{
				this.ClosePlaybar();
			}
		}

		protected void StopCheckFramesMonitor()
		{
			if (ValidImageMonitor != null)
			{
				ValidImageMonitor.StopTask();
				ValidImageMonitor = null;
			}
		}

		private void AcquireLightImages()
		{
			StartValidDftFrameNumber = DetInstance.Detector.Prop_Attr_DefectValidFrames;
			StartStore();
			ValidCount = 0;
			EnableBufferCine = true;
			base.EnableStartAcqBtn = false;
			ValidImageMonitor = new MonitorTask(40, UpdateValidLightImageStatus);
			ValidImageMonitor.RunTask();
		}

		protected void AcquireDarkImages()
		{
			DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
			{
				if (!IsCanceled)
				{
					DetInstance.Shell.ShowMessage("Stop exposure and start to acquire dark images.");
				}
			}, new object[0]);
			if (DetInstance.Detector != null && !IsCanceled)
			{
				DetInstance.InvokeProxy.Invoke(1010, 0);
				ValidImageMonitor = new MonitorTask(40, UpdateValidDarkImageStatus);
				ValidImageMonitor.RunTask();
				IsAcquireLightImage = false;
			}
		}

		private void StartStore()
		{
			if (this.ClosePlaybar != null)
			{
				this.ClosePlaybar();
			}
			if (this.StartStoreImage != null)
			{
				this.StartStoreImage(ImageListVM.SelectedIndex);
			}
		}

		private void StopStore(bool updateCover = true)
		{
			if (this.StopStoreImage != null)
			{
				this.StopStoreImage(updateCover);
			}
		}

		public virtual void StartInit(NavigationService navigationService)
		{
			throw new NotImplementedException();
		}

		protected virtual void UpdateValidLightImageStatus()
		{
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			UpdateProgress(num);
			if (num >= OneGroupDefectImageNumber / 2 && OneGroupDefectImageNumber / 2 == num && ValidImageMonitor.IsTaskRunning)
			{
				ValidImageMonitor.StopTask();
				AcquireDarkImages();
				StopStore();
				EnableBufferCine = false;
			}
		}

		protected virtual void UpdateValidDarkImageStatus()
		{
			int num = DetInstance.Detector.Prop_Attr_DefectValidFrames - StartValidDftFrameNumber;
			UpdateProgress(num);
			if (num >= OneGroupDefectImageNumber && OneGroupDefectImageNumber == num)
			{
				ValidImageMonitor.StopTask();
				DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
				{
					base.EnableLoadFileBtn = true;
					GroupAcquireFinished();
				}, new object[0]);
				EnterNextStage(Enm_CreateStage.SelectFinished);
			}
		}

		protected override void AcquiredImage()
		{
			if (IsInExposeView)
			{
				EnterPreViewImageView();
			}
			if (EnableBufferCine)
			{
				ValidCount++;
			}
			if (IsAcquireLightImage)
			{
				base.AcquiredImage();
			}
		}

		protected override void UpdateProgress(int acquiredFrames)
		{
			base.AcquiedProgress = acquiredFrames.ToString() + "/" + OneGroupDefectImageNumber.ToString();
		}
	}
}
