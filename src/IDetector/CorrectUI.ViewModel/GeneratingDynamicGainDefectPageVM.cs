using iDetector;
using System;

namespace CorrectUI.ViewModel
{
	internal class GeneratingDynamicGainDefectPageVM : GeneratingTemplatePageVM
	{
		private DetectorInfo DetInstance;

		private readonly string GeneratingInfo = StringResource.FindString("GeneratingGainDefect");

		private readonly string GeneratSucceed = StringResource.FindString("GenerateGainDefectSuceed");

		private readonly string GeneratFailed = StringResource.FindString("GenerateGainDefectFailed");

		public GeneratingDynamicGainDefectPageVM(DetectorInfo detInstance, DoseAlgorithm doseAlgorithm)
			: base(detInstance, doseAlgorithm)
		{
			DetInstance = detInstance;
			base.Caption = GeneratingInfo;
			GeneratingInfo = base.Caption;
		}

		public void StartCreateFile()
		{
			int nRetCode = DetInstance.Detector.Invoke(3006);
			DetInstance.Shell.ParentUI.Dispatcher.Invoke((Action)delegate
			{
				if (nRetCode == 0)
				{
					GenerateTempalteSucceed();
				}
				else if (1 != nRetCode)
				{
					GenerateTempalteFailed(nRetCode);
					if (TryGenerateAgain())
					{
						StartCreateFile();
					}
				}
			}, new object[0]);
		}

		protected override void GenerateTempalteSucceed()
		{
			base.Caption = GeneratSucceed;
			base.GenerateTempalteSucceed();
		}

		protected override void GenerateTempalteFailed(int errorCode)
		{
			base.Caption = GeneratFailed;
			base.GenerateTempalteFailed(errorCode);
		}
	}
}
