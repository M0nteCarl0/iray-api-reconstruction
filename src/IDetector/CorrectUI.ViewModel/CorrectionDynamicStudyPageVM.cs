using iDetector;
using System;

namespace CorrectUI.ViewModel
{
	internal class CorrectionDynamicStudyPageVM : CorrectionStudyPageVM
	{
		private const int ValidTotalFrames = 10;

		private int IgnoreTotalFrames = 10;

		private int IgnoreFrames;

		private int ValidFrames;

		private int[] GrayArray;

		public CorrectionDynamicStudyPageVM(DetectorInfo instance)
			: base(instance)
		{
			GrayArray = new int[10];
			if (58 == instance.Detector.Prop_Attr_UROM_ProductNo)
			{
				IgnoreTotalFrames = 4;
				TimeoutSeconds = 40;
			}
		}

		protected override void InitDoseUint()
		{
			DoseUnit = "mA";
		}

		protected override void InitParams()
		{
			Array.Clear(GrayArray, 0, GrayArray.Length);
			ValidFrames = 0;
			IgnoreFrames = 0;
			base.PrevBtnVisibility = false;
			base.NextBtnVisibility = false;
		}

		protected override void StartExposeWindowTimerAfterStartAcquire()
		{
			ExposeTimerEndCallback();
		}

		protected override void ExposeTimerEndCallback()
		{
			if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("WaitImage"), true, TaskCanceledHandler);
			}
			else
			{
				base.ExposeTimerEndCallback();
			}
		}

		private void TaskCanceledHandler()
		{
			EnableEnterNextPage(true);
			int num = StopAcq();
			if (num == 1)
			{
				ShowMessageBox(StringResource.FindString("Wait"), StringResource.FindString("StopAcq"), WaitStopTimeOut);
			}
		}

		private void WaitStopTimeOut()
		{
			DetInstance.Shell.ShowMessage("StopAcq timeout !");
		}

		protected override void AcquiredImage(IRayImageData image)
		{
			if (IgnoreFrames == 0)
			{
				UpdateMessage("Acquiring images...");
			}
			if (IgnoreFrames < IgnoreTotalFrames)
			{
				base.EnableStartAcqBtn = false;
				IgnoreFrames++;
				return;
			}
			if (ValidFrames < 10)
			{
				GrayArray[ValidFrames] = GetCenterAverageValue(image);
				Log.Instance().Write("Study page index {0}:{1}", ValidFrames, GrayArray[ValidFrames]);
			}
			else if (ValidFrames >= 10)
			{
				return;
			}
			ValidFrames++;
			if (ValidFrames == 10)
			{
				CloseMessageBox();
				if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
				{
					DetInstance.Shell.ShowMessage("Acquire images finished, please Stop exposure!");
				}
				StopAcq();
			}
		}

		private int GetAverageGray(int[] grays)
		{
			if (grays.Length == 0)
			{
				return 0;
			}
			int num = 0;
			foreach (int num2 in grays)
			{
				num += num2;
			}
			return num / grays.Length;
		}

		private int StopAcq()
		{
			return DetInstance.InvokeProxy.Invoke(1005);
		}

		protected override void ProcessSuceedEvent(int cmdID)
		{
			if (ValidFrames == 10 && 1005 == cmdID)
			{
				GroupFinished(GetAverageGray(GrayArray));
			}
			base.ProcessSuceedEvent(cmdID);
		}

		protected override void ProcessFailedEvent(int cmdID, int errorCode)
		{
			if (ValidFrames == 10 && 1005 == cmdID)
			{
				GroupFinished(GetAverageGray(GrayArray));
			}
			base.ProcessFailedEvent(cmdID, errorCode);
		}

		private void GroupFinished(int grayValue)
		{
			base.EnableStartAcqBtn = true;
			EnableEnterNextPage(true);
			GroupAcquireFinised(grayValue);
		}
	}
}
