using iDetector;
using System.Windows.Navigation;

namespace CorrectUI.ViewModel
{
	internal class GenerateDynamicGainDefectPageVM : GenerateDynamicTemplate
	{
		private DoseInfo[] DefectDoseList;

		private string[] KVList;

		private int[] ExpectedGrayList;

		private DoseAlgorithm DoseAlgorithm;

		public GenerateDynamicGainDefectPageVM(DetectorInfo instance, CorrectionTemplateImageList imagelist, DoseAlgorithm doseAlgorithm)
			: base(instance, imagelist)
		{
			DoseAlgorithm = doseAlgorithm;
			base.CorrectionMode = StringResource.FindString("CreateGain+Defect");
			ExpectedGrayList = new int[3]
			{
				2000,
				12000,
				4000
			};
			KVList = new string[3];
			LoadKVList(KVList);
			DefectDoseList = new DoseInfo[3];
			for (int i = 0; i < 3; i++)
			{
				uint result = 0u;
				if (uint.TryParse(KVList[i], out result))
				{
					DefectDoseList[i] = new DoseInfo(string.Format("{0}kV / {1}mA", KVList[i], CalcDosemA(doseAlgorithm, result, ExpectedGrayList[i])));
				}
				else
				{
					DefectDoseList[i] = new DoseInfo(KVList[i]);
				}
			}
			InitImagelist(KVList);
		}

		private double CalcDosemA(DoseAlgorithm doseAlgorithm, uint _kv, int gray)
		{
			if (1 == DetInstance.Detector.Prop_Attr_UROM_FluroSync)
			{
				return doseAlgorithm.GetDosemAs(_kv, gray);
			}
			if (DetInstance.Detector.Prop_Attr_UROM_SyncExpTime != 0)
			{
				return doseAlgorithm.GetDosemAs(_kv, gray) * doseAlgorithm.TimeCoeff / (double)DetInstance.Detector.Prop_Attr_UROM_SyncExpTime;
			}
			return 0.0;
		}

		private void LoadKVList(string[] kvlist)
		{
			if (kvlist != null && DetInstance.Detector != null)
			{
				string lpFileName = DetInstance.Detector.Prop_Attr_WorkDir + "iDetectorConfig.ini";
				for (int i = 0; i < kvlist.Length; i++)
				{
					string lpKeyName = "KV" + (i + 1).ToString();
					kvlist[i] = IniParser.GetPrivateProfileString("GainDefect", lpKeyName, "", lpFileName);
					ExpectedGrayList[i] = (int)IniParser.GetPrivateProfileInt("GainDefect", string.Format("Gray{0}", i + 1), ExpectedGrayList[i], lpFileName);
				}
			}
		}

		public override void StartInit(NavigationService navigationService)
		{
			base.NavigationService = navigationService;
			InitForCreateTemplate(3002, "Initialize to create gain and defect...");
			SetDoseInfo(DefectDoseList[0].GetDoseInfo());
		}

		protected override void EnterNextCorrection()
		{
			if (base.NavigationService != null)
			{
				if (Next == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GeneratingDynamicGainDefectPage(DetInstance, DoseAlgorithm));
				}
				else if (Skip == base.SkipOrNextLabel)
				{
					base.NavigationService.Navigate(new GenerateTemplateEnd(DetInstance, DoseAlgorithm));
				}
			}
		}

		protected override void EnterPrevCorrection()
		{
			if (base.NavigationService != null)
			{
				base.NavigationService.Navigate(new CorrectionStudyPage(DetInstance));
			}
		}

		protected override void SetDoseInfo(int frameIndex)
		{
			if (frameIndex < DefectDoseList.Length)
			{
				SetDoseInfo(DefectDoseList[frameIndex].GetDoseInfo());
			}
		}

		protected override int GetExpectedGray(int frameIndex)
		{
			frameIndex = ((frameIndex < 3) ? frameIndex : 2);
			return ExpectedGrayList[frameIndex];
		}
	}
}
