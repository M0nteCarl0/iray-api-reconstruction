using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Windows;

[assembly: AssemblyProduct("XRay Detector Control Tool")]
[assembly: AssemblyTrademark("XRay")]
[assembly: AssemblyCompany("XRay")]
[assembly: AssemblyCopyright("Copyright (C) 2015, 2018, XRay")]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]
[assembly: AssemblyConfiguration("")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: AssemblyFileVersion("4.0.29.5086")]
[assembly: AssemblyTitle("iDetector")]
[assembly: AssemblyDescription("XRay Detector Control Software")]
[assembly: AssemblyVersion("4.0.29.5086")]
