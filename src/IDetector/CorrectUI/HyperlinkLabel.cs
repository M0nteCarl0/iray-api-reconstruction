using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace CorrectUI
{
	public class HyperlinkLabel : UserControl, IComponentConnector
	{
		public static readonly DependencyProperty ClickProperty = DependencyProperty.Register("Command", typeof(ICommand), typeof(HyperlinkLabel), new PropertyMetadata(null, OnPropertyChanged));

		public static readonly DependencyProperty LinkContentProperty = DependencyProperty.Register("LinkContent", typeof(string), typeof(HyperlinkLabel), new PropertyMetadata(OnContentPropertyChanged));

		internal Hyperlink xHyperlink;

		internal Label xForwardLabel;

		internal Image xIcon;

		internal Label xBackLabel;

		private bool _contentLoaded;

		public ImageSource Source
		{
			get
			{
				return xIcon.Source;
			}
			set
			{
				xIcon.Source = value;
			}
		}

		public ICommand Command
		{
			get
			{
				return (ICommand)GetValue(ClickProperty);
			}
			set
			{
				SetValue(ClickProperty, value);
			}
		}

		public string LinkContent
		{
			get
			{
				return (string)GetValue(LinkContentProperty);
			}
			set
			{
				SetValue(LinkContentProperty, value);
			}
		}

		private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			HyperlinkLabel hyperlinkLabel = d as HyperlinkLabel;
			ICommand command = (ICommand)hyperlinkLabel.GetValue(ClickProperty);
			if (hyperlinkLabel != null)
			{
				if (e.OldValue == null && e.NewValue != null)
				{
					hyperlinkLabel.PreviewMouseLeftButtonUp += OnMouseLeftUp;
				}
				else if (e.OldValue != null && e.NewValue == null)
				{
					hyperlinkLabel.PreviewMouseLeftButtonUp -= OnMouseLeftUp;
				}
			}
		}

		public static void OnMouseLeftUp(object sender, MouseButtonEventArgs e)
		{
			DependencyObject dependencyObject = sender as DependencyObject;
			ICommand command = (ICommand)dependencyObject.GetValue(ClickProperty);
			if (command != null && command.CanExecute(dependencyObject))
			{
				command.Execute(dependencyObject);
			}
		}

		private static void OnContentPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			HyperlinkLabel hyperlinkLabel = d as HyperlinkLabel;
			string labelContent = (string)hyperlinkLabel.GetValue(LinkContentProperty);
			hyperlinkLabel.SetLabelContent(labelContent);
		}

		protected virtual void SetLabelContent(string content)
		{
			throw new NotImplementedException();
		}

		public HyperlinkLabel()
		{
			InitializeComponent();
			xForwardLabel.Content = "hello";
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/hyperlink.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xHyperlink = (Hyperlink)target;
				break;
			case 2:
				xForwardLabel = (Label)target;
				break;
			case 3:
				xIcon = (Image)target;
				break;
			case 4:
				xBackLabel = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
