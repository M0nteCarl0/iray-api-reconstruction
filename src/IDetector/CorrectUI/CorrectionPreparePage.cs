using CorrectUI.ViewModel;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;

namespace CorrectUI
{
	public class CorrectionPreparePage : Page, IComponentConnector
	{
		private DetectorInfo DetectorInfo;

		private bool _contentLoaded;

		public CorrectionPreparePage()
		{
			InitializeComponent();
			base.IsVisibleChanged += VisibleChanged;
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if (e.NewValue.Equals(true))
			{
				CorrectionPreparePageVM correctionPreparePageVM = base.DataContext as CorrectionPreparePageVM;
				if (correctionPreparePageVM != null && DetectorInfo.Detector != null)
				{
					correctionPreparePageVM.LoadDemoExposeGraphic();
				}
			}
		}

		public void InitDetectorInfo(DetectorInfo instance)
		{
			DetectorInfo = instance;
			base.DataContext = new CorrectionPreparePageVM(instance);
		}

		private void OnStartCorrection(object sender, RoutedEventArgs args)
		{
			if (base.NavigationService != null && DetectorInfo.Detector != null)
			{
				if (11 == DetectorInfo.Detector.Prop_Attr_UROM_ProductNo)
				{
					base.NavigationService.Navigate(new CorrectionStudyPage(DetectorInfo));
				}
				else
				{
					base.NavigationService.Navigate(new GenerateOffsetPage(DetectorInfo));
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionpreparepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((Hyperlink)target).Click += OnStartCorrection;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
