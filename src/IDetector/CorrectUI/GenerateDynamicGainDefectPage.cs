using Cinema;
using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateDynamicGainDefectPage : Page, IComponentConnector
	{
		private GenerateDynamicGainDefectPageVM GenerateDynamicGainDefectPageVM;

		private CorrectionTemplateImageList ImageListVM;

		private CinePlayerCtrl mCinePlayCtrl;

		private Detector DetInstance;

		private string[] CineBufferName;

		private int GroupNumber = 3;

		private int GroupLightFrames;

		private ReceiveImageCtrl RecvImgProcessor;

		internal GenerateTemplatePage xDynamicGainDefectAcqPage;

		private bool _contentLoaded;

		public GenerateDynamicGainDefectPage(DetectorInfo instance, DoseAlgorithm doseAlgorithm)
		{
			InitializeComponent();
			DetInstance = instance.Detector;
			xDynamicGainDefectAcqPage.xLoadFile.Visibility = Visibility.Visible;
			CineBufferName = new string[GroupNumber];
			GroupLightFrames = DetInstance.Prop_Attr_DefectTotalFrames / GroupNumber / 2;
			string text = DetInstance.Prop_Attr_WorkDir + "temp\\";
			for (int i = 0; i < CineBufferName.Length; i++)
			{
				CineBufferName[i] = text + i.ToString() + "-" + GroupLightFrames.ToString() + "Cine.cin";
			}
			if (!Utility.IsDirectoryExisted(text))
			{
				Utility.CreateDirectory(text);
			}
			ImageListVM = new CorrectionCineListVM(CineBufferName, DetInstance.Prop_Attr_Width, DetInstance.Prop_Attr_Height);
			ImageListVM.DisplayThumb += ShowImage;
			xDynamicGainDefectAcqPage.xImageList.DataContext = ImageListVM;
			xDynamicGainDefectAcqPage.xImageList.MouseDoubleClick += ImageListMouseDoubleClick;
			GenerateDynamicGainDefectPageVM = new GenerateDynamicGainDefectPageVM(instance, ImageListVM, doseAlgorithm);
			GenerateDynamicGainDefectPageVM.ClosePlaybar += ClosePlaybar;
			GenerateDynamicGainDefectPageVM.StartStoreImage += StartStoreCineData;
			GenerateDynamicGainDefectPageVM.StopStoreImage += StopStoreCineData;
			base.DataContext = GenerateDynamicGainDefectPageVM;
			GenerateDynamicGainDefectPageVM.ProgressTimer = xDynamicGainDefectAcqPage.xProgressBar;
			GenerateDynamicGainDefectPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += OnLoad;
			RecvImgProcessor = new ReceiveImageCtrl();
			RecvImgProcessor.PanitImage += PaintImage;
			RecvImgProcessor.AdjustWinLevelAndWidth += AdjustWinLevelAndWinWidth;
			int num = Math.Min(DetInstance.Prop_Attr_Width, 640);
			int height = DetInstance.Prop_Attr_Height * num / DetInstance.Prop_Attr_Width;
			xDynamicGainDefectAcqPage.xImageView.SetDisplaySize(num, height);
		}

		private void ImageListMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			InitCinePlay();
			if (mCinePlayCtrl.Init(CineBufferName[ImageListVM.SelectedIndex], DetInstance.Prop_Attr_Width, DetInstance.Prop_Attr_Height))
			{
				xDynamicGainDefectAcqPage.xImageView.xPlayerBar.SetDataContext(mCinePlayCtrl);
				xDynamicGainDefectAcqPage.xImageView.xPlayerBar.Start();
				mCinePlayCtrl.HandleEvent(Enum_PlayerEvent.Player_StepForward);
			}
		}

		private void StartStoreCineData(int groupNo)
		{
			RecvImgProcessor.StartBufferCine(CineBufferName[ImageListVM.SelectedIndex], GroupLightFrames, DetInstance.Prop_Attr_Width, DetInstance.Prop_Attr_Height);
		}

		private void StopStoreCineData(bool updateCover = true)
		{
			RecvImgProcessor.StopBufferCine();
			if (updateCover)
			{
				base.Dispatcher.Invoke((Action)delegate
				{
					ImageListVM.UpdateImage(ImageListVM.SelectedIndex, RecvImgProcessor.LastImage);
				}, new object[0]);
			}
		}

		private void InitCinePlay()
		{
			ClosePlaybar();
			mCinePlayCtrl = new CinePlayerCtrl();
			mCinePlayCtrl.DisplayHandle += ShowImage;
		}

		private void ClosePlaybar()
		{
			if (mCinePlayCtrl != null)
			{
				xDynamicGainDefectAcqPage.xImageView.xPlayerBar.Close();
				xDynamicGainDefectAcqPage.xImageView.xPlayerBar.SetDataContext(null);
				mCinePlayCtrl.Close();
				mCinePlayCtrl = null;
			}
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GenerateDynamicGainDefectPageVM.StartInit(base.NavigationService);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GenerateDynamicGainDefectPageVM.AddCallbackHandler();
					xDynamicGainDefectAcqPage.xImageView.xImageView.StartPaintTimer();
					RecvImgProcessor.StartReceive();
				}
			}
			else
			{
				RecvImgProcessor.Close();
				xDynamicGainDefectAcqPage.xImageView.xImageView.StopPaintTimer();
				GenerateDynamicGainDefectPageVM.RemoveCallbackHandler();
				ReleaseResource();
			}
		}

		private void ShowImage(byte[] data, int width, int height)
		{
			ShowImage(new IRayImageData(data, width, height));
		}

		private void ShowImage(IRayImageData image)
		{
			RecvImgProcessor.ReceiveImage(image);
		}

		private void PaintImage(IRayImageData image)
		{
			xDynamicGainDefectAcqPage.xImageView.UpdateImage(image);
		}

		private void AdjustWinLevelAndWinWidth()
		{
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				ushort[] oriGray16ImgBuffer = xDynamicGainDefectAcqPage.xImageView.xImageView.OriGray16ImgBuffer;
				int winLevel = 0;
				int winWidth = 0;
				CalcWinLevelWinWidth.GetWinLevelAndWidth(oriGray16ImgBuffer, ref winLevel, ref winWidth, DetInstance.Prop_Attr_Width, DetInstance.Prop_Attr_Height);
				xDynamicGainDefectAcqPage.xImageView.xImageView.SetWindow(winWidth, winLevel);
			});
		}

		private void ReleaseResource()
		{
			string[] cineBufferName = CineBufferName;
			foreach (string path in cineBufferName)
			{
				if (File.Exists(path))
				{
					File.Delete(path);
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatedynamicgaindefectpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xDynamicGainDefectAcqPage = (GenerateTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
