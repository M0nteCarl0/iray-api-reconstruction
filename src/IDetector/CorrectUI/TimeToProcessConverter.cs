using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace CorrectUI
{
	public class TimeToProcessConverter : IValueConverter
	{
		private const double Thickness = 8.0;

		private const double Padding = 1.0;

		private const double WarnValue = 40.0;

		private const int SuccessRateFontSize = 34;

		private readonly SolidColorBrush NormalBrush;

		private readonly SolidColorBrush WarnBrush;

		private readonly Typeface SuccessRateTypeface;

		private Point centerPoint;

		private double radius;

		public TimeToProcessConverter()
		{
			NormalBrush = new SolidColorBrush(Colors.Green);
			WarnBrush = new SolidColorBrush(Colors.Yellow);
			SuccessRateTypeface = new Typeface(new FontFamily("MSYH"), default(FontStyle), default(FontWeight), default(FontStretch));
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is double && !string.IsNullOrEmpty((string)parameter))
			{
				double value2 = (double)value;
				double num = double.Parse((string)parameter);
				radius = num / 2.0;
				centerPoint = new Point(radius, radius);
				return DrawBrush(value2, 100.0, radius, radius, 8.0, 1.0);
			}
			throw new ArgumentException();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		private Point GetPointByAngel(Point CenterPoint, double r, double angel)
		{
			Point result = default(Point);
			result.X = Math.Sin(angel * Math.PI / 180.0) * r + CenterPoint.X;
			result.Y = CenterPoint.Y - Math.Cos(angel * Math.PI / 180.0) * r;
			return result;
		}

		private Geometry DrawingArcGeometry(Point bigFirstPoint, Point bigSecondPoint, Point smallFirstPoint, Point smallSecondPoint, double bigRadius, double smallRadius, bool isLargeArc)
		{
			PathFigure pathFigure = new PathFigure();
			pathFigure.IsClosed = true;
			PathFigure pathFigure2 = pathFigure;
			pathFigure2.StartPoint = bigFirstPoint;
			pathFigure2.Segments.Add(new ArcSegment
			{
				Point = bigSecondPoint,
				IsLargeArc = isLargeArc,
				Size = new Size(bigRadius, bigRadius),
				SweepDirection = SweepDirection.Clockwise
			});
			pathFigure2.Segments.Add(new LineSegment
			{
				Point = smallSecondPoint
			});
			pathFigure2.Segments.Add(new ArcSegment
			{
				Point = smallFirstPoint,
				IsLargeArc = isLargeArc,
				Size = new Size(smallRadius, smallRadius),
				SweepDirection = SweepDirection.Counterclockwise
			});
			PathGeometry pathGeometry = new PathGeometry();
			pathGeometry.Figures.Add(pathFigure2);
			return pathGeometry;
		}

		private Geometry GetGeometry(double value, double maxValue, double radiusX, double radiusY, double thickness, double padding)
		{
			bool isLargeArc = false;
			double num = value / maxValue;
			double num2 = num * 360.0;
			if (num2 > 180.0)
			{
				isLargeArc = true;
			}
			double num3 = radiusX - thickness + padding;
			Point pointByAngel = GetPointByAngel(centerPoint, radiusX, 0.0);
			Point pointByAngel2 = GetPointByAngel(centerPoint, radiusX, num2);
			Point pointByAngel3 = GetPointByAngel(centerPoint, num3, 0.0);
			Point pointByAngel4 = GetPointByAngel(centerPoint, num3, num2);
			return DrawingArcGeometry(pointByAngel, pointByAngel2, pointByAngel3, pointByAngel4, radiusX, num3, isLargeArc);
		}

		private void DrawingGeometry(DrawingContext drawingContext, double value, double maxValue, double radiusX, double radiusY, double thickness, double padding)
		{
			if (value != maxValue)
			{
				SolidColorBrush normalBrush = NormalBrush;
				drawingContext.DrawEllipse(null, new Pen(new SolidColorBrush(Color.FromRgb(221, 223, 225)), thickness), centerPoint, radiusX, radiusY);
				drawingContext.DrawGeometry(normalBrush, new Pen(), GetGeometry(value, maxValue, radiusX, radiusY, thickness, padding));
			}
			else
			{
				drawingContext.DrawEllipse(null, new Pen(NormalBrush, thickness), centerPoint, radiusX, radiusY);
			}
			drawingContext.Close();
		}

		private Brush DrawBrush(double value, double maxValue, double radiusX, double radiusY, double thickness, double padding)
		{
			DrawingGroup drawingGroup = new DrawingGroup();
			DrawingContext drawingContext = drawingGroup.Open();
			DrawingGeometry(drawingContext, value, maxValue, radiusX, radiusY, thickness, padding);
			return new DrawingBrush(drawingGroup);
		}
	}
}
