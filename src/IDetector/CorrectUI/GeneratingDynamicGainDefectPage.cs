using CorrectUI.ViewModel;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GeneratingDynamicGainDefectPage : Page, IComponentConnector
	{
		private GeneratingDynamicGainDefectPageVM GeneratingDynamicGainDefectPageVM;

		internal GeneratingTemplatePage xGeneratingDynamicGain;

		private bool _contentLoaded;

		public GeneratingDynamicGainDefectPage(DetectorInfo detInstance, DoseAlgorithm doseAlgorithm)
		{
			InitializeComponent();
			GeneratingDynamicGainDefectPageVM = new GeneratingDynamicGainDefectPageVM(detInstance, doseAlgorithm);
			base.DataContext = GeneratingDynamicGainDefectPageVM;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += OnLoad;
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GeneratingDynamicGainDefectPageVM.NavigationService = base.NavigationService;
				new Thread((ThreadStart)delegate
				{
					GeneratingDynamicGainDefectPageVM.StartCreateFile();
				}).Start();
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GeneratingDynamicGainDefectPageVM.AddCallbackHandler();
				}
			}
			else
			{
				GeneratingDynamicGainDefectPageVM.RemoveCallbackHandler();
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatingdynamicgaindefectpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xGeneratingDynamicGain = (GeneratingTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
