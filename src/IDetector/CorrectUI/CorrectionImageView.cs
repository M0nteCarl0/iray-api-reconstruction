using Cinema;
using iDetector;
using IMG;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace CorrectUI
{
	public class CorrectionImageView : UserControl, IComponentConnector
	{
		internal ImageProcessor xImageView;

		internal PlayerBar xPlayerBar;

		private bool _contentLoaded;

		public CorrectionImageView()
		{
			InitializeComponent();
			base.DataContext = xImageView;
			xPlayerBar.PlaySpeedVisibility = false;
			base.MouseLeftButtonDown += ShowPlayBar;
		}

		public void SetDisplaySize(int width, int height)
		{
			xImageView.SetDisplaySize(width & -4, height & -4);
		}

		public void OnClose()
		{
			xImageView.Deinit();
		}

		private void ShowPlayBar(object sender, MouseButtonEventArgs e)
		{
			xPlayerBar.Show();
		}

		public void DisplayImage(IRayImageData image)
		{
			if (image != null && image.ImgData != null && image.ImgData.Length != 0)
			{
				xImageView.ShowGray16To8Image(image.ImgData, (ushort)image.nWidth, (ushort)image.nHeight);
				int winLevel = 0;
				int winWidth = 0;
				CalcWinLevelWinWidth.GetWinLevelAndWidth(xImageView.OriGray16ImgBuffer, ref winLevel, ref winWidth, image.nWidth, image.nHeight);
				xImageView.SetWindowAndPaint(winWidth, winLevel);
			}
		}

		public void UpdateImage(IRayImageData image)
		{
			if (image != null && image.ImgData != null)
			{
				xImageView.ShowGray16To8Image(image.ImgData, (ushort)image.nWidth, (ushort)image.nHeight);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionimageview.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xImageView = (ImageProcessor)target;
				break;
			case 2:
				xPlayerBar = (PlayerBar)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
