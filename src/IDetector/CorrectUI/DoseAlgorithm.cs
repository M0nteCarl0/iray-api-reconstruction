using System;
using System.Collections.Generic;

namespace CorrectUI
{
	public class DoseAlgorithm
	{
		private Dictionary<uint, Coefficient> CoeffList;

		private double timeCoeff;

		public double TimeCoeff
		{
			get
			{
				return timeCoeff;
			}
		}

		public DoseAlgorithm()
		{
			CoeffList = new Dictionary<uint, Coefficient>();
		}

		public void AddCoeffItem(uint _kv, double _coeff, double _intercept)
		{
			if (!CoeffList.ContainsKey(_kv))
			{
				CoeffList.Add(_kv, new Coefficient(_coeff, _intercept, _kv));
			}
		}

		public void UpdateCoeff(uint _kv, double _coeff, double _intercept)
		{
			if (CoeffList.ContainsKey(_kv))
			{
				Coefficient coefficient = CoeffList[_kv];
				coefficient.Coeff = _coeff;
				coefficient.Intercept = _intercept;
			}
			else
			{
				CoeffList.Add(_kv, new Coefficient(_coeff, _intercept, _kv));
			}
		}

		public void UpdateTimeCoeff(double _timeCoeff)
		{
			timeCoeff = _timeCoeff;
		}

		public double GetDosemAs(uint _kv, int gray)
		{
			double result = 0.0;
			if (CoeffList.ContainsKey(_kv))
			{
				result = ((CoeffList[_kv].Coeff == 0.0) ? 0.0 : (((double)gray - CoeffList[_kv].Intercept) / CoeffList[_kv].Coeff));
				result = Math.Round(result, 1);
			}
			return result;
		}
	}
}
