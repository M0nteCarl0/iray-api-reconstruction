using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class CorrectionImageList : UserControl, IComponentConnector
	{
		internal ListBox xListBox;

		private bool _contentLoaded;

		public CorrectionImageList()
		{
			InitializeComponent();
		}

		private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			xListBox.ScrollIntoView(xListBox.SelectedItem);
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionimagelist.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xListBox = (ListBox)target;
				xListBox.SelectionChanged += ListBox_SelectionChanged;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
