using System.Windows;

namespace CorrectUI
{
	public class HyperlinkBack : HyperlinkLabel
	{
		public string Caption
		{
			get
			{
				return xBackLabel.Content.ToString();
			}
			set
			{
				xBackLabel.Content = value;
			}
		}

		public HyperlinkBack()
		{
			xForwardLabel.Visibility = Visibility.Collapsed;
		}

		protected override void SetLabelContent(string content)
		{
			Caption = content;
		}
	}
}
