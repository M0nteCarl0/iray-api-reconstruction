using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class CircularProgressBar : UserControl, INotifyPropertyChanged, IComponentConnector
	{
		private const int timerInterval = 100;

		private UITimer timer;

		private double TotalTime;

		private bool timerOverFlag;

		private int progressRate;

		private double leftTime;

		private bool _contentLoaded;

		public int ProgressRate
		{
			get
			{
				return progressRate;
			}
			set
			{
				if (value != progressRate)
				{
					progressRate = value;
					OnPropertyChanged("ProgressRate");
				}
			}
		}

		public double LeftTime
		{
			get
			{
				return leftTime;
			}
			set
			{
				if (value != leftTime)
				{
					leftTime = Math.Round(value, 1);
					OnPropertyChanged("LeftTime");
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		private event Action TimerEndCallback;

		public void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		public CircularProgressBar()
		{
			InitializeComponent();
			timerOverFlag = false;
			timer = new UITimer(100, timer_tick);
			LeftTime = TotalTime;
			base.DataContext = this;
		}

		private void timer_tick()
		{
			LeftTime -= 0.1;
			ProgressRate = Convert.ToInt32(leftTime * 100.0 / TotalTime);
			if (progressRate == 0)
			{
				timer.Stop();
				if (this.TimerEndCallback != null)
				{
					timerOverFlag = true;
					this.TimerEndCallback();
				}
			}
		}

		public void StartTimer(double totalSeconds, Action timerStop)
		{
			TotalTime = ((totalSeconds - 0.1 <= 0.0) ? 0.1 : totalSeconds);
			LeftTime = TotalTime;
			this.TimerEndCallback = timerStop;
			timerOverFlag = false;
			timer.Start();
		}

		public void StopTimer()
		{
			timer.Stop();
			ProgressRate = 0;
			LeftTime = 0.0;
		}

		public void AbortTimer()
		{
			StopTimer();
			if (!timerOverFlag && this.TimerEndCallback != null)
			{
				timerOverFlag = true;
				this.TimerEndCallback();
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/circularprogressbar.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			_contentLoaded = true;
		}
	}
}
