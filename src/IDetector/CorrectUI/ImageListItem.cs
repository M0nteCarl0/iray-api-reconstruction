using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CorrectUI
{
	public class ImageListItem : UserControl, IComponentConnector
	{
		private const double DPIX = 96.0;

		private const double DPIY = 96.0;

		private ImageCompress compressor;

		private AImageCompress.TPicRegion dstRegion;

		internal Image xImage;

		internal Image xCineFlag;

		internal Image xImageStatus;

		internal Label xDoseInfo;

		internal Label xFrameIndex;

		private bool _contentLoaded;

		public ImageListItem(int index, string kv, bool isCine = false)
		{
			InitializeComponent();
			xFrameIndex.Content = index.ToString();
			if (kv.Length == 0)
			{
				xDoseInfo.Visibility = Visibility.Collapsed;
			}
			else
			{
				xDoseInfo.Content = kv;
			}
			if (isCine)
			{
				xCineFlag.Visibility = Visibility.Visible;
			}
			if (compressor == null)
			{
				compressor = new ImageCompress();
				dstRegion = new AImageCompress.TPicRegion
				{
					stride = 100,
					width = 100,
					height = 100
				};
				dstRegion.pdata = new byte[dstRegion.width * dstRegion.height];
			}
		}

		public void ClearThumb()
		{
			xImage.Source = new BitmapImage(new Uri("/Images/none.png", UriKind.Relative));
			xImageStatus.Visibility = Visibility.Collapsed;
		}

		public void UpdateImage(ref IRayImageData image)
		{
			if (image != null)
			{
				AImageCompress.TPicRegion tPicRegion = default(AImageCompress.TPicRegion);
				tPicRegion.stride = image.nWidth;
				tPicRegion.height = image.nHeight;
				tPicRegion.width = image.nWidth;
				tPicRegion.pdata = CalcWinLevelWinWidth.GetGray8ImageByAutoWinLevelAndWidth(image.ImgData, image.nWidth, image.nHeight);
				AImageCompress.TPicRegion src = tPicRegion;
				compressor.PicZoom(ref dstRegion, src);
				xImage.Source = BitmapSource.Create(dstRegion.width, dstRegion.height, 96.0, 96.0, PixelFormats.Gray8, BitmapPalettes.BlackAndWhite, dstRegion.pdata, dstRegion.width);
			}
		}

		public void SetImageValidility(ImageStatus status)
		{
			xImageStatus.Visibility = Visibility.Visible;
			switch (status)
			{
			case ImageStatus.OK:
				xImageStatus.Source = new BitmapImage(new Uri("/Images/Yes.ico", UriKind.Relative));
				break;
			case ImageStatus.NO:
				xImageStatus.Source = new BitmapImage(new Uri("/Images/No.ico", UriKind.Relative));
				break;
			case ImageStatus.QUESTION:
				xImageStatus.Source = new BitmapImage(new Uri("/Images/Alarm.ico", UriKind.Relative));
				break;
			default:
				xImageStatus.Visibility = Visibility.Collapsed;
				break;
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/imagelistitem.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xImage = (Image)target;
				break;
			case 2:
				xCineFlag = (Image)target;
				break;
			case 3:
				xImageStatus = (Image)target;
				break;
			case 4:
				xDoseInfo = (Label)target;
				break;
			case 5:
				xFrameIndex = (Label)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
