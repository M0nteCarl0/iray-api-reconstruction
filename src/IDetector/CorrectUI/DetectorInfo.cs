using iDetector;

namespace CorrectUI
{
	public struct DetectorInfo
	{
		public SDKInvokeProxy InvokeProxy;

		public Detector Detector;

		public Shell Shell;

		public DetectorInfo(Detector instance, Shell shell)
		{
			Detector = instance;
			Shell = shell;
			InvokeProxy = new SDKInvokeProxy(Detector, shell);
		}
	}
}
