using Cinema;
using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateDynamicAcqPage : Page, IComponentConnector
	{
		private GenerateDynamicTemplate GenerateDynamicAcqPageVM;

		private CorrectionTemplateImageList ImageListVM;

		private CinePlayerCtrl mCinePlayCtrl;

		private Detector DetInstance;

		private string[] CineBufferName;

		private int GroupNumber = 3;

		private int GroupLightFrames;

		internal GenerateTemplatePage xDynamicGainAcqPage;

		private bool _contentLoaded;

		public GenerateDynamicAcqPage(DetectorInfo instance, DoseAlgorithm doseAlgorithm, Enm_FileTypes filetype)
		{
			InitializeComponent();
			DetInstance = instance.Detector;
			CineBufferName = new string[GroupNumber];
			GroupLightFrames = DetInstance.Prop_Attr_DefectTotalFrames / GroupNumber / 2;
			string text = DetInstance.Prop_Attr_WorkDir + "temp\\";
			for (int i = 0; i < CineBufferName.Length; i++)
			{
				CineBufferName[i] = text + i.ToString() + "-" + GroupLightFrames.ToString() + "Cine.cin";
			}
			if (!Utility.IsDirectoryExisted(text))
			{
				Utility.CreateDirectory(text);
			}
			ImageListVM = new CorrectionImageListVM();
			ImageListVM.DisplayThumb += ShowImage;
			xDynamicGainAcqPage.xImageList.DataContext = ImageListVM;
			xDynamicGainAcqPage.xImageList.MouseDoubleClick += ImageListVM.DoubleClick;
			switch (filetype)
			{
			case Enm_FileTypes.Enm_File_Gain:
				xDynamicGainAcqPage.xLoadFile.Visibility = Visibility.Hidden;
				GenerateDynamicAcqPageVM = new GenerateDynamicGainPageVM(instance, ImageListVM, doseAlgorithm);
				break;
			case Enm_FileTypes.Enm_File_Defect:
				xDynamicGainAcqPage.xLoadFile.Visibility = Visibility.Visible;
				GenerateDynamicAcqPageVM = new GenerateDynamicDefectPage4MNVM(instance, ImageListVM, doseAlgorithm);
				break;
			}
			GenerateDynamicAcqPageVM.ClosePlaybar += ClosePlaybar;
			GenerateDynamicAcqPageVM.StartStoreImage += StartStoreCineData;
			GenerateDynamicAcqPageVM.StopStoreImage += StopStoreCineData;
			base.DataContext = GenerateDynamicAcqPageVM;
			GenerateDynamicAcqPageVM.ProgressTimer = xDynamicGainAcqPage.xProgressBar;
			GenerateDynamicAcqPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += OnLoad;
			int num = Math.Min(DetInstance.Prop_Attr_Width, 640);
			int height = DetInstance.Prop_Attr_Height * num / DetInstance.Prop_Attr_Width;
			xDynamicGainAcqPage.xImageView.SetDisplaySize(num, height);
		}

		private void ImageListMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			InitCinePlay();
			if (mCinePlayCtrl.Init(CineBufferName[ImageListVM.SelectedIndex], DetInstance.Prop_Attr_Width, DetInstance.Prop_Attr_Height))
			{
				xDynamicGainAcqPage.xImageView.xPlayerBar.SetDataContext(mCinePlayCtrl);
				xDynamicGainAcqPage.xImageView.xPlayerBar.Start();
				mCinePlayCtrl.HandleEvent(Enum_PlayerEvent.Player_StepForward);
			}
		}

		private void StartStoreCineData(int groupNo)
		{
		}

		private void StopStoreCineData(bool updateCover = true)
		{
			if (updateCover)
			{
				base.Dispatcher.Invoke((Action)delegate
				{
				}, new object[0]);
			}
		}

		private void InitCinePlay()
		{
			ClosePlaybar();
			mCinePlayCtrl = new CinePlayerCtrl();
			mCinePlayCtrl.DisplayHandle += ShowImage;
		}

		private void ClosePlaybar()
		{
			if (mCinePlayCtrl != null)
			{
				xDynamicGainAcqPage.xImageView.xPlayerBar.Close();
				xDynamicGainAcqPage.xImageView.xPlayerBar.SetDataContext(null);
				mCinePlayCtrl.Close();
				mCinePlayCtrl = null;
			}
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GenerateDynamicAcqPageVM.StartInit(base.NavigationService);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GenerateDynamicAcqPageVM.AddCallbackHandler();
					xDynamicGainAcqPage.xImageView.xImageView.StartPaintTimer();
				}
			}
			else
			{
				xDynamicGainAcqPage.xImageView.xImageView.StopPaintTimer();
				GenerateDynamicAcqPageVM.RemoveCallbackHandler();
				ReleaseResource();
			}
		}

		private void ShowImage(byte[] data, int width, int height)
		{
			ShowImage(new IRayImageData(data, width, height));
		}

		private void ShowImage(IRayImageData image)
		{
			xDynamicGainAcqPage.xImageView.DisplayImage(image);
		}

		private void PaintImage(IRayImageData image)
		{
			xDynamicGainAcqPage.xImageView.UpdateImage(image);
		}

		private void AdjustWinLevelAndWinWidth()
		{
			base.Dispatcher.BeginInvoke((Action)delegate
			{
				ushort[] oriGray16ImgBuffer = xDynamicGainAcqPage.xImageView.xImageView.OriGray16ImgBuffer;
				int winLevel = 0;
				int winWidth = 0;
				CalcWinLevelWinWidth.GetWinLevelAndWidth(oriGray16ImgBuffer, ref winLevel, ref winWidth, DetInstance.Prop_Attr_Width, DetInstance.Prop_Attr_Height);
				xDynamicGainAcqPage.xImageView.xImageView.SetWindow(winWidth, winLevel);
			});
		}

		private void ReleaseResource()
		{
			string[] cineBufferName = CineBufferName;
			foreach (string path in cineBufferName)
			{
				if (File.Exists(path))
				{
					File.Delete(path);
				}
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatedynamicacqpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xDynamicGainAcqPage = (GenerateTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
