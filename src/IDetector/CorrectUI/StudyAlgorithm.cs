using System;

namespace CorrectUI
{
	public class StudyAlgorithm
	{
		public static Coefficient CalculateCoeff(DoseFactor item)
		{
			double num = (double)(item.Gray2 - item.Gray1) / (item.mAs2 - item.mAs1);
			double value = (num == 0.0) ? 0.0 : ((double)item.Gray2 - item.mAs2 * num);
			return new Coefficient(Math.Round(num, 1), Math.Round(value, 1), item.KV);
		}
	}
}
