using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateTemplateEnd : Page, IComponentConnector
	{
		private DoseAlgorithm DoseAlgorithm;

		private DetectorInfo DetInstance;

		private bool _contentLoaded;

		public GenerateTemplateEnd(DetectorInfo instance, DoseAlgorithm doseAlgorithm)
		{
			InitializeComponent();
			DoseAlgorithm = doseAlgorithm;
			DetInstance = instance;
		}

		private void OnBrowseBack(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				switch (DetInstance.Detector.Prop_Attr_UROM_ProductNo)
				{
				case 6:
				case 46:
				case 48:
				case 49:
				case 50:
				case 56:
				case 68:
				case 73:
				case 74:
				case 87:
					base.NavigationService.Navigate(new GenerateDynamicGainDefectPage(DetInstance, DoseAlgorithm));
					break;
				case 58:
					base.NavigationService.Navigate(new GenerateDynamicAcqPage(DetInstance, DoseAlgorithm, Enm_FileTypes.Enm_File_Defect));
					break;
				default:
					base.NavigationService.Navigate(new GenerateDefectPage(DetInstance, DoseAlgorithm));
					break;
				}
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatetemplateend.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				((Hyperlink)target).Click += OnBrowseBack;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
