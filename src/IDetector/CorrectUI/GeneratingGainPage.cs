using CorrectUI.ViewModel;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GeneratingGainPage : Page, IComponentConnector
	{
		private GeneratingGainPageVM GeneratingGainPageVM;

		internal GeneratingTemplatePage xGeneratingGain;

		private bool _contentLoaded;

		public GeneratingGainPage(DetectorInfo detInstance, DoseAlgorithm doseAlgorithm)
		{
			InitializeComponent();
			GeneratingGainPageVM = new GeneratingGainPageVM(detInstance, doseAlgorithm);
			base.DataContext = GeneratingGainPageVM;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += OnLoad;
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GeneratingGainPageVM.NavigationService = base.NavigationService;
				new Thread((ThreadStart)delegate
				{
					GeneratingGainPageVM.StartCreateFile();
				}).Start();
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GeneratingGainPageVM.AddCallbackHandler();
				}
			}
			else
			{
				GeneratingGainPageVM.RemoveCallbackHandler();
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatinggainpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xGeneratingGain = (GeneratingTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
