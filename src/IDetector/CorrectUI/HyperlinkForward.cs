using System.Windows;

namespace CorrectUI
{
	public class HyperlinkForward : HyperlinkLabel
	{
		public string Caption
		{
			get
			{
				return xForwardLabel.Content.ToString();
			}
			set
			{
				xForwardLabel.Content = value;
			}
		}

		public HyperlinkForward()
		{
			xBackLabel.Visibility = Visibility.Collapsed;
		}

		protected override void SetLabelContent(string content)
		{
			Caption = content;
		}
	}
}
