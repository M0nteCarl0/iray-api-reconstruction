namespace CorrectUI
{
	public class Coefficient
	{
		public uint KV;

		public double Coeff;

		public double Intercept;

		public Coefficient(double _coeff, double _intercept, uint _kv)
		{
			Coeff = _coeff;
			Intercept = _intercept;
			KV = _kv;
		}
	}
}
