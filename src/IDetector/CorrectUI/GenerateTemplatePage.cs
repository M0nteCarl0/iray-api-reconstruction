using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateTemplatePage : UserControl, IComponentConnector
	{
		internal Button xLoadFile;

		internal Label xAcqProgress;

		internal CorrectionImageList xImageList;

		internal TextBox xFlagForAutoTest;

		internal CircularProgressBar xProgressBar;

		internal CorrectionImageView xImageView;

		private bool _contentLoaded;

		public GenerateTemplatePage()
		{
			InitializeComponent();
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatetemplatepage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			switch (connectionId)
			{
			case 1:
				xLoadFile = (Button)target;
				break;
			case 2:
				xAcqProgress = (Label)target;
				break;
			case 3:
				xImageList = (CorrectionImageList)target;
				break;
			case 4:
				xFlagForAutoTest = (TextBox)target;
				break;
			case 5:
				xProgressBar = (CircularProgressBar)target;
				break;
			case 6:
				xImageView = (CorrectionImageView)target;
				break;
			default:
				_contentLoaded = true;
				break;
			}
		}
	}
}
