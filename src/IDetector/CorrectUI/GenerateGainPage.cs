using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateGainPage : Page, IComponentConnector
	{
		private GenerateGainPageVM GenerateGainPageVM;

		private CorrectionImageListVM ImageListVM;

		internal GenerateTemplatePage xGainAcquireImage;

		private bool _contentLoaded;

		public GenerateGainPage(DetectorInfo instance, DoseAlgorithm doseAlgorithm)
		{
			InitializeComponent();
			ImageListVM = new CorrectionImageListVM();
			ImageListVM.DisplayThumb += ShowImage;
			xGainAcquireImage.xImageList.DataContext = ImageListVM;
			xGainAcquireImage.xImageList.MouseDoubleClick += ImageListVM.DoubleClick;
			GenerateGainPageVM = new GenerateGainPageVM(instance, ImageListVM, doseAlgorithm);
			base.DataContext = GenerateGainPageVM;
			GenerateGainPageVM.ProgressTimer = xGainAcquireImage.xProgressBar;
			GenerateGainPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += Onload;
			xGainAcquireImage.xImageView.SetDisplaySize(instance.Detector.Prop_Attr_Width, instance.Detector.Prop_Attr_Height);
		}

		private void Onload(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GenerateGainPageVM.StartGainInit(base.NavigationService);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GenerateGainPageVM.AddCallbackHandler();
				}
			}
			else
			{
				GenerateGainPageVM.RemoveCallbackHandler();
			}
		}

		private void ShowImage(IRayImageData image)
		{
			xGainAcquireImage.xImageView.DisplayImage(image);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generategainpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xGainAcquireImage = (GenerateTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
