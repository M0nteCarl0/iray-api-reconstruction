using CorrectUI.ViewModel;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateOffsetPage : Page, IComponentConnector
	{
		private GenerateOffsetPageVM OffsetGeneratePageVM;

		private bool bEnableSkip;

		internal Label xInfo;

		private bool _contentLoaded;

		public bool Finished
		{
			get
			{
				return OffsetGeneratePageVM.FinishedFlag;
			}
		}

		public GenerateOffsetPage(DetectorInfo instance)
		{
			InitializeComponent();
			OffsetGeneratePageVM = new GenerateOffsetPageVM(instance);
			base.DataContext = OffsetGeneratePageVM;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += Onload;
			bEnableSkip = true;
		}

		public void EnableSkip(bool bEnable)
		{
			bEnableSkip = bEnable;
		}

		private void Onload(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				OffsetGeneratePageVM.StartGenerateOffset(base.NavigationService, bEnableSkip);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					OffsetGeneratePageVM.AddCallbackHandler();
				}
			}
			else
			{
				OffsetGeneratePageVM.RemoveCallbackHandler();
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generateoffsetpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xInfo = (Label)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
