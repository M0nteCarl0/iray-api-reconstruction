using CorrectUI.ViewModel;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GeneratingDefectPage : Page, IComponentConnector
	{
		private GeneratingDefectPageVM GeneratingDefectPageVM;

		internal GeneratingTemplatePage xGeneratingDefect;

		private bool _contentLoaded;

		public GeneratingDefectPage(DetectorInfo detInstance, DoseAlgorithm doseAlgorithm)
		{
			InitializeComponent();
			GeneratingDefectPageVM = new GeneratingDefectPageVM(detInstance, doseAlgorithm);
			base.DataContext = GeneratingDefectPageVM;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += OnLoad;
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GeneratingDefectPageVM.NavigationService = base.NavigationService;
				new Thread((ThreadStart)delegate
				{
					GeneratingDefectPageVM.StartCreateFile();
				}).Start();
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GeneratingDefectPageVM.AddCallbackHandler();
				}
			}
			else
			{
				GeneratingDefectPageVM.RemoveCallbackHandler();
			}
		}

		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatingdefectpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerNonUserCode]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xGeneratingDefect = (GeneratingTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
