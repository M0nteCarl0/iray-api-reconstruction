using CorrectUI.ViewModel;
using iDetector;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class GenerateDefectPage : Page, IComponentConnector
	{
		private GenerateDefectPageVM GenerateDefectPageVM;

		private CorrectionImageListVM ImageListVM;

		internal GenerateTemplatePage xDefectAcquireImage;

		private bool _contentLoaded;

		public GenerateDefectPage(DetectorInfo instance, DoseAlgorithm doseAlgorithm, bool skipGeneratingPage = false)
		{
			InitializeComponent();
			xDefectAcquireImage.xLoadFile.Visibility = Visibility.Visible;
			ImageListVM = new CorrectionImageListVM();
			ImageListVM.DisplayThumb += ShowImage;
			xDefectAcquireImage.xImageList.DataContext = ImageListVM;
			xDefectAcquireImage.xImageList.MouseDoubleClick += ImageListVM.DoubleClick;
			GenerateDefectPageVM = new GenerateDefectPageVM(instance, ImageListVM, doseAlgorithm, skipGeneratingPage);
			base.DataContext = GenerateDefectPageVM;
			GenerateDefectPageVM.ProgressTimer = xDefectAcquireImage.xProgressBar;
			GenerateDefectPageVM.ShowImage += ShowImage;
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += OnLoad;
			xDefectAcquireImage.xImageView.SetDisplaySize(instance.Detector.Prop_Attr_Width, instance.Detector.Prop_Attr_Height);
		}

		private void OnLoad(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				GenerateDefectPageVM.StartDefectInit(base.NavigationService);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					GenerateDefectPageVM.AddCallbackHandler();
				}
			}
			else
			{
				GenerateDefectPageVM.RemoveCallbackHandler();
			}
		}

		private void ShowImage(IRayImageData image)
		{
			xDefectAcquireImage.xImageView.DisplayImage(image);
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/generatedefectpage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xDefectAcquireImage = (GenerateTemplatePage)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
