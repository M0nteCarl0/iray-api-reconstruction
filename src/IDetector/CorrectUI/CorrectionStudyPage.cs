using CorrectUI.ViewModel;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace CorrectUI
{
	public class CorrectionStudyPage : Page, IComponentConnector
	{
		private CorrectionStudyPageVM CorrectionStudyPageVM;

		internal CircularProgressBar xProgressBar;

		private bool _contentLoaded;

		public CorrectionStudyPage(DetectorInfo instance)
		{
			InitializeComponent();
			switch (instance.Detector.Prop_Attr_UROM_ProductNo)
			{
			case 6:
			case 46:
			case 48:
			case 49:
			case 50:
			case 56:
			case 58:
			case 68:
			case 73:
			case 74:
			case 87:
				CorrectionStudyPageVM = new CorrectionDynamicStudyPageVM(instance);
				break;
			default:
				CorrectionStudyPageVM = new CorrectionStudyPageVM(instance);
				break;
			}
			CorrectionStudyPageVM.ProgressTimer = xProgressBar;
			base.DataContext = CorrectionStudyPageVM;
			CorrectionStudyPageVM.AddCallbackHandler();
			base.IsVisibleChanged += VisibleChanged;
			base.Loaded += Onload;
		}

		private void Onload(object sender, RoutedEventArgs e)
		{
			if (base.NavigationService != null)
			{
				CorrectionStudyPageVM.InitNavigationService(base.NavigationService);
			}
		}

		private void VisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			if ((bool)e.NewValue)
			{
				if (base.NavigationService != null)
				{
					CorrectionStudyPageVM.AddCallbackHandler();
				}
			}
			else
			{
				CorrectionStudyPageVM.RemoveCallbackHandler();
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		public void InitializeComponent()
		{
			if (!_contentLoaded)
			{
				_contentLoaded = true;
				Uri resourceLocator = new Uri("/iDetector;component/createtemplate/correctionstudypage.xaml", UriKind.Relative);
				Application.LoadComponent(this, resourceLocator);
			}
		}

		[GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
		[DebuggerNonUserCode]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void IComponentConnector.Connect(int connectionId, object target)
		{
			if (connectionId == 1)
			{
				xProgressBar = (CircularProgressBar)target;
			}
			else
			{
				_contentLoaded = true;
			}
		}
	}
}
