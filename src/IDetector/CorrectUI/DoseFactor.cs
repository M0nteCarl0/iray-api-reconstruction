namespace CorrectUI
{
	public struct DoseFactor
	{
		public uint KV;

		public double mAs1;

		public double mAs2;

		public int Gray1;

		public int Gray2;

		public DoseFactor(uint _kv, double _mAs1, double _mAs2)
		{
			KV = _kv;
			mAs1 = _mAs1;
			mAs2 = _mAs2;
			Gray1 = 0;
			Gray2 = 0;
		}
	}
}
