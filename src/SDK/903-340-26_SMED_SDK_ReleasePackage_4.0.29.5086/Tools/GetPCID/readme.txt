GetPCID.exe
This is a tool to read PC ID that is used to create license.
Usage:
1. Double click the GetPCID.exe
2. Click "Get PCID" button
3. Select and copy the textbox content
4. Mail the ID string to the authorizer
5. Authorizer send you a FpdSys.lic file
6. Backup the FpdSys.lic file that already exist in the same directory with     iDetector.exe
7. Replace the original FpdSys.lic file that received in step 5
8. Reopen iDetector.exe and write parameters
9. Recovery the original FpdSys.lic file
