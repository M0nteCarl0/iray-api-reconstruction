/******************************************************************
**  FileName   : FpdIrDALib.h
**
**  Description: This is the main interface definition file.
**  
**  Copyright  : iRay Technology (Shanghai) Ltd.
******************************************************************/

#ifndef __FPD_IRDA_API_H
#define __FPD_IRDA_API_H

#ifndef FPDIRDAREGISTER_EXPORTS
#define FPD_IR_API extern "C" __declspec(dllimport)
#else
#define FPD_IR_API extern "C" __declspec(dllexport)
#endif


enum IR_CALLBACK_INFO
{
	IR_INFO_OCCUR_ERROR,
	IR_INFO_FIND_DEVICE,
	IR_INFO_FINISH_REGIST,
	IR_INFO_GET_CONFIG,
};

enum IR_LIB_RESULT
{
	IR_RESULT_SUCCESS,
	IR_RESULT_FAILED,
};

enum IR_WORK_MODE
{	
	IR_WRITE_ALWAYS = 0x01,
	IR_WRITE_COMFIRM_BY_USER = 0x02,
	IR_READ = 0x10,
};

enum IR_UPDATE_MODE
{
	IR_UPDATE_NONE,
	IR_UPDATE_WIFI,
	IR_UPDATE_IP,
};

enum IR_GET_INFO
{
	IR_GET_NONE,
	IR_GET_WIFI_PARAM = 0x01,
	IR_GET_FPD_PARAM = 0x02,
};

#pragma pack(push, 1)
struct IrNetworkParam
{
	int  APModeEn;
	struct CLIENT 
	{
		char SSID[32];
		char Key[32];
	}Client;
	struct AP
	{
		char SSID[32];
		char Key[32];
		char CountryCode[2];
		int  FrequencySel;	
		int  BandWidthSel;
		int  ChannelSel;
		int  SecuritySel;	
		int  DHCPServerEn;		// Reserved
	}Ap;
	char WorkIP[32];
	char FpdInfo[32];
};
#pragma pack(pop)

#pragma pack(push, 1)
struct IrWorkModeParam
{
	int IrWorkMode;
	int IrWaitingComfirmTimeout;
	int IrUpdateMode;
};
#pragma pack(pop)

typedef int (CALLBACK* IrDACallback)(IR_CALLBACK_INFO, int, int, void*);


FPD_IR_API const char* FPD_IrDAGetLastError();
FPD_IR_API const char* FPD_IrDAGetDeviceName();
FPD_IR_API IR_LIB_RESULT FPD_IrDAGetWiFiMode(IrNetworkParam &workParam);
FPD_IR_API IR_LIB_RESULT FPD_IrDASetWiFiMode(IrNetworkParam workParam);
FPD_IR_API IR_LIB_RESULT FPD_IrDAStart(IrDACallback pIrCallback);
FPD_IR_API IR_LIB_RESULT FPD_IrDAStop();
FPD_IR_API IR_LIB_RESULT FPD_IrDAGetWorkMode(IrWorkModeParam &workMode);
FPD_IR_API IR_LIB_RESULT FPD_IrDASetWorkMode(IrWorkModeParam workMode);
FPD_IR_API IR_LIB_RESULT FPD_IrDAUpdateOnce(int updateMode);

#endif