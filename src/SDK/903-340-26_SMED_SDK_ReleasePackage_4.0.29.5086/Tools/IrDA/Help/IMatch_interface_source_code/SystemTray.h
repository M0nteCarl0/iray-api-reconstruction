#pragma once

#define WM_ICON_NOTIFY WM_USER+111

// CSystemTray

class CSystemTray : public CWnd
{
	DECLARE_DYNAMIC(CSystemTray)

public:
	CSystemTray();
	virtual ~CSystemTray();

	// Operations
public:
	BOOL Create(UINT uCallbackMessage, LPCTSTR lpszTip, HICON hIcon, UINT uID);

	BOOL SetIcon(UINT nIDResource);

	BOOL SetNotificationWnd(CWnd* pNotifyWnd);
	virtual LRESULT OnTrayNotification(UINT wParam, LONG lParam);

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSystemTray)
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

	// Implementation
protected:
	NOTIFYICONDATA  m_NotifyIconData;

protected:
	DECLARE_MESSAGE_MAP()
};


