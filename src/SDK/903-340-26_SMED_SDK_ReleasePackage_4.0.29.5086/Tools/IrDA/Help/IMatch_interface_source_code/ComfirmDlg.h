#pragma once


// CComfirmDlg 对话框

class CComfirmDlg : public CDialog
{
	DECLARE_DYNAMIC(CComfirmDlg)

public:
	CComfirmDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CComfirmDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_COMFIRM_UPDATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:
	bool	m_bComfirmResut;
	int		m_nElapseTime;
	int		m_nTimer;

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonYes();
	afx_msg void OnBnClickedButtonNo();

	__declspec(property(get = getResult))  bool ComfirmResult;
	const bool getResult() const {return m_bComfirmResut;}
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	void CloseDlg();
	void SetElapseTime(int nElapseTime);
};
