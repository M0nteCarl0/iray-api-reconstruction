// ComfirmDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "IrDARegister.h"
#include "ComfirmDlg.h"


// CComfirmDlg 对话框

IMPLEMENT_DYNAMIC(CComfirmDlg, CDialog)

CComfirmDlg::CComfirmDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CComfirmDlg::IDD, pParent)
{
	m_bComfirmResut = false;
	m_nElapseTime = 10000;
	m_nTimer = 0;
}

CComfirmDlg::~CComfirmDlg()
{
}

void CComfirmDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CComfirmDlg, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_YES, &CComfirmDlg::OnBnClickedButtonYes)
	ON_BN_CLICKED(IDC_BUTTON_NO, &CComfirmDlg::OnBnClickedButtonNo)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// CComfirmDlg 消息处理程序

void CComfirmDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(m_nTimer == nIDEvent)
	{
		CString strInfo;
		m_nElapseTime-=1000;
		strInfo.Format(_T("Update New Wireless Setting? %d"), m_nElapseTime/1000);
		SetDlgItemText(IDC_STATIC_COMFIRM_INFO, strInfo);
		UpdateData();
	}

	CDialog::OnTimer(nIDEvent);
}

void CComfirmDlg::OnBnClickedButtonYes()
{
	m_bComfirmResut = true;
	OnCancel();
}

void CComfirmDlg::OnBnClickedButtonNo()
{
	m_bComfirmResut = false;
	OnCancel();
}

void CComfirmDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if(bShow)
	{
		CString strInfo;
		strInfo.Format(_T("Update New Wireless Setting? %d"), m_nElapseTime/1000);
		SetDlgItemText(IDC_STATIC_COMFIRM_INFO, strInfo);
		m_bComfirmResut = false;
		m_nTimer = SetTimer(100, 1000, NULL);
	}
}

void CComfirmDlg::CloseDlg()
{
	OnCancel();
}

void CComfirmDlg::SetElapseTime( int nElapseTime )
{
	m_nElapseTime = nElapseTime;
}