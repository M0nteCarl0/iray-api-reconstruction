
// IrDARegister.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "SystemTray.h"

// CIrDARegisterApp:
// See IrDARegister.cpp for the implementation of this class
//

class CIrDARegisterApp : public CWinAppEx
{
public:
	CIrDARegisterApp();
	CSystemTray m_TrayIcon;

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()

private:
	CString m_strAppDir;

public:
	CString GetAppDir();

};

extern CIrDARegisterApp theApp;