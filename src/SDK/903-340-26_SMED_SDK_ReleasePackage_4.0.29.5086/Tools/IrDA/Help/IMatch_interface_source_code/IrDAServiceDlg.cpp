// IrDAServiceDlg.cpp : 实现文件
//

#pragma comment (lib,"FpdIrDARegister.lib") 

#include "stdafx.h"
#include "IrDARegister.h"
#include "IrDAServiceDlg.h"
#include "FpdIrDALib.h"

// Frequency	 0:2.4GHz; 1:5GHz
// BandWidth	 0:HT20; 1:HT40+; 2:HT40-

//2.4GHz	1～14
//5.0GHz	36,40,44,48,52,56,60,64,100,104,108,112,116,120,124,128,132,136,140,144,149,153,157,161,165

#define WM_IRDA_COMFIRM_DISPLAY			WM_USER+100

#define WAITING_USER_COMFIRM_TIMEOUT	15000
#define CHANNEL_TABLE_LEN				60
#define VERSION_STR						L"Ver: 1.0.0.5"

CHANNEL_TABLE Channel_Def[] = {
    {0, {'C', 'N'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
    {0, {'C', 'N'}, 0, 1, {1, 2, 3, 4, 5, 6, 7}},
    {0, {'C', 'N'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
    {0, {'C', 'N'}, 1, 0, {149, 153, 157, 161, 165}},
    {0, {'C', 'N'}, 1, 1, {149, 157}},
    {0, {'C', 'N'}, 1, 2, {153, 161}},

    {1, {'D', 'E'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
    {1, {'D', 'E'}, 0, 1, {1, 2, 3, 4, 5, 6, 7, 8, 9}},
    {1, {'D', 'E'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
    {1, {'D', 'E'}, 1, 0, {36, 40, 44, 48}},
    {1, {'D', 'E'}, 1, 1, {36, 44}},
    {1, {'D', 'E'}, 1, 2, {40, 48}},
    
	{2, {'F', 'R'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{2, {'F', 'R'}, 0, 1, {1, 2, 3, 4, 5, 6, 7, 8, 9}},
	{2, {'F', 'R'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{2, {'F', 'R'}, 1, 0, {36, 40, 44, 48}},
	{2, {'F', 'R'}, 1, 1, {36, 44}},
	{2, {'F', 'R'}, 1, 2, {40, 48}},

	{3, {'G', 'B'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{3, {'G', 'B'}, 0, 1, {1, 2, 3, 4, 5, 6, 7, 8, 9}},
	{3, {'G', 'B'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{3, {'G', 'B'}, 1, 0, {36, 40, 44, 48}},
	{3, {'G', 'B'}, 1, 1, {36, 44}},
	{3, {'G', 'B'}, 1, 2, {40, 48}},

	{4, {'H', 'K'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}},
    {4, {'H', 'K'}, 0, 1, {1, 2, 3, 4, 5, 6, 7}},
    {4, {'H', 'K'}, 0, 2, {5, 6, 7, 8, 9, 10, 11}},
    {4, {'H', 'K'}, 1, 0, {36, 40, 44, 48, 149, 153, 157, 161, 165}},
    {4, {'H', 'K'}, 1, 1, {36, 44, 149, 157}},
    {4, {'H', 'K'}, 1, 2, {40, 48, 153, 161}},

	{5, {'I', 'T'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{5, {'I', 'T'}, 0, 1, {1, 2, 3, 4, 5, 6, 7, 8, 9}},
	{5, {'I', 'T'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{5, {'I', 'T'}, 1, 0, {36, 40, 44, 48}},
	{5, {'I', 'T'}, 1, 1, {36, 44}},
	{5, {'I', 'T'}, 1, 2, {40, 48}},

    {6, {'K', 'R'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
    {6, {'K', 'R'}, 0, 1, {}},
	{6, {'K', 'R'}, 0, 2, {}},
    {6, {'K', 'R'}, 1, 0, {36, 40, 44, 48, 149, 153, 157, 161}},
    {6, {'K', 'R'}, 1, 1, {36, 44, 149, 157}},
    {6, {'K', 'R'}, 1, 2, {40, 48, 153, 161}},

	{7, {'N', 'L'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{7, {'N', 'L'}, 0, 1, {1, 2, 3, 4, 5, 6, 7, 8, 9}},
	{7, {'N', 'L'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{7, {'N', 'L'}, 1, 0, {36, 40, 44, 48}},
	{7, {'N', 'L'}, 1, 1, {36, 44}},
	{7, {'N', 'L'}, 1, 2, {40, 48}},

	{8, {'R', 'U'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{8, {'R', 'U'}, 0, 1, {1, 2, 3, 4, 5, 6, 7, 8, 9}},
	{8, {'R', 'U'}, 0, 2, {5, 6, 7, 8, 9, 10, 11, 12, 13}},
	{8, {'R', 'U'}, 1, 0, {36, 40, 44, 48, 149, 153, 157, 161, 165}},
	{8, {'R', 'U'}, 1, 1, {36, 44, 149, 157}},
	{8, {'R', 'U'}, 1, 2, {40, 48, 153, 161}},
	
	{9, {'U', 'S'}, 0, 0, {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}},
    {9, {'U', 'S'}, 0, 1, {1, 2, 3, 4, 5, 6, 7}},
    {9, {'U', 'S'}, 0, 2, {5, 6, 7, 8, 9, 10, 11}},
    {9, {'U', 'S'}, 1, 0, {36, 40, 44, 48, 149, 153, 157, 161, 165}},
    {9, {'U', 'S'}, 1, 1, {36, 44, 149, 157}},
    {9, {'U', 'S'}, 1, 2, {40, 48, 153, 161}},
};


// CIrDAServiceDlg 对话框

IMPLEMENT_DYNAMIC(CIrDAServiceDlg, CDialog)

CIrDAServiceDlg::CIrDAServiceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIrDAServiceDlg::IDD, pParent)
{
	m_bServiceRun = FALSE;
	m_bInitialized = FALSE;
	m_bConfig = FALSE;
	m_nLogCount = 0;

	CWnd* pWnd=CWnd::FindWindow(NULL,_T("Quick'n IrDA Regist Service"));
	if(pWnd!=NULL){
		AfxMessageBox(_T("Another exe running!"));
		PostMessage(WM_QUIT,0,0);
	}

	m_nTimer = 0;
}

CIrDAServiceDlg::~CIrDAServiceDlg()
{
}

void CIrDAServiceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_BUTTON_SETTING, m_btnSetting);
	DDX_Control(pDX, IDC_BUTTON_APPLY, m_btnApply);
	DDX_Control(pDX, IDC_EDIT_SSID, m_txtSSID);
	DDX_Control(pDX, IDC_EDIT_PWD, m_txtPWD);
	DDX_Control(pDX, IDC_COMBO_AP_SECURITY, m_cboSecurity);
	DDX_Control(pDX, IDC_COMBO_AP_COUNTRY, m_cboCountryCode);
	DDX_Control(pDX, IDC_COMBO_AP_FREQUENCY, m_cboFrequency);
	DDX_Control(pDX, IDC_COMBO_AP_BAND, m_cboBandWidth);
	DDX_Control(pDX, IDC_COMBO_AP_CHANNEL_SEL, m_cboChannel);
	DDX_Control(pDX, IDC_CBO_WORKMODE, m_cboWorkMode);
}


BEGIN_MESSAGE_MAP(CIrDAServiceDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CIrDAServiceDlg::OnBnClickedButtonExit)
	ON_WM_SYSCOMMAND()
	ON_COMMAND(ID_POPUP_SHUTDOWN, &CIrDAServiceDlg::OnPopupShutdown)
	ON_COMMAND(ID_POPUP_SHOW_WINDOW, &CIrDAServiceDlg::OnPopupShowWindow)
	ON_COMMAND(ID_POPUP_START_SERVICE, &CIrDAServiceDlg::OnPopupStartService)
	ON_COMMAND(ID_POPUP_STOP_SERVICE, &CIrDAServiceDlg::OnPopupStopService)
	ON_BN_CLICKED(IDC_BUTTON_START_STOP, &CIrDAServiceDlg::OnBnClickedButtonStartStop)
	ON_WM_INITMENUPOPUP()
//	ON_WM_CREATE()
	ON_BN_CLICKED(IDC_BUTTON_SETTING, &CIrDAServiceDlg::OnBnClickedButtonSetting)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BUTTON_APPLY, &CIrDAServiceDlg::OnBnClickedButtonApply)
	ON_BN_CLICKED(IDC_CHECK_AS_AP, &CIrDAServiceDlg::OnBnClickedCheckAsAp)
	ON_CBN_SELCHANGE(IDC_COMBO_AP_FREQUENCY, &CIrDAServiceDlg::OnCbnSelchangeComboApFrequencyCountryBand)
	ON_CBN_SELCHANGE(IDC_COMBO_AP_COUNTRY, &CIrDAServiceDlg::OnCbnSelchangeComboApFrequencyCountryBand)
	ON_CBN_SELCHANGE(IDC_COMBO_AP_BAND, &CIrDAServiceDlg::OnCbnSelchangeComboApFrequencyCountryBand)
	ON_MESSAGE(WM_IRDA_COMFIRM_DISPLAY, &CIrDAServiceDlg::OnIrdaComfirmDisplay)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CIrDAServiceDlg 消息处理程序

int CALLBACK IrDAInfoCallback(IR_CALLBACK_INFO iMsg, int iValue1, int iValue2, void* pVoid)
{
	CString strInfo;
	CIrDAServiceDlg* pFrame = (CIrDAServiceDlg*)AfxGetApp()->m_pMainWnd;

	USES_CONVERSION; 
	switch(iMsg)
	{
	case IR_INFO_OCCUR_ERROR:	
		strInfo.Format(_T("%s | E | %s"), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")), A2T(FPD_IrDAGetLastError()));
		break;
	case IR_INFO_FIND_DEVICE:
		strInfo.Format(_T("%s | I | Find Dev:%s"), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")), A2T(FPD_IrDAGetDeviceName()));
		break;
	case IR_INFO_FINISH_REGIST:	
		strInfo.Format(_T("%s | I | Finish regist."), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")));
		Beep(1000,200);
#if _DEBUG
		::PostMessage(pFrame->m_hWnd, WM_IRDA_COMFIRM_DISPLAY, NULL, NULL);
#endif
		break;
	case IR_INFO_GET_CONFIG:
		{
			if(iValue1 > 0)
			{
				IrNetworkParam* pIrNetParm = (IrNetworkParam*)pVoid;
			
				if((iValue1 & IR_GET_WIFI_PARAM) > 0)
				{				
					if(pIrNetParm->APModeEn)
					{
						strInfo.Format(_T("%s | I | AP Mode %s."), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")), pIrNetParm->Ap.FrequencySel?_T("5G"):_T("2.4G"));
						pFrame->SetLogText(strInfo);
						strInfo.Format(_T("(SSID: %s, Key: %s)"), A2T(pIrNetParm->Ap.SSID), A2T(pIrNetParm->Ap.Key));
					}
					else
					{
						strInfo.Format(_T("%s | I | Client Mode."), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")));
						pFrame->SetLogText(strInfo);
						strInfo.Format(_T("(SSID: %s, Key: %s)"), A2T(pIrNetParm->Client.SSID), A2T(pIrNetParm->Client.Key));
					}
				}
				if((iValue1 & IR_GET_FPD_PARAM) > 0)
				{
					pFrame->SetLogText(strInfo);
					strInfo.Format(_T("(IP: %s, SN: %s)"), A2T(pIrNetParm->WorkIP), A2T(pIrNetParm->FpdInfo));
				}
			}
			else
			{
				strInfo.Format(_T("%s | I | Unknow Work Mode!"), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")));
			}
			if(pFrame->GetIrdaWorkMode() == 3)
			{
				::PostMessage(pFrame->m_hWnd, WM_IRDA_COMFIRM_DISPLAY, NULL, NULL);
			}
		}
		break;
	}
	pFrame->SetLogText(strInfo);

	return 0;
}


BOOL CIrDAServiceDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cboCountryCode.ResetContent();
	m_cboCountryCode.AddString(_T("CN"));
    m_cboCountryCode.AddString(_T("DE"));
    m_cboCountryCode.AddString(_T("FR"));
    m_cboCountryCode.AddString(_T("GB"));
    m_cboCountryCode.AddString(_T("HK"));
    m_cboCountryCode.AddString(_T("IT"));
    m_cboCountryCode.AddString(_T("KR"));
    m_cboCountryCode.AddString(_T("NL"));
    m_cboCountryCode.AddString(_T("RU"));
	m_cboCountryCode.AddString(_T("US"));

	m_cboFrequency.ResetContent();
	m_cboFrequency.AddString(_T("2.4GHz"));
	m_cboFrequency.AddString(_T("5GHz"));

	m_cboBandWidth.ResetContent();
	m_cboBandWidth.AddString(_T("HT20"));
	m_cboBandWidth.AddString(_T("HT40+"));
	m_cboBandWidth.AddString(_T("HT40-"));

	m_cboSecurity.ResetContent();
	m_cboSecurity.AddString(_T("WPA/WPA2-PSK"));
	m_cboSecurity.AddString(_T("WPA-PSK"));
	m_cboSecurity.AddString(_T("WPA2-PSK"));

	m_cboWorkMode.SetCurSel(0);

	//---------------------------

	HICON hIcon = AfxGetApp()->LoadIcon(IDI_ICON_SETUP);			//::LoadIcon( ::AfxGetInstanceHandle(), (LPCTSTR)IDI_ICON_SETUP );
	m_btnSetting.SetIcon(hIcon);
	//m_btnSetting.ModifyStyle(BS_TEXT, BS_ICON);
	//if (m_btnSetting.GetIcon() == NULL)
	//m_btnSetting.SetIcon(::LoadIcon(::AfxGetInstanceHandle(),MAKEINTRESOURCE(IDI_ICON_SETUP)));
	//m_txtSSID.Create()
	//((CListBox*)GetDlgItem(IDC_LIST_RESULT))->MoveWindow(0,17,172,184,true);

	//-----------------------------

	CRect rcDlgs;
	// get dialog area rect
	GetDlgItem(IDC_STATIC_PORPPAGE)->GetWindowRect(rcDlgs);
	ScreenToClient(rcDlgs);
	GetDlgItem(IDC_LIST_RESULT)->MoveWindow(rcDlgs); 

	m_txtSSID.SetLimitText(32);
	m_txtPWD.SetLimitText(32);

	SetLogText(VERSION_STR);

	m_bInitialized = TRUE;
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}



void CIrDAServiceDlg::OnBnClickedButtonExit()
{
	// TODO: 在此添加控件通知处理程序代码
	if(m_bServiceRun)
	{
		OnPopupStopService();
	}
	OnCancel();
}

void CIrDAServiceDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch(nID)
	{
	case SC_CLOSE:
		// do not minimize to the taskbar
		ShowWindow(SW_HIDE);
		break;
	default:
		CDialog::OnSysCommand(nID, lParam);
		break;
	}
}

void CIrDAServiceDlg::OnPopupShutdown()
{
	// TODO: 在此添加命令处理程序代码
	if(m_bServiceRun)
	{
		OnPopupStopService();
	}
	OnCancel();
}

void CIrDAServiceDlg::OnPopupShowWindow()
{
	// TODO: 在此添加命令处理程序代码
	ShowWindow(SW_SHOW);
}

void CIrDAServiceDlg::OnPopupStartService()
{
	// TODO: 在此添加命令处理程序代码
	/*char strConfigFilePath[256] = {0};
	sprintf_s(strConfigFilePath, 256, "%sres\\config.ini", theApp.GetAppDir());*/
	CString strConfigFilePath = theApp.GetAppDir() + "res\\config.ini";
	USES_CONVERSION; 
	TCHAR strTemp[128] = {0};
	IrNetworkParam workParam;
	IrWorkModeParam workMode;

	memset(&workParam, 0, sizeof(IrNetworkParam));
	GetPrivateProfileString(_T("WIRELESS_INFO"), _T("AP_EN"), _T("0"), strTemp, 128, strConfigFilePath);

	if( _ttoi(strTemp) == 0)
	{
		workParam.APModeEn = 0;
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("SSID"), _T("IrayAP"), strTemp, 128, strConfigFilePath);
		strcpy_s(workParam.Client.SSID, 32, T2A(strTemp));
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("KEY"), _T("123456"), strTemp, 128, strConfigFilePath);
		strcpy_s(workParam.Client.Key, 32, T2A(strTemp));
	}
	else
	{
		workParam.APModeEn = 1;
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("AP_SSID"), _T("IrayAP"), strTemp, 128, strConfigFilePath);
		strcpy_s(workParam.Ap.SSID, 32, T2A(strTemp));
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("AP_KEY"), _T("123456"), strTemp, 128, strConfigFilePath);
		strcpy_s(workParam.Ap.Key, 32, T2A(strTemp));
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("COUNTRY"), _T("US"), strTemp, 128, strConfigFilePath);
		workParam.Ap.CountryCode[0] = T2A(strTemp)[0];
		workParam.Ap.CountryCode[1] = T2A(strTemp)[1];

		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("SECURITY"), _T("0"), strTemp, 128, strConfigFilePath);
		workParam.Ap.SecuritySel = _ttoi(strTemp);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("FREQUENCY"), _T("0"), strTemp, 128, strConfigFilePath);
		workParam.Ap.FrequencySel = _ttoi(strTemp);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("BAND"), _T("0"), strTemp, 128, strConfigFilePath);
		workParam.Ap.BandWidthSel = _ttoi(strTemp);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("CHANNEL"), _T("6"), strTemp, 128, strConfigFilePath);
		workParam.Ap.ChannelSel = _ttoi(strTemp);
	}
	GetPrivateProfileString(_T("CONFIG"), _T("DEST_IP_1"), _T(""), strTemp, 128, strConfigFilePath);
	strcpy_s(workParam.WorkIP, 32, T2A(strTemp));

	memset(&workMode, 0, sizeof(IrWorkModeParam));
	workMode.IrWaitingComfirmTimeout = WAITING_USER_COMFIRM_TIMEOUT;
	workMode.IrUpdateMode = IR_UPDATE_WIFI | IR_UPDATE_IP;
	switch (m_cboWorkMode.GetCurSel())
	{
	case 1:
		workMode.IrWorkMode = IR_READ;
		break;
	case 2:
		workMode.IrWorkMode = IR_READ | IR_WRITE_ALWAYS;
		break;
	case 3:
		workMode.IrWorkMode = IR_READ | IR_WRITE_COMFIRM_BY_USER;
		break;
	default:
		workMode.IrWorkMode = IR_WRITE_ALWAYS;
		break;
	}

	FPD_IrDASetWorkMode(workMode);
	FPD_IrDASetWiFiMode(workParam);
	FPD_IrDAStart(IrDAInfoCallback);

	m_btnApply.EnableWindow(false);
	m_txtSSID.EnableWindow(false);
	m_txtPWD.EnableWindow(false);
	m_cboSecurity.EnableWindow(false);
	m_cboCountryCode.EnableWindow(false);
	m_cboFrequency.EnableWindow(false);
	m_cboBandWidth.EnableWindow(false);
	m_cboChannel.EnableWindow(false);
	GetDlgItem(IDC_CHECK_AS_AP)->EnableWindow(false);
	m_cboWorkMode.EnableWindow(false);

	m_bServiceRun = TRUE;

	((CListBox*)GetDlgItem(IDC_LIST_RESULT))->ResetContent();
	m_nLogCount = 0;

	SetLogText(VERSION_STR);

	memset(&workParam, 0, sizeof(IrNetworkParam));
	FPD_IrDAGetWiFiMode(workParam);
	
	CString strInfo;

	if(workParam.APModeEn)
		strInfo.Format(_T("==[SSID: %s | PWD: %s]=="), A2T(workParam.Ap.SSID), A2T(workParam.Ap.Key));
	else
		strInfo.Format(_T("==[SSID: %s | PWD: %s]=="), A2T(workParam.Client.SSID), A2T(workParam.Client.Key));
	SetLogText(strInfo);
	strInfo.Format(_T("%s | I | Start"), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")));
	SetLogText(strInfo);

}

void CIrDAServiceDlg::OnPopupStopService()
{
	// TODO: 在此添加命令处理程序代码
	//m_pAdaptor.StopIrDA();
	FPD_IrDAStop();

	m_btnApply.EnableWindow(true);
	m_txtSSID.EnableWindow(true);
	m_txtPWD.EnableWindow(true);
	m_cboSecurity.EnableWindow(true);
	m_cboCountryCode.EnableWindow(true);
	m_cboFrequency.EnableWindow(true);
	m_cboBandWidth.EnableWindow(true);
	m_cboChannel.EnableWindow(true);
	GetDlgItem(IDC_CHECK_AS_AP)->EnableWindow(true);
	m_cboWorkMode.EnableWindow(true);

	m_bServiceRun = FALSE;

	CString strInfo;
	strInfo.Format(_T("%s | I | Stoped"), CTime::GetCurrentTime().Format(_T("%m.%d %H:%M:%S")));
	SetLogText(strInfo);
}

void CIrDAServiceDlg::OnBnClickedButtonStartStop()
{
	// TODO: 在此添加控件通知处理程序代码
	if(m_bServiceRun)
	{
		OnPopupStopService();
		SetDlgItemText(IDC_BUTTON_START_STOP, _T("Start"));
	}
	else
	{
		OnPopupStartService();
		SetDlgItemText(IDC_BUTTON_START_STOP, _T("Stop"));
	}
}

void CIrDAServiceDlg::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
	CDialog::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

	// TODO: 在此处添加消息处理程序代码

	if(m_bServiceRun)
	{
		pPopupMenu->EnableMenuItem(ID_POPUP_START_SERVICE, MF_GRAYED);
		pPopupMenu->EnableMenuItem(ID_POPUP_STOP_SERVICE, MF_ENABLED);
	}
	else
	{
		pPopupMenu->EnableMenuItem(ID_POPUP_START_SERVICE, MF_ENABLED);
		pPopupMenu->EnableMenuItem(ID_POPUP_STOP_SERVICE, MF_GRAYED);
	}
}

void CIrDAServiceDlg::SetLogText( CString strInfo )
{
	CListBox* lstCtrl = (CListBox*)GetDlgItem(IDC_LIST_RESULT);

	m_nLogCount++;
	if(m_nLogCount > 500)
	{
		lstCtrl->ResetContent();
		m_nLogCount = 1;
	}

	lstCtrl->AddString(strInfo);
	lstCtrl->SetCurSel(lstCtrl->GetCurSel()+1);

}

//////////////////////////////////////////////////////////////////////////

void CIrDAServiceDlg::OnBnClickedButtonSetting()
{
	// TODO: 在此添加控件通知处理程序代码
	if(m_bConfig)
	{
		m_btnApply.ShowWindow(SW_HIDE);
		m_txtSSID.ShowWindow(SW_HIDE);
		m_txtPWD.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_ESSID)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_PWD)->ShowWindow(SW_HIDE);
		m_cboSecurity.ShowWindow(SW_HIDE);
		m_cboCountryCode.ShowWindow(SW_HIDE);
		m_cboFrequency.ShowWindow(SW_HIDE);
		m_cboBandWidth.ShowWindow(SW_HIDE);
		m_cboChannel.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_SECURITY)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_COUNTRY)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_FREQUENCY)->ShowWindow(SW_HIDE);	
		GetDlgItem(IDC_STATIC_BAND)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_CHANNEL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_CHECK_AS_AP)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_WORKMODE)->ShowWindow(SW_HIDE);
		m_cboWorkMode.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_LIST_RESULT)->ShowWindow(SW_SHOW);	

		HICON hIcon = AfxGetApp()->LoadIcon(IDI_ICON_SETUP);			
		m_btnSetting.SetIcon(hIcon);
		m_bConfig = FALSE;
	}
	else
	{
		m_btnApply.ShowWindow(SW_SHOW);
		m_txtSSID.ShowWindow(SW_SHOW);
		m_txtPWD.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_ESSID)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_PWD)->ShowWindow(SW_SHOW);
		m_cboSecurity.ShowWindow(SW_SHOW);
		m_cboCountryCode.ShowWindow(SW_SHOW);
		m_cboFrequency.ShowWindow(SW_SHOW);
		m_cboBandWidth.ShowWindow(SW_SHOW);
		m_cboChannel.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_SECURITY)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_COUNTRY)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_FREQUENCY)->ShowWindow(SW_SHOW);	
		GetDlgItem(IDC_STATIC_BAND)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_CHANNEL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_CHECK_AS_AP)->ShowWindow(SW_SHOW);		
		GetDlgItem(IDC_STATIC_WORKMODE)->ShowWindow(SW_SHOW);
		m_cboWorkMode.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_LIST_RESULT)->ShowWindow(SW_HIDE);


		CString strConfigFilePath = theApp.GetAppDir() + "res\\config.ini";
		USES_CONVERSION; 
		TCHAR strTemp[128] = {0};

		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("AP_EN"), _T("0"), strTemp, 128, strConfigFilePath);

		if( _ttoi(strTemp) == 0)
		{
			((CButton*)GetDlgItem(IDC_CHECK_AS_AP))->SetCheck(0);
		}
		else
		{
			((CButton*)GetDlgItem(IDC_CHECK_AS_AP))->SetCheck(1);
		}
		OnBnClickedCheckAsAp();

		//////////////////////////////////////////////////////////////////////////

		if(m_bServiceRun)
		{
			m_btnApply.EnableWindow(false);
			m_txtSSID.EnableWindow(false);
			m_txtPWD.EnableWindow(false);
			m_cboSecurity.EnableWindow(false);
			m_cboCountryCode.EnableWindow(false);
			m_cboFrequency.EnableWindow(false);
			m_cboBandWidth.EnableWindow(false);
			m_cboChannel.EnableWindow(false);
			m_cboWorkMode.EnableWindow(false);
			GetDlgItem(IDC_CHECK_AS_AP)->EnableWindow(false);
		}

		HICON hIcon = AfxGetApp()->LoadIcon(IDI_ICON_LOG);			
		m_btnSetting.SetIcon(hIcon);
		m_bConfig = TRUE;
	}

}

void CIrDAServiceDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: 在此处添加消息处理程序代码
	if(m_bInitialized)
	{
		
	}
}

void CIrDAServiceDlg::OnBnClickedButtonApply()
{
	// TODO: 在此添加控件通知处理程序代码
	CString strConfigFilePath = theApp.GetAppDir() + "res\\config.ini";
	USES_CONVERSION; 
	CString strTemp;
	if(((CButton*)GetDlgItem(IDC_CHECK_AS_AP))->GetCheck())
	{
		m_txtPWD.GetWindowText(strTemp);
		if(strTemp.GetLength() < 8)
		{
			MessageBox(L"The length of password must be equal or greater than 8 character!", NULL, MB_ICONERROR);
			OnBnClickedCheckAsAp();
		}
		else if(m_cboChannel.GetCurSel() < 0)
		{
			MessageBox(L"Please select the correct WiFi channel in AP mode!", NULL, MB_ICONERROR);
			OnBnClickedCheckAsAp();
		}
		else
		{
			strTemp.Format(_T("%d"), 1);
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("AP_EN"), strTemp, strConfigFilePath);
			m_txtSSID.GetWindowText(strTemp);
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("AP_SSID"), strTemp, strConfigFilePath);
			m_txtPWD.GetWindowText(strTemp);
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("AP_KEY"), strTemp, strConfigFilePath);
			strTemp.Format(_T("%d"), m_cboSecurity.GetCurSel());
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("SECURITY"), strTemp, strConfigFilePath);
			strTemp.Format(_T("%d"), m_cboFrequency.GetCurSel());
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("FREQUENCY"), strTemp, strConfigFilePath);
			m_cboCountryCode.GetLBText(m_cboCountryCode.GetCurSel(), strTemp); 
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("COUNTRY"), strTemp, strConfigFilePath);
			strTemp.Format(_T("%d"), m_cboBandWidth.GetCurSel());
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("BAND"), strTemp, strConfigFilePath);
			m_cboChannel.GetLBText(m_cboChannel.GetCurSel(), strTemp);
			WritePrivateProfileString(_T("WIRELESS_INFO"), _T("CHANNEL"), strTemp, strConfigFilePath);
		}
	}
	else
	{
		strTemp.Format(_T("%d"), 0);
		WritePrivateProfileString(_T("WIRELESS_INFO"), _T("AP_EN"), strTemp, strConfigFilePath);
		m_txtSSID.GetWindowText(strTemp);
		WritePrivateProfileString(_T("WIRELESS_INFO"), _T("SSID"), strTemp, strConfigFilePath);
		m_txtPWD.GetWindowText(strTemp);
		WritePrivateProfileString(_T("WIRELESS_INFO"), _T("KEY"), strTemp, strConfigFilePath);
	}
}


void CIrDAServiceDlg::OnBnClickedCheckAsAp()
{
	// TODO: 在此添加控件通知处理程序代码

	CString strConfigFilePath = theApp.GetAppDir() + "res\\config.ini";
	USES_CONVERSION; 
	TCHAR strTemp[128] = {0};

	if(((CButton*)GetDlgItem(IDC_CHECK_AS_AP))->GetCheck())
	{
		m_cboSecurity.ShowWindow(SW_SHOW);
		m_cboCountryCode.ShowWindow(SW_SHOW);
		m_cboFrequency.ShowWindow(SW_SHOW);
		m_cboBandWidth.ShowWindow(SW_SHOW);
		m_cboChannel.ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_SECURITY)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_COUNTRY)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_FREQUENCY)->ShowWindow(SW_SHOW);	
		GetDlgItem(IDC_STATIC_BAND)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_CHANNEL)->ShowWindow(SW_SHOW);
		GetDlgItem(IDC_STATIC_WORKMODE)->ShowWindow(SW_SHOW);
		m_cboWorkMode.ShowWindow(SW_SHOW);

		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("AP_SSID"), _T("IrayAP"), strTemp, 128, strConfigFilePath);
		m_txtSSID.SetWindowText(strTemp);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("AP_KEY"), _T("123456"), strTemp, 128, strConfigFilePath);
		m_txtPWD.SetWindowText(strTemp);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("SECURITY"), _T("0"), strTemp, 128, strConfigFilePath);
		m_cboSecurity.SetCurSel(_ttoi(strTemp));
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("FREQUENCY"), _T("0"), strTemp, 128, strConfigFilePath);
		m_cboFrequency.SetCurSel(_ttoi(strTemp));
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("COUNTRY"), _T("US"), strTemp, 128, strConfigFilePath);
		if(wcscmp(strTemp, _T("CN")) == 0)
			m_cboCountryCode.SetCurSel(0);
		else if(wcscmp(strTemp, _T("DE")) == 0)
			m_cboCountryCode.SetCurSel(1);
		else if(wcscmp(strTemp, _T("FR")) == 0)
			m_cboCountryCode.SetCurSel(2);
		else if(wcscmp(strTemp, _T("GB")) == 0)
			m_cboCountryCode.SetCurSel(3);
		else if(wcscmp(strTemp, _T("HK")) == 0)
			m_cboCountryCode.SetCurSel(4);
		else if(wcscmp(strTemp, _T("IT")) == 0)
			m_cboCountryCode.SetCurSel(5);
		else if(wcscmp(strTemp, _T("KR")) == 0)
			m_cboCountryCode.SetCurSel(6);
		else if(wcscmp(strTemp, _T("NL")) == 0)
			m_cboCountryCode.SetCurSel(7);
		else if(wcscmp(strTemp, _T("RU")) == 0)
			m_cboCountryCode.SetCurSel(8);
		else if(wcscmp(strTemp, _T("US")) == 0)
			m_cboCountryCode.SetCurSel(9);
		else
			m_cboCountryCode.SetCurSel(-1);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("BAND"), _T("0"), strTemp, 128, strConfigFilePath);
		m_cboBandWidth.SetCurSel(_ttoi(strTemp));

		m_nCountry = -1; m_nFrequency = -1; m_nBand = -1;
		OnCbnSelchangeComboApFrequencyCountryBand();

		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("CHANNEL"), _T("6"), strTemp, 128, strConfigFilePath);
		SetSelectedApChannel(_ttoi(strTemp));
	}
	else
	{
		m_cboSecurity.ShowWindow(SW_HIDE);
		m_cboCountryCode.ShowWindow(SW_HIDE);
		m_cboFrequency.ShowWindow(SW_HIDE);
		m_cboBandWidth.ShowWindow(SW_HIDE);
		m_cboChannel.ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_SECURITY)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_COUNTRY)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_FREQUENCY)->ShowWindow(SW_HIDE);	
		GetDlgItem(IDC_STATIC_BAND)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_CHANNEL)->ShowWindow(SW_HIDE);
		GetDlgItem(IDC_STATIC_WORKMODE)->ShowWindow(SW_SHOW);
		m_cboWorkMode.ShowWindow(SW_SHOW);

		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("SSID"), _T("IrayAP"), strTemp, 128, strConfigFilePath);
		m_txtSSID.SetWindowText(strTemp);
		GetPrivateProfileString(_T("WIRELESS_INFO"), _T("KEY"), _T("123456"), strTemp, 128, strConfigFilePath);
		m_txtPWD.SetWindowText(strTemp);
	}
}

void CIrDAServiceDlg::OnCbnSelchangeComboApFrequencyCountryBand()
{
	// TODO: 在此添加控件通知处理程序代码
	int nCountry =  m_cboCountryCode.GetCurSel();
	int nFrequency = m_cboFrequency.GetCurSel();
	int nBand = m_cboBandWidth.GetCurSel();

	if(nCountry < 0 || nFrequency < 0 || nBand < 0)
		return;

	if(nCountry == m_nCountry && nFrequency == m_nFrequency && nBand == m_nBand)
		return;

	m_nCountry = nCountry;
	m_nFrequency = nFrequency;
	m_nBand = nBand;

	m_cboChannel.ResetContent();
	int nIndex, nChnlCnt = 0;
	for(int i=0; i<CHANNEL_TABLE_LEN; i++)
	{
		if(Channel_Def[i].CountryCodeIndex == m_nCountry && 
			Channel_Def[i].Frequency == m_nFrequency && 
			Channel_Def[i].BandWidth == m_nBand){  
				nIndex = i;		
				break;
		}
	}
	CString strTmp;
	for(int i=0; i<MAX_CHANNEL; i++)
	{
		if(Channel_Def[nIndex].Channel[i]>0)
			strTmp.Format(_T("%d"),Channel_Def[nIndex].Channel[i]);
		else
			break;
		m_cboChannel.AddString(strTmp);
	}
	m_cboChannel.SetCurSel(0);

}

void CIrDAServiceDlg::SetSelectedApChannel(int nChannelSel)
{
	// TODO: 在此添加控件通知处理程序代码
	int nCountry =  m_cboCountryCode.GetCurSel();
	int nFrequency = m_cboFrequency.GetCurSel();
	int nBand = m_cboBandWidth.GetCurSel();

	if(nCountry < 0 || nFrequency < 0 || nBand < 0)
		return;

	int nIndex, nChnlCnt = 0;
	for(int i=0; i<CHANNEL_TABLE_LEN; i++)
	{
		if(Channel_Def[i].CountryCodeIndex == nCountry && 
			Channel_Def[i].Frequency == nFrequency && 
			Channel_Def[i].BandWidth == nBand){  
				nIndex = i;		
				break;
		}
	}
	CString strTmp;
	for(int i=0; i<MAX_CHANNEL; i++)
	{
		if(Channel_Def[nIndex].Channel[i]>0)
		{
			if(Channel_Def[nIndex].Channel[i] == nChannelSel)
			{
				m_cboChannel.SetCurSel(i);
				break;
			}	
		}
		else
			break;
	}
}

LRESULT CIrDAServiceDlg::OnIrdaComfirmDisplay( WPARAM wParam, LPARAM lParam )
{

	if(m_nTimer != 0)
		return 0;
	
	m_nTimer = SetTimer(100, WAITING_USER_COMFIRM_TIMEOUT, NULL);
	
	m_hComfirmDlg.SetElapseTime(WAITING_USER_COMFIRM_TIMEOUT);
	m_hComfirmDlg.DoModal();
	
	if(m_nTimer != 0)
	{
		if(m_hComfirmDlg.ComfirmResult == true)
		{
			FPD_IrDAUpdateOnce(IR_UPDATE_WIFI | IR_UPDATE_IP);
		}
		KillTimer(m_nTimer);
		m_nTimer = 0;
	}
	return 1;
}
void CIrDAServiceDlg::OnTimer(UINT_PTR nIDEvent)
{
	
	if(nIDEvent == m_nTimer)
	{
		m_nTimer = 0;
		
		m_hComfirmDlg.CloseDlg();
	}
	KillTimer(nIDEvent);

	CDialog::OnTimer(nIDEvent);
}
