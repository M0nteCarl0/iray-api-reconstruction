
// IrDARegister.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "IrDARegister.h"
#include "IrDAServiceDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CIrDARegisterApp

BEGIN_MESSAGE_MAP(CIrDARegisterApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CIrDARegisterApp construction

CIrDARegisterApp::CIrDARegisterApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CIrDARegisterApp object

CIrDARegisterApp theApp;


// CIrDARegisterApp initialization

BOOL CIrDARegisterApp::InitInstance()
{
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	// Create the tray icon
	if (!m_TrayIcon.Create(WM_ICON_NOTIFY,			// Icon notify message to use
		_T("Quick'n IrDA Regist Service"),		// tooltip
		LoadIcon(IDI_IRDA),		// Icon
		IDR_POPUP_MENU))				// ID of tray icon
		return -1;

	// ******************** start ********************** //
	// get App Dir.
	TCHAR sDrive[_MAX_DRIVE],sDir[_MAX_DIR],sFilename[_MAX_FNAME],Filename[_MAX_FNAME],sExt[_MAX_EXT];
	GetModuleFileName(AfxGetInstanceHandle(), Filename, _MAX_PATH);   
	_tsplitpath(Filename, sDrive, sDir, sFilename, sExt);   
	CString homeDir(CString(sDrive) + CString(sDir));   
	int nLen = homeDir.GetLength();   
	if(homeDir.GetAt(nLen-1) != _T('\\')) homeDir += _T('\\');
	m_strAppDir = homeDir;


	CIrDAServiceDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


CString CIrDARegisterApp::GetAppDir()
{
	return m_strAppDir;
}