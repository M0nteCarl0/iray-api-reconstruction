#pragma once

#include "ComfirmDlg.h"
// CIrDAServiceDlg 对话框

#define MAX_CHANNEL		24

typedef struct
{
	int CountryCodeIndex;
	CHAR CountryCode[2];
	int Frequency;	// 0:2.4GHz; 1:5GHz
	int BandWidth;	// 0:HT20; 1:HT40+; 2:HT40-
	int Channel[MAX_CHANNEL];
}CHANNEL_TABLE, *PCHANNEL_TABLE;



class CIrDAServiceDlg : public CDialog
{
	DECLARE_DYNAMIC(CIrDAServiceDlg)

public:
	CIrDAServiceDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CIrDAServiceDlg();

// 对话框数据
	enum { IDD = IDD_DIALOG_IRDA_SERVICE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPopupShutdown();
	afx_msg void OnPopupShowWindow();
	afx_msg void OnPopupStartService();
	afx_msg void OnPopupStopService();
	afx_msg void OnBnClickedButtonStartStop();
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg LRESULT OnIrdaComfirmDisplay(WPARAM wParam, LPARAM lParam);

private:
	BOOL	m_bServiceRun;
	BOOL	m_bInitialized;
	BOOL	m_bConfig;

	CButton m_btnSetting;
	CButton m_btnApply;
	CEdit	m_txtSSID;
	CEdit	m_txtPWD;

	CComboBox m_cboSecurity;
	CComboBox m_cboCountryCode;
	CComboBox m_cboFrequency;
	CComboBox m_cboBandWidth;
	CComboBox m_cboChannel;
	CComboBox m_cboWorkMode;

	int m_nCountry;
	int m_nFrequency;
	int m_nBand;

	int m_nLogCount;

	int m_nTimer;
	CComfirmDlg m_hComfirmDlg;

public:
	//CFpdIrDARegister m_pAdaptor;

	void SetLogText(CString strInfo);

//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
//	virtual BOOL Create(LPCTSTR lpszTemplateName, CWnd* pParentWnd = NULL);
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonSetting();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnBnClickedButtonApply();
	afx_msg void OnBnClickedCheckAsAp();
	afx_msg void OnCbnSelchangeComboApFrequencyCountryBand();
	void SetSelectedApChannel(int nChannelSel);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	int GetIrdaWorkMode() { return m_cboWorkMode.GetCurSel(); };
};

