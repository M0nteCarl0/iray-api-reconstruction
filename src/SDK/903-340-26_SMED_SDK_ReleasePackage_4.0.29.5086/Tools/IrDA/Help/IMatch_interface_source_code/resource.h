//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by IrDARegister.rc
//
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_IRDA_SERVICE         129
#define IDR_POPUP_MENU                  130
#define IDI_IRDA                        131
#define IDI_ICON_SETUP                  132
#define IDI_ICON_LOG                    135
#define IDD_DIALOG_COMFIRM_UPDATE       137
#define IDC_LIST_RESULT                 1003
#define IDC_BUTTON_START_STOP           1004
#define IDC_BUTTON_EXIT                 1005
#define IDC_BUTTON_SETTING              1006
#define IDC_EDIT_SSID                   1008
#define IDC_EDIT_PWD                    1009
#define IDC_BUTTON_APPLY                1010
#define IDC_STATIC_PORPPAGE             1013
#define IDC_STATIC_PWD                  1014
#define IDC_STATIC_ESSID                1015
#define IDC_CHECK1                      1016
#define IDC_CHECK_AS_AP                 1016
#define IDC_COMBO_AP_SECURITY           1017
#define IDC_COMBO_AP_FREQUENCY          1018
#define IDC_COMBO_AP_COUNTRY            1019
#define IDC_COMBO_AP_BAND               1020
#define IDC_COMBO_AP_CHANNEL_SEL        1021
#define IDC_STATIC_CHANNEL              1022
#define IDC_STATIC_BAND                 1023
#define IDC_STATIC_COUNTRY              1024
#define IDC_STATIC_FREQUENCY            1025
#define IDC_STATIC_SECURITY             1026
#define IDC_CBO_WORKMODE                1027
#define IDC_BUTTON_YES                  1028
#define IDC_BUTTON_NO                   1029
#define ID_POPUP_STARTSERVER            32771
#define ID_POPUP_STOPSERVER             32772
#define ID_POPUP_SHUTDOWN               32773
#define ID_Menu                         32774
#define ID_POPUP_ABOUT                  32775
#define ID_POPUP_SHOWWINDOW             32776
#define ID_POPUP_SHOW_WINDOW            32777
#define ID_POPUP_START_SERVICE          32778
#define ID_POPUP_STOP_SERVICE           32779
#define IDC_STATIC_WORKMODE             -1
#define IDC_STATIC_INFO                 -1
#define IDC_STATIC_COMFIRM_INFO         -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        138
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
