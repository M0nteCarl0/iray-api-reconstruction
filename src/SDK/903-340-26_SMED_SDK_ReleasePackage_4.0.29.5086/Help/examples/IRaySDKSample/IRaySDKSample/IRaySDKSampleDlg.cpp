
// IRaySDKSampleDlg.cpp : implementation file
//

#include "stdafx.h"
#include "IRaySDKSample.h"
#include "IRaySDKSampleDlg.h"
#include "afxdialogex.h"
#include "Detector.h"
#include "IRayEventDef.h"
#include "Util.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define MONITOR_STATUS_ID	1
#define WM_UPDATE_ELEMENT 	(WM_USER + 100)
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CIRaySDKSampleDlg dialog



CIRaySDKSampleDlg::CIRaySDKSampleDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CIRaySDKSampleDlg::IDD, pParent)
	, m_strWorkDir(_T(""))
	, m_pDetector(NULL)
	, m_SDKStatus(_T(""))
	, m_bConnected(false)
	, m_Temperature(_T(""))
	, m_CorrOption(_T(""))
	, m_strTrigMode(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CIRaySDKSampleDlg::~CIRaySDKSampleDlg()
{
	DestoryDetector();
}

void CIRaySDKSampleDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_strWorkDir);
	DDX_Control(pDX, IDC_LIST1, m_EvtMsgList);
	DDX_Text(pDX, IDC_EDIT2, m_SDKStatus);
	DDX_Text(pDX, IDC_EDIT3, m_Temperature);
	DDX_Control(pDX, IDC_EDIT2, m_sdkStatusControl);
	DDX_Text(pDX, IDC_EDIT4, m_CorrOption);
	DDX_Text(pDX, IDC_EDIT_TRIGGERMODE, m_strTrigMode);
}

BEGIN_MESSAGE_MAP(CIRaySDKSampleDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	ON_MESSAGE(WM_UPDATE_ELEMENT, &CIRaySDKSampleDlg::OnUpdateUIInfo)
	ON_BN_CLICKED(IDC_BUTTON1, &CIRaySDKSampleDlg::OnSelectWorkDir)
	ON_BN_CLICKED(IDC_BUTTON_CREATE, &CIRaySDKSampleDlg::OnCreateDetector)
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &CIRaySDKSampleDlg::OnConnect)
	ON_BN_CLICKED(IDC_BUTTON_DISCONNECT, &CIRaySDKSampleDlg::OnDisconnect)
	ON_BN_CLICKED(IDC_BUTTON_DESTORY, &CIRaySDKSampleDlg::OnDestoryDetector)
	ON_BN_CLICKED(IDC_BUTTON_STARTACQ, &CIRaySDKSampleDlg::OnStartAcq)
	ON_BN_CLICKED(IDC_BUTTON_STOPACQ, &CIRaySDKSampleDlg::OnStopAcq)
	ON_EN_CHANGE(IDC_EDIT1, &CIRaySDKSampleDlg::OnEnChangeWorkDir)
	ON_BN_CLICKED(IDC_READ_TEMPERATURE, &CIRaySDKSampleDlg::OnBnClickedReadTemperature)
	ON_BN_CLICKED(IDC_BUTTON2, &CIRaySDKSampleDlg::OnSetCorrectionOption)
	ON_BN_CLICKED(IDC_RADIO1, &CIRaySDKSampleDlg::OnBnPresetSWPre)
	ON_BN_CLICKED(IDC_RADIO2, &CIRaySDKSampleDlg::OnBnPresetSWPost)
	ON_BN_CLICKED(IDC_RADIO3, &CIRaySDKSampleDlg::OnBnPresetHWPre)
	ON_BN_CLICKED(IDC_RADIO4, &CIRaySDKSampleDlg::OnBnPresetHWPost)
	ON_BN_CLICKED(IDC_RADIO6, &CIRaySDKSampleDlg::OnBnPresetSWGain)
	ON_BN_CLICKED(IDC_RADIO7, &CIRaySDKSampleDlg::OnBnPresetHWGain)
	ON_BN_CLICKED(IDC_RADIO8, &CIRaySDKSampleDlg::OnBnPresetGainNone)
	ON_BN_CLICKED(IDC_RADIO5, &CIRaySDKSampleDlg::OnBnPresetOffsetNone)
	ON_BN_CLICKED(IDC_RADIO9, &CIRaySDKSampleDlg::OnBnPresetSWDefect)
	ON_BN_CLICKED(IDC_RADIO10, &CIRaySDKSampleDlg::OnBnPresetHWDefect)
	ON_BN_CLICKED(IDC_RADIO11, &CIRaySDKSampleDlg::OnBnPresetDefectNone)
	ON_BN_CLICKED(IDC_BUTTON_PREPACQ, &CIRaySDKSampleDlg::OnPrepAcq)
END_MESSAGE_MAP()


// CIRaySDKSampleDlg message handlers

BOOL CIRaySDKSampleDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	InitAllButtonStatus(false);
	SetTriggerModeVisible(false);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CIRaySDKSampleDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CIRaySDKSampleDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CIRaySDKSampleDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

static wchar_t *DetStates[] = { _T("Unknown"), _T("Ready"), _T("Busy"), _T("Sleeping") };
void CIRaySDKSampleDlg::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent)
	{
	case MONITOR_STATUS_ID:
		if (m_pDetector)
		{
			AttrResult attr;
			m_pDetector->GetAttr(Attr_State, attr);
			m_SDKStatus.Format(_T("%s"), DetStates[attr.nVal]);
			UpdateData(FALSE);
		}
		break;
	default:
		break;
	}
}

void SaveFile(const void* pData, unsigned size)
{
	if (!pData || (0 == size)) return;
	HANDLE hFile = CreateFile(L"image.raw", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	DWORD nWriten = 0;
	WriteFile(hFile, pData, size, &nWriten, NULL);
	CloseHandle(hFile);
}

void CIRaySDKSampleDlg::SDKHandler(int nDetectorID, int nEventID, int nEventLevel,
	const char* pszMsg, int nParam1, int nParam2, int nPtrParamLen, void* pParam)
{
	CString message;
	message.Format(_T("EventID:%d - %s"), nEventID, CUtil::_atow(pszMsg).c_str());
	UpdateListBoxMessage(message);
	switch (nEventID)
	{
		case Evt_TaskResult_Succeed:
		{
			switch (nParam1)
			{
				case Cmd_Connect:
					SetButtonStatusAfterConnected(true);
					UpdatetriggerMode();
					break;
				case Cmd_Disconnect:
					EnableElement(IDC_BUTTON_CONNECT, true);
					SetButtonStatusAfterConnected(false);
					break;
				case Cmd_ReadTemperature:
				{
					AttrResult attr;
					m_pDetector->GetAttr(Attr_RdResult_T2, attr);
					m_Temperature.Format(_T("%0.1f"), attr.fVal);
					PostMessage(WM_UPDATE_ELEMENT, 0, NULL);
					break;
				}
				default:
					break;
			}
			break;
		}
		case Evt_TaskResult_Failed:
			switch (nParam1)
			{
				case Cmd_Connect:
				{
					string errorInfo = m_pDetector->GetErrorInfo(nParam2);
					SetButtonStatusAfterConnected(false);
					break;
				}
				case Cmd_Disconnect:
					break;
				default:
					break;
			}
			break;
		case Evt_Image:
		{
			IRayImage* pImg = (IRayImage*)pParam;
			const unsigned short* pImageData = pImg->pData;
			int nImageWidth = pImg->nWidth;
			int nImageHeight = pImg->nHeight;
			int nImageSize = nImageWidth * nImageHeight * pImg->nBytesPerPixel;
			//SaveFile(pImageData, nImageSize);
			break;
		}
		default:
			break;
	}
}

void CIRaySDKSampleDlg::UpdateListBoxMessage(CString message)
{
	m_EvtMsgList.AddString(message);
	m_EvtMsgList.SetCurSel(m_EvtMsgList.GetCount() - 1);
}

void CIRaySDKSampleDlg::OnSelectWorkDir()
{
	TCHAR szDir[MAX_PATH];
	BROWSEINFO bi;
	ITEMIDLIST *pidl;
	bi.hwndOwner = this->m_hWnd;
	bi.pidlRoot = NULL;
	bi.pszDisplayName = szDir;  
	bi.lpszTitle = _T("Select workdir");  
	bi.ulFlags = BIF_NEWDIALOGSTYLE;   
	bi.lpfn = NULL;
	bi.lParam = 0;
	bi.iImage = 0;
	pidl = SHBrowseForFolder(&bi);
	if (pidl == NULL)  
		return;
	if (SHGetPathFromIDList(pidl, szDir))
	{
		m_strWorkDir = szDir;
	}
	UpdateData(FALSE);
}

void CIRaySDKSampleDlg::OnCreateDetector()
{
	if (NULL == m_pDetector)
	{
		m_pDetector = CDetector::CreateDetector(CUtil::_wtoa(m_strWorkDir.GetString()).c_str());
		if (m_pDetector)
		{
			SetTimer(MONITOR_STATUS_ID, 200, NULL);
			m_bConnected = true;
			m_pDetector->RegisterHandler(this);
			UpdateListBoxMessage(_T("Cerate detector succeed!"));
		
			EnableElement(IDC_BUTTON_CREATE, false);
			EnableElement(IDC_BUTTON_CONNECT, true);
			EnableElement(IDC_BUTTON_DESTORY, true);
		}
		else
		{
			MessageBox(_T("Cerate detector failed!"));
		}
	}
}


void CIRaySDKSampleDlg::OnConnect()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->Connect();
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("Connect failed"));
	}
	else
	{
		EnableElement(IDC_BUTTON_CONNECT, false);
	}
}

void CIRaySDKSampleDlg::OnDisconnect()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->Disconnect();
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("Disconnect failed"));
	}
	else
	{
		EnableElement(IDC_BUTTON_DISCONNECT, false);
	}
}


void CIRaySDKSampleDlg::OnDestoryDetector()
{
	DestoryDetector();
	KillTimer(MONITOR_STATUS_ID);
	InitAllButtonStatus(false);
}

void CIRaySDKSampleDlg::DestoryDetector()
{
	if (NULL == m_pDetector)
		return;
	m_pDetector->UnRegisterHandler(this);
	CDetector::DestoryDetector(m_pDetector->DetectorID());
	m_pDetector = NULL;
}

void CIRaySDKSampleDlg::OnStartAcq()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->StartAcquire();
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("StartAcquire failed"));
	}
}

void CIRaySDKSampleDlg::OnStopAcq()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->StopAcquire();
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("StopAcquire failed"));
	}
}

void CIRaySDKSampleDlg::OnPrepAcq()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->PrepAcquire();
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("PrepAcquire failed"));
	}
}


void CIRaySDKSampleDlg::OnEnChangeWorkDir()
{
	UpdateData(TRUE);
	CString dd = m_strWorkDir;
}

void CIRaySDKSampleDlg::OnBnClickedReadTemperature()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->ReadTemperature();
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("StartAcquire failed"));
	}
}

void CIRaySDKSampleDlg::InitAllButtonStatus(bool bValue)
{
	EnableElement(IDC_BUTTON_CREATE, !bValue);
	EnableElement(IDC_BUTTON_CONNECT, bValue);
	EnableElement(IDC_BUTTON_DISCONNECT, bValue);
	EnableElement(IDC_BUTTON_DESTORY, bValue);
	EnableElement(IDC_BUTTON_STARTACQ, bValue);
	EnableElement(IDC_BUTTON_PREPACQ, bValue);
	EnableElement(IDC_BUTTON_STOPACQ, bValue);
	EnableElement(IDC_BUTTON_SETCORROPTION, bValue);
	EnableElement(IDC_READ_TEMPERATURE, bValue);
}

void CIRaySDKSampleDlg::SetButtonStatusAfterConnected(bool bSucceed)
{
	EnableElement(IDC_BUTTON_DISCONNECT, bSucceed);
	EnableElement(IDC_BUTTON_STARTACQ, bSucceed);
	EnableElement(IDC_BUTTON_PREPACQ, bSucceed);
	EnableElement(IDC_BUTTON_SETCORROPTION, bSucceed);
	EnableElement(IDC_BUTTON_STOPACQ, bSucceed);
	EnableElement(IDC_READ_TEMPERATURE, bSucceed);
	EnableElement(IDC_BUTTON_CONNECT, !bSucceed);
}

void CIRaySDKSampleDlg::EnableElement(int IDC, bool bEnable)
{
	GetDlgItem(IDC)->EnableWindow(bEnable);
}

LRESULT CIRaySDKSampleDlg::OnUpdateUIInfo(WPARAM wParam, LPARAM lParam)
{
	UpdateData(FALSE);
	return 0;
}

void CIRaySDKSampleDlg::OnSetCorrectionOption()
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->SetCorrectionOption(m_nCorrectionOption);
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		MessageBox(_T("SetCorrectionOption failed"));
	}
}


void CIRaySDKSampleDlg::OnBnPresetSWPre()
{
	m_nCorrectionOption &= ~CDetector::OFFSETMASK;
	m_nCorrectionOption |= Enm_CorrectOp_SW_PreOffset;
	UpdateCorrectionOption();
}

void CIRaySDKSampleDlg::OnBnPresetSWPost()
{
	m_nCorrectionOption &= ~CDetector::OFFSETMASK;
	m_nCorrectionOption |= Enm_CorrectOp_SW_PostOffset;
	UpdateCorrectionOption();
}


void CIRaySDKSampleDlg::OnBnPresetHWPre()
{
	m_nCorrectionOption &= ~CDetector::OFFSETMASK;
	m_nCorrectionOption |= Enm_CorrectOp_HW_PreOffset;
	UpdateCorrectionOption();
}

void CIRaySDKSampleDlg::OnBnPresetHWPost()
{
	m_nCorrectionOption &= ~CDetector::OFFSETMASK;
	m_nCorrectionOption |= Enm_CorrectOp_HW_PostOffset;
	UpdateCorrectionOption();
}

void CIRaySDKSampleDlg::OnBnPresetOffsetNone()
{
	m_nCorrectionOption &= ~CDetector::OFFSETMASK;
	UpdateCorrectionOption();
}

void CIRaySDKSampleDlg::OnBnPresetSWGain()
{
	m_nCorrectionOption &= ~CDetector::GAINMASK;
	m_nCorrectionOption |= Enm_CorrectOp_SW_Gain;
	UpdateCorrectionOption();
}


void CIRaySDKSampleDlg::OnBnPresetHWGain()
{
	m_nCorrectionOption &= ~CDetector::GAINMASK;
	m_nCorrectionOption |= Enm_CorrectOp_HW_Gain;
	UpdateCorrectionOption();
}


void CIRaySDKSampleDlg::OnBnPresetGainNone()
{
	m_nCorrectionOption &= ~CDetector::GAINMASK;
	UpdateCorrectionOption();
}


void CIRaySDKSampleDlg::OnBnPresetSWDefect()
{
	m_nCorrectionOption &= ~CDetector::DEFECTMASK;
	m_nCorrectionOption |= Enm_CorrectOp_SW_Defect;
	UpdateCorrectionOption();
}


void CIRaySDKSampleDlg::OnBnPresetHWDefect()
{
	m_nCorrectionOption &= ~CDetector::DEFECTMASK;
	m_nCorrectionOption |= Enm_CorrectOp_HW_Defect;
	UpdateCorrectionOption();
}


void CIRaySDKSampleDlg::OnBnPresetDefectNone()
{
	m_nCorrectionOption &= ~CDetector::DEFECTMASK;
	UpdateCorrectionOption();
}

void CIRaySDKSampleDlg::UpdateCorrectionOption()
{
	m_CorrOption.Format(_T("0x%08x"), m_nCorrectionOption);
	UpdateData(FALSE);
}

CString CIRaySDKSampleDlg::GetErrorMessage(int nErrorCode)
{
	CString errInfo;
	errInfo.Format(_T("DynamicDet_SetSyncMode failed! Err=%s"), m_pDetector->GetErrorInfo(nErrorCode).c_str());
	return errInfo;
}

void CIRaySDKSampleDlg::SetTriggerModeVisible(bool bVisible)
{
	int nVisible = bVisible ? SW_SHOW : SW_HIDE;
	GetDlgItem(IDC_EDIT_TRIGGERMODE)->ShowWindow(nVisible);
	GetDlgItem(IDC_STATIC_TRIGGERMODE)->ShowWindow(nVisible);
}

void CIRaySDKSampleDlg::UpdatetriggerMode()
{
	SetTriggerModeVisible(true);
	AttrResult attr;
	m_pDetector->GetAttr(Attr_UROM_ProductNo, attr);
	int nProductNo = attr.nVal;
	if (Enm_Prd_Mercu0909F == nProductNo)
	{
		CString syncMode[] = { _T("FreeRun"), _T("SyncIn"), _T("SyncOut") };
		GetDlgItem(IDC_STATIC_TRIGGERMODE)->SetWindowText(_T("SyncMode:"));
		m_pDetector->GetAttr(Attr_UROM_FluroSync, attr);
		if (attr.nVal > Enm_FluroSync_SyncOut)
		{
			m_strTrigMode = _T("Unknown");
		}
		else
		{
			m_strTrigMode = syncMode[attr.nVal];
		}
	}
	else
	{
		CString trigMode[] = { _T("Outer"), _T("Inner"), _T("Soft"),_T("Prep"), _T("Service"), _T("FreeSync") };
		GetDlgItem(IDC_STATIC_TRIGGERMODE)->SetWindowText(_T("TrigMode:"));
		m_pDetector->GetAttr(Attr_UROM_TriggerMode, attr);
		if (attr.nVal > Enm_TriggerMode_FreeSync)
		{
			m_strTrigMode = _T("Unknown");
		}
		else
		{
			m_strTrigMode = trigMode[attr.nVal];
		}
	}
	PostMessage(WM_UPDATE_ELEMENT, 0, NULL);
}


void CIRaySDKSampleDlg::Test()
{
	//open only one operation
	DynamicDet_SetApplicationMode("Mode1");
	//DynamicDet_SetSyncMode(Enm_FluroSync_SyncOut);
	//StaticDet_SetTriggerMode(Enm_TriggerMode_Soft);
}


void CIRaySDKSampleDlg::DynamicDet_SetApplicationMode(const char* strMode)
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->Invoke(Cmd_SetCaliSubset, strMode);
	if ((Err_TaskPending != ret) && (Err_OK != ret))
	{
		CString errInfo;
		errInfo.Format(_T("SetDynamicApplicationMode failed! Err=%s"), m_pDetector->GetErrorInfo(ret).c_str());
		MessageBox(errInfo);
	}
}

void CIRaySDKSampleDlg::DynamicDet_SetSyncMode(Enm_FluroSync eSyncMode)
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->SetAttr(Attr_UROM_FluroSync_W, eSyncMode);
	if (Err_OK != ret)
		return;
	ret = m_pDetector->Invoke(Cmd_WriteUserROM);
	if (Err_OK != ret && Err_TaskPending != ret)
	{
		MessageBox(GetErrorMessage(ret));
	}
}

void CIRaySDKSampleDlg::StaticDet_SetTriggerMode(Enm_TriggerMode eTriggerMode)
{
	if (NULL == m_pDetector)
		return;
	FPDRESULT ret = m_pDetector->SetAttr(Attr_UROM_TriggerMode_W, eTriggerMode);
	if (Err_OK != ret)
		return;
	ret = m_pDetector->Invoke(Cmd_WriteUserROM);
	if (Err_OK != ret && Err_TaskPending != ret)
	{
		CString errInfo;
		errInfo.Format(_T("StaticDet_SetTriggerMode failed! Err=%s"), m_pDetector->GetErrorInfo(ret).c_str());
		MessageBox(errInfo);
	}
}

