
// IRaySDKSampleDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "EventReceiver.h"
#include "IRayEnumDef.h"

// CIRaySDKSampleDlg dialog
class CDetector;
class CIRaySDKSampleDlg : public CDialogEx, public iEventReceiver
{
// Construction
public:
	CIRaySDKSampleDlg(CWnd* pParent = NULL);	// standard constructor
	~CIRaySDKSampleDlg();

// Dialog Data
	enum { IDD = IDD_IRAYSDKSAMPLE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	virtual void SDKHandler(int nDetectorID, int nEventID, int nEventLevel,
		const char* pszMsg, int nParam1, int nParam2, int nPtrParamLen, void* pParam);
	CString m_strWorkDir;
	afx_msg void OnSelectWorkDir();
	afx_msg void OnCreateDetector();
	afx_msg void OnConnect();
	afx_msg void OnDisconnect();
	afx_msg void OnDestoryDetector();
	afx_msg void OnStartAcq();
	afx_msg void OnStopAcq();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg LRESULT OnUpdateUIInfo(WPARAM wParam, LPARAM lParam);
	void UpdateListBoxMessage(CString message);
	void DestoryDetector();
	void EnableElement(int IDC, bool bEnable);
	void InitAllButtonStatus(bool bValue);
	void SetButtonStatusAfterConnected(bool bSucceed);
	CListBox m_EvtMsgList;
	CDetector* m_pDetector;
	CString m_SDKStatus;
	bool m_bConnected;
	afx_msg void OnEnChangeWorkDir();
	CString m_Temperature;
	afx_msg void OnBnClickedReadTemperature();
	CEdit m_sdkStatusControl;
	CString m_CorrOption;
	afx_msg void OnSetCorrectionOption();
	afx_msg void OnBnPresetSWPre();
	afx_msg void OnBnPresetSWPost();
	afx_msg void OnBnPresetHWPre();
	afx_msg void OnBnPresetHWPost();
	afx_msg void OnBnPresetSWGain();
	afx_msg void OnBnPresetHWGain();
	afx_msg void OnBnPresetGainNone();
	afx_msg void OnBnPresetOffsetNone();
	afx_msg void OnBnPresetSWDefect();
	afx_msg void OnBnPresetHWDefect();
	afx_msg void OnBnPresetDefectNone();
	int m_nCorrectionOption;
	void UpdateCorrectionOption();
	CString GetErrorMessage(int nErrorCode);
	void SetTriggerModeVisible(bool bEnable);
	void UpdatetriggerMode();

	//test
	void Test();
	void DynamicDet_SetApplicationMode(const char* strMode);
	void DynamicDet_SetSyncMode(Enm_FluroSync eSyncMode);
	void StaticDet_SetTriggerMode(Enm_TriggerMode eTriggerMode);
	afx_msg void OnPrepAcq();
	CString m_strTrigMode;
};
