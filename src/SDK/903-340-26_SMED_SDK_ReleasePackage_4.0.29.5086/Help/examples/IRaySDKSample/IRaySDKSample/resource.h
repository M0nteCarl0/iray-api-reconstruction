//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by IRaySDKSample.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_IRAYSDKSAMPLE_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDC_LIST1                       1000
#define IDC_EDIT1                       1003
#define IDC_BUTTON1                     1004
#define IDC_BUTTON_CREATE               1005
#define IDC_BUTTON_CONNECT              1006
#define IDC_BUTTON_DISCONNECT           1007
#define IDC_BUTTON_DESTORY              1008
#define IDC_BUTTON_STARTACQ             1015
#define IDC_BUTTON_STOPACQ              1016
#define IDC_EDIT2                       1017
#define IDC_EDIT3                       1020
#define IDC_READ_TEMPERATURE            1021
#define IDC_RADIO2                      1023
#define IDC_RADIO5                      1024
#define IDC_RADIO6                      1025
#define IDC_RADIO8                      1026
#define IDC_RADIO9                      1028
#define IDC_RADIO11                     1029
#define IDC_RADIO1                      1031
#define IDC_RADIO3                      1032
#define IDC_RADIO4                      1033
#define IDC_RADIO7                      1034
#define IDC_RADIO10                     1035
#define IDC_STATIC10                    1037
#define IDC_STATIC11                    1038
#define IDC_STATIC13                    1040
#define IDC_EDIT4                       1041
#define IDC_BUTTON2                     1042
#define IDC_BUTTON_SETCORROPTION        1042
#define IDC_BUTTON_PREPACQ              1043
#define IDC_EDIT_TRIGGERMODE            1044
#define IDC_STATIC_TRIGGERMODE          1045

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1046
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
