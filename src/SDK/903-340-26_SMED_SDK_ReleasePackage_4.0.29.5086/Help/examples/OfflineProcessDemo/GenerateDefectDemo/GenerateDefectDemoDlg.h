
// GenerateDefectDemoDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CGenerateDefectDemoDlg dialog
class CGenerateDefectDemoDlg : public CDialogEx
{
// Construction
public:
	CGenerateDefectDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_GENERATEDEFECTDEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
    CComboBox m_cProductNoCombo;
public:
    afx_msg void OnCbnSelchangeComboProductNo();
    afx_msg void OnBnClickedButtonSelectIni();
    afx_msg void OnBnClickedButtonSelectRawdata();
    afx_msg void OnBnClickedButtonSelectResult();
    afx_msg void OnBnClickedButtonGenerate();
};
