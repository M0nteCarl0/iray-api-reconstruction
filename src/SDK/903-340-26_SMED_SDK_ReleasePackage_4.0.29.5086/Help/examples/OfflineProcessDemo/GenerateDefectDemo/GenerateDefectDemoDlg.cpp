
// GenerateDefectDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GenerateDefectDemo.h"
#include "GenerateDefectDemoDlg.h"
#include "../IRaySignalProcessingLib.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static const int PRODUCTNO_VALUES[] = { 0x06, 0x20, 0x25, 0x26, 0x2D, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x38, 0x3A, 0x3C, 0x3D };
static LPCTSTR PRODUCTNO_STRINGS[] = { _T("Mercu0909F"), _T("Mars1417V"), _T("Mars1717V"), _T("Venu1717MF"), _T("Mars1717XU"), _T("Mars1012X"),
_T("Mercu1717V"), _T("Mercu1616TE"), _T("Mercu1616VE"), _T("Mars1717XF"), _T("Mars1417XF"), _T("Mercu0909FN"), _T("Venu1717MN"), _T("Venu1012X"),
_T("Mars1012VN") };
static const int PRODUCTNO_IMGW[] = { 1024, 2304, 3072, 3072, 3072, 2448, 3072, 2048, 2048, 2848, 2342, 1024, 3072, 2448, 2448 };
static const int PRODUCTNO_IMGH[] = { 1024, 2800, 3072, 3072, 3072, 2048, 3072, 2048, 2048, 2840, 2840, 1024, 3072, 2048, 2048 };


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CGenerateDefectDemoDlg dialog



CGenerateDefectDemoDlg::CGenerateDefectDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CGenerateDefectDemoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CGenerateDefectDemoDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO_PRODUCTNO, m_cProductNoCombo);
}

BEGIN_MESSAGE_MAP(CGenerateDefectDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_CBN_SELCHANGE(IDC_COMBO_PRODUCTNO, &CGenerateDefectDemoDlg::OnCbnSelchangeComboProductNo)
    ON_BN_CLICKED(IDC_BUTTON_SELECT_INI, &CGenerateDefectDemoDlg::OnBnClickedButtonSelectIni)
    ON_BN_CLICKED(IDC_BUTTON_SELECT_RAWDATA, &CGenerateDefectDemoDlg::OnBnClickedButtonSelectRawdata)
    ON_BN_CLICKED(IDC_BUTTON_SELECT_RESULT, &CGenerateDefectDemoDlg::OnBnClickedButtonSelectResult)
    ON_BN_CLICKED(IDC_BUTTON_GENERATE, &CGenerateDefectDemoDlg::OnBnClickedButtonGenerate)
END_MESSAGE_MAP()


// CGenerateDefectDemoDlg message handlers

BOOL CGenerateDefectDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

    // TODO: Add extra initialization here
    for (int i = 0; i < _countof(PRODUCTNO_STRINGS); ++i)
    {
        m_cProductNoCombo.AddString(PRODUCTNO_STRINGS[i]);
    }

    m_cProductNoCombo.SetCurSel(0);
    OnCbnSelchangeComboProductNo();
    SetDlgItemText(IDC_EDIT_RESULT, _T(".\\result\\"));
    CreateDirectory(_T(".\\result\\"), NULL);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGenerateDefectDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CGenerateDefectDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CGenerateDefectDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CGenerateDefectDemoDlg::OnCbnSelchangeComboProductNo()
{
    const UINT PRODUCT_SEL = m_cProductNoCombo.GetCurSel();
    SetDlgItemInt(IDC_EDIT_IMGW, PRODUCTNO_IMGW[PRODUCT_SEL]);
    SetDlgItemInt(IDC_EDIT_IMGH, PRODUCTNO_IMGH[PRODUCT_SEL]);
}


void CGenerateDefectDemoDlg::OnBnClickedButtonSelectIni()
{
    CFileDialog dlgFile(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, _T("Ini Files (*.ini)|*.ini||"));
    if (dlgFile.DoModal() != IDOK)
    {
        return;
    }

    SetDlgItemText(IDC_EDIT_INI, dlgFile.GetPathName());
}


void CGenerateDefectDemoDlg::OnBnClickedButtonSelectRawdata()
{
    TCHAR pszPath[1024] = { 0 };
    BROWSEINFO bi;
    bi.hwndOwner = GetSafeHwnd();
    bi.pidlRoot = NULL;
    bi.pszDisplayName = NULL;
    bi.lpszTitle = TEXT("");
    bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
    bi.lpfn = NULL;
    bi.lParam = 0;
    bi.iImage = 0;

    LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
    if (pidl == NULL)
    {
        return;
    }

    if (SHGetPathFromIDList(pidl, pszPath))
    {
        SetDlgItemText(IDC_EDIT_RAWDATA, pszPath);
    }
}


void CGenerateDefectDemoDlg::OnBnClickedButtonSelectResult()
{
    TCHAR pszPath[1024] = { 0 };
    BROWSEINFO bi;
    bi.hwndOwner = GetSafeHwnd();
    bi.pidlRoot = NULL;
    bi.pszDisplayName = NULL;
    bi.lpszTitle = TEXT("");
    bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
    bi.lpfn = NULL;
    bi.lParam = 0;
    bi.iImage = 0;

    LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
    if (pidl == NULL)
    {
        return;
    }

    if (SHGetPathFromIDList(pidl, pszPath))
    {
        SetDlgItemText(IDC_EDIT_RESULT, pszPath);
    }
}


void CGenerateDefectDemoDlg::OnBnClickedButtonGenerate()
{
    const UINT IMG_W = GetDlgItemInt(IDC_EDIT_IMGW);
    const UINT IMG_H = GetDlgItemInt(IDC_EDIT_IMGH);
    if (0 == IMG_W || 0 == IMG_H || 9999 < IMG_W || 9999 < IMG_H)
    {
        AfxMessageBox(_T("Invalid width or height!"));
        return;
    }

    CString sIniPath;
    CString sDataDir;
    CString sResultDir;
    GetDlgItemText(IDC_EDIT_INI, sIniPath);
    GetDlgItemText(IDC_EDIT_RAWDATA, sDataDir);
    GetDlgItemText(IDC_EDIT_RESULT, sResultDir);
    if (!PathFileExists(sIniPath) || !PathIsDirectory(sDataDir) || !PathIsDirectory(sResultDir))
    {
        AfxMessageBox(_T("Invalid path of ini or raw data or result!"));
        return;
    }

    if (0 != sDataDir.Right(1).CompareNoCase(_T("\\")) && 0 != sDataDir.Right(1).CompareNoCase(_T("/")))
    {
        sDataDir += _T("\\");
    }

    if (0 != sResultDir.Right(1).CompareNoCase(_T("\\")) && 0 != sResultDir.Right(1).CompareNoCase(_T("/")))
    {
        sResultDir += _T("\\");
    }

    LAB_ERRCODE nRetCode = LabErr_OK;
    HMODULE hDll = ::LoadLibrary(_T("SignalProcessing.dll"));
    if (!hDll)
    {
        AfxMessageBox(_T("Failed to load DLL!"));
        return;
    }

    FnGenerateDefect fGen = (FnGenerateDefect)::GetProcAddress(hDll, IRAY_SP_GENERATE_DEFECT);
    GenerateDefectInfo info;
    memset(&info, 0, sizeof(GenerateDefectInfo));
    info.nFpdProdNo = PRODUCTNO_VALUES[m_cProductNoCombo.GetCurSel()];
    info.nImgWidth = IMG_W;
    info.nImgHeight = IMG_H;
    info.pszConfigIniFilePath = sIniPath.GetBuffer();
    info.pszImageFolderPath = sDataDir.GetBuffer();
    info.pszOutputFolderPath = sResultDir.GetBuffer();

    nRetCode = fGen(&info);
    FreeLibrary(hDll);
    if (LabErr_OK != nRetCode)
    {
        CString sTemp;
        sTemp.Format(_T("Failed(%d)!"), nRetCode);
        AfxMessageBox(sTemp);
        return;
    }

    MessageBox(_T("Generated."), NULL, MB_ICONINFORMATION | MB_OK);
}
