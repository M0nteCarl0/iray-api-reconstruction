
// OfflineDefectCorrectionDemoDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "../../IRaySignalProcessingLib.h"


// COfflineDefectCorrectionDemoDlg dialog
class COfflineDefectCorrectionDemoDlg : public CDialogEx
{
// Construction
public:
	COfflineDefectCorrectionDemoDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_OFFLINEDEFECTCORRECTIONDEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
    CComboBox m_cProductNoCombo;
    FnApplyOfflineCorrection m_fApply;
    FnUninitializeOfflineCorrection m_fUninit;
    void* m_pOfflineCorrectionInstance;
    HMODULE m_hDll;
    unsigned short* m_pImageBuf;
    unsigned int m_nImgSize;
public:
    afx_msg void OnBnClickedButtonApply();
    afx_msg void OnBnClickedButtonOn();
    afx_msg void OnBnClickedButtonOff();
    afx_msg void OnCbnSelchangeComboProductNo();
    afx_msg void OnBnClickedButtonSelectWorkdir();
    afx_msg void OnBnClickedButtonSelectImage();
};
