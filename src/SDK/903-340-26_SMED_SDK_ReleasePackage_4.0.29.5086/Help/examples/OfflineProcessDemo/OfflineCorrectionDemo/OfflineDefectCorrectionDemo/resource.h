//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by OfflineDefectCorrectionDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_OFFLINEDEFECTCORRECTIONDEMO_DIALOG 102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_APPLY                1000
#define IDC_COMBO_PRODUCTNO             1001
#define IDC_EDIT_IMGW                   1002
#define IDC_EDIT_IMGH                   1003
#define IDC_BUTTON_ON                   1004
#define IDC_BUTTON_OFF                  1005
#define IDC_EDIT_WORKDIR                1006
#define IDC_EDIT_SUBSET                 1007
#define IDC_BUTTON_SELECT_WORKDIR       1008
#define IDC_EDIT_IMAGE                  1009
#define IDC_BUTTON_SELECT_IMAGE         1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
