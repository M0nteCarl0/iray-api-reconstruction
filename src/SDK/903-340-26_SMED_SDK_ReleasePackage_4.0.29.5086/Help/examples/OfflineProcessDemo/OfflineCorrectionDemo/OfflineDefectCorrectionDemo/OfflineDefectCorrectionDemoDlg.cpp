
// OfflineDefectCorrectionDemoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OfflineDefectCorrectionDemo.h"
#include "OfflineDefectCorrectionDemoDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


static const int PRODUCTNO_VALUES[] = { 0x06, 0x20, 0x25, 0x26, 0x2D, 0x29, 0x30, 0x31, 0x32, 0x33, 0x34, 0x38, 0x3A, 0x3C, 0x3D };
static LPCTSTR PRODUCTNO_STRINGS[] = { _T("Mercu0909F"), _T("Mars1417V"), _T("Mars1717V"), _T("Venu1717MF"), _T("Mars1717XU"), _T("Mars1012X"), 
    _T("Mercu1717V"), _T("Mercu1616TE"), _T("Mercu1616VE"), _T("Mars1717XF"), _T("Mars1417XF"), _T("Mercu0909FN"), _T("Venu1717MN"), _T("Venu1012X"), 
    _T("Mars1012VN") };
static const int PRODUCTNO_IMGW[] = { 1024, 2304, 3072, 3072, 3072, 2448, 3072, 2048, 2048, 2848, 2342, 1024, 3072, 2448, 2448 };
static const int PRODUCTNO_IMGH[] = { 1024, 2800, 3072, 3072, 3072, 2048, 3072, 2048, 2048, 2840, 2840, 1024, 3072, 2048, 2048 };


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// COfflineDefectCorrectionDemoDlg dialog



COfflineDefectCorrectionDemoDlg::COfflineDefectCorrectionDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(COfflineDefectCorrectionDemoDlg::IDD, pParent)
    , m_fApply(NULL)
    , m_fUninit(NULL)
    , m_pOfflineCorrectionInstance(NULL)
    , m_hDll(NULL)
    , m_pImageBuf(NULL)
    , m_nImgSize(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void COfflineDefectCorrectionDemoDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_COMBO_PRODUCTNO, m_cProductNoCombo);
}

BEGIN_MESSAGE_MAP(COfflineDefectCorrectionDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON_APPLY, &COfflineDefectCorrectionDemoDlg::OnBnClickedButtonApply)
    ON_BN_CLICKED(IDC_BUTTON_ON, &COfflineDefectCorrectionDemoDlg::OnBnClickedButtonOn)
    ON_BN_CLICKED(IDC_BUTTON_OFF, &COfflineDefectCorrectionDemoDlg::OnBnClickedButtonOff)
    ON_CBN_SELCHANGE(IDC_COMBO_PRODUCTNO, &COfflineDefectCorrectionDemoDlg::OnCbnSelchangeComboProductNo)
    ON_BN_CLICKED(IDC_BUTTON_SELECT_WORKDIR, &COfflineDefectCorrectionDemoDlg::OnBnClickedButtonSelectWorkdir)
    ON_BN_CLICKED(IDC_BUTTON_SELECT_IMAGE, &COfflineDefectCorrectionDemoDlg::OnBnClickedButtonSelectImage)
END_MESSAGE_MAP()


// COfflineDefectCorrectionDemoDlg message handlers

BOOL COfflineDefectCorrectionDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	ShowWindow(SW_MINIMIZE);

	// TODO: Add extra initialization here
    for (int i = 0; i < _countof(PRODUCTNO_STRINGS); ++i)
    {
        m_cProductNoCombo.AddString(PRODUCTNO_STRINGS[i]);
    }

    m_cProductNoCombo.SetCurSel(0);
    OnCbnSelchangeComboProductNo();
    SetDlgItemText(IDC_EDIT_SUBSET, _T("Default"));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void COfflineDefectCorrectionDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void COfflineDefectCorrectionDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR COfflineDefectCorrectionDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void COfflineDefectCorrectionDemoDlg::OnBnClickedButtonApply()
{
    if (!m_fApply)
    {
        return;
    }

    CString sFname;
    GetDlgItemText(IDC_EDIT_IMAGE, sFname);
    HANDLE hFile = CreateFile(sFname, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE)
    {
        AfxMessageBox(_T("Failed to open image file!"));
        return;
    }

    DWORD nRead = 0;
    if (!ReadFile(hFile, m_pImageBuf, m_nImgSize * sizeof(WORD), &nRead, NULL) || nRead != m_nImgSize * sizeof(WORD))
    {
        CloseHandle(hFile);
        AfxMessageBox(_T("Failed to load image file!"));
        return;
    }

    CloseHandle(hFile);
    sFname += _T(".d.raw");
    hFile = CreateFile(sFname, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (hFile == INVALID_HANDLE_VALUE)
    {
        AfxMessageBox(_T("Failed to open image file!"));
        return;
    }

    int nRetCode = m_fApply(m_pOfflineCorrectionInstance, m_pImageBuf, NULL, -1, -1);
    if (LabErr_OK != nRetCode)
    {
        CString sTemp;
        sTemp.Format(_T("Failed(%d)!"), nRetCode);
        AfxMessageBox(sTemp);
        return;
    }

    DWORD nWritten = 0;
    if (!WriteFile(hFile, m_pImageBuf, m_nImgSize * sizeof(WORD), &nWritten, NULL) || nWritten != m_nImgSize * sizeof(WORD))
    {
        CloseHandle(hFile);
        AfxMessageBox(_T("Failed to save image file!"));
        return;
    }

    CloseHandle(hFile);

    CString sTemp;
    sTemp.Format(_T("Saved at '%s'."), sFname);
    MessageBox(sTemp, NULL, MB_ICONINFORMATION | MB_OK);
}


void COfflineDefectCorrectionDemoDlg::OnBnClickedButtonOn()
{
    if (m_hDll)
    {
        AfxMessageBox(_T("Unknown status!"));
        return;
    }

    const UINT IMG_W = GetDlgItemInt(IDC_EDIT_IMGW);
    const UINT IMG_H = GetDlgItemInt(IDC_EDIT_IMGH);
    if (0 == IMG_W || 0 == IMG_H || 9999 < IMG_W || 9999 < IMG_H)
    {
        AfxMessageBox(_T("Invalid width or height!"));
        return;
    }

    CString sWorkDir;
    CString sSubset;
    GetDlgItemText(IDC_EDIT_WORKDIR, sWorkDir);
    GetDlgItemText(IDC_EDIT_SUBSET, sSubset);
    if (!PathIsDirectory(sWorkDir))
    {
        AfxMessageBox(_T("Invalid workdir!"));
        return;
    }

    m_nImgSize = IMG_W * IMG_H;
    m_pImageBuf = (unsigned short*)malloc(m_nImgSize * sizeof(unsigned short));
    if (!m_pImageBuf)
    {
        AfxMessageBox(_T("Lack of memory!"));
        return;
    }

    LAB_ERRCODE nRetCode = LabErr_OK;
    m_hDll = ::LoadLibrary(_T("SignalProcessing.dll"));
    if (!m_hDll)
    {
        AfxMessageBox(_T("Failed to load DLL!"));
        return;
    }

    FnInitializeOfflineCorrection fInit = (FnInitializeOfflineCorrection)::GetProcAddress(m_hDll, IRAY_SP_OFFLINECORR_INIT);
    m_fApply = (FnApplyOfflineCorrection)::GetProcAddress(m_hDll, IRAY_SP_OFFLINECORR_APPLY);
    m_fUninit = (FnUninitializeOfflineCorrection)::GetProcAddress(m_hDll, IRAY_SP_OFFLINECORR_UNINIT);

    m_pOfflineCorrectionInstance = NULL;
    OfflineCorrectionInfo info;
    memset(&info, 0, sizeof(OfflineCorrectionInfo));
    info.nFpdProdNo = PRODUCTNO_VALUES[m_cProductNoCombo.GetCurSel()];
    info.nFpdTriggerMode = -1;
    info.nFpdPrepCapMode = -1;
    info.nFpdSeqInterval = -1;
    info.nFpdExpMode = -1;
    info.nImgWidth = IMG_W;
    info.nImgHeight = IMG_H;
    info.pszFpdWorkDir = sWorkDir.GetBuffer();
    info.pszFpdCaliSubset = sSubset.GetBuffer();
    info.nDoDefect = 1;

    nRetCode = fInit(&info, &m_pOfflineCorrectionInstance);
    if (LabErr_OK != nRetCode)
    {
        FreeLibrary(m_hDll);
        m_hDll = NULL;

        CString sTemp;
        sTemp.Format(_T("Failed(%d)!"), nRetCode);
        AfxMessageBox(sTemp);
        return;
    }

    GetDlgItem(IDC_BUTTON_ON)->EnableWindow(FALSE);
    GetDlgItem(IDC_BUTTON_OFF)->EnableWindow(TRUE);
    GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(TRUE);
}


void COfflineDefectCorrectionDemoDlg::OnBnClickedButtonOff()
{
    if (!m_hDll)
    {
        return;
    }

    if (m_fUninit)
    {
        int nRetCode = m_fUninit(m_pOfflineCorrectionInstance);
        if (LabErr_OK != nRetCode)
        {
            CString sTemp;
            sTemp.Format(_T("Failed(%d)!"), nRetCode);
            AfxMessageBox(sTemp);
            return;
        }
    }

    FreeLibrary(m_hDll);
    m_hDll = NULL;
    GetDlgItem(IDC_BUTTON_ON)->EnableWindow(TRUE);
    GetDlgItem(IDC_BUTTON_OFF)->EnableWindow(FALSE);
    GetDlgItem(IDC_BUTTON_APPLY)->EnableWindow(FALSE);
}


void COfflineDefectCorrectionDemoDlg::OnCbnSelchangeComboProductNo()
{
    const UINT PRODUCT_SEL = m_cProductNoCombo.GetCurSel();
    SetDlgItemInt(IDC_EDIT_IMGW, PRODUCTNO_IMGW[PRODUCT_SEL]);
    SetDlgItemInt(IDC_EDIT_IMGH, PRODUCTNO_IMGH[PRODUCT_SEL]);
}


void COfflineDefectCorrectionDemoDlg::OnBnClickedButtonSelectWorkdir()
{
    TCHAR pszPath[1024] = { 0 };
    BROWSEINFO bi;
    bi.hwndOwner = GetSafeHwnd();
    bi.pidlRoot = NULL;
    bi.pszDisplayName = NULL;
    bi.lpszTitle = TEXT("");
    bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_STATUSTEXT;
    bi.lpfn = NULL;
    bi.lParam = 0;
    bi.iImage = 0;

    LPITEMIDLIST pidl = SHBrowseForFolder(&bi);
    if (pidl == NULL)
    {
        return;
    }

    if (SHGetPathFromIDList(pidl, pszPath))
    {
        SetDlgItemText(IDC_EDIT_WORKDIR, pszPath);
    }
}


void COfflineDefectCorrectionDemoDlg::OnBnClickedButtonSelectImage()
{
    CFileDialog dlgFile(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST, _T("Raw Image Files (*.raw)|*.raw|All Files (*.*)|*.*||"));
    if (dlgFile.DoModal() != IDOK)
    {
        return;
    }

    SetDlgItemText(IDC_EDIT_IMAGE, dlgFile.GetPathName());
}
